<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
if(defined('NO_JS_ANALYTICS') && NO_JS_ANALYTICS) {
	// заглушки для функций учета событий
	?><script type="text/javascript">
		yaCounter24799106 = {
			reachGoal: function(sGoal) {
				alert('reachGoal: '+sGoal);
			}
		}; 
		ga = function(sParam1 = '', sParam2 = '', sParam3 = '', sParam4 = '') {
			alert('ga: param1 = '+sParam1+'; param2 = '+sParam2+'; param3 = '+sParam3+'; param4 = '+sParam4);
		};
	</script><?
	return;
}

//
// код Google Analytics для области <head>
//
ob_start();
?><script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-46047978-1', 'auto');
	ga('send', 'pageview');
	<?
	$iTmpUserId = $GLOBALS['USER']->GetID();
	if($iTmpUserId) {
		echo 'ga(\'create\', \'UA-46047978-1\', {\'USER_ID\': \''.$iTmpUserId.'\'});';
		echo 'ga(\'set\', \'&uid\', '.$iTmpUserId.');';
	}
?></script><?
$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());


//
// Код, который выводится в конце <body>
//
?><!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
	(w[c] = w[c] || []).push(function() {
		try {
			w.yaCounter24799106 = new Ya.Metrika({id:24799106,
					webvisor:true,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					trackHash:true});
		} catch(e) { }
	});

	var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
	s.type = "text/javascript";
	s.async = true;
	s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
	} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24799106" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --><?

//
// calltouch
//
ob_start();
?><script type="text/javascript">
    (function(w, d, e) {
        var a = 'all', b = 'tou'; var src = b + 'c' +'h'; src = 'm' + 'o' + 'd.c' + a + src;
        var jsHost = (("https:" == d.location.protocol) ? "https://" : "http://")+ src;
        s = d.createElement(e); p = d.getElementsByTagName(e)[0]; s.async = 1; s.src = jsHost +"."+"r"+"u/d_client.js?param;ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie);
        if(!w.jQuery) { jq = d.createElement(e); jq.src = jsHost  +"."+"r"+'u/js/jquery-1.7.min.js'; p.parentNode.insertBefore(jq, p);}
        p.parentNode.insertBefore(s, p);
    }(window, document, 'script'));
</script><?
$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());

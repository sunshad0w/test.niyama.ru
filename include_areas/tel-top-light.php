<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();?>

<div class="feedback">
	<div <?=CCustomProject::GetAnalyticsByCode('link-telefon')?> class="feedback__phone"><a href="tel:84957817819" onclick="yaCounter24799106.reachGoal('tel'); return true;">+7 495 781-781-9</a></div>
	<div class="feedback__links align-right">
		<?=CCustomProject::GetOctolineFormLink('Обратный звонок', '', '', ' '.CCustomProject::GetAnalyticsByCode('link-back_call', ''))?>
	</div>
</div>
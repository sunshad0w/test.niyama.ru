<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
$GLOBALS['APPLICATION']->IncludeComponent(
	'niyama:discount.leveles-timeline',
	'niyama.1.0',
	array(
		'DISCOUNT_LEVELS_IBLOCK_CODE' => 'discount_levels',
		'USERS_DISCOUNT_LEVELS_IBLOCK_CODE' => 'users_discount_levels',
		'USER_ID' => $GLOBALS['USER']->GetId(),
		'CACHE_TYPE' => 'A',
		'CACHE_TIME' => '43200',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

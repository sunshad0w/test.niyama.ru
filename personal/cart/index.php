<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle('Корзина');
?>

<div class="main-content">
    <div class="main-content__wrapper">
               <img src="/local/templates/pizzapi/img/travka.png" class="bg-travka">
               <img src="/local/templates/pizzapi/img/pomidor.png" class="bg-pomidor">
               <img src="/local/templates/pizzapi/img/grib.png" class="bg-grib">

<div class = "container">
<h4 class="order_title">Корзина</h4>
<div class="bitrix_sale_basket_basket"> 
<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "orderbasket", Array(
	"ACTION_VARIABLE" => "action",	// Название переменной действия
		"AUTO_CALCULATION" => "Y",
		"TEMPLATE_THEME" => "blue",
		"COLUMNS_LIST" => array(	// Выводимые колонки
			0 => "NAME",
			1 => "QUANTITY",
			2 => "PRICE",
			3 => "DELETE",

		),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",	// Рассчитывать скидку для каждой позиции (на все количество товара)
		"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
		"OFFERS_PROPS" => array(),// Свойства, влияющие на пересчет корзины
		"PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
		"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
		"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	),
	false
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
</div>
</div>
</div>
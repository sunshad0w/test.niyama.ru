<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2017-05-05
 * Time: 15:12
 */
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use
    Bitrix,
    Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\Payment,
    Bitrix\Sale,
    Bitrix\Sale\Order;

Loader::includeModule('sale');

if(!$USER->IsAuthorized()) {
    $userId = CUsersData::GetGuestUserId();
    $USER->Authorize($userId);
}

$id = intval($_REQUEST['ID']);

/** @var Order $order */
if ($order = Order::loadByAccountNumber($id))
{
    $arOrder = $order->getFieldValues();
    $arResult["ORDER_ID"] = $arOrder["ID"];
    $arResult["ACCOUNT_NUMBER"] = $arOrder["ACCOUNT_NUMBER"];
}

$checkedBySession = is_array($_SESSION['SALE_ORDER_ID']) && in_array(intval($order->getId()), $_SESSION['SALE_ORDER_ID']);

$arPaySysAction = null;

if (!empty($arOrder) && ($order->getUserId() == $USER->GetID() || $checkedBySession))
{

    $arResult["PAYMENT"] = array();
    $paymentCollection = $order->getPaymentCollection();
    /** @var Payment $payment */
    foreach ($paymentCollection as $payment)
    {
        $arResult["PAYMENT"][$payment->getId()] = $payment->getFieldValues();

        if (intval($payment->getPaymentSystemId()) > 0 && !$payment->isPaid())
        {
            $paySystemService = PaySystem\Manager::getObjectById($payment->getPaymentSystemId());

            if (!empty($paySystemService))
            {
                $arPaySysAction = $paySystemService->getFieldsValues();

                /** @var PaySystem\ServiceResult $initResult */
                $initResult = $paySystemService->initiatePay($payment, null, PaySystem\BaseServiceHandler::STRING);
                if ($initResult->isSuccess())
                    $arPaySysAction['BUFFERED_OUTPUT'] = $initResult->getTemplate();
                else
                    $arPaySysAction["ERROR"] = $initResult->getErrorMessages();

                $arResult["PAYMENT"][$payment->getId()]['PAID'] = $payment->getField('PAID');

                $arOrder['PAYMENT_ID'] = $payment->getId();
                $arOrder['PAY_SYSTEM_ID'] = $payment->getPaymentSystemId();
                $arPaySysAction["NAME"] = htmlspecialcharsEx($arPaySysAction["NAME"]);
                $arPaySysAction["IS_AFFORD_PDF"] = $paySystemService->isAffordPdf();

                if ($arPaySysAction > 0)
                    $arPaySysAction["LOGOTIP"] = CFile::GetFileArray($arPaySysAction["LOGOTIP"]);

                if (!$payment->isInner())
                {
                    // compatibility
                    \CSalePaySystemAction::InitParamArrays($order->getFieldValues(), $order->getId(), '', array(), $payment->getFieldValues());
                    $map = CSalePaySystemAction::getOldToNewHandlersMap();
                    $oldHandler = array_search($arPaySysAction["ACTION_FILE"], $map);
                    if ($oldHandler !== false && !$paySystemService->isCustom())
                        $arPaySysAction["ACTION_FILE"] = $oldHandler;

                    if (strlen($arPaySysAction["ACTION_FILE"]) > 0 && $arPaySysAction["NEW_WINDOW"] != "Y")
                    {
                        $pathToAction = $_SERVER['DOCUMENT_ROOT'].$arPaySysAction["ACTION_FILE"];

                        $pathToAction = str_replace("\\", "/", $pathToAction);
                        while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
                            $pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);

                        if (file_exists($pathToAction))
                        {
                            if (is_dir($pathToAction) && file_exists($pathToAction."/payment.php"))
                                $pathToAction .= "/payment.php";

                            $arPaySysAction["PATH_TO_ACTION"] = $pathToAction;

                        }
                    }

                    $arResult["PAY_SYSTEM"] = $arPaySysAction;
                }

                $arResult["PAY_SYSTEM_LIST"][$payment->getPaymentSystemId()] = $arPaySysAction;
            }
            else
                $arResult["PAY_SYSTEM_LIST"][$payment->getPaymentSystemId()] = array('ERROR' => true);
        }
    }
    $arResult["ORDER"] = $arOrder;
}
else
    $arResult["ACCOUNT_NUMBER"] = $orderId;
echo "<div>";
        // Явно указан обработчик РФИ. Не подклюяается через $arPaySysAction["PATH_TO_ACTION"]
        require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/rficb.payment/payment/payment.php";

echo "</div>";

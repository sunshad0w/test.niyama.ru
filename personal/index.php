<?
define("NEED_AUTH", true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

if(CUsersData::isGuest()) {
    $USER->Logout();
    LocalRedirect("/");
}

$APPLICATION->SetTitle("Личные данные");
?>


<div class="main-content">
    <div class="main-content__wrapper">


        <div class="catalog-wrapper">
            <div class="container">
                <div class="profile">
                    <div class="profile__left__column">
                        <div class="prifole__discount_card">
                            <h2>Моя карта</h2>

                            <? $APPLICATION->IncludeComponent(
                                'adv:system.empty',
                                'niyama.1.0.profile-card',
                                array(),
                                null,
                                array(
                                    'HIDE_ICONS' => 'Y'
                                )
                            ); ?>
                        </div>
                        <div class="profile__order__list">
                            <h2>История заказов</h2>
                            <?
                            $APPLICATION->IncludeComponent("bitrix:sale.personal.order.list", "", Array(
                                    "STATUS_COLOR_N" => "green",
                                    "STATUS_COLOR_P" => "yellow",
                                    "STATUS_COLOR_F" => "gray",
                                    "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
                                    "PATH_TO_DETAIL" => "order_detail.php?ID=#ID#",
                                    "PATH_TO_COPY" => "basket.php",
                                    "PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
                                    "PATH_TO_BASKET" => "cart/index.php",
                                    "PATH_TO_PAYMENT" => "payment.php",
                                    "PATH_TO_CATALOG" => "/",
                                    "ORDERS_PER_PAGE" => 20,
                                    "ID" => $ID,
                                    "SET_TITLE" => "N",
                                    "SAVE_IN_SESSION" => "Y",
                                    "NAV_TEMPLATE" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "3600",
                                    "CACHE_GROUPS" => "Y",
                                    "HISTORIC_STATUSES" => "F",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y H:i"
                                )
                            );
                            ?>
                            <div style="display:none;">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:sale.basket.basket.line",
                                    "pizzabasketprofile",
                                    Array(
                                        "HIDE_ON_BASKET_PAGES" => "Y",
                                        "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                        "POSITION_FIXED" => "Y",
                                        "SHOW_IMAGE" => "Y",
                                        "SHOW_NOTAVAIL" => "N",
                                        "SHOW_NUM_PRODUCTS" => "Y",
                                        "SHOW_PRICE" => "Y",
                                        "SHOW_PRODUCTS" => "Y",
                                        "SHOW_SUBSCRIBE" => "Y",
                                        "SHOW_SUMMARY" => "Y",
                                        "SHOW_TOTAL_PRICE" => "Y",
                                        'EMPTY' => 'Y'
                                    )
                                ); ?>
                            </div>
                        </div>
                        <!--<div class="profile-sale-size">
                            Скидка в ресторане
                            <div class="profile-sale-size__circle">10</div>
                        </div>
                        <div class="profile-sale-size">
                            Скидка на доставку
                            <div class="profile-sale-size__circle">15</div>
                        </div>-->
                    </div>
                    <div class="profile__right__column">
                        <h2>Личные данные</h2>

                        <div class="profile-margin">
                            <? $rsUser = CUser::GetByID($USER->GetId());
                            $arUser = $rsUser->Fetch();
                            $rsFile = CFile::GetByID($arUser['PERSONAL_PHOTO']);
                            $arFile = $rsFile->Fetch();
                            $arResizedFile = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_EXACT, false, false, true, false) ?>
                            <div class="profile-data" id="profileDataContainer">
                                <div class="profile-data__ava">
                                    <img src="<?= $arResizedFile['src'] ?>"/>
                                </div>
                                <div class="profile-data__text">
                                    <div class="profile-data__name">
                                        <?= $arUser['NAME'] ?>              <?= $arUser['LAST_NAME'] ?>
                                    </div>
                                    <div class="profile-data__email">
                                        <?= $arUser['EMAIL'] ?>
                                    </div>
                                    <div class="profile-data__phone">
                                        <?= $arUser['PERSONAL_PHONE'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-data-edit">
                                <i class="fa fa-pencil"></i> <a class="show-main-profile-popup">Редактировать
                                    личные данные</a>
                            </div>

                            <h3>Адреса доставки</h3>
                            <div class="profile-addresses">
                                <?
                                $APPLICATION->IncludeComponent("pizzapi:sale.personal.profile.list", "", Array(
                                    "PATH_TO_DETAIL" => "",
                                    "PER_PAGE" => 20,
                                    "SET_TITLE" => "N"
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<script>
    BX.ready(function () {
        var oPopup = new BX.PopupWindow('MainProfile', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'black', opacity: '80'
            },
            events: {
                onPopupShow: function() {
                    var readURL = function(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            
                            reader.onload = function (e) {
                                $('#personalPhotoImg').attr('src', e.target.result);
                            };
                            
                            reader.readAsDataURL(input.files[0]);
                        }
                    };

                    var c_b_obj = {
                        "onincomplete" : function () {
                            $(this).addClass('notValid');
                        },
                        "oncomplete"   : function () {
                            $(this).removeClass('notValid');
                        }
                    };

                    $(document).on('keypress', "input[name='NAME']", FilterTextInput.inputOnlyLetters);
                    $(document).on('keypress', "input[name='LAST_NAME']", FilterTextInput.inputOnlyLetters);
                    $(".popup__personal_phone").inputmask("+7 (999) 999-99-99", c_b_obj);
                    $('#personalPhotoInput').change(function(){
                        readURL(this);
                    });

                    $('#PERSONAL_BIRTHDAY').change(function () {
                        $(this).removeClass('notValid');
                    });

                    $( "#profile-form-popup" ).submit(function( event ) {
                        event.preventDefault();
                       var inputDate = new Date($('#PERSONAL_BIRTHDAY').val());
                       var nowDate = new Date();
                       if (!inputDate
                           || parseInt(inputDate.getFullYear()) <= 1950
                           || parseInt(inputDate.getFullYear()) >= parseInt(nowDate.getFullYear())
                       ) {
                           $('#PERSONAL_BIRTHDAY').addClass('notValid');
                           return false;
                       }


                        var formData = new FormData($(this)[0]);
                        formData.append('save', '1');
                        $.ajax({
                            type: "POST",
                            url: '/local/ajax/mainProfile.php',
                            data: formData,
                            processData: false,
                            contentType: false,
                            headers: {
                                "Upgrade-Insecure-Requests": 1
                            },
                            success: function(data) {
                                try {
                                    data = JSON.parse(data);
                                    $('#profilePopupErrors').html(data['errors']);
                                } catch (e) {
                                    $('#profileDataContainer').html(data);
                                    Informer.success('Ваши данные были успешно сохранены');
                                    oPopup.close();
                                }
                            }
                        });
                    });
                }
            }
        });
        var showMainProfilePopup = function (oPopup) {
            $.post('/local/ajax/mainProfile.php', {'dataType':'html'},
                function (response) {
                    if (response) {
                        oPopup.setContent(response);
                        oPopup.show();
                    } else {
                        console.log('ошибка')
                    }
                });
        };
        $('.show-main-profile-popup').click(
            BX.proxy(function (e) {
                if (!e) {
                    e = window.event;
                }
                BX.PreventDefault(e);
                showMainProfilePopup (this);
            }, oPopup));
    });
</script>

    <script type="application/javascript">
        var delAvatar = function (obj) {
            $(obj).closest('.profile-popup__ava').find('img').css('background','transparent').prop('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkNDQ0ZGRUQ1MUY1ODExRTc5NzU0QTE2OEMwN0FFQTIxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkNDQ0ZGRUQ2MUY1ODExRTc5NzU0QTE2OEMwN0FFQTIxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Q0NDRkZFRDMxRjU4MTFFNzk3NTRBMTY4QzA3QUVBMjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Q0NDRkZFRDQxRjU4MTFFNzk3NTRBMTY4QzA3QUVBMjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7385oHAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAAAxJREFUeNpiYAAIMAAAAgABT21Z4QAAAABJRU5ErkJggg==');
            $(obj).closest('.profile-popup__ava').find('.deleteAva').hide();
            $(obj).closest('.profile-popup__ava').find('input[name="PERSONAL_PHOTO_del"]').val('Y');
        };
        $(document).on('change', '#personalPhotoInput', function () {
            $('.deleteAva').show();
            $('.profile-popup__ava img').css('background','#647b9c');
        })
    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
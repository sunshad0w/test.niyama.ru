<?
$debug = false;

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

// получим пост данные
$pass = $_POST['pass'];
$numbersXml = $_POST['xml'];
$date = $_POST['date'];

// проверим пароль
if ((!$pass || $pass != md5('topyandex')) && !($debug)) {
    returnError('Неверный пароль');
}

// В зависимости от вида запроса отправляется либо параметр xml (запрос со списком идентификаторов), либо параметр date (запрос за дату).
if (CModule::IncludeModule("sale")) {

    // фильтр заказов только экшнпей
    $arFilter = Array(
        "PROPERTY_VAL_BY_CODE_SOURCE" => "actionpay",
    );

    // добавим фильтр либо по дате либо по айдишникам заказов
    if ($date) {
        $type = 'date';
        $arFilter[">=DATE_INSERT"] = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), strtotime($date));
    } elseif (strlen($numbersXml) > 1 || 1) {
        $type = 'numbers';
        $p = xml_parser_create();
        xml_parse_into_struct($p, $numbersXml, $vals);
        xml_parser_free($p);
        foreach ($vals as $key => $arr) {
            if ($arr['tag'] == 'ITEM') {
                $numbers[] = $arr['value'];
            }
        }
        if (!count($numbers)) {
            returnError();
        } else {
            $arFilter["UF_FO_ORDER_CODE"] = $numbers;
        }
    } else {
        returnError();
    }

    $arSelectFields = Array(
        "ID", "SOURCE", "DATE_STATUS", "PRICE", "CANCELED", "STATUS_ID", "UF_FO_ORDER_CODE"
    );
    $rsSales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter, false, false, $arSelectFields);
    while ($orderArr = $rsSales->Fetch()) {

        $db_props = CSaleOrderPropsValue::GetOrderProps($orderArr['ID']);

        while ($orderProps = $db_props->Fetch()) {
            switch ($orderProps['CODE']) {
                case 'ACTION_PAY_DATA':
                    $orderArr[$orderProps['CODE']] = $orderProps['VALUE'];
                    break;
            }
        }

        $ap = explode('.', $orderArr['ACTION_PAY_DATA']);
        $orderArr['action_pay_click'] = $ap[0];
        $orderArr['action_pay_source'] = $ap[1];

        if($orderArr['action_pay_click'] != '' && $orderArr['action_pay_source'] != '') {
            $ordersData[] = array(
                'id' => $orderArr['UF_FO_ORDER_CODE'],
                'price' => $orderArr['PRICE'],
                'click' => $orderArr['action_pay_click'],
                'source' => $orderArr['action_pay_source'],
                'status' => getOrderStatus($orderArr),
                'date' => $orderArr['DATE_STATUS']
            );
        }

    }

}

$GLOBALS['APPLICATION']->RestartBuffer();
header('Content-type: text/xml; charset=' . SITE_CHARSET);
$out = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";
$out .= "<items>";
foreach($ordersData as $order){

        $out .= "<item>
                  <id>" . $order['id'] . "</id>
                  <click>" . $order['click'] . "</click>
                  <source>" . $order['source'] . "</source>
                  <price>" . $order['price'] . "</price>
                  <status>" . $order['status'] . "</status>
                  <date>" . $order['date'] . "</date>
               </item>";
}
$out .= "</items>";
echo $out;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');

function getOrderStatus($orderArr)
{
    if ($orderArr['CANCELED'] == 'Y') {
        $status = 3;
    } elseif ($orderArr['STATUS_ID'] == 'F') {
        $status = 1;
    } else {
        $status = 2;
    }
    return $status;
}

function returnError($textError = 'Ошибка'){
    $GLOBALS['APPLICATION']->RestartBuffer();
    header('Content-type: text/xml; charset=' . SITE_CHARSET);
    $out = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";
    $out .= "<error>{$textError}</error>";
    echo $out;
    die;
}
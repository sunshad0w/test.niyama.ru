<?
//
// Выгрузка каталога в xml для курьерских служб
//
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

$bRefreshCache = false;
$bGetNotActive = false;
$PIZZA_SECTION_ID = 314; // ID раздела пиццы
$iCatalogBaseIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
$arSectionsTree = CCustomProject::GetIBlockSectionsTree($iCatalogBaseIBlockId, $bRefreshCache);
//$arProductsList = CNiyamaCatalog::GetProductsDetailsList($bRefreshCache, $bGetNotActive);
$outputOffersList = CNiyamaCatalog::GetOffersList();

$GLOBALS['APPLICATION']->RestartBuffer();
header('Content-type: text/xml; charset='.SITE_CHARSET);
echo '<?xml version="1.0" encoding="'.SITE_CHARSET.'"?>'."\n";
?>
<yml_catalog date="<?=htmlspecialcharsbx(date('Y-m-d H:i'))?>">
    <shop>
        <name>Пицца-Пи</name>
        <url>http://pizzapi.ru/</url>
        <currencies>
            <currency id="RUR" rate="1"/>
        </currencies>
	<categories>
<?
		foreach($arSectionsTree as $arItem) {
			if($arItem['ACTIVE'] != 'Y' || $arItem['GLOBAL_ACTIVE'] != 'Y') {
				continue;
			}
?>
		<category id="<?=$arItem['ID']?>"><?=$arItem['NAME']?></category>
<?
		}
?>
	</categories>
	<offers>
<?
		foreach($outputOffersList as $arItem) {

            // echo "<pre>";var_dump($arItem['INFO']);echo "</pre>";die;

			if($arItem['PRODUCT']['ACTIVE'] != 'Y' || $arItem['ACTIVE'] != 'Y') {
				continue;
			}
			$sDetailPictureUrl = '';
			$iTmpPictureId = $arItem['PRODUCT']['DETAIL_PICTURE'] ? $arItem['PRODUCT']['DETAIL_PICTURE'] : $arItem['PRODUCT']['PREVIEW_PICTURE'];
			if($iTmpPictureId) {
				$arFile = CFile::GetFileArray($iTmpPictureId);
				if($arFile['SRC']) {
					$sDetailPictureUrl = 'http://'.SITE_SERVER_NAME.$arFile['SRC'];
				}
			}
?>
		<offer id="<?=$arItem['ID']?>" available="true">
			<categoryId><?=intval($arItem['PRODUCT']['IBLOCK_SECTION_ID'])?></categoryId>
			<name><?=$arItem['NAME']?></name>
            <?if($arItem['PRODUCT']['IBLOCK_SECTION_ID'] == $PIZZA_SECTION_ID &&  $arItem['CATALOG_LENGTH']):?>
                <size><?= $arItem['CATALOG_LENGTH']?></size>
            <?else:?>
                <size />
            <?endif;?>
<?
			if($arItem['PRODUCT']['PREVIEW_TEXT']) {
?>
			<description><?=($arItem['PRODUCT']['PREVIEW_TEXT_TYPE'] == 'html' ? htmlspecialcharsbx($arItem['PRODUCT']['PREVIEW_TEXT']) : $arItem['PRODUCT']['PREVIEW_TEXT'])?></description>
<?
			} else {
?>
			<description />
<?			
			}

			if($sDetailPictureUrl) {
?>
			<picture><?=$sDetailPictureUrl?></picture>
<?
			} else {
?>
			<picture />
<?
			}

			if(isset($arItem['CATALOG_PRICE_2'])) {
?>
			<price><?=doubleval($arItem['CATALOG_PRICE_2'])?></price>
<?
			} else {
?>
			<price />
<?
			}
?>
            <url>http://pizzapi.ru<?=$arItem['PRODUCT']['DETAIL_PAGE_URL']?></url>
		</offer>
<?
		}
?>
	</offers>
    </shop>
</yml_catalog><?

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

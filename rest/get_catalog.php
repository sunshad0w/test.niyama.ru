<?
//
// Выгрузка каталога в xml для курьерских служб
//
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

$bRefreshCache = true;
$bGetNotActive = false;
$PIZZA_SECTION_ID = 314; // ID раздела пиццы
$iCatalogBaseIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
$arSectionsTree = CCustomProject::GetIBlockSectionsTree($iCatalogBaseIBlockId, $bRefreshCache);

$outputOffersList = CNiyamaCatalog::GetOffersList();

$GLOBALS['APPLICATION']->RestartBuffer();

header('Content-type: text/xml; charset='.SITE_CHARSET);
echo '<?xml version="1.0" encoding="'.SITE_CHARSET.'"?>'."\n";
?>
<catalog build_date="<?=htmlspecialcharsbx(date('r'))?>">
	<categories>
<?
		foreach($arSectionsTree as $arItem) {
			if($arItem['ACTIVE'] != 'Y' || $arItem['GLOBAL_ACTIVE'] != 'Y') {
				continue;
			}
?>
		<category id="<?=$arItem['ID']?>" code="<?=$arItem['CODE']?>">
			<name><?=$arItem['NAME']?></name>
		</category>
<?
		}
?>
	</categories>
	<products>
<?
		foreach($outputOffersList as $arItem) {
			if($arItem['PRODUCT']['ACTIVE'] != 'Y' || $arItem['ACTIVE'] != 'Y') {
				continue;
			}
			$sDetailPictureUrl = '';
			$iTmpPictureId = $arItem['PRODUCT']['DETAIL_PICTURE'] ? $arItem['PRODUCT']['DETAIL_PICTURE'] : $arItem['PRODUCT']['PREVIEW_PICTURE'];
			if($iTmpPictureId) {
				$arFile = CFile::GetFileArray($iTmpPictureId);
				if($arFile['SRC']) {
					$sDetailPictureUrl = 'http://'.SITE_SERVER_NAME.$arFile['SRC'];
				}
			}
?>
		<product id="<?=$arItem['ID']?>" external_id="<?=$arItem['XML_ID']?>" code="<?=$arItem['CODE']?>">
			<category_id><?=intval($arItem['PRODUCT']['IBLOCK_SECTION_ID'])?></category_id>
			<name><?=$arItem['NAME']?></name>

            <?if($arItem['PRODUCT']['IBLOCK_SECTION_ID'] == $PIZZA_SECTION_ID &&  $arItem['CATALOG_LENGTH']):?>
                <size><?= $arItem['CATALOG_LENGTH']?></size>
            <?else:?>
                <size />
            <?endif;?>
<?
			if($arItem['PRODUCT']['PREVIEW_TEXT']) {
?>
			<descritption><?=($arItem['PRODUCT']['PREVIEW_TEXT_TYPE'] == 'html' ? htmlspecialcharsbx($arItem['PRODUCT']['PREVIEW_TEXT']) : $arItem['PRODUCT']['PREVIEW_TEXT'])?></descritption>
<?
			} else {
?>
			<descritption />
<?			
			}

			if($sDetailPictureUrl) {
?>
			<picture><?=$sDetailPictureUrl?></picture>
<?
			} else {
?>
			<picture />
<?
			}

			if(isset($arItem['CATALOG_PRICE_2'])) {
?>
			<price currency="RUB"><?=doubleval($arItem['CATALOG_PRICE_2'])?></price>
<?
			} else {
?>
			<price />
<?
			}
?>
		</product>
<?
		}
?>
	</products>
</catalog><?

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

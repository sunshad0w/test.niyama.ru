<?


require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
//include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

?>
<div class="main-content">
    <div class="main-content__wrapper">
        <img src="/local/templates/pizzapi/img/travka.png" class="bg-travka">
        <img src="/local/templates/pizzapi/img/pomidor.png" class="bg-pomidor">
        <img src="/local/templates/pizzapi/img/grib.png" class="bg-grib">
        <div class="container">
            <h1 class="page__title">Ошибка 404 - страница не найдена</h1>
            <div class="page__block">
                <p>Возможно, эта страница была удалена, переименована, или она временно недоступна.</p>
                <p>Попробуйте следующее:</p>
                <ul>
                    <li>проверьте правильность адреса страницы в строке адреса;</li>
                    <li>перейдите на <a href="<?=SITE_DIR?>">главную страницу</a>, затем найдите там ссылки на нужные вам данные;</li>
                    <li>вернитесь <a href="javascript:history.back();">назад</a>, чтобы использовать другую ссылку.</li>
                </ul>
            </div>
        </div>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");

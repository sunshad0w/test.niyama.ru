<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("NIYAMA_PAGE_CONTENT_CLASS", "_wide");

$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.product-detail',
    array(
        'CODE' => isset($_REQUEST['CODE']) ? trim($_REQUEST['CODE']) : false
    ),
    null,
    array(
        'HIDE_ICONS' => 'Y'
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
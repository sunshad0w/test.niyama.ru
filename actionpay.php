<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Content-type: application/XML');

if (isset($_POST['xml']) && isset($_POST['pass']) && !empty($_POST['pass'])) {

    $parser = xml_parser_create();
    xml_parse_into_struct($parser, $_POST['xml'], $vals, $index);
    xml_parser_free($parser);
    $peremenn = array();
    foreach ($vals as $v) {
        if ($v['value'] == " "){
            continue;
        }

        if (!$v['value']) continue;
        $xmlItem = $v['value'];
        array_push($peremenn, (int)$xmlItem);

    }

    $prameters = array(
        'select' => array('*'),
        'filter' => array(
            '=ORDER_ID' => $peremenn
        ),
    );



    $items = ActionPayTable::getList($prameters);
    $readItems = $items->FetchAll();


    if (empty($readItems)) {
        $dom = new DOMDocument("1.0", "utf-8");
        $root = $dom->createElement("items", " ");
        $dom->appendChild($root);
        echo $dom->saveXML();


    } else {
        generate_xml($readItems);
    }



}

else {
    if (isset($_POST['date']) && isset($_POST['pass']) && !empty($_POST['pass'])) {
        $date = new DateTime($_POST['date']);
        $date->modify('+1 day -1 sec');
        $d = date_format($date, 'd.m.Y H:i:s');

        $prameters = array(
            'select' => array('*'),
            'filter' => array(
                ">=DATE" => $_POST['date'],
                "<=DATE" => $d,
            ),
        );

        $items = ActionPayTable::getList($prameters);
        $readItems = $items->FetchAll();


        if (empty($readItems)) {
            $dom = new DOMDocument("1.0", "utf-8");
            $root = $dom->createElement("items", " ");
            $dom->appendChild($root);
            echo $dom->saveXML();

        } else {
            generate_xml($readItems);
        }

    } else {
        $dom = new DOMDocument("1.0", "utf-8");
        $root = $dom->createElement("items", "Error");
        $dom->appendChild($root);
        echo $dom->saveXML();
    }

}



function generate_xml($readItems)
{
    $dom = new DOMDocument("1.0", "utf-8");
    $root = $dom->createElement("items");
    $dom->appendChild($root);
    foreach ($readItems as $i) {
        $itm = $dom->createElement("item");
        $order_id = $dom->createElement("ID", $i['ORDER_ID']);
        foreach ($i as $key => $value) {
            if ($key == 'ID') {
                continue;
            }
            if ($key == 'ORDER_ID') {
                continue;
            }

            $itemField = $dom->createElement($key, $value);
            $itm->appendChild($itemField);

        }
        $itm->appendChild($order_id);
        $root->appendChild($itm);
    }

//$dom->save("items.xml");
    echo $dom->saveXML();
}
?>
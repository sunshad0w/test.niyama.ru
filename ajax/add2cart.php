<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iProductID = (isset($_REQUEST['pid'])) ? intval($_REQUEST['pid']) : false;
$iQuantity = (isset($_REQUEST['quantity'])) ? intval($_REQUEST['quantity']) : 1;
$bFromOrderPromo = isset($_REQUEST['fromOrderPromo']) && $_REQUEST['fromOrderPromo'] == 'true';

$arJson = array();
if ($iProductID) {

	$arCartItems = CNiyamaCart::getCartList();

	if(!$bFromOrderPromo) {
		$iGuestID = (isset($_REQUEST['gid'])) ? intval($_REQUEST['gid']) : null;
		if (is_null($iGuestID)) {
			$iGuestIDTmp = CNiyamaCart::getGuestID();
			$iGuestID = ($iGuestIDTmp) ? $iGuestIDTmp : null;
		}
	} else {
		if(isset($_REQUEST['guestid'])) {
			// перетаскивают на стол
			$iGuestID = intval($_REQUEST['guestid']);
			if($iGuestID != 1) {
				$iGuestID = isset($arCartItems['guest'][$iGuestID]) ? $iGuestID : 0;
			}
		} else {
			// просто нажали кнопку добавить
			if($arCartItems['PERSONS_CNT'] > 1) {
				// есть столы гостей, добавляем на общий стол
				$iGuestID = 1;
			} else {
				// добавляем на "свой стол"
				$iGuestID = 0;
			}
		}			
	}

    # Проверка на максимальное количество гостей
    if (!is_null($iGuestID)) {
        if (!isset($arCartItems['guest'][$iGuestID])) {
            $iGuestCount = (count($arCartItems['guest']));

            $iOptionsMaxGuest = intval(CNiyamaCustomSettings::getStringValue('max_num_guests','8') - 1);
            if ($iGuestCount > $iOptionsMaxGuest) {
				$arJson = array(
					'ERROR' => 'Количество гостей не должно превышать '.intval(CNiyamaCustomSettings::getStringValue('max_num_guests', '8'))
				);
            }
        }
    }

    # Если добавляем на
	if(!$arJson['ERROR']) {
		if ($iItemId = CNiyamaCart::AddToCart($iProductID, $iQuantity, $iGuestID)) {

			if(!$bFromOrderPromo) {
				CNiyamaCart::setCurrentOrderData(false);

				//$sBackUrl = (isset($_REQUEST['backurl'])) ? $_REQUEST['backurl'] : false;
				//if ($sBackUrl){
				//	LocalRedirect($sBackUrl);
				//}

                if (isset($_REQUEST['auction']) && ($_REQUEST['auction'] == "true")) {
                    $arEventHandlers = GetModuleEvents('main', 'OnAfterAuctionProductsCartAdd', true);
                    foreach($arEventHandlers as $arEvent) {
                        ExecuteModuleEventEx($arEvent, array($iItemId,$iProductID));
                    }
                }

				$iTotalPrice = (isset($arCartItems['TOTAL_PRICE'])) ? $arCartItems['TOTAL_PRICE'] : '0';
				$iSubPrice = CNiyamaCart::getSubPrice($arCartItems);

				$arJson['TOTAL_PRICE'] = $iTotalPrice;
				$arJson['SUB_PRICE'] = $iSubPrice;
				$arJson['ID'] = $iItemId;
			}
		}
	}
}

echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

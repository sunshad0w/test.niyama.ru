<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

# Если false то устанавливается корзина текущего пользователя
$mGuestId = isset($_REQUEST['gid']) ? intval($_REQUEST['gid']) : false;

unset($_SESSION['CART_GUEST_DELETE']);

# Получаем текущую корзину пользователя
$arCartItems = CNiyamaCart::getCartList();

$bIsGuest = isset($arCartItems['guest']) && !empty($arCartItems['guest']) ? true : false;
if($bIsGuest) {
    $bIsNeedleGuest = isset($arCartItems['guest'][$mGuestId]) ? true : false;
    if ($bIsNeedleGuest) {
        if ($arCartItems['guest'][$mGuestId]['ITEMS']) {
            foreach ($arCartItems['guest'][$mGuestId]['ITEMS'] as $prod) {
				// переносим блюда со стола гостя на общий стол
                $res = CCustomProject::updateBasketPropsById($prod['ID'], array('guest' => CNiyamaCart::$iForAllGuestID));
                $_SESSION['CART_GUEST_DELETE'][$mGuestId][$prod['ID']] = $prod;
            }
        }
    }
}

$bIsCommon = isset($arCartItems['common']) && !empty($arCartItems['common']) ? true : false;
if($bIsCommon) {
	// если удаляется общий стол, то переносим все на "свой стол" юзера
    if($mGuestId == CNiyamaCart::$iForAllGuestID) {
        if(isset($arCartItems['common'][0]['ITEMS']) && is_array($arCartItems['common'][0]['ITEMS'])) {
            $_SESSION['CART_COMMON_DELETE'] = array();
            foreach($arCartItems['common'][0]['ITEMS'] as $prod) {
                $res = CCustomProject::updateBasketPropsById($prod['ID'], array('guest' => ''));
                $_SESSION['CART_COMMON_DELETE'][$prod['ID']] = $prod;
            }
        }
    }
}

if ($mGuestId) {
    unset($_SESSION['NIYAMA']['GUESTS'][$mGuestId]);
    if(!isset($_SESSION['CART_GUEST_DELETE'][$mGuestId])) {
        $_SESSION['CART_GUEST_DELETE'][$mGuestId] = $mGuestId;
    }
}

// это вместо эпилога
CProjectUtils::AjaxEpilog();

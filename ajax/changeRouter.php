<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

if (!isset($_REQUEST['mode'])){
    CCustomProject::SetMode(0);
} else {
    CCustomProject::SetMode($_REQUEST['mode']);
}

LocalRedirect("/");


// это вместо эпилога
CProjectUtils::AjaxEpilog();
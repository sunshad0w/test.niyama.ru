<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$APPLICATION->SetTitle("");

# Если установлен переключатель корзины
$arCurrentOrderData = CNiyamaCart::getCurrentOrderData();

if ($arCurrentOrderData && !empty($arCurrentOrderData['ID'])) {

    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-cart-order',
        array(
            'AJAX_REQUEST' => 'N',
            'ORDER_ID' => $arCurrentOrderData['ID']
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );

} else {

    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-cart',
        array(
            'AJAX_REQUEST' => 'Y'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );

}

// это вместо эпилога
CProjectUtils::AjaxEpilog();
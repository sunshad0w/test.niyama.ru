<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("sale");

CNiyamaCart::setCurrentOrderData(false);
$iOrderId = (isset($_REQUEST['order_id'])) ? intval($_REQUEST['order_id']) : false;

$bCanAccess = false;

if ($iOrderId && $iOrderId != "cart") {

    if ($iOrderId){
        $arOrder = CNiyamaOrders::GetOrderById($iOrderId, true);
        if ($arOrder){
            if ($arOrder['CAN_ACCESS'] == "Y"){
                $bCanAccess = true;
            }
        }
    }

    if ($bCanAccess) {

        CNiyamaCart::setCurrentOrderData(array(
            "ID"   => $arOrder['ORDER']['ID'],
            "DATE" => $arOrder['ORDER']['PRICE'],
            "NAME" => ADV_textDate($arOrder['ORDER']['DATE_INSERT'], false)
        ));

        echo "true";

    }

}


// это вместо эпилога
CProjectUtils::AjaxEpilog();
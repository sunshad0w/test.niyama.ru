<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$arJson = array();
$sResultHtml = '';
if ($GLOBALS['USER']->IsAuthorized()) {
	$mCouponId = isset($_REQUEST['pid']) ? $_REQUEST['pid'] : false;
	$mBonusWithdrawVal = isset($_REQUEST['coupon__count']) ? trim($_REQUEST['coupon__count']) : false;
	$bWithdrawAction = $mBonusWithdrawVal !== false;

	if ($mCouponId) {
		$arCartItems = CNiyamaCart::getCartList();

		$iGuestIDTmp = CNiyamaCart::getGuestID();
		$iGuestID = $iGuestIDTmp ? $iGuestIDTmp : 0;
		$iGuestIDAdd = $iGuestIDTmp ? $iGuestIDTmp : null;
		$arTableTypes = 'current';
		if($iGuestID > 0) {
			if($iGuestID == CNiyamaCart::$iForAllGuestID) {
				$arTableTypes = 'common';
				$iGuestID = 0;
			} else {
				$arTableTypes = 'guest';
			}
		}
		$old_price = 0;
		if (!empty($arCartItems[$arTableTypes][$iGuestID]['OLD_PRICE'])) {
			$old_price = $arCartItems[$arTableTypes][$iGuestID]['OLD_PRICE'];
		}
		$bonus_rub_used = 0;
		if (!empty($arCartItems[$arTableTypes][$iGuestID]['DISCOUNT_RUB']['all'])) {
			$bonus_rub_used = $arCartItems[$arTableTypes][$iGuestID]['DISCOUNT_RUB']['all'];
		}
		$DISCOUNT_PERCENT = 0;
		if (!empty($arCartItems['DISCOUNT_PERCENT'])) {
			$DISCOUNT_PERCENT = $arCartItems['DISCOUNT_PERCENT'];
		}
		$arResult = CNiyamaCoupons::GetCouponById($mCouponId);
		$discount_count = 0;
		$koefficient_niam_by_rub = intval(CNiyamaCustomSettings::getStringValue('koefficient_niam_by_rub', 100));
		if ($arResult['MONEY_TYPE'] == 'NIYAM') {
			$discount_count = $koefficient_niam_by_rub * CNiyamaCoupons::GetMaxNiamDiscount($old_price, $DISCOUNT_PERCENT, $bonus_rub_used);
		} elseif($arResult['MONEY_TYPE'] == 'MONEY') {
			$discount_count = CNiyamaCoupons::GetMaxMoneyDiscount($old_price, $DISCOUNT_PERCENT, $bonus_rub_used);
		}
		$arResult['MAX_USED'] = $discount_count;

		if($bWithdrawAction) {
			$arResult['BONUS_WITHDRAW_VAL'] = $mBonusWithdrawVal;
			$arResult['coupon__count'] = 0;
			$arResult['error'] = true;
			$arResult['error_msg'] = '';
			if(is_numeric($mBonusWithdrawVal) && intval($mBonusWithdrawVal) > 0) {
				$arResult['coupon__count'] = intval($mBonusWithdrawVal);
				if($arResult['coupon__count'] > $arResult['MONEY']) {
					$arResult['error_msg'] = 'Указанный бонус превышает номинал купона';
				} elseif($arResult['coupon__count'] > $arResult['MAX_USED']) {
					// текст ошибки в шаблоне будет переорпеделен
					$arResult['error_msg'] = 'Недостаточная сумма заказа для списания указанного бонуса';
				} elseif($arResult['MONEY_TYPE'] == 'NIYAM' && $arResult['coupon__count'] % $koefficient_niam_by_rub != 0) {
					$arResult['error_msg'] = 'Значение должно быть кратно '.$koefficient_niam_by_rub;
				} else {
					$arResult['error'] = false;
					if (CNiyamaCoupons::GetTypeForAll($mCouponId)) {
						$iGuestIDAdd = CNiyamaCart::$iForAllGuestID;
					}

					if($arResult['ID'] = CNiyamaCart::AddToCart($mCouponId, $arResult['coupon__count'], $iGuestIDAdd, CNiyamaCart::$arCartItemType[1])) {
						//CNiyamaCart::SetValCouponUsed($arResult['ID'], $arResult['coupon__count']);
					}
				}
			} else {
				$arResult['error_msg'] = 'Значение для списания задано неверно';
			}
		}

		ob_start();
		$arTmpResult = $GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.coupon-popup',
			array(
				'arResult' => $arResult,
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		$sResultHtml = ob_get_clean();

		if($bWithdrawAction) {
			$arJson = array(
				'success' => $arResult['error'] ? false : true,
				'coupon_id' => $arResult['ID'],
				'html' => $sResultHtml,
				'pid' => $mCouponId,
				'coupon__title' => $arTmpResult['sCouponTitleBlockHtml']
			);
		}
	}
}

$GLOBALS['APPLICATION']->RestartBuffer();
if($arJson) {
	header('Content-Type: application/json');
	echo json_encode($arJson);
} else {
	echo $sResultHtml;
}

// это вместо эпилога
CProjectUtils::AjaxEpilog();

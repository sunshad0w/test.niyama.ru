<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');


/* Меню с переключением режимов  */
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.general-modemenu',
    array(
        "AJAX_REQUEST" => "Y"
    ),
    null,
    array(
        'HIDE_ICONS' => 'Y'
    )
);


// это вместо эпилога
CProjectUtils::AjaxEpilog();
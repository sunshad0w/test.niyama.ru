<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
error_reporting(E_ALL & !E_NOTICE & !E_DEPRECATED);
ini_set('display_errors', 'on');

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

/* Addition of the goods in a basket at addition in a basket */
if(isset($_POST['ajaxaddid']) && $_POST["ajaxaddid"] && $_POST["ajaxaction"] == 'add'){
    Add2BasketByProductID($_POST["ajaxaddid"], 1, array());
}
/* Goods removal at pressing on to remove in a small basket */
if(isset($_POST['ajaxdeleteid']) && $_POST["ajaxdeleteid"] && $_POST["ajaxaction"] == 'delete'){
    CSaleBasket::Delete($_POST["ajaxdeleteid"]);
}
/* Changes of quantity of the goods after receipt of inquiry from a small basket */
if(isset($_POST['ajaxbasketcountid']) && $_POST["ajaxbasketcountid"] && $_POST["ajaxbasketcount"] && $_POST["ajaxaction"] == 'update'){
    $arFields = array(
       "QUANTITY" => $_POST["ajaxbasketcount"]
);
CSaleBasket::Update($_POST["ajaxbasketcountid"], $arFields);
}

?>

<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "orderbasket", Array(
	"ACTION_VARIABLE" => "action",	// Название переменной действия
		"AUTO_CALCULATION" => "Y",
		"TEMPLATE_THEME" => "blue",
		"COLUMNS_LIST" => array(	// Выводимые колонки
			0 => "NAME",
			1 => "QUANTITY",
			2 => "PRICE",
			3 => "DELETE",

		),
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",	// Рассчитывать скидку для каждой позиции (на все количество товара)
		"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
		"OFFERS_PROPS" => array(),// Свойства, влияющие на пересчет корзины
		"PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
		"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
		"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
	),
	false
);?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
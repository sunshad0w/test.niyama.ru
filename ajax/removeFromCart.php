<?php

define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

# Записываем в сесию данные о товаре.
//$_SESSION[CNiyamaCart::$sSessionName] = CNiyamaCart::getItemData($_REQUEST['ID']);
$bFromOrder = isset($_REQUEST['fromOrder']) && $_REQUEST['fromOrder'] == 'true';

if (is_array($_REQUEST['ID'])) {
    foreach($_REQUEST['ID'] as $id) {
        $bSuccess = CNiyamaCart::RemoveFromCart($id);
    }
}
else {
    $bSuccess = CNiyamaCart::RemoveFromCart($_REQUEST['ID']);
}

$arTotalPrice = array();
if ($bSuccess) {
	if(!$bFromOrder) {
		$arCartItems = CNiyamaCart::getCartList();
		$iTotalPrice = (isset($arCartItems['TOTAL_PRICE'])) ? $arCartItems['TOTAL_PRICE'] : '0';
		$iSubPrice = CNiyamaCart::getSubPrice($arCartItems);

		$arTotalPrice['TOTAL_PRICE'] = $iTotalPrice;
		$arTotalPrice['SUB_PRICE'] = $iSubPrice;
	}
} else {
   // $_SESSION[CNiyamaCart::$sSessionName] = false;
}
echo json_encode($arTotalPrice);

// это вместо эпилога
CProjectUtils::AjaxEpilog();
<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("sale");

if (isset($_SESSION["CART_GUEST_DELETE"])){
    $arData = $_SESSION["CART_GUEST_DELETE"];
    if (is_array($arData)){

        $iAccess = false;
        foreach ($_SESSION["CART_GUEST_DELETE"] as $val){
            if(is_array($val)) {
                foreach ($val as $al){
                    if(CCustomProject::updateBasketPropsById($al['ID'], $al['PROPS'])) {
                        $iAccess = true;
                    }
                }
            } else {
                $_SESSION['NIYAMA']['GUESTS'][$val] = $val;
            }
        }
    }
}

if(isset($_SESSION["CART_COMMON_DELETE"]) && is_array($_SESSION["CART_COMMON_DELETE"])) {

    foreach($_SESSION["CART_COMMON_DELETE"] as $prod) {
        if(CCustomProject::updateBasketPropsById($prod['ID'], $prod['PROPS'])) {
            $iAccess = true;
        }
    }

}

if (isset($iAccess) && $iAccess === true){
    $arCartItems = CNiyamaCart::getCartList();
    $iTotalPrice = (isset($arCartItems['TOTAL_PRICE'])) ? $arCartItems['TOTAL_PRICE'] : '0';
    $iSubPrice   = CNiyamaCart::getSubPrice($arCartItems);

    $arTotalPrice['TOTAL_PRICE'] = $iTotalPrice;
    $arTotalPrice['SUB_PRICE'] = $iSubPrice ;
    $arTotalPrice['ID'] = $iReturn;
    echo json_encode($arTotalPrice);
}


// это вместо эпилога
CProjectUtils::AjaxEpilog();
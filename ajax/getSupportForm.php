<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if($GLOBALS['USER']->IsAuthorized()) {
	# Форма отправки уведомления в тех.поддержку
	$APPLICATION->IncludeComponent(
		'bitrix:form.result.new',
		'niyama.1.0.support',
		array(
			'WEB_FORM_ID' => CCustomProject::GetFormIdByCode('SUPPORT'),
			'IGNORE_CUSTOM_TEMPLATE' => 'N',
			'USE_EXTENDED_ERRORS' => 'Y',
			'SEF_MODE' => 'N',
			'SEF_FOLDER' => '/',
			'CACHE_TYPE' => 'A',
			'CACHE_TIME' => '43200',
			'LIST_URL' => '',
			'EDIT_URL' => '',
			'SUCCESS_URL' => '',
			'LIST_URL' => '',
			'CHAIN_ITEM_TEXT' => '',
			'CHAIN_ITEM_LINK' => '',
			'VARIABLE_ALIASES' => array(
				'WEB_FORM_ID' => 'WEB_FORM_ID',
				'RESULT_ID' => 'RESULT_ID',
			)
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
} else {
	echo '<div style="padding: 20px;"><p>Пожалуйста, авторизуйтесь</p></div>';
}

// это вместо эпилога
CProjectUtils::AjaxEpilog();

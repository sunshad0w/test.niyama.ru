<?php

define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

$fid = 0;

if (!empty($_FILES["file"])){
	$arIMAGE['name'] = $_FILES["file"]['name'];
	$arIMAGE['type'] = $_FILES["file"]['type'];
	$arIMAGE['tmp_name'] = $_FILES["file"]['tmp_name'];
	$arIMAGE['error'] = $_FILES["file"]['error'];
	$arIMAGE['size'] = $_FILES["file"]['size'];
	$arIMAGE["MODULE_ID"] = "main";
}

if (!empty($arIMAGE["name"])&&($arIMAGE['size']>0)) {
    $fid = CFile::SaveFile($arIMAGE, "main");
}elseif (!empty($_REQUEST["file_id"])){
	$fid=(int)$_REQUEST["file_id"];		
}
$return=array( 
	"id"  => false,
	"src" => '',
	"err" => '',
);


if (intval($fid) > 0) {
	$arNewFile = CFile::GetFileArray($fid);	
	$urlPathNew = parse_url($arNewFile['SRC'], PHP_URL_PATH);
	$newpath = $_SERVER['DOCUMENT_ROOT'] . $urlPathNew;
	
	$arFile = CFile::ResizeImageGet($fid, array('width'=>390, 'height'=>320), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	//PR($arFile);
	$urlPath = parse_url($arFile['src'], PHP_URL_PATH);
	$path = $_SERVER['DOCUMENT_ROOT'] . $urlPath;
	
	
	$new_width=100;
	$new_height=100;
	$x1=0;
	$x2=0;
	$y1=false;
	$y2=false;
	if (!empty($_REQUEST["new_width"])){
		$new_width=$_REQUEST["new_width"];
	}
	if (!empty($_REQUEST["new_height"])){
		$new_height=$_REQUEST["new_height"];
	}
	if (!empty($_REQUEST["x1"])){
		$x1=$_REQUEST["x1"];
	}
	if (!empty($_REQUEST["x2"])){
		$x2=$_REQUEST["x2"];
	}
	if (!empty($_REQUEST["y1"])){
		$y1=$_REQUEST["y1"];
	}
	if (!empty($_REQUEST["y2"])){
		$y2=$_REQUEST["y2"];
	}
	$res=CropAndResizeFile($path,$newpath,$new_width,$new_height,$x1,$y1,$x2,$y2);
	
	if ($res['res']){
		$return["id"] = $fid;
		$return["src"] = $arNewFile['SRC'];		
	}else{
		$return["err"] = $res['err'];
	}
    
}

echo json_encode($return);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

function imagecreatefromfile($imagepath=false) { 	
    if(!$imagepath || !is_readable($imagepath)){ 
		return false;
	}else{ 
    	return @imagecreatefromstring(file_get_contents($imagepath)); 
	}
} 
function CropAndResizeFile($filename,$newfilename, $newwidth=100, $newheight=100, $x1=0,$y1=0,$x2=false,$y2=false){		
	list($width, $height) = getimagesize($filename);
	if (($x2===false)||($x2<=0)){
		$x2=$width;
	}
	if (($y2===false)||($y2<=0)){
		$y2=$height;
	}
	
	//пропорциональность	
	$selWidth=$x2-$x1;
	$selHeight=$y2-$y1;
	$new_width=$newwidth;
	$new_height=$newheight;
	if ($selWidth < $selHeight){
    	$new_width=round(($selWidth/$selHeight)*$newheight);
	} else{
    	$new_height=round(($selHeight/$selWidth)*$newwidth);
	}
	$newwidth=($new_width > $newwidth)?$newwidth:$new_width;
    $newheight=($new_height > $newheight)?$newheight:$new_height;
	
	$return=array(
			'res'=>false,
			'err'=>'',
	);	
	
	// загрузка
	$thumb = imagecreatetruecolor($newwidth, $newheight);
	if ($thumb===false){
		$return['err']='Ошибка создания изображения';		
		return $return;
	}
	
	$source = imagecreatefromfile($filename);	
	if ($source===false){
		$return['err']='Ошибка открытия изображения';		
		return $return;
	}
		
	/*PR($x1);
	PR($y1);
	PR($x2);
	PR($y2);
	PR($newwidth);
	PR($newheight);
	PR($x2-$x1);
	PR($y2-$y1);*/
	// изменение размера и обрезка
	$res=imagecopyresized($thumb, $source, 0, 0, $x1, $y1, $newwidth, $newheight, $x2-$x1, $y2-$y1);
	if ($res===false){
		$return['err']='Ошибка кадрирорования изображения';		
		return $return;
	}
	
	// запись в файл
	$res=imagejpeg($thumb,$newfilename);
	if ($res===false){
		$return['err']='Ошибка сохранения изображения';		
		return $return;
	}
	
	imagedestroy($thumb);
	if (empty($return['err'])){
		$return['res']=true;
	}
	return $return;
}
<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

/*
if(isset($_REQUEST['?page']) && !isset($_REQUEST['page'])) {
	$_REQUEST['page'] = $_REQUEST['?page'];
}
*/

$GLOBALS['APPLICATION']->RestartBuffer();
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.general-products',
    array(
        'AJAX_REQUEST' => 'Y',
        'NOFILTER' => isset($_REQUEST['nofilter']) && $_REQUEST['nofilter'] == 'yes' ? 'Y' : 'N',
    ),
    null,
    array(
        'HIDE_ICONS' => 'Y'
    )
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$_REQUEST['CUSTOMER_PAYMENT'] = '';

$GLOBALS['APPLICATION']->IncludeComponent(
	'niyama:order.make',
	'niyama.1.0',
	array(
		'NO_MAKE_ORDER' => 'Y',
		'STEP_ORDER_INFO_SIDE_ONLY' => 'Y' // только правую колнку третьего шага запрашиваем
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

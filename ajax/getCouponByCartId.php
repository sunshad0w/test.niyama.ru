<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iCouponID = (isset($_REQUEST['pid']))? ($_REQUEST['pid']) : false;


if ($iCouponID) {

 	global $APPLICATION;
	$APPLICATION->IncludeComponent(
		"adv:system.empty",
		"niyama.1.0.coupon",
		Array(
			"CUPONS_IDS" => $iCouponID,
			"SHOW_USED" => 'Y',
			"SHOW_IN_CART" => 'Y',
			"ID_IN_CART"	=> $arBasketItem['ID']
		)
	);
	
}


// это вместо эпилога
CProjectUtils::AjaxEpilog();
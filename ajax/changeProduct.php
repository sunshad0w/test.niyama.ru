<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iNewProductId = (isset($_REQUEST['pid'])) ? intval($_REQUEST['pid']) : 0;
$iBasketItemId = (isset($_REQUEST['old'])) ? intval($_REQUEST['old']) : 0;

if (($iNewProductId>0)&&($iBasketItemId>0)) {
	CNiyamaCart::ChangeProduct($iBasketItemId, $iNewProductId);
	 if (isset($_REQUEST['auction']) && ($_REQUEST['auction'] == "true")) {
		$arEventHandlers = GetModuleEvents('main', 'OnAfterAuctionProductsCartAdd', true);
		foreach($arEventHandlers as $arEvent) {
			ExecuteModuleEventEx($arEvent, array($iItemId,$iProductID));
		}
	}
}

$arCartItems = CNiyamaCart::getCartList();
$arTotalPrice['TOTAL_PRICE'] = isset($arCartItems['TOTAL_PRICE']) ? $arCartItems['TOTAL_PRICE'] : 0;
$arTotalPrice['SUB_PRICE'] = CNiyamaCart::getSubPrice($arCartItems);


echo json_encode($arTotalPrice);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

?>
<?php /*
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

$iProductID = (isset($_REQUEST['pid']))?intval($_REQUEST['pid']):false;
$iOld = (isset($_REQUEST['old']))?intval($_REQUEST['old']):false;

$iQuantity = 1;
$arJson = array();

if ($iProductID && $iOld){

    $arCartItems = CNiyamaCart::getCartList();
    $iGuestID = (isset($_REQUEST['gid'])) ? intval($_REQUEST['gid']) : null;
    if (is_null($iGuestID)) {
        $iGuestIDTmp = CNiyamaCart::getGuestID();
        $iGuestID = ($iGuestIDTmp) ? $iGuestIDTmp : null;
    }
    if (!is_null($iGuestID)){

        if (!isset($arCartItems['guest'][$iGuestID])) {
            $iGuestCount = (count($arCartItems['guest']));

            $iOptionsMaxGuest = intval(CNiyamaCustomSettings::getStringValue('max_num_guests','8') - 1);
            if ($iGuestCount > $iOptionsMaxGuest) {
                $arJson = array(
                    'ERROR' => 'Количество гостей не должно превышать '.intval(CNiyamaCustomSettings::getStringValue('max_num_guests', '8'))
                );
            }
        }

        if(!$arJson['ERROR']) {
            if ($iItemId = CNiyamaCart::AddToCart($iProductID, $iQuantity, $iGuestID)) {

                CNiyamaCart::setCurrentOrderData(false);

                //$sBackUrl = (isset($_REQUEST['backurl'])) ? $_REQUEST['backurl'] : false;
                //if ($sBackUrl){
                //	LocalRedirect($sBackUrl);
                //}

                if (isset($_REQUEST['auction']) && ($_REQUEST['auction'] == "true")) {
                    $arEventHandlers = GetModuleEvents('main', 'OnAfterAuctionProductsCartAdd', true);
                    foreach($arEventHandlers as $arEvent) {
                        ExecuteModuleEventEx($arEvent, array($iItemId,$iProductID));
                    }
                }


                $bSuccess = CNiyamaCart::RemoveFromCart($iOld);

                $arTotalPrice = array();
                if ($bSuccess) {
                    if(!$bFromOrder) {
                        $arCartItems = CNiyamaCart::getCartList();
                        $iTotalPrice = (isset($arCartItems['TOTAL_PRICE'])) ? $arCartItems['TOTAL_PRICE'] : '0';
                        $iSubPrice = CNiyamaCart::getSubPrice($arCartItems);

                        $arJson['TOTAL_PRICE'] = $iTotalPrice;
                        $arJson['SUB_PRICE'] = $iSubPrice;
                    }
                }

                $arJson['ID'] = $iItemId;

            }
        }

    }
}

echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();
*/
?>

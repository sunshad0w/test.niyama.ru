<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.product-detail',
    array(
        'AJAX_REQUEST' => 'Y',
        'CODE' => (isset($_REQUEST['CODE']) ? $_REQUEST['CODE'] : false),
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

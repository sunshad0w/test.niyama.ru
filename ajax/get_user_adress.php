<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');
$arJson = array();
$data=array(
	'city'=>1,
	'city_dop'=>'',
	'metro'=>'',
	'mkad'=>false,
	'street'=>'',
	'house'=>'',
	'apart'=>'',
	'porch'=>'',
	'intercom'=>'',
	'floor'=>'',
	
	
);
$iAddress=(int)$_REQUEST['address_id'];
if ($iAddress>0){
	if (($GLOBALS['USER']->IsAuthorized())&&($user_id=$GLOBALS['USER']->GetID()>0)){
		$arAdress = CUsersData::GetAddressByID($iAddress);
		//$arSubWays = CNiyamaIBlockSubway::GetAllStationsList();
		if ($arAdress['PROPERTY_USER_ID_VALUE']==$user_id){
			$data['city']=$arAdress['PROPERTY_REGION_VALUE'];
			$data['city_dop']=$arAdress['PROPERTY_CITY_DOP_VALUE'];
			$data['metro']=$arAdress['PROPERTY_SUBWAY_VALUE'];
			$data['mkad']=$arAdress['PROPERTY_WITHIN_MKAD_VALUE']==1?true:false;
			$data['street']=$arAdress['PROPERTY_ADDRESS_VALUE'];
			$data['house']=$arAdress['PROPERTY_HOUSE_VALUE'];
			$data['apart']=$arAdress['PROPERTY_HOME_VALUE'];
			$data['porch']=$arAdress['PROPERTY_PORCH_VALUE'];
			$data['intercom']=$arAdress['PROPERTY_INTERCOM_VALUE'];
			$data['floor']=$arAdress['PROPERTY_FLOOR_VALUE'];			
		}
		//PR($arSubWays);
	}
}
$arJson['data']=$data;
echo json_encode($arJson);
// это вместо эпилога
CProjectUtils::AjaxEpilog();?>
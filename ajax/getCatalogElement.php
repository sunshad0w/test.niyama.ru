<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?

if (isset($_POST['id']))
{
    $id = null;
    $code = null;
    if(is_numeric($_POST['id'])){
        $id = (int) $_POST['id'];
    }else {
        $code = $_POST['id'];
    }

//if ($productID > 0  && Main\Loader::includeModule('catalog') && Main\Loader::includeModule('sale'));



$APPLICATION->IncludeComponent(
    "bitrix:catalog.element",
    "$template",
    Array(
    
        "DISPLAY_NAME" => "Y",
        "DETAIL_PICTURE_MODE" => "IMG",
        "ADD_DETAIL_TO_SLIDER" => "Y",
        "ADD_PICT_PROP" => "-",
        "LABEL_PROP" => "-",
        "DISPLAY_PREVIEW_TEXT_MODE" => "E",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_MAX_QUANTITY" => "N",
        "SHOW_CLOSE_POPUP" => "Y",
        "DISPLAY_COMPARE" => "Y",
        "COMPARE_PATH" => "",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_BTN_COMPARE" => "Сравнить",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "USE_VOTE_RATING" => "N",
        "USE_COMMENTS" => "N",
        "BRAND_USE" => "N",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "58",
        "ELEMENT_ID" => $id,//$_REQUEST["ELEMENT_ID"],
        "ELEMENT_CODE" => $code,
        "SECTION_ID" => $_REQUEST["SECTION_ID"],
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => "/personal/basket.php",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "CHECK_SECTION_ID_VARIABLE" => "N",
        "SEF_MODE" => "N",
        "SET_TITLE" => "Y",
        "SET_CANONICAL_URL" => "N",
        "SHOW_DEACTIVATED" => "N",
        "SET_BROWSER_TITLE" => "Y",
        "BROWSER_TITLE" => "-",
        "SET_META_KEYWORDS" => "Y",
        "META_KEYWORDS" => "-",
        "SET_META_DESCRIPTION" => "Y",
        "META_DESCRIPTION" => "-",
        "SET_LAST_MODIFIED" => "Y",
        "USE_MAIN_ELEMENT_SECTION" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "ADD_ELEMENT_CHAIN" => "N",
        "PROPERTY_CODE" => array(),
        "OFFERS_FIELD_CODE" => array('WEIGHT', 'XML_ID'),
        "OFFERS_PROPERTY_CODE" => array(),
        "OFFERS_SORT_FIELD" => "weight",
        "OFFERS_SORT_ORDER" => "desc",
        //"OFFERS_SORT_FIELD2" => "name",
        //"OFFERS_SORT_ORDER2" => "desc",
        "OFFERS_LIMIT" => "0",
        "BACKGROUND_IMAGE" => "-",
        "PRICE_CODE" => array(),
        "USE_PRICE_COUNT" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRICE_VAT_SHOW_VALUE" => "Y",
        "PRODUCT_PROPERTIES" => array(),
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "USE_PRODUCT_QUANTITY" => "Y",
        "LINK_IBLOCK_TYPE" => "",
        "LINK_IBLOCK_ID" => "",
        "LINK_PROPERTY_SID" => "",
        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
        "SET_VIEWED_IN_COMPONENT" => "N",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y",
        "USE_ELEMENT_COUNTER" => "Y",
        "HIDE_NOT_AVAILABLE" => "N",
        "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
        "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
        "GIFTS_MESS_BTN_BUY" => "Выбрать",
        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
        "GIFTS_SHOW_IMAGE" => "Y",
        "GIFTS_SHOW_NAME" => "Y",
        "GIFTS_SHOW_OLD_PRICE" => "Y",
        "USE_GIFTS_DETAIL" => "Y",
        "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
        "OFFERS_CART_PROPERTIES" => array(),
        "ADD_TO_BASKET_ACTION" => array(
            0 => "BUY",
            1 => "ADD",
        ),
        "SHOW_BASIS_PRICE" => "N",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "QUANTITY_FLOAT" => "N"
  ),
			$component
		
		);




} 
else 
{
echo "товар не найден";
}
?>
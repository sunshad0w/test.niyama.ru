<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iCouponID = isset($_REQUEST['pid']) ? $_REQUEST['pid'] : false;

if($iCouponID) {

	$iGuestID = isset($_REQUEST['gid']) ? intval($_REQUEST['gid']) : null;
	if(is_null($iGuestID)) {
		$iGuestIDTmp = CNiyamaCart::GetGuestID();
		$iGuestID = ($iGuestIDTmp) ? $iGuestIDTmp : null;
	}

	// Проверка на максимальное количество гостей
	$arCartItems = CNiyamaCart::GetCartList();
	if(!is_null($iGuestID)) {
		if(!isset($arCartItems['guest'][$iGuestID])) {
			$iGuestCount = count($arCartItems['guest']);

			$iOptionsMaxGuest = intval(CNiyamaCustomSettings::GetStringValue('max_num_guests', '8'));
			if($iGuestCount >= $iOptionsMaxGuest) {
				echo json_encode(array('ERROR' => 'Количество гостей не должно превышать '.$iOptionsMaxGuest));
				die();
			}
		}
	}

	if(CNiyamaCoupons::GetTypeForAll($iCouponID)) {
		$iGuestID = CNiyamaCart::$iForAllGuestID;
	}

	if($iItemId = CNiyamaCart::AddToCart($iCouponID, 1, $iGuestID, CNiyamaCart::$arCartItemType[1])) {

		CNiyamaCart::SetCurrentOrderData(false);

		$iTotalPrice = isset($arCartItems['TOTAL_PRICE']) ? $arCartItems['TOTAL_PRICE'] : '0';
		$iSubPrice = CNiyamaCart::GetSubPrice($arCartItems);

		$arTotalPrice['TOTAL_PRICE'] = $iTotalPrice;
		$arTotalPrice['SUB_PRICE'] = $iSubPrice ;
		$arTotalPrice['ID'] = $iItemId;

		/*
		if (($iCouponID == CNiyamaCoupons::$BONUS_NIYAM) || ($iCouponID == CNiyamaCoupons::$BONUS_RUB)) {
			$arTotalPrice['need_update_coupon'] = true;
		}
		*/

		echo json_encode($arTotalPrice);
	}
}

// это вместо эпилога
CProjectUtils::AjaxEpilog();

<?
//
// Автокомплит улиц
//
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iStreetsLimit = 20;
$iBuildingsLimit = 20;

$sCity = trim($_REQUEST['city']);
$sAddress = trim($_REQUEST['address']);



//$sCity = 'Москва';
//$sAddress = 'Ленинская Слобода';

$arJson = array();
$arJson['buildings'] = array();
if(strlen($sCity) && strlen($sAddress)) {
	$arCityes=CNiyamaOrders::GetDeliveryRegionsByFilter(false,array('CITY_NAME'=>$sCity));

	foreach($arCityes as $city){
		$arSubWays = CNiyamaIBlockSubway::GetStationsListByFilter(false,array('%NAME'=>$sAddress,'CITY_CODE'=>$city['ID']));		
		foreach($arSubWays as $sub){
			$arJson['buildings'][] = array(
					'name' => $sub['NAME'],
					'full_name' => $sub['NAME']
			);
		}
	}	
}


echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$bAddComp = false;
$bAddCountGuest = false;
$iBasketItemId = 0;
$iImpElementId = 0;
if(isset($_REQUEST['ID'])) {
	$sVal = trim($_REQUEST['ID']);
	if(strpos($sVal, 'add-') === 0) {
		// add-<id импортируемого элемента меню>-<0 или id позиции корзины>
		$arTmp = explode('-', $sVal);
		if(count($arTmp) == 3) {
			if(strpos($arTmp[1], CNiyamaCart::$arCartItemType[3]) === 0) { 
				$bAddCountGuest = true;
				$iImpElementId = 1;
				$iBasketItemId = intval($arTmp[2]);
			}else{
				$bAddComp = true;
				$iImpElementId = intval($arTmp[1]);
				$iBasketItemId = intval($arTmp[2]);
			}
		}
	}
	if(!$bAddComp&&!$bAddCountGuest) {
		$iBasketItemId = intval($sVal);
	}
}
$iQuantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : 1;
$bFromOrder = isset($_REQUEST['fromOrder']) && $_REQUEST['fromOrder'] == 'true';
$arTotalPrice = array();
if($bAddComp) {
	// работаем с дополнительными компонентами
	if($iBasketItemId) {
		if($iQuantity > 0) {
			CNiyamaCart::SetQuantity($iBasketItemId, $iQuantity);
		} else {
			CNiyamaCart::RemoveFromCart($iBasketItemId);
		}
	} elseif($iImpElementId > 0) {
		$iBasketItemId = CNiyamaCart::AddToCart($iImpElementId, $iQuantity, false, 'dop_component');
	}
}elseif($bAddCountGuest) {	
	// работаем с дополнительными гостями
	if($iBasketItemId) {
		if($iQuantity > 0) {
			CNiyamaCart::SetQuantity($iBasketItemId, $iQuantity);
		} else {
			CNiyamaCart::RemoveFromCart($iBasketItemId);
		}
	} elseif($iImpElementId > 0) {
		
		$iBasketItemId = CNiyamaCart::AddToCart($iImpElementId, $iQuantity, false, CNiyamaCart::$arCartItemType[3]);
	}
} else {
	// работаем с обычными блюдами
	if($iBasketItemId) {
		CNiyamaCart::SetQuantity($iBasketItemId, $iQuantity);
	}
}

if(!$bFromOrder) {
	$arCartItems = CNiyamaCart::getCartList();
	$arTotalPrice['TOTAL_PRICE'] = isset($arCartItems['TOTAL_PRICE']) ? $arCartItems['TOTAL_PRICE'] : 0;
	$arTotalPrice['SUB_PRICE'] = CNiyamaCart::getSubPrice($arCartItems);
}

echo json_encode($arTotalPrice);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

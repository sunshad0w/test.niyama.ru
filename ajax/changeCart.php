<?php
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

# Если false то устанавливается корзина текущего пользователя
$iGuestID = (isset($_REQUEST['gid'])) ? intval($_REQUEST['gid']) : false;

CNiyamaCart::setGuestID($iGuestID);

// это вместо эпилога
CProjectUtils::AjaxEpilog();
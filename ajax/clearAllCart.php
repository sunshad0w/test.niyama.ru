<?php

define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

CNiyamaCart::clearCart();

$arTotalPrice = CNiyamaCart::getTotalPrice();
echo json_encode($arTotalPrice);

// это вместо эпилога
CProjectUtils::AjaxEpilog();
<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iFileId = 0;
$bResize = $_REQUEST['no_resize'] && $_REQUEST['no_resize'] == 'Y' ? false : true;
$arFile = array();
if(!empty($_FILES['files'])) {
	$arFile['name'] = $_FILES['files']['name'][0];
	$arFile['type'] = $_FILES['files']['type'][0];
	$arFile['tmp_name'] = $_FILES['files']['tmp_name'][0];
	$arFile['error'] = $_FILES['files']['error'][0];
	$arFile['size'] = $_FILES['files']['size'][0];
} elseif(!empty($_FILES['PERSONAL_PHOTO'])) {
	$arFile['name'] = $_FILES['PERSONAL_PHOTO']['name'];
	$arFile['type'] = $_FILES['PERSONAL_PHOTO']['type'];
	$arFile['tmp_name'] = $_FILES['PERSONAL_PHOTO']['tmp_name'];
	$arFile['error'] = $_FILES['PERSONAL_PHOTO']['error'];
	$arFile['size'] = $_FILES['PERSONAL_PHOTO']['size'];
}

$sErrMsg = '';
$arFile['MODULE_ID'] = 'main';
if(isset($arFile['name']) && strlen($arFile['name'])) {
	$sTmpErr = CFile::CheckImageFile($arFile, 0, 0, 0, array('IMAGE'));
	if(!strlen($sTmpErr)) {
		$iFileId = CFile::SaveFile($arFile, 'main');
	} else {
		$sErrMsg = 'Данный файл не является изображением, либо не поддерживается тип файла';
	}
}

$arImg = array();
if(intval($iFileId) > 0) {
	if(!$bResize) {
		$arImg = CFile::ResizeImageGet($iFileId, array('width' => 390, 'height' => 320), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	} else {
		$arImg = CFile::ResizeImageGet($iFileId, array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT, false);
	}
}

$arJson = array();
if(!strlen($sErrMsg)) {
	$arJson = array(
		'id' => $iFileId,
		'src' => $arImg['src'], 
		'width' => $arImg['width'],
		'height' => $arImg['height']
	);
} else {
	$arJson = array(
		'error' => $sErrMsg
	);
}

$GLOBALS['APPLICATION']->RestartBuffer();
echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

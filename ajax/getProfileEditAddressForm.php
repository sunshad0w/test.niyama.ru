<?php

define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

# Форма добавления гостя.
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.profile-address-popup',
    array(
        "SHOW_MODAL" => "Y",
        "AID"   => $_REQUEST['aid']
    ),
    null,
    array(
        'HIDE_ICONS' => 'Y'
    )
);


// это вместо эпилога
CProjectUtils::AjaxEpilog();
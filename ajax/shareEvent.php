<?php

define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule('iblock');

if ( (!empty($_REQUEST['social'])) && (!empty($_REQUEST['type']))) {

    $events = GetModuleEvents("main", "OnUserSocShare", true);
    foreach ($events as $arEvent) {
        ExecuteModuleEventEx($arEvent, array($_REQUEST['social'],$_REQUEST['type']));
    }

}

// это вместо эпилога
CProjectUtils::AjaxEpilog();
<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
	'bitrix:main.profile',
	'niyama.1.0',
	array(
		'USER_PROPERTY_NAME' => '',
		'SET_TITLE' => 'Y',
		'AJAX_MODE' => 'N',
		'USER_PROPERTY' => '',
		'SEND_INFO' => 'N',
		'CHECK_RIGHTS' => 'N',
		'AJAX_OPTION_JUMP' => 'N',
		'AJAX_OPTION_STYLE' => 'Y',
		'AJAX_OPTION_HISTORY' => 'N',
		'ACTION_PARAM' => 'show_edit_profile'
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

<?

define('STOP_STATISTICS', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");

$OrderPrice = isset($_REQUEST['totalPrice']) ? intval($_REQUEST['totalPrice']) : 0;
$id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 1;

$arTmp = array();
$minPrice = false;
$arrCityIdMinPrice = array();
$arrCityIdMaxPrice = array();

$data = CIBlockElement::GetList(array(), array('ID' => '6117'), false, array('PROPERTY_*'));
while ($ob = $data->GetNextElement()) {
    $arProps = $ob->GetProperties();
    $arrCityIdMinPrice['min'] = $arProps['R_REGION']['VALUE'];
}
var_dump($arrCityIdMinPrice);

$data = CIBlockElement::GetList(array(), array('ID' => '8570'), false, array('PROPERTY_*'));
while ($ob = $data->GetNextElement()) {
    $arProps = $ob->GetProperties();
    $arrCityIdMaxPrice['max'] = $arProps['R_REGION']['VALUE'];
}

if (in_array($id, $arrCityIdMinPrice['min'])) {
    $minPrice = CNiyamaCustomSettings::GetStringValue('min_sum_by_order');
}
if (in_array($id, $arrCityIdMaxPrice['max'])) {
    $minPrice = CNiyamaCustomSettings::GetStringValue('min_sum_by_away_order');
}

if($minPrice > $OrderPrice){
    $arTmp['DELIVERY_AVAILABLE'] = false;

} else {
    $arTmp['DELIVERY_AVAILABLE'] = true;
}



$arJson = array(
	'bDeliveryAvailable' => $arTmp['DELIVERY_AVAILABLE'],
	'min_price' => $minPrice
);

//_log_array(array($arDeliveryMatrix, $arTimePeriod, $arJson));


$GLOBALS['APPLICATION']->RestartBuffer();
echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

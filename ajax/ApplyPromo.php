<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
error_reporting(E_ALL & !E_NOTICE & !E_DEPRECATED);
ini_set('display_errors', 'on');

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

/*apply promo*/

if(isset($_POST['ajaxbasketpromo']) && $_POST["ajaxbasketpromo"] && $_POST["ajaxaction"] == 'applyPromo'){
$promo = CCatalogDiscount::SetCoupon($_POST['ajaxbasketpromo']);
echo $promo;
}
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
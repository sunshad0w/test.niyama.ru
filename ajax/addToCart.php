<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
error_reporting(E_ALL & !E_NOTICE & !E_DEPRECATED);
ini_set('display_errors', 'on');

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

/* Addition of the goods in a basket at addition in a basket */
if(isset($_POST['ajaxaddid']) && $_POST["ajaxaddid"] && $_POST["ajaxaction"] == 'add'){
    Add2BasketByProductID($_POST["ajaxaddid"], 1, array());
}
/* Goods removal at pressing on to remove in a small basket */
if(isset($_POST['ajaxdeleteid']) && $_POST["ajaxdeleteid"] && $_POST["ajaxaction"] == 'delete'){
    CSaleBasket::Delete($_POST["ajaxdeleteid"]);
}
/* Changes of quantity of the goods after receipt of inquiry from a small basket */
if(isset($_POST['ajaxbasketcountid']) && $_POST["ajaxbasketcountid"] && $_POST["ajaxbasketcount"] && $_POST["ajaxaction"] == 'update'){
    $arFields = array(
       "QUANTITY" => $_POST["ajaxbasketcount"]
);
CSaleBasket::Update($_POST["ajaxbasketcountid"], $arFields);
}

?>

    <?$APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "pizzabasketpopup",
            Array(
                "HIDE_ON_BASKET_PAGES" => "Y",
                "PATH_TO_BASKET" => "/personal/basket.php",
                "POSITION_FIXED" => "Y",
                "SHOW_IMAGE" => "Y",
                "SHOW_NOTAVAIL" => "N",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_PRICE" => "Y",
                "SHOW_PRODUCTS" => "Y",
                "SHOW_SUBSCRIBE" => "Y",
                "SHOW_SUMMARY" => "Y",
                "SHOW_TOTAL_PRICE" => "Y"
            )
        );?>
    </div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
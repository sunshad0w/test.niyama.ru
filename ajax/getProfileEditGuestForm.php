<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (isset($_REQUEST['gid']) && is_numeric($_REQUEST['gid'])) {

    # Форма добавления гостя
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.profile-guest-popup',
        array(
            'SHOW_MODAL' => 'Y',
            'GID' => $_REQUEST['gid']
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );
}

// это вместо эпилога
CProjectUtils::AjaxEpilog();

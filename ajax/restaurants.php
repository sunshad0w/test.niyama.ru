<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
	'niyama:restaurants.data',
	'niyama.1.0.ajax',
	array(
		'RESTAURANTS_IBLOCK_CODE' => 'restaurants-list',
		'SERVICES_IBLOCK_CODE' => 'restaurants-services',
		'CITIES_IBLOCK_CODE' => 'restaurants-cities',
		'CACHE_TYPE' => 'A',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

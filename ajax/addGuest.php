<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.profile-guest-popup',
	array(
		'SHOW_MODAL' => 'N',
		'JSON_RESULT' => 'Y',
		'MODE' => 'ADD_GUEST',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

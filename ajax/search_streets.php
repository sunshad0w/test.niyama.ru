<?
//
// Автокомплит улиц
//
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iStreetsLimit = 20;
$iBuildingsLimit = 20;

$sCity = trim($_REQUEST['city']);
$sAddress = trim($_REQUEST['address']);
$sHouse = trim($_REQUEST['house']);

//$sCity = 'Москва';
//$sAddress = 'Ленинская Слобода';

$arJson = array();
$arJson['streets'] = array();
//$arJson['buildings'] = array();
if(strlen($sCity) && strlen($sAddress)) {
	include_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/lib/kladr.php');
	//$arTmp = explode(',', $sAddress);
	//$sStreet = trim($arTmp[0]);
	$sStreet = $sAddress;
	$sBuilding = strlen($sHouse) ? $sHouse : '';

	$sKladrToken = CNiyamaCustomSettings::GetStringValue('kladr_cloud_token', '');
	$sKladrKey = CNiyamaCustomSettings::GetStringValue('kladr_cloud_key', '');

	// Инициализация api, в качестве параметров указываем токен и ключ для доступа к сервису
	$obKladr = new Kladr\Api($sKladrToken, $sKladrKey);

	$arCurCity = CCustomProject::KladrGetCity($sCity, $sKladrToken, $sKladrKey);

	// теперь ищем улицы
	$arStreetsResult = array();
	$arCurStreet = array();
	if($arCurCity) {
		if($_SESSION['_NIYAMA_SERACH_STREETS_'] && $_SESSION['_NIYAMA_SERACH_STREETS_']['arStreetsResult'] && $_SESSION['_NIYAMA_SERACH_STREETS_']['arCurStreet']['name'] == $sStreet) {
			// берем из кэша
			$arStreetsResult = $_SESSION['_NIYAMA_SERACH_STREETS_']['arStreetsResult'];
			$arCurStreet = $_SESSION['_NIYAMA_SERACH_STREETS_']['arCurStreet'];
		} else {
			$_SESSION['_NIYAMA_SERACH_STREETS_'] = array();
			$obKladrStreetQuery = new Kladr\Query();
			$obKladrStreetQuery->ContentName = $sStreet;
			// ограничиваем поиск улицы городом
			$obKladrStreetQuery->ParentType = Kladr\ObjectType::City;
			$obKladrStreetQuery->ParentId = $arCurCity['id'];
			$obKladrStreetQuery->ContentType = Kladr\ObjectType::Street;
			$obKladrStreetQuery->WithParent = false;
			$obKladrStreetQuery->Limit = $iStreetsLimit;
			$arStreetsResult = $obKladr->QueryToArray($obKladrStreetQuery);
			if($arStreetsResult) {
				foreach($arStreetsResult as $arItem) {
					if($arItem['name'] == $sStreet) {
						$arCurStreet = $arItem;
						break;
					}
				}
			}
			if($arCurStreet) {
				// кэшируем в сессии значение
				$_SESSION['_NIYAMA_SERACH_STREETS_'] = array(
					'arCurStreet' => $arCurStreet,
					'arStreetsResult' => $arStreetsResult
				);
			}
		}
	}

	// дома (если выбрана улица)
	$arBuildingsResult = array();
	if($arCurStreet && strlen($sHouse)) {
		$obKladrBuildingsQuery = new Kladr\Query();
		$obKladrBuildingsQuery->ContentName = $sBuilding;

		$obKladrBuildingsQuery->ParentType = Kladr\ObjectType::Street;
		$obKladrBuildingsQuery->ParentId = $arCurStreet['id'];

		$obKladrBuildingsQuery->ContentType = Kladr\ObjectType::Building;
		$obKladrBuildingsQuery->WithParent = false;
		$obKladrBuildingsQuery->Limit = $iBuildingsLimit;

		// дома
		$arBuildingsResult = $obKladr->QueryToArray($obKladrBuildingsQuery);
		$arJson['buildings'] = array();
	}

	if($arStreetsResult) {
		foreach($arStreetsResult as $arItem) {
			$arJson['streets'][] = array(
				'name' => $arItem['name'],
				'full_name' => $arItem['name'],
			);
		}
	}

	if($arBuildingsResult) {
		foreach($arBuildingsResult as $arItem) {
			$arJson['buildings'][] = array(
				'name' => $arItem['name'],
				'full_name' => $arItem['name']
			);
		}
	}
}

/*
if(empty($arJson['buildings'])) {
	unset($arJson['buildings']);
}
*/

echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

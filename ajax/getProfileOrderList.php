<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iGuestID = isset($_REQUEST['gid']) && (intval($_REQUEST['gid']) > 0) ? intval($_REQUEST['gid']) : 0;
$bFilter = isset($_REQUEST['gid']) ? true : false;

$arParams = array();
$arParams['SELECT_STATUSES'] = array('E', 'F', 'N', 'P', 'R', 'S', 'T');
if($iGuestID) {
	$arParams['AJAX_REQUEST'] = 'Y';
	$arParams['GID'] = $iGuestID;
} else {
	if($bFilter) {
		$arParams['AJAX_REQUEST'] = 'Y';
	}
}

$APPLICATION->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.profile-order-popup',
	$arParams,
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

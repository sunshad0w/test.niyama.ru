<?
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

# Форма добавления или редактирования карты
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.profile-card-popup',
    array(
    ),
    null,
    array(
        'HIDE_ICONS' => 'Y'
    )
);

// это вместо эпилога
CProjectUtils::AjaxEpilog();

<?
define("NIYAMA_LIGHT_TEMPLATE", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подтверждение регистрации карты");
?>
<div class="main-content">
    <div class="main-content__wrapper">
        <img src="/local/templates/pizzapi/img/travka.png" class="bg-travka">
        <img src="/local/templates/pizzapi/img/pomidor.png" class="bg-pomidor">
        <img src="/local/templates/pizzapi/img/grib.png" class="bg-grib">

        <div class="catalog-wrapper">
            <div class="container">
                <?
                $APPLICATION->IncludeComponent(
                    'adv:system.empty',
                    'niyama.1.0.card-confirm-form',
                    array(
                    ),
                    null,
                    array(
                        'HIDE_ICONS' => 'Y'
                    )
                );
                ?>
            </div>

        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
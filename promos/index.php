<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Скидки и акции");
$h1 = $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_H1') ? $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_H1') :'Пицца Пи: интересные предложения для насыщения и экономии';
?>

<div class="main-content">
	<div class="main-content__wrapper">

		<div class="catalog-wrapper">
			<div class="container">

                <h1 class="page__title"><?=$h1?></h1>
                <div class="promos__text">
                    <p>Сеть итальянских ресторанов Пицца Пи отличается от конкурентов богатым меню с невероятно вкусной едой. Нам доверяют клиенты, потому что мы используем качественные компоненты в процессе приготовления пищи. Благодаря умелым поварам даже самое заурядное блюдо получается превосходным. Мы заботимся о том, чтобы вам было с нами комфортно. Именно поэтому каждый день мы проводим интересные акции и делаем щедрые скидки для вашей экономии. Пицца Пи — вкусно, полезно и недорого!</p>
                </div>

                <?$APPLICATION->IncludeComponent("bitrix:news.list","template2",Array(
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "AJAX_MODE" => "Y",
                        "IBLOCK_TYPE" => "promos",
                        "IBLOCK_ID" => "64",
                        "NEWS_COUNT" => "20",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "ID",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => Array(""),
                        "PROPERTY_CODE" => Array("DESCRIPTION"),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "Y",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_LAST_MODIFIED" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_TOP_PAGER" => "Y",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "Y",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "Y",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "PAGER_BASE_LINK_ENABLE" => "Y",
                        "SET_STATUS_404" => "Y",
                        "SHOW_404" => "Y",
                        "MESSAGE_404" => "",
                        "PAGER_BASE_LINK" => "",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                    )
                );?>
                <h4 class="page__title">Акции</h4>
                <div class="promos__text">
                    <p>В данном разделе вы можете ознакомиться с различными акционными предложениями. Каждый день мы предлагаем своим клиентам подарки. Например, заказав у нас на определённую сумму, вы можете получить какое-либо блюдо абсолютно бесплатно. Таким образом, покупая в ресторанах Пицца Пи вы и экономите, и насыщаетесь ещё больше. Этот раздел регулярно обновляется, поэтому посещайте почаще данную страницу, чтобы не пропустить никаких интересных предложений от нашей компании.</p>
                    <p>Прямо сейчас при заказе на сумму от 2000 рублей вы получите сливочный сет или пиццу пепперони в подарок!</p>
                </div>
                <div class="promos__text">
                    <h4 class="page__title">Скидки</h4>

                    <p>Стоимость блюд нашего меню сама по себе невысокая. Политика нашей компании такова, что мы предлагаем отличную еду за приемлемую цену. Это одна из причин, почему у нас так много клиентов. Несмотря на это, Пицца Пи каждый день делает скидки в 10%, 20% и даже 30%, чтобы вы могли экономить ещё больше. В нашем ресторане вы купите больше еды за меньшие деньги!</p>

                    <p>На данный момент действуют следующие скидки:</p>
                    <ol>
                        <li>10%, если вы закажете какое-либо блюдо через мобильное приложение.</li>
                        <li>20% при заказе через call-центр еды домой или в офис.</li>
                        <li>30% скидка на доставку нашей продукции.</li>
                    </ol>
                    <p>Раздел регулярно обновляется, поэтому не исключено, что скоро появятся новые скидки.</p>
                </div>
                <div class="promos__text">
                    <h4 class="page__title">Преимущества компании Пицца Пи</h4>

                    <p>Почему заказывать еду следует у нас? Вот несколько причин:</p>
                <ul>
                <li>богатое меню: покупайте итальянскую еду на свой вкус, вам всегда будет, с чего выбрать;</li>
                <li>быстрая доставка без каких-либо задержек;</li>
                <li>рассчитаться можно как наличными с курьером, так и сразу с помощью карты через наш сайт;</li>
                    <li>все блюда изготавливаются из компонентов, высокое качество которых подтверждено соответствующими сертификатами.</li>
                </ul>
                    <p>Оформляйте заказ любого блюда прямо сейчас</p>

                    <p>Обязательно ознакомьтесь с нашим меню и сделайте заказ на понравившееся блюдо. Гарантируем быструю доставку в ваш город. Если возникли вопросы, можете задать их по телефону, указанному в верхнем правом углу страницы. Желаем хорошего отдыха и приятного аппетита!</p>
                </div>

            </div>
        </div>
    </div>
</div>

					 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');

if ( !CNiyamaCustomSettings::getStringValue("text_on_delivery_no_sum_price",false) ){
    CNiyamaCustomSettings::setStringValue("Текст отображающийся на странице проверки заказа,если не хватает стоимости заказа для выбранной доставки (#sum_price# - код для подстановки)","text_on_delivery_no_sum_price","<p>Минимальная стоимость доставки заказа по вашему адресу составляет #sum_price#.</p><p>Вы можете выбрать дополнительные блюда или заказать доставку на другой адрес.</p>");
	echo '<p>Добавлена настройка: Текст отображающийся на странице проверки заказа</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("delevery_id_sity_other",false) ){
    CNiyamaCustomSettings::setStringValue("ID города 'Другой'","delevery_id_sity_other","27");
	echo '<p>Добавлена настройка:ID города Другой</p>';
}

if ( !CNiyamaCustomSettings::getStringValue("delevery_id_sity_other_text",false) ){
    CNiyamaCustomSettings::setStringValue("Подпись если выбран город Другой","delevery_id_sity_other_text","Не нашли свой город? Позвоните нам <span>+7 495 781-781-9</span> или <a href='/delivery/'>посмотрите карту доставки</a>");
	echo '<p>Добавлена настройка:подпись если выбран город Другой</p>';
}
$iNiyamaBugVer = intval(COption::GetOptionInt('main', 'NiyamaBugVer', 0, '-'));
if($iNiyamaBugVer < 2) {
	$iNiyamaBugVer = 2;

	$iOrderDeliveryGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Информация о доставке') {
			$iOrderDeliveryGroupId = $arItem['ID'];
		}
	}

	if(!$iOrderDeliveryGroupId) {
		$iOrderDeliveryGroupId = CSaleOrderPropsGroup::Add(
			array(
				'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
				'NAME' => 'Информация о доставке',
				'SORT' => 100
			)
		);
		if($iOrderCustomerGroupId) {
			echo '<p>Добавлена группа свойств заказа: ['.$iOrderCustomerGroupId.'] Информация о доставке</p>';
		}
	}

	if($iOrderDeliveryGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderDeliveryGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}
		if(!$arTmpOrderPropsList['DELIVERY_CITY_DOP']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Город (другой)',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 105,
					'CODE' => 'DELIVERY_CITY_DOP',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Город (другой)</p>';
			}
		}
	}
}elseif($iNiyamaBugVer < 3) {
	$iNiyamaBugVer = 3;
	$IBLOCK_ID_genaral_menu = CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog');
	$sUserFieldEntityId = 'IBLOCK_'.$IBLOCK_ID_genaral_menu.'_SECTION';
	$sUserFieldName = 'UF_UPSAILCAT';

	$obUserTypeEntity = new CUserTypeEntity();
	$iUserFieldId = $obUserTypeEntity->Add(
		array(
			'ENTITY_ID' => $sUserFieldEntityId,
			'FIELD_NAME' => $sUserFieldName,
			'USER_TYPE_ID' => 'boolean',
			'MULTIPLE' => 'N',
			'SHOW_FILTER' => 'I',
			'SHOW_IN_LIST' => 'Y',
			'EDIT_IN_LIST' => 'Y',
			'IS_SEARCHABLE' => 'N',
			'SETTINGS' => array(
				'DISPLAY' => 'CHECKBOX'
			),
			'EDIT_FORM_LABEL' => array('ru' => 'Предлагать блюда из данной категории в качестве дополнительных'),
			'LIST_COLUMN_LABEL' => array('ru' => 'Предлагать блюда из данной категории в качестве дополнительных'),
			'LIST_FILTER_LABEL' => array('ru' => 'Предлагать блюда из данной категории в качестве дополнительных'),
			'ERROR_MESSAGE' => array('ru' => ''),
			'HELP_MESSAGE' => array('ru' => '')
		)
	);	
	if ($iUserFieldId){
		echo '<p>Добавлено пользовательское свойство: ['.$iUserFieldId.'] '.$sUserFieldName.'</p>';
	}
}		
//$iNiyamaBugVer = 3;		
if($iNiyamaBugVer < 4) {
	$iNiyamaBugVer = 4;	
	
	/**
	 *  ингредиенты
	 *
	 */
	$iBlockExIngredient = 0;
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-ex-ingredients', 'catalog');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Исключающие ингредиенты',
			'CODE' => 'catalog-ex-ingredients',
			'IBLOCK_TYPE_ID' => 'catalog',
			'SITE_ID' => 's1',
			'SORT' => '1000',
			'GROUP_ID' => array(2 => "R"),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			$iBlockExIngredient = $iTmpIBlockId;
			?><p>Создан инфоблок "Исключающие ингредиенты"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Исключающие ингредиенты": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}else{
		$iBlockExIngredient = $iTmpIBlockId;
	}
	
	$IBLOCK_ID_genaral_menu = CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog');
	if($IBLOCK_ID_genaral_menu>0) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 650;
		// ---
	
	
		$sTmpPropCode = 'EXCLUDE_INGREDIENT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Ингредиенты исключения',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => $iBlockExIngredient,
			'IS_REQUIRED' => 'N',
		);
		echo $iBlockExIngredient." - ID Исключающие ингредиенты<br>";
	
	
	
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $IBLOCK_ID_genaral_menu;
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
	# Количество ингредиентов, выводимое до ссылки "Все исключения"
	if ( !CNiyamaCustomSettings::getStringValue("default_ex_ingredient_count",false) ){
		CNiyamaCustomSettings::setStringValue('Количество ингредиентов, выводимое до ссылки Все исключени" (0 или пустая строка - выводит все сразу)',"default_ex_ingredient_count","10");
	}
}
		
COption::SetOptionInt('main', 'NiyamaBugVer', $iNiyamaBugVer, '', '-');
echo "<hr>";
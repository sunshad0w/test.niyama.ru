<?php


$dbItems = CIBlockType::GetById('statistic');
if(!$dbItems->Fetch()) {
    $arFields = array(
        'ID' => 'statistic',
        'SECTIONS' => 'N',
        'IN_RSS' => 'N',
        'SORT' => 500,
        'LANG' => array(
            'ru' => array(
                'NAME' => 'Статистика'
            ),
            'en' => array(
                'NAME' => 'Статистика'
            ),
        )
    );
    $obIBlockType = new CIBlockType();
    $bResult = $obIBlockType->Add($arFields);
    if($bResult) {
        ?><p>Добавлен тип инфоблоков "Статистика"</p><?
    } else {
        ?><p>Ошибка добавления типа инфоблоков "Статистика": <?=$obIBlockType->LAST_ERROR?></p><?
    }
}


/**
 *  Фасетный поиск
 *
 */
$iBlockSpecMenu = 0;
$iStatFasId = CProjectUtils::GetIBlockIdByCodeEx('fas-search', 'statistic');
if(!$iStatFasId) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Фасетный поиск',
        'CODE' => 'fas-search',
        'IBLOCK_TYPE_ID' => 'statistic',
        'SITE_ID' => 's1',
        'SORT' => '60',
        'GROUP_ID' => array(2 => "R"),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iStatFasId = $obIBlock->Add($arFields);
    if($iStatFasId) {
        $iBlockSpecMenu = $iStatFasId;
        ?><p>Создан инфоблок "Специальное меню"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Специальное меню": <?=$obIBlock->LAST_ERROR?></p><?
    }
}


if($iStatFasId) {
    $arTmpPropertiesMeta = array();
    $iPropsSort = 400;

    $sTmpPropCode = 'FAS_COUNT';
    $iPropsSort += 100;
    $arStatSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Количество выборок',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iStatFasId));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arStatSIDPropertiesMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iStatFasId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
    
}

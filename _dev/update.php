<?
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);
define('_NIYAMA_COMMON_DEV_UPDATE_', true);

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}
BXClearCache(true);


/*
//
// Настройки / Опции
//
echo '<h1>NIYAMA-97.php</h1>';
include_once('NIYAMA-97.php');

//
// Настройка сайта
//
//echo '<h1>NIYAMA-71.php</h1>';
//include_once('NIYAMA-71.php');

//
// Авторизация \ Регистрация
//
echo '<h1>NIYAMA-162.php</h1>';
include_once('NIYAMA-162.php');
*/

//
// LSV
//
/*
echo '<h1>NIYAMA-LSV.php</h1>';
include_once('NIYAMA-LSV.php');
*/

/*
//
// Слайдер на Главной
//
echo '<h1>NIYAMA-40.php</h1>';
include_once('NIYAMA-40.php');

//
// Промо-блок на Главной
//
echo '<h1>NIYAMA-94.php</h1>';
include_once('NIYAMA-94.php');


//
// Аватары и персоны пользователей
//
echo '<h1>NIYAMA-122.php</h1>';
include_once('NIYAMA-122.php');

//
// Каталог
//
echo '<h1>NIYAMA-78.php</h1>';
include_once('NIYAMA-78.php');

//
// Купоны
//
echo '<h1>NIYAMA-88.php</h1>';
include_once('NIYAMA-88.php');
echo '<h1>NIYAMA-106.php</h1>';
include_once('NIYAMA-106.php');
//
// Дополнительная Информация
//
echo '<h1>NIYAMA-SID.php</h1>';
include_once('NIYAMA-SID.php');

echo "<h1>NIYAMA-STAT.php</h1>";
include_once('NIYAMA-STAT.php');

echo '<h1>NIYAMA-84.php</h1>';
include_once('NIYAMA-84.php');

echo '<h1>NIYAMA-BUG.php</h1>';
include_once('NIYAMA-BUG.php');

echo '<h1>NIYAMA03-2.php</h1>';
include_once('NIYAMA03-2.php');
*/

// 2015-09-10-001
include_once('NIYAMA15-2.php');

BXClearCache(true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

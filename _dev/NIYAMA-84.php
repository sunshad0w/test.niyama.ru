<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');



if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_BONUS_NIYAM'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_BONUS_NIYAM',
        'NAME' => "Бонусы-ниямы",
        'SORT' => 1000,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Бонус',
        'ELEMENT_NAME' => 'Бонус',
        'ELEMENT_ADD' => 'Добавить бонус',
        'ELEMENT_EDIT' => 'Изменить бонус',
        'ELEMENT_DELETE' => 'Удалить бонус',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){
		
		
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}


//добавление свойств к ИБ "Бонусы-ниямы"
if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && ($ID=CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_BONUS_NIYAM'))) {
		
		$sTmpPropCode = 'BASE_TYPE_ACTION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип действия',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "Все типы действий",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"ALL",
									),
									array(
										  "VALUE" => "Заказ",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"ORDER",
									),
									array(
										  "VALUE" => "Авторизация через социальную сеть",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"AUTHSOC",
									),
									array(
										  "VALUE" => "Участие в опросе",
										  "DEF" => "N",
										  "SORT" => "400",
										  "XML_ID"=>"SURVEY",
									),
									array(
										  "VALUE" => "За получение медали",
										  "DEF" => "N",
										  "SORT" => "500",
										  "XML_ID"=>"MEDAL",
									),
									array(
										  "VALUE" => "За получение следующего уровня скидки",
										  "DEF" => "N",
										  "SORT" => "600",
										  "XML_ID"=>"TIMELINE",
									), 
									array(
										  "VALUE" => "За рекомендацию в социальных сетях",
										  "DEF" => "N",
										  "SORT" => "700",
										  "XML_ID"=>"RECOMENDATIONSOC",
									)
								)
		);
		
		
		
		$sTmpPropCode = 'ORDER_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип заказа',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "За каждый N заказ",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"COUNT_N",
									),
									array(
										  "VALUE" => "Заказ в День Рождения",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"BIRTHDAY",
									)
								)
		);
		$sTmpPropCode = 'ORDER_COUNT_NIAM';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество ниям равно сумме заказа',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'C',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "Количество ниям равно сумме заказа",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"Y",
									)
									
								)
		);
		
		$sTmpPropCode = 'MEDAL_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'За какие медали',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "За каждую N медаль",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"COUNT_N",
									),
									array(
										  "VALUE" => "За определенные медали",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"LIST",
									)
								)
		);

		$sTmpPropCode = 'MEDAL_LIST';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Список медалей',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'EList', 			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'LINK_IBLOCK_ID'=> CProjectUtils::GetIBlockIdByCodeEx('medals', 'NIYAMA_DISCOUNTS'),
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'USER_TYPE_SETTINGS' => array(
				'multiple' 	=> 'Y',
				'size'		=> 5
			)
		);
		$sTmpPropCode = 'TIMELINE_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип получения уровня скидки',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "За каждую N уровень",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"COUNT_N",
									),
									array(
										  "VALUE" => "За определенный уровень",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"LIST",
									)
								)
		);

		$sTmpPropCode = 'TIMELINE_LIST';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Список уровней скидок',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'EList', 			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'LINK_IBLOCK_ID'=> CProjectUtils::GetIBlockIdByCodeEx('discount_levels', 'customer_loyalty'),
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'USER_TYPE_SETTINGS' => array(
				'multiple' 	=> 'Y',
				'size'		=> 5
			)
		);
		
		$sTmpPropCode = 'RECOMENDATIONSOC_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип рекомендации',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => "Любая",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"ANY",
									),
									array(
										  "VALUE" => "Поделился новым уровнем скидки или медалью",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"TWEET_YROVEN_OR_MEDAL",
									),	
									array(
										  "VALUE" => "Рекомендовал страничку любой социальной сети (через виджет)",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"RECOMENDATON_FROM_VIDZHET",
									),																										
								)	
		);
		
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'За какое количество действий',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 1,
			'HINT'			=> '',
	
		);
		$sTmpPropCode = 'BASE_COUNT_NIYAM';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество ниямов',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			
			
	
		);	
		
		

		
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $ID));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem;
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {			
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $ID; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
}
echo "<hr>";

if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_BONUS_NIYAM_USERS'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_BONUS_NIYAM_USERS',
        'NAME' => "Бонусы-ниямы пользователей",
        'SORT' => 100,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',        
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Бонусы',
        'ELEMENT_NAME' => 'Бонус',
        'ELEMENT_ADD' => 'Добавить бонус',
        'ELEMENT_EDIT' => 'Изменить бонус',
        'ELEMENT_DELETE' => 'Удалить бонус',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){		
		$fields = CIBlock::getFields($ID);
		$fields["ACTIVE"]["DEFAULT_VALUE"]='N';
		CIBlock::setFields($ID, $fields);
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}

if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && ($ID=CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_BONUS_NIYAM_USERS'))) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		
		$sTmpPropCode = 'USER_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Пользователь',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'UserID',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BONUS_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'ID Бонуса',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '', 			
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'LINK_IBLOCK_ID'=>CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_BONUS_NIYAM'), 
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		
		$sTmpPropCode = 'BASE_QUANITY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество полученых бонусов в шт.',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);	
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_ORDER';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Заказ',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_AUTHSOC';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Авторизаций через соц сети',   
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_MEDAL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Получение медали',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_TIMELINE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Получения уровня скидки',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_SURVEY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Участие в опросе',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий За рекомендацию в социальных сетях',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		
		$sTmpPropCode = 'ORDER_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'ID заказа',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
			
		

		
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $ID));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem;
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {			
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $ID; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
}


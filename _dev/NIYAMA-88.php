<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');


if (!CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')){
	$arFields['ID'] = 'NIYAMA_DISCOUNTS';
	$arFields['SECTIONS'] = 'N';
	$arFields['IN_RSS'] = 'N';
	$arFields['SORT'] = 120;
	$langDefaults = array(
		'ru' => array(
			'NAME'          => 'Скидки',
			'SECTION_NAME'  => 'Разделы',
			'ELEMENT_NAME'  => 'Элементы',
		),
		'en' => array(
			'NAME'          => 'Discounts',
			'SECTION_NAME'  => 'Sections',
			'ELEMENT_NAME'  => 'Elements',
		),
	);	
	$l = CLanguage::GetList($lby="sort", $lorder="asc");
	while($arIBTLang = $l->GetNext())
	{
		if (array_key_exists($arIBTLang["LID"], $langDefaults))
			$arFields["LANG"][$arIBTLang["LID"]] = $langDefaults[$arIBTLang["LID"]];
	}
	$obBlocktype = new CIBlockType;	
	$res = $obBlocktype->Add($arFields);
	if(!$res){
	   echo '<p>Error: '.$obBlocktype->LAST_ERROR.'</p>';
	}
	else{		
		echo '<p>Добавлен тип ИБ: '.$arFields['ID'].'</p>';
	}
}
if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS_BLUDA'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_CUPONS_BLUDA',
        'NAME' => "Бонусные блюда для скидок",
        'SORT' => 100,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Блюда',
        'ELEMENT_NAME' => 'Блюдо',
        'ELEMENT_ADD' => 'Добавить блюдо',
        'ELEMENT_EDIT' => 'Изменить блюдо',
        'ELEMENT_DELETE' => 'Удалить блюдо',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){
		
		
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
//добавление свойств к ИБ "Бонусные блюда для скидок"
/*if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && ($ID=CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS_BLUDA'))) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'BLUDO';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Блюдо',			
			'PROPERTY_TYPE' => 'E',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'LINK_IBLOCK_ID'=>CProjectUtils::GetIBlockIdByCodeEx('general-site-menu'), 
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			
		);
		$sTmpPropCode = 'TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "Бесплатно (в подарок)",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"free",
									),
									array(
										  "VALUE" => "По сниженной цене",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"reduced",
									)
								)
		);
		$sTmpPropCode = 'PRICE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Новая стоимость',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> 'C',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			
		);

		
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $ID));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $ID; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
}

*/

if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_CUPONS',
        'NAME' => "Купоны скидок",
        'SORT' => 100,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '/local/php_interface/adv/iblock_forms/iblock_cupons_before_save.php',
        'EDIT_FILE_AFTER' => '/local/php_interface/adv/iblock_forms/iblock_cupons_form.php',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Купоны',
        'ELEMENT_NAME' => 'Купон',
        'ELEMENT_ADD' => 'Добавить купон',
        'ELEMENT_EDIT' => 'Изменить купон',
        'ELEMENT_DELETE' => 'Удалить купон',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){		
		$fields = CIBlock::getFields($ID);
		$fields["ACTIVE"]["DEFAULT_VALUE"]='N';
		CIBlock::setFields($ID, $fields);
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
//добавление свойств к ИБ "Купоны скидок"
if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && ($ID=CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS'))) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'BASE_TYPE_IMG';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип изображения',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "По умолчанию",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"DEFAULT",
									),
									array(
										  "VALUE" => "Загрузка изображения",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"DOWNLOAD",
									),
									array(
										  "VALUE" => "Изображение блюд/блюда",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"BLUDA",
									)
								)
		);
		$sTmpPropCode = 'BASE_TYPE_IMG_DOWNLOAD_H';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Загрузка изображения для горизонтального купона (десктоп)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'FileMan',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_TYPE_IMG_DOWNLOAD_V';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Загрузка изображения для вертикального купона (планшет)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'FileMan',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_TYPE_ACTION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип действия',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "Все типы действий",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"ALL",
									),
									array(
										  "VALUE" => "Заказ",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"ORDER",
									),
									array(
										  "VALUE" => "Авторизация через социальную сеть",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"AUTHSOC",
									),
									array(
										  "VALUE" => "Участие в опросе",
										  "DEF" => "N",
										  "SORT" => "400",
										  "XML_ID"=>"SURVEY",
									),
									array(
										  "VALUE" => "За рекомендацию в социальных сетях",
										  "DEF" => "N",
										  "SORT" => "500",
										  "XML_ID"=>"RECOMENDATIONSOC",
									)
								)
		);
		$sTmpPropCode = 'BASE_COUNT_ACTION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество получений купона',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 1,
			'HINT'			=> 'Сколько всего купонов с одним набором условий может получить пользователь, т.е. сколько раз выполнение всех установленных условий обеспечит добавление купона (из расчета - один купон за одно выполнение всех условий)',
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество действий',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 1,
			'HINT'			=> 'Количество действий установленного типа, которое нужно осуществить',
	
		);	
		
		$sTmpPropCode = 'BASE_PERIOD_ACTION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Период действия',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Период, в течение которого должно осуществляться действие с учетом всех установленных условий',
			'VALUES'		=> array(
									array(
										  "VALUE" => "Любой период",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"ANY",
									),
									array(
										  "VALUE" => "За месяц",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"MONTH",
									),
									array(
										  "VALUE" => "В день рождения",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"BIRTHDAY",
									),
									array(
										  "VALUE" => "Выбрать период",
										  "DEF" => "N",
										  "SORT" => "400",
										  "XML_ID"=>"CUSTOM",
									),									
								)
		);
		$sTmpPropCode = 'BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество дней до и после ДР',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Количество дней, до и после даты рождения',
			'DEFAULT_VALUE'	=> 7,
			
		);
		$sTmpPropCode = 'BASE_PERIOD_ACTION_CUSTOM_S';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'C',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'DateTime',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Дата начала периода действия купона',

			
		);
		$sTmpPropCode = 'BASE_PERIOD_ACTION_CUSTOM_DO';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'До',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'DateTime',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Дата окончания периода действия купона',

			
		);
			
		$sTmpPropCode = 'ORDER_TIME_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип временного диапазона',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Диапазон с-до",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"DIAPOZON",
									),
									array(
										  "VALUE" => "Количество часов перед доставкой",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"COUNT_HOURS_DELIVERY",
									),																	
								)
		);
		$sTmpPropCode = 'BASE_TIME_ACTION_S';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'C',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Время начала действия купона',

			
		);
		$sTmpPropCode = 'BASE_TIME_ACTION_DO';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'До',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'HINT'			=> 'Время окончания действия купона',

			
		);
		$sTmpPropCode = 'ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество часов перед доставкой',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			'HINT'			=> 'Бонус предоставляется в том случае, если пользователь при оформлении заказа указывает значения параметров опции «К определенной дате и времени», которые в часах больше, чем значение этого поля',		
		);
		
		$sTmpPropCode = 'BASE_BONUS_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип бонуса',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Скидка в %",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"DISCOUNT_PERSENT",
									),
									array(
										  "VALUE" => "Скидка в деньгах",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"DISCOUNT_MONEY",
									),	
									array(
										  "VALUE" => "Бонусное блюдо",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"BLUDO",
									),																	
								)
		);

		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_PERSENT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Размер скидки (в %)',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип скидки в деньгах',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Скидка в ниямах",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"NIYAM",
									),
									array(
										  "VALUE" => "Скидка в деньгах",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"MONEY",
									),																										
								)
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_MONEY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Размер скидки',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_BLUDO';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Бонусное блюдо',			
			'PROPERTY_TYPE' => 'E',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'bonus_bluds', 			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'MULTIPLE_CNT' 	=> 1,			
			'LINK_IBLOCK_ID'=>CProjectUtils::GetIBlockIdByCodeEx('general-site-menu'), 
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_BLUDO_VISIUAL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Как отображать',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Выводить, как комплект",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"COMPLECT",
									),
									array(
										  "VALUE" => "Выводить отдельными карточками",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"CARTS",
									),																										
								)
		);
		$sTmpPropCode = 'BASE_BONUS_NO_SUM';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Не суммировать с другими скидками',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'C',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Да",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"Yes",
									)																																		
								)
		);
		$sTmpPropCode = 'BASE_BONUS_TYPEACTIVE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Когда предоставлять бонус',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Со следующего заказа",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"NEXT",
									),
									array(
										  "VALUE" => "В текущем заказе",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"CURRENT",
									),																										
								)
		);
		$sTmpPropCode = 'ORDER_TYPE_DELIVERY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип доставки',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Самовывоз",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"SAMOVYVOZ",
									),
									array(
										  "VALUE" => "Курьером",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"CURIER",
									),																										
								)	
		);
		$sTmpPropCode = 'ORDER_TYPE_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Типы блюд',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'ORDER_INGRIDIENTS_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Ингридиенты',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'ORDER_SPEC_MENU_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Специальное меню',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'ORDER_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Блюда',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		
		$sTmpPropCode = 'ORDER_SOSTAV_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Состав',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => "Разные позиции",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"DIFFERENT",
									),
									array(
										  "VALUE" => "В любом",
										  "DEF" => "Y",
										  "SORT" => "200",
										  "XML_ID"=>"ANY",
									),	
									array(
										  "VALUE" => "Одинаковые позиции",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"SAME",
									),																										
								)	
		);
		$sTmpPropCode = 'ORDER_IN_WHAT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'В каком заказе',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => "В одном",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"ONE",
									),
									array(
										  "VALUE" => "В любом",
										  "DEF" => "Y",
										  "SORT" => "200",
										  "XML_ID"=>"ANY",
									),	
									array(
										  "VALUE" => "Подряд",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"SUCCESSION",
									),																										
								)	
		);
		$sTmpPropCode = 'ORDER_COUNT_BLUDS_YSLOVIE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество блюд условие',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => ">=",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"BOLSHE",
									),
									array(
										  "VALUE" => "=",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"RAVNO",
									),																										
								)	
		);
		$sTmpPropCode = 'ORDER_COUNT_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество блюд',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 1,
			
		);
		
		$sTmpPropCode = 'ORDER_SUMM_ZACAZ_YSLOVIE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Сумма заказа условие',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => "Сумма равна",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"RAVNA",
									),
									array(
										  "VALUE" => "Минимальная сумма",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"MIN",
									),	
									array(
										  "VALUE" => "Максимальная сумма",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"MAX",
									),																										
								)	
		);
		$sTmpPropCode = 'ORDER_SUMM_ZACAZ';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Сумма заказа в рублях',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		
		);
		$sTmpPropCode = 'RECOMENDATIONSOC_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип рекомендации',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,				
			'VALUES'		=> array(
									array(
										  "VALUE" => "Любая",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"ANY",
									),
									array(
										  "VALUE" => "Поделился новым уровнем скидки или медалью",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"TWEET_YROVEN_OR_MEDAL",
									),	
									array(
										  "VALUE" => "Рекомендовал страничку любой социальной сети (через виджет)",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"RECOMENDATON_FROM_VIDZHET",
									),																										
								)	
		);

		
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $ID));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem;
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if ($arPropMeta['CODE']=='BASE_BONUS_TYPE_BLUDO'){
				$ibp = new CIBlockProperty;				
				if ((!empty($arAddProps[$arPropMeta['CODE']]['ID']))&&($arAddProps[$arPropMeta['CODE']]['USER_TYPE'] != 'bonus_bluds')){
					if(!$ibp->Update($arAddProps[$arPropMeta['CODE']]['ID'], $arPropMeta)){
						echo '<p>Ошибка обновления свойства: ['.$arAddProps[$arPropMeta['CODE']]['ID'].'] '.$arPropMeta['CODE'].': '.$ibp->LAST_ERROR.'</p>'; 
					}else{
						echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']]['ID'].'] '.$arPropMeta['CODE'].'</p>';
					}
				}
			}
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $ID; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
}
echo "<hr>";


<?
return; // подстраховка от ошибочного запуска


define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

CModule::IncludeModule('iblock');

function object2array($object) { return @json_decode(@json_encode($object),1); }


if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/upload")) {
    mkdir($_SERVER["DOCUMENT_ROOT"]."/upload", 0777, true);
}
if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/upload/tmp")) {
    mkdir($_SERVER["DOCUMENT_ROOT"]."/upload/tmp", 0777, true);
}


$sResultXml = file_get_contents('export_news_niyama.xml');

if(strlen($sResultXml)) {
    $obXmlContent = simplexml_load_string($sResultXml);
    $xmlArray = object2array($obXmlContent);
    $arNewsCatalog = $xmlArray['Каталог']['Товары']['Товар'];

    $i=0;
    foreach ( $arNewsCatalog as $arItem ) {

        $sName  = $arItem['Наименование'];

        $sImage = $arItem['Картинка'];
        $arIMAGE = array();
        if (!empty($sImage)) {
            $imageContent = file_get_contents($sImage);
            $ext = strtolower(substr($sImage,-3));
            if (!in_array($ext,array('jpg','jpeg','png','gif','bmp'))) {
                $ext = 'jpg';
            }

            $tmpName = $tmpName = randString(10).'.'.$ext;
            $tmpName = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$tmpName;
            if (file_put_contents($tmpName,$imageContent)){
                $arIMAGE = CFile::MakeFileArray($tmpName);
                $arIMAGE["MODULE_ID"] = "main";
            }
        }

        $bActive = ($arItem['ЗначенияСвойств']['ЗначенияСвойства'][0]['Значение'] === "True") ? "Y" : "N";
        $sDateActiveFrom = $arItem['ЗначенияСвойств']['ЗначенияСвойства'][2]['Значение'];
        $sPREVIEW_TEXT = $arItem['ЗначенияСвойств']['ЗначенияСвойства'][3]['Значение'];
        $sDETAIL_TEXT  = $arItem['ЗначенияСвойств']['ЗначенияСвойства'][4]['Значение'];

        $iTmpIBlockId = CProjectUtils::GetIBlockIdByCode('news', 'misc');

        $el = new CIBlockElement;
        $arPROP = array(
            "F_PHOTOS" => $arIMAGE
        );

        $arLoadProductArray = Array(
            'MODIFIED_BY' => $GLOBALS['USER']->GetID(),
            'ACTIVE_FROM' =>  ConvertTimeStamp(strtotime($sDateActiveFrom), "SHORT"),
            'IBLOCK_SECTION_ID' => false,
            'IBLOCK_ID' => $iTmpIBlockId,
            'PROPERTY_VALUES' => $arPROP,
            'NAME' => $sName,
            'ACTIVE' => $bActive,
            'PREVIEW_TEXT' => $sPREVIEW_TEXT,
            'DETAIL_TEXT' => $sDETAIL_TEXT,
        );

        if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
            echo 'New ID: '.$PRODUCT_ID;
        } else {
            echo 'Error: '.$el->LAST_ERROR;
        }

        $i++;
    }
    
}



require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

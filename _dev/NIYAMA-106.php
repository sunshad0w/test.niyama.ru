<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');


if (!CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')){
	$arFields['ID'] = 'NIYAMA_DISCOUNTS';
	$arFields['SECTIONS'] = 'N';
	$arFields['IN_RSS'] = 'N';
	$arFields['SORT'] = 120;
	$langDefaults = array(
		'ru' => array(
			'NAME'          => 'Скидки',
			'SECTION_NAME'  => 'Разделы',
			'ELEMENT_NAME'  => 'Элементы',
		),
		'en' => array(
			'NAME'          => 'Discounts',
			'SECTION_NAME'  => 'Sections',
			'ELEMENT_NAME'  => 'Elements',
		),
	);	
	$l = CLanguage::GetList($lby="sort", $lorder="asc");
	while($arIBTLang = $l->GetNext())
	{
		if (array_key_exists($arIBTLang["LID"], $langDefaults))
			$arFields["LANG"][$arIBTLang["LID"]] = $langDefaults[$arIBTLang["LID"]];
	}
	$obBlocktype = new CIBlockType;	
	$res = $obBlocktype->Add($arFields);
	if(!$res){
	   echo '<p>Error: '.$obBlocktype->LAST_ERROR.'</p>';
	}
	else{		
		echo '<p>Добавлен тип ИБ: '.$arFields['ID'].'</p>';
	}
}



if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS_USERS'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_CUPONS_USERS',
        'NAME' => "Купоны скидок пользователей",
        'SORT' => 100,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',        
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Купоны',
        'ELEMENT_NAME' => 'Купон',
        'ELEMENT_ADD' => 'Добавить купон',
        'ELEMENT_EDIT' => 'Изменить купон',
        'ELEMENT_DELETE' => 'Удалить купон',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){		
		$fields = CIBlock::getFields($ID);
		$fields["ACTIVE"]["DEFAULT_VALUE"]='N';
		CIBlock::setFields($ID, $fields);
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
//добавление свойств к ИБ "Купоны скидок"
if ((CCustomProject::IfIBTypeByID('NIYAMA_DISCOUNTS')) && ($ID=CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS_USERS'))) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		
		$sTmpPropCode = 'USER_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Пользователь',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'UserID',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'COUPON_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'ID Купона',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '', 			
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'LINK_IBLOCK_ID'=>CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_CUPONS'), 
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'BASE_TYPE_IMG';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип изображения',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'VALUES'		=> array(
									array(
										  "VALUE" => "По умолчанию",
										  "DEF" => "Y",
										  "SORT" => "100",
										  "XML_ID"=>"DEFAULT",
									),
									array(
										  "VALUE" => "Загрузка изображения",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"DOWNLOAD",
									),
									array(
										  "VALUE" => "Изображение блюд/блюда",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"BLUDA",
									)
								)
		);
		

		$sTmpPropCode = 'BASE_TYPE_IMG_DOWNLOAD_H';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Загрузка изображения для горизонтального купона (десктоп)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'FileMan',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_TYPE_IMG_DOWNLOAD_V';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Загрузка изображения для вертикального купона (планшет)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'FileMan',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип бонуса',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Скидка в %",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"DISCOUNT_PERSENT",
									),
									array(
										  "VALUE" => "Скидка в деньгах",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"DISCOUNT_MONEY",
									),	
									array(
										  "VALUE" => "Бонусное блюдо",
										  "DEF" => "N",
										  "SORT" => "300",
										  "XML_ID"=>"BLUDO",
									),																	
								)
		);

		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_PERSENT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Размер скидки (в %)',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Тип скидки в деньгах',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Скидка в ниямах",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"NIYAM",
									),
									array(
										  "VALUE" => "Скидка в деньгах",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"MONEY",
									),																										
								)
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_DISCOUNT_MONEY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Размер скидки',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,	
			
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_BLUDO';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Бонусное блюдо',			
			'PROPERTY_TYPE' => 'E',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> 'bonus_bluds', 			
			'MULTIPLE' 		=> 'Y',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'MULTIPLE_CNT' 	=> 1,			
			'LINK_IBLOCK_ID'=>CProjectUtils::GetIBlockIdByCodeEx('general-site-menu'), 
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
		);
		$sTmpPropCode = 'BASE_BONUS_TYPE_BLUDO_VISIUAL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Как отображать',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'L',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Выводить, как комплект",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"COMPLECT",
									),
									array(
										  "VALUE" => "Выводить отдельными карточками",
										  "DEF" => "N",
										  "SORT" => "200",
										  "XML_ID"=>"CARTS",
									),																										
								)
		);
		$sTmpPropCode = 'BASE_BONUS_NO_SUM';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Не суммировать с другими скидками',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' 	=> 'C',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,			
			'VALUES'		=> array(
									array(
										  "VALUE" => "Да",
										  "DEF" => "N",
										  "SORT" => "100",
										  "XML_ID"=>"Yes",
									)																																		
								)
		);
		$sTmpPropCode = 'BASE_COUNT_ACTION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество полученных купонов этого типа',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,
			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);	
		$sTmpPropCode = 'BASE_QUANITY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество неиспользованных купонов',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);	
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_ORDER';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Заказ',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_AUTHSOC';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Авторизаций через соц сети',   
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_SURVEY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий Участие в опросе',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество совершенных действий За рекомендацию в социальных сетях',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'COUNT_ORDER_BLUDS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'Количество уже купленных блюд',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
		$sTmpPropCode = 'ORDER_ID';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' 			=> 'ID заказа',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' 	=> '',
			'SEARCHABLE' 	=> 'N',
			'USER_TYPE' 	=> '',
			'MULTIPLE' 		=> 'N',
			'ACTIVE' 		=> 'Y',
			'FILTRABLE' 	=> 'Y',
			'SORT' 			=> $iPropsSort,
			'CODE' 			=> $sTmpPropCode,
			'DEFAULT_VALUE'	=> 0,			
	
		);
			
		

		
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $ID));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem;
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if ($arPropMeta['CODE']=='BASE_BONUS_TYPE_BLUDO'){
				$ibp = new CIBlockProperty;				
				if ((!empty($arAddProps[$arPropMeta['CODE']]['ID']))&&($arAddProps[$arPropMeta['CODE']]['USER_TYPE'] != 'bonus_bluds')){
					if(!$ibp->Update($arAddProps[$arPropMeta['CODE']]['ID'], $arPropMeta)){
						echo '<p>Ошибка обновления свойства: ['.$arAddProps[$arPropMeta['CODE']]['ID'].'] '.$arPropMeta['CODE'].': '.$ibp->LAST_ERROR.'</p>'; 
					}else{
						echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']]['ID'].'] '.$arPropMeta['CODE'].'</p>';
					}
				}
			}
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $ID; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
}


if ( !CNiyamaCustomSettings::getStringValue("max_discount_by_order",false) ){
    CNiyamaCustomSettings::setStringValue("Максимальная скидка для закза в %","max_discount_by_order","15");
	echo '<p>Добавлена настройка: Максимальная скидка</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("max_discount_vip_by_order",false) ){
    CNiyamaCustomSettings::setStringValue("Максимальная скидка (VIP клиенты) для  закза в %","max_discount_vip_by_order","45");
	echo '<p>Добавлена настройка: Максимальная скидка для VIP</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("koefficient_niam_by_rub",false) ){
    CNiyamaCustomSettings::setStringValue("Коэффициент перевода ниям в рубли (число ниам равное 1 рублю)","koefficient_niam_by_rub","100");
	echo '<p>Добавлена настройка :Коэффициент перевода ниам в рубли</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("min_sum_order_by_niam",false) ){
    CNiyamaCustomSettings::setStringValue("Минимальная сумма заказа в рублях для того чтобы оплатить часть заказа ниямами","min_sum_order_by_niam","100");
	echo '<p>Добавлена настройка: Минимальная сумма заказа в рублях для ниям</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("max_discount_order_by_niam",false) ){
    CNiyamaCustomSettings::setStringValue("Максимальный % от стоимости заказа который можно оплатить ниямами","max_discount_order_by_niam","10");
	echo '<p>Добавлена настройка: Максимальный % от стоимости заказа который можно оплатить ниямами</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("text_coupon_by_niam",false) ){
    CNiyamaCustomSettings::setStringValue("Текст купона для ниям","text_coupon_by_niam","100 ниям = 1р. Воспользуйтесь при оплате любого заказа");
	echo '<p>Добавлена настройка: Текст купона для ниям</p>';
}
if ( !CNiyamaCustomSettings::getStringValue("text_coupon_by_rub",false) ){
    CNiyamaCustomSettings::setStringValue("Текст купона для рублей","text_coupon_by_rub","Воспользуйтесь при оплате любого заказа");
	echo '<p>Добавлена настройка: Текст купона для рублей</p>';
}

/**
 * Пользовательские поля
 * ======================
 */
$iPropsSort = 100;
$arFields=array();

$iPropsSort += 100;
$fCODE = "COUNT_NIYAM";
$arFields[$fCODE] = array(
    'ENTITY_ID' => 'USER',
    'FIELD_NAME' => 'UF_' . $fCODE,
    "EDIT_FORM_LABEL" => Array("ru" => "Количество бонусов Ниям"),
    'SORT' => $iPropsSort,
    'USER_TYPE_ID' => 'integer',
    'XML_ID' => 'XML_' . $fCODE,
    'SHOW_FILTER' => 'Y',
    "SETTINGS" => array(
    	"DEFAULT_VALUE" => "0"        
    ),
);
$iPropsSort += 100;
$fCODE = "COUNT_RUB";
$arFields[$fCODE] = array(
    'ENTITY_ID' => 'USER',
    'FIELD_NAME' => 'UF_' . $fCODE,
    "EDIT_FORM_LABEL" => Array("ru" => "Количество бонусов рублей"),
    'SORT' => $iPropsSort,
    'USER_TYPE_ID' => 'integer',
    'XML_ID' => 'XML_' . $fCODE,
    'SHOW_FILTER' => 'Y',
    "SETTINGS" => array(
    	"DEFAULT_VALUE" => "0"        
    ),
);
$iPropsSort += 100;
$fCODE = "DISCOUNT";
$arFields[$fCODE] = array(
    'ENTITY_ID' => 'USER',
    'FIELD_NAME' => 'UF_' . $fCODE,
    "EDIT_FORM_LABEL" => Array("ru" => "Постоянная скидка в %"),
    'SORT' => $iPropsSort,
    'USER_TYPE_ID' => 'integer',
    'XML_ID' => 'XML_' . $fCODE,
    'SHOW_FILTER' => 'Y',
    "SETTINGS" => array(
    	"DEFAULT_VALUE" => "0"        
    ),
);

$iPropsSort += 100;
$fCODE = "DISCOUNT_NO_SUM";
$arFields[$fCODE] = array(
    'ENTITY_ID' => 'USER',
    'FIELD_NAME' => 'UF_' . $fCODE,
    "EDIT_FORM_LABEL" => Array("ru" => "Не суммировать постоянную скидку с другими скидками"),
    'SORT' => $iPropsSort,
    'USER_TYPE_ID' => 'boolean',
    'XML_ID' => 'XML_' . $fCODE,
    'SHOW_FILTER' => 'Y',
    "SETTINGS" => array(
        "DEFAULT_VALUE" => "0",
        'DISPLAY' => 'CHECKBOX',
    ),
);

$UserType = new CUserTypeEntity;
foreach ($arFields as $field){
	$ID = $UserType->Add($field);
	if ($ID) {
		echo '<p>Добавлено поле: ' . $field['FIELD_NAME'] . '</p>';
	} else {
		echo '<p>Ошибка добавления поля: ' . $field['FIELD_NAME'] . '</p>';
	}
}

echo "<hr>";
<?
//
// Проверка наличия элементов в основном меню для связей с импортируемым меню
//

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

CModule::IncludeModule('iblock');
$dbItems = CIBlockElement::GetList(
	array(
		'ID' => 'ASC'
	),
	array(
		'IBLOCK_ID' => CNiyamaIBlockCatalogBase::GetIBlockId(),
	),
	false,
	false,
	array(
		'ID', 'NAME'
	)
);
while($arItem = $dbItems->Fetch()) {
	CNiyamaIBlockCatalogBase::AddSiteMenuItem($arItem['ID'], array('NAME' => $arItem['NAME']));
}

echo 'Finished!';

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

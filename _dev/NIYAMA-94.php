<?php

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');

if (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_PROMO-BLOCK')) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_GENERAL_PAGE',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_PROMO-BLOCK',
        'NAME' => "Промо-блок",
        'SORT' => 600,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Элементы промо-блока',
        'ELEMENT_NAME' => 'Элемент промо-блока',
        'ELEMENT_ADD' => 'Добавить элемент промо-блока',
        'ELEMENT_EDIT' => 'Изменить элемент промо-блока',
        'ELEMENT_DELETE' => 'Удалить элемент промо-блока',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID)
    {
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}

echo "<hr>";
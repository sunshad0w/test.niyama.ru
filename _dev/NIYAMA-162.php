<?php

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

/* Проверка на доступ */
if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

/**
 * Пользовательские поля
 * ======================
 */
$fCODE = "SUBSCRIPTION";
$arFields = array(
    'ENTITY_ID' => 'USER',
    'FIELD_NAME' => 'UF_' . $fCODE,
    "EDIT_FORM_LABEL" => Array("ru" => "Cогласен получать рассылку"),
    'SORT' => 100,
    'USER_TYPE_ID' => 'boolean',
    'XML_ID' => 'XML_' . $fCODE,
    'SHOW_FILTER' => 'Y',
    "SETTINGS" => array(
        "DEFAULT_VALUE" => "0",
        'DISPLAY' => 'CHECKBOX',
    ),
);
$UserType = new CUserTypeEntity;
$ID = $UserType->Add($arFields);
if ($ID) {
    echo '<p>Добавлено поле: ' . $arFields['FIELD_NAME'] . '</p>';
} else {
    echo '<p>Ошибка добавления поля: ' . $arFields['FIELD_NAME'] . '</p>';
}


/**
 * Создаем HIB
 * ==================================
 */
$sTmpEntityName = 'UserSoc';
$sTmpEntityTableName = 'niyama_usersoc';
$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
if (!$iTmpEntityId) {
    $obTmpResult = CHLEntity::Add(
        array(
            'NAME' => $sTmpEntityName,
            'TABLE_NAME' => $sTmpEntityTableName
        )
    );

    if ($obTmpResult->IsSuccess()) {
        $iTmpEntityId = $obTmpResult->GetId();
    }

    if ($iTmpEntityId) {
        ?><p>Создана HL-сущность "Связь пользователей с соцеальными сервисами"</p><?
    } else {
        echo '<p>' . implode('<br />', $obTmpResult->GetErrorMessages()) . '</p>';
    }
}

/**
 * Создаем Поля
 * ===================================
 */
if($iTmpEntityId) {
    $arAddFields = array(
        'UF_USER_ID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Пользователь'
        ),

        'UF_UID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'ID в соцеальной сети'
        ),

        'UF_SERVICES' => array(
            'USER_SOC_TYPE' => 'string',
            'NAME' => 'Сервис'
        ),

        'UF_IDENTITY' => array(
        'USER_SOC_TYPE' => 'string',
        'NAME' => 'Идентификатор'
        )
    );

    $arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);

    foreach($arAddFields as $sTmpFieldName => $arAddParams) {
        if(!isset($arTmpCurFields[$sTmpFieldName])) {
            $arTmpParams = array_merge(
                array(
                    'FIELD_NAME' => $sTmpFieldName,
                    'EDIT_FORM_LABEL' => '',
                    'USER_TYPE_ID' => 'string',
                    'MULTIPLE' => 'N',
                    'SHOW_FILTER' => 'I',
                    'SHOW_IN_LIST' => 'Y',
                    'EDIT_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'SIZE' => '50'
                    ),
                    'LIST_COLUMN_LABEL' => '',
                    'LIST_FILTER_LABEL' => '',
                    'ERROR_MESSAGE' => '',
                    'HELP_MESSAGE' => ''
                ),
                $arAddParams
            );
            if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
                $arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
                $arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
                $arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
            }
            $iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
            if($iTmpUserFieldId) {

                if($sTmpFieldName == 'UF_USER_ID') {
                    // изменим тип поля UF_XML_ID с text на varchar(255)
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_USER_ID UF_USER_ID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                } else if($sTmpFieldName == 'UF_UID') {
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_UID UF_UID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                }

                echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
            } else {
                echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
            }
        }
    }
}
echo '<hr />';
<?
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

$iOrderId = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : '';
?><form method="get" action="">
	<label>ID заказа: </label>
	<input type="text" name="id" value="<?=$iOrderId?>" />
	<input type="submit" value="Отправить" />
</form><?

if($iOrderId) {
	$arOrderData = CNiyamaOrders::GetOrderById($iOrderId);
	if($arOrderData['ORDER']) {
		?><p>ID заказа: <?=$arOrderData['ORDER']['ID']?></p><?
		?><p>Статус заказа: <?=$arOrderData['ORDER']['STATUS_ID']?></p><?
		?><p>ID заказа FO: <?=$arOrderData['ORDER']['UF_FO_ORDER_ID']?></p><?
		?><p><b>Код заказа FO</b>: <?=$arOrderData['ORDER']['UF_FO_ORDER_CODE']?></p><?
		?><p>Была отправка в FO: <?=($arOrderData['ORDER']['UF_FO_WAS_SEND'] ? 'Да' : 'Нет')?></p><?
		?><p>Подробная информация о заказе:</p><?
		?><div style="height: 300px; width: 700px; overflow: auto"><?
			_show_array($arOrderData)
		?></div><?

		if($arOrderData['ORDER']['UF_FO_ORDER_CODE']) {
			?><p>Подробная информация о заказе в FO:</p><?
			$obFO = new CNiyamaFastOperator();
			?><br /><div style="height: 300px; width: 700px; overflow: auto"><?
				_show_array($obFO->GetOrdersArray($arOrderData['ORDER']['UF_FO_USER_LOGIN'], $arOrderData['ORDER']['UF_FO_ORDER_CODE']));
			?></div><?
		}
	}
}


require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

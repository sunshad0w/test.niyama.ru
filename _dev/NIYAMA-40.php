<?php

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');


$arFields['ID'] = 'NIYAMA_GENERAL_PAGE';
$arFields['SECTIONS'] = 'N';
$arFields['IN_RSS'] = 'N';
$arFields['SORT'] = 100;
$langDefaults = array(
    'ru' => array(
        'NAME'          => 'Главная страница',
        'SECTION_NAME'  => 'Разделы',
        'ELEMENT_NAME'  => 'Элементы',
    ),
    'en' => array(
        'NAME'          => 'General page',
        'SECTION_NAME'  => 'Sections',
        'ELEMENT_NAME'  => 'Elements',
    ),
);
$l = CLanguage::GetList($lby="sort", $lorder="asc");
while($arIBTLang = $l->GetNext())
{
    if (array_key_exists($arIBTLang["LID"], $langDefaults))
        $arFields["LANG"][$arIBTLang["LID"]] = $langDefaults[$arIBTLang["LID"]];
}
$obBlocktype = new CIBlockType;
$DB->StartTransaction();
$res = $obBlocktype->Add($arFields);
if(!$res)
{
    $DB->Rollback();
    echo '<p>Error: '.$obBlocktype->LAST_ERROR.'</p>';
}
else{
    $DB->Commit();

    unset($arFields);
    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_GENERAL_PAGE',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_SLIDER_TOP',
        'NAME' => "Слайдер",
        'SORT' => 500,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Элементы слайдера',
        'ELEMENT_NAME' => 'Элемент слайдера',
        'ELEMENT_ADD' => 'Добавить элемент слайдера',
        'ELEMENT_EDIT' => 'Изменить элемент слайдера',
        'ELEMENT_DELETE' => 'Удалить элемент слайдера',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID)
    {
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
echo "<hr>";
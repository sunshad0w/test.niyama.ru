<?
return; // подстраховка от ошибочного запуска
//
// Перенос данных о пользователях из временной таблицы в рабочие сущности сайта
//

//if(php_sapi_name() !== "cli") {
//	die('only cli mode');
//}

@ini_set('mbstring.func_overload', 2);
@ini_set('mbstring.internal_encoding', 'UTF-8');

if(!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = realpath(dirname(__FILE__).'/..');
}

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
//define('NOT_CHECK_PERMISSIONS', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if(!$GLOBALS['USER']->IsAdmin() && php_sapi_name() !== 'cli') {
	echo 'Доступ запрещен';
	return;
}

@set_time_limit(0);

$arNewUsersGroups = array();
$iTmpId = CProjectUtils::GetUsersGroupByCode('REGISTERED_USERS');
if($iTmpId) {
	$arNewUsersGroups[] = $iTmpId;
}
$iTmpId = CProjectUtils::GetUsersGroupByCode('RATING_VOTE');
if($iTmpId) {
	$arNewUsersGroups[] = $iTmpId;
}
$iTmpId = CProjectUtils::GetUsersGroupByCode('RATING_VOTE_AUTHORITY');
if($iTmpId) {
	$arNewUsersGroups[] = $iTmpId;
}

$iIBlockIdMedals = CProjectUtils::GetIBlockIdByCode('users_medals', 'NIYAMA_DISCOUNTS');

// название на старом сайте => название на новом сайте
$arMedalsPivot = array(
	'За преданность' => 'За преданность',
	'Пехоты' => 'МЕДАЛЬ ПЕХОТЫ',
	'За отвагу' => 'ЗА ОТВАГУ',
	'Супа-стар' => 'Медаль супа-стар',
	'За любовь к острым ощущениям' => 'За любовь к острым ощущениям',
	'За вклад в сушиедство' => 'За вклад в сушиедство',
	'Некоторые любят по-горячее' => 'Некоторые любят погорячее',
	'За поедание лосося' => 'За поедание лосося',
	'Четыре сыра' => 'Четыре сыра',
	'I LOVE NY' => 'I LOVE NY',
	'Энергетику' => 'Энергетику',
	'Звезда Дурова-Цукерберга' => 'Звезда Дурова-Цукерберга',
	'Ночной дозор' => 'Ночной дозор',
	'За взятие сета' => 'За взятие сета',
	'Утренней пташки' =>  'Утренней пташки',
	'Юбилейная' => 'Юбилейная'
);

$arLevels2DiscPerc = array(
	'0' => '0',
	'1' => '5',
	'2' => '6',
	'3' => '7',
	'4' => '8',
	'5' => '9',
	'6' => '10',
	'7' => '11',
	'8' => '12',
	'9' => '13',
	'10' => '14',
	'11' => '15'
);

$arUnknownMedals = array();

try {

	$parameters = array(
		'filter' => array(
			''
		),
		'select' => array('*'),
	);

	$sCurLogDate = date('d-m-Y_H-i-s');
	$iCnt = 0;
	$dbTmpUsers = TmpUsersTable::GetList($parameters);
	while($arItem = $dbTmpUsers->Fetch()) {

		if(!check_email($arItem['UF_TMP_EMAIL'])) {
			continue;
		}

		$arLog = array(
			'N' => ++$iCnt, 
			'ID' => $arItem['ID'], 
			'EMAIL' => $arItem['UF_TMP_EMAIL']
		);

		$obUser = new CUser();

		$arUser = CUser::GetByLogin($arItem['UF_TMP_EMAIL'])->Fetch();

		$sFirstName = $arItem['UF_TMP_NAME'];
		$sLastName = $arItem['UF_TMP_SECOND_NAME'];
		if(!strlen($sLastName)) {
			$arTmp = explode(' ', $arItem['UF_TMP_NAME']);
			if(count($arTmp) > 1) {
				$sLastName = trim($arTmp[1]);
			}
		}

		if(strlen($arItem['UF_TMP_BIRTH_DATE'])) {
			$sDateBeafore = $arItem['UF_TMP_BIRTH_DATE'];
			$arItem['UF_TMP_BIRTH_DATE'] = CProjectUtils::GetDateTimeFieldFilter($arItem['UF_TMP_BIRTH_DATE'], false);
		}

		$GLOBALS['DB']->StartTransaction();

		$dDiscountVal = 0;
		if(!$arUser) {
			//
			// Добавляем пользователя
			//
			$sPassword = CProjectUtils::GenPassword(8);
			$arFields = array(
				'ACTIVE' => 'N',
				'LOGIN' => $arItem['UF_TMP_EMAIL'],
				'NAME' => $sFirstName,
				'LAST_NAME' => $sLastName,
				'EMAIL' => $arItem['UF_TMP_EMAIL'],
				'GROUP_ID' => $arNewUsersGroups,
				'PASSWORD' => $sPassword,
				'CONFIRM_PASSWORD' => $sPassword,
				//'PERSONAL_GENDER' => $arItem['UF_TMP_GANDER'] = 'male' ? 'M' : 'F',
				'PERSONAL_PHONE' => $arItem['UF_TMP_PHONE'],
				'UF_COUNT_NIYAM' => $arItem['UF_TMP_NIYAMA'],
				'PERSONAL_BIRTHDAY' => $arItem['UF_TMP_BIRTH_DATE'],
				// значение check_old_site_auth нужно как флаг, указывающий на необходимость проверки авторизации на старом сайте
				'XML_ID' => 'check_old_site_auth',
				// отключим показ модального окна при первом логине
				'ADMIN_NOTES' => 'sm',
				// установим сразу маркер, что юзер синхронизирован с FO
				'UF_MARKER' => 'Y',
				// отключаем отражение в таймлайне и синхронизацию данных с FO
				'_NIYAMA_IGNORE_TIMELINE_' => 'Y',
				'_SKIP_FO_SYNC_' => 'Y',

			);
			$iImportUserId = $obUser->Add($arFields);
			$arLog['NEW_USER_ID'] = $iImportUserId;
			if(!$iImportUserId) {
				throw new Exception($obUser->LAST_ERROR);
			}
		} else {
			$iImportUserId = $arUser['ID'];

			$arFields = array(
				'NAME' => $sFirstName,
				'LAST_NAME' => $sLastName,
				'PERSONAL_PHONE' => $arItem['UF_TMP_PHONE'],
				'UF_COUNT_NIYAM' => $arItem['UF_TMP_NIYAMA'],
				'PERSONAL_BIRTHDAY' => $arItem['UF_TMP_BIRTH_DATE'],
				// значение check_old_site_auth нужно как флаг, указывающий на необходимость проверки авторизации на старом сайте
				'XML_ID' => $arUser['ACTIVE'] == 'Y' ? $arUser['XML_ID'] : 'check_old_site_auth',
				// отключим показ модального окна при первом логине
				'ADMIN_NOTES' => 'sm',
				// установим сразу маркер, что юзер синхронизирован с FO
				'UF_MARKER' => 'Y',
				// отключаем отражение в таймлайне и синхронизацию данных с FO
				'_NIYAMA_IGNORE_TIMELINE_' => 'Y',
				'_SKIP_FO_SYNC_' => 'Y',
			);

			$arLog['UPDATE_USER_ID'] = $iImportUserId;
			$obUser->Update($iImportUserId, $arFields);
		}

		if($arItem['UF_TMP_LEVEL'] && $arLevels2DiscPerc[$arItem['UF_TMP_LEVEL']]) {
			$dDiscountVal = $arLevels2DiscPerc[$arItem['UF_TMP_LEVEL']];
		}

		//
		// Привязка карты
		//
		$sCardCode = intval($arItem['UF_TMP_CARD_NUMBER']) > 0 ? trim($arItem['UF_TMP_CARD_NUMBER']) : '';
		if(strlen($sCardCode)) {
			$arLog['DISCOUNT_CARD'] = $sCardCode;
			$arLog['DISCOUNT_CARD_STATUS'] = 'DISCOUNT_CARD_ERROR';

			$arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sCardCode);
			if($arPdsCardInfo && !$arPdsCardInfo['_ERRORS_']) {
				$bUpdateIfExists = true;
				$bCompareUpdateFields = false;
				$iTmpCardElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arPdsCardInfo, $iImportUserId, array(), $bUpdateIfExists, $bCompareUpdateFields);
				if($iTmpCardElementId) {
					$arLog['DISCOUNT_CARD_STATUS'] = 'DISCOUNT_CARD_SUCCESS';
					$arLog['DISCOUNT_CARD_ELEMENT_ID'] = $iTmpCardElementId;
				} else {
					$arLog['DISCOUNT_CARD_ERROR_INFO'] = 'Не удалось добавить/обновить элемент с данными по дисконтной карте'."\n".print_r($arPdsCardInfo, true);
				}
				if($arPdsCardInfo['Account']['Discount']) {
					$dTmpVal = CNiyamaIBlockCardData::GetTrueDiscount($arPdsCardInfo['Account']['Discount']);
					$dDiscountVal = $dDiscountVal > $dTmpVal ? $dDiscountVal : $dTmpVal;
				}
			} else {
				$arLog['DISCOUNT_CARD_ERROR_INFO'] = $arPdsCardInfo;
			}
		}

		// установим достижение за регистрацию для отображения в таймлайне, но не будем давать бонусы
		$bNoBonus = true;
		CNiyamaDiscountLevels::UpdateUserRegistrationLevel($iImportUserId, $bNoBonus, array('_SKIP_SET_DISCOUNT_CARD_VALUE_' => false));

		if($iImportUserId && $dDiscountVal) {
			// не начисляем бонусы
			$bNoBonus = true;
			// установим достигнутый уровень скидки, попытаемся сразу обновить значение скидки и в ПДС (если привязана карта)
			CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iImportUserId, $dDiscountVal, $bNoBonus, array('_SKIP_SET_DISCOUNT_CARD_VALUE_' => false));

			// если нет карты (или она не добавилась), то установим величину скидки юзеру напрямую в карточку
			$GLOBALS['USER_FIELD_MANAGER']->Update('USER', $iImportUserId, array('UF_DISCOUNT' => intval($dDiscountVal)));

			$arLog['DISCOUNT_VAL'] = $dDiscountVal;
		}

		//
		// Сохранение медалей
		//
		if(!empty($arItem['UF_TMP_MEDALS'])) {
			$arMedalsTmp = explode(';', $arItem['UF_TMP_MEDALS']);
			foreach($arMedalsTmp as $sMedalName) {
				$sMedalName = trim($sMedalName);
				if(strlen($sMedalName)) {
					$arMedal = CNiyamaMedals::GetMedalByName($sMedalName);
					if(!$arMedal && $arMedalsPivot[$sMedalName]) {
						$arMedal = CNiyamaMedals::GetMedalByName($arMedalsPivot[$sMedalName]);
					}
					if($arMedal) {
						// костылик, чтобы не начислялись бонусы за получение медали
						CEventHandlers::SetSkipMedalEvent($arMedal['ID'], $iImportUserId);
						$iTmpUserMedalId = CNiyamaMedals::SetMedal($arMedal['ID'], $iImportUserId, array(), true);
						$arLog['MEDALS'][] = $arMedal['ID'].' || '.$iTmpUserMedalId;
					} else {
						if(!isset($arUnknownMedals[$sMedalName])) {
							$arUnknownMedals[$sMedalName] = 0;
						}
						$arUnknownMedals[$sMedalName] += 1;
					}
				}
			}
		}

		// Записываем в соцсети
		// Не делаем, возможно, устарело требование
		/*
		if(!empty($arItem['UF_TMP_SOCIAL'])) {
			$arSocDecode = @unserialize($arItem['UF_TMP_SOCIAL']);
			if($arSocDecode) {
				if(is_array($arSocDecode)) {
					foreach($arSocDecode as $soc => $uid) {
						if(!empty($uid) && strtolower($uid) != 'none') {
							$arSocData = array(
								'UF_USER_ID' => $ID,
								'UF_UID' => $uid,
								'UF_SERVICES' => $soc,
								'UF_IDENTITY' => ''
							);
							UsersocTable::add($arSocData);
						}
					}
				}
			}
		}
		*/

		TmpUsersTable::Update($arItem['ID'], array('UF_XML_ID' => date('d.m.Y H:i:s')));

		$GLOBALS['DB']->Commit();
		_log_array($arLog, 'merge_user_'.$sCurLogDate.'.log');
	}

} catch (Exception $e) {
	$GLOBALS['DB']->Rollback();
	_log_array(
		array(
			'error_text' => $e->getMessage(),
			'file' => $e->getFile(),
			'line' => $e->getLine(),
		), 
		'merge_users_err.log'
	);
}

if($arUnknownMedals) {
	_log_array($arUnknownMedals, '$arUnknownMedals');
}

BXClearCache(true);

echo 'Finish';

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

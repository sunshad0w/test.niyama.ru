<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

$sNiyamaLsvVer = COption::GetOptionString('main', 'niyama_lsv_new_ver', '', '-');
//$sNiyamaLsvVer = '';

if($sNiyamaLsvVer < '2015-09-10-001') {
	$sNiyamaLsvVer = '2015-09-10-001';
	//
	// ---
	//
	CModule::IncludeModule('iblock');

	echo '<h1>'.basename(__FILE__).' - '.$sNiyamaLsvVer.'</h1>';

	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCode('url_redirect', 'settings');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Ссылки для перенаправления',
			'CODE' => 'url_redirect',
			'XML_ID' => 'url_redirect',
			'IBLOCK_TYPE_ID' => 'settings',
			'SITE_ID' => 's1',
			'SORT' => '5000',
			'GROUP_ID' => array(
				'2' => 'R', 
				'8' => 'W',
			),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'WORKFLOW' => 'N',
			'VERSION' => 2,
			'FIELDS' => array(
				'CODE' => array(
					'IS_REQUIRED' => 'N', // Обязательное
					'DEFAULT_VALUE' => array(
						'UNIQUE' => 'Y', // Проверять на уникальность
						'TRANSLITERATION' => 'N', // Транслитерировать
						'TRANS_LEN' => '100', // Максмальная длина транслитерации
						'TRANS_CASE' => 'L', // Приводить к нижнему регистру
						'TRANS_SPACE' => '-', // Символы для замены
						'TRANS_OTHER' => '-',
						'TRANS_EAT' => 'Y',
						'USE_GOOGLE' => 'N',
					),
				),
				'SECTION_CODE' => array(
					'IS_REQUIRED' => 'N', // Обязательное
					'DEFAULT_VALUE' => array(
						'UNIQUE' => 'Y', // Проверять на уникальность
						'TRANSLITERATION' => 'Y', // Транслитерировать
						'TRANS_LEN' => '100', // Максмальная длина транслитерации
						'TRANS_CASE' => 'L', // Приводить к нижнему регистру
						'TRANS_SPACE' => '-', // Символы для замены
						'TRANS_OTHER' => '-',
						'TRANS_EAT' => 'Y',
						'USE_GOOGLE' => 'N',
					),
				),
			),
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "<?=$arFields['NAME']?>"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "<?=$arFields['NAME']?>": <?=$obIBlock->LAST_ERROR?></p><?
			return;
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'INCOMING_URL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Входящий URL',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => '1',
			'COL_COUNT' => '80',
			'HINT' => ''
		);

		// ---
		$sTmpPropCode = 'REDIRECT_URL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'URL куда перенаправлять',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => '1',
			'COL_COUNT' => '80',
		);
		// ---
		$sTmpPropCode = 'REDIRECT_STATUS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Код статуса перенаправления',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => '1',
			'COL_COUNT' => '30',
			'HINT' => '',
			'VALUES' => array(
				array(
					'XML_ID' => '301',
					'VALUE' => '301 Moved Permanently',
					'DEF' => 'Y',
					'SORT' => 100
				),
				array(
					'XML_ID' => '302',
					'VALUE' => '302 Found',
					'DEF' => 'N',
					'SORT' => 200
				),
			)
		);

		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 3500;
		// ---
		$sTmpPropCode = 'CANONICAL_ITEM';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Каноническое блюдо',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => '1',
			'COL_COUNT' => '80',
			'HINT' => 'Укажите блюдо, для которого текущее блюдо является синонимом',
			'LINK_IBLOCK_ID' => $iTmpIBlockId
		);

		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}

	}


}

COption::SetOptionString('main', 'niyama_lsv_new_ver', $sNiyamaLsvVer, '', '-');

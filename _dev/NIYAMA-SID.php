<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}
CModule::IncludeModule('iblock');

//
// Типы меню
//
SetMenuTypes(array(
	'top' => 'Главное меню (сверху)',
	'bottom' => 'Нижнее меню (футер)',
));

/**
 * Дополнем Персон
 * ===============
 *
 */
$iGuestIblockId = CProjectUtils::GetIBlockIdByCodeEx("IB_NIYAMA_PERSON");
if ($iGuestIblockId){
    
    $sTmpPropCode = 'USER_ID';
    $iPropsSort += 100;
    $arProductSIDPropertiesMetaGuest[$sTmpPropCode] = array(
        'NAME' => 'Пользователь',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'UserID',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'N',
    );

    $sTmpPropCode = 'FUSER_ID';
    $iPropsSort += 100;
    $arProductSIDPropertiesMetaGuest[$sTmpPropCode] = array(
        'NAME' => 'Привязка к корзине',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'Y',
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iGuestIblockId));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arProductSIDPropertiesMetaGuest as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iGuestIblockId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
    
}

/**
 * Дополнем Ингредиентов
 * ===============
 */
$iIngIblockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-ingredients', 'catalog');
if ($iIngIblockId){

    $sTmpPropCode = 'IS_POPULAR';
    $iPropsSort += 100;
    $arTmpIngProps[$sTmpPropCode] = array(
        'NAME' => 'Популярный',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'Y',
        'DEFAULT_VALUE' => 0
    );
	$sTmpPropCode = 'IS_HIDDEN';
    $iPropsSort += 100;
    $arTmpIngProps[$sTmpPropCode] = array(
        'NAME' => 'Скрытый',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'N',
        'DEFAULT_VALUE' => 0
    );
    $sTmpPropCode = 'TP_NAME';
    $iPropsSort += 100;
    $arTmpIngProps[$sTmpPropCode] = array(
        'NAME' => 'Название (Кем? Чем?)',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iIngIblockId));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpIngProps as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iIngIblockId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }

}


// Рекомендуем
$iTmpRecComponentIblock = CProjectUtils::GetIBlockIdByCodeEx('recommended', 'catalog');
if (!$iTmpRecComponentIblock){
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Рекомендуем',
        'CODE' => 'recommended',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpRecComponentIblock = $obIBlock->Add($arFields);
    if($iTmpRecComponentIblock) {
        ?><p>Создан инфоблок "Рекомендуем"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Рекомендуем": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if ($iTmpRecComponentIblock){

    $arTmpPropertiesMetaRec = array();
    $iPropsSort = 400;

    $sTmpPropCode = 'PRODUCT_ID';
    $iPropsSort  += 100;
    $arTmpPropertiesMetaRec[$sTmpPropCode] = array(
        'NAME' => 'Блюдо',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog')
    );

    $sTmpPropCode = 'PRODUCTS';
    $iPropsSort  += 100;
    $arTmpPropertiesMetaRec[$sTmpPropCode] = array(
        'NAME' => 'Список рекомендуемых блюд',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'rec_products',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog')
    );

    // список текущих свойств инфоблока
    $arAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpRecComponentIblock));
    while($arItem = $dbItems->Fetch()) {
        $arAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpPropertiesMetaRec as $arPropMeta) {
        if(!isset($arAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpRecComponentIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}




$iTmpDopComponentIblock = CProjectUtils::GetIBlockIdByCodeEx('additional_components', 'catalog');
if(!$iTmpDopComponentIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Дополнительные компоненты',
        'CODE' => 'additional_components',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpDopComponentIblock = $obIBlock->Add($arFields);
    if($iTmpDopComponentIblock) {
        ?><p>Создан инфоблок "Дополнительные компоненты"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Дополнительные компоненты": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpDopComponentIblock) {

    $arTmpPropertiesMetaDopComp = array();
    $iPropsSort = 400;
    // ---
    $sTmpPropCode = 'UNIT_TYPE';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Единица измерения компонента',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'VALUES' => array(
            array(
                'XML_ID' => 'TYPE_1',
                'VALUE' => 'Шт.',
                'DEF' => 'Y',
                'SORT' => 100
            ),
            array(
                'XML_ID' => 'TYPE_2',
                'VALUE' => 'Порц.',
                'DEF' => 'N',
                'SORT' => 200
            ),
            array(
                'XML_ID' => 'TYPE_3',
                'VALUE' => 'Компл.',
                'DEF' => 'N',
                'SORT' => 300
            ),
        )
    );

    $sTmpPropCode = 'CALC_ALG';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Алгоритм расчета бесплатного количества компонента',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'IS_REQUIRED' => 'Y',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'VALUES' => array(
            array(
                'XML_ID' => 'ALG_1',
                'VALUE' => 'За каждое блюдо',
                'DEF' => 'Y',
                'SORT' => 100
            ),
            array(
                'XML_ID' => 'ALG_2',
                'VALUE' => 'По количеству персон',
                'DEF' => 'N',
                'SORT' => 200
            ),
            array(
                'XML_ID' => 'ALG_3',
                'VALUE' => 'По количеству для блюда ',
                'DEF' => 'N',
                'SORT' => 300
            ),
        )
    );

    $sTmpPropCode = 'SELL';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Продавать дополнительно',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    $sTmpPropCode = 'FREE_IMPORT_ELEMENT';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Данные бесплатного компонента',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'import_menu_element',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE'   => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'HINT' => "Привязка к товару из импортируемого меню с нулевой ценой, для дальнейшего определение его в системе FO как бесплатный компонент",
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('catalog-base', 'catalog')
    );

    $sTmpPropCode = 'PAID_IMPORT_ELEMENT';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Данные платного компонента',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'import_menu_element',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE'   => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'HINT' => "Привязка к товару из импортируемого меню с определенной ценой, для дальнейшего определение его в системе FO как платный компонент",
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('catalog-base', 'catalog')
    );

    $sTmpPropCode = 'PRICE';
    $iPropsSort += 100;
    $arTmpPropertiesMetaDopComp[$sTmpPropCode] = array(
        'NAME' => 'Стоимость компонента за 1 шт.',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // список текущих свойств инфоблока
    $arAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpDopComponentIblock));
    while($arItem = $dbItems->Fetch()) {
        $arAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpPropertiesMetaDopComp as $arPropMeta) {
        if(!isset($arAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpDopComponentIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


/**
 * Дополнем в Блюдо
 * ===============
 */
$iProductIblockId = CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog');
if ($iProductIblockId){

    # Добавляем, кол-во шт в блюде.
    $sTmpPropCode = 'NUM_OF_PIECES';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Кол-во шт. в блюде',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    # Количество калорий в порции
    $sTmpPropCode = 'NUM_OF_CALORIES';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Кол-во кал. в блюде',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    # Маржинальность Блюда YesNo
    $sTmpPropCode = 'MARGINALITY';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Маржинальность',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    # Добавляем, Вес мфржинальности.
    $sTmpPropCode = 'MARGINALITY_WEIGHT';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Вес маржинальности',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    # Дополнительная информация о блюде
    $sTmpPropCode = 'OTHER_DESCRIPTION';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Дополнительная информация о блюде',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'HTML',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );


    $sTmpPropCode = 'RELATED_PRODUCTS';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Похожие блюда',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'related_products',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'N',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog')
    );


    $sTmpPropCode = 'DOP_COMPONENT';
    $iPropsSort += 100;
    $arProductSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Дополнительные компоненты',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => $iTmpDopComponentIblock,
        'IS_REQUIRED' => 'N',
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iProductIblockId));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arProductSIDPropertiesMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iProductIblockId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


$iTmpModalsIblock = CProjectUtils::GetIBlockIdByCodeEx('modals', 'settings');
if(!$iTmpModalsIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Поздравления за новый уровень',
        'CODE' => 'modals',
        'IBLOCK_TYPE_ID' => 'settings',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpModalsIblock = $obIBlock->Add($arFields);
    if($iTmpModalsIblock) {
        ?><p>Создан инфоблок "Поздравления за новый уровень"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Поздравления за новый уровень": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpModalsIblock) {

    $arModelSIDPropertiesMeta = array();
    $iPropsSort = 400;

    $sTmpPropCode = 'TYPE';
    $iPropsSort += 100;
    $arModelSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Тип информационных сообщений',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'IS_REQUIRED' => 'Y',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'VALUES' => array(
            array(
                'XML_ID' => 'TYPE_1',
                'VALUE' => 'За скидку',
                'DEF' => 'Y',
                'SORT' => 100
            ),
            array(
                'XML_ID' => 'TYPE_2',
                'VALUE' => 'За медаль',
                'DEF' => 'N',
                'SORT' => 200
            ),
            array(
                'XML_ID' => 'TYPE_3',
                'VALUE' => 'За регистрацию',
                'DEF' => 'N',
                'SORT' => 300
            ),
        )
    );

    $sTmpPropCode = 'VIDEO';
    $iPropsSort += 100;
    $arModelSIDPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Видео',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'video',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpModalsIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arModelSIDPropertiesMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpModalsIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


/**
 * Адреса доставки
 * ==================================
 */
$iTmpAddressIblock = CProjectUtils::GetIBlockIdByCodeEx('delivery_address', 'NIYAMA_FOR_USER');
if(!$iTmpAddressIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Адреса доставки',
        'CODE' => 'delivery_address',
        'IBLOCK_TYPE_ID' => 'NIYAMA_FOR_USER',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpAddressIblock = $obIBlock->Add($arFields);
    if($iTmpAddressIblock) {
        ?><p>Создан инфоблок "Адреса доставки"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Адреса доставки": <?=$obIBlock->LAST_ERROR?></p><?
    }
}
if($iTmpAddressIblock) {
    $arTmpAddressMeta = array();

    $sTmpPropCode = 'USER_ID';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Пользователь',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'UserID',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'Y',
    );


    // Населенный пункт
    $sTmpPropCode = 'REGION';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Населенный пункт',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE'     => 'L',
        'SEARCHABLE'    => 'N',
        'USER_TYPE'     => '',
        'IS_REQUIRED'   => 'Y',
        'MULTIPLE'      => 'N',
        'ACTIVE'        => 'Y',
        'FILTRABLE'     => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        /*'VALUES' => array(
            array(
                'XML_ID' => 'REGION_1',
                'VALUE' => 'Москва',
                'DEF' => 'Y',
                'SORT' => 100
            ),
            array(
                'XML_ID' => 'REGION_2',
                'VALUE' => 'Московская область',
                'DEF' => 'N',
                'SORT' => 200
            )
        )*/
    );
	
	 // город другой
    $sTmpPropCode = 'CITY_DOP';
    $iPropsSort += 50;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Город (другой)',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE'     => 'L',
        'SEARCHABLE'    => 'N',
        'USER_TYPE'     => '',
        'IS_REQUIRED'   => 'N',
        'MULTIPLE'      => 'N',
        'ACTIVE'        => 'Y',
        'FILTRABLE'     => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
       
    );

    // Станция метро
    $sTmpPropCode = 'SUBWAY';
    $iPropsSort += 50;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Станция метро',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('subway', 'data'),
        'IS_REQUIRED' => 'N',
    );


    $sTmpPropCode = 'ADDRESS';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Почтовый адрес: улица, дом ',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );
	$sTmpPropCode = 'HOUSE';
    $iPropsSort += 50;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Дом ',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );
	$iPropsSort += 50;
	 
	$sTmpPropCode = 'WITHIN_MKAD';
	$iPropsSort += 100;
	$arTmpAddressMeta[$sTmpPropCode] = array(
		'NAME' => 'В пределах МКАД',
		'PROPERTY_TYPE' => 'N',
		'LIST_TYPE' => 'L',
		'SEARCHABLE' => 'N',
		'USER_TYPE' => 'YesNo',
		'MULTIPLE' => 'N',
		'ACTIVE' => 'Y',
		'FILTRABLE' => 'Y',
		'SORT' => $iPropsSort,
		'CODE' => $sTmpPropCode,			
		'IS_REQUIRED' => 'N',
	);

    $sTmpPropCode = 'HOME';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Квартира',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    $sTmpPropCode = 'PORCH';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Подъезд, домофон, этаж',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );
	$sTmpPropCode = 'INTERCOM';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Домофон,',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );
	$sTmpPropCode = 'FLOOR';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Этаж',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    $sTmpPropCode = 'IS_DEFAULT';
    $iPropsSort += 100;
    $arTmpAddressMeta[$sTmpPropCode] = array(
        'NAME' => 'Выбрать по умолчанию',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'N',
        'DEFAULT_VALUE' => 0
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpAddressIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem;
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpAddressMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpAddressIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }else{
			if (($arPropMeta['CODE']=='SUBWAY')&&($arSidAddProps[$arPropMeta['CODE']]['PROPERTY_TYPE']!=$arPropMeta['PROPERTY_TYPE'])){
				$obIBlockProperty = new CIBlockProperty();
				if(!$obIBlockProperty->Update($arSidAddProps[$arPropMeta['CODE']]['ID'], $arPropMeta)){
    				echo $ibp->LAST_ERROR; 
				}else{
					echo '<p>Обновлено свойство: ['.$arSidAddProps[$arPropMeta['CODE']]['ID'].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
    }
}


/**
 * Данные о карте пользователя
 * ==================================
 */
$iTmpCardIblock = CProjectUtils::GetIBlockIdByCodeEx('card_data', 'NIYAMA_FOR_USER');
if(!$iTmpCardIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Данные карты',
        'CODE' => 'card_data',
        'IBLOCK_TYPE_ID' => 'NIYAMA_FOR_USER',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpCardIblock = $obIBlock->Add($arFields);
    if($iTmpCardIblock) {
        ?><p>Создан инфоблок "Данные карты"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Данные карты": <?=$obIBlock->LAST_ERROR?></p><?
    }
}
if ($iTmpCardIblock) {
    $arTmpCardMeta = array();

    $sTmpPropCode = 'USER_ID';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Пользователь',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'UserID',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'Y',
    );

    // Номер карты
    $sTmpPropCode = 'CARDNUM';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Номер карты',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Имя
    $sTmpPropCode = 'NAME';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Имя',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Фамилия
    $sTmpPropCode = 'LAST_NAME';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Фамилия',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'Y',
    );

    // Дата рождения
    $sTmpPropCode = 'BIRTHDAY';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Дата рождения',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Телефон
    $sTmpPropCode = 'PHONE';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Телефон',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // EMAIL
    $sTmpPropCode = 'EMAIL';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'EMAIL',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Город
    $sTmpPropCode = 'CITY';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Город',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // 	Улица
    $sTmpPropCode = 'STREET';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Улица',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Дом
    $sTmpPropCode = 'HOME';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Дом',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Корпус
    $sTmpPropCode = 'HOUSING';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Корпус',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Строение
    $sTmpPropCode = 'BUILDING';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Строение',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // Квартира
    $sTmpPropCode = 'APARTMENT';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Квартира',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );


    // Скика
    $sTmpPropCode = 'DISCOUNT';
    $iPropsSort += 100;
    $arTmpCardMeta[$sTmpPropCode] = array(
        'NAME' => 'Скидка',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpCardIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpCardMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpCardIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}

if ( !CNiyamaCustomSettings::getStringValue("discount_32",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_32","5");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_34",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_34","6");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_35",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_35","7");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_36",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_36","8");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_37",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_37","9");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_38",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_38","10");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_40",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_40","11");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_41",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_41","12");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_42",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_42","13");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_43",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_43","14");
}

if ( !CNiyamaCustomSettings::getStringValue("discount_44",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие кодов и скидок","discount_44","15");
}


/**
 * Соответсвие кодов и скидок
 */
if ( !CNiyamaCustomSettings::getStringValue("true_discount_5",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_5","32");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_6",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_6","34");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_7",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_7","35");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_8",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_8","36");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_9",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_9","37");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_10",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_10","38");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_11",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_11","40");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_12",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_12","41");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_13",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_13","42");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_14",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_14","43");
}
if ( !CNiyamaCustomSettings::getStringValue("true_discount_15",false) ){
    CNiyamaCustomSettings::setStringValue("соответствие скидок и кодов","true_discount_15","44");
}

$iTmpCartLineIblock = CProjectUtils::GetIBlockIdByCodeEx('cartline', 'customer_loyalty');
if(!$iTmpCartLineIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Уровни скидок в корзине',
        'CODE' => 'cartline',
        'IBLOCK_TYPE_ID' => 'customer_loyalty',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpCartLineIblock= $obIBlock->Add($arFields);
    if($iTmpCartLineIblock) {
        ?><p>Создан инфоблок "Уровни скидок в корзине"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Уровни скидок в корзине": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpCartLineIblock) {
    $arTmpCartLimeMeta = array();

    $sTmpPropCode = 'CUPONBG_VERTICAL';
    $iPropsSort += 100;
    $arTmpCartLimeMeta[$sTmpPropCode] = array(
        'NAME' => 'Картинка для купона (вертикальная)',
        'PROPERTY_TYPE' => 'F',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    $sTmpPropCode = 'CUPONBG_HORIZONTAL';
    $iPropsSort += 100;
    $arTmpCartLimeMeta[$sTmpPropCode] = array(
        'NAME' => 'Картинка для купона (Горизонтальня)',
        'PROPERTY_TYPE' => 'F',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpCartLineIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpCartLimeMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpCartLineIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


$iTmpActionsIblock = CProjectUtils::GetIBlockIdByCodeEx('actions', 'misc');
if(!$iTmpActionsIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Акции',
        'CODE' => 'actions',
        'IBLOCK_TYPE_ID' => 'misc',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '#SITE_DIR#promos/',
        'SECTION_PAGE_URL' => '#SITE_DIR#promos/',
        'DETAIL_PAGE_URL' => '#SITE_DIR#promos/#ELEMENT_ID#.php',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpActionsIblock= $obIBlock->Add($arFields);
    if($iTmpActionsIblock) {
        ?><p>Создан инфоблок "Акции"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Акции": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpActionsIblock) {
    $arTmpActionsMeta = array();

    $sTmpPropCode = 'F_PHOTOS';
    $iPropsSort += 100;
    $arTmpActionsMeta[$sTmpPropCode] = array(
        'NAME' => 'Изображения',
        'PROPERTY_TYPE' => 'F',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpActionsIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpActionsMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpActionsIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


$iPropsSort = 100;
$fCODE = "MARKER";

$aUserFields    = array(
    'ENTITY_ID'         => 'USER',
    'FIELD_NAME'        => 'UF_'.$fCODE ,
    'USER_TYPE_ID'      => 'string',
    'XML_ID'            => 'UF_'.$fCODE ,
    'SORT'              => $iPropsSort,
    'MULTIPLE'          => 'N',
    'MANDATORY'         => 'N',
    'SHOW_FILTER'       => 'N',
    'SHOW_IN_LIST'      => '',
    'EDIT_IN_LIST'      => '',
    'IS_SEARCHABLE'     => 'N',
    'SETTINGS'          => array(
        'DEFAULT_VALUE' => 'N',
        'SIZE'          => '20',
        'ROWS'          => '1',
        'MIN_LENGTH'    => '0',
        'MAX_LENGTH'    => '0',
        'REGEXP'        => '',
    ),
    'EDIT_FORM_LABEL'   => array(
        'ru'    => 'Маркер синхронизации с FO',
        'en'    => 'Keywords',
    ),
    'LIST_COLUMN_LABEL' => array(
        'ru'    => 'Маркер синхронизации с FO',
        'en'    => 'Keywords',
    ),
    /* Подпись фильтра в списке */
    'LIST_FILTER_LABEL' => array(
        'ru'    => 'Маркер синхронизации с FO',
        'en'    => 'Keywords',
    ),
    /* Помощь */
    'HELP_MESSAGE'      => array(
        'ru'    => 'Маркер синхронизации с системой FO',
        'en'    => '',
    ),
);

$UserType = new CUserTypeEntity;
    $ID = $UserType->Add($aUserFields);
    if ($ID) {
        echo '<p>Добавлено поле: FO_REGISTER_MARKER</p>';
    } else {
        echo '<p>Ошибка добавления поля: FO_REGISTER_MARKER</p>';
    }



$arAddAgents = array(
    'CAgentHandlers::SyncUserWithFO();' => array(
        'module' => 'iblock',
        'period' => 'N',
        'interval' => 86400,
        'datecheck' => '',
        'active' => 'Y',
        'next_exec' => '',
        'sort' => 1000
    ),
);
$dbItems = CAgent::GetList();
while($arItem = $dbItems->Fetch()) {
    if(isset($arAddAgents[$arItem['NAME']])) {
        unset($arAddAgents[$arItem['NAME']]);
    }
}
foreach($arAddAgents as $sAgentName => $arTmpParams) {
    $iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
    if($iNewAgentId) {
        echo '<p>Добавлен агент '.$sAgentName.'</p>';
    }
}


//$sTmpPropCode = 'USER_ID';
//$iPropsSort += 100;
//$arTmpAddressMeta[$sTmpPropCode] = array(
//    'NAME' => 'Пользователь',
//    'PROPERTY_TYPE' => 'S',
//    'LIST_TYPE' => 'L',
//    'SEARCHABLE' => 'N',
//    'USER_TYPE' => 'UserID',
//    'MULTIPLE' => 'N',
//    'ACTIVE' => 'Y',
//    'FILTRABLE' => 'Y',
//    'SORT' => $iPropsSort,
//    'CODE' => $sTmpPropCode,
//    'IS_REQUIRED' => 'Y',
//);

//$sTmpPropCode = 'CUPONBG_VERTICAL';
//$iPropsSort += 100;
//$arTmpCartLimeMeta[$sTmpPropCode] = array(
//    'NAME' => 'Картинка для купона (вертикальная)',
//    'PROPERTY_TYPE' => 'F',
//    'LIST_TYPE' => 'L',
//    'SEARCHABLE' => 'N',
//    'USER_TYPE' => '',
//    'MULTIPLE' => 'N',
//    'ACTIVE' => 'Y',
//    'FILTRABLE' => 'Y',
//    'SORT' => $iPropsSort,
//    'CODE' => $sTmpPropCode,
//    'ROW_COUNT' => 2,
//    'COL_COUNT' => 30,
//    'IS_REQUIRED' => 'N',
//);

//$sTmpPropCode = 'MARGINALITY';
//$iPropsSort += 100;
//$arProductSIDPropertiesMeta[$sTmpPropCode] = array(
//    'NAME' => 'Маржинальность',
//    'PROPERTY_TYPE' => 'N',
//    'LIST_TYPE' => 'L',
//    'SEARCHABLE' => 'N',
//    'USER_TYPE' => 'YesNo',
//    'MULTIPLE' => 'N',
//    'ACTIVE' => 'Y',
//    'FILTRABLE' => 'Y',
//    'SORT' => $iPropsSort,
//    'CODE' => $sTmpPropCode,
//    'ROW_COUNT' => 2,
//    'COL_COUNT' => 30,
//    'IS_REQUIRED' => 'N',
//);

// Медали
$iTmpMedalsIblock = CProjectUtils::GetIBlockIdByCodeEx('medals', 'NIYAMA_DISCOUNTS');
if(!$iTmpMedalsIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Список медалей',
        'CODE' => 'medals',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '/local/php_interface/adv/iblock_forms/iblock_medals_before_save.php',
        'EDIT_FILE_AFTER' => '/local/php_interface/adv/iblock_forms/iblock_medals_form.php',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpMedalsIblock = $obIBlock->Add($arFields);
    if($iTmpMedalsIblock) {
        ?><p>Создан инфоблок "Список Медалей"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Список Медалей": <?=$obIBlock->LAST_ERROR?></p><?
    }
}
if($iTmpMedalsIblock) {
    $arTmpMedalsMeta = array();

    $sTmpPropCode = 'BASE_STATUS_TYPE';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' => 'Статус медали',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'YesNo',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );

    // ------------------------------------

    $sTmpPropCode = 'BASE_TYPE_IMG';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Тип изображения',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "По умолчанию",
                "DEF" => "Y",
                "SORT" => "100",
                "XML_ID"=>"DEFAULT",
            ),
            array(
                "VALUE" => "Загрузка изображения",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"DOWNLOAD",
            )
        )
    );

    $sTmpPropCode = 'BASE_TYPE_IMG_DOWNLOAD';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' => 'Изображение',
        'PROPERTY_TYPE' => 'F',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30
    );

    $sTmpPropCode = 'BASE_TYPE_ACTION';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Тип действия',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "Все типы действий",
                "DEF" => "Y",
                "SORT" => "100",
                "XML_ID"=>"ALL",
            ),
            array(
                "VALUE" => "Заказ",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"ORDER",
            ),
            array(
                "VALUE" => "Авторизация через социальную сеть",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"AUTHSOC",
            ),
            array(
                "VALUE" => "Участие в опросе",
                "DEF" => "N",
                "SORT" => "400",
                "XML_ID"=>"SURVEY",
            ),
            array(
                "VALUE" => "За рекомендацию в социальных сетях",
                "DEF" => "N",
                "SORT" => "500",
                "XML_ID"=>"RECOMENDATIONSOC",
            )
        )
    );

    $sTmpPropCode = 'BASE_COUNT_ACTION';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Количество получений медалей',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'DEFAULT_VALUE'	=> 1,
        'HINT'			=> 'Сколько всего медалей с одним набором условий может получить пользователь',

    );

    $sTmpPropCode = 'BASE_PERIOD_ACTION';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Период действия',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'HINT'			=> 'Период, в течение которого должно осуществляться действие с учетом всех установленных условий',
        'VALUES'		=> array(
            array(
                "VALUE" => "Любой период",
                "DEF" => "Y",
                "SORT" => "100",
                "XML_ID"=>"ANY",
            ),
            array(
                "VALUE" => "За месяц",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"MONTH",
            ),
            array(
                "VALUE" => "В день рождения",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"BIRTHDAY",
            ),
            array(
                "VALUE" => "Выбрать период",
                "DEF" => "N",
                "SORT" => "400",
                "XML_ID"=>"CUSTOM",
            ),
        )
    );

    $sTmpPropCode = 'BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Количество дней до и после ДР',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'HINT'			=> 'Количество дней, до и после даты рождения',
        'DEFAULT_VALUE'	=> 7,

    );

    //S:DateTime

    $sTmpPropCode = 'BASE_PERIOD_ACTION_CUSTOM_S';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'C',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> 'DateTime',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'HINT'			=> 'Дата начала периода действия купона',


    );
    $sTmpPropCode = 'BASE_PERIOD_ACTION_CUSTOM_DO';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'До',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> 'DateTime',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'HINT'			=> 'Дата окончания периода действия купона',


    );
	$sTmpPropCode = 'BASE_TIME_ACTION_S';
	$iPropsSort += 20;
	$arTmpMedalsMeta[$sTmpPropCode] = array(
		'NAME' 			=> 'C',
		'PROPERTY_TYPE' => 'S',
		'LIST_TYPE' 	=> '',
		'SEARCHABLE' 	=> 'N',
		'USER_TYPE' 	=> '',
		'MULTIPLE' 		=> 'N',
		'ACTIVE' 		=> 'Y',
		'FILTRABLE' 	=> 'Y',
		'SORT' 			=> $iPropsSort,
		'CODE' 			=> $sTmpPropCode,
		'HINT'			=> 'Время начала действия (минимальное 00:00:00). Формат ЧЧ:ММ:СС.',

		
	);
	$sTmpPropCode = 'BASE_TIME_ACTION_DO';
	$iPropsSort += 20;
	$arTmpMedalsMeta[$sTmpPropCode] = array(
		'NAME' 			=> 'До',
		'PROPERTY_TYPE' => 'S',
		'LIST_TYPE' 	=> '',
		'SEARCHABLE' 	=> 'N',
		'USER_TYPE' 	=> '',
		'MULTIPLE' 		=> 'N',
		'ACTIVE' 		=> 'Y',
		'FILTRABLE' 	=> 'Y',
		'SORT' 			=> $iPropsSort,
		'CODE' 			=> $sTmpPropCode,
		'HINT'			=> 'Время окончания действия (максимальное 23:59:59). Формат ЧЧ:ММ:СС.',

		
	);

    $sTmpPropCode = 'BASE_NUMBER_OF_ACTIONS';
    $iPropsSort += 60;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' => 'Кол-во действий',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
        'HINT' => 'Количество действий установленного типа, которое нужно осуществить'
    );


    $sTmpPropCode = 'ORDER_TYPE_DELIVERY';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Тип доставки',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,

        'VALUES'		=> array(
            array(
                "VALUE" => "Самовывоз",
                "DEF" => "Y",
                "SORT" => "100",
                "XML_ID"=>"SAMOVYVOZ",
            ),
            array(
                "VALUE" => "Курьером",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"CURIER",
            ),
        )
    );


    $sTmpPropCode = 'ORDER_TYPE_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Типы блюд',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'MULTIPLE' 		=> 'Y',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
    );
    $sTmpPropCode = 'ORDER_INGRIDIENTS_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Ингридиенты',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'MULTIPLE' 		=> 'Y',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
    );
    $sTmpPropCode = 'ORDER_SPEC_MENU_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Специальное меню',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'MULTIPLE' 		=> 'Y',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
    );

    $sTmpPropCode = 'ORDER_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Блюда',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'MULTIPLE' 		=> 'Y',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
    );

    $sTmpPropCode = 'ORDER_COUNT_BLUDS_YSLOVIE';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Количество блюд условие',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => ">",
                "DEF" => "N",
                "SORT" => "100",
                "XML_ID"=>"BOLSHE",
            ),
            array(
                "VALUE" => "=",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"RAVNO",
            ),
        )
    );
    $sTmpPropCode = 'ORDER_COUNT_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Количество блюд',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'DEFAULT_VALUE'	=> 1,

    );

    $sTmpPropCode = 'ORDER_SOSTAV_BLUDS';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Состав',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "Разные позиции",
                "DEF" => "N",
                "SORT" => "100",
                "XML_ID"=>"DIFFERENT",
            ),
            array(
                "VALUE" => "В любом",
                "DEF" => "Y",
                "SORT" => "200",
                "XML_ID"=>"ANY",
            ),
            array(
                "VALUE" => "Одинаковые позиции",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"SAME",
            ),
        )
    );

    $sTmpPropCode = 'ORDER_IN_WHAT';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'В каком заказе',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "В одном",
                "DEF" => "N",
                "SORT" => "100",
                "XML_ID"=>"ONE",
            ),
            array(
                "VALUE" => "В любом",
                "DEF" => "Y",
                "SORT" => "200",
                "XML_ID"=>"ANY",
            ),
            array(
                "VALUE" => "Подряд",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"SUCCESSION",
            ),
        )
    );


    $sTmpPropCode = 'ORDER_SUMM_ZACAZ_YSLOVIE';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Сумма заказа условие',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "Сумма равна",
                "DEF" => "N",
                "SORT" => "100",
                "XML_ID"=>"RAVNA",
            ),
            array(
                "VALUE" => "Минимальная сумма",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"MIN",
            ),
            array(
                "VALUE" => "Максимальная сумма",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"MAX",
            ),
        )
    );
    $sTmpPropCode = 'ORDER_SUMM_ZACAZ';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Сумма заказа в рублях',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' 	=> '',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,

    );
    $sTmpPropCode = 'RECOMENDATIONSOC_TYPE';
    $iPropsSort += 100;
    $arTmpMedalsMeta[$sTmpPropCode] = array(
        'NAME' 			=> 'Тип рекомендации',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE' 	=> 'L',
        'SEARCHABLE' 	=> 'N',
        'USER_TYPE' 	=> '',
        'MULTIPLE' 		=> 'N',
        'ACTIVE' 		=> 'Y',
        'FILTRABLE' 	=> 'Y',
        'SORT' 			=> $iPropsSort,
        'CODE' 			=> $sTmpPropCode,
        'VALUES'		=> array(
            array(
                "VALUE" => "Любая",
                "DEF" => "N",
                "SORT" => "100",
                "XML_ID"=>"ANY",
            ),
            array(
                "VALUE" => "Поделился новым уровнем скидки или медалью",
                "DEF" => "N",
                "SORT" => "200",
                "XML_ID"=>"TWEET_YROVEN_OR_MEDAL",
            ),
            array(
                "VALUE" => "Рекомендовал страничку любой социальной сети (через виджет)",
                "DEF" => "N",
                "SORT" => "300",
                "XML_ID"=>"RECOMENDATON_FROM_VIDZHET",
            ),
        )
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpMedalsIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpMedalsMeta as $arPropMeta) {
		
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpMedalsIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


/***
 * =======================
 * =======================
 * =======================
 */
$iTmpUsersMedalsIblock = CProjectUtils::GetIBlockIdByCodeEx('users_medals', 'NIYAMA_DISCOUNTS');
if(!$iTmpUsersMedalsIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Список медалей пользователей',
        'CODE' => 'users_medals',
        'IBLOCK_TYPE_ID' => 'NIYAMA_DISCOUNTS',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpUsersMedalsIblock = $obIBlock->Add($arFields);
    if($iTmpMedalsIblock) {
        ?><p>Создан инфоблок "Список Медалей"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Список Медалей": <?=$obIBlock->LAST_ERROR?></p><?
    }
}
if($iTmpUsersMedalsIblock) {
    $arTmpUsersMedalsMeta = array();

    $sTmpPropCode = 'USER_ID';
    $iPropsSort += 100;
    $arTmpUsersMedalsMeta[$sTmpPropCode] = array(
        'NAME' => 'Пользователь',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'UserID',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'IS_REQUIRED' => 'Y',
    );


    $sTmpPropCode = 'MEDAL_ID';
    $iPropsSort  += 100;
    $arTmpUsersMedalsMeta[$sTmpPropCode] = array(
        'NAME' => 'Медаль',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => $iTmpMedalsIblock
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpUsersMedalsIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpUsersMedalsMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpUsersMedalsIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}



/**
 * Создаем HIB
 * ==================================
 */
$sTmpEntityName = 'UserActions';
$sTmpEntityTableName = 'niyama_useractions';
$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
if (!$iTmpEntityId) {
    $obTmpResult = CHLEntity::Add(
        array(
            'NAME' => $sTmpEntityName,
            'TABLE_NAME' => $sTmpEntityTableName
        )
    );

    if ($obTmpResult->IsSuccess()) {
        $iTmpEntityId = $obTmpResult->GetId();
    }

    if ($iTmpEntityId) {
        ?><p>Создана HL-сущность "Действия пользователей"</p><?
    } else {
        echo '<p>' . implode('<br />', $obTmpResult->GetErrorMessages()) . '</p>';
    }
}

/**
 * Создаем Поля
 * ===================================
 */
if($iTmpEntityId) {
    $arAddFields = array(
        'UF_USER_ID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Пользователь'
        ),

        'UF_TYPE_ID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'ID типа действия'
        ),

        'UF_TM' => array(
            'USER_SOC_TYPE' => 'date',
            'NAME' => 'Дата выполнения'
        ),

        'UF_COUNT' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Кол-о выполнения'
        ),
    );

    $arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);

    foreach($arAddFields as $sTmpFieldName => $arAddParams) {
        if(!isset($arTmpCurFields[$sTmpFieldName])) {
            $arTmpParams = array_merge(
                array(
                    'FIELD_NAME' => $sTmpFieldName,
                    'EDIT_FORM_LABEL' => '',
                    'USER_TYPE_ID' => 'string',
                    'MULTIPLE' => 'N',
                    'SHOW_FILTER' => 'I',
                    'SHOW_IN_LIST' => 'Y',
                    'EDIT_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'SIZE' => '50'
                    ),
                    'LIST_COLUMN_LABEL' => '',
                    'LIST_FILTER_LABEL' => '',
                    'ERROR_MESSAGE' => '',
                    'HELP_MESSAGE' => ''
                ),
                $arAddParams
            );
            if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
                $arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
                $arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
                $arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
            }
            $iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
            if($iTmpUserFieldId) {

                if($sTmpFieldName == 'UF_USER_ID') {
                    // изменим тип поля UF_XML_ID с text на varchar(255)
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_USER_ID UF_USER_ID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                } else if($sTmpFieldName == 'UF_UID') {
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_UID UF_UID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                }

                echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
            } else {
                echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
            }
        }
    }
}
echo '<hr />';



/**
 * Создаем HIB
 * ==================================
 */
$sTmpEntityName = 'UserModalShow';
$sTmpEntityTableName = 'niyama_usermodalshow';
$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
if (!$iTmpEntityId) {
    $obTmpResult = CHLEntity::Add(
        array(
            'NAME' => $sTmpEntityName,
            'TABLE_NAME' => $sTmpEntityTableName
        )
    );

    if ($obTmpResult->IsSuccess()) {
        $iTmpEntityId = $obTmpResult->GetId();
    }

    if ($iTmpEntityId) {
        ?><p>Создана HL-сущность "Модальные окна"</p><?
    } else {
        echo '<p>' . implode('<br />', $obTmpResult->GetErrorMessages()) . '</p>';
    }
}

/**
 * Создаем Поля
 * ===================================
 */
if($iTmpEntityId) {
    $arAddFields = array(
        'UF_USER_ID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Пользователь'
        ),

        'UF_TYPE_ID' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Тип'
        ),
		'UF_ELEM_ID' => array(
            'USER_TYPE_ID' => 'integer',
            'NAME' => 'ID связанного элемента'
        ),
    );

    $arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);

    foreach($arAddFields as $sTmpFieldName => $arAddParams) {
        if(!isset($arTmpCurFields[$sTmpFieldName])) {
            $arTmpParams = array_merge(
                array(
                    'FIELD_NAME' => $sTmpFieldName,
                    'EDIT_FORM_LABEL' => '',
                    'USER_TYPE_ID' => 'string',
                    'MULTIPLE' => 'N',
                    'SHOW_FILTER' => 'I',
                    'SHOW_IN_LIST' => 'Y',
                    'EDIT_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'SIZE' => '50'
                    ),
                    'LIST_COLUMN_LABEL' => '',
                    'LIST_FILTER_LABEL' => '',
                    'ERROR_MESSAGE' => '',
                    'HELP_MESSAGE' => ''
                ),
                $arAddParams
            );
            if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
                $arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
                $arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
                $arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
            }
            $iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
            if($iTmpUserFieldId) {

                if($sTmpFieldName == 'UF_USER_ID') {
                    // изменим тип поля UF_XML_ID с text на varchar(255)
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_USER_ID UF_USER_ID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                } else if($sTmpFieldName == 'UF_UID') {
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_UID UF_UID VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                }

                echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
            } else {
                echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
            }
        }
    }
}
echo '<hr />';



$arAddAgents = array(
    'CAgentHandlers::CalculateMedals();' => array(
        'module' => 'iblock',
        'period' => 'N',
        'interval' => 86400,
        'datecheck' => '',
        'active' => 'Y',
        'next_exec' => '',
        'sort' => 1000
    ),
);
$dbItems = CAgent::GetList();
while($arItem = $dbItems->Fetch()) {
    if(isset($arAddAgents[$arItem['NAME']])) {
        unset($arAddAgents[$arItem['NAME']]);
    }
}
foreach($arAddAgents as $sAgentName => $arTmpParams) {
    $iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
    if($iNewAgentId) {
        echo '<p>Добавлен агент '.$sAgentName.'</p>';
    }
}




$dbItems = CForm::GetBySID('FRANCHISE');
$arForm = $dbItems->Fetch();
$iTmpFormId = $arForm['ID'] ? $arForm['ID'] : 0;
if(!$iTmpFormId) {
    $iTmpFormId = CForm::Set(
        array(
            'NAME' => 'Заявка на франшизу',
            'SID' => 'FRANCHISE',
            'C_SORT' => 100,
            'BUTTON' => 'Отправить',
            'DESCRIPTION' => '<h2 class="h2 vacancies-form__title">Отправить заявку</h2><p class="vacancies-form__text">Если вы готовы к сотрудничеству, отправляйте нам заявку. Мы обязательно свяжемся с вами.</p>',
            'DESCRIPTION_TYPE' => 'html',
            'STAT_EVENT1' => 'form',
            'STAT_EVENT2' => '',
            'arSITE' => array('s1'),
            'arMENU' => array(
                'ru' => 'Заявка на франшизу',
            ),
        )
    );
    if($iTmpFormId) {
        ?><p>Создана форма "Заявка на франшизу"</p><?
    } else {
        ?><p>Ошибка создания формы "Заявка на франшизу"</p><?
    }

    if($iTmpFormId) {
        CFormStatus::Set(
            array(
                'FORM_ID' => $iTmpFormId,
                'C_SORT' => 100,
                'ACTIVE' => 'Y',
                'TITLE' => 'DEFAULT',
                'DESCRIPTION' => 'DEFAULT',
                'CSS' => 'statusgreen',
                'HANDLER_OUT' => '',
                'HANDLER_IN' => '',
                'DEFAULT_VALUE' => 'Y',
                'arPERMISSION_VIEW' => array(0),
                'arPERMISSION_MOVE' => array(0),
                'arPERMISSION_EDIT' => array(0),
                'arPERMISSION_DELETE' => array(0),
            )
        );
    }
}


if($iTmpFormId) {
    $iTmpFormFieldId = CFormField::Set(
        array(
            'FORM_ID' => $iTmpFormId,
            'ACTIVE' => 'Y',
            'TITLE' => 'Имя и фамилия',
            'SID' => 'FRANCHISE_NAME',
            'C_SORT' => 100,
            'REQUIRED' => 'Y',
            'ADDITIONAL' => 'N',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FIELD_TYPE' => 'text',
            'FILTER_TITLE' => 'Имя и фамилия',
            'RESULTS_TABLE_TITLE' => 'Имя и фамилия',
            'arFILTER_FIELD' => array('text'),
            'arANSWER' => array(
                array(
                    'MESSAGE' => ' ',
                    'C_SORT' => 100,
                    'ACTIVE' => 'Y',
                    'FIELD_TYPE' => 'text',
                    'FIELD_PARAM' => 'class="input _mb_1"'
                )
            )
        )
    );

    $iTmpFormFieldId = CFormField::Set(
        array(
            'FORM_ID' => $iTmpFormId,
            'ACTIVE' => 'Y',
            'TITLE' => 'Телефон',
            'SID' => 'FRANCHISE_TEL',
            'C_SORT' => 200,
            'REQUIRED' => 'N',
            'ADDITIONAL' => 'N',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FIELD_TYPE' => 'text',
            'FILTER_TITLE' => 'Телефон',
            'RESULTS_TABLE_TITLE' => 'Телефон',
            'arFILTER_FIELD' => array('text'),
            'arANSWER' => array(
                array(
                    'MESSAGE' => ' ',
                    'C_SORT' => 100,
                    'ACTIVE' => 'Y',
                    'FIELD_TYPE' => 'text',
                    'FIELD_PARAM' => 'class="input _mb_1"'
                )
            )
        )
    );

    $iTmpFormFieldId = CFormField::Set(
        array(
            'FORM_ID' => $iTmpFormId,
            'ACTIVE' => 'Y',
            'TITLE' => 'Электронная почта',
            'SID' => 'FRANCHISE_EMAIL',
            'C_SORT' => 300,
            'REQUIRED' => 'Y',
            'ADDITIONAL' => 'N',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FIELD_TYPE' => 'email',
            'FILTER_TITLE' => 'Электронная почта',
            'RESULTS_TABLE_TITLE' => 'Электронная почта',
            'arFILTER_FIELD' => array('email'),
            'arANSWER' => array(
                array(
                    'MESSAGE' => ' ',
                    'C_SORT' => 100,
                    'ACTIVE' => 'Y',
                    'FIELD_TYPE' => 'email',
                    'FIELD_PARAM' => 'class="input _mb_1"'
                )
            )
        )
    );

    $iTmpFormFieldId = CFormField::Set(
        array(
            'FORM_ID' => $iTmpFormId,
            'ACTIVE' => 'Y',
            'TITLE' => 'Город',
            'SID' => 'FRANCHISE_CITY',
            'C_SORT' => 200,
            'REQUIRED' => 'N',
            'ADDITIONAL' => 'N',
            'IN_RESULTS_TABLE' => 'Y',
            'IN_EXCEL_TABLE' => 'Y',
            'FIELD_TYPE' => 'text',
            'FILTER_TITLE' => 'Город',
            'RESULTS_TABLE_TITLE' => 'Город',
            'arFILTER_FIELD' => array('text'),
            'arANSWER' => array(
                array(
                    'MESSAGE' => ' ',
                    'C_SORT' => 100,
                    'ACTIVE' => 'Y',
                    'FIELD_TYPE' => 'text',
                    'FIELD_PARAM' => 'class="input _mb_1"'
                )
            )
        )
    );

}



$iTmpAuctionsIblock = CProjectUtils::GetIBlockIdByCodeEx('auctions_products', 'catalog');
if(!$iTmpAuctionsIblock) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Аукционные блюда',
        'CODE' => 'auctions_products',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpAuctionsIblock = $obIBlock->Add($arFields);
    if($iTmpAuctionsIblock) {
        ?><p>Создан инфоблок "Аукционные блюда"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Аукционные блюда": <?=$obIBlock->LAST_ERROR?></p><?
    }
}


if($iTmpAuctionsIblock) {
    $arActionProductsMeta = array();

    $sTmpPropCode = 'PRODUCT_ID';
    $iPropsSort  += 100;
    $arActionProductsMeta[$sTmpPropCode] = array(
        'NAME' => 'Блюдо',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog')
    );


    // список текущих свойств инфоблока
    $arSidAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpAuctionsIblock));
    while($arItem = $dbItems->Fetch()) {
        $arSidAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arActionProductsMeta as $arPropMeta) {
        if(!isset($arSidAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpAuctionsIblock;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}



/**
 * Создаем HIB
 * ==================================
 */
$sTmpEntityName = 'TmpUsers';
$sTmpEntityTableName = 'niyama_tmpusers';
$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
if (!$iTmpEntityId) {
    $obTmpResult = CHLEntity::Add(
        array(
            'NAME' => $sTmpEntityName,
            'TABLE_NAME' => $sTmpEntityTableName
        )
    );

    if ($obTmpResult->IsSuccess()) {
        $iTmpEntityId = $obTmpResult->GetId();
    }

    if ($iTmpEntityId) {
        ?><p>Создана HL-сущность "Старые пользователи сайта"</p><?
    } else {
        echo '<p>' . implode('<br />', $obTmpResult->GetErrorMessages()) . '</p>';
    }
}

/**
 * Создаем Поля
 * ===================================
 */
if($iTmpEntityId) {
    $arAddFields = array(
        'UF_TMP_EMAIL' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Email'
        ),

        'UF_TMP_GANDER' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Пол'
        ),

        'UF_TMP_NAME' => array(
            'USER_SOC_TYPE' => 'string',
            'NAME' => 'Имя'
        ),

        'UF_TMP_SECOND_NAME' => array(
            'USER_SOC_TYPE' => 'string',
            'NAME' => 'Фамилия'
        ),

        'UF_TMP_PHONE' => array(
            'USER_SOC_TYPE' => 'string',
            'NAME' => 'Телефон'
        ),

        'UF_TMP_BIRTH_DATE' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'День рождения'
        ),

        'UF_TMP_NIYAMA' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Niyama'
        ),

        'UF_TMP_CARD_NUMBER' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Номер карты'
        ),

        'UF_TMP_RATING' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Рейтинг'
        ),

        'UF_TMP_LEVEL' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Уровень'
        ),

        'UF_TMP_MEDALS' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Медали'
        ),

        'UF_TMP_SOCIAL' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'СоцСети'
        ),

        'UF_TMP_ORDERS' => array(
            'USER_TYPE_ID' => 'string',
            'NAME' => 'Заказы'
        ),

    );

    $arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);

    foreach($arAddFields as $sTmpFieldName => $arAddParams) {
        if(!isset($arTmpCurFields[$sTmpFieldName])) {
            $arTmpParams = array_merge(
                array(
                    'FIELD_NAME' => $sTmpFieldName,
                    'EDIT_FORM_LABEL' => '',
                    'USER_TYPE_ID' => 'string',
                    'MULTIPLE' => 'N',
                    'SHOW_FILTER' => 'I',
                    'SHOW_IN_LIST' => 'Y',
                    'EDIT_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'SIZE' => '50'
                    ),
                    'LIST_COLUMN_LABEL' => '',
                    'LIST_FILTER_LABEL' => '',
                    'ERROR_MESSAGE' => '',
                    'HELP_MESSAGE' => ''
                ),
                $arAddParams
            );
            if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
                $arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
                $arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
                $arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
            }
            $iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
            if($iTmpUserFieldId) {

                if($sTmpFieldName == 'UF_TMP_EMAIL') {
                    $sTmpQuery = 'ALTER TABLE '.$sTmpEntityTableName.' CHANGE UF_TMP_EMAIL UF_TMP_EMAIL VARCHAR(255)';
                    $GLOBALS['DB']->Query($sTmpQuery);
                }

                echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
            } else {
                echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
            }
        }
    }
}
echo '<hr />';



# Устанавливаем Минимальную сумму зкорзины для заказа
if ( !CNiyamaCustomSettings::getStringValue("min_sum_by_order",false) ){
    CNiyamaCustomSettings::setStringValue("Минимальная сумма корзины для закза","min_sum_by_order","0");
}

# Устанавливаем Максимальное количество гостей для Корзины/Заказа
if ( !CNiyamaCustomSettings::getStringValue("max_num_guests",false) ){
    CNiyamaCustomSettings::setStringValue("Максимальное количество гостей для Корзины/Заказа","max_num_guests","8");
}

# Максимальнок количество товаров для замены в корзине
if ( !CNiyamaCustomSettings::getStringValue("max_num_related_products",false) ){
    CNiyamaCustomSettings::setStringValue("Максимальное количество товаров для замены в Корзине","max_num_related_products","4");
}

# Количество товаров в блоке "Рекомендуем"
if ( !CNiyamaCustomSettings::getStringValue("recommended_products_count",false) ){
    CNiyamaCustomSettings::setStringValue('Количество товаров в блоке "Рекомендуем"',"recommended_products_count","10");
}

# Количество ингредиентов, выводимое до ссылки "Все ингредиенты"
if ( !CNiyamaCustomSettings::getStringValue("default_ingredient_count",false) ){
    CNiyamaCustomSettings::setStringValue('Количество ингредиентов, выводимое до ссылки "Все ингредиенты" (0 или пустая строка - выводит все сразу)',"default_ingredient_count","10");
}

# Количество типов блюд, выводимое до ссылки "ВСЕ КАТЕГОРИИ"
if ( !CNiyamaCustomSettings::getStringValue("default_categories_count",false) ){
    CNiyamaCustomSettings::setStringValue('Количество типов блюд, выводимое до ссылки "Все категории" (0 или пустая строка - выводит все сразу)',"default_categories_count","10");
}


/**
 * Устанавливаем соответсвие на скидку из сиарого сайта
 */
if ( !CNiyamaCustomSettings::getStringValue("old_level_0",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_0","0");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_1",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_1","5");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_2",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_2","6");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_3",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_3","7");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_4",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_4","8");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_5",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_5","9");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_6",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_6","10");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_7",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_7","11");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_8",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_8","12");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_9",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_9","13");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_10",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_10","14");
}
if ( !CNiyamaCustomSettings::getStringValue("old_level_11",false) ){
    CNiyamaCustomSettings::setStringValue('Соответсвие скидки со старого сайта',"old_level_11","15");
}

if(!defined('_NIYAMA_COMMON_DEV_UPDATE_') || !_NIYAMA_COMMON_DEV_UPDATE_) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
}
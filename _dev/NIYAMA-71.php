<?php

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');

// Настройка TODO заменить на реальные
COption::SetOptionString("main","email_from","admin@site.com");
COption::SetOptionString("main","site_name","Нияма");
COption::SetOptionString("main","server_name","niyama.ru");

/* Контактная Информация */
CNiyamaCustomSettings::setStringValue("Основной телефон","header_phone","+7 495 781-781-9");


echo "<p>Добавлены первичные настройки сайта.</p>";
echo "<hr>";
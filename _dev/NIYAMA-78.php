<?php

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}
CModule::IncludeModule('iblock');

/**
 *  ингредиенты
 *
 */
$iBlockIngredient = 0;
$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-ingredients', 'catalog');
if(!$iTmpIBlockId) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Ингредиенты',
        'CODE' => 'catalog-ingredients',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '1000',
        'GROUP_ID' => array(2 => "R"),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpIBlockId = $obIBlock->Add($arFields);
    if($iTmpIBlockId) {
        $iBlockIngredient = $iTmpIBlockId;
        ?><p>Создан инфоблок "Ингредиенты"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Ингредиенты": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

/**
 *  Специальное меню
 *
 */
$iBlockSpecMenu = 0;
$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-special-menu', 'catalog');
if(!$iTmpIBlockId) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Специальное меню',
        'CODE' => 'catalog-special-menu',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '60',
        'GROUP_ID' => array(2 => "R"),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
    );
    $obIBlock = new CIBlock();
    $iTmpIBlockId = $obIBlock->Add($arFields);
    if($iTmpIBlockId) {
        $iBlockSpecMenu = $iTmpIBlockId;
        ?><p>Создан инфоблок "Специальное меню"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Специальное меню": <?=$obIBlock->LAST_ERROR?></p><?
    }
}


/**
 * Основное меню
 */
$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog');
if(!$iTmpIBlockId) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Основное меню',
        'CODE' => 'general-site-menu',
        'IBLOCK_TYPE_ID' => 'catalog',
        'SITE_ID' => 's1',
        'SORT' => '50',
        'GROUP_ID' => array(2 => "R"),
        'LIST_PAGE_URL'    => '#SITE_DIR#catalog/',
        'SECTION_PAGE_URL' => '#SITE_DIR#catalog/##SECTION_ID#',
        'DETAIL_PAGE_URL'  => '#SITE_DIR#catalog/#ELEMENT_CODE#.php',
        'INDEX_SECTION'    => 'N',
        'INDEX_ELEMENT'    => 'Y',
        'EDIT_FILE_BEFORE' => '',
        'ELEMENTS_NAME'  => 'Элементы меню',
        'ELEMENT_NAME'   => 'Элемент меню',
        'ELEMENT_ADD'    => 'Добавить элемент меню',
        'ELEMENT_EDIT'   => 'Изменить элемент меню',
        'ELEMENT_DELETE' => 'Удалить элемент меню',
        'SECTIONS_NAME'  => 'Разделы',
        'SECTION_NAME'   => 'Раздел',
        'SECTION_ADD'    => 'Добавить раздел',
        'SECTION_EDIT'   => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'VERSION' => 2,
        'FIELDS' => array(
            'CODE' => array(
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => array(
                    'UNIQUE' => 'Y',
                    'TRANSLITERATION' => 'Y',
                    'TRANS_LEN' => '100',
                    'TRANS_CASE' => 'L',
                    'TRANS_SPACE' => '_',
                    'TRANS_OTHER' => '_',
                    'TRANS_EAT' => 'Y',
                    'USE_GOOGLE' => 'N',
                ),
            ),
        )
    );
    $obIBlock = new CIBlock();
    $iTmpIBlockId = $obIBlock->Add($arFields);
    if($iTmpIBlockId) {
        ?><p>Создан инфоблок "Основное меню"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Основное меню": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpIBlockId) {
    $arTmpPropertiesMeta = array();
    $iPropsSort = 400;
    // ---
    $sTmpPropCode = 'IMPORT_ELEMENT';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Импортируемые данные',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'import_menu_element',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'IS_REQUIRED' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('catalog-base', 'catalog')
    );

    $sTmpPropCode = 'GENERAL_INGREDIENT';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Основной ингредиент',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => $iBlockIngredient,
        'IS_REQUIRED' => 'N',
    );
    echo $iBlockIngredient." - ID Ингридиентов<br>";

    $sTmpPropCode = 'WEIGHT';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Вес блюда',
        'PROPERTY_TYPE' => 'N',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 2,
        'COL_COUNT' => 30,
        'IS_REQUIRED' => 'N',
    );
    echo $iBlockIngredient." - Вес блюда<br>";

    $sTmpPropCode = 'MORE_INGREDIENT';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Дополнительный ингредиент',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'Y',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => $iBlockIngredient,
        'IS_REQUIRED' => 'N',
    );
    echo $iBlockIngredient." - ID Ингридиентов<br>";

    $sTmpPropCode = 'SPEC_MENU';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Специальное меню',
        'PROPERTY_TYPE' => 'E',
        'LIST_TYPE' => 'L',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => 'EList',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'LINK_IBLOCK_ID' => $iBlockSpecMenu,
        'IS_REQUIRED' => 'N',
    );
    echo $iBlockIngredient." - ID Ингридиентов<br>";

    // список текущих свойств инфоблока
    $arAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
    while($arItem = $dbItems->Fetch()) {
        $arAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpPropertiesMeta as $arPropMeta) {
        if(!isset($arAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpIBlockId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}

echo "<hr>";





if(!defined('_NIYAMA_COMMON_DEV_UPDATE_') || !_NIYAMA_COMMON_DEV_UPDATE_) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
}

<?
//
// Тест карт. Привязка к юзеру
//

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');


if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

$sLogin = isset($_POST['USER_LOGIN']) ? htmlspecialcharsbx(trim($_POST['USER_LOGIN'])) : '';
$sCardNum = isset($_POST['CARD_NUM']) ? htmlspecialcharsbx(trim($_POST['CARD_NUM'])) : '';

$arErrors = array();
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(check_bitrix_sessid()) {
		if(strlen($sLogin)) {
			$arUser = CUser::GetByLogin($sLogin)->Fetch();
			if($arUser) {
				if(strlen($sCardNum)) {
					$arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sCardNum);
					if($arPdsCardInfo) {
						if($arPdsCardInfo['_ERRORS_']) {
							$arErrors = array_merge($arErrors, $arPdsCardInfo['_ERRORS_']);
						}
						if(!$arErrors && $arPdsCardInfo['Account']) {
							// все ок, добавляем карту юзеру
							$iCardDataElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arPdsCardInfo, $arUser['ID'], array('ACTIVE' => 'Y'), true);
							if($iCardDataElementId) {
								echo '<p>Карта привязана</p>';
								CNiyamaDiscountCard::DeleteCardIdentifyData($iUserId);
								$bSuccess = true;
							} else {
								$arFormErrors['UNKNOWN_ERROR'] = 'Не удалось привязать карту к пользователю. Попробуйте повторить операцию позже, либо обратитесь в тех.поддержку сайта';
							}
						}
					} else {
						$arErrors[] = 'Карта не зарегистрирована';
					}
				} else {
					$arErrors[] = 'Укажите номер карты';
				}
			} else {
				$arErrors[] = 'Пользователь не найден';
			}
		} else {
			$arErrors[] = 'Укажите логин';
		}
	} else {
		$arErrors[] = 'Ваша сессия истекла';
	}
}

if($arErrors) {
	echo '<p>'.implode('<br />', $arErrors).'</p>';
}
?><form action="" method="post"><?
	echo bitrix_sessid_post();
	?><p>Логин пользователя, к которому привязать карту:<br />
	<input type="text" name="USER_LOGIN" value="<?=$sLogin?>" /></p>
	<p>Номер привязываемой карты:<br /><input type="text" name="CARD_NUM" value="<?=$sCardNum?>" /></p>
	<input type="submit" value="Привязать карту" />
</form><?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

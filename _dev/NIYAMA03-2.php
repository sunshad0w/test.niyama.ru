<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');

$iTmpIBlockId = CProjectUtils::GetIBlockIdByCode('catalog_seo', 'catalog');
if ((CCustomProject::IfIBTypeByID('catalog')) && (!CProjectUtils::GetIBlockIdByCode('catalog_seo'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'catalog',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'catalog_seo',
        'NAME' => "SEO для каталога",
        'SITE_ID' => 's1',
        'SORT' => '1500',
        'GROUP_ID' => array(),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',       
        'VERSION' => 2,
        
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){		
    
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
        $iTmpIBlockId=$ID;
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }
}
if (!empty($iTmpIBlockId)&&$iTmpIBlockId>0){    
    $arTmpPropertiesMeta = array();
    $iPropsSort = 0;
    // ---
    $sTmpPropCode = 'URL';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'URL страницы каталога',
        'PROPERTY_TYPE' => 'S',			
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 100,
        
    );
    $sTmpPropCode = 'TITLE';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'TITLE',
        'PROPERTY_TYPE' => 'S',			
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 100,
    );
    $sTmpPropCode = 'KEYWORDS';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'KEYWORDS',
        'PROPERTY_TYPE' => 'S',			
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 5,
        'COL_COUNT' => 100,
    );
    $sTmpPropCode = 'DESCRIPTION';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'DESCRIPTION',
        'PROPERTY_TYPE' => 'S',			
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 5,
        'COL_COUNT' => 100,
    );
    $sTmpPropCode = 'SEO_H1';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'SEO_H1',
        'PROPERTY_TYPE' => 'S',			
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
        'ROW_COUNT' => 1,
        'COL_COUNT' => 100,
    );
    
    
    // список текущих свойств инфоблока
    $arAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
    while($arItem = $dbItems->Fetch()) {
        $arAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpPropertiesMeta as $arPropMeta) {
        if(!isset($arAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}


echo "<hr>";
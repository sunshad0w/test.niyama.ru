<?
return; // подстраховка от ошибочного запуска
//
// Перенос данных о пользователях со старого сайта во временную таблицу
//

@ini_set('mbstring.func_overload', 2);
@ini_set('mbstring.internal_encoding', 'UTF-8');

if(!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = realpath(dirname(__FILE__).'/..');
}

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
//define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);
if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

// очищать таблицу перед импортом
$bClearTableBefore = false;
// не делать дублей
$bAvoidDuplicates = true;
// импортировать данные о заказах
$bImportOrdersData = true;

$obResultXml = simplexml_load_file('import_xml/users.xml');

if(!function_exists('object2array')) {
	function object2array($object) {
		return @json_decode(@json_encode($object), true);
	}
}

if($obResultXml) {

	if($bClearTableBefore) {
		$GLOBALS['DB']->Query('TRUNCATE niyama_tmpusers');
	}

	foreach($obResultXml->user as $obImportUser) {
		// Добавляем пользователей
		if(!$bImportOrdersData && isset($obImportUser->orders)) {
			unset($obImportUser->orders);
		}

		$sTmpFirstName = CNiyamaImportUtils::GetXmlAttrValue($obImportUser->fio, 'name', true);
		$sTmpSecondName = CNiyamaImportUtils::GetXmlAttrValue($obImportUser->fio, 'second_name', true);
		$sTmpFirstName = !strlen($sTmpFirstName) && !strlen($sTmpSecondName) ? trim($obImportUser->fio) : $sTmpFirstName;
		$arSetFields = array(
			'UF_TMP_EMAIL' => trim($obImportUser->email),
			'UF_TMP_GANDER' => trim($obImportUser->gender),
			'UF_TMP_NAME' => $sTmpFirstName,
			'UF_TMP_SECOND_NAME' => $sTmpSecondName,
			'UF_TMP_PHONE' => trim($obImportUser->phone),
			'UF_TMP_BIRTH_DATE' => trim($obImportUser->birth_date),
			'UF_TMP_NIYAMA' => CNiyamaImportUtils::Str2Double(trim($obImportUser->niyama)),
			'UF_TMP_CARD_NUMBER' => trim($obImportUser->card_number),
			'UF_TMP_RATING' => CNiyamaImportUtils::Str2Double(trim($obImportUser->rating)),
			'UF_TMP_LEVEL' => CNiyamaImportUtils::Str2Double(trim($obImportUser->level)),
			'UF_TMP_MEDALS' => trim($obImportUser->medals),
			'UF_TMP_SOCIAL' => $obImportUser->socials ? serialize(object2array($obImportUser->socials)) : '',
			'UF_TMP_ORDERS' => $obImportUser->orders->order ? serialize(object2array($obImportUser->orders->order)) : ''
		);

		if($bAvoidDuplicates) {
			$dbTmpUsers = TmpUsersTable::GetList(
				array(
					'filter' => array(
						'=UF_TMP_EMAIL' => $arSetFields['UF_TMP_EMAIL']
					)
				)
			);
			while($arItem = $dbTmpUsers->Fetch()) {
				if($arSetFields) {
					TmpUsersTable::Update($arItem['ID'], $arSetFields);
					$arSetFields = array();
				} else {
					TmpUsersTable::Delete($arItem['ID']);
				}
			}
		}
		if($arSetFields) {
			TmpUsersTable::Add($arSetFields);
		}
	}
}


require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

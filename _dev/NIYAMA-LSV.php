<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

CModule::IncludeModule('iblock');
CModule::IncludeModule('form');
CModule::IncludeModule('sale');

$iNiyamaLsvVer = intval(COption::GetOptionInt('main', 'niyama_lsv_ver', 0, '-'));
//$iNiyamaLsvVer = 0;


//
// Версионные апдейты
//
if($iNiyamaLsvVer < 1) {
	$iNiyamaLsvVer = 1;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('catalog');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'catalog',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 100,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Каталог'
				),
				'en' => array(
					'NAME' => 'Каталог'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Каталог"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Каталог": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}
	
	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-base', 'catalog');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Импортируемое меню',
			'CODE' => 'catalog-base',
			'IBLOCK_TYPE_ID' => 'catalog',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '/local/php_interface/adv/iblock_forms/catalog_base_before.php',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Импортируемое меню"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Импортируемое меню": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'SIZE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Размер блюда',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'NAME_SHORT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Сокращенное название',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'UNIT_NAME';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Единица измерения',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'WEIGHT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Вес блюда',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'SORT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Сортировка',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'RECOMMENDED';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Рекомендуемое блюдо',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'PRICE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Цена блюда',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::UpdateCatalogBase();' => array(
			'module' => 'iblock',
			'period' => 'N',
			'interval' => 86400,
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1000
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}

if($iNiyamaLsvVer < 2) {
	$iNiyamaLsvVer = 2;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('data');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'data',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 200,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Справочники'
				),
				'en' => array(
					'NAME' => 'Справочники'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Справочники"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Справочники": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}

	//
	// --- departments
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('departments', 'data');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Департаменты',
			'CODE' => 'departments',
			'IBLOCK_TYPE_ID' => 'data',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Импортируемое меню"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Импортируемое меню": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'REMARK';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Примечание',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LONG';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Долгота',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LAT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Широта',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// --- cities
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('cities', 'data');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Города',
			'CODE' => 'cities',
			'IBLOCK_TYPE_ID' => 'data',
			'SITE_ID' => 's1',
			'SORT' => '200',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Города"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Города": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// --- streets
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('streets', 'data');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Улицы',
			'CODE' => 'streets',
			'IBLOCK_TYPE_ID' => 'data',
			'SITE_ID' => 's1',
			'SORT' => '300',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Улицы"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Улицы": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'CITY_CODE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Код города',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'REMARK';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Примечание',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// --- subway
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('subway', 'data');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Станции метро',
			'CODE' => 'subway',
			'IBLOCK_TYPE_ID' => 'data',
			'SITE_ID' => 's1',
			'SORT' => '400',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Станции метро"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Станции метро": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'CITY_CODE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Код города',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LONG';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Долгота',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LAT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Широта',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// --- pds_discounts
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('pds_discounts', 'data');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Типы скидок ПДС',
			'CODE' => 'pds_discounts',
			'IBLOCK_TYPE_ID' => 'data',
			'SITE_ID' => 's1',
			'SORT' => '500',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Типы скидок ПДС"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Типы скидок ПДС": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'DISCOUNT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Величина скидки',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

}

if($iNiyamaLsvVer < 3) {
	$iNiyamaLsvVer = 3;

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_index.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_index.php\');'); 
	}

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_catalog_base.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_catalog_base.php\');'); 
	}

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_data_departments.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_data_departments.php\');'); 
	}

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_data_cities.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_data_cities.php\');'); 
	}

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_data_streets.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_data_streets.php\');'); 
	}

	$sFilePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/adv_import_data_subway.php';
	if(!file_exists($sFilePath)) {
		file_put_contents($sFilePath, '<?require_once($_SERVER[\'DOCUMENT_ROOT\'].\'/local/php_interface/adv/admin/adv_import_data_subway.php\');'); 
	}
}

if($iNiyamaLsvVer < 4) {
	$iNiyamaLsvVer = 4;
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('pds_discounts', 'data');
	if($iTmpIBlockId) {
		$arUpdateItemsList = array(
			'32' => array(
				'NAME' => '5%',
				'PROPERTY_DISCOUNT' => '5',
				'CODE' => '32'
			),
			'34' => array(
				'NAME' => '6%',
				'PROPERTY_DISCOUNT' => '6',
				'CODE' => '34'
			),
			'35' => array(
				'NAME' => '7%',
				'PROPERTY_DISCOUNT' => '7',
				'CODE' => '35'
			),
			'36' => array(
				'NAME' => '8%',
				'PROPERTY_DISCOUNT' => '8',
				'CODE' => '36'
			),
			'37' => array(
				'NAME' => '9%',
				'PROPERTY_DISCOUNT' => '9',
				'CODE' => '37'
			),
			'38' => array(
				'NAME' => '10%',
				'PROPERTY_DISCOUNT' => '10',
				'CODE' => '38'
			),
			'40' => array(
				'NAME' => '11%',
				'PROPERTY_DISCOUNT' => '11',
				'CODE' => '40'
			),
			'41' => array(
				'NAME' => '12%',
				'PROPERTY_DISCOUNT' => '12',
				'CODE' => '41'
			),
			'42' => array(
				'NAME' => '13%',
				'PROPERTY_DISCOUNT' => '13',
				'CODE' => '42'
			),
			'43' => array(
				'NAME' => '14%',
				'PROPERTY_DISCOUNT' => '14',
				'CODE' => '43'
			),
			'44' => array(
				'NAME' => '15%',
				'PROPERTY_DISCOUNT' => '15',
				'CODE' => '44'
			),
		);

		if($arUpdateItemsList) {
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iTmpIBlockId,
					'=CODE' => array_keys($arUpdateItemsList)
				),
				false,
				false,
				array(
					'ID', 'CODE'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if($arUpdateItemsList[$arItem['CODE']]) {
					$arTmpUpdateItem = $arUpdateItemsList[$arItem['CODE']];
					$arFields = array(
						'NAME' => $arTmpUpdateItem['NAME'],
						'ACTIVE' => 'Y',
						'CODE' => $arTmpUpdateItem['CODE'],
					);
					$arProp = array(
						'DISCOUNT' => $arTmpUpdateItem['PROPERTY_DISCOUNT'],
					);
					$obIBlockElement = new CIBlockElement();
					$obIBlockElement->Update($arItem['ID'], $arFields);
					CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iTmpIBlockId, $arProp);
					// !!!
					unset($arUpdateItemsList[$arItem['CODE']]);
				}
			}
		}

		// добавление элементов
		if($arUpdateItemsList) {
			foreach($arUpdateItemsList as $arTmpUpdateItem) {
				$arFields = array(
					'IBLOCK_ID' => $iTmpIBlockId,
					'NAME' => $arTmpUpdateItem['NAME'],
					'ACTIVE' => 'Y',
					'CODE' => $arTmpUpdateItem['CODE'],
				);
				$arProp = array(
					'DISCOUNT' => $arTmpUpdateItem['PROPERTY_DISCOUNT'],
				);
				$arFields['PROPERTY_VALUES'] = $arProp;
				$obIBlockElement = new CIBlockElement();
				$obIBlockElement->Add($arFields);
			}
		}
	}
}

if($iNiyamaLsvVer < 5) {
	$iNiyamaLsvVer = 5;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('misc');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'misc',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 300,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Разное'
				),
				'en' => array(
					'NAME' => 'Разное'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Разное"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Разное": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('vacancies', 'misc');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Вакансии',
			'CODE' => 'vacancies',
			'IBLOCK_TYPE_ID' => 'misc',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '#SITE_DIR#about/vacancies/',
			'SECTION_PAGE_URL' => '#SITE_DIR#about/vacancies/#SECTION_ID#/',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Вакансии"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Вакансии": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 400;
		// ---
		$sTmpPropCode = 'IMG_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Тип изображения',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'VALUES' => array(
				array(
					'XML_ID' => 'TYPE_1',
					'VALUE' => 'Тип 1 (мужчина)',
					'DEF' => 'Y',
					'SORT' => 100
				),
				array(
					'XML_ID' => 'TYPE_2',
					'VALUE' => 'Тип 2 (женщина)',
					'DEF' => 'N',
					'SORT' => 200
				),
			)
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}


		// добавим пользовательское поле
		$sUserFieldEntityId = 'IBLOCK_'.$iTmpIBlockId.'_SECTION';
		$sUserFieldName = 'UF_PP';

		$obUserTypeEntity = new CUserTypeEntity();
		$iUserFieldId = $obUserTypeEntity->Add(
			array(
				'ENTITY_ID' => $sUserFieldEntityId,
				'FIELD_NAME' => $sUserFieldName,
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'SHOW_FILTER' => 'I',
				'SHOW_IN_LIST' => 'Y',
				'EDIT_IN_LIST' => 'Y',
				'IS_SEARCHABLE' => 'N',
				'SETTINGS' => array(
					'SIZE' => '50'
				),
				'EDIT_FORM_LABEL' => 'Название в предложном падеже (Где?)',
				'LIST_COLUMN_LABEL' => 'Название в предложном падеже (Где?)',
				'LIST_FILTER_LABEL' => 'Название в предложном падеже (Где?)',
				'ERROR_MESSAGE' => '',
				'HELP_MESSAGE' => ''
			)
		);

		$sUserFieldEntityId = 'IBLOCK_'.$iTmpIBlockId.'_SECTION';
		$sUserFieldName = 'UF_RP';

		$obUserTypeEntity = new CUserTypeEntity();
		$iUserFieldId = $obUserTypeEntity->Add(
			array(
				'ENTITY_ID' => $sUserFieldEntityId,
				'FIELD_NAME' => $sUserFieldName,
				'USER_TYPE_ID' => 'string',
				'MULTIPLE' => 'N',
				'SHOW_FILTER' => 'I',
				'SHOW_IN_LIST' => 'Y',
				'EDIT_IN_LIST' => 'Y',
				'IS_SEARCHABLE' => 'N',
				'SETTINGS' => array(
					'SIZE' => '50'
				),
				'EDIT_FORM_LABEL' => 'Название в родительном падеже (Кого?)',
				'LIST_COLUMN_LABEL' => 'Название в родительном падеже (Кого?)',
				'LIST_FILTER_LABEL' => 'Название в родительном падеже (Кого?)',
				'ERROR_MESSAGE' => '',
				'HELP_MESSAGE' => ''
			)
		);

	}

	$dbItems = CForm::GetBySID('VACANCY');
	$arForm = $dbItems->Fetch();
	$iTmpFormId = $arForm['ID'] ? $arForm['ID'] : 0;
	if(!$iTmpFormId) {
		$iTmpFormId = CForm::Set(
			array(
				'NAME' => 'Резюме',
				'SID' => 'VACANCY',
				'C_SORT' => 100,
				'BUTTON' => 'Отправить',
				'DESCRIPTION' => '<h2 class="h2 vacancies-form__title">Отправить резюме</h2><p class="vacancies-form__text">Даже если вы не нашли подходящую вакансию, присылайте нам резюме. В любом случае.</p>',
				'DESCRIPTION_TYPE' => 'html',
				'STAT_EVENT1' => 'form',
				'STAT_EVENT2' => '',
				'arSITE' => array('s1'),
				'arMENU' => array(
					'ru' => 'Резюме', 
				),
			)
		);
		if($iTmpFormId) {
			?><p>Создана форма "Резюме"</p><?
		} else {
			?><p>Ошибка создания формы "Резюме"</p><?
		}

		if($iTmpFormId) {
			CFormStatus::Set(
				array(
					'FORM_ID' => $iTmpFormId,
					'C_SORT' => 100,
					'ACTIVE' => 'Y',
					'TITLE' => 'DEFAULT',
					'DESCRIPTION' => 'DEFAULT',
					'CSS' => 'statusgreen',
					'HANDLER_OUT' => '',
					'HANDLER_IN' => '',
					'DEFAULT_VALUE' => 'Y',
					'arPERMISSION_VIEW' => array(0),
					'arPERMISSION_MOVE' => array(0),
					'arPERMISSION_EDIT' => array(0),
					'arPERMISSION_DELETE' => array(0),
				)
			);
		}
	}

	if($iTmpFormId) {
		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Имя и фамилия',
				'SID' => 'VACANCY_NAME',
				'C_SORT' => 100,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Имя и фамилия',
				'RESULTS_TABLE_TITLE' => 'Имя и фамилия',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input _mb_1"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Телефон',
				'SID' => 'VACANCY_TEL',
				'C_SORT' => 200,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Телефон',
				'RESULTS_TABLE_TITLE' => 'Телефон',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input _mb_1"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Электронная почта',
				'SID' => 'VACANCY_EMAIL',
				'C_SORT' => 300,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'email',
				'FILTER_TITLE' => 'Электронная почта',
				'RESULTS_TABLE_TITLE' => 'Электронная почта',
				'arFILTER_FIELD' => array('email'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'email',
						'FIELD_PARAM' => 'class="input _mb_2"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Интересующая позиция',
				'SID' => 'VACANCY_POS',
				'C_SORT' => 400,
				'REQUIRED' => 'N',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'dropdown',
				'FILTER_TITLE' => 'Интересующая позиция',
				'RESULTS_TABLE_TITLE' => 'Интересующая позиция',
				'arFILTER_FIELD' => array('dropdown'),
				'arANSWER' => array(
					array(
						'MESSAGE' => 'Официант',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
					array(
						'MESSAGE' => 'Пеший курьер',
						'C_SORT' => 200,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
					array(
						'MESSAGE' => 'Повар',
						'C_SORT' => 300,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Резюме',
				'SID' => 'VACANCY_FILE',
				'C_SORT' => 500,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'file',
				'FILTER_TITLE' => 'Резюме',
				'RESULTS_TABLE_TITLE' => 'Резюме',
				'arFILTER_FIELD' => array('file'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'file',
						'FIELD_PARAM' => 'class="input__file-input"'
					)
				)
			)
		);
	}
}


if($iNiyamaLsvVer < 6) {
	$iNiyamaLsvVer = 6;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('restaurants');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'restaurants',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 1000,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Рестораны'
				),
				'en' => array(
					'NAME' => 'Рестораны'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Рестораны"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Рестораны": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('restaurants-cities', 'restaurants');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Города ресторанов',
			'CODE' => 'restaurants-cities',
			'IBLOCK_TYPE_ID' => 'restaurants',
			'SITE_ID' => 's1',
			'SORT' => '900',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Города ресторанов"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Города ресторанов": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('restaurants-services', 'restaurants');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Услуги ресторанов',
			'CODE' => 'restaurants-services',
			'IBLOCK_TYPE_ID' => 'restaurants',
			'SITE_ID' => 's1',
			'SORT' => '1000',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Услуги ресторанов"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Услуги ресторанов": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}


	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('restaurants-list', 'restaurants');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Список ресторанов Пицца-Пи',
			'CODE' => 'restaurants-list',
			'IBLOCK_TYPE_ID' => 'restaurants',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(2 => 'R'),
			'LIST_PAGE_URL' => '#SITE_DIR#restaurants/',
			'SECTION_PAGE_URL' => '#SITE_DIR#restaurants/##SECTION_ID#',
			'DETAIL_PAGE_URL' => '#SITE_DIR#restaurants/#ELEMENT_CODE#.php',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
			'FIELDS' => array(
				'CODE' => array(
					'IS_REQUIRED' => 'Y', // Обязательное
					'DEFAULT_VALUE' => array(
						'UNIQUE' => 'Y', // Проверять на уникальность
						'TRANSLITERATION' => 'Y', // Транслитерировать
						'TRANS_LEN' => '100', // Максмальная длина транслитерации
						'TRANS_CASE' => 'L', // Приводить к нижнему регистру
						'TRANS_SPACE' => '-', // Символы для замены
						'TRANS_OTHER' => '-',
						'TRANS_EAT' => 'Y',
						'USE_GOOGLE' => 'N',
					),
				),
			)
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Список ресторанов"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Список ресторанов": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}


	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'R_SERVICES';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Услуги',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('restaurants-services', 'restaurants'),
			'IS_REQUIRED' => 'N',
			'USER_TYPE_SETTINGS' => array(
				'multiple' => 'Y'
			)
		);
		// ---
		$sTmpPropCode = 'R_CITY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Город',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('restaurants-cities', 'restaurants'),
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'R_SUBWAY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Станция метро',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('subway', 'data'),
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'R_RESTAURANT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Ресторан',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('departments', 'data'),
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'F_PHOTOS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Фотографии',
			'PROPERTY_TYPE' => 'F',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'S_ADDRESS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Адрес ресторана',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30,
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'S_SHEDULE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Режим работы',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30,
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'S_TEL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Контактный телефон',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30,
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'F_DOWNLOAD';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Ссылка на документ (pdf)',
			'PROPERTY_TYPE' => 'F',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30,
			'WITH_DESCRIPTION' => 'Y'
		);
		// ---
		$sTmpPropCode = 'F_DOWNLOAD_BG';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Изображение для ссылки на документ',
			'PROPERTY_TYPE' => 'F',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30
		);
		// ---
		$sTmpPropCode = 'S_WAY_SUBWAY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Как добраться на метро',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30
		);
		// ---
		$sTmpPropCode = 'S_WAY_CAR';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Как добраться на автомобиле',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 7) {
	$iNiyamaLsvVer = 7;
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('restaurants-cities', 'restaurants');
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'S_NAME_RP';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Название в родительном падеже (Кого? Чего?)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 1,
			'COL_COUNT' => 30,
			'IS_REQUIRED' => 'Y',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 8) {
	$iNiyamaLsvVer = 8;

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('news', 'misc');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Новости',
			'CODE' => 'news',
			'IBLOCK_TYPE_ID' => 'misc',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(2 => 'R'),
			'LIST_PAGE_URL' => '#SITE_DIR#about/news/',
			'SECTION_PAGE_URL' => '#SITE_DIR#about/news/',
			'DETAIL_PAGE_URL' => '#SITE_DIR#about/news/#ELEMENT_ID#.php',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'Y',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
			'FIELDS' => array(
				'ACTIVE_FROM' => array(
					'IS_REQUIRED' => 'Y', // Обязательное
					'DEFAULT_VALUE' => '=today',
				),
			)
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Новости"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Новости": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}


	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'R_RESTAURANT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Новость относится к ресторану',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('restaurants-list', 'restaurants'),
			'IS_REQUIRED' => 'N',
			'USER_TYPE_SETTINGS' => array(
				'multiple' => 'Y'
			)
		);
		// ---
		$sTmpPropCode = 'R_CITY';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Город',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('restaurants-cities', 'restaurants'),
			'IS_REQUIRED' => 'N',
			'USER_TYPE_SETTINGS' => array(
				'multiple' => 'Y'
			)
		);
		// ---
		$sTmpPropCode = 'F_PHOTOS';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Изображения',
			'PROPERTY_TYPE' => 'F',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'B_MARK';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Выделить новость',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'YesNo',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'ROW_COUNT' => 2,
			'COL_COUNT' => 30,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

}


if($iNiyamaLsvVer < 9) {
	$iNiyamaLsvVer = 9;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('customer_loyalty');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'customer_loyalty',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 2000,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Система лояльности клиентов'
				),
				'en' => array(
					'NAME' => 'Система лояльности клиентов'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Система лояльности клиентов"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Система лояльности клиентов": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('discount_levels', 'customer_loyalty');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Уровни скидок',
			'CODE' => 'discount_levels',
			'XML_ID' => 'discount_levels',
			'IBLOCK_TYPE_ID' => 'customer_loyalty',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'EDIT_FILE_AFTER' => '/local/php_interface/adv/iblock_forms/discount_levels_form.php',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Уровни скидок"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Уровни скидок": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'DISCOUNT_TYPE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Тип скидки',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'Y',
			'VALUES' => array(
				array(
					'XML_ID' => 'order',
					'VALUE' => 'За заказ',
					'DEF' => 'N',
					'SORT' => 100
				),
				array(
					'XML_ID' => 'registration',
					'VALUE' => 'За регистрацию',
					'DEF' => 'N',
					'SORT' => 200
				),
			)
		);
		// ---
		$sTmpPropCode = 'DISCOUNT_VAL_PERC';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Размер скидки, %',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'DISCOUNT_VAL_RUB';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Размер скидки, руб',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'NIYAM_CNT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Количество ниямов',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'BONUS_DISH';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Блюдо в подарок',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog'),
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LEVEL_CONDITION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Сумма всех заказов для достижения уровня',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'B_ONE_TIME';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Единоразовый бонус',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'YesNo',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'B_SINGLE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Не суммировать с другими скидками',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'YesNo',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}


	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('users_discount_levels', 'customer_loyalty');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Достигнутые уровни скидок пользователями',
			'CODE' => 'restaurants-services',
			'XML_ID' => 'restaurants-services',
			'IBLOCK_TYPE_ID' => 'customer_loyalty',
			'SITE_ID' => 's1',
			'SORT' => '5000',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Достигнутые уровни скидок пользователями"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Достигнутые уровни скидок пользователями": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'R_USER';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Пользователь',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'UserID',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'Y',
		);
		// ---
		$sTmpPropCode = 'R_DISCOUNT_LEVEL';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Уровень скидок',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('discount_levels', 'customer_loyalty'),
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

}


if($iNiyamaLsvVer < 10) {
	$iNiyamaLsvVer = 10;
	$dbItems = CForm::GetBySID('QA');
	$arForm = $dbItems->Fetch();
	$iTmpFormId = $arForm['ID'] ? $arForm['ID'] : 0;
	if(!$iTmpFormId) {
		$iTmpFormId = CForm::Set(
			array(
				'NAME' => 'Контроль качества',
				'SID' => 'QA',
				'C_SORT' => 200,
				'BUTTON' => 'Отправить',
				'DESCRIPTION' => '<p class="_big">Чем Вы недовольны? А может наоборот довольны? Что произошло, в каком ресторане или с каким <br>номером заказа при доставке? Подробно опишите ситуацию, дату, время и другие подробности.</p>',
				'DESCRIPTION_TYPE' => 'html',
				'STAT_EVENT1' => 'form',
				'STAT_EVENT2' => '',
				'arSITE' => array('s1'),
				'arMENU' => array(
					'ru' => 'Контроль качества', 
				),
			)
		);
		if($iTmpFormId) {
			?><p>Создана форма "Контроль качества"</p><?
		} else {
			?><p>Ошибка создания формы "Контроль качества"</p><?
		}

		if($iTmpFormId) {
			CFormStatus::Set(
				array(
					'FORM_ID' => $iTmpFormId,
					'C_SORT' => 100,
					'ACTIVE' => 'Y',
					'TITLE' => 'DEFAULT',
					'DESCRIPTION' => 'DEFAULT',
					'CSS' => 'statusgreen',
					'HANDLER_OUT' => '',
					'HANDLER_IN' => '',
					'DEFAULT_VALUE' => 'Y',
					'arPERMISSION_VIEW' => array(0),
					'arPERMISSION_MOVE' => array(0),
					'arPERMISSION_EDIT' => array(0),
					'arPERMISSION_DELETE' => array(0),
				)
			);
		}
	}

	if($iTmpFormId) {
		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Представьтесь',
				'SID' => 'QA_NAME',
				'C_SORT' => 100,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Представьтесь',
				'RESULTS_TABLE_TITLE' => 'Представьтесь',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Что произошло?',
				'SID' => 'QA_MESSAGE',
				'C_SORT' => 200,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'textarea',
				'FILTER_TITLE' => 'Что произошло?',
				'RESULTS_TABLE_TITLE' => 'Что произошло?',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'textarea',
						'FIELD_PARAM' => 'class="textarea"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Где это произошло?',
				'SID' => 'QA_NOCENT',
				'C_SORT' => 300,
				'REQUIRED' => 'N',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'dropdown',
				'FILTER_TITLE' => 'Где это произошло?',
				'RESULTS_TABLE_TITLE' => 'Где это произошло?',
				'arFILTER_FIELD' => array('dropdown'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
					array(
						'MESSAGE' => 'Место 1',
						'C_SORT' => 200,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
					array(
						'MESSAGE' => 'Место 2',
						'C_SORT' => 300,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'dropdown',
						'FIELD_PARAM' => ''
					),
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Когда произошло?',
				'SID' => 'QA_DATE',
				'C_SORT' => 400,
				'REQUIRED' => 'N',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Когда произошло?',
				'RESULTS_TABLE_TITLE' => 'Когда произошло?',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'onclick="return {\'inputDate\': {}}" class="input js-widget" data-maxdate="true"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Контактный телефон',
				'SID' => 'QA_TEL',
				'C_SORT' => 500,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Контактный телефон',
				'RESULTS_TABLE_TITLE' => 'Контактный телефон',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Электронная почта',
				'SID' => 'QA_EMAIL',
				'C_SORT' => 600,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'email',
				'FILTER_TITLE' => 'Электронная почта',
				'RESULTS_TABLE_TITLE' => 'Электронная почта',
				'arFILTER_FIELD' => array('email'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'email',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);
	}

}


if($iNiyamaLsvVer < 11) {
	$iNiyamaLsvVer = 11;

	//
	// ---
	//
	$dbItems = CIBlockType::GetById('orders');
	if(!$dbItems->Fetch()) {
		$arFields = array(
			'ID' => 'orders',
			'SECTIONS' => 'Y',
			'IN_RSS' => 'N',
			'SORT' => 5000,
			'LANG' => array(
				'ru' => array(
					'NAME' => 'Система заказов'
				),
				'en' => array(
					'NAME' => 'Система заказов'
				),
			)
		);
		$obIBlockType = new CIBlockType();
		$bResult = $obIBlockType->Add($arFields);
		if($bResult) {
			?><p>Добавлен тип инфоблоков "Система заказов"</p><?
		} else {
			?><p>Ошибка добавления типа инфоблоков "Система заказов": <?=$obIBlockType->LAST_ERROR?></p><?
		}
	}

	//
	// ---
	//
	/*
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('delivery-regions', 'orders');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Регионы доставки',
			'CODE' => 'delivery-regions',
			'XML_ID' => 'delivery-regions',
			'IBLOCK_TYPE_ID' => 'orders',
			'SITE_ID' => 's1',
			'SORT' => '1000',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'EDIT_FILE_AFTER' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Регионы доставки"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Регионы доставки": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}
	*/

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('delivery', 'orders');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Параметры доставки',
			'CODE' => 'delivery',
			'XML_ID' => 'delivery',
			'IBLOCK_TYPE_ID' => 'orders',
			'SITE_ID' => 's1',
			'SORT' => '100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'EDIT_FILE_AFTER' => '/local/php_interface/adv/iblock_forms/delivery_form.php',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Параметры доставки"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Параметры доставки": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'R_REGION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Регион',
			'PROPERTY_TYPE' => 'E',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'EList',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'LINK_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('delivery-regions', 'orders'),
			'IS_REQUIRED' => 'Y',
			'USER_TYPE_SETTINGS' => array(
				'multiple' => 'Y',
				'size' => 15
			)
		);
		// ---
		$sTmpPropCode = 'B_MKAD';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'В пределах МКАД',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'YesNo',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'S_TIME_PERIOD';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Период доставки (hh:mm-hh:mm)',
			'HINT' => 'Если ничего не задано, то подразумевается круглосуточная доставка',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'D_MIN_ORDER_PRICE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Минимальная сумма заказа, руб.',
			'HINT' => '',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'D_DELIVERY_PRICE';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Стоимость доставки, руб.',
			'HINT' => 'Если ничего не задано, то подразумевается бесплатная доставка',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 12) {
	$iNiyamaLsvVer = 12;
	CNiyamaCustomSettings::SetStringValue('Максимальное количество блюда в заказе', 'order_dish_max_quantity', '12');
	CNiyamaCustomSettings::SetStringValue('Номер телефона для альтернативного оформления заказа', 'order_alt_tel', '+7 495 781-781-9');
	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'D',
			'SORT' => '1',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Черновик',
					'DESCRIPTION' => 'Заказы, которые находятся в стадии оформления'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}

	$dbTmpItems = CSalePersonType::GetList(
		array(),
		array(
			'NAME' => 'Физическое лицо'
		)
	);
	if(!$dbTmpItems->SelectedRowsCount()) {
		$iPersonTypeId = CSalePersonType::Add(
			array(
				'LID' => 's1',
				'NAME' => 'Физическое лицо',
				'SORT' => '100',
				'ACTIVE' => 'Y'
			)
		);
		if($iPersonTypeId) {
			echo '<p>Добавлен тип плательщика: Физическое лицо</p>';
		}
	}
}

if($iNiyamaLsvVer < 14) {
	$iNiyamaLsvVer = 14;
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('subway', 'data');
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'LINE';
		$iPropsSort = 800;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Ветка метро',
			'PROPERTY_TYPE' => 'L',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
			'VALUES' => array(
				array(
					'XML_ID' => 'M1',
					'VALUE' => 'Сокольническая линия (Москва)',
					'DEF' => 'N',
					'SORT' => 100
				),
				array(
					'XML_ID' => 'M2',
					'VALUE' => 'Замоскворецкая линия (Москва)',
					'DEF' => 'N',
					'SORT' => 200
				),
				array(
					'XML_ID' => 'M3',
					'VALUE' => 'Арбатско-Покровская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 300
				),
				array(
					'XML_ID' => 'M4',
					'VALUE' => 'Филевская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 400
				),
				array(
					'XML_ID' => 'M5',
					'VALUE' => 'Кольцевая линия (Москва)',
					'DEF' => 'N',
					'SORT' => 500
				),
				array(
					'XML_ID' => 'M6',
					'VALUE' => 'Калужско-Рижская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 600
				),
				array(
					'XML_ID' => 'M7',
					'VALUE' => 'Таганско-Краснопресненская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 700
				),
				array(
					'XML_ID' => 'M8',
					'VALUE' => 'Калининская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 800
				),
				array(
					'XML_ID' => 'M9',
					'VALUE' => 'Серпуховско-Тимирязевская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 900
				),
				array(
					'XML_ID' => 'M10',
					'VALUE' => 'Люблинско-Дмитровская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 1000
				),
				array(
					'XML_ID' => 'M11',
					'VALUE' => 'Каховская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 1100
				),
				array(
					'XML_ID' => 'M12',
					'VALUE' => 'Бутовская линия (Москва)',
					'DEF' => 'N',
					'SORT' => 1200
				),
			)
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 15) {
	$iNiyamaLsvVer = 15;

	$iOrderCustomerGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Контактная информация') {
			$iOrderCustomerGroupId = $arItem['ID'];
		}
	}

	if(!$iOrderCustomerGroupId) {
		$iOrderCustomerGroupId = CSaleOrderPropsGroup::Add(
			array(
				'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
				'NAME' => 'Контактная информация',
				'SORT' => 100
			)
		);
		if($iOrderCustomerGroupId) {
			echo '<p>Добавлена группа свойств заказа: ['.$iOrderCustomerGroupId.'] Контактная информация</p>';
		}
	}

	if($iOrderCustomerGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderCustomerGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}
		if(!$arTmpOrderPropsList['CUSTOMER_NAME']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Имя',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 100,
					'CODE' => 'CUSTOMER_NAME',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderCustomerGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Имя</p>';
			}
		}

		if(!$arTmpOrderPropsList['CUSTOMER_LAST_NAME']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Фамилия',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 200,
					'CODE' => 'CUSTOMER_LAST_NAME',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderCustomerGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Фамилия</p>';
			}
		}

		if(!$arTmpOrderPropsList['CUSTOMER_PHONE']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Телефон',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 300,
					'CODE' => 'CUSTOMER_PHONE',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderCustomerGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Телефон</p>';
			}
		}

		if(!$arTmpOrderPropsList['CUSTOMER_EMAIL']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Электронная почта',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 400,
					'CODE' => 'CUSTOMER_EMAIL',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderCustomerGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Электронная почта</p>';
			}
		}
	}

	//
	// ---
	//
	$iOrderDeliveryGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Информация о доставке') {
			$iOrderDeliveryGroupId = $arItem['ID'];
		}
	}

	if(!$iOrderDeliveryGroupId) {
		$iOrderDeliveryGroupId = CSaleOrderPropsGroup::Add(
			array(
				'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
				'NAME' => 'Информация о доставке',
				'SORT' => 100
			)
		);
		if($iOrderCustomerGroupId) {
			echo '<p>Добавлена группа свойств заказа: ['.$iOrderCustomerGroupId.'] Информация о доставке</p>';
		}
	}

	if($iOrderDeliveryGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderDeliveryGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}
		if(!$arTmpOrderPropsList['DELIVERY_REGION']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Город доставки',
					'TYPE' => 'LOCATION',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 100,
					'CODE' => 'DELIVERY_REGION',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'Y',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Имя</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_SUBWAY']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Станция метро',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 200,
					'CODE' => 'DELIVERY_SUBWAY',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Станция метро</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_ADDRESS']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Почтовый адрес: улица, дом',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 300,
					'CODE' => 'DELIVERY_ADDRESS',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Почтовый адрес: улица, дом</p>';
			}
		}
		
		



		if(!$arTmpOrderPropsList['DELIVERY_APART']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Квартира',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 400,
					'CODE' => 'DELIVERY_APART',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Квартира</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_PORCH']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Подъезд',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 500,
					'CODE' => 'DELIVERY_PORCH',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Подъезд</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_INTERCOM']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Домофон',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 600,
					'CODE' => 'DELIVERY_INTERCOM',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Домофон</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_FLOOR']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Этаж',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 700,
					'CODE' => 'DELIVERY_FLOOR',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Этаж</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_BY_DATE']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Дата ожидания заказа',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 800,
					'CODE' => 'DELIVERY_BY_DATE',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Дата ожидания заказа</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_BY_TIME']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Время ожидания заказа',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 900,
					'CODE' => 'DELIVERY_BY_TIME',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Время ожидания заказа</p>';
			}
		}
	}
}

/*
if($iNiyamaLsvVer < 16) {
	$iNiyamaLsvVer = 16;

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('payments', 'orders');
	if(!$iTmpIBlockId) {
		$arFields = array(
			'ACTIVE' => 'Y',
			'NAME' => 'Способы оплаты',
			'CODE' => 'payments',
			'XML_ID' => 'payments',
			'IBLOCK_TYPE_ID' => 'orders',
			'SITE_ID' => 's1',
			'SORT' => '1100',
			'GROUP_ID' => array(),
			'LIST_PAGE_URL' => '',
			'SECTION_PAGE_URL' => '',
			'DETAIL_PAGE_URL' => '',
			'INDEX_SECTION' => 'N',
			'INDEX_ELEMENT' => 'N',
			'EDIT_FILE_BEFORE' => '',
			'EDIT_FILE_AFTER' => '',
			'VERSION' => 2,
		);
		$obIBlock = new CIBlock();
		$iTmpIBlockId = $obIBlock->Add($arFields);
		if($iTmpIBlockId) {
			?><p>Создан инфоблок "Способы оплаты"</p><?
		} else {
			?><p>Ошибка добавления инфоблока "Способы оплаты": <?=$obIBlock->LAST_ERROR?></p><?
		}
	}
}
*/


if($iNiyamaLsvVer < 17) {
	$iNiyamaLsvVer = 17;

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('delivery', 'orders');

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 0;
		// ---
		$sTmpPropCode = 'R_REGION';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Регион',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => 'SaleLoc',
			'MULTIPLE' => 'Y',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'Y',
			'USER_TYPE_SETTINGS' => array(
				'multiple' => 'Y',
				'size' => 15
			)
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['ID'];
		}
		// обновим или добавим свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			} else {
				$obIBlockProperty = new CIBlockProperty();
				$bResult = $obIBlockProperty->Update($arAddProps[$arPropMeta['CODE']], $arPropMeta);
				if($bResult) {
					echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}


if($iNiyamaLsvVer < 18) {
	$iNiyamaLsvVer = 18;
	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'E',
			'SORT' => '2',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Экспорт в FO',
					'DESCRIPTION' => 'Передача заказа в FO'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}
}


if($iNiyamaLsvVer < 19) {
	$iNiyamaLsvVer = 19;

	$iOrderDeliveryGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Информация о доставке') {
			$iOrderDeliveryGroupId = $arItem['ID'];
		}
	}


	if($iOrderDeliveryGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderDeliveryGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}

		if(!$arTmpOrderPropsList['DELIVERY_TYPE']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Тип доставки',
					'TYPE' => 'SELECT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 10,
					'CODE' => 'DELIVERY_TYPE',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Тип доставки</p>';

				$arVariants = array(
					array(
						'ORDER_PROPS_ID' => $iTmpOrderProp,
						'NAME' => 'Доставка курьером',
						'VALUE' => 'courier',
						'SORT' => '100',
						'DESCRIPTION' => ''
					),
					array(
						'ORDER_PROPS_ID' => $iTmpOrderProp,
						'NAME' => 'Самовывоз',
						'VALUE' => 'self',
						'SORT' => '200',
						'DESCRIPTION' => ''
					),
				);
				foreach($arVariants as $arFields) {
					CSaleOrderPropsVariant::Add($arFields);
				}
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_RESTAURANT']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Ресторан самовывоза',
					'TYPE' => 'SELECT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 1000,
					'CODE' => 'DELIVERY_RESTAURANT',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => 'Внимание! Список синхронизируется автоматически со справочником ресторанов',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Ресторан самовывоза</p>';
			}
			// заполним значениями список
			CNiyamaIBlockDepartments::UpdateSaleOrderProp();
		}

		if(!$arTmpOrderPropsList['DELIVERY_CHANGE_AMOUNT']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Сдача с суммы',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 1100,
					'CODE' => 'DELIVERY_CHANGE_AMOUNT',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Сдача с суммы</p>';
			}
		}

		if(!$arTmpOrderPropsList['DELIVERY_WITHIN_MKAD']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Доставка в пределах МКАД',
					'TYPE' => 'CHECKBOX',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 1100,
					'CODE' => 'DELIVERY_WITHIN_MKAD',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Доставка в пределах МКАД</p>';
			}
		}
	}
}

if($iNiyamaLsvVer < 20) {
	$iNiyamaLsvVer = 20;
	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'R',
			'SORT' => '3',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Принят на стороне FO',
					'DESCRIPTION' => 'Заказ принят на стороне FO'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_FO_ORDER_ID';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 100,
		'USER_TYPE_ID' => 'Varchar',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$sTmpCode = 'UF_FO_ORDER_CODE';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 100,
		'USER_TYPE_ID' => 'Varchar',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}
}

if($iNiyamaLsvVer < 21) {
	$iNiyamaLsvVer = 21;
	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'P',
			'SORT' => '10',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Ожидает оплаты',
					'DESCRIPTION' => 'Заказ ожидает оплаты через сервис онлайн-платежей'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}
}

if($iNiyamaLsvVer < 22) {
	$iNiyamaLsvVer = 22;

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_FO_WAS_SEND';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 100,
		'USER_TYPE_ID' => 'boolean',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}


	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::ExportFailOrders2FO();' => array(
			'module' => 'sale',
			'period' => 'N',
			'interval' => 900,
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1100
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}

if($iNiyamaLsvVer < 23) {
	$iNiyamaLsvVer = 23;
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('departments', 'data');
	//
	// ---
	//
	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 700;
		// ---
		$sTmpPropCode = 'LONG_ALT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Долгота (не из FO)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// ---
		$sTmpPropCode = 'LAT_ALT';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Широта (не из FO)',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'C',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['CODE'];
		}
		// добавим недостающие свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}


if($iNiyamaLsvVer < 24) {
	$iNiyamaLsvVer = 24;

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_FO_USER_LOGIN';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 100,
		'USER_TYPE_ID' => 'string',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}


	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::UpdateOrderStatuses();' => array(
			'module' => 'sale',
			'period' => 'N',
			'interval' => 900,
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1100
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}


if($iNiyamaLsvVer < 25) {
	$iNiyamaLsvVer = 25;

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_ACCEPT_DATE';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 100,
		'USER_TYPE_ID' => 'datetime',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}

	CNiyamaCustomSettings::SetStringValue('Количество карточек блюд для промо-блока оформления заказа', 'order_info_promo_items_cnt', '4');
}

if($iNiyamaLsvVer < 26) {
	$iNiyamaLsvVer = 26;

	CNiyamaCustomSettings::SetStringValue('Средняя сумма заказа на одного человека', 'order_sum_by_person', '1000');
}

if($iNiyamaLsvVer < 27) {
	$iNiyamaLsvVer = 27;
	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'S',
			'SORT' => '40',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Отложен',
					'DESCRIPTION' => 'Заказ отложен, ожидает выполнения'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}

	$sTmpResult = CSaleStatus::Add(
		array(
			'ID' => 'T',
			'SORT' => '50',
			'LANG' => array(
				array(
					'LID' => 'ru',
					'NAME' => 'Доставляется',
					'DESCRIPTION' => 'Заказ доставляется'
				)
			),
		)
	);
	if($sTmpResult) {
		echo '<p>Добавлен статус заказа: '.$sTmpResult.'</p>';
	}

}

if($iNiyamaLsvVer < 28) {
	$iNiyamaLsvVer = 28;
	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('card_data', 'NIYAMA_FOR_USER');

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 2700;
		// ---
		$sTmpPropCode = 'OFFERED';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Дата выдачи',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['ID'];
		}
		// обновим или добавим свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			} else {
				$obIBlockProperty = new CIBlockProperty();
				$bResult = $obIBlockProperty->Update($arAddProps[$arPropMeta['CODE']], $arPropMeta);
				if($bResult) {
					echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 29) {
	$iNiyamaLsvVer = 29;

	CNiyamaCustomSettings::SetStringValue('ApiId ВКонтакте', 'vk_api_id', '22710713');
	CNiyamaCustomSettings::SetStringValue('Токен для "КЛАДР в облаке"', 'kladr_cloud_token', '53e8f405fca9162602892086');
	CNiyamaCustomSettings::SetStringValue('Ключ для "КЛАДР в облаке"', 'kladr_cloud_key', 'e352a611726b6e7455e97856ac27a36aaadcf7bf');
}

if($iNiyamaLsvVer < 30) {
	$iNiyamaLsvVer = 30;

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-base', 'catalog');

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		// ---
		$sTmpPropCode = 'WEIGHT_FULL';
		$iPropsSort = 750;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Оригинальное значение веса',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'WEIGHT';
		$iPropsSort = 800;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Вес',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'CALORIC';
		$iPropsSort = 850;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Калорийность',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['ID'];
		}
		// обновим или добавим свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			} else {
				$obIBlockProperty = new CIBlockProperty();
				$bResult = $obIBlockProperty->Update($arAddProps[$arPropMeta['CODE']], $arPropMeta);
				if($bResult) {
					echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('general-site-menu', 'catalog');

	if($iTmpIBlockId) {
		$obIBlock = new CIBlock();
		$obIBlock->Update(
			$iTmpIBlockId, 
			array(
				'LIST_PAGE_URL' => '/',
				'SECTION_PAGE_URL' => '/#SECTION_CODE#/',
				'DETAIL_PAGE_URL' => '/#SECTION_CODE#/#ELEMENT_CODE#/',
			)
		);

		$arTmpPropertiesMeta = array();
		// ---
		$sTmpPropCode = 'WEIGHT';
		$iPropsSort = 700;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Вес блюда',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'N',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// ---
		$sTmpPropCode = 'NUM_OF_CALORIES';
		$iPropsSort = 1000;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Кол-во кал. в блюде',
			'PROPERTY_TYPE' => 'N',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'N',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['ID'];
		}
		// обновим или добавим свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			} else {
				$obIBlockProperty = new CIBlockProperty();
				$bResult = $obIBlockProperty->Update($arAddProps[$arPropMeta['CODE']], $arPropMeta);
				if($bResult) {
					echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 31) {
	$iNiyamaLsvVer = 31;

	$iOrderDeliveryGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Информация о доставке') {
			$iOrderDeliveryGroupId = $arItem['ID'];
		}
	}

	if(!$iOrderDeliveryGroupId) {
		$iOrderDeliveryGroupId = CSaleOrderPropsGroup::Add(
			array(
				'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
				'NAME' => 'Информация о доставке',
				'SORT' => 100
			)
		);
		if($iOrderCustomerGroupId) {
			echo '<p>Добавлена группа свойств заказа: ['.$iOrderCustomerGroupId.'] Информация о доставке</p>';
		}
	}

	if($iOrderDeliveryGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderDeliveryGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}
		if(!$arTmpOrderPropsList['DELIVERY_HOUSE']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Дом',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 350,
					'CODE' => 'DELIVERY_HOUSE',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] дом</p>';
			}
		}
	}
}

if($iNiyamaLsvVer < 32) {
	$iNiyamaLsvVer = 32;

	$iOrderDeliveryGroupId = 0;
	$dbItems = CSaleOrderPropsGroup::GetList(
		array(),
		array(
			'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId()
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['NAME'] == 'Информация о доставке') {
			$iOrderDeliveryGroupId = $arItem['ID'];
		}
	}

	if($iOrderDeliveryGroupId) {
		$arTmpOrderPropsList = array();
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PROPS_GROUP_ID' => $iOrderDeliveryGroupId
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arTmpOrderPropsList[$arItem['CODE']] = $arItem;
		}

		if(!$arTmpOrderPropsList['DELIVERY_FO_ADDRESS']) {
			$iTmpOrderProp = CSaleOrderProps::Add(
				array(
					'PERSON_TYPE_ID' => CNiyamaOrders::GetPrivateClientPersonTypeId(),
					'NAME' => 'Адрес доставки (FO)',
					'TYPE' => 'TEXT',
					'REQUIED' => 'N',
					'DEFAULT_VALUE' => '',
					'SORT' => 9000,
					'CODE' => 'DELIVERY_FO_ADDRESS',
					'USER_PROPS' => 'Y',
					'IS_LOCATION' => 'N',
					'IS_LOCATION4TAX' => 'N',
					'PROPS_GROUP_ID' => $iOrderDeliveryGroupId,
					'SIZE1' => 0,
					'SIZE2' => 0,
					'DESCRIPTION' => '',
					'IS_EMAIL' => 'N',
					'IS_PROFILE_NAME' => 'N',
					'IS_PAYER' => 'N',
					'UTIL' => 'Y'
				)
			);
			if($iTmpOrderProp) {
				echo '<p>Добавлено свойство заказа: ['.$iTmpOrderProp.'] Адрес доставки (FO)</p>';
			}
		}
	}
}

if($iNiyamaLsvVer < 33) {
	$iNiyamaLsvVer = 33;

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_RECEIVED_FROM_FO';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 500,
		'USER_TYPE_ID' => 'boolean',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}
}

if($iNiyamaLsvVer < 34) {
	$iNiyamaLsvVer = 34;

	$arTmpPropertiesMeta = array();
	$sTmpCode = 'UF_FO_DATE_CREATE';
	$arTmpPropertiesMeta[$sTmpCode] = array(
		'ENTITY_ID' => 'ORDER',
		'FIELD_NAME' => $sTmpCode,
		'EDIT_FORM_LABEL' => array(
			'ru' => $sTmpCode
		),
		'SORT' => 500,
		'USER_TYPE_ID' => 'datetime',
		'XML_ID' => $sTmpCode,
		'SHOW_FILTER' => 'Y',
		'SETTINGS' => array(
			'DEFAULT_VALUE' => ''
		),
	);
	$obUserTypeEntity = new CUserTypeEntity();
	foreach($arTmpPropertiesMeta as $arFields) {
		$iTmpId = $obUserTypeEntity->Add($arFields);
		if($iTmpId) {
			echo '<p>Добавлено поле: '.$arFields['FIELD_NAME'].'</p>';
		}
	}
}

if($iNiyamaLsvVer < 35) {
	$iNiyamaLsvVer = 35;
	//
	// ---
	//
	$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('card_data', 'NIYAMA_FOR_USER');

	if($iTmpIBlockId) {
		$arTmpPropertiesMeta = array();
		$iPropsSort = 1650;
		// ---
		$sTmpPropCode = 'SECOND_NAME';
		$iPropsSort += 100;
		$arTmpPropertiesMeta[$sTmpPropCode] = array(
			'NAME' => 'Отчество',
			'PROPERTY_TYPE' => 'S',
			'LIST_TYPE' => 'L',
			'SEARCHABLE' => 'N',
			'USER_TYPE' => '',
			'MULTIPLE' => 'N',
			'ACTIVE' => 'Y',
			'FILTRABLE' => 'Y',
			'SORT' => $iPropsSort,
			'CODE' => $sTmpPropCode,
			'IS_REQUIRED' => 'N',
		);
		// список текущих свойств инфоблока
		$arAddProps = array();
		$dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
		while($arItem = $dbItems->Fetch()) {
			$arAddProps[$arItem['CODE']] = $arItem['ID'];
		}
		// обновим или добавим свойства в инфоблок
		foreach($arTmpPropertiesMeta as $arPropMeta) {
			if(!isset($arAddProps[$arPropMeta['CODE']])) {
				$arPropMeta['IBLOCK_ID'] = $iTmpIBlockId; 
				$obIBlockProperty = new CIBlockProperty();
				$iPropId = $obIBlockProperty->Add($arPropMeta);
				if($iPropId) {
					echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
				}
			} else {
				$obIBlockProperty = new CIBlockProperty();
				$bResult = $obIBlockProperty->Update($arAddProps[$arPropMeta['CODE']], $arPropMeta);
				if($bResult) {
					echo '<p>Обновлено свойство: ['.$arAddProps[$arPropMeta['CODE']].'] '.$arPropMeta['CODE'].'</p>';
				}
			}
		}
	}

	$sEventTypeCode = 'NIYAMA_CARD_CONFIRMATION';
	$sEventTypeName = 'Подтверждение идентификации дисконтной карты';
	$dbItems = CEventType::GetList(
		array(
			'TYPE_ID' => $sEventTypeCode
		)
	);
	if(!$arItem = $dbItems->Fetch()) {
		$sEventDescription = '
#CONFIRM_URL# - Ссылка для подтверждения
#CARD_NUM# - Номер карты
#CARD_TYPE# - Тип карты
#CARD_EMAIL# - E-mail, указанный в карте
#CARD_TEL# - Телефон, указанный в карте
#AUTHOR_USER_LOGIN# - Логин отправителя
#AUTHOR_USER_ID# - ID отправителя
#DATE# - Дата
';
		$obEventType = new CEventType();
		$iEventTypeId = $obEventType->Add(
			array(
				'LID' => 'ru',
				'EVENT_NAME' => $sEventTypeCode,
				'NAME' => $sEventTypeName,
				'DESCRIPTION' => $sEventDescription
			)
		);
		if($iEventTypeId) {
			?><p>Создан новый тип почтовых уведомлений <?=$sEventTypeCode?></p><?
		} else {
			?><p>Ошибка создания нового типа почтовых уведомлений <?=$sEventTypeCode?></p><?
		}
	}

	$dbItems = CEventMessage::GetList(
		$sBy = 'id',
		$sOrder = 'asc',
		array(
			'TYPE_ID' => $sEventTypeCode,
			'SITE_ID' => 's1'
		)
	);
	if(!$arItem = $dbItems->Fetch()) {
		$sEventMessage = '
Ссылка для подтверждения: http://#SERVER_NAME##CONFIRM_URL#
Номер карты: #CARD_NUM#
Тип карты: #CARD_TYPE#
Логин отправителя: #AUTHOR_USER_LOGIN#
ID отправителя: #AUTHOR_USER_ID#
Дата: #DATE#
E-mail, указанный в карте: #CARD_EMAIL#
Телефон, указанный в карте: #CARD_TEL#

------------------
Сообщение сгенерировано автоматически.
';
		$obEventMessage = new CEventMessage();
		$iEventMessageId = $obEventMessage->Add(
			array(
				'ACTIVE' => 'Y',
				'LID' => 's1',
				'EVENT_NAME' => $sEventTypeCode,
				'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
				'EMAIL_TO' => '#CARD_EMAIL#',
				'SUBJECT' => '#SITE_NAME#: '.$sEventTypeName,
				'BODY_TYPE' => 'text',
				'MESSAGE' => $sEventMessage
			)
		);
		if($iEventMessageId) {
			?><p>Создан новый шаблон почтовых уведомлений типа <?=$sEventTypeCode?></p><?
		} else {
			?><p>Ошибка создания нового шаблона почтовых уведомлений типа <?=$sEventTypeCode?></p><?
		}
	}
}

if($iNiyamaLsvVer < 36) {
	$iNiyamaLsvVer = 36;

	$dbItems = CForm::GetBySID('SUPPORT');
	$arForm = $dbItems->Fetch();
	$iTmpFormId = $arForm['ID'] ? $arForm['ID'] : 0;
	if(!$iTmpFormId) {
		$iTmpFormId = CForm::Set(
			array(
				'NAME' => 'Обращения в тех.поддержку',
				'SID' => 'SUPPORT',
				'C_SORT' => 100,
				'BUTTON' => 'Отправить',
				'DESCRIPTION' => '',
				'DESCRIPTION_TYPE' => 'html',
				'STAT_EVENT1' => 'form',
				'STAT_EVENT2' => '',
				'arSITE' => array('s1'),
				'arMENU' => array(
					'ru' => 'Обращения в тех.поддержку', 
				),
			)
		);
		if($iTmpFormId) {
			?><p>Создана форма "Обращения в тех.поддержку"</p><?
		} else {
			?><p>Ошибка создания формы "Обращения в тех.поддержку"</p><?
		}

		if($iTmpFormId) {
			CFormStatus::Set(
				array(
					'FORM_ID' => $iTmpFormId,
					'C_SORT' => 100,
					'ACTIVE' => 'Y',
					'TITLE' => 'DEFAULT',
					'DESCRIPTION' => 'DEFAULT',
					'CSS' => 'statusgreen',
					'HANDLER_OUT' => '',
					'HANDLER_IN' => '',
					'DEFAULT_VALUE' => 'Y',
					'arPERMISSION_VIEW' => array(0),
					'arPERMISSION_MOVE' => array(0),
					'arPERMISSION_EDIT' => array(0),
					'arPERMISSION_DELETE' => array(0),
				)
			);
		}
	}

	if($iTmpFormId) {
		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Имя',
				'SID' => 'SUPPORT_NAME',
				'C_SORT' => 100,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Имя',
				'RESULTS_TABLE_TITLE' => 'Имя',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Фамилия',
				'SID' => 'SUPPORT_LAST_NAME',
				'C_SORT' => 200,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Фамилия',
				'RESULTS_TABLE_TITLE' => 'Фамилия',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Телефон',
				'SID' => 'SUPPORT_TEL',
				'C_SORT' => 300,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'text',
				'FILTER_TITLE' => 'Телефон',
				'RESULTS_TABLE_TITLE' => 'Телефон',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Электронная почта',
				'SID' => 'SUPPORT_EMAIL',
				'C_SORT' => 400,
				'REQUIRED' => 'Y',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'email',
				'FILTER_TITLE' => 'Электронная почта',
				'RESULTS_TABLE_TITLE' => 'Электронная почта',
				'arFILTER_FIELD' => array('email'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'email',
						'FIELD_PARAM' => 'class="input"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Текст обращения',
				'SID' => 'SUPPORT_MESSAGE',
				'C_SORT' => 500,
				'REQUIRED' => 'N',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'textarea',
				'FILTER_TITLE' => 'Текст обращения',
				'RESULTS_TABLE_TITLE' => 'Текст обращения',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'textarea',
						'FIELD_PARAM' => 'class="textarea"'
					)
				)
			)
		);

		$iTmpFormFieldId = CFormField::Set(
			array(
				'FORM_ID' => $iTmpFormId,
				'ACTIVE' => 'Y',
				'TITLE' => 'Служебная информация',
				'SID' => 'SUPPORT_ADMIN_MSG',
				'C_SORT' => 600,
				'REQUIRED' => 'N',
				'ADDITIONAL' => 'N',
				'IN_RESULTS_TABLE' => 'Y',
				'IN_EXCEL_TABLE' => 'Y',
				'FIELD_TYPE' => 'textarea',
				'FILTER_TITLE' => 'Служебная информация',
				'RESULTS_TABLE_TITLE' => 'Служебная информация',
				'arFILTER_FIELD' => array('text'),
				'arANSWER' => array(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'textarea',
						'FIELD_PARAM' => 'class="textarea"'
					)
				)
			)
		);
	}
}

if($iNiyamaLsvVer < 37) {
	$iNiyamaLsvVer = 37;
	$sTmpEntityName = 'CardsIdentify';
	$sTmpEntityTableName = 'niyama_cards_identify';
	$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
	if(!$iTmpEntityId) {
		$obTmpResult = CHLEntity::Add(
			array(
				'NAME' => $sTmpEntityName,
				'TABLE_NAME' => $sTmpEntityTableName
			)
		);

		if($obTmpResult->IsSuccess()) {
			$iTmpEntityId = $obTmpResult->GetId();
		}

		if($iTmpEntityId) {
			?><p>Создана HL-сущность "<?=$sTmpEntityName?>"</p><?
		} else {
			echo '<p>'.implode('<br />', $obTmpResult->GetErrorMessages()).'</p>';
		}
	}

	if($iTmpEntityId) {
		$arAddFields = array(
			'UF_USER_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'Пользователь'
			),
			'UF_CARDNUM' => array(
				'USER_TYPE_ID' => 'Varchar',
				'NAME' => 'Номер карты'
			),
			'UF_CHECKWORD' => array(
				'USER_TYPE_ID' => 'Varchar',
				'NAME' => 'Контрольное слово для проверки'
			),
			'UF_MANUAL_CHECK' => array(
				'USER_TYPE_ID' => 'boolean',
				'NAME' => 'Идентификация через тех.поддержку'
			),
			'UF_DATE_CREATE' => array(
				'USER_TYPE_ID' => 'datetime',
				'NAME' => 'Дата создания записи'
			),
		);

		$arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);
		foreach($arAddFields as $sTmpFieldName => $arAddParams) {
			if(!isset($arTmpCurFields[$sTmpFieldName])) {
				$arTmpParams = array_merge(
					array(
						'FIELD_NAME' => $sTmpFieldName,
						'EDIT_FORM_LABEL' => '',
						'USER_TYPE_ID' => 'string',
						'MULTIPLE' => 'N',
						'SHOW_FILTER' => 'I',
						'SHOW_IN_LIST' => 'Y',
						'EDIT_IN_LIST' => 'Y',
						'IS_SEARCHABLE' => 'N',
						'SETTINGS' => array(
							'SIZE' => '50'
						),
						'LIST_COLUMN_LABEL' => '',
						'LIST_FILTER_LABEL' => '',
						'ERROR_MESSAGE' => '',
						'HELP_MESSAGE' => ''
					),
					$arAddParams
				);
				if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
					$arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
					$arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
					$arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
				}
				$iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
				if($iTmpUserFieldId) {
					echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
				} else {
					echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 38) {
	$iNiyamaLsvVer = 38;
	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::ClearCardsIdentify();' => array(
			'module' => 'highloadblock',
			'period' => 'N',
			'interval' => 86400,
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1000
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}

if($iNiyamaLsvVer < 39) {
	$iNiyamaLsvVer = 39;
	$sTmpEntityName = 'CardsHistory';
	$sTmpEntityTableName = 'niyama_cards_history';
	$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
	if(!$iTmpEntityId) {
		$obTmpResult = CHLEntity::Add(
			array(
				'NAME' => $sTmpEntityName,
				'TABLE_NAME' => $sTmpEntityTableName
			)
		);

		if($obTmpResult->IsSuccess()) {
			$iTmpEntityId = $obTmpResult->GetId();
		}

		if($iTmpEntityId) {
			?><p>Создана HL-сущность "<?=$sTmpEntityName?>"</p><?
		} else {
			echo '<p>'.implode('<br />', $obTmpResult->GetErrorMessages()).'</p>';
		}
	}

	if($iTmpEntityId) {
		$arAddFields = array(
			'UF_DATE_CREATE' => array(
				'USER_TYPE_ID' => 'datetime',
				'NAME' => 'Дата создания записи'
			),
			/*
			'UF_ELEMENT_ID' => array(
				'USER_TYPE_ID' => 'iblock_element',
				'NAME' => 'Элемент инфоблока',
				'SETTINGS' => array(
					'IBLOCK_TYPE_ID' => 'NIYAMA_FOR_USER',
					'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCodeEx('card_data', 'NIYAMA_FOR_USER')
				)
			),
			*/
			'UF_ELEMENT_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'Элемент инфоблока',
			),
			'UF_USER_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'Пользователь'
			),
			'UF_CARDNUM' => array(
				'USER_TYPE_ID' => 'Varchar',
				'NAME' => 'Номер карты'
			),
			'UF_NAME' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Имя'
			),
			'UF_LAST_NAME' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Фамилия'
			),
			'UF_SECOND_NAME' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Отчество'
			),
			'UF_BIRTHDAY' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Дата рождения'
			),
			'UF_PHONE' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Телефон'
			),
			'UF_EMAIL' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'E-mail'
			),
			'UF_CITY' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Город'
			),
			'UF_STREET' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Улица'
			),
			'UF_HOME' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Дом'
			),
			'UF_HOUSING' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Корпус'
			),
			'UF_BUILDING' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Строение'
			),
			'UF_APARTMENT' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Квартира'
			),
			'UF_DISCOUNT' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Скидка'
			),
			'UF_OFFERED' => array(
				'USER_TYPE_ID' => 'string',
				'NAME' => 'Дата выдачи'
			),
		);

		$arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);
		foreach($arAddFields as $sTmpFieldName => $arAddParams) {
			if(!isset($arTmpCurFields[$sTmpFieldName])) {
				$arTmpParams = array_merge(
					array(
						'FIELD_NAME' => $sTmpFieldName,
						'EDIT_FORM_LABEL' => '',
						'USER_TYPE_ID' => 'string',
						'MULTIPLE' => 'N',
						'SHOW_FILTER' => 'I',
						'SHOW_IN_LIST' => 'Y',
						'EDIT_IN_LIST' => 'Y',
						'IS_SEARCHABLE' => 'N',
						'SETTINGS' => array(
							'SIZE' => '50'
						),
						'LIST_COLUMN_LABEL' => '',
						'LIST_FILTER_LABEL' => '',
						'ERROR_MESSAGE' => '',
						'HELP_MESSAGE' => ''
					),
					$arAddParams
				);
				if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
					$arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
					$arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
					$arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
				}
				$iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
				if($iTmpUserFieldId) {
					echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
				} else {
					echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 40) {
	$iNiyamaLsvVer = 40;
	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::CheckCardsActivationQueue();' => array(
			'module' => 'iblock',
			'period' => 'N',
			'interval' => 300, // каждые пять минут
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1000
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}

if($iNiyamaLsvVer < 41) {
	$iNiyamaLsvVer = 41;

	// добавим новое пользовательское поле
	$sUserFieldEntityId = 'USER';
	$sUserFieldName = 'UF_ORDERS_IMP_DATE';
	$obUserTypeEntity = new CUserTypeEntity();
	$iUserFieldId = $obUserTypeEntity->Add(
		array(
			'ENTITY_ID' => $sUserFieldEntityId,
			'FIELD_NAME' => $sUserFieldName,
			'USER_TYPE_ID' => 'datetime',
			'MULTIPLE' => 'N',
			'SHOW_FILTER' => 'I',
			'SHOW_IN_LIST' => 'Y',
			'EDIT_IN_LIST' => 'N',
			'IS_SEARCHABLE' => 'N',
			'SETTINGS' => array(
				'SIZE' => '50'
			),
			'EDIT_FORM_LABEL' => array(
				'ru' => 'Дата последнего импорта заказов из FO',
				'en' => 'Дата последнего импорта заказов из FO',
			),
			'LIST_COLUMN_LABEL' => '',
			'LIST_FILTER_LABEL' => '',
			'ERROR_MESSAGE' => '',
			'HELP_MESSAGE' => ''
		)
	);
	if($iUserFieldId) {
		echo '<p>Добавлено пользовательское поле '.$sUserFieldName.' в объект '.$sUserFieldEntityId.'</p>';
	}
	//
	// ---
	//
	$arAddAgents = array(
		'CAgentHandlers::ImportUsersOrders();' => array(
			'module' => 'iblock',
			'period' => 'N',
			'interval' => 3600, // каждый час
			'datecheck' => '',
			'active' => 'Y',
			'next_exec' => '',
			'sort' => 1000
		),
	);
	$dbItems = CAgent::GetList();
	while($arItem = $dbItems->Fetch()) {
		if(isset($arAddAgents[$arItem['NAME']])) {
			unset($arAddAgents[$arItem['NAME']]);
		}
	}
	foreach($arAddAgents as $sAgentName => $arTmpParams) {
		$iNewAgentId = CAgent::AddAgent($sAgentName, $arTmpParams['module'], $arTmpParams['period'], $arTmpParams['interval'], $arTmpParams['datecheck'], $arTmpParams['active'], $arTmpParams['next_exec'], $arTmpParams['sort']);
		if($iNewAgentId) {
			echo '<p>Добавлен агент '.$sAgentName.'</p>';
		}
	}
}


if($iNiyamaLsvVer < 42) {
	$iNiyamaLsvVer = 42;
	$dbItems = CForm::GetBySID('QA');
	$arForm = $dbItems->Fetch();
	$iTmpFormId = $arForm['ID'] ? $arForm['ID'] : 0;

	if($iTmpFormId) {
		$arTmp = CFormField::GetBySID('QA_EMAIL', $iTmpFormId)->Fetch();
		if($arTmp['ID']) {
			$dbItems = CFormAnswer::GetList($arTmp['ID'], $sBy = 'id', $sOrder = 'asc', array('FIELD_TYPE' => 'email'), $bIsFiltered = false);
			while($arItem = $dbItems->Fetch()) {
				CFormAnswer::Set(
					array(
						'MESSAGE' => ' ',
						'C_SORT' => 100,
						'ACTIVE' => 'Y',
						'FIELD_TYPE' => 'text',
						'FIELD_PARAM' => 'class="input"'
					),
					$arItem['ID']
				);
				?><p>Изменен тип поля формы <?='FORM_ID = '.$iTmpFormId.'; ANSWER_ID = '.$arItem['ID'].' ('.$arTmp['TITLE'].')'?></p><?
			}
			$arItem = CFormValidator::GetList($arTmp['ID'], array('NAME' => 'custom_email_validator'), $sBy = 'C_SORT', $sOrder = 'asc')->Fetch();
			if(!$arItem) {
				if(CFormValidator::Set($iTmpFormId, $arTmp['ID'], 'custom_email_validator', array('ERR_MSG' => 'Введен некорректный адрес e-mail'))) {
					?><p>Установлен валидатор поля формы <?=$iTmpFormId.' - '.$arTmp['ID']?> custom_email_validator</p><?
				}
			}
		}
	}
}


if($iNiyamaLsvVer < 43) {
	$iNiyamaLsvVer = 43;
	CNiyamaCustomSettings::SetStringValue('Авторизация через Яндекс, client_id', 'yandex_oauth_client_id', '4f4c8a70fca74a1fb37bef3234bfce55');
	?><p>Установлен параметр yandex_oauth_client_id</p><?
	CNiyamaCustomSettings::SetStringValue('Авторизация через Яндекс, client_secret', 'yandex_oauth_client_secret', 'c0c89e760ec34561ace95754a6e16e88');
	?><p>Установлен параметр yandex_oauth_client_secret</p><?
}


if($iNiyamaLsvVer < 44) {
	$iNiyamaLsvVer = 44;

	// добавим новое пользовательское поле
	$sUserFieldEntityId = 'USER';
	$sUserFieldName = 'UF_MEDALS_CALC_DATE';
	$obUserTypeEntity = new CUserTypeEntity();
	$iUserFieldId = $obUserTypeEntity->Add(
		array(
			'ENTITY_ID' => $sUserFieldEntityId,
			'FIELD_NAME' => $sUserFieldName,
			'USER_TYPE_ID' => 'datetime',
			'MULTIPLE' => 'N',
			'SHOW_FILTER' => 'I',
			'SHOW_IN_LIST' => 'Y',
			'EDIT_IN_LIST' => 'N',
			'IS_SEARCHABLE' => 'N',
			'SETTINGS' => array(
				'SIZE' => '50'
			),
			'EDIT_FORM_LABEL' => array(
				'ru' => 'Дата последнего расчета медалей',
				'en' => 'Дата последнего расчета медалей',
			),
			'LIST_COLUMN_LABEL' => '',
			'LIST_FILTER_LABEL' => '',
			'ERROR_MESSAGE' => '',
			'HELP_MESSAGE' => ''
		)
	);
	if($iUserFieldId) {
		echo '<p>Добавлено пользовательское поле '.$sUserFieldName.' в объект '.$sUserFieldEntityId.'</p>';
	}
}


if($iNiyamaLsvVer < 45) {
	$iNiyamaLsvVer = 45;
	CNiyamaCustomSettings::SetStringValue('Текст отображающийся на странице проверки заказа, если не хватает стоимости заказа для выбранной доставки (#sum_price# - код для подстановки)', 'text_on_delivery_no_sum_price', '<p>Минимальная стоимость заказа для доставки по вашему адресу составляет #sum_price#.</p><p>Вы можете выбрать дополнительные блюда или заказать доставку на другой адрес.</p>');
}

if($iNiyamaLsvVer < 46) {
	$iNiyamaLsvVer = 46;
	$sTmpEntityName = 'UserMedalsInfo';
	$sTmpEntityTableName = 'niyama_user_medals_info';
	$iTmpEntityId = CHLEntity::GetEntityIdByName($sTmpEntityName);
	if(!$iTmpEntityId) {
		$obTmpResult = CHLEntity::Add(
			array(
				'NAME' => $sTmpEntityName,
				'TABLE_NAME' => $sTmpEntityTableName
			)
		);

		if($obTmpResult->IsSuccess()) {
			$iTmpEntityId = $obTmpResult->GetId();
		}

		if($iTmpEntityId) {
			?><p>Создана HL-сущность "<?=$sTmpEntityName?>"</p><?
		} else {
			echo '<p>'.implode('<br />', $obTmpResult->GetErrorMessages()).'</p>';
		}
	}

	if($iTmpEntityId) {
		$arAddFields = array(
			'UF_USER_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID пользователя',
				'MANDATORY' => 'Y',
			),
			'UF_MEDAL_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID карточки медали',
				'MANDATORY' => 'Y',
			),
			'UF_USER_MEDAL_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID медали пользователя',
			),
			'UF_ORDER_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID заказа, за который получена медаль'
			),
			'UF_PRODUCT_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID товара, за который получена медаль'
			),
			'UF_ACTION_ID' => array(
				'USER_TYPE_ID' => 'integer',
				'NAME' => 'ID действия, за которое получена медаль'
			),
		);

		$arTmpCurFields = CHLEntity::GetEntityFieldsById($iTmpEntityId);
		foreach($arAddFields as $sTmpFieldName => $arAddParams) {
			if(!isset($arTmpCurFields[$sTmpFieldName])) {
				$arTmpParams = array_merge(
					array(
						'FIELD_NAME' => $sTmpFieldName,
						'EDIT_FORM_LABEL' => '',
						'USER_TYPE_ID' => 'string',
						'MULTIPLE' => 'N',
						'SHOW_FILTER' => 'I',
						'SHOW_IN_LIST' => 'Y',
						'EDIT_IN_LIST' => 'Y',
						'IS_SEARCHABLE' => 'N',
						'SETTINGS' => array(
							'SIZE' => '50'
						),
						'LIST_COLUMN_LABEL' => '',
						'LIST_FILTER_LABEL' => '',
						'ERROR_MESSAGE' => '',
						'HELP_MESSAGE' => ''
					),
					$arAddParams
				);
				if(!is_array($arTmpParams['EDIT_FORM_LABEL']) && $arAddParams['NAME']) {
					$arTmpParams['EDIT_FORM_LABEL'] = array('ru' => $arAddParams['NAME'].' ['.$sTmpFieldName.']');
					$arTmpParams['LIST_COLUMN_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
					$arTmpParams['LIST_FILTER_LABEL'] = $arTmpParams['EDIT_FORM_LABEL'];
				}
				$iTmpUserFieldId = CHLEntity::AddEntityField($iTmpEntityId, $arTmpParams);
				if($iTmpUserFieldId) {
					echo '<p>Добавлено поле: '.$sTmpFieldName.'</p>';
				} else {
					echo '<p>Ошибка добавления поля: '.$sTmpFieldName.'</p>';
				}
			}
		}
	}
}

if($iNiyamaLsvVer < 47) {
	$iNiyamaLsvVer = 47;
	if(CNiyamaCustomSettings::SetStringValue('Минимальная сумма заказа самовывоза при заказе по телефону', 'min_self_delivery_sum_by_order', '150')) {
		?><p>Установлена опция "Минимальная сумма заказа самовывоза при заказе по телефону" = 150</p><?
	}
}

COption::SetOptionInt('main', 'niyama_lsv_ver', $iNiyamaLsvVer, '', '-');


if(!defined('_NIYAMA_COMMON_DEV_UPDATE_') || !_NIYAMA_COMMON_DEV_UPDATE_) {
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
}

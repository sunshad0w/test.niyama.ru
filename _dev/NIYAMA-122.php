<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}

CModule::IncludeModule('iblock');


if (!CCustomProject::IfIBTypeByID('NIYAMA_FOR_USER')){
	$arFields['ID'] = 'NIYAMA_FOR_USER';
	$arFields['SECTIONS'] = 'N';
	$arFields['IN_RSS'] = 'N';
	$arFields['SORT'] = 100;
	$langDefaults = array(
		'ru' => array(
			'NAME'          => 'Для пользователей',
			'SECTION_NAME'  => 'Разделы',
			'ELEMENT_NAME'  => 'Элементы',
		),
		'en' => array(
			'NAME'          => 'For users',
			'SECTION_NAME'  => 'Sections',
			'ELEMENT_NAME'  => 'Elements',
		),
	);	
	$l = CLanguage::GetList($lby="sort", $lorder="asc");
	while($arIBTLang = $l->GetNext())
	{
		if (array_key_exists($arIBTLang["LID"], $langDefaults))
			$arFields["LANG"][$arIBTLang["LID"]] = $langDefaults[$arIBTLang["LID"]];
	}
	$obBlocktype = new CIBlockType;	
	$res = $obBlocktype->Add($arFields);
	if(!$res){
	   echo '<p>Error: '.$obBlocktype->LAST_ERROR.'</p>';
	}
	else{		
		echo '<p>Добавлен тип ИБ: '.$arFields['ID'].'</p>';
	}
}
if ((CCustomProject::IfIBTypeByID('NIYAMA_FOR_USER')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_AVATARS'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_FOR_USER',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_AVATARS',
        'NAME' => "Стандартные аватары пользователей",
        'SORT' => 600,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Аватары',
        'ELEMENT_NAME' => 'Аватар',
        'ELEMENT_ADD' => 'Добавить аватар',
        'ELEMENT_EDIT' => 'Изменить аватар',
        'ELEMENT_DELETE' => 'Удалить аватар',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID)
    {
		$fields = CIBlock::getFields($ID);
		$fields["PREVIEW_PICTURE"]["DEFAULT_VALUE"]=array(
			'FROM_DETAIL' =>'N',
			'SCALE' =>'Y',
			'WIDTH' =>47,
			'HEIGHT' =>47,
			'IGNORE_ERRORS' =>'N',
			'METHOD' =>'resample',
			'COMPRESSION' => 95, 
			'DELETE_WITH_DETAIL' =>'N',
			'UPDATE_WITH_DETAIL' =>'N', 
		
		);
		CIBlock::setFields($ID, $fields);
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
if ((CCustomProject::IfIBTypeByID('NIYAMA_FOR_USER')) && (!CProjectUtils::GetIBlockIdByCodeEx('IB_NIYAMA_PERSON'))) {

    $arFields = array(
        'ACTIVE' => 'Y',
        'IBLOCK_TYPE_ID' => 'NIYAMA_FOR_USER',
        'LID'  => array(
            0=>'s1'
        ),
        'CODE' => 'IB_NIYAMA_PERSON',
        'NAME' => "Персоны пользователя",
        'SORT' => 700,
        'LIST_PAGE_URL'     => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/index.php?ID=#IBLOCK_ID#',
        'SECTION_PAGE_URL'  => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/list.php?SECTION_ID=#ID#',
        'DETAIL_PAGE_URL'   => '#SITE_DIR#/'.$arFields['IBLOCK_TYPE_ID'].'/detail.php?ID=#ID#',
        'INDEX_SECTION' => 'Y',
        'INDEX_ELEMENT' => 'Y',
        'PICTURE' => array(
            'del' => null,
            'MODULE_ID' => 'iblock',
        ),
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'EDIT_FILE_BEFORE' => '',
        'EDIT_FILE_AFTER' => '',
        'WORKFLOW' => 'N',
        'BIZPROC' => 'N',
        'SECTION_CHOOSER' => 'L',
        'LIST_MODE' => '',
        'FIELDS' => array(),
        'ELEMENTS_NAME' => 'Персоны',
        'ELEMENT_NAME' => 'Персона',
        'ELEMENT_ADD' => 'Добавить персону',
        'ELEMENT_EDIT' => 'Изменить персону',
        'ELEMENT_DELETE' => 'Удалить персону',
        'SECTIONS_NAME' => 'Разделы',
        'SECTION_NAME' => 'Раздел',
        'SECTION_ADD' => 'Добавить раздел',
        'SECTION_EDIT' => 'Изменить раздел',
        'SECTION_DELETE' => 'Удалить раздел',
        'RIGHTS_MODE' => 'S',
        'GROUP_ID' => array(
            2 => 'R',
        ),
        'VERSION' => 2
		

		
		
    );
    $Iblock = new CIBlock();
    $ID = $Iblock->Add($arFields);
    if ($ID){
		$fields = CIBlock::getFields($ID);
		$fields["PREVIEW_PICTURE"]["DEFAULT_VALUE"]=array(
			'FROM_DETAIL' =>'N',
			'SCALE' =>'Y',
			'WIDTH' =>47,
			'HEIGHT' =>47,
			'IGNORE_ERRORS' =>'N',
			'METHOD' =>'resample',
			'COMPRESSION' => 95, 
			'DELETE_WITH_DETAIL' =>'N',
			'UPDATE_WITH_DETAIL' =>'N', 		
		);
		CIBlock::setFields($ID, $fields);
        echo '<p>Добавлен ИБ: '.$ID.'</p>';
    } else {
        echo '<p>Ошибка добавления ИБ: '.$Iblock->LAST_ERROR.'</p>';
    }

}
echo "<hr>";
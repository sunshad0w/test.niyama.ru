<?php
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
@set_time_limit(3600);

if(!$GLOBALS['USER']->IsAdmin()) {
    echo 'Доступ запрещен';
    return;
}
CModule::IncludeModule('iblock');


$dbItems = CIBlockType::GetById('settings');
if(!$dbItems->Fetch()) {
    $arFields = array(
        'ID' => 'settings',
        'SECTIONS' => 'N',
        'IN_RSS' => 'N',
        'SORT' => 500,
        'LANG' => array(
            'ru' => array(
                'NAME' => 'Настройки'
            ),
            'en' => array(
                'NAME' => 'Настройки'
            ),
        )
    );
    $obIBlockType = new CIBlockType();
    $bResult = $obIBlockType->Add($arFields);
    if($bResult) {
        ?><p>Добавлен тип инфоблоков "Настройки"</p><?
    } else {
        ?><p>Ошибка добавления типа инфоблоков "Настроки": <?=$obIBlockType->LAST_ERROR?></p><?
    }
}

/**
 *  Опции
 *
 */
$iBlockIngredient = 0;
$iTmpIBlockId = CProjectUtils::GetIBlockIdByCodeEx('options', 'settings');
if(!$iTmpIBlockId) {
    $arFields = array(
        'ACTIVE' => 'Y',
        'NAME' => 'Опции',
        'CODE' => 'options',
        'IBLOCK_TYPE_ID' => 'settings',
        'SITE_ID' => 's1',
        'SORT' => '1000',
        'GROUP_ID' => array(2 => "R"),
        'LIST_PAGE_URL' => '',
        'SECTION_PAGE_URL' => '',
        'DETAIL_PAGE_URL' => '',
        'INDEX_SECTION' => 'N',
        'INDEX_ELEMENT' => 'N',
        'EDIT_FILE_BEFORE' => '',
        'VERSION' => 2,
        'FIELDS' => array(
            'CODE' => array(
                'IS_REQUIRED' => 'Y',
                'DEFAULT_VALUE' => array(
                    'UNIQUE' => 'Y',
                    'TRANSLITERATION' => 'Y',
                    'TRANS_LEN' => '100',
                    'TRANS_CASE' => 'L',
                    'TRANS_SPACE' => '-',
                    'TRANS_OTHER' => '-',
                    'TRANS_EAT' => 'Y',
                    'USE_GOOGLE' => 'N',
                ),
            ),
        )
    );
    $obIBlock = new CIBlock();
    $iTmpIBlockId = $obIBlock->Add($arFields);
    if($iTmpIBlockId) {
        $iBlockIngredient = $iTmpIBlockId;
        ?><p>Создан инфоблок "Опции"</p><?
    } else {
        ?><p>Ошибка добавления инфоблока "Опции": <?=$obIBlock->LAST_ERROR?></p><?
    }
}

if($iTmpIBlockId) {

    $arTmpPropertiesMeta = array();
    $iPropsSort = 400;
    // ---
    $sTmpPropCode = 'STRING_VALUE';
    $iPropsSort += 100;
    $arTmpPropertiesMeta[$sTmpPropCode] = array(
        'NAME' => 'Строковое значение',
        'PROPERTY_TYPE' => 'S',
        'LIST_TYPE' => 'C',
        'SEARCHABLE' => 'N',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'ACTIVE' => 'Y',
        'FILTRABLE' => 'Y',
        'SORT' => $iPropsSort,
        'CODE' => $sTmpPropCode,
    );

    // список текущих свойств инфоблока
    $arAddProps = array();
    $dbItems = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $iTmpIBlockId));
    while($arItem = $dbItems->Fetch()) {
        $arAddProps[$arItem['CODE']] = $arItem['CODE'];
    }
    // добавим недостающие свойства в инфоблок
    foreach($arTmpPropertiesMeta as $arPropMeta) {
        if(!isset($arAddProps[$arPropMeta['CODE']])) {
            $arPropMeta['IBLOCK_ID'] = $iTmpIBlockId;
            $obIBlockProperty = new CIBlockProperty();
            $iPropId = $obIBlockProperty->Add($arPropMeta);
            if($iPropId) {
                echo '<p>Добавлено свойство: ['.$iPropId.'] '.$arPropMeta['CODE'].'</p>';
            }
        }
    }
}
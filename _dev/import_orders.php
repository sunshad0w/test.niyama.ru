<?
return; // подстраховка от ошибочного запуска
//
// Импорт заказов из FO (пошаговый)
//

define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('NO_AGENT_CHECK', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

@set_time_limit(3600);
$iStepTimeSec = 5;

if(!$GLOBALS['USER']->IsAdmin()) {
	echo 'Доступ запрещен';
	return;
}

$bAjaxRequest = $_POST['IS_AJAX'] && $_POST['IS_AJAX'] == 'Y';

if($bAjaxRequest) {
	CModule::IncludeModule('iblock');
	$iLastId = isset($_POST['ID']) ? intval($_POST['ID']) : 0;
	$sNl = "\n";
	$sStartDate = '';
	if($iLastId) {
		$sStartDate = isset($_SESSION['_FO_ORDERS_IMPORT_']['START_DATE']) ? $_SESSION['_FO_ORDERS_IMPORT_']['START_DATE'] : '';
	}
	if(!strlen($sStartDate)) {
		$sStartDate = date('d-m-Y_H-i');
		$_SESSION['_FO_ORDERS_IMPORT_']['START_DATE'] = $sStartDate;
	}
	$sLogFilePathErr = $_SERVER['DOCUMENT_ROOT'].'/_log/import_orders_err_'.$sStartDate.'.log';
	$sLogFilePathOk = $_SERVER['DOCUMENT_ROOT'].'/_log/import_orders_ok_'.$sStartDate.'.log';

	$dEndTime = time() + $iStepTimeSec;

	$dbItems = TmpUsersTable::GetList(
		array(
			'order' => array(
				'ID' => 'ASC'
			),
			'filter' => array(
				'>ID' => $iLastId
			),
			'select' => array(
				'ID', 'UF_TMP_EMAIL'
			)
		)
	);
	$iLastId = 0;
	$iImportCnt = 0;
	while($arItem = $dbItems->Fetch()) {
		$arItem['UF_TMP_EMAIL'] = trim($arItem['UF_TMP_EMAIL']);
		if(strlen($arItem['UF_TMP_EMAIL'])) {
			$dbUserItems = CUser::GetList(
				$sBy = 'ID',
				$sOrder = 'ASC',
				array(
					'LOGIN' => $arItem['UF_TMP_EMAIL']
				),
				array(
					'FIELDS' => array(
						'ID', 'LOGIN'
					)
				)
			);
			if($arUserItem = $dbUserItems->Fetch()) {
				CNiyamaOrders::ImportUserOrdersFO($arItem['UF_TMP_EMAIL']);
				file_put_contents($sLogFilePathOk, 'ID:'.$arItem['ID'].'. USER_ID: '.$arUserItem['ID'].'. E-mail: '.$arItem['UF_TMP_EMAIL'].$sNl, FILE_APPEND);
			} else {
				$sPassword = CProjectUtils::GenPassword(8);
				$obUser = new CUser();
				$iNewUserId = $obUser->Add(
					array(
						'LOGIN' => $arItem['UF_TMP_EMAIL'],
						'EMAIL' => $arItem['UF_TMP_EMAIL'],
						'LID' => 's1',
						'ACTIVE' => 'N',
						'PASSWORD' => $sPassword,
						'CONFIRM_PASSWORD' => $sPassword,
						'GROUP_ID' => array(6), // зарегистрированные пользователи
						'_NIYAMA_IGNORE_TIMELINE_' => 'Y', // чтобы регистрация не отражалась на таймлайне скидок
						'_SKIP_FO_SYNC_' => 'Y', // чтобы не синхронизировалось с FO
					)
				);
				if($iNewUserId) {
					CNiyamaOrders::ImportUserOrdersFO($arItem['UF_TMP_EMAIL']);
					file_put_contents($sLogFilePathOk, 'ID:'.$arItem['ID'].'. USER_ID: '.$iNewUserId.'. E-mail: '.$arItem['UF_TMP_EMAIL'].$sNl, FILE_APPEND);
				} else {
					file_put_contents($sLogFilePathErr, 'ID:'.$arItem['ID'].'. E-mail: '.$arItem['UF_TMP_EMAIL'].'. Не удалось добавить пользователя: '.$obUser->LAST_ERROR.$sNl, FILE_APPEND);
				}
			}
		} else {
			file_put_contents($sLogFilePathErr, 'ID:'.$arItem['ID'].'. Не задан e-mail'.$sNl);
		}
		++$iImportCnt;
		if($dEndTime <= time()) {
			$iLastId = $arItem['ID'];
			break;
		}
	}
	if(!$iLastId) {
		file_put_contents($sLogFilePathOk, 'Finished! '.date('d.m.Y H:i:s').$sNl, FILE_APPEND);
	}
}

if(!$bAjaxRequest) {
	if($_SESSION['_FO_ORDERS_IMPORT_']) {
		unset($_SESSION['_FO_ORDERS_IMPORT_']);
	}
	echo CJSCore::Init(array('jquery'), true);
	?><h1>Импорт заказов пользователей</h1><?
	?><div id="ajax-result-block"></div><?
	?><input id="button" type="button" value="Начать импорт" onclick="return StartOrdersImport();" />&nbsp;<?
	?><input id="stop-button" type="button" disabled="disabled" value="Остановить" onclick="return StopOrdersImport();" /><?
	?><script type="text/javascript">
		var iImportedItems = 0;
		DoOrdersImportStep = function(iItemId) {
			var sUrl = '<?=$_SERVER['SCRIPT_NAME']?>';
			var obData = {
				'IS_AJAX': 'Y',
				'ID': iItemId
			};
			jQuery.post(
				sUrl,
				obData,
				function(obReturnData, sStatus, jqXHR) {
					var sHtml = '';
					var jqButton = jQuery('#button');
					var jqStopButton = jQuery('#stop-button');

					if(obReturnData.iImportCnt) {
						iImportedItems += obReturnData.iImportCnt;
					}
					sHtml += '<p>Обработано записей пользователей: '+iImportedItems+'</p>';
					if(obReturnData.iLastId) {
						sHtml += '<p>ID последней обработанной записи: '+obReturnData.iLastId+'</p>';
					}
					if(obReturnData.bContinue && obReturnData.iLastId && !jqStopButton.data('bPressed')) {
						sHtml += '<p>Пожалуйста, подождите...</p>';
						DoOrdersImportStep(obReturnData.iLastId);
					} else {
						sHtml += '<p>Импорт завершен</p>';
					}

					if(!obReturnData.bContinue || jqStopButton.data('bPressed')) {
						jqButton.data('bStarted', false);
						jqStopButton.data('bPressed', false);
						jqButton.attr('disabled', false);
						jqStopButton.attr('disabled', true);
					}
					jQuery('#ajax-result-block').html(sHtml);
				},
				'json'
			)
		};

		StartOrdersImport = function() {
			var jqButton = jQuery('#button');
			var jqStopButton = jQuery('#stop-button');
			if(!jqButton.data('bStarted')) {
				iImportedItems = 0;
				jqButton.attr('disabled', true);
				jqStopButton.attr('disabled', false);
				jqButton.data('bStarted', true);
				jqStopButton.data('bPressed', false);
				DoOrdersImportStep(0);
			}
			return false;
		};

		StopOrdersImport = function() {
			var jqButton = jQuery('#button');
			var jqStopButton = jQuery('#stop-button');
			if(jqButton.data('bStarted') && !jqStopButton.data('bPressed')) {
				jqStopButton.attr('disabled', true);
				jqStopButton.data('bPressed', true);
			}
			return false;
		};
	</script><?
} else {
	if(!$iLastId && $_SESSION['_FO_ORDERS_IMPORT_']) {
		unset($_SESSION['_FO_ORDERS_IMPORT_']);
	}

	$GLOBALS['APPLICATION']->RestartBuffer();
	$arJson = array(
		'iLastId' => $iLastId,
		'iImportCnt' => $iImportCnt,
		'bContinue' => $iLastId > 0,
	);
	echo json_encode($arJson);
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

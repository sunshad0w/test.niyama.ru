<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("currency")) {
	return false;
}

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if($saleModulePermissions == "D") {
	return false;
}


$arGadgetParams["RND_STRING"] = randString(8);

if(intval($arGadgetParams["LIMIT"]) <= 0) {
	$arGadgetParams["LIMIT"] = 5;
}

$arR = CNiyamaCatalog::GetList(null, null, null, array('nTopCount' => $arGadgetParams['LIMIT']));

$iUserId = false;
$arOrderProductsIds = CNiyamaCatalog::GetOrderProductsIds($iUserId);


?><script type="text/javascript">
	var gdSaleProductsTabControl_<?=$arGadgetParams['RND_STRING']?> = false;
	BX.ready(function(){
		gdSaleProductsTabControl_<?=$arGadgetParams['RND_STRING']?> = new gdTabControl('bx_gd_tabset_sale_products_<?=$arGadgetParams['RND_STRING']?>');
	});
</script><?

$aTabs = array(
	array(
		"DIV" => "bx_gd_sale_products2_".$arGadgetParams["RND_STRING"],
		"TAB" => GetMessage("GD_PRD_TAB_2"),
		"ICON" => "",
		"TITLE" => "",
		"ONSELECT" => "gdSaleProductsTabControl_".$arGadgetParams["RND_STRING"].".SelectTab('bx_gd_sale_products2_".$arGadgetParams["RND_STRING"]."');"
	),
    array(
        "DIV" => "bx_gd_sale_products1_".$arGadgetParams["RND_STRING"],
        "TAB" => GetMessage("GD_PRD_TAB_1"),
        "ICON" => "",
        "TITLE" => "",
        "ONSELECT" => "gdSaleProductsTabControl_".$arGadgetParams["RND_STRING"].".SelectTab('bx_gd_sale_products1_".$arGadgetParams["RND_STRING"]."');"
    )
);

$tabControl = new CAdminViewTabControl("salePrdTabControl", $aTabs);

?><div class="bx-gadgets-tabs-wrap" id="bx_gd_tabset_sale_products_<?=$arGadgetParams["RND_STRING"]?>"><?

	$tabControl->Begin();
	for($i = 0; $i < count($aTabs); $i++) {
		$tabControl->BeginNextTab();
	}
	$tabControl->End();

	?><div class="bx-gadgets-tabs-cont"><?
		for($i = 0; $i < count($aTabs); $i++)
		{
            if ($i <= 0){
			?><div id="<?=$aTabs[$i]["DIV"]?>_content" style="display: <?=($i==0 ? "block" : "none")?>;" class="bx-gadgets-tab-container"><?
					if (count($arR) > 0)
					{

						?><table class="bx-gadgets-table">
							<tbody>
								<tr>
									<th>Название</th>
									<th>Просмотров</th>
									<th>Цена</th>
								</tr><?
								foreach($arR['ITEMS'] as  $val)
								{
									?><tr>
										<td><?=htmlspecialcharsbx($val['INFO']["NAME"])?></td>
										<td align="right"><?=IntVal($val['INFO']["SHOW_COUNTER"])?></td>
										<td align="right" nowrap><?= $val['INFO']["PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE"]; ?> р.</td>
									</tr><?
								}
							?></tbody>
						</table><?
					}
					else
					{
						?><div align="center" class="bx-gadgets-content-padding-rl bx-gadgets-content-padding-t"><?=GetMessage("GD_PRD_NO_DATA")?></div><?
					}

			?></div><?
		} else {
                ?><div id="<?=$aTabs[$i]['DIV'].'_content'?>" style="display: <?=($i == 0 ? 'block' : 'none')?>;" class="bx-gadgets-tab-container"><?
	                if(!empty($arOrderProductsIds)) {
    	                ?><table class="bx-gadgets-table">
	    	                <tbody>
	    		                <tr>
    	    		                <th>Название</th>
        	    		            <th>Покупок</th>
            	    		        <th>Цена</th>
		                    	</tr><?
	        		            $s = 0;
    	            		    foreach($arOrderProductsIds as $key => $val) {
			                        $arProduct = CNiyamaCatalog::GetProductDetail($key);
        			                if($arProduct['INFO']['NAME']) {
	            			            ?><tr>
											<td><?=htmlspecialcharsbx($arProduct['INFO']['NAME'])?></td>
    	                	    			<td align="right"><?=intval($val)?></td>
		        		    	            <td align="right" nowrap><?=$arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?> р.</td>
        		        	    	    </tr><?
                		            	$s++;
									}
				                    if ($s >= $arGadgetParams['LIMIT']) {
	    			                	break;
	        	    		        }
		        	           }
        	        	    ?></tbody>
	                    </table><?
    	            } else {
        	            ?><div align="center" class="bx-gadgets-content-padding-rl bx-gadgets-content-padding-t"><?=GetMessage("GD_PRD_NO_DATA")?></div><?
            	    }
                ?></div><?
            }
        }
	?></div>
</div><?

<?php
if (!function_exists('clog')) {
    function clog()
    {
        $args   = func_get_args();
        $ex_txt = '<script type="application/javascript">';
        $ex_txt .= 'console.debug=function(){function o(r){var n=JSON.parse(JSON.stringify(r));if(n&&"object"==typeof n){n.__proto__=null;for(var e in n)n[e]=o(n[e])}return n}for(var r=0,n=Array.prototype.slice.call(arguments,0);r<n.length;r++)n[r]=o(n[r]);console.log.apply(console,n)};';
        foreach ($args as $i => $arg) {
            $ex_txt .= 'console.debug("'.++$i. ' var:",' . json_encode($arg) . ');';
        }
        $ex_txt .= '</script>';
        echo $ex_txt;
    }
}
<?php

/* Авторизация */
$MESS['/bitrix/modules/main/lang/ru/classes/general/user.php']['WRONG_LOGIN'] = "Неправильное имя учетной записи или пароль";
/* Смена пароля */
$MESS['/bitrix/modules/main/lang/ru/classes/general/user.php']["CHECKWORD_INCORRECT"] = "Неверное контрольное слово для учетной записи \"#LOGIN#\".";
$MESS['/bitrix/modules/main/lang/ru/classes/general/user.php']["ACCOUNT_INFO_SENT"] = "На вашу почту будет отправлена ссылка для смены пароля. Дождитесь письма и перейдите по ссылке.";
$MESS['/bitrix/modules/main/lang/ru/classes/general/user.php']["PASSWORD_CHANGE_OK"] = "Пароль успешно изменен.<br>На ваш E-mail высланы новые регистрационные данные.";
$MESS['/bitrix/modules/sale/lang/ru/lib/discountcoupon.php']['BX_SALE_DCM_COUPON_CHECK_RANGE_ACTIVE_TO_DISCOUNT'] = 'Указанный купон не действителен';
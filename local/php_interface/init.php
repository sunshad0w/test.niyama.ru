<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
unset($_SERVER['AUTHENTICATE_CN'], $_SERVER['REMOTE_USER'], $_SERVER['PHP_AUTH_PW'], $_SERVER['PHP_AUTH_USER']);

#

#
$sBitrixRoot = '/local';

#

#
include_once($_SERVER['DOCUMENT_ROOT'].$sBitrixRoot.'/php_interface/adv/_constants.php');
include_once($_SERVER['DOCUMENT_ROOT'].$sBitrixRoot.'/php_interface/adv/_globals.php');

#

#
$sClassesPath = $sBitrixRoot.'/php_interface/adv/classes/';

CModule::AddAutoloadClasses(
	'',
	array(

	    'CPizzaPiPartners.php'=> $sClassesPath.'CPizzaPiPartners.php',

		'CStaticCache' => $sClassesPath.'CStaticCache.php',
		'CExtCache' => $sClassesPath.'CExtCache.php',
		'CHLEntity' => $sClassesPath.'CHLEntity.php',
		'CCustomLog' => $sClassesPath.'CCustomLog.php',
		'CProjectUtils' => $sClassesPath.'CProjectUtils.php',
		'CSoapServices' => $sClassesPath.'CSoapServices.php',
		'CCustomFormValidatorEmail' => $sClassesPath.'CCustomFormValidatorEmail.php',

		// ADV-lib
		'ADV\\CEntityDataManger' => $sClassesPath.'lib/CEntityDataManger.php',
		'ADV\\CEntityDataMangerDBResult' => $sClassesPath.'lib/CEntityDataMangerDBResult.php',
		'ADV\\IBlock\\IBlockElementTable' => $sClassesPath.'lib/IBlock/IBlockElementTable.php',
		'ADV\\Authentication\\Adapter\\AdapterFactory' => $sClassesPath.'Authentication/Adapter/AdapterFactory.php',
		'ADV\\Authentication\\Adapter\\AbstractAdapter' => $sClassesPath.'Authentication/Adapter/AbstractAdapter.php',
		'ADV\\Authentication\\Adapter\\Odnoklassniki' => $sClassesPath.'Authentication/Adapter/Odnoklassniki.php',
		'ADV\\Authentication\\Adapter\\GooglePlus' => $sClassesPath.'Authentication/Adapter/GooglePlus.php',
		'ADV\\Authentication\\Adapter\\Vkontakte' => $sClassesPath.'Authentication/Adapter/Vkontakte.php',
		'ADV\\Authentication\\Adapter\\Facebook' => $sClassesPath.'Authentication/Adapter/Facebook.php',
		'ADV\\Authentication\\Adapter\\MailRu' => $sClassesPath.'Authentication/Adapter/MailRu.php',
		'ADV\\Authentication\\Adapter\\Yandex' => $sClassesPath.'Authentication/Adapter/Yandex.php',

		// ?????????
		'CIBlockUserTypeNYesNo' => $sClassesPath.'CustomTypes/CIBlockUserTypeNYesNo.php',
		'CImportMenuElement' => $sClassesPath.'CustomTypes/CImportMenuElement.php',
		'CIBlockUserTypeSaleLoc' => $sClassesPath.'CustomTypes/CIBlockUserTypeSaleLoc.php',
		'CUFVarchar' => $sClassesPath.'CustomTypes/CUFVarchar.php',
		'CRelatedProducts' => $sClassesPath.'CustomTypes/CRelatedProducts.php',
		'CRecProducts' => $sClassesPath.'CustomTypes/CRecProducts.php',
		'CBonusBluds' => $sClassesPath.'CustomTypes/CBonusBluds.php',

		// ????????? ?????? ???????
		'CCustomProject' => $sClassesPath.'CCustomProject.php',
		'CEventHandlers' => $sClassesPath.'CEventHandlers.php',
		'CAgentHandlers' => $sClassesPath.'CAgentHandlers.php',
		'CNiyamaImportUtils' => $sClassesPath.'CNiyamaImportUtils.php',
		'CNiyamaFastOperator' => $sClassesPath.'CNiyamaFastOperator.php',
		'CNiyamaPds' => $sClassesPath.'CNiyamaPds.php',
		'CNiyamaIBlockCatalogBase' => $sClassesPath.'CNiyamaIBlockCatalogBase.php',
		'CNiyamaIBlockDepartments' => $sClassesPath.'CNiyamaIBlockDepartments.php',
		'CNiyamaIBlockCities' => $sClassesPath.'CNiyamaIBlockCities.php',
		'CNiyamaIBlockStreets' => $sClassesPath.'CNiyamaIBlockStreets.php',
		'CNiyamaIBlockSubway' => $sClassesPath.'CNiyamaIBlockSubway.php',
		'CNiyamaIBlockCardData' => $sClassesPath.'CNiyamaIBlockCardData.php',
		'CNiyamaIBlockCartLine' => $sClassesPath.'CNiyamaIBlockCartLine.php',
		'CNiyamaCatalog' => $sClassesPath.'CNiyamaCatalog.php',
		'CDebug' => $sClassesPath.'CDebug.php',
		'CNiyamaDiscountLevels' => $sClassesPath.'CNiyamaDiscountLevels.php',
		'CNiyamaCustomSettings' => $sClassesPath.'CNiyamaCustomSettings.php',
		'CNiyamaCart' => $sClassesPath.'CNiyamaCart.php',
		'CNiyamaOrders' => $sClassesPath.'CNiyamaOrders.php',
		'CUsersData' => $sClassesPath.'CUsersData.php',
		'CNiyamaStatistic' => $sClassesPath.'CNiyamaStatistic.php',
		'CNiyamaCoupons' => $sClassesPath.'CNiyamaCoupons.php',
		'CNiyamaBonus' => $sClassesPath.'CNiyamaBonus.php',
		'CNiyamaModal' => $sClassesPath.'CNiyamaModal.php',
		'CNiyamaMedals' => $sClassesPath.'CNiyamaMedals.php',
		'CNiyamaUserActions' => $sClassesPath.'CNiyamaUserActions.php',
		'CNiyamaVotes' => $sClassesPath.'CNiyamaVotes.php',
		'CNiyamaOldSiteAuth' => $sClassesPath.'CNiyamaOldSiteAuth.php',
		'CNiyamaDiscountCard' => $sClassesPath.'CNiyamaDiscountCard.php',
		'CNiyamaIBlockUrlRedirect' => $sClassesPath.'CNiyamaIBlockUrlRedirect.php',
		'CPizzaPiActionPayTable' => $sClassesPath.'CPizzaPiActionPayTable.php',


		// Entities
		'UsersocTable' => $sClassesPath.'entities/UsersocTable.php',
		'UserActionsTable' => $sClassesPath.'entities/UserActionsTable.php',
		'UserModalShowTable' => $sClassesPath.'entities/UserModalShowTable.php',
		'TmpUsersTable' => $sClassesPath.'entities/TmpUsersTable.php',
		// Vendor
		'Spyc' => $sBitrixRoot.'/php_interface/lib/spyc.php',
		'idna_convert' => $sBitrixRoot.'/php_interface/lib/idna_convert/idna_convert.class.php',
	)
);

#

#
include_once($_SERVER['DOCUMENT_ROOT']. $sBitrixRoot .'/php_interface/adv/_functions.php');

#

#
include_once($_SERVER['DOCUMENT_ROOT']. $sBitrixRoot .'/php_interface/adv/_handlers.php');


if ($_REQUEST['COPY_ORDER'] == 'Y' && CModule::IncludeModule("sale")){
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

/**
 * ======================================
 * Выводим дату в текстовом формате
 * @param  $date
 * @param  bool $show_time
 * @param  string $_format = "DD.MM.YYYY HH:MI:SS" | ("timestamp")
 * @return string
 */
if (!function_exists('ADV_textDate')) {
    function ADV_textDate($date, $show_time = true, $_format = "DD.MM.YYYY HH:MI:SS")
    {
        if ($_format != "timestamp") {
            $timestamp = MakeTimeStamp($date, $_format);
        } else {
            $timestamp = $date;
        }
        $mon = array('01' => 'Января', '02' => 'Февраля', '03' => 'Марта', '04' => 'Апреля', '05' => 'Мая',
            '06' => 'Июня', '07' => 'Июля', '08' => 'Августа', '09' => 'Сентября', '10' => 'Октября',
            '11' => 'Ноября', '12' => 'Декабря');
        $of = 'в';
        $year = (date("Y", $timestamp) != date("Y")) ? ' ' . date("Y", $timestamp) : '';
        $time = ($show_time) ? $of . ' ' . date("h:i", $timestamp) : '';
//        if (date("d.m.Y", $timestamp) === date("d.m.Y")) {
//            return 'Сегодня ' . $time;
//        } else {
            return date("d", $timestamp) . ' ' . $mon[date("m", $timestamp)] . $year . ' ' . $time;
//        }
    }
}

/**
 * @param bool $content
 * @return bool
 */
if (!function_exists("ADV_menu_custom_content")) {
    function ADV_menu_custom_content($CustContent)
    {
        global $APPLICATION;
        $APPLICATION->AddViewContent("menu_custom_content_fix_panel", " _has_catalog_bread_crumbs");
        $APPLICATION->AddViewContent("menu_custom_content", $CustContent);
    }
}

/**
 * ADV_iformat
 * @param $number
 * @return string
 */
if (!function_exists("ADV_iformat")) {
    function ADV_iformat($number)
    {
        return  ($number >= 1000) ? number_format($number,2,","," ") : $number;
    }
}

/**
 * ADV_set_flashdata
 * @param $newdata
 * @param string $newval
 */
if (!function_exists("ADV_set_flashdata")) {
    function ADV_set_flashdata($newdata, $newval = '')
    {
        if (is_string($newdata))
        {
            $newdata = array($newdata => $newval);
        }

        if (count($newdata) > 0)
        {
            foreach ($newdata as $key => $val)
            {
                $flashdata_key = 'flash'.':new:'.$key;
                $_SESSION[$flashdata_key] = $val;
            }
        }
    }
}

/**
 * ADV_get_flashdata
 * @param $key
 * @return bool
 */
if (!function_exists("ADV_get_flashdata")) {
    function ADV_get_flashdata($key)
    {
        $flashdata_key = 'flash'.':new:'.$key;
        if (isset($_SESSION[$flashdata_key]) && (!empty($_SESSION[$flashdata_key]))  ){
            $data = $_SESSION[$flashdata_key];
            unset($_SESSION[$flashdata_key]);
            return $data;
        }
        return false;
    }
}

if(!function_exists('log_array')) {
    function log_array($arFields = array(), $sFileName = 'log.txt', $bBacktrace = true) {
        if($GLOBALS['USER']->isAdmin()) {
            _log_array($arFields, $sFileName, $bBacktrace);
        }
    }
}

if(!function_exists('_log_array')) {
    function _log_array($arFields = array(), $sFileName = 'log.txt', $bBacktrace = true) {
        $sMess = '';
        $sMess .= date('d.m.Y H:i:s')."\n";
        $sMess .= print_r($arFields, true)."\n";
        if($bBacktrace && function_exists('debug_backtrace')) {
            $arBacktrace = debug_backtrace();
            $iterationsCount = min(count($arBacktrace), 4);
            for($i = 1; $i < $iterationsCount; $i++) {
                if(strlen($strFunctionStack)) {
                    $sMess .= ' < ';
                }
                if(strlen($arBacktrace[$i]['class'])) {
                    $sMess .= $arBacktrace[$i]['class'].'::';
                }
                $sMess .= $arBacktrace[$i]['function']." >> ";
            }
        }
        $sMess .= "\n----------\n\n";
        $sFileName = empty($sFileName) ? 'log.txt' : $sFileName;
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/_log/'.$sFileName, $sMess, FILE_APPEND);
    }
}
if(!function_exists('PR')) {
    function PR($o, $name='', $show_all=false){
		global $USER;
		if ($USER->IsAdmin()||$show_all) {
	
			$bt = debug_backtrace();
			$bt = $bt[0];
			$dRoot = $_SERVER["DOCUMENT_ROOT"];
			$dRoot = str_replace("/","\\",$dRoot);
			$bt["file"] = str_replace($dRoot,"",$bt["file"]);
			$dRoot = str_replace("\\","/",$dRoot);
			$bt["file"] = str_replace($dRoot,"",$bt["file"]);
			?>
			<div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
			<div style='padding:3px 5px; background:#9CCFF; font-weight:bold;'>FILE: <?=$bt["file"]?> [<?=$bt["line"]?>] <?=(!empty($name))?'NAME='.$name:''?></div>
			<pre style='padding:10px;'><?print_r($o)?></pre>
			</div>
			<?
		}
	
	}	
}

if(!function_exists('ADV_array_pagination')) {
    function ADV_array_pagination($arDisplayData, $page = 1, $per_page = 24)
    {
        $page = $page < 1 ? 1 : $page;
        $start = ($page - 1) * ($per_page + 1);
        $offset = $per_page + 1;
        $outArray = array_slice($arDisplayData, $start, $offset);
        return $outArray;
    }
}
if(!function_exists('ADV_pluralForm')) {
	function ADV_pluralForm($n, $form1, $form2, $form5) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
}

if(!function_exists("strptime")) 
{ 
    function strptime($sDate, $sFormat) 
    { 
        $aResult = array 
        ( 
            'tm_sec'   => 0, 
            'tm_min'   => 0, 
            'tm_hour'  => 0, 
            'tm_mday'  => 1, 
            'tm_mon'   => 0, 
            'tm_year'  => 0, 
            'tm_wday'  => 0, 
            'tm_yday'  => 0, 
            'unparsed' => $sDate, 
        ); 
         
        while($sFormat != "") 
        { 
            // ===== Search a %x element, Check the static string before the %x ===== 
            $nIdxFound = strpos($sFormat, '%'); 
            if($nIdxFound === false) 
            { 
                 
                // There is no more format. Check the last static string. 
                $aResult['unparsed'] = ($sFormat == $sDate) ? "" : $sDate; 
                break; 
            } 
             
            $sFormatBefore = substr($sFormat, 0, $nIdxFound); 
            $sDateBefore   = substr($sDate,   0, $nIdxFound); 
             
            if($sFormatBefore != $sDateBefore) break; 
             
            // ===== Read the value of the %x found ===== 
            $sFormat = substr($sFormat, $nIdxFound); 
            $sDate   = substr($sDate,   $nIdxFound); 
             
            $aResult['unparsed'] = $sDate; 
             
            $sFormatCurrent = substr($sFormat, 0, 2); 
            $sFormatAfter   = substr($sFormat, 2); 
             
            $nValue = -1; 
            $sDateAfter = ""; 
            switch($sFormatCurrent) 
            { 
                case '%S': // Seconds after the minute (0-59) 
                     
                    sscanf($sDate, "%2d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if(($nValue < 0) || ($nValue > 59)) return false; 
                     
                    $aResult['tm_sec']  = $nValue; 
                    break; 
                 
                // ---------- 
                case '%M': // Minutes after the hour (0-59) 
                    sscanf($sDate, "%2d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if(($nValue < 0) || ($nValue > 59)) return false; 
                 
                    $aResult['tm_min']  = $nValue; 
                    break; 
                 
                // ---------- 
                case '%H': // Hour since midnight (0-23) 
                    sscanf($sDate, "%2d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if(($nValue < 0) || ($nValue > 23)) return false; 
                 
                    $aResult['tm_hour']  = $nValue; 
                    break; 
                 
                // ---------- 
                case '%d': // Day of the month (1-31) 
                    sscanf($sDate, "%2d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if(($nValue < 1) || ($nValue > 31)) return false; 
                 
                    $aResult['tm_mday']  = $nValue; 
                    break; 
                 
                // ---------- 
                case '%m': // Months since January (0-11) 
                    sscanf($sDate, "%2d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if(($nValue < 1) || ($nValue > 12)) return false; 
                 
                    $aResult['tm_mon']  = ($nValue - 1); 
                    break; 
                 
                // ---------- 
                case '%Y': // Years since 1900 
                    sscanf($sDate, "%4d%[^\\n]", $nValue, $sDateAfter); 
                     
                    if($nValue < 1900) return false; 
                 
                    $aResult['tm_year']  = ($nValue - 1900); 
                    break; 
                 
                // ---------- 
                default: break 2; // Break Switch and while 
            } 
             
            // ===== Next please ===== 
            $sFormat = $sFormatAfter; 
            $sDate   = $sDateAfter; 
             
            $aResult['unparsed'] = $sDate; 
             
        } // END while($sFormat != "") 
         
       
        // ===== Create the other value of the result array ===== 
      /*  $nParsedDateTimestamp = mktime($aResult['tm_hour'], $aResult['tm_min'], $aResult['tm_sec'], 
                                $aResult['tm_mon'] + 1, $aResult['tm_mday'], $aResult['tm_year'] + 1900); 
         
        
        if(($nParsedDateTimestamp === false) 
        ||($nParsedDateTimestamp === -1)) return false; 
         
        $aResult['tm_wday'] = (int) strftime("%w", $nParsedDateTimestamp); // Days since Sunday (0-6) 
        $aResult['tm_yday'] = (strftime("%j", $nParsedDateTimestamp) - 1); // Days since January 1 (0-365) */

        return $aResult; 
    } // END of function 
     
} 
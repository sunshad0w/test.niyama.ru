<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Кастомные кнопки на страницах админки
//

$sCurPage = $GLOBALS['APPLICATION']->GetCurPage(false);
if($sCurPage == '/bitrix/admin/iblock_section_admin.php' || $sCurPage == '/bitrix/admin/iblock_element_admin.php') {
	$iCurIBockId = $_REQUEST['IBLOCK_ID'] ? intval($_REQUEST['IBLOCK_ID']) : 0;
	$iCatalogBaseIBlockId = CNiyamaIBlockCatalogBase::GetPizzapiIblockID();//CNiyamaIBlockCatalogBase::GetIBlockId();
	$iDataDepartmentsIBlockId = CNiyamaIBlockDepartments::GetIBlockId();
	$iDataCitiesIBlockId = CNiyamaIBlockCities::GetIBlockId();
	$iDataStreetsIBlockId = CNiyamaIBlockStreets::GetIBlockId();
	$iDataSubwayIBlockId = CNiyamaIBlockSubway::GetIBlockId();
	switch($iCurIBockId) {
		case $iCatalogBaseIBlockId:
			$sIBlockPermission = CNiyamaIBlockCatalogBase::GetCurUserPermissions();
			if($sIBlockPermission > 'W') {
				$arContextItems[] = array(
					'TEXT' => 'Импортировать данные из FastOperator',
					'ICON' => '',
					'LINK' => '/bitrix/admin/adv_import_catalog_base.php',
					'LINK_PARAM' => '',
					'TITLE' => '',
				);
			}
		break;

		case $iDataDepartmentsIBlockId:
			$sIBlockPermission = CNiyamaIBlockDepartments::GetCurUserPermissions();
			if($sIBlockPermission > 'W') {
				$arContextItems[] = array(
					'TEXT' => 'Импортировать данные из FastOperator',
					'ICON' => '',
					'LINK' => '/bitrix/admin/adv_import_data_departments.php',
					'LINK_PARAM' => '',
					'TITLE' => '',
				);
			}
		break;

		case $iDataCitiesIBlockId:
			$sIBlockPermission = CNiyamaIBlockCities::GetCurUserPermissions();
			if($sIBlockPermission > 'W') {
				$arContextItems[] = array(
					'TEXT' => 'Импортировать данные из FastOperator',
					'ICON' => '',
					'LINK' => '/bitrix/admin/adv_import_data_cities.php',
					'LINK_PARAM' => '',
					'TITLE' => '',
				);
			}
		break;

		case $iDataStreetsIBlockId:
			$sIBlockPermission = CNiyamaIBlockStreets::GetCurUserPermissions();
			if($sIBlockPermission > 'W') {
				$arContextItems[] = array(
					'TEXT' => 'Импортировать данные из FastOperator',
					'ICON' => '',
					'LINK' => '/bitrix/admin/adv_import_data_streets.php',
					'LINK_PARAM' => '',
					'TITLE' => '',
				);
			}
		break;

		case $iDataSubwayIBlockId:
			$sIBlockPermission = CNiyamaIBlockSubway::GetCurUserPermissions();
			if($sIBlockPermission > 'W') {
				$arContextItems[] = array(
					'TEXT' => 'Импортировать данные из FastOperator',
					'ICON' => '',
					'LINK' => '/bitrix/admin/adv_import_data_subway.php',
					'LINK_PARAM' => '',
					'TITLE' => '',
				);
			}
		break;
	}
}

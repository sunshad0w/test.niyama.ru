<?
/**
 * Актуализация меню из FastOperator
 *
 * @author Sergey Leshchenko, 2014
 * @updated: 07.04.2015
 */
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/adv/classes/CNiyamaIBlockCatalogBaseIts.php');

@set_time_limit(3600);

$iIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
$sIBlockPermission = CNiyamaIBlockCatalogBase::GetCurUserPermissions();
if($sIBlockPermission < 'W') {
	$GLOBALS['APPLICATION']->AuthForm('Доступ запрещен');
}

CModule::IncludeModule('iblock');

$arPostingErrors = array();
$bSuccessUpdate = false;

//CNiyamaImportUtils::FlushLockingFlag('niyama_imp_catalog_base_img');

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_POST['IMPORT']) && $_POST['IMPORT'] == 'Y') {
		if (check_bitrix_sessid()) {
			if (!CNiyamaImportUtils::IsFlagLocked('niyama_imp_catalog_base')) {
				CNiyamaImportUtils::SetLockingFlag('niyama_imp_catalog_base', 10800);
				$bResult = CNiyamaIBlockCatalogBaseIts::ImportData();
				CNiyamaImportUtils::FlushLockingFlag('niyama_imp_catalog_base');
				if ($bResult) {
					$bSuccessUpdate = true;
				} else {
					$arPostingErrors[] = CProjectUtils::GetExceptionString();
				}
			} else {
				$arPostingErrors[] = 'Импорт уже запущен другим пользователем или системным процессом. Пожалуйста, попробуйте позже.';
			}

		} else {
			$arPostingErrors[] = 'Ваша сессия истекла, пожалуйста, повторите операцию<br />';
		}
	} elseif (isset($_POST['IMAGES']) && $_POST['IMAGES'] == 'Y') {
		if (check_bitrix_sessid()) {
			if (!CNiyamaImportUtils::IsFlagLocked('niyama_imp_catalog_base_img')) {
				CNiyamaImportUtils::SetLockingFlag('niyama_imp_catalog_base_img', 3600);
				$bResult = CNiyamaIBlockCatalogBaseIts::ImportImages();
				CNiyamaImportUtils::FlushLockingFlag('niyama_imp_catalog_base_img');
				if ($bResult) {
					$bSuccessUpdate = true;
				} else {
					$arPostingErrors[] = CProjectUtils::GetExceptionString();
				}
			} else {
				$arPostingErrors[] = 'Загрузка уже запущена другим пользователем или системным процессом. Пожалуйста, попробуйте позже.1';
			}

		} else {
			$arPostingErrors[] = 'Ваша сессия истекла, пожалуйста, повторите операцию<br />';
		}
	}
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/prolog.php');

$GLOBALS['APPLICATION']->SetTitle('Актуализация меню из FastOperator');

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

if($arPostingErrors) {
	CAdminMessage::ShowOldStyleError(implode('<br />', $arPostingErrors));
}

//CNiyamaImportUtils::FlushLockingFlag('niyama_imp_catalog_base');

if(!$bSuccessUpdate) {
	if(CNiyamaImportUtils::IsFlagLocked('niyama_imp_catalog_base')) {
		echo ShowMessage(array('TYPE' => 'ERROR', 'MESSAGE' => 'Импорт уже запущен другим пользователем или системным процессом. Пожалуйста, попробуйте позже.'));
	} elseif(CNiyamaImportUtils::IsFlagLocked('niyama_imp_catalog_base_img')) {
		echo ShowMessage(array('TYPE' => 'ERROR', 'MESSAGE' => 'Загрузка уже запущена другим пользователем или системным процессом. Пожалуйста, попробуйте позже.2'));
	} else {
		$sFormName = 'adv_import_catalog_base_img';
		?><form id="<?=$sFormName?>" name="<?=$sFormName?>" class="adv_import_catalog_base" method="post" action="" enctype="multipart/form-data"><div><?
			echo bitrix_sessid_post();
			?><p>Данная операция запустит загрузку изображений блюд из системы FastOperator.<br/><?
			?><p>Процедура загрузки может занять несколько минут.</p><?
			?><input type="hidden" name="IMAGES" value="Y" /><?
			?><input type="hidden" name="lang" value="<?=LANG?>" /><?
			?><input type="submit" name="button" value="Загрузить" /><?
			?></div></form>

		<?
		$sFormName = 'adv_import_catalog_base';
		?><form id="<?=$sFormName?>" name="<?=$sFormName?>" class="adv_import_catalog_base" method="post" action="" enctype="multipart/form-data"><div><?
			echo bitrix_sessid_post();
			?><p>Данная операция запустит импорт меню блюд из системы FastOperator.<br />Все элементы и секции инфоблока "Импортируемое меню" будут обновлены. Элементы и секции, которых нет в импортируемых данных, будут деактивированы.</p><?
			?><p>Процедура импорта может занять несколько минут.</p><?
			?><input type="hidden" name="IMPORT" value="Y" /><?
			?><input type="hidden" name="lang" value="<?=LANG?>" /><?
			?><input type="submit" name="button" value="Импортировать" /><?
		?></div></form><?
	}
} else {
	echo ShowMessage(array('TYPE' => 'OK', 'MESSAGE' => 'Данные успешно импортированы.'));
	?><a href="/bitrix/admin/iblock_section_admin.php?IBLOCK_ID=<?=$iIBlockId?>&type=catalog&find_section_section=0&lang=ru">Перейти в инфоблок</a><?
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');


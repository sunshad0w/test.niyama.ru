<? 
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if ( CModule::IncludeModule('iblock')) :


    # Фильтр
    $arPFilter = array();
	
    if (!empty($_REQUEST)){
        # Меню
        if (!empty($_REQUEST['menu']) && (is_array($_REQUEST['menu']))){
			foreach($_REQUEST['menu'] as $key=>$el){
				if (empty($el)){
					unset($_REQUEST['menu'][$key]);
				}
			}
            $arPFilter['SECTION_ID'] = $_REQUEST['menu'];
            $arPFilter['INCLUDE_SUBSECTIONS']  = "Y";
            
        }
        # Специальное меню
        if (!empty($_REQUEST["specmenu"]) && (is_array($_REQUEST['specmenu']))){
			foreach($_REQUEST['specmenu'] as $key=>$el){
				if (empty($el)){
					unset($_REQUEST['specmenu'][$key]);
				}
			}
            $arPFilter['PROPERTY_SPEC_MENU.ID'] = $_REQUEST['specmenu'];
            
        }
        CModule::IncludeModule('iblock');

        # Ингредиенты
        if (!empty($_REQUEST['ing']) && (is_array($_REQUEST['ing']))) {
			foreach($_REQUEST['ing'] as $key=>$el){
				if (empty($el)){
					unset($_REQUEST['ing'][$key]);
				}
			}
//        foreach ($_REQUEST['ing'] as $row) {
//            $arrayTmp = null;
//            $arrayTmp['LOGIC'] = "OR";
//            $arrayTmp[] = array(
//                "PROPERTY_GENERAL_INGREDIENT.ID" => $row,
//            );
//            $arrayTmp[] = array(
//                "PROPERTY_MORE_INGREDIENT.ID" => $row,
//            );
//            $arrayTmpLogic[] = $arrayTmp;
//        }
//        $arPFilter[] = array("LOGIC" => "AND", $arrayTmpLogic);
            
            $arPFilter[] = array(
                "LOGIC" => "OR",
                array("?PROPERTY_GENERAL_INGREDIENT" => implode(" && ",$_REQUEST['ing'])),
                array("?PROPERTY_MORE_INGREDIENT"    => implode(" && ",$_REQUEST['ing']))
            );

        }
    }

    $arMenuItems = CNiyamaCatalog::GetList(null,$arPFilter,null,false);
	?><option value="" selected>Все блюда</option><?
	foreach($arMenuItems['ITEMS'] as $el){
		?><option value="<?=$el['INFO']['ID']?>"><?=$el['INFO']['NAME']?></option><?						
	}
	
endif;
// это вместо эпилога
CProjectUtils::AjaxEpilog();
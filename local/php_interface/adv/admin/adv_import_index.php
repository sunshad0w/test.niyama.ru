<?
/**
 * Индексная страница импорта справочников в админпанели - /bitrix/admin/adv_import_index.php
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 */
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

if(!CModule::IncludeModule('iblock')) {
	return;
}

$sModulePermissions = 'R';
if($sModulePermissions < 'R') {
	$GLOBALS['APPLICATION']->AuthForm(GetMessage('ACCESS_DENIED'));
}


IncludeModuleLangFile(__FILE__);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/prolog.php');
$bList = $_REQUEST['mode'] == 'list';

$GLOBALS['APPLICATION']->SetTitle('Импорт справочников');

if($bList) {
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_js.php');
} else {
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
}

$adminPage->ShowSectionIndex('adv_import_index');

if($bList) {
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin_js.php');
} else {
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
}

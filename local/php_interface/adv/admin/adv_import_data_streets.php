<?
/**
 * Актуализация справочника Улицы
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 */
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
@set_time_limit(3600);

$iIBlockId = CNiyamaIBlockStreets::GetIBlockId();
$sIBlockPermission = CNiyamaIBlockStreets::GetCurUserPermissions();
if($sIBlockPermission < 'W') {
	$GLOBALS['APPLICATION']->AuthForm('Доступ запрещен');
}

CModule::IncludeModule('iblock');

$arPostingErrors = array();
$bSuccessUpdate = false;
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['IMPORT']) && $_POST['IMPORT'] == 'Y') {
	if(check_bitrix_sessid()) {
		$bResult = CNiyamaIBlockStreets::ImportData();
		if($bResult) {
			$bSuccessUpdate = true;
		} else {
			$arPostingErrors[] = CProjectUtils::GetExceptionString();
		}
	} else {
		$arPostingErrors[] = 'Ваша сессия истекла, пожалуйста, повторите операцию<br />';
	}
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/prolog.php');

$GLOBALS['APPLICATION']->SetTitle('Актуализация справочника Улицы');

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

if($arPostingErrors) {
	CAdminMessage::ShowOldStyleError(implode('<br />', $arPostingErrors));
}

if(!$bSuccessUpdate) {
	$sFormName = 'adv_import_data_streets';
	?><form id="<?=$sFormName?>" name="<?=$sFormName?>" class="adv_import_data_streets" method="post" action="" enctype="multipart/form-data"><div><?
		echo bitrix_sessid_post();
		?><p>Данная операция запустит импорт справочника Улицы из системы FastOperator.<br />Все элементы инфоблока "Улицы" будут обновлены. Элементы, которых нет в импортируемых данных, будут деактивированы.</p><?
		?><p>Процедура импорта может занять несколько минут.</p><?
		?><input type="hidden" name="IMPORT" value="Y" /><?
		?><input type="hidden" name="lang" value="<?=LANG?>" /><?
		?><input type="submit" name="button" value="Импортировать" /><?
	?></div></form><?
} else {
	echo ShowMessage(array('TYPE' => 'OK', 'MESSAGE' => 'Данные успешно импортированы.'));
	?><a href="/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=<?=$iIBlockId?>&type=data&find_el_y=Y&lang=ru">Перейти в инфоблок</a><?
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');

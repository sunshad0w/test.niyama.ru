<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Катомное меню административной панели, добавляемое по событию OnBuildGlobalMenu
//

if(!CModule::IncludeModule('iblock')) {
	return;
}

$arTmpSubitems = array();

// Импортируемое меню
$sIBlockPermission = CNiyamaIBlockCatalogBase::GetCurUserPermissions();
if($sIBlockPermission >= 'W') {
	$arTmpSubitems[] = array(
		'text' => 'Актуализация меню из FastOperator',
		'url' => '/bitrix/admin/adv_import_catalog_base.php',
		'more_url' => array(),
		'title' => 'Обновление инфоблока "Импортируемое меню"',
		'sort' => '100',
		'icon' => '',
		'page_icon' => '',
		'module_id' => 'iblock',
		'items_id' => '',
		'dynamic' => false,
		'items' => array()
	);
}

// Справочник Департаменты
$sIBlockPermission = CNiyamaIBlockDepartments::GetCurUserPermissions();
if($sIBlockPermission >= 'W') {
	$arTmpSubitems[] = array(
		'text' => 'Актуализация справочника Департаменты',
		'url' => '/bitrix/admin/adv_import_data_departments.php',
		'more_url' => array(),
		'title' => 'Обновление инфоблока "Департаменты"',
		'sort' => '200',
		'icon' => '',
		'page_icon' => '',
		'module_id' => 'iblock',
		'items_id' => '',
		'dynamic' => false,
		'items' => array()
	);
}

// Справочник Города
$sIBlockPermission = CNiyamaIBlockCities::GetCurUserPermissions();
if($sIBlockPermission >= 'W') {
	$arTmpSubitems[] = array(
		'text' => 'Актуализация справочника Города',
		'url' => '/bitrix/admin/adv_import_data_cities.php',
		'more_url' => array(),
		'title' => 'Обновление инфоблока "Города"',
		'sort' => '300',
		'icon' => '',
		'page_icon' => '',
		'module_id' => 'iblock',
		'items_id' => '',
		'dynamic' => false,
		'items' => array()
	);
}

// Справочник Улицы
$sIBlockPermission = CNiyamaIBlockStreets::GetCurUserPermissions();
if($sIBlockPermission >= 'W') {
	$arTmpSubitems[] = array(
		'text' => 'Актуализация справочника Улицы',
		'url' => '/bitrix/admin/adv_import_data_streets.php',
		'more_url' => array(
		),
		'title' => 'Обновление инфоблока "Улицы"',
		'sort' => '400',
		'icon' => '',
		'page_icon' => '',
		'module_id' => 'iblock',
		'items_id' => '',
		'dynamic' => false,
		'items' => array()
	);
}

// Справочник Станции метро
$sIBlockPermission = CNiyamaIBlockSubway::GetCurUserPermissions();
if($sIBlockPermission >= 'W') {
	$arTmpSubitems[] = array(
		'text' => 'Актуализация справочника Станции метро',
		'url' => '/bitrix/admin/adv_import_data_subway.php',
		'more_url' => array(
		),
		'title' => 'Обновление инфоблока "Станции метро"',
		'sort' => '500',
		'icon' => '',
		'page_icon' => '',
		'module_id' => 'iblock',
		'items_id' => '',
		'dynamic' => false,
		'items' => array()
	);
}

if($arTmpSubitems) {
	$arModuleMenu[] = array(
		'text' => 'Интеграция с внешними системами',
		'url' => '/bitrix/admin/adv_import_index.php',
		'more_url' => array(
			'/bitrix/admin/adv_import_catalog_base.php',
			'/bitrix/admin/adv_import_data_departments.php',
			'/bitrix/admin/adv_import_data_cities.php',
			'/bitrix/admin/adv_import_data_streets.php',
			'/bitrix/admin/adv_import_data_subway.php',
		),
		'title' => '',
		'parent_menu' => 'global_menu_content',
		'sort' => '310',
		'icon' => '',
		'page_icon' => '',
		'section' => '',
		'module_id' => 'iblock',
		'items_id' => 'adv_import_index',
		'dynamic' => false,
		'items' => $arTmpSubitems
	);
}

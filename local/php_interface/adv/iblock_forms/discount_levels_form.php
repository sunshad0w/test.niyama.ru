<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Форма для инфоблока "Уровни скидок"
//

CJSCore::Init(array('jquery'));
//$GLOBALS['APPLICATION']->SetAdditionalCSS('/local/php_interface/adv/admin/public/discount_levels_form.css');
//$GLOBALS['APPLICATION']->AddHeadString('<script type="text/javascript" src="'.CUtil::GetAdditionalFileURL('/local/php_interface/adv/admin/public/discount_levels_form.js').'"></script>');

$iIBlockId = CNiyamaDiscountLevels::GetIBlockId();
$iRegistrationTypeId = CNiyamaDiscountLevels::GetEnumValIdByXmlId('registration', 'DISCOUNT_TYPE');
$iOrderTypeId = CNiyamaDiscountLevels::GetEnumValIdByXmlId('order', 'DISCOUNT_TYPE');

$bEditFieldsEnabled = true;
if($ID > 0 && !$bCopy) {
	$bEditFieldsEnabled = false;
	$sTmpFieldKey = '_NIYAMA_DISCOUNT_LEVEL_ELEMENT_'.intval($ID);
	if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST[$sTmpFieldKey] == 'Y' && $_POST['_NIYAMA_DISCOUNT_LEVEL_EDIT_ENABLED_'] == 'Y' && $strWarning) {
		$bEditFieldsEnabled = true;
	}
}

ob_start();
?><script type="text/javascript">
var jsNiyamaDiscLevelForm = {
	ToggleTypeSelect: function(jqSelect) {
		var sVal = jqSelect.val();
		switch(sVal) {
			case '<?=$iRegistrationTypeId?>':
				jQuery('tr.niyama-order-type').hide();
				jQuery('tr.niyama-register-type').show();
				jQuery('div.niyama-field-required-order-type').css({
					'font-weight': 'normal'
				});
			break;

			case '<?=$iOrderTypeId?>':
				jQuery('tr.niyama-register-type').hide();
				jQuery('tr.niyama-order-type').show();
				jQuery('div.niyama-field-required-order-type').css({
					'font-weight': 'bold'
				});
			break;
			
			default:
				jQuery('tr.niyama-order-type').hide();
				jQuery('tr.niyama-register-type').hide();
			break;
		}
	},
	DisableCheckbox: function() {
		return false;
	},
	SwitchControlledFields: function(bDisable) {
		bDisable = bDisable == undefined ? true : bDisable;
		jQuery('td.controlled-prop-cell input, td.controlled-prop-cell textarea, td.controlled-prop-cell select').attr('readonly', bDisable);
		jQuery('td.controlled-field-cell input, td.controlled-field-cell textarea, td.controlled-field-cell select').attr('readonly', bDisable);

		jQuery('td.controlled-prop-cell input:file, td.controlled-prop-cell input:submit, td.controlled-prop-cell input:button').attr('disabled', bDisable);
		jQuery('td.controlled-field-cell input:file, td.controlled-field-cell input:submit, td.controlled-field-cell input:button').attr('disabled', bDisable);

		jQuery('td.controlled-prop-cell select option:not(:selected)').attr('disabled', bDisable);
		jQuery('td.controlled-field-cell select option:not(:selected)').attr('disabled', bDisable);

		jQuery('td.controlled-prop-cell input:radio:not(:checked)').attr('disabled', bDisable);
		jQuery('td.controlled-field-cell input:radio:not(:checked)').attr('disabled', bDisable);

		if(bDisable) {
			jQuery('td.controlled-prop-cell input:checkbox').bind(
				'click', 
				jsNiyamaDiscLevelForm.DisableCheckbox
			);
			jQuery('td.controlled-field-cell input:checkbox').bind(
				'click', 
				jsNiyamaDiscLevelForm.DisableCheckbox
			);
		} else {
			jQuery('td.controlled-prop-cell input:checkbox').unbind(
				'click', 
				jsNiyamaDiscLevelForm.DisableCheckbox
			);
			jQuery('td.controlled-field-cell input:checkbox').unbind(
				'click', 
				jsNiyamaDiscLevelForm.DisableCheckbox
			);
		}

		if(bDisable) {
			jQuery('td.controlled-field-cell .add-file-popup-btn').hide();
		} else {
			jQuery('td.controlled-field-cell .add-file-popup-btn').show();
		}

		jQuery('#niyama-edit-field-enabled').val((bDisable ? 'N' : 'Y'));
	},
	EnableFieldsEdit: function(obNode) {
		jsNiyamaDiscLevelForm.SwitchControlledFields(false);
		jQuery(obNode).hide();
		jQuery('#fields-edit-switch-note').show();
	}
};

jQuery(document).ready(
	function() {
		var obSelect = jQuery('tr.niyama-type-select select').get(0);
		if(!obSelect) {
			return;
		}
		var jqSelect = jQuery(obSelect);
		jsNiyamaDiscLevelForm.ToggleTypeSelect(jqSelect);
		jqSelect.bind(
			'change',
			function() {
				jsNiyamaDiscLevelForm.ToggleTypeSelect(jqSelect);
			}
		);
		<?
		if($ID > 0 && !$bCopy) {
			?>
			jQuery('#niyama-edit-field-flag').val('Y');
			<?
		}
		if($bEditFieldsEnabled) {
			?>
			jsNiyamaDiscLevelForm.SwitchControlledFields(false);
			<?
		} else {
			?>
			jsNiyamaDiscLevelForm.SwitchControlledFields(true);
			<?
		}
		?>
	}
);
</script><?

$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());


$tabControl->BeginPrologContent();
echo CAdminCalendar::ShowScript();

if(COption::GetOptionString('iblock', 'use_htmledit', 'Y') == 'Y' && $bFileman) {
	// TODO:This dirty hack will be replaced by special method like calendar do
	echo '<div style="display:none">';
	CFileMan::AddHTMLEditorFrame(
		'SOME_TEXT',
		'',
		'SOME_TEXT_TYPE',
		'text',
		array(
			'height' => 450,
			'width' => '100%'
		),
		'N',
		0,
		'',
		'',
		$arIBlock['LID']
	);
	echo '</div>';
}

if($arTranslit['TRANSLITERATION'] == 'Y') {
	CJSCore::Init(array('translit'));
	?><script type="text/javascript">
		var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
		function set_linked()
		{
			linked=!linked;

			var name_link = document.getElementById('name_link');
			if(name_link)
			{
				if(linked)
					name_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					name_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var code_link = document.getElementById('code_link');
			if(code_link)
			{
				if(linked)
					code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var linked_state = document.getElementById('linked_state');
			if(linked_state)
			{
				if(linked)
					linked_state.value='Y';
				else
					linked_state.value='N';
			}
		}
		var oldValue = '';
		function transliterate()
		{
			if(linked)
			{
				var from = document.getElementById('NAME');
				var to = document.getElementById('CODE');
				if(from && to && oldValue != from.value)
				{
					BX.translit(from.value, {
						'max_len' : <?=intval($arTranslit['TRANS_LEN'])?>,
						'change_case' : '<?=$arTranslit['TRANS_CASE']?>',
						'replace_space' : '<?=$arTranslit['TRANS_SPACE']?>',
						'replace_other' : '<?=$arTranslit['TRANS_OTHER']?>',
						'delete_repeat_replace' : <?=$arTranslit['TRANS_EAT'] == 'Y'? 'true': 'false'?>,
						'use_google' : <?=$arTranslit['USE_GOOGLE'] == 'Y'? 'true': 'false'?>,
						'callback' : function(result){to.value = result; setTimeout('transliterate()', 250); }
					});
					oldValue = from.value;
				}
				else
				{
					setTimeout('transliterate()', 250);
				}
			}
			else
			{
				setTimeout('transliterate()', 250);
			}
		}
		transliterate();
	</script><?
}

?><script type="text/javascript">
	var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
		'<?=$tabControl->GetName()?>_form',
		'/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?=intval($IBLOCK_ID)?>&ENTITY_ID=<?=intval($ID)?>'
	);
	BX.ready(
		function() {
			setTimeout(
				function() {
					InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
				}, 
				1000
			);
		}
	);
</script><?

$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();

echo bitrix_sessid_post();
echo GetFilterHiddens('find_');

?><input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>" />
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="from" value="<?=htmlspecialcharsbx($from)?>" />
<input type="hidden" name="WF" value="<?=htmlspecialcharsbx($WF)?>" />
<input type="hidden" name="return_url" value="<?=htmlspecialcharsbx($return_url)?>" /><?
if($ID > 0 && !$bCopy) {
	?><input type="hidden" name="ID" value="<?=$ID?>" /><?

	// !!! специальное поле-флаг, которое разблокирует возможность редактирования элемента !!!
	// открывающее значение "Y" устанавливается через js
	?><input type="hidden" id="niyama-edit-field-flag" name="<?='_NIYAMA_DISCOUNT_LEVEL_ELEMENT_'.intval($ID)?>" value="N" /><?
	?><input type="hidden" id="niyama-edit-field-enabled" name="_NIYAMA_DISCOUNT_LEVEL_EDIT_ENABLED_" value="N" /><?
}
if($bCopy) {
	?><input type="hidden" name="copyID" value="<?=intval($ID)?>" /><?
}
if($bCatalog) {
	CCatalogAdminTools::showFormParams();
}

?><input type="hidden" name="IBLOCK_SECTION_ID" value="<?=intval($IBLOCK_SECTION_ID)?>" /><?
?><input type="hidden" name="TMP_ID" value="<?=intval($TMP_ID)?>" /><?

// костылики
// привязка к секции
?><input type="hidden" name="IBLOCK_SECTION[]" value="0" /><?
// символьный код
?><input type="hidden" name="CODE" value="<?=$str_CODE?>" /><?
// xml_id
if(COption::GetOptionString('iblock', 'show_xml_id', 'N') == 'Y') {
	?><input type="hidden" name="XML_ID" value="<?=$str_XML_ID?>" /><?
}


$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);
$tabControl->AddTabs($customTabber);

$arEditLinkParams = array(
	'find_section_section' => intval($find_section_section)
);
if($bAutocomplete) {
	$arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(
	array(
		'FORM_ACTION' => '/bitrix/admin/'.CIBlock::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
	)
);

$tabControl->BeginNextFormTab();
if($ID > 0 && !$bCopy) {
	$p = CIblockElement::GetByID($ID);
	$pr = $p->ExtractFields('prn_');
} else {
	$pr = array();
}
$tabControl->AddCheckBoxField('ACTIVE', GetMessage('IBLOCK_FIELD_ACTIVE').':', false, array('Y', 'N'), $str_ACTIVE == 'Y');

if($ID > 0 && !$bCopy) {
	$tabControl->BeginCustomField('_custom_1_', '');
	?><tr>
		<td>&nbsp;</td>
		<td><?
			if(!$bEditFieldsEnabled) {
				?><a onclick="return jsNiyamaDiscLevelForm.EnableFieldsEdit(this)" style="text-decoration: none; border-bottom: dashed 1px blue;" href="javascript:void(0)">Редактировать поля</a><?
				?><div style="display: none" id="fields-edit-switch-note">Режим редактирования полей<br /><b>Внимние! Все выполненные изменения отразятся и на уже достигнутых статусах пользователей</b></div><?
			} else {
				?><div id="fields-edit-switch-note">Режим редактирования полей<br /><b>Внимние! Все выполненные изменения отразятся и на уже достигнутых статусах пользователей</b></div><?
			}
		?></td>
	</tr><?
	$tabControl->EndCustomField('_custom_1_', '');
}

/*
$tabControl->BeginCustomField('ACTIVE_FROM', GetMessage('IBLOCK_FIELD_ACTIVE_PERIOD_FROM'), $arIBlock['FIELDS']['ACTIVE_FROM']['IS_REQUIRED'] === 'Y');
?><tr id="tr_ACTIVE_FROM">
	<td><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td><?=CAdminCalendar::CalendarDate('ACTIVE_FROM', $str_ACTIVE_FROM, 19, true)?></td>
</tr><?
$tabControl->EndCustomField('ACTIVE_FROM', '<input type="hidden" id="ACTIVE_FROM" name="ACTIVE_FROM" value="'.$str_ACTIVE_FROM.'" />');

$tabControl->BeginCustomField('ACTIVE_TO', GetMessage('IBLOCK_FIELD_ACTIVE_PERIOD_TO'), $arIBlock['FIELDS']['ACTIVE_TO']['IS_REQUIRED'] === 'Y');
?><tr id="tr_ACTIVE_TO">
	<td><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td><?=CAdminCalendar::CalendarDate('ACTIVE_TO', $str_ACTIVE_TO, 19, true)?></td>
</tr><?
$tabControl->EndCustomField("ACTIVE_TO", '<input type="hidden" id="ACTIVE_TO" name="ACTIVE_TO" value="'.$str_ACTIVE_TO.'">');
*/

$tabControl->BeginCustomField('NAME', GetMessage('IBLOCK_FIELD_NAME').':', true);
?><tr id="tr_NAME">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<?=$str_NAME?>" /><?
		if($arTranslit['TRANSLITERATION'] == 'Y') {
			?><image id="name_link" title="<?=GetMessage('IBEL_E_LINK_TIP')?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" /><?
		}
	?></td>
</tr><?
$tabControl->EndCustomField('NAME', '<input type="hidden" name="NAME" id="NAME" value="'.$str_NAME.'" />');

/*
$tabControl->BeginCustomField('CODE', GetMessage('IBLOCK_FIELD_CODE').':', $arIBlock['FIELDS']['CODE']['IS_REQUIRED'] === 'Y');
?><tr id="tr_CODE">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<?=$str_CODE?>" /><?
		if($arTranslit['TRANSLITERATION'] == 'Y') {
			?><image id="code_link" title="<?=GetMessage('IBEL_E_LINK_TIP')?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" /><?
		}
	?></td>
</tr><?
$tabControl->EndCustomField('CODE', '<input type="hidden" name="CODE" id="CODE" value="'.$str_CODE.'" />');


if(COption::GetOptionString('iblock', 'show_xml_id', 'N') == 'Y') {
	$tabControl->AddEditField('XML_ID', GetMessage('IBLOCK_FIELD_XML_ID').':', $arIBlock['FIELDS']['XML_ID']['IS_REQUIRED'] === 'Y', array('size' => 20, 'maxlength' => 255), $str_XML_ID);
}
*/

$tabControl->BeginCustomField('SORT', GetMessage('IBLOCK_FIELD_SORT').':', $arIBlock['FIELDS']['SORT']['IS_REQUIRED'] === 'Y');
?><tr id="tr_CODE">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="7" name="SORT" id="SORT" maxlength="10" value="<?=$str_SORT?>" />
	</td>
</tr><?
$tabControl->EndCustomField('SORT', '<input type="hidden" name="SORT" id="SORT" value="'.$str_SORT.'" />');


$tabControl->BeginCustomField('PREVIEW_TEXT', 'Краткое описание', $arIBlock['FIELDS']['PREVIEW_TEXT']['IS_REQUIRED'] === 'Y');
?><tr class="heading" id="tr_PREVIEW_TEXT_LABEL">
	<td colspan="2"><?=$tabControl->GetCustomLabelHTML()?></td>
</tr><?
if($ID && $PREV_ID && $bWorkflow) {
	?><tr id="tr_PREVIEW_TEXT_DIFF">
		<td colspan="2" class="controlled-field-cell">
			<div style="width:95%;background-color:white;border:1px solid black;padding:5px">
				<?=getDiff($prev_arElement['PREVIEW_TEXT'], $arElement['PREVIEW_TEXT'])?>
			</div>
		</td>
	</tr><?
} elseif(COption::GetOptionString('iblock', 'use_htmledit', 'Y') == 'Y' && $bFileman) {
	?><tr id="tr_PREVIEW_TEXT_EDITOR">
		<td colspan="2" align="center" class="controlled-field-cell"><?
			CFileMan::AddHTMLEditorFrame(
				'PREVIEW_TEXT',
				$str_PREVIEW_TEXT,
				'PREVIEW_TEXT_TYPE',
				$str_PREVIEW_TEXT_TYPE,
				array(
					'height' => 150,
					'width' => '100%'
				),
				'N',
				0,
				'',
				'',
				$arIBlock['LID'],
				true,
				false,
				array(
					'toolbarConfig' => CFileman::GetEditorToolbarConfig('iblock_'.(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
					'saveEditorKey' => $IBLOCK_ID,
					'saveEditorState' => false
				)
			);
		?></td>
	</tr><?
} else {
	?><tr id="tr_PREVIEW_TEXT_TYPE" class="controlled-field-cell">
		<td><?=GetMessage('IBLOCK_DESC_TYPE')?></td>
		<td><input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_text" value="text"<?if($str_PREVIEW_TEXT_TYPE!="html")echo " checked"?> /><label for="PREVIEW_TEXT_TYPE_text"><?=GetMessage("IBLOCK_DESC_TYPE_TEXT")?></label> / <input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_html" value="html"<?if($str_PREVIEW_TEXT_TYPE=="html")echo " checked"?>> <label for="PREVIEW_TEXT_TYPE_html"><?=GetMessage("IBLOCK_DESC_TYPE_HTML")?></label></td>
	</tr>
	<tr id="tr_PREVIEW_TEXT">
		<td colspan="2" align="center" class="controlled-field-cell">
			<textarea cols="60" rows="10" name="PREVIEW_TEXT" style="width:100%"><?=$str_PREVIEW_TEXT?></textarea>
		</td>
	</tr><?
}
$tabControl->EndCustomField('PREVIEW_TEXT', '<input type="hidden" name="PREVIEW_TEXT" value="'.$str_PREVIEW_TEXT.'" />'.'<input type="hidden" name="PREVIEW_TEXT_TYPE" value="'.$str_PREVIEW_TEXT_TYPE.'" />');

$tabControl->BeginCustomField('DETAIL_TEXT', 'Полное описание', $arIBlock['FIELDS']['DETAIL_TEXT']['IS_REQUIRED'] === 'Y');
?><tr class="heading" id="tr_DETAIL_TEXT_LABEL">
	<td colspan="2"><?=$tabControl->GetCustomLabelHTML()?></td>
</tr><?
if($ID && $PREV_ID && $bWorkflow) {
	?><tr id="tr_DETAIL_TEXT_DIFF">
		<td colspan="2" class="controlled-field-cell">
			<div style="width:95%;background-color:white;border:1px solid black;padding:5px">
				<?=getDiff($prev_arElement['DETAIL_TEXT'], $arElement['DETAIL_TEXT'])?>
			</div>
		</td>
	</tr><?
} elseif(COption::GetOptionString('iblock', 'use_htmledit', 'Y') == 'Y' && $bFileman) {
	?><tr id="tr_DETAIL_TEXT_EDITOR">
		<td colspan="2" align="center" class="controlled-field-cell"><?
			CFileMan::AddHTMLEditorFrame(
				'DETAIL_TEXT',
				$str_DETAIL_TEXT,
				'DETAIL_TEXT_TYPE',
				$str_DETAIL_TEXT_TYPE,
				array(
					'height' => 250,
					'width' => '100%'
				),
				'N',
				0,
				'',
				'',
				$arIBlock['LID'],
				true,
				false,
				array(
					'toolbarConfig' => CFileman::GetEditorToolbarConfig('iblock_'.(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')), 
					'saveEditorKey' => $IBLOCK_ID,
					'saveEditorState' => false
				)
			);
		?></td>
	</tr><?
} else {
	?><tr id="tr_DETAIL_TEXT_TYPE" class="controlled-field-cell">
		<td><?=GetMessage('IBLOCK_DESC_TYPE')?></td>
		<td><input type="radio" name="DETAIL_TEXT_TYPE" id="DETAIL_TEXT_TYPE_text" value="text"<?if($str_DETAIL_TEXT_TYPE!="html")echo " checked"?> /><label for="DETAIL_TEXT_TYPE_text"><?=GetMessage("IBLOCK_DESC_TYPE_TEXT")?></label> / <input type="radio" name="DETAIL_TEXT_TYPE" id="DETAIL_TEXT_TYPE_html" value="html"<?if($str_DETAIL_TEXT_TYPE=="html")echo " checked"?>> <label for="DETAIL_TEXT_TYPE_html"><?=GetMessage("IBLOCK_DESC_TYPE_HTML")?></label></td>
	</tr>
	<tr id="tr_DETAIL_TEXT">
		<td colspan="2" align="center" class="controlled-field-cell">
			<textarea cols="60" rows="20" name="DETAIL_TEXT" style="width:100%"><?=$str_DETAIL_TEXT?></textarea>
		</td>
	</tr><?
}
$tabControl->EndCustomField('DETAIL_TEXT', '<input type="hidden" name="DETAIL_TEXT" value="'.$str_DETAIL_TEXT.'" />'.'<input type="hidden" name="DETAIL_TEXT_TYPE" value="'.$str_DETAIL_TEXT_TYPE.'" />');

// --
$tabControl->BeginCustomField('PREVIEW_PICTURE', 'Маркер', $arIBlock['FIELDS']['PREVIEW_PICTURE']['IS_REQUIRED'] === 'Y');
if($bVarsFromForm && !array_key_exists('PREVIEW_PICTURE', $_REQUEST) && $arElement) {
	$str_PREVIEW_PICTURE = intval($arElement['PREVIEW_PICTURE']);
}
?><tr id="tr_PREVIEW_PICTURE" class="adm-detail-file-row">
	<td width="40%" class="adm-detail-valign-top"><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td width="60%" class="controlled-field-cell"><?
		if($historyId > 0) {
			echo CFileInput::Show(
				'PREVIEW_PICTURE', 
				$str_PREVIEW_PICTURE, 
				array(
					'IMAGE' => 'Y',
					'PATH' => 'Y',
					'FILE_SIZE' => 'Y',
					'DIMENSIONS' => 'Y',
					'IMAGE_POPUP' => 'Y',
					'MAX_SIZE' => array(
						'W' => COption::GetOptionString('iblock', 'detail_image_size'),
						'H' => COption::GetOptionString('iblock', 'detail_image_size'),
					),
				)
			);
		} else {
			echo CFileInput::Show(
				'PREVIEW_PICTURE', ($ID > 0 && !$bCopy ? $str_PREVIEW_PICTURE : 0),
				array(
					'IMAGE' => 'Y',
					'PATH' => 'Y',
					'FILE_SIZE' => 'Y',
					'DIMENSIONS' => 'Y',
					'IMAGE_POPUP' => 'Y',
					'MAX_SIZE' => array(
						'W' => COption::GetOptionString('iblock', 'detail_image_size'),
						'H' => COption::GetOptionString('iblock', 'detail_image_size'),
					),
				), 
				array(
					'upload' => true,
					'medialib' => true,
					'file_dialog' => true,
					'cloud' => true,
					'del' => true,
					'description' => true,
				)
			);
		}
	?></td>
</tr><?
$tabControl->EndCustomField('PREVIEW_PICTURE', '');


$tabControl->BeginCustomField('DETAIL_PICTURE', 'Изображение для карточки купона', $arIBlock['FIELDS']['DETAIL_PICTURE']['IS_REQUIRED'] === 'Y');
if($bVarsFromForm && !array_key_exists('DETAIL_PICTURE', $_REQUEST) && $arElement) {
	$str_DETAIL_PICTURE = intval($arElement['DETAIL_PICTURE']);
}
?><tr id="tr_DETAIL_PICTURE" class="adm-detail-file-row">
	<td width="40%" class="adm-detail-valign-top"><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td width="60%" class="controlled-field-cell"><?
		if($historyId > 0) {
			echo CFileInput::Show(
				'DETAIL_PICTURE', 
				$str_DETAIL_PICTURE, 
				array(
					'IMAGE' => 'Y',
					'PATH' => 'Y',
					'FILE_SIZE' => 'Y',
					'DIMENSIONS' => 'Y',
					'IMAGE_POPUP' => 'Y',
					'MAX_SIZE' => array(
						'W' => COption::GetOptionString('iblock', 'detail_image_size'),
						'H' => COption::GetOptionString('iblock', 'detail_image_size'),
					),
				)
			);
		} else {
			echo CFileInput::Show(
				'DETAIL_PICTURE', 
				($ID > 0 && !$bCopy ? $str_DETAIL_PICTURE : 0),
				array(
					'IMAGE' => 'Y',
					'PATH' => 'Y',
					'FILE_SIZE' => 'Y',
					'DIMENSIONS' => 'Y',
					'IMAGE_POPUP' => 'Y',
					'MAX_SIZE' => array(
						'W' => COption::GetOptionString('iblock', 'detail_image_size'),
						'H' => COption::GetOptionString('iblock', 'detail_image_size'),
					),
				), 
				array(
					'upload' => true,
					'medialib' => true,
					'file_dialog' => true,
					'cloud' => true,
					'del' => true,
					'description' => true,
				)
			);
		}
	?></td>
</tr><?
$tabControl->EndCustomField('DETAIL_PICTURE', '');

// DISCOUNT_TYPE
$prop_code = 'DISCOUNT_TYPE';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-type-select<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// DISCOUNT_VAL_PERC
$prop_code = 'DISCOUNT_VAL_PERC';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type niyama-order-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			?><div class="niyama-field-required-order-type"><?
				echo $tabControl->GetCustomLabelHTML().':';
			?></div><?
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// DISCOUNT_VAL_RUB
$prop_code = 'DISCOUNT_VAL_RUB';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// NIYAM_CNT
$prop_code = 'NIYAM_CNT';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// BONUS_DISH
$prop_code = 'BONUS_DISH';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// LEVEL_CONDITION
$prop_code = 'LEVEL_CONDITION';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-order-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			?><div class="niyama-field-required-order-type"><?
				echo $tabControl->GetCustomLabelHTML().':';
			?></div><?
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// B_ONE_TIME
$prop_code = 'B_ONE_TIME';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type niyama-order-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// B_SINGLE
$prop_code = 'B_SINGLE';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type niyama-order-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// остальные свойства
if($PROP) {
	foreach($PROP as $prop_code => $prop_fields) {
		$prop_values = $prop_fields['VALUE'];
		$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
		$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
		?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-other-props<?=$sTmpClass?>">
			<td class="adm-detail-valign-top" width="40%"><?
				if(strlen($prop_fields['HINT'])) {
					?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
				}
				echo $tabControl->GetCustomLabelHTML().':';
			?></td>
			<td width="60%"><?
				_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
			?></td>
		</tr><?
		$hidden = '';
		if(!is_array($prop_fields['~VALUE'])) {
			$values = array();
		} else {
			$values = $prop_fields['~VALUE'];
		}
		$start = 1;
		foreach($values as $key => $val) {
			if($bCopy) {
				$key = 'n'.$start;
				$start++;
			}
			if(is_array($val) && array_key_exists('VALUE', $val)) {
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
			} else {
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
			}
		}
		$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	}	
}

$bDisabled = ($view == 'Y') || ($bWorkflow && $prn_LOCK_STATUS == 'red') || ((($ID <= 0) || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, 'section_element_bind')) || ((($ID > 0) && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, 'element_edit')) || ($bBizproc && !$canWrite);
if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1) {
	$sTmpDisabled = $bDisabled ? ' disabled="disabled"' : '';
	ob_start();
	?><input<?=$sTmpDisabled?> type="submit" class="adm-btn-save" name="save" id="save" value="<?=GetMessage('IBLOCK_EL_SAVE')?>" /><? 
	if(!$bAutocomplete) {
		?><input<?=$sTmpDisabled?> type="submit" class="button" name="apply" id="apply" value="<?=GetMessage('IBLOCK_APPLY')?>" /><?
	}
	?><input<?=$sTmpDisabled?> type="submit" class="button" name="dontsave" id="dontsave" value="<?=GetMessage('IBLOCK_EL_CANC')?>" /><?
	if(!$bAutocomplete) {
		?><input<?=$sTmpDisabled?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?=GetMessage('IBLOCK_EL_SAVE_AND_ADD')?>" /><?
	}
	$buttons_add_html = ob_get_clean();
	$tabControl->Buttons(false, $buttons_add_html);
} elseif(!$bPropertyAjax && $nobuttons !== 'Y') {

	$wfClose = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_CANC'))."',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$save_and_add = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_SAVE_AND_ADD'))."',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$cancel = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_CANC'))."',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
	$tabControl->ButtonsPublic(
		array(
			'.btnSave',
			($ID > 0 && $bWorkflow ? $wfClose : $cancel),
			$save_and_add,
		)
	);
}

$tabControl->Show();
if((!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1) && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, 'iblock_edit') && !$bAutocomplete) {
	echo
		BeginNote(),
		GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
		' <a href="/bitrix/admin/iblock_edit.php?type='.htmlspecialcharsbx($type).'&amp;lang='.LANGUAGE_ID.'&amp;ID='.$IBLOCK_ID.'&amp;admin=Y&amp;return_url='.urlencode("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF=="Y"? "Y": null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url)>0? $return_url: null)))).'">',
		GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
		'</a>',
		EndNote()
	;
}

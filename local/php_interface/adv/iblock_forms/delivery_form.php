<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Форма для инфоблока "Уровни скидок"
//

CJSCore::Init(array('jquery'));
//$GLOBALS['APPLICATION']->SetAdditionalCSS('/local/php_interface/adv/admin/public/discount_levels_form.css');
//$GLOBALS['APPLICATION']->AddHeadString('<script type="text/javascript" src="'.CUtil::GetAdditionalFileURL('/local/php_interface/adv/admin/public/discount_levels_form.js').'"></script>');

$iMoscowCityId = 0;
$arDeliveryRegions = CNiyamaOrders::GetDeliveryRegions();
foreach($arDeliveryRegions as $arRegion) {
	if(ToUpper($arRegion['NAME']) == 'МОСКВА') {
		$iMoscowCityId = $arRegion['ID'];
		break;
	}
}

ob_start();

?><script type="text/javascript">
var jsNiyamaDeliveryForm = {
	ToggleRegionSelect: function(jqSelect) {
		var bDisable = true;
		var arVal = jqSelect.val();
		if(arVal) {
			for(var i in arVal) {
				if(arVal[i] == '<?=$iMoscowCityId?>') {
					bDisable = false;
				}
			}
		}
		jsNiyamaDeliveryForm.SwitchControlledFields(bDisable);
	},
	SwitchControlledFields: function(bDisable) {
		bDisable = bDisable == undefined ? true : bDisable;

		jQuery('td.region-controlled-prop-cell input:checkbox').attr('disabled', bDisable);

		if(bDisable) {
			jQuery('tr.niyama-mkad-row').hide();
		} else {
			jQuery('tr.niyama-mkad-row').show();
		}
	}
};

jQuery(document).ready(
	function() {
		var obSelect = jQuery('tr.niyama-region-select select').get(0);
		if(!obSelect) {
			return;
		}
		var jqSelect = jQuery(obSelect);
		jsNiyamaDeliveryForm.ToggleRegionSelect(jqSelect);
		jqSelect.bind(
			'change',
			function() {
				jsNiyamaDeliveryForm.ToggleRegionSelect(jqSelect);
			}
		);
	}
);
</script><?

$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());


$tabControl->BeginPrologContent();
echo CAdminCalendar::ShowScript();

if($arTranslit['TRANSLITERATION'] == 'Y') {
	CJSCore::Init(array('translit'));
	?><script type="text/javascript">
		var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
		function set_linked()
		{
			linked=!linked;

			var name_link = document.getElementById('name_link');
			if(name_link)
			{
				if(linked)
					name_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					name_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var code_link = document.getElementById('code_link');
			if(code_link)
			{
				if(linked)
					code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var linked_state = document.getElementById('linked_state');
			if(linked_state)
			{
				if(linked)
					linked_state.value='Y';
				else
					linked_state.value='N';
			}
		}
		var oldValue = '';
		function transliterate()
		{
			if(linked)
			{
				var from = document.getElementById('NAME');
				var to = document.getElementById('CODE');
				if(from && to && oldValue != from.value)
				{
					BX.translit(from.value, {
						'max_len' : <?=intval($arTranslit['TRANS_LEN'])?>,
						'change_case' : '<?=$arTranslit['TRANS_CASE']?>',
						'replace_space' : '<?=$arTranslit['TRANS_SPACE']?>',
						'replace_other' : '<?=$arTranslit['TRANS_OTHER']?>',
						'delete_repeat_replace' : <?=$arTranslit['TRANS_EAT'] == 'Y'? 'true': 'false'?>,
						'use_google' : <?=$arTranslit['USE_GOOGLE'] == 'Y'? 'true': 'false'?>,
						'callback' : function(result){to.value = result; setTimeout('transliterate()', 250); }
					});
					oldValue = from.value;
				}
				else
				{
					setTimeout('transliterate()', 250);
				}
			}
			else
			{
				setTimeout('transliterate()', 250);
			}
		}
		transliterate();
	</script><?
}

?><script type="text/javascript">
	var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
		'<?=$tabControl->GetName()?>_form',
		'/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?=intval($IBLOCK_ID)?>&ENTITY_ID=<?=intval($ID)?>'
	);
	BX.ready(
		function() {
			setTimeout(
				function() {
					InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
				}, 
				1000
			);
		}
	);
</script><?

$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();

echo bitrix_sessid_post();
echo GetFilterHiddens('find_');

?><input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>" />
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="from" value="<?=htmlspecialcharsbx($from)?>" />
<input type="hidden" name="WF" value="<?=htmlspecialcharsbx($WF)?>" />
<input type="hidden" name="return_url" value="<?=htmlspecialcharsbx($return_url)?>" /><?
if($ID > 0 && !$bCopy) {
	?><input type="hidden" name="ID" value="<?=$ID?>" /><?
}
if($bCopy) {
	?><input type="hidden" name="copyID" value="<?=intval($ID)?>" /><?
}
if($bCatalog) {
	CCatalogAdminTools::showFormParams();
}

?><input type="hidden" name="IBLOCK_SECTION_ID" value="<?=intval($IBLOCK_SECTION_ID)?>" /><?
?><input type="hidden" name="TMP_ID" value="<?=intval($TMP_ID)?>" /><?

// костылики
// привязка к секции
?><input type="hidden" name="IBLOCK_SECTION[]" value="0" /><?
// символьный код
?><input type="hidden" name="CODE" value="<?=$str_CODE?>" /><?
// xml_id
if(COption::GetOptionString('iblock', 'show_xml_id', 'N') == 'Y') {
	?><input type="hidden" name="XML_ID" value="<?=$str_XML_ID?>" /><?
}


$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);
$tabControl->AddTabs($customTabber);

$arEditLinkParams = array(
	'find_section_section' => intval($find_section_section)
);
if($bAutocomplete) {
	$arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(
	array(
		'FORM_ACTION' => '/bitrix/admin/'.CIBlock::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
	)
);

$tabControl->BeginNextFormTab();
if($ID > 0 && !$bCopy) {
	$p = CIblockElement::GetByID($ID);
	$pr = $p->ExtractFields('prn_');
} else {
	$pr = array();
}
$tabControl->AddCheckBoxField('ACTIVE', GetMessage('IBLOCK_FIELD_ACTIVE').':', false, array('Y', 'N'), $str_ACTIVE == 'Y');

/*
$tabControl->BeginCustomField('ACTIVE_FROM', GetMessage('IBLOCK_FIELD_ACTIVE_PERIOD_FROM'), $arIBlock['FIELDS']['ACTIVE_FROM']['IS_REQUIRED'] === 'Y');
?><tr id="tr_ACTIVE_FROM">
	<td><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td><?=CAdminCalendar::CalendarDate('ACTIVE_FROM', $str_ACTIVE_FROM, 19, true)?></td>
</tr><?
$tabControl->EndCustomField('ACTIVE_FROM', '<input type="hidden" id="ACTIVE_FROM" name="ACTIVE_FROM" value="'.$str_ACTIVE_FROM.'" />');

$tabControl->BeginCustomField('ACTIVE_TO', GetMessage('IBLOCK_FIELD_ACTIVE_PERIOD_TO'), $arIBlock['FIELDS']['ACTIVE_TO']['IS_REQUIRED'] === 'Y');
?><tr id="tr_ACTIVE_TO">
	<td><?=$tabControl->GetCustomLabelHTML()?>:</td>
	<td><?=CAdminCalendar::CalendarDate('ACTIVE_TO', $str_ACTIVE_TO, 19, true)?></td>
</tr><?
$tabControl->EndCustomField("ACTIVE_TO", '<input type="hidden" id="ACTIVE_TO" name="ACTIVE_TO" value="'.$str_ACTIVE_TO.'">');
*/

$tabControl->BeginCustomField('NAME', GetMessage('IBLOCK_FIELD_NAME').':', true);
?><tr id="tr_NAME">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<?=$str_NAME?>" /><?
		if($arTranslit['TRANSLITERATION'] == 'Y') {
			?><image id="name_link" title="<?=GetMessage('IBEL_E_LINK_TIP')?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" /><?
		}
	?></td>
</tr><?
$tabControl->EndCustomField('NAME', '<input type="hidden" name="NAME" id="NAME" value="'.$str_NAME.'" />');

/*
$tabControl->BeginCustomField('CODE', GetMessage('IBLOCK_FIELD_CODE').':', $arIBlock['FIELDS']['CODE']['IS_REQUIRED'] === 'Y');
?><tr id="tr_CODE">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<?=$str_CODE?>" /><?
		if($arTranslit['TRANSLITERATION'] == 'Y') {
			?><image id="code_link" title="<?=GetMessage('IBEL_E_LINK_TIP')?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" /><?
		}
	?></td>
</tr><?
$tabControl->EndCustomField('CODE', '<input type="hidden" name="CODE" id="CODE" value="'.$str_CODE.'" />');


if(COption::GetOptionString('iblock', 'show_xml_id', 'N') == 'Y') {
	$tabControl->AddEditField('XML_ID', GetMessage('IBLOCK_FIELD_XML_ID').':', $arIBlock['FIELDS']['XML_ID']['IS_REQUIRED'] === 'Y', array('size' => 20, 'maxlength' => 255), $str_XML_ID);
}
*/

$tabControl->BeginCustomField('SORT', GetMessage('IBLOCK_FIELD_SORT').':', $arIBlock['FIELDS']['SORT']['IS_REQUIRED'] === 'Y');
?><tr id="tr_CODE">
	<td><?=$tabControl->GetCustomLabelHTML()?></td>
	<td style="white-space: nowrap;" class="controlled-field-cell">
		<input type="text" size="7" name="SORT" id="SORT" maxlength="10" value="<?=$str_SORT?>" />
	</td>
</tr><?
$tabControl->EndCustomField('SORT', '<input type="hidden" name="SORT" id="SORT" value="'.$str_SORT.'" />');





// R_REGION
$prop_code = 'R_REGION';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-region-select<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// B_MKAD
$prop_code = 'B_MKAD';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-mkad-row<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			?><div class="niyama-field-required-order-type"><?
				echo $tabControl->GetCustomLabelHTML().':';
			?></div><?
		?></td>
		<td width="60%" class="region-controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// S_TIME_PERIOD
$prop_code = 'S_TIME_PERIOD';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// D_MIN_ORDER_PRICE
$prop_code = 'D_MIN_ORDER_PRICE';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}

// D_DELIVERY_PRICE
$prop_code = 'D_DELIVERY_PRICE';
if($prop_fields = $PROP[$prop_code]) {
	$prop_values = $prop_fields['VALUE'];
	$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
	$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
	?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-register-type<?=$sTmpClass?>">
		<td class="adm-detail-valign-top" width="40%"><?
			if(strlen($prop_fields['HINT'])) {
				?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
			}
			echo $tabControl->GetCustomLabelHTML().':';
		?></td>
		<td width="60%" class="controlled-prop-cell"><?
			_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
		?></td>
	</tr><?
	$hidden = '';
	if(!is_array($prop_fields['~VALUE'])) {
		$values = array();
	} else {
		$values = $prop_fields['~VALUE'];
	}
	$start = 1;
	foreach($values as $key => $val) {
		if($bCopy) {
			$key = 'n'.$start;
			$start++;
		}
		if(is_array($val) && array_key_exists('VALUE', $val)) {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
		} else {
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
			$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
		}
	}
	$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	unset($PROP[$prop_code]);
}


// остальные свойства
if($PROP) {
	foreach($PROP as $prop_code => $prop_fields) {
		$prop_values = $prop_fields['VALUE'];
		$tabControl->BeginCustomField('PROPERTY_'.$prop_fields['ID'], $prop_fields['NAME'], $prop_fields['IS_REQUIRED'] === 'Y');
		$sTmpClass = $prop_fields['PROPERTY_TYPE'] == 'F' ? ' adm-detail-file-row' : '';
		?><tr id="tr_PROPERTY_<?=$prop_fields['ID']?>" class="niyama-other-props<?=$sTmpClass?>">
			<td class="adm-detail-valign-top" width="40%"><?
				if(strlen($prop_fields['HINT'])) {
					?><span id="hint_<?=$prop_fields['ID']?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?=$prop_fields['ID']?>'), '<?=CUtil::JSEscape($prop_fields['HINT'])?>');</script>&nbsp;<?
				}
				echo $tabControl->GetCustomLabelHTML().':';
			?></td>
			<td width="60%"><?
				_ShowPropertyField('PROP['.$prop_fields['ID'].']', $prop_fields, $prop_fields['VALUE'], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
			?></td>
		</tr><?
		$hidden = '';
		if(!is_array($prop_fields['~VALUE'])) {
			$values = array();
		} else {
			$values = $prop_fields['~VALUE'];
		}
		$start = 1;
		foreach($values as $key => $val) {
			if($bCopy) {
				$key = 'n'.$start;
				$start++;
			}
			if(is_array($val) && array_key_exists('VALUE', $val)) {
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val['VALUE']);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', $val['DESCRIPTION']);
			} else {
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][VALUE]', $val);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields['ID'].']['.$key.'][DESCRIPTION]', '');
			}
		}
		$tabControl->EndCustomField('PROPERTY_'.$prop_fields['ID'], $hidden);
	}	
}

$bDisabled = ($view == 'Y') || ($bWorkflow && $prn_LOCK_STATUS == 'red') || ((($ID <= 0) || $bCopy) && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, 'section_element_bind')) || ((($ID > 0) && !$bCopy) && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, 'element_edit')) || ($bBizproc && !$canWrite);
if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1) {
	$sTmpDisabled = $bDisabled ? ' disabled="disabled"' : '';
	ob_start();
	?><input<?=$sTmpDisabled?> type="submit" class="adm-btn-save" name="save" id="save" value="<?=GetMessage('IBLOCK_EL_SAVE')?>" /><? 
	if(!$bAutocomplete) {
		?><input<?=$sTmpDisabled?> type="submit" class="button" name="apply" id="apply" value="<?=GetMessage('IBLOCK_APPLY')?>" /><?
	}
	?><input<?=$sTmpDisabled?> type="submit" class="button" name="dontsave" id="dontsave" value="<?=GetMessage('IBLOCK_EL_CANC')?>" /><?
	if(!$bAutocomplete) {
		?><input<?=$sTmpDisabled?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?=GetMessage('IBLOCK_EL_SAVE_AND_ADD')?>" /><?
	}
	$buttons_add_html = ob_get_clean();
	$tabControl->Buttons(false, $buttons_add_html);
} elseif(!$bPropertyAjax && $nobuttons !== 'Y') {

	$wfClose = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_CANC'))."',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$save_and_add = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_SAVE_AND_ADD'))."',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$cancel = "{
		title: '".CUtil::JSEscape(GetMessage('IBLOCK_EL_CANC'))."',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
	$tabControl->ButtonsPublic(
		array(
			'.btnSave',
			($ID > 0 && $bWorkflow ? $wfClose : $cancel),
			$save_and_add,
		)
	);
}

$tabControl->Show();
if((!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1) && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, 'iblock_edit') && !$bAutocomplete) {
	echo
		BeginNote(),
		GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
		' <a href="/bitrix/admin/iblock_edit.php?type='.htmlspecialcharsbx($type).'&amp;lang='.LANGUAGE_ID.'&amp;ID='.$IBLOCK_ID.'&amp;admin=Y&amp;return_url='.urlencode("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF=="Y"? "Y": null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url)>0? $return_url: null)))).'">',
		GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
		'</a>',
		EndNote()
	;
}

<?
CJSCore::Init(array('jquery'));
//$APPLICATION->AddHeadScript('/local/templates/.default/static/dest/js/main.js');
//////////////////////////
//START of the custom form
//////////////////////////

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings

$tabControl->BeginPrologContent();
echo CAdminCalendar::ShowScript();

if(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman)
{
	//TODO:This dirty hack will be replaced by special method like calendar do
	echo '<div style="display:none">';
	CFileMan::AddHTMLEditorFrame(
		"SOME_TEXT",
		"",
		"SOME_TEXT_TYPE",
		"text",
		array(
			'height' => 450,
			'width' => '100%'
		),
		"N",
		0,
		"",
		"",
		$arIBlock["LID"]
	);
	echo '</div>';
}

if($arTranslit["TRANSLITERATION"] == "Y")
{
	CJSCore::Init(array('translit'));
	?>
	<script type="text/javascript">
		var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
		function set_linked()
		{
			linked=!linked;

			var name_link = document.getElementById('name_link');
			if(name_link)
			{
				if(linked)
					name_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					name_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var code_link = document.getElementById('code_link');
			if(code_link)
			{
				if(linked)
					code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
				else
					code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
			}
			var linked_state = document.getElementById('linked_state');
			if(linked_state)
			{
				if(linked)
					linked_state.value='Y';
				else
					linked_state.value='N';
			}
		}
		var oldValue = '';
		function transliterate()
		{
			if(linked)
			{
				var from = document.getElementById('NAME');
				var to = document.getElementById('CODE');
				if(from && to && oldValue != from.value)
				{
					BX.translit(from.value, {
						'max_len' : <?echo intval($arTranslit['TRANS_LEN'])?>,
						'change_case' : '<?echo $arTranslit['TRANS_CASE']?>',
						'replace_space' : '<?echo $arTranslit['TRANS_SPACE']?>',
						'replace_other' : '<?echo $arTranslit['TRANS_OTHER']?>',
						'delete_repeat_replace' : <?echo $arTranslit['TRANS_EAT'] == 'Y'? 'true': 'false'?>,
						'use_google' : <?echo $arTranslit['USE_GOOGLE'] == 'Y'? 'true': 'false'?>,
						'callback' : function(result){to.value = result; setTimeout('transliterate()', 250); }
					});
					oldValue = from.value;
				}
				else
				{
					setTimeout('transliterate()', 250);
				}
			}
			else
			{
				setTimeout('transliterate()', 250);
			}
		}
		transliterate();
	</script>
<?
}
?>
	<script type="text/javascript">
		var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
			'<?echo $tabControl->GetName()?>_form',
			'/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?echo intval($IBLOCK_ID)?>&ENTITY_ID=<?echo intval($ID)?>'
		);
		BX.ready(function(){
			setTimeout(function(){
				InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
			}, 1000);
		});
	</script>
<?
$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();
?>
<?=bitrix_sessid_post()?>
<?echo GetFilterHiddens("find_");?>
	<input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>">
	<input type="hidden" name="Update" value="Y">
	<input type="hidden" name="from" value="<?echo htmlspecialcharsbx($from)?>">
	<input type="hidden" name="WF" value="<?echo htmlspecialcharsbx($WF)?>">
	<input type="hidden" name="return_url" value="<?echo htmlspecialcharsbx($return_url)?>">
<?if($ID>0 && !$bCopy):?>
	<input type="hidden" name="ID" value="<?echo $ID?>">
<?endif;?>
<?if ($bCopy)
{
	?><input type="hidden" name="copyID" value="<? echo intval($ID); ?>"><?
}
if ($bCatalog)
	CCatalogAdminTools::showFormParams();
?>
	<input type="hidden" name="IBLOCK_SECTION_ID" value="<?echo intval($IBLOCK_SECTION_ID)?>">
	<input type="hidden" name="TMP_ID" value="<?echo intval($TMP_ID)?>">
<?
$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);
$tabControl->AddTabs($customTabber);

$arEditLinkParams = array(
	"find_section_section" => intval($find_section_section)
);
if ($bAutocomplete)
{
	$arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(array(
	"FORM_ACTION" => "/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
));

$tabControl->BeginNextFormTab();
if($ID > 0 && !$bCopy)
{
	$p = CIblockElement::GetByID($ID);
	$pr = $p->ExtractFields("prn_");
}
else
{
	$pr = array();
}
$tabControl->AddCheckBoxField("ACTIVE", GetMessage("IBLOCK_FIELD_ACTIVE").":", false, "Y", $str_ACTIVE=="Y");
/*$tabControl->BeginCustomField("ACTIVE", GetMessage("IBLOCK_FIELD_ACTIVE"));
?>
	<tr id="tr_ACTIVE">
		<td>Статус:</td>
		<td><?=($str_ACTIVE=="Y")?'Активно':'Черновик'?></td>
	</tr>
<?
$tabControl->EndCustomField("ACTIVE");*/

/*$tabControl->BeginCustomField("ACTIVE_FROM", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_FROM"), $arIBlock["FIELDS"]["ACTIVE_FROM"]["IS_REQUIRED"] === "Y");
?>
	<tr id="tr_ACTIVE_FROM">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td><?echo CAdminCalendar::CalendarDate("ACTIVE_FROM", $str_ACTIVE_FROM, 19, true)?></td>
	</tr>
<?
$tabControl->EndCustomField("ACTIVE_FROM", '<input type="hidden" id="ACTIVE_FROM" name="ACTIVE_FROM" value="'.$str_ACTIVE_FROM.'">');
$tabControl->BeginCustomField("ACTIVE_TO", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_TO"), $arIBlock["FIELDS"]["ACTIVE_TO"]["IS_REQUIRED"] === "Y");
?>
	<tr id="tr_ACTIVE_TO">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td><?echo CAdminCalendar::CalendarDate("ACTIVE_TO", $str_ACTIVE_TO, 19, true)?></td>
	</tr>

<?
$tabControl->EndCustomField("ACTIVE_TO", '<input type="hidden" id="ACTIVE_TO" name="ACTIVE_TO" value="'.$str_ACTIVE_TO.'">');
*/
if($arTranslit["TRANSLITERATION"] == "Y")
{
	$tabControl->BeginCustomField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true);
	?>
	<tr id="tr_NAME">
		<td><?echo $tabControl->GetCustomLabelHTML()?></td>
		<td style="white-space: nowrap;">
			<input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<?echo $str_NAME?>"><image id="name_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("NAME",
		'<input type="hidden" name="NAME" id="NAME" value="'.$str_NAME.'">'
	);

	/*$tabControl->BeginCustomField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y");
	?>
		<tr id="tr_CODE">
			<td><?echo $tabControl->GetCustomLabelHTML()?></td>
			<td style="white-space: nowrap;">
				<input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<?echo $str_CODE?>"><image id="code_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
			</td>
		</tr>
	<?
	$tabControl->EndCustomField("CODE",
		'<input type="hidden" name="CODE" id="CODE" value="'.$str_CODE.'">'
	);*/
}
else
{
	$tabControl->AddEditField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true, array("size" => 50, "maxlength" => 255), $str_NAME);
	//$tabControl->AddEditField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_CODE);
}


//IS_REQUIRED
//$tabControl->BeginCustomField('PROPERTY_'.$PROP["STATUS"]["ID"], "Статус медали".":", ($PROP["STATUS"]["IS_REQUIRED"] == "Y")? true : false );
//?><!--<tr id="tr_PROPERTY_--><?//= $PROP["STATUS"]["ID"] ?><!--">-->
<!--	<td>--><?//echo $tabControl->GetCustomLabelHTML()?><!--</td>-->
<!--	<td style="white-space: nowrap;">-->
<!--		--><?// echo _ShowPropertyField('PROP['.$PROP["STATUS"]["ID"].'][VALUE]', $PROP["STATUS"], $PROP["STATUS"]["VALUE"]); ?>
<!--	</td>-->
<!--</tr>--><?//
//$tabControl->EndCustomField('PROP['.$PROP["STATUS"]["ID"].']',
//	'<input type="hidden" name="PROP['.$PROP["STATUS"]["ID"].'][VALUE]" id="PROP['.$PROP["STATUS"]["ID"].'][VALUE]" value="'.$PROP["STATUS"]["~VALUE"]['n0']['VALUE'].'">'
//);
/*if(COption::GetOptionString("iblock", "show_xml_id", "N")=="Y")
	$tabControl->AddEditField("XML_ID", GetMessage("IBLOCK_FIELD_XML_ID").":", $arIBlock["FIELDS"]["XML_ID"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_XML_ID);
*/
//$tabControl->AddEditField("SORT", GetMessage("IBLOCK_FIELD_SORT").":", $arIBlock["FIELDS"]["SORT"]["IS_REQUIRED"] === "Y", array("size" => 7, "maxlength" => 10), $str_SORT);

$tabControl->BeginCustomField("PREVIEW_TEXT", GetMessage("IBLOCK_FIELD_PREVIEW_TEXT"), $arIBlock["FIELDS"]["PREVIEW_TEXT"]["IS_REQUIRED"] === "Y");
?>
	<tr class="heading" id="tr_PREVIEW_TEXT_LABEL">
		<td colspan="2">Краткое описание</td>
	</tr>
<?if($ID && $PREV_ID && $bWorkflow):?>
	<tr id="tr_PREVIEW_TEXT_DIFF">
		<td colspan="2">
			<div style="width:95%;background-color:white;border:1px solid black;padding:5px">
				<?echo getDiff($prev_arElement["PREVIEW_TEXT"], $arElement["PREVIEW_TEXT"])?>
			</div>
		</td>
	</tr>
<?elseif(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman):?>
	<tr id="tr_PREVIEW_TEXT_EDITOR">
		<td colspan="2" align="center">
			<?CFileMan::AddHTMLEditorFrame(
				"PREVIEW_TEXT",
				$str_PREVIEW_TEXT,
				"PREVIEW_TEXT_TYPE",
				$str_PREVIEW_TEXT_TYPE,
				array(
					'height' => 150,
					'width' => '100%'
				),
				"N",
				0,
				"",
				"",
				$arIBlock["LID"],
				true,
				false,
				array(
					'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
					'saveEditorKey' => $IBLOCK_ID
				)
			);?>
		</td>
	</tr>
<?else:?>
	<tr id="tr_PREVIEW_TEXT_TYPE">
		<td><?echo GetMessage("IBLOCK_DESC_TYPE")?></td>
		<td><input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_text" value="text"<?if($str_PREVIEW_TEXT_TYPE!="html")echo " checked"?>> <label for="PREVIEW_TEXT_TYPE_text"><?echo GetMessage("IBLOCK_DESC_TYPE_TEXT")?></label> / <input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_html" value="html"<?if($str_PREVIEW_TEXT_TYPE=="html")echo " checked"?>> <label for="PREVIEW_TEXT_TYPE_html"><?echo GetMessage("IBLOCK_DESC_TYPE_HTML")?></label></td>
	</tr>
	<tr id="tr_PREVIEW_TEXT">
		<td colspan="2" align="center">
			<textarea cols="60" rows="10" name="PREVIEW_TEXT" style="width:100%"><?echo $str_PREVIEW_TEXT?></textarea>
		</td>
	</tr>
<? endif;
$tabControl->EndCustomField("PREVIEW_TEXT",
	'<input type="hidden" name="PREVIEW_TEXT" value="'.$str_PREVIEW_TEXT.'">'.
	'<input type="hidden" name="PREVIEW_TEXT_TYPE" value="'.$str_PREVIEW_TEXT_TYPE.'">'
);


$tabControl->BeginCustomField("DETAIL_TEXT", GetMessage("IBLOCK_FIELD_DETAIL_TEXT"), $arIBlock["FIELDS"]["DETAIL_TEXT"]["IS_REQUIRED"] === "Y");
?>
	<tr class="heading" id="tr_DETAIL_TEXT_LABEL">
		<td colspan="2">Описание для поздравления</td>
	</tr>
	<?if($ID && $PREV_ID && $bWorkflow):?>
	<tr id="tr_DETAIL_TEXT_DIFF">
		<td colspan="2">
			<div style="width:95%;background-color:white;border:1px solid black;padding:5px">
				<?echo getDiff($prev_arElement["DETAIL_TEXT"], $arElement["DETAIL_TEXT"])?>
			</div>
		</td>
	</tr>
	<?elseif(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman):?>
	<tr id="tr_DETAIL_TEXT_EDITOR">
		<td colspan="2" align="center">
			<?CFileMan::AddHTMLEditorFrame(
				"DETAIL_TEXT",
				$str_DETAIL_TEXT,
				"DETAIL_TEXT_TYPE",
				$str_DETAIL_TEXT_TYPE,
				array(
					'height' => 450,
					'width' => '100%'
				),
				"N",
				0,
				"",
				"",
				$arIBlock["LID"],
				true,
				false,
				array('toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')), 'saveEditorKey' => $IBLOCK_ID)
			);
		?></td>
	</tr>
	<?else:?>
	<tr id="tr_DETAIL_TEXT_TYPE">
		<td><?echo GetMessage("IBLOCK_DESC_TYPE")?></td>
		<td><input type="radio" name="DETAIL_TEXT_TYPE" id="DETAIL_TEXT_TYPE_text" value="text"<?if($str_DETAIL_TEXT_TYPE!="html")echo " checked"?>> <label for="DETAIL_TEXT_TYPE_text"><?echo GetMessage("IBLOCK_DESC_TYPE_TEXT")?></label> / <input type="radio" name="DETAIL_TEXT_TYPE" id="DETAIL_TEXT_TYPE_html" value="html"<?if($str_DETAIL_TEXT_TYPE=="html")echo " checked"?>> <label for="DETAIL_TEXT_TYPE_html"><?echo GetMessage("IBLOCK_DESC_TYPE_HTML")?></label></td>
	</tr>
	<tr id="tr_DETAIL_TEXT">
		<td colspan="2" align="center">
			<textarea cols="60" rows="10" name="DETAIL_TEXT" style="width:100%"><?echo $str_DETAIL_TEXT?></textarea>
		</td>
	</tr>
	<?endif?>
<?
$tabControl->EndCustomField("DETAIL_TEXT",
	'<input type="hidden" name="DETAIL_TEXT" value="'.$str_DETAIL_TEXT.'">'.
	'<input type="hidden" name="DETAIL_TEXT_TYPE" value="'.$str_DETAIL_TEXT_TYPE.'">'
);

if(!empty($PROP)):
	if ($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT"))
	{
		$arPropLinks = array("IBLOCK_ELEMENT_PROP_VALUE");
		if(is_array($str_IBLOCK_ELEMENT_SECTION) && !empty($str_IBLOCK_ELEMENT_SECTION))
		{
			foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
			{
				foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, $section_id) as $PID => $arLink)
					$arPropLinks[$PID] = "PROPERTY_".$PID;
			}
		}
		else
		{
			foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, 0) as $PID => $arLink)
				$arPropLinks[$PID] = "PROPERTY_".$PID;
		}
		$tabControl->AddFieldGroup("IBLOCK_ELEMENT_PROPERTY", GetMessage("IBLOCK_ELEMENT_PROP_VALUE"), $arPropLinks, $bPropertyAjax);
	}

	$BASE_TYPE_ACTION_arr=array();
	$BASE_TYPE_ACTION_val=false;
	if (!empty($PROP['BASE_TYPE_ACTION'])){
		$BASE_TYPE_ACTION_arr=get_array_by_property_list($PROP['BASE_TYPE_ACTION']);
		$BASE_TYPE_ACTION_val=get_array_selvalue_by_property_list($PROP['BASE_TYPE_ACTION']);
	}

	foreach($PROP as $prop_code=>$prop_fields):
		$onchange_arr=false;
		$onchange_val=false;
		$type_prop_detail='';
		$prop_custom_fields=false;

		$type_prop_i=stripos($prop_code, '_');
		if ($type_prop_i!==false){
			$type_prop=substr($prop_code, 0, $type_prop_i);
			switch($prop_code) {

				case 'BASE_STATUS_TYPE':
					$tabControl->AddSection("BASE_STATUS_HEADER", 'Статус медали');
					break;

				case 'BASE_TYPE_IMG':
					$tabControl->AddSection("BASE_IMG_HEADER", 'Изображение');
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					break;
				case 'BASE_TYPE_IMG_DOWNLOAD':
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					$type_prop_detail='BASE_TYPE_IMG_DOWNLOAD';

					break;
				case 'BASE_TYPE_IMG_DOWNLOAD_V':
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					$type_prop_detail='BASE_TYPE_IMG_DOWNLOAD';

					break;
				case 'BASE_TYPE_ACTION':
					$tabControl->AddSection("BASE_TYPE_ACTION_HEADER", 'Основные параметры');
					break;
				case 'BASE_PERIOD_ACTION':
					$tabControl->AddSection("BASE_PERIOD_ACTION_HEADER", 'Период действия');
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					break;
				case 'BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY':
					$type_prop_detail='BASE_PERIOD_ACTION_BIRTHDAY';
					break;
				case 'BASE_PERIOD_ACTION_CUSTOM_S':
					$type_prop_detail='BASE_PERIOD_ACTION_CUSTOM';
					break;
				case 'BASE_PERIOD_ACTION_CUSTOM_DO':
					$type_prop_detail='BASE_PERIOD_ACTION_CUSTOM';
					break;
				/*case 'ORDER_TIME_TYPE':
					
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					break;*/
				case 'BASE_TIME_ACTION_S':
					$tabControl->AddSection("BASE_PERIOD_TIME_HEADER", 'Время осуществления действия');
					$type_prop_detail='BASE_TIME_ACTION_S';
					break;
				case 'BASE_TIME_ACTION_DO':
					$type_prop_detail='BASE_TIME_ACTION_DO';
					break;
				case 'BASE_BONUS_TYPE':
					$tabControl->AddSection("BASE_BONUS_HEADER", 'Бонусы');
					$onchange_arr=get_array_by_property_list($prop_fields);
					$onchange_val=get_array_selvalue_by_property_list($prop_fields);
					break;
				case 'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE':
					$type_prop_detail='BASE_BONUS_TYPE_DISCOUNT_MONEY';
					break;
				case 'BASE_BONUS_TYPE_BLUDO_VISIUAL':
					$type_prop_detail='BASE_BONUS_TYPE';
					break;
				case 'ORDER_TYPE_DELIVERY':
					//$tabControl->AddSection("ORDER ORDER_TYPE_HEADER", 'Настройка параметров типа действия "Заказ"');
					$tabControl->BeginCustomField('ORDER_TYPE_HEADER', 'Настройка параметров типа действия "Заказ"', false);?>
					<tr id="tr_ORDER_TYPE_HEADER" class="propery_elems heading ORDER">
						<td colspan="2">Настройка параметров типа действия "Заказ"</td>
					</tr>
					<? $tabControl->EndCustomField('ORDER_TYPE_HEADER');
					break;
				case 'BASE_NUMBER_OF_ACTIONS':
					$tabControl->AddSection("BASE_NUMBER_OF_ACTIONS_HEADER", 'Кол-во действий');
					break;
				case 'ORDER_COUNT_BLUDS_YSLOVIE':
					//$tabControl->AddSection("ORDER ORDER_COUNT_BLUDS_HEADER", 'Количество блюд для типа действия "Заказ"');
					$tabControl->BeginCustomField('ORDER_COUNT_BLUDS_HEADER', 'Сумма заказа для типа действия "Заказ"', false);?>
					<tr id="tr_ORDER_COUNT_BLUDS_HEADER" class="propery_elems heading ORDER">
						<td colspan="2">Количество блюд для типа действия "Заказ"</td>
					</tr>
					<? $tabControl->EndCustomField('ORDER_COUNT_BLUDS_HEADER');
					break;
				case 'ORDER_SUMM_ZACAZ_YSLOVIE':
					//$tabControl->AddSection("ORDER ORDER_SUMM_ZACAZ_HEADER", 'Сумма заказа для типа действия "Заказ"');
					$tabControl->BeginCustomField('ORDER_SUMM_ZACAZ_HEADER', 'Сумма заказа для типа действия "Заказ"', false);
					?><tr id="tr_ORDER_TYPE_HEADER" class="propery_elems heading ORDER">
						<td colspan="2">Настройка параметров типа действия "Заказ"</td>
					</tr><?
					$tabControl->EndCustomField('ORDER_SUMM_ZACAZ_HEADER');
				break;

				case 'BASE_BONUS_TYPE_BLUDO_VISIUAL':
					$type_prop_detail='BASE_BONUS_TYPE';
				break;

				case 'ORDER_TYPE_BLUDS':
					$tabControl->BeginCustomField('ORDER_TYPE_BLUDS_HEADER', 'Сведения о блюдах', false);
					?><tr id="tr_ORDER_TYPE_BLUDS_HEADER" class="propery_elems heading ORDER">
						<td colspan="2">Сведения о блюдах</td>
					</tr><?
					$tabControl->EndCustomField('ORDER_TYPE_BLUDS_HEADER');
	
					$prop_custom_fields = $prop_fields;
					$prop_custom_fields['PROPERTY_TYPE'] = 'L';
					$prop_custom_fields['VALUES_NEW'] = array();
					foreach(CNiyamaCatalog::getMenu() as $el) {
						$prop_custom_fields['VALUES_NEW'][] = array(
							'ID' => $el['ID'],
							'VALUE' => $el['NAME'],
							'PROPERTY_ID' => $prop_custom_fields['ID'],
						);
					}
				break;

				case 'ORDER_INGRIDIENTS_BLUDS':
					$prop_custom_fields=$prop_fields;
					$prop_custom_fields['PROPERTY_TYPE']='L';
					$prop_custom_fields['VALUES_NEW']=array();
					foreach(CNiyamaCatalog::getIngredients() as $el){
						$prop_custom_fields['VALUES_NEW'][]=array(
							'ID'=>$el['ID'],
							'VALUE'=>$el['NAME'],
							'PROPERTY_ID'=>$prop_custom_fields['ID'],

						);
					}
					break;
				case 'ORDER_SPEC_MENU_BLUDS':
					$prop_custom_fields=$prop_fields;
					$prop_custom_fields['PROPERTY_TYPE']='L';
					$prop_custom_fields['VALUES_NEW']=array();
					foreach(CNiyamaCatalog::getSpecMenu() as $el){
						$prop_custom_fields['VALUES_NEW'][]=array(
							'ID'=>$el['ID'],
							'VALUE'=>$el['NAME'],
							'PROPERTY_ID'=>$prop_custom_fields['ID'],
						);
					}
					break;

				case 'ORDER_BLUDS':
					$prop_custom_fields=$prop_fields;
					$prop_custom_fields['PROPERTY_TYPE']='L';
					$prop_custom_fields['VALUES_NEW']=array();

					$arPFilter = array();

					# Меню

					if (!empty($PROP['ORDER_TYPE_BLUDS']['VALUE']) && (is_array($PROP['ORDER_TYPE_BLUDS']['VALUE']))){

						$arPFilter['SECTION_ID'] = $PROP['ORDER_TYPE_BLUDS']['VALUE'];
						$arPFilter['INCLUDE_SUBSECTIONS']  = "Y";

					}
					# Специальное меню
					if (!empty($PROP['ORDER_SPEC_MENU_BLUDS']['VALUE']) && (is_array($PROP['ORDER_SPEC_MENU_BLUDS']['VALUE']))){

						$arPFilter['PROPERTY_SPEC_MENU.ID'] = $PROP['ORDER_SPEC_MENU_BLUDS']['VALUE'];

					}
					CModule::IncludeModule('iblock');

					# Ингредиенты
					if (!empty($PROP['ORDER_INGRIDIENTS_BLUDS']['VALUE']) && (is_array($PROP['ORDER_INGRIDIENTS_BLUDS']['VALUE']))) {
						//		foreach ($_REQUEST['ing'] as $row) {
						//			$arrayTmp = null;
						//			$arrayTmp['LOGIC'] = "OR";
						//			$arrayTmp[] = array(
						//				"PROPERTY_GENERAL_INGREDIENT.ID" => $row,
						//			);
						//			$arrayTmp[] = array(
						//				"PROPERTY_MORE_INGREDIENT.ID" => $row,
						//			);
						//			$arrayTmpLogic[] = $arrayTmp;
						//		}
						//		$arPFilter[] = array("LOGIC" => "AND", $arrayTmpLogic);

						$arPFilter[] = array(
							"LOGIC" => "OR",
							array("?PROPERTY_GENERAL_INGREDIENT" => implode(" && ",$PROP['ORDER_INGRIDIENTS_BLUDS']['VALUE'])),
							array("?PROPERTY_MORE_INGREDIENT"	=> implode(" && ",$PROP['ORDER_INGRIDIENTS_BLUDS']['VALUE']))
						);

					}


					$arMenuItems = CNiyamaCatalog::GetList(null,$arPFilter,null,false);

					foreach($arMenuItems['ITEMS'] as $el){

						$prop_custom_fields['VALUES_NEW'][]=array(
							'ID'=>$el['INFO']['ID'],
							'VALUE'=> htmlspecialcharsback($el['INFO']['NAME']),
							'PROPERTY_ID'=>$prop_custom_fields['ID'],

						);

					}
					break;

			}
		}
	 
		
		$prop_values = $prop_fields["VALUE"];
		$tabControl->BeginCustomField("PROPERTY_".$prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"]==="Y");



		?>
		<tr  id="tr_PROPERTY_<?echo $prop_fields["ID"];?>" class="propery_elems <?=$type_prop?> <?=$type_prop_detail?> <?=$prop_code?><?if ($prop_fields["PROPERTY_TYPE"]=="F"):?> adm-detail-file-row<?endif?>">
			<td class="adm-detail-valign-top" width="40%"><?if($prop_fields["HINT"]!=""):
					?><span id="hint_<?echo $prop_fields["ID"];?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
				endif;?><?echo $tabControl->GetCustomLabelHTML();?>:</td>
			<td width="60%"><?
				if ($prop_custom_fields!==false){
					ADV_ShowListPropertyField('PROP['.$prop_custom_fields["ID"].']', $prop_custom_fields, $prop_custom_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID<=0)), false,$prop_custom_fields['VALUES_NEW']);
				}else{
					_ShowPropertyField('PROP['.$prop_fields["ID"].']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID<=0)), $bVarsFromForm, 50000, $tabControl->GetFormName(), $bCopy);
				}?>
			</td>
		</tr>

		<?
		$hidden = "";
		if(!is_array($prop_fields["~VALUE"]))
			$values = Array();
		else
			$values = $prop_fields["~VALUE"];
		$start = 1;
		foreach($values as $key=>$val)
		{
			if($bCopy)
			{
				$key = "n".$start;
				$start++;
			}

			if(is_array($val) && array_key_exists("VALUE",$val))
			{
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val["VALUE"]);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', $val["DESCRIPTION"]);
			}
			else
			{
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val);
				$hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', "");
			}
		}
		$tabControl->EndCustomField("PROPERTY_".$prop_fields["ID"], $hidden);

		?>
		<script type="text/javascript">
			$(document).ready(function(e) {
				<? if ($onchange_arr!==false){
					foreach ($onchange_arr as $el){?>
				$('.propery_elems.<?=$prop_code?>_<?=$el?>').hide();
				<? }
				if ($onchange_val!==false){?>
				$('.propery_elems.<?=$prop_code?>_<?=$onchange_val?>').show();
				<? }?>
				<?=$prop_code?>_arr=<?=CUtil::PhpToJSObject($onchange_arr);?>;
				$('.<?=$prop_code?> select').change(function(e) {
					<? foreach ($onchange_arr as $el){?>
					$('.propery_elems.<?=$prop_code?>_<?=$el?>').hide();
					<? }?>

					$('.propery_elems.<?=$prop_code?>_'+<?=$prop_code?>_arr[$(this).val()]).show();
				});
				<? }?>

			});
		</script><?
	endforeach;?>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$("[name='PROP[<?=$PROP['ORDER_INGRIDIENTS_BLUDS']['ID']?>][]']").change(function(e) {
				sendAjax();
			});
			$("[name='PROP[<?=$PROP['ORDER_TYPE_BLUDS']['ID']?>][]']").change(function(e) {
				sendAjax();
			});
			$("[name='PROP[<?=$PROP['ORDER_SPEC_MENU_BLUDS']['ID']?>][]']").change(function(e) {
				sendAjax();
			});


		});
		function sendAjax(){

			$.ajax({
				type: "GET",
				url: '/local/php_interface/adv/admin/public/ajax/GetProducts.php',
				// dataType: 'json',
				data: {
					menu: $("[name='PROP[<?=$PROP['ORDER_TYPE_BLUDS']['ID']?>][]']").val(),
					specmenu: $("[name='PROP[<?=$PROP['ORDER_SPEC_MENU_BLUDS']['ID']?>][]']").val(),
					ing: $("[name='PROP[<?=$PROP['ORDER_INGRIDIENTS_BLUDS']['ID']?>][]']").val(),
					// all: all
				},
				success: function (data) {
					$("[name='PROP[<?=$PROP['ORDER_BLUDS']['ID']?>][]']").html(data)
				}
			});
		}
	</script>

	<script type="text/javascript">
		//BX.loadScript('/local/templates/.default/static/dest/js/main.js');

		$(document).ready(function(e) {

			$('.propery_elems').not('.BASE').hide();
			<? if ($BASE_TYPE_ACTION_val!==false){?>
			$('.propery_elems.<?=$BASE_TYPE_ACTION_val?>').show();
			<? }?>
			BASE_TYPE_ACTION_arr=<?=CUtil::PhpToJSObject($BASE_TYPE_ACTION_arr);?>;
			$('.BASE_TYPE_ACTION select').change(function(e) {
				$('.propery_elems').not('.BASE').hide();
				$('.propery_elems.'+BASE_TYPE_ACTION_arr[$(this).val()]).show();
			});
		});
	</script>

<?endif;

if (!$bAutocomplete)
{
	$rsLinkedProps = CIBlockProperty::GetList(array(), array(
		"PROPERTY_TYPE" => "E",
		"LINK_IBLOCK_ID" => $IBLOCK_ID,
		"ACTIVE" => "Y",
		"FILTRABLE" => "Y",
	));
	$arLinkedProp = $rsLinkedProps->GetNext();
	if($arLinkedProp)
	{
		$tabControl->BeginCustomField("LINKED_PROP", GetMessage("IBLOCK_ELEMENT_EDIT_LINKED"));
		?>
		<tr class="heading" id="tr_LINKED_PROP">
			<td colspan="2"><?echo $tabControl->GetCustomLabelHTML();?></td>
		</tr>
		<?
		do {
			$elements_name = CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "ELEMENTS_NAME");
			if(strlen($elements_name) <= 0)
				$elements_name = GetMessage("IBLOCK_ELEMENT_EDIT_ELEMENTS");
			?>
			<tr id="tr_LINKED_PROP<?echo $arLinkedProp["ID"]?>">
				<td colspan="2"><a href="<?echo htmlspecialcharsbx(CIBlock::GetAdminElementListLink($arLinkedProp["IBLOCK_ID"], array('set_filter'=>'Y', 'find_el_property_'.$arLinkedProp["ID"]=>$ID)))?>"><?echo CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "NAME").": ".$elements_name?></a></td>
			</tr>
		<?
		} while ($arLinkedProp = $rsLinkedProps->GetNext());
		$tabControl->EndCustomField("LINKED_PROP", "");
	}
}



$bDisabled =
	($view=="Y")
	|| ($bWorkflow && $prn_LOCK_STATUS=="red")
	|| (
		(($ID <= 0) || $bCopy)
		&& !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
	)
	|| (
		(($ID > 0) && !$bCopy)
		&& !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
	)
	|| (
		$bBizproc
		&& !$canWrite
	)
;

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):
	ob_start();
	?>
	<input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-save" name="save" id="save" value="<?echo GetMessage("IBLOCK_EL_SAVE")?>">
	<input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="apply" id="apply" value="<?echo GetMessage('IBLOCK_APPLY')?>">
	<input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="dontsave" id="dontsave" value="<?echo GetMessage("IBLOCK_EL_CANC")?>">
	<input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?echo GetMessage("IBLOCK_EL_SAVE_AND_ADD")?>">
	<?
	$buttons_add_html = ob_get_contents();
	ob_end_clean();
	$tabControl->Buttons(false, $buttons_add_html);
elseif(!$bPropertyAjax && $nobuttons !== "Y"):

	$wfClose = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$save_and_add = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_SAVE_AND_ADD"))."',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
	$cancel = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
	$tabControl->ButtonsPublic(array(
		'.btnSave',
		($ID > 0 && $bWorkflow? $wfClose: $cancel),
		$save_and_add,
	));
endif;

$tabControl->Show();
if (
	(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1)
	&& CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_edit")
	&& !$bAutocomplete
)
{

	echo
	BeginNote(),
	GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
		' <a href="/bitrix/admin/iblock_edit.php?type='.htmlspecialcharsbx($type).'&amp;lang='.LANGUAGE_ID.'&amp;ID='.$IBLOCK_ID.'&amp;admin=Y&amp;return_url='.urlencode("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF=="Y"? "Y": null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url)>0? $return_url: null)))).'">',
	GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
	'</a>',
	EndNote()
	;
}
//////////////////////////
//END of the custom form
//////////////////////////
function get_array_by_property_list($prop_fields){
	$res=array();
	if ($prop_fields['PROPERTY_TYPE']= 'L'){
		$prop_enums = CIBlockProperty::GetPropertyEnum($prop_fields['ID']);
		while($ar_enum = $prop_enums->Fetch()){
			//CDebug::PR($ar_enum);
			$res[$ar_enum['ID']]=$ar_enum['XML_ID'];
		}
	}
	return $res;
}
function get_array_selvalue_by_property_list($prop_fields){
	$res=array();
	$def=false;
	if ($prop_fields['PROPERTY_TYPE']= 'L'){
		$values=$prop_fields['VALUE'];
		foreach($values as $key => $value){
			if(is_array($value) && array_key_exists("VALUE", $value))
				$values[$key] = $value["VALUE"];
		}

		$prop_enums = CIBlockProperty::GetPropertyEnum($prop_fields['ID']);
		while($ar_enum = $prop_enums->Fetch()){
			if (in_array($ar_enum["ID"], $values)){
				$res[]=$ar_enum['XML_ID'];
			}
			if ($ar_enum['DEF']=='Y'){
				$def=$ar_enum['XML_ID'];
			}
		}
		if (sizeof($res)==1){
			return $res[0];
		}elseif(sizeof($res)<1){
			if ($def!==false){
				return $def;
			}else{
				return false;
			}
		}else{
			return $res;
		}
	}else{
		return false;
	}

}

function ADV_ShowListPropertyField($name, $property_fields, $values, $bInitDef = false, $def_text = false, $prop_enums_arr=array())
{
	//CDebug::PR($prop_enums_arr);
	if(!is_array($values)) $values = array();
	foreach($values as $key => $value)
	{
		if(is_array($value) && array_key_exists("VALUE", $value))
			$values[$key] = $value["VALUE"];
	}

	$id = $property_fields["ID"];
	$multiple = $property_fields["MULTIPLE"];
	if (empty($prop_enums_arr)){
		$prop_enums = CIBlockProperty::GetPropertyEnum($id);
		while($ar_enum = $prop_enums->Fetch()){
			$prop_enums_arr[]=$ar_enum;
		}
	}

	$res = "";
	if($property_fields["LIST_TYPE"]=="C") //list property as checkboxes
	{
		$cnt = 0;
		$wSel = false;
		//$prop_enums = CIBlockProperty::GetPropertyEnum($id);

		foreach ($prop_enums_arr as $ar_enum)
		{
			$cnt++;
			if($bInitDef)
				$sel = ($ar_enum["DEF"]=="Y");
			else
				$sel = in_array($ar_enum["ID"], $values);
			if($sel)
				$wSel = true;

			$uniq = md5(uniqid(rand(), true));
			if($multiple=="Y") //multiple
				$res .= '<input type="checkbox" name="'.$name.'[]" value="'.htmlspecialcharsbx($ar_enum["ID"]).'"'.($sel?" checked":"").' id="'.$uniq.'"><label for="'.$uniq.'">'.htmlspecialcharsex($ar_enum["VALUE"]).'</label><br>';
			else //if(MULTIPLE=="Y")
				$res .= '<input type="radio" name="'.$name.'[]" id="'.$uniq.'" value="'.htmlspecialcharsbx($ar_enum["ID"]).'"'.($sel?" checked":"").'><label for="'.$uniq.'">'.htmlspecialcharsex($ar_enum["VALUE"]).'</label><br>';

			if($cnt==1)
				$res_tmp = '<input type="checkbox" name="'.$name.'[]" value="'.htmlspecialcharsbx($ar_enum["ID"]).'"'.($sel?" checked":"").' id="'.$uniq.'"><br>';
		}


		$uniq = md5(uniqid(rand(), true));

		if($cnt==1)
			$res = $res_tmp;
		elseif($multiple!="Y")
			$res = '<input type="radio" name="'.$name.'[]" value=""'.(!$wSel?" checked":"").' id="'.$uniq.'"><label for="'.$uniq.'">'.htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NO") )).'</label><br>'.$res;

		if($multiple=="Y" || $cnt==1)
			$res = '<input type="hidden" name="'.$name.'" value="">'.$res;
	}
	else //list property as list
	{
		$bNoValue = true;
		//$prop_enums = CIBlockProperty::GetPropertyEnum($id);
		foreach ($prop_enums_arr as $ar_enum)
		{
			if($bInitDef)
				$sel = ($ar_enum["DEF"]=="Y");
			else
				$sel = in_array($ar_enum["ID"], $values);
			if($sel)
				$bNoValue = false;
			$res .= '<option value="'.htmlspecialcharsbx($ar_enum["ID"]).'"'.($sel?" selected":"").'>'.htmlspecialcharsex($ar_enum["VALUE"]).'</option>';
		}

		if($property_fields["MULTIPLE"]=="Y" && IntVal($property_fields["ROW_COUNT"])<2)
			$property_fields["ROW_COUNT"] = 5;
		if($property_fields["MULTIPLE"]=="Y")
			$property_fields["ROW_COUNT"]++;
		$res = '<select name="'.$name.'[]" size="'.$property_fields["ROW_COUNT"].'" '.($property_fields["MULTIPLE"]=="Y"?"multiple":"").'>'.
			'<option value=""'.($bNoValue?' selected':'').'>'.htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NA") )).'</option>'.
			$res.
			'</select>';
	}
	echo $res;
}
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Инфоблок "Импортируемое меню": отмена редактирования через админку
//

$_POST = array();
$error = new _CIBlockError(2, 'EDIT_DISABLED', 'Элементы данного инфоблока нельзя редактировать');

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Обработчики событий (статические обработчики объявлять только здесь)
 * (файл подключается в php_interface/init.php)
 */
$obEventManager = \Bitrix\Main\EventManager::GetInstance();

// базовые обработчики событий для проектов (main, iblock)
CProjectUtils::AddCommonEventHandlers();
// Highloadblock: добавление обработчиков событий для автозаполнения полей UF_TIMESTAMP_X и UF_MODIFIED_BY, сброса тегированного кэша
CHLEntity::AddCommonEventHandlers();


#
# main
#
// делаем так, чтобы ресайз изображений работал не только для файлов, находящихся в папке upload
AddEventHandler('main', 'OnBeforeResizeImage', array('CEventHandlers', 'OnBeforeResizeImageHandler'));
// кастомное отключение публички для сайтов
AddEventHandler('main', 'OnProlog', array('CEventHandlers', 'OnPrologHandler'));
// служебные операции (включение вывода ошибок на экран для админов)
AddEventHandler('main', 'OnBeforeProlog', array('CEventHandlers', 'OnBeforePrologHandler'), 1);
// перенос js в футер
AddEventHandler('main', 'OnEndBufferContent', array('CEventHandlers', 'OnEndBufferContentHandler'), 999999);
// регистрация типа в Журнале событий
AddEventHandler('main', 'OnEventLogGetAuditTypes', array('CEventHandlers', 'OnEventLogGetAuditTypesHandler'));
// UF-поле "Varchar"
AddEventHandler('main', 'OnUserTypeBuildList', array('CUFVarchar', 'GetUserTypeDescription'));
// добавление в админке кастомного меню
AddEventHandler('main', 'OnBuildGlobalMenu', array('CEventHandlers', 'OnBuildGlobalMenuHandler'));

AddEventHandler('main', 'OnBeforeUserRegister', array('CEventHandlers', 'OnBeforeUserRegisterHandler'));
AddEventHandler('main', 'OnBeforeUserAdd', array('CEventHandlers', 'OnBeforeUserAddHandler'));
AddEventHandler('main', 'OnAfterUserAdd', array('CEventHandlers', 'OnAfterUserAddHandler'));
// Устанавливаем email в качестве логина при обнавлении пользователя
AddEventHandler('main', 'OnBeforeUserUpdate',   array('CEventHandlers', 'OnBeforeUserUpdateHandler'));
AddEventHandler('main', 'OnAfterUserUpdate', array('CEventHandlers', 'OnAfterUserUpdateHandler'));
// Перед авторизацией пользователя
AddEventHandler('main', 'OnBeforeUserLogin', array('CEventHandlers', 'OnBeforeUserLoginHandler'));
AddEventHandler('main', 'OnUserLogin', array('CEventHandlers', 'OnUserLoginHandler1'), 1); // !!! сортировка важна, этот обработчик должен выполниться раньше обработчика модуля sale !!!
AddEventHandler('main', 'OnUserLogin', array('CEventHandlers', 'OnUserLoginHandler2'), 9999); // !!! сортировка важна !!!
AddEventHandler('main', 'OnAfterUserLogin', array('CEventHandlers', 'OnAfterUserLoginHandler'));
// костыли для смены пароля неактивным юзером
AddEventHandler('main', 'OnBeforeUserSendPassword', array('CEventHandlers', 'OnBeforeUserSendPasswordHandler'));
AddEventHandler('main', 'OnBeforeUserChangePassword', array('CEventHandlers', 'OnBeforeUserChangePasswordHandler'));
// удаляем связи
AddEventHandler('main', 'OnBeforeUserDelete', array('CEventHandlers', 'OnBeforeUserDeleteHandler'));

// Соцсети
AddEventHandler('main', 'OnAfterSocUserRegister', array('CEventHandlers', 'OnAfterSocUserRegisterHandler'));
AddEventHandler('main', 'OnAfterSocAuthorize', array('CEventHandlers', 'OnAfterSocAuthorizeHandler'));
AddEventHandler('main', 'OnUserSocShare', array('CEventHandlers', 'OnUserSocShareHandler'));

// добавление в админке кастомных кнопок
AddEventHandler('main', 'OnAdminContextMenuShow', array('CEventHandlers', 'OnAdminContextMenuShowHandler'));

// Медали
AddEventHandler('main', 'OnAfterSetMedal', array('CEventHandlers', 'OnAfterSetMedalHandler'));

// После получения скидки по таймлайну
AddEventHandler('main', 'OnUserDiscountLevelAdd', array('CEventHandlers', 'OnUserDiscountLevelAddHandler'));
// После  переноса корзины от анонимного покупателя к зарегистрированном
//AddEventHandler('main', 'OnTransferAnonymousOrders', array('CEventHandlers', 'OnTransferAnonymousOrdersHandler'));


#
# sale
#
AddEventHandler('sale', 'OnBasketUpdate', array('CEventHandlers', 'OnBasketUpdateHandler'));
AddEventHandler('sale', 'OnBasketAdd', array('CEventHandlers', 'OnBasketAddHandler'));
AddEventHandler('sale', 'OnBeforeBasketDelete', array('CEventHandlers', 'OnBeforeBasketDeleteHandler'));
AddEventHandler('sale', 'OnBasketDelete', array('CEventHandlers', 'OnBasketDeleteHandler'));
//AddEventHandler('sale', 'OnBeforeBasketDelete', array('CEventHandlers', 'OnBeforeBasketDeleteHandler'));

AddEventHandler('sale', 'OnOrderSave', array('CEventHandlers', 'OnOrderSaveHandler')); // Вызывается после обработки свойств заказа

AddEventHandler('sale', 'OnSaleStatusOrder', array('CEventHandlers', 'OnSaleStatusOrderHandler'));
AddEventHandler('sale', 'OnSalePayOrder', array('CEventHandlers', 'OnSalePayOrderHandler'));
AddEventHandler('sale', 'OnSaleCancelOrder', array('CEventHandlers', 'OnSaleCancelOrderHandler'));


AddEventHandler('sale', 'OnSaleCalculateOrder', array('CEventHandlers', 'OnSaleCalculateOrderHandler'));



#
# vote
#
AddEventHandler('vote', 'onAfterVoting', array('CEventHandlers', 'onAfterVotingHandler'));


#
# iblock
#
// юзертайп: Да/Нет [YesNo]
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CIBlockUserTypeNYesNo', 'GetUserTypeDescription'), 5000);
// юзертайп: Элемент Импортированного меню [import_menu_element]
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CImportMenuElement', 'GetUserTypeDescription'), 5000);
// юзертайп: Привязка к местоположениям [SaleLoc]
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CIBlockUserTypeSaleLoc', 'GetUserTypeDescription'), 5000);
// юзертайп: Похожие блюда
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CRelatedProducts', 'GetUserTypeDescription'), 5000);
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CRecProducts', 'GetUserTypeDescription'), 5000);

AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('CEventHandlers', 'OnAfterIBlockElementAddHandler'), 5000);
// каталог, уровни скидок
AddEventHandler('iblock', 'OnBeforeIBlockElementDelete', array('CEventHandlers', 'OnBeforeIBlockElementDeleteHandler'), 5000);
// уровни скидок
AddEventHandler('iblock', 'OnBeforeIBlockElementAdd', array('CEventHandlers', 'OnBeforeIBlockElementAddHandler'), 1);
AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', array('CEventHandlers', 'OnBeforeIBlockElementUpdateHandler'), 1);
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('CEventHandlers', 'OnAfterIBlockElementUpdateHandler'), 1);
// бонусные блюда
AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CBonusBluds', 'GetUserTypeDescription'), 5000);
// каталог, уровни скидок
AddEventHandler('iblock', 'OnAfterIBlockElementDelete', array('CEventHandlers', 'OnAfterIBlockElementDeleteHandler'), 5000);
// Инфблок "Ссылки для перенаправления": генерация файла редиректов
AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('CNiyamaIBlockUrlRedirect', 'OnAfterIBlockElementAddHandler'), 5000);
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('CNiyamaIBlockUrlRedirect', 'OnAfterIBlockElementUpdateHandler'), 5000);
AddEventHandler('iblock', 'OnAfterIBlockElementDelete', array('CNiyamaIBlockUrlRedirect', 'OnAfterIBlockElementDeleteHandler'), 5000);

#
# form
#
// кастомные валидаторы
AddEventHandler('form', 'onFormValidatorBuildList', array('CCustomFormValidatorEmail', 'GetDescription'));

//
// search
//
AddEventHandler('search', 'BeforeIndex', array('CEventHandlers', 'BeforeIndexHandler'));

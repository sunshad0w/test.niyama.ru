<?
/**
 *
 * Класс утилит для инфблока "Города"
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 *
 */

class CNiyamaIBlockCities {

	public static function GetIBlockId() {
		return CProjectUtils::GetIBlockIdByCode('cities', 'data');
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	//
	// Актуализация справочника Города из FastOperator
	//
	public static function ImportData() {
		$bResult = false;
		CModule::IncludeModule('iblock');
		$iIBlockId = self::GetIBlockId();
		$obFastOperator = new CNiyamaFastOperator();
		$arImportData = $obFastOperator->GetCitiesArray();
		if(!$arImportData['Items'] || !$iIBlockId) {
			return $bResult;
		}

		$bResult = true;

		$sUpdateTmpIdField = time();

		// работа с элементами
		if($arImportData['Items']) {
			// обновление элементов
			$arChunks = array_chunk($arImportData['Items'], 1000);
			foreach($arChunks as $arChunkItem) {
				$arUpdateItemsList = array();
				foreach($arChunkItem as $arTmpItem) {
					if(strlen($arTmpItem['Code'])) {
						$arUpdateItemsList[$arTmpItem['Code']] = $arTmpItem;
					}
				}

				if($arUpdateItemsList) {
					$dbItems = CIBlockElement::GetList(
						array(
							'ID' => 'ASC'
						),
						array(
							'IBLOCK_ID' => $iIBlockId,
							'=XML_ID' => array_keys($arUpdateItemsList),
							'CHECK_PERMISSIONS' => 'N',
						),
						false,
						false,
						array(
							'ID', 'XML_ID'
						)
					);

					while($arItem = $dbItems->Fetch()) {
						if($arUpdateItemsList[$arItem['XML_ID']]) {
							$arTmpUpdateItem = $arUpdateItemsList[$arItem['XML_ID']];
							$arFields = array(
								'NAME' => $arTmpUpdateItem['Name'],
								'ACTIVE' => 'Y',
								'CODE' => $arTmpUpdateItem['Code'],
								// !!!
								'TMP_ID' => $sUpdateTmpIdField,
							);
							$obIBlockElement = new CIBlockElement();
							$obIBlockElement->Update($arItem['ID'], $arFields);
							// !!!
							unset($arUpdateItemsList[$arItem['XML_ID']]);
						}
					}
				}

				// добавление элементов
				if($arUpdateItemsList) {
					foreach($arUpdateItemsList as $arTmpUpdateItem) {
						$arFields = array(
							'IBLOCK_ID' => $iIBlockId,
							'NAME' => $arTmpUpdateItem['Name'],
							'ACTIVE' => 'Y',
							// !!!
							'CODE' => $arTmpUpdateItem['Code'],
							'XML_ID' => $arTmpUpdateItem['Code'],
							'TMP_ID' => $sUpdateTmpIdField,
						);
						$obIBlockElement = new CIBlockElement();
						$obIBlockElement->Add($arFields);
					}
				}
			}

			// деактивируем элементы, которых не было в выгрузке
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'!TMP_ID' => $sUpdateTmpIdField,
					'CHECK_PERMISSIONS' => 'N',
				),
				false,
				array(
					'ID'
				)
			);
			while($arTmpItem = $dbItems->Fetch()) {
				$obIBlockElement = new CIBlockElement();
				$obIBlockElement->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
			}
		}
		return $bResult;
	}
}
<?
/**
 * Класс утилит для инфблока "Уровни скидок"
 *
 * @author Sergey Leshchenko, 2014
 * @updated: 15.04.2015
 */


class CNiyamaDiscountLevels {
	public static function GetIBlockId() {
		static $iIBlockId = 0;
		if(!$iIBlockId) {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('discount_levels', 'customer_loyalty');
		}
		return $iIBlockId;
	}

	public static function GetUsersLevelsIBlockId() {
		static $iIBlockId = 0;
		if(!$iIBlockId) {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('users_discount_levels', 'customer_loyalty');
		}
		return $iIBlockId;
	}

	public static function GetIBlockPropsMeta() {
		static $arPropsMeta = array();
		$iIBlockId = self::GetIBlockId();
		if(!$arPropsMeta) {
			CModule::IncludeModule('iblock');
			$dbItems = CIBlockProperty::GetList(
				array(
					'ID' => 'ASC'
				), 
				array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$arPropsMeta[$arItem['CODE']] = $arItem;
			}
		}
		return $arPropsMeta;
	}

	public static function GetEnumValIdByXmlId($sEnumValXmlId, $sEnumPropCode) {
		$iReturn = 0;
		static $arPropsEnum = array();
		if(!$arPropsEnum[$sEnumPropCode]) {
			$iIBlockId = self::GetIBlockId();
			$arPropsEnum[$sEnumPropCode] = CCustomProject::GetEnumPropValues($iIBlockId, 'DISCOUNT_TYPE');
		}
		if($arPropsEnum[$sEnumPropCode]) {
			foreach($arPropsEnum[$sEnumPropCode] as $arTmpItem) {
				if($arTmpItem['XML_ID'] == $sEnumValXmlId) {
					$iReturn = $arTmpItem['ID'];
					break;
				}
			}
		}
		return $iReturn;
	}

	public static function OnBeforeIBlockElementAddHandler(&$arFields) {
		return self::CheckElementFields($arFields, true);
	}

	public static function OnBeforeIBlockElementUpdateHandler(&$arFields) {
		return self::CheckElementFields($arFields, false);
	}

	public static function OnBeforeIBlockElementDeleteHandler($iElementId, $arFields) {
		$bReturn = true;
		$iIBlockId = self::GetIBlockId();

		if($arFields['IBLOCK_ID'] != $iIBlockId) {
			return $bReturn;
		}

		$iUsersLevelsIBlockId = self::GetUsersLevelsIBlockId();
		if(!$iUsersLevelsIBlockId) {
			return $bReturn;
		}

		$iCnt = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => $iUsersLevelsIBlockId,
				'=PROPERTY_R_DISCOUNT_LEVEL' => $iElementId,
				'CHECK_PERMISSIONS' => 'N',
			),
			array()
		);
		if($iCnt) {
			$GLOBALS['APPLICATION']->ThrowException('Невозможно удалить элемент '.$iElementId.' - имеется связь с элементами инфоблока '.$iUsersLevelsIBlockId);
			$bReturn = false;
		}

		return $bReturn;
	}

	public static function FindRegistrationTypeItems() {
		$arReturn = array();
		CModule::IncludeModule('iblock');
		$dbItems = CIBlockElement::GetList(
			array(), 
			array(
				'IBLOCK_ID' => self::GetIBlockId(),
				'ACTIVE' => 'Y',
				'CHECK_PERMISSIONS' => 'N',
				'PROPERTY_DISCOUNT_TYPE' => self::GetEnumValIdByXmlId('registration', 'DISCOUNT_TYPE'),
			),
			false,
			false,
			array('ID')
		);
		while($arItem = $dbItems->Fetch()) {
			$arReturn[] = $arItem['ID'];
		}
		return $arReturn;
	}

	protected static function FindOrderTypeItemsBySum($dFilterSum = 0) {
		$arReturn = array();
		CModule::IncludeModule('iblock');
		$dbItems = CIBlockElement::GetList(
			array(
				'PROPERTY_DISCOUNT_VAL_PERC' => 'ASC'
			), 
			array(
				'IBLOCK_ID' => self::GetIBlockId(),
				'ACTIVE' => 'Y',
				'CHECK_PERMISSIONS' => 'N',
				'PROPERTY_DISCOUNT_TYPE' => self::GetEnumValIdByXmlId('order', 'DISCOUNT_TYPE'),
				'<=PROPERTY_LEVEL_CONDITION' => $dFilterSum
			),
			false,
			false,
			array('ID')
		);
		while($arItem = $dbItems->Fetch()) {
			$arReturn[] = $arItem['ID'];
		}
		return $arReturn;
	}

	protected static function FindOrderTypeItemsByDiscount($dDiscountVal = 0) {
		$arReturn = array();
		CModule::IncludeModule('iblock');
		$dbItems = CIBlockElement::GetList(
			array(
				'PROPERTY_DISCOUNT_VAL_PERC' => 'ASC'
			), 
			array(
				'IBLOCK_ID' => self::GetIBlockId(),
				'ACTIVE' => 'Y',
				'CHECK_PERMISSIONS' => 'N',
				'PROPERTY_DISCOUNT_TYPE' => self::GetEnumValIdByXmlId('order', 'DISCOUNT_TYPE'),
				'<=PROPERTY_DISCOUNT_VAL_PERC' => $dDiscountVal
			),
			false,
			false,
			array('ID')
		);
		while($arItem = $dbItems->Fetch()) {
			$arReturn[] = $arItem['ID'];
		}
		return $arReturn;
	}

	public static function FindMaxDiscountBySum($dFilterSum = 0) {
		$arReturn = array();
		CModule::IncludeModule('iblock');
		$dbItems = CIBlockElement::GetList(
			array(
				'PROPERTY_DISCOUNT_VAL_PERC' => 'DESC'
			),
			array(
				'IBLOCK_ID' => self::GetIBlockId(),
				'ACTIVE' => 'Y',
				'CHECK_PERMISSIONS' => 'N',
				'PROPERTY_DISCOUNT_TYPE' => self::GetEnumValIdByXmlId('order', 'DISCOUNT_TYPE'),
				'<=PROPERTY_LEVEL_CONDITION' => $dFilterSum
			),
			false,
			array(
				'nTopCount' => 1
			),
			array(
				'ID', 'PROPERTY_DISCOUNT_VAL_PERC'
			)
		);
		if($arItem = $dbItems->Fetch()) {
			$arReturn = array(
				'ID' => $arItem['ID'],
				'DISCOUNT' => $arItem['PROPERTY_DISCOUNT_VAL_PERC_VALUE'],
			);
		}
		return $arReturn;
	}

	protected static function GetPropIdByCode($sPropCode) {
		$iReturn = 0;
		$arPropsMeta = self::GetIBlockPropsMeta();
		if(!$arPropsMeta[$sPropCode]) {
			return $iReturn;
		}
		$iReturn = $arPropsMeta[$sPropCode]['ID'];
		return $iReturn;
	}

	// преобразование массива свойств, генерируемого формой редактирования элемента инфоблока, к нормальному виду
    // [PROPERTY_VALUES] => array(
	//   [id свойства 1] => array(
	//     [0] => array(
	//       [VALUE] => значение 
	//     )
	//   )
	//   [id свойства 2] => array(
	//       [0] => значение
	//   )
	//   ...
	protected static function GetPropFieldValue($arProp, $sPropCode) {
		$arReturn = array();
		$iPropId = self::GetPropIdByCode($sPropCode);
		if(!$iPropId) {
			return $arReturn;
		}
		$mDiscountTypeVal = array();
		if(isset($arProp[$iPropId])) {
			$mDiscountTypeVal = $arProp[$iPropId];
		} elseif(isset($arProp[$sPropCode])) {
			$mDiscountTypeVal = $arProp[$sPropCode];
		}
		if(is_array($mDiscountTypeVal)) {
			if(!array_key_exists('VALUE', $mDiscountTypeVal)) {
				foreach($mDiscountTypeVal as $mTmpVal) {
					if(is_array($mTmpVal)) {
						if(array_key_exists('VALUE', $mTmpVal)) {
							$arReturn[] = $mTmpVal['VALUE'];
						}
					} else {
						$arReturn[] = $mTmpVal;
					}
				}
			} elseif(array_key_exists('VALUE', $mDiscountTypeVal)) {
				$arReturn[] = $arTmpVal['VALUE'];
			} else {
				// это если значения свойств линейным массивом передали
				$arReturn = $arTmpVal;
			}
		} elseif(is_scalar($mDiscountTypeVal)) {
			$arReturn[] = $mDiscountTypeVal;
		}
		return $arReturn;
	}

	protected static function CheckElementFields(&$arFields, $bNewElement = false) {
		$arErr = array();
		$bReturn = true;
		$iIBlockId = self::GetIBlockId();

		if($arFields['IBLOCK_ID'] != $iIBlockId) {
			return $bReturn;
		}

		// редактирование разрешим только при наличии специального поля-флага: _NIYAMA_DISCOUNT_LEVEL_ELEMENT_<ID элемента>
		if(!$bNewElement) {
			$sTmpFieldKey = '_NIYAMA_DISCOUNT_LEVEL_ELEMENT_'.$arFields['ID'];
			if((!isset($arFields[$sTmpFieldKey]) || $arFields[$sTmpFieldKey] != 'Y') && (!isset($_POST[$sTmpFieldKey]) || $_POST[$sTmpFieldKey] != 'Y')) {
				$arErr['EDIT_BLOCKED'] = 'Редактирование элемента заблокировано';
			}
		}

		if(!$arErr) {
			$arPropVals = isset($arFields['PROPERTY_VALUES']) ? $arFields['PROPERTY_VALUES'] : array();
			//if(!$arPropVals && $arFields['ID']) {
			//	$dbItems = CIBlockElement::GetProperty($iIBlockId, $arFields['ID']);
			//	while($arItem = $dbItems->Fetch()) {
			//		$arPropVals[$arItem['ID']][$arItem['PROPERTY_VALUE_ID']]['VALUE'] = $arItem['VALUE'];
			//	}
			//}

			if(!$arPropVals) {
				return $bReturn;
			}

			$mDiscountTypeVal = self::GetPropFieldValue($arPropVals, 'DISCOUNT_TYPE');

			$iTmpVal = intval(reset($mDiscountTypeVal));
			if($iTmpVal > 0) {
				$iRegistrationTypeId = self::GetEnumValIdByXmlId('registration', 'DISCOUNT_TYPE');
				$iOrderTypeId = self::GetEnumValIdByXmlId('order', 'DISCOUNT_TYPE');
				switch($iTmpVal) {
					case $iRegistrationTypeId:
						// не может быть двух активных скидок за регистрацию
						$arTmp = self::FindRegistrationTypeItems();
						$bTmpErr = false;
						if($arTmp) {
							if($bNewElement) {
								$bTmpErr = true;
							} elseif(!in_array($arFields['ID'], $arTmp)) {
								$bTmpErr = true;
							}
							if($bTmpErr && isset($arFields['ACTIVE']) && $arFields['ACTIVE'] != 'Y') {
								$bTmpErr = false;
							}
						}
						if($bTmpErr) {
							$arErr['WRONG_DISCOUNT_TYPE'] = 'Может быть только одна активная скидка за регистрацию';
						} 

						// проверим, чтобы был задан хоть один бонус или скидка
						$bWasVal = false;

						$mTmpVal = self::GetPropFieldValue($arPropVals, 'DISCOUNT_VAL_PERC');
						$mTmpVal = $mTmpVal ? doubleval(reset($mTmpVal)) : 0;
						if($mTmpVal < 0) {
							$arErr['WRONG_DISCOUNT_VAL_PERC'] = 'Скидка не может быть отрицательной';
						} 
						if($mTmpVal > 0) {
							$bWasVal = true;
						}

						$mTmpVal = self::GetPropFieldValue($arPropVals, 'DISCOUNT_VAL_RUB');
						$mTmpVal = $mTmpVal ? doubleval(reset($mTmpVal)) : 0;
						if($mTmpVal < 0) {
							$arErr['WRONG_DISCOUNT_VAL_RUB'] = 'Скидка не может быть отрицательной';
						} 
						if($mTmpVal > 0) {
							$bWasVal = true;
						}

						$mTmpVal = self::GetPropFieldValue($arPropVals, 'NIYAM_CNT');
						$mTmpVal = $mTmpVal ? intval(reset($mTmpVal)) : 0;
						if($mTmpVal < 0) {
							$arErr['WRONG_NIYAM_CNT'] = 'Количество начисляемых "ниямов" не может быть отрицательным';
						} 
						if($mTmpVal > 0) {
							$bWasVal = true;
						}

						$mTmpVal = self::GetPropFieldValue($arPropVals, 'BONUS_DISH');
						$mTmpVal = $mTmpVal ? intval(reset($mTmpVal)) : 0;
						if($mTmpVal > 0) {
							$bWasVal = true;
						}

						if(!$arErr && !$bWasVal) {
							$arErr['FIELDS_REQUIRED'] = 'Необходимо задать хотя бы один бонус или скидку';
						}

						if(!$arErr) {
							// обнулим свойства, связанные с другими типами бонусов
							$arClearProps = array('LEVEL_CONDITION');
							foreach($arClearProps as $sClearPropCode) {
								if($arPropVals[$sClearPropCode]) {
									$arPropVals[$sClearPropCode] = array();
								} else {
									$iPropId = self::GetPropIdByCode($sClearPropCode);
									if($arPropVals[$iPropId]) {
										$arPropVals[$iPropId] = array();
									}
								}
							}
						}
					break;

					case $iOrderTypeId:
						// условие достижения уровня должно быть задано
						$mTmpVal = self::GetPropFieldValue($arPropVals, 'LEVEL_CONDITION');
						$mTmpVal = $mTmpVal ? doubleval(reset($mTmpVal)) : 0;
						if($mTmpVal < 0) {
							$arErr['WRONG_LEVEL_CONDITION'] = 'Условие достижения уровня не может быть отрицательным';
						}  elseif($mTmpVal == 0) {
							$arErr['EMPTY_LEVEL_CONDITION'] = 'Необходимо задать условие достижения уровня';
						}

						// проверим, чтобы был задан хоть один бонус или скидка
						$bWasVal = false;

						$mTmpVal = self::GetPropFieldValue($arPropVals, 'DISCOUNT_VAL_PERC');
						$mTmpVal = $mTmpVal ? doubleval(reset($mTmpVal)) : 0;
						if($mTmpVal < 0 || $mTmpVal > 100) {
							$arErr['WRONG_DISCOUNT_VAL_PERC'] = 'Скидка не может быть отрицательной или больше 100%';
						} elseif($mTmpVal == 0) {
							$arErr['EMPTY_DISCOUNT_VAL_PERC'] = 'Необходимо задать размер скидки';
						}
						if($mTmpVal > 0) {
							$bWasVal = true;
						}

						if(!$arErr) {
							// обнулим свойства, связанные с другими типами бонусов
							$arClearProps = array('BONUS_DISH', 'NIYAM_CNT', 'DISCOUNT_VAL_RUB');
							foreach($arClearProps as $sClearPropCode) {
								if($arPropVals[$sClearPropCode]) {
									$arPropVals[$sClearPropCode] = array();
								} else {
									$iPropId = self::GetPropIdByCode($sClearPropCode);
									if($arPropVals[$iPropId]) {
										$arPropVals[$iPropId] = array();
									}
								}
							}
						}
					break;
				}

			} else {
				// проверка выполняется БУСом
				//$arErr['EMPTY_DISCOUNT_TYPE'] = 'Не задан тип скидки';
			}
		}

		if($arErr) {
			$GLOBALS['APPLICATION']->ThrowException(implode('<br />', $arErr));
			$bReturn = false;
		}
		return $bReturn;
	}

	public static function OnAfterUserAddHandler($arFields) {
		// _NIYAMA_IGNORE_TIMELINE_ = Y - чтобы регистрация не отражалась на таймлайне скидок
		if(isset($arFields['_NIYAMA_IGNORE_TIMELINE_']) && $arFields['_NIYAMA_IGNORE_TIMELINE_'] == 'Y') {
			return;
		} else {
			// добавление записей в инфоблок достижений юзеров за регистрацию
			self::UpdateUserRegistrationLevel($arFields['ID']);
		}
	}

	// добавление записей в инфоблок достижений юзеров за регистрацию
	public static function UpdateUserRegistrationLevel($iUserId, $bNoBonus = false, $arAddEventParams = array()) {
		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			return;
		}
		$iUsersLevelsIBlockId = self::GetUsersLevelsIBlockId();
		$arRegistrationItems = self::FindRegistrationTypeItems();

		if($iUsersLevelsIBlockId && $arRegistrationItems) {
			CModule::IncludeModule('iblock');
			$arCurItems = array();
			$dbItems = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $iUsersLevelsIBlockId,
					'CHECK_PERMISSIONS' => 'N',
					'=PROPERTY_R_USER' => $iUserId,
				),
				false,
				false,
				array('ID', 'IBLOCK_ID', 'PROPERTY_R_DISCOUNT_LEVEL')
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE']) {
					$arCurItems[] = $arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE'];
				}
			}
			$arRegistrationItems = array_diff($arRegistrationItems, $arCurItems);
			if($arRegistrationItems) {
				foreach($arRegistrationItems as $iDiscountLevelElementId) {
					$obIBlockElement = new CIBlockElement();
					$iUserLevelElementId = $obIBlockElement->Add(
						array(
							'IBLOCK_ID' => $iUsersLevelsIBlockId,
							'ACTIVE' => 'Y',
							'NAME' => 'registration-'.$iUserId,
							'PROPERTY_VALUES' => array(
								'R_USER' => $iUserId,
								'R_DISCOUNT_LEVEL' => $iDiscountLevelElementId,
							)
						)
					);

					if($iUserLevelElementId) {
						$arDiscountLevel = self::GetDiscountLevelById($iDiscountLevelElementId);
						$arDiscountLevel['USER_ID'] = $iUserId;
						$arDiscountLevel['USER_LEVEL_ELEMENT_ID'] = $iUserLevelElementId;
						$arDiscountLevel['NO_BONUS'] = $bNoBonus;

						$arEventHandlers = GetModuleEvents('main', 'OnUserDiscountLevelAdd', true);
						foreach($arEventHandlers as $arEvent) {
							ExecuteModuleEventEx($arEvent, array($arDiscountLevel, $arAddEventParams));
						}
					}
				}
			}
		}
	}

	public static function OnSaleStatusOrderHandler($iOrderId) {
		CModule::IncludeModule('sale');
		$arOrder = CSaleOrder::GetById($iOrderId);
		if($arOrder['USER_ID']) {
			$dOrdersSum = self::GetOrdersSumByUser($arOrder['USER_ID']);
			if($dOrdersSum > 0) {
				self::UpdateUserOrderLevelBySum($arOrder['USER_ID'], $dOrdersSum);
			}
		}
	}

	public static function GetOrdersSumByUser($iUserId) {
		$dOrdersSum = 0;

		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			return $dOrdersSum;
		}

		$arUser = CUser::GetById($iUserId)->Fetch();
		if($arUser['ACTIVE'] == 'Y') {
			// достижения по заказам даются только если привязана карта
			$arUserCard = CNiyamaIBlockCardData::GetCardData($iUserId);
			if(!$arUserCard['DATE_ACTIVE_FROM']) {
				return;
			}
			CModule::IncludeModule('sale');
			$dbItems = CSaleOrder::GetList(
				array(),
				array(
					'USER_ID' => $iUserId,
					'CANCELED' => 'N',
					'@STATUS_ID' => array('F', 'N'),
					//'>=UF_ACCEPT_DATE' => $arUserCard['DATE_ACTIVE_FROM'], // убираем так как старые заказы надо тоже учитывать
				),
				array(
					'SUM' => 'PRICE'
				)
			);
			if($arItem = $dbItems->Fetch()) {
				$dOrdersSum = doubleval($arItem['PRICE']);
			}
		}

		return $dOrdersSum;
	}

	protected static function UpdateUserOrderLevel($iUserId, $arAddLevels, $bNoBonus = false, $arAddEventParams = array()) {

		$iUsersLevelsIBlockId = self::GetUsersLevelsIBlockId();
		if($iUserId && $iUsersLevelsIBlockId && $arAddLevels) {
			CModule::IncludeModule('iblock');
			$arCurItems = array();
			$dbItems = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $iUsersLevelsIBlockId,
					'CHECK_PERMISSIONS' => 'N',
					'=PROPERTY_R_USER' => $iUserId,
				),
				false,
				false,
				array('ID', 'IBLOCK_ID', 'PROPERTY_R_DISCOUNT_LEVEL')
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE']) {
					$arCurItems[] = $arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE'];
				}
			}
			$arOrdersItems = array_diff($arAddLevels, $arCurItems);
			if($arOrdersItems) {
				foreach($arOrdersItems as $iDiscountLevelElementId) {
					$obIBlockElement = new CIBlockElement();
					$iUserLevelElementId = $obIBlockElement->Add(
						array(
							'IBLOCK_ID' => $iUsersLevelsIBlockId,
							'ACTIVE' => 'Y',
							'NAME' => 'order-'.$iUserId,
							'PROPERTY_VALUES' => array(
								'R_USER' => $iUserId,
								'R_DISCOUNT_LEVEL' => $iDiscountLevelElementId,
							)
						)
					);
					if($iUserLevelElementId) {
						$arDiscountLevel = self::GetDiscountLevelById($iDiscountLevelElementId);

						$arDiscountLevel['USER_ID'] = $iUserId;
						$arDiscountLevel['USER_LEVEL_ELEMENT_ID'] = $iUserLevelElementId;
						$arDiscountLevel['NO_BONUS'] = $bNoBonus;

						$arEventHandlers = GetModuleEvents('main', 'OnUserDiscountLevelAdd', true);
						foreach($arEventHandlers as $arEvent) {
							ExecuteModuleEventEx($arEvent, array($arDiscountLevel, $arAddEventParams));
						}
					}
				}
			}
		}
	}

	// добавление записей в инфоблок достижений юзеров за суммы заказов
	public static function UpdateUserOrderLevelBySum($iUserId, $dOrdersSum, $bNoBonus = false, $arAddEventParams = array()) {
		$iUserId = intval($iUserId);
		$dOrdersSum = doubleval($dOrdersSum);
		if($iUserId <= 0 || $dOrdersSum <= 0) {
			return;
		}

		$arAddLevels = self::FindOrderTypeItemsBySum($dOrdersSum);
		if($arAddLevels) {
			self::UpdateUserOrderLevel($iUserId, $arAddLevels, $bNoBonus, $arAddEventParams);
		}
	}

	// добавление записей в инфоблок достижений юзеров за изменение величины скидки
	public static function UpdateUserOrderLevelByDiscount($iUserId, $dDiscountVal, $bNoBonus = false, $arAddEventParams = array()) {
		$iUserId = intval($iUserId);
		$dDiscountVal = doubleval($dDiscountVal);
		if($iUserId <= 0 || $dDiscountVal <= 0) {
			return;
		}

		$arAddLevels = self::FindOrderTypeItemsByDiscount($dDiscountVal);
		if($arAddLevels) {
			self::UpdateUserOrderLevel($iUserId, $arAddLevels, $bNoBonus, $arAddEventParams);
		}
	}

	public static function GetDiscountLevels($bActualOnly = true, $bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v5';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = self::GetIBlockId();
			$arDiscountTypeEnumValues = CCustomProject::GetEnumPropValues($iIBlockId, 'DISCOUNT_TYPE');
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'IBLOCK_ID' => $iIBlockId,
					'CHECK_PERMISSIONS' => 'N'
				);
				if($bActualOnly) {
					$arFilter['ACTIVE'] = 'Y';
				}
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC'
					),
					$arFilter,
					false,
					false,
					array(
						'ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'SORT', 'PREVIEW_TEXT', 'DETAIL_TEXT',
						'PROPERTY_*'
					)
				);
				while($obItem = $dbItems->GetNextElement(false, false)) {
					$arItem = $obItem->GetFields();
					$arProp = $obItem->GetProperties();
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'PREVIEW_PICTURE' => $arItem['PREVIEW_PICTURE'],
						'DETAIL_PICTURE' => $arItem['DETAIL_PICTURE'],
						'SORT' => $arItem['SORT'],
						'PREVIEW_TEXT' => $arItem['PREVIEW_TEXT'],
						'DETAIL_TEXT' => $arItem['DETAIL_TEXT'],

						'DISCOUNT_VAL_PERC' => doubleval($arProp['DISCOUNT_VAL_PERC']['VALUE']),
						'DISCOUNT_VAL_RUB' => doubleval($arProp['DISCOUNT_VAL_RUB']['VALUE']),
						'BONUS_DISH' => intval($arProp['BONUS_DISH']['VALUE']),
						'B_ONE_TIME' => $arProp['B_ONE_TIME']['VALUE'] ? 'Y' : 'N',
						'B_SINGLE' => $arProp['B_SINGLE']['VALUE'] ? 'Y' : 'N',
						'NIYAM_CNT' => intval($arProp['NIYAM_CNT']['VALUE']),
						'DISCOUNT_TYPE' => $arProp['DISCOUNT_TYPE']['VALUE'],
						'DISCOUNT_TYPE_XML_ID' => $arProp['DISCOUNT_TYPE']['VALUE'] && $arDiscountTypeEnumValues[$arProp['DISCOUNT_TYPE']['VALUE']] ? $arDiscountTypeEnumValues[$arProp['DISCOUNT_TYPE']['VALUE']]['XML_ID'] : '',
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetDiscountLevelById($iElementId, $bActualOnly = true, $bRefreshCache = false) {
		$arReturn = array();
		$iElementId = intval($iElementId);
		if($iElementId > 0) {
			$arDiscountLevels = self::GetDiscountLevels($bActualOnly, $bRefreshCache);
			if($arDiscountLevels[$iElementId]) {
				$arReturn = $arDiscountLevels[$iElementId];
			}
		}
		return $arReturn;
	}

}

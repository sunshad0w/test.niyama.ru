<?
/**
 * Класс дисконтных карт
 * 
 * @author Sergey Leshchenko, 2014
 * @updated: 14.04.2015
 */

class CNiyamaDiscountCard {
	private static $obNiyamaPds = null;
	protected static $sCardsIdentifyEntityName = 'CardsIdentify';

	// соответствие номеров карт их типам 
	// ! важна последовательность из-за пересечения номеров в типах карт ! 
	// Сначала должны идити карты с наивысшим статусом
	protected static $arCardTypesBorders = array(
		'vip' => array( // VIP-гости
			'left' => 130001,
			'right' => 130100
		),
		'special' => array( // особые гости
			'left' => 130601,
			'right' => 131490
		),
		'staff' => array( // персонал
			'left' => 130105,
			'right' => 130600
		),
		'sushied' => array( // старые карты
			'left' => 100005,
			'right' => 162000
		),
		'pizza_pi' => array( // текущие
			'left' => 162001,
//			'right' => 177000
			'right' => 900100
		),
		'test' => array( // для тестов
			'left' => 100000,
			'right' => 100004
		),
	);

	// соответствие типов карт кодам параметра folder в системе ПДС
	protected static $arCardTypePdsFolfers = array(
		'sushied' => 11,
		'pizza_pi' => 11,
		'test' => 11,
		'staff' => 12,
		'vip' => 14,
		'special' => 16,
	);

	public static function GetCardsIdentifyEntity() {
		return CHLEntity::GetEntityByName(self::$sCardsIdentifyEntityName);
	}

	public static function GetCardsIdentifyEntityClass() {
		$obEntity = self::GetCardsIdentifyEntity();
		if($obEntity) {
			return $obEntity->GetdataClass();
		}
		return '';
	}

	protected static function GetPds($arConnectParams = array()) {
		if(is_null(self::$obNiyamaPds)) {
			self::$obNiyamaPds = new CNiyamaPds($arConnectParams);
		}
		return self::$obNiyamaPds;
	}

	//
	// Возвращает тип карты по ее номеру
	//
	public static function GetCardTypeByCardNum($sCardNum) {
		foreach(self::$arCardTypesBorders as $sCardType => $arTypeBorders) {
			if($sCardNum >= $arTypeBorders['left'] && $sCardNum <= $arTypeBorders['right']) {
				return $sCardType;
			}
		}
		return '';
	}

	public static function GetPdsFolderCodeByCardNum($sCardNum) {
		$sReturn = '';
		$sCardType = self::GetCardTypeByCardNum($sCardNum);
		if($sCardType) {
			if(isset(self::$arCardTypePdsFolfers[$sCardType])) {
				$sReturn = self::$arCardTypePdsFolfers[$sCardType];
			}
		}
		return $sReturn;
	}

	//
	// Можно ли через сайт создавать новую карту с указанным номером
	//
	public static function CanSiteRegisterNum($sCardNum) {
		$sType = self::GetCardTypeByCardNum($sCardNum);
		return $sType && in_array($sType, array('sushied', 'pizza_pi'));
	}

	public static function GetPdsCardInfo($sCardNum, $bGetTransact = false) {
		$obNiyamaPds = self::GetPds();
		$arReturn = $obNiyamaPds->GetDiscountCardArray($sCardNum, $bGetTransact);
		if($arReturn && $arReturn['Number']) {
			$arErrors = array();
			if($arReturn['Number'] == $sCardNum) {
				if($arReturn['Account']) {
					// карта существует, выполним проверки по активности
					$obTmpDate = strlen($arReturn['Account']['Expired']) ? date_create($arReturn['Account']['Expired']) : null;
					$sExpired = $obTmpDate ? $obTmpDate->Format('Ymd') : '';
					$sNow = date('Ymd');
					if($sExpired && $sNow > $sExpired) {
						$arErrors['CARD_DATE_EXPIRED'] = 'Срок действия указанной карты истек';
					} elseif($arReturn['Account']['Deleted'] != 'no') {
						$arErrors['CARD_DELETED'] = 'Указанная карта удалена';
					} elseif($arReturn['Account']['Locked'] != 'no') {
						$arErrors['CARD_LOCKED'] = 'Указанная карта заблокирована';
					} elseif($arReturn['Account']['Seize'] != 'no') {
						$arErrors['CARD_SEIZE'] = 'Указанная карта подлежит изъятию';
					}
				} elseif($arReturn['GetErrorText']) {
					$arErrors['PDS_INTERNAL_ERROR'] = $arReturn['GetErrorText'];
				}
			} else {
				$arErrors['CARD_NUM_ERROR'] = 'Номер карты ответа не совпадает с запрашиваемым';
			}
			$arReturn['_ERRORS_'] = $arErrors;
		}
		return $arReturn;
	}

	public static function RegisterPdsCard($sCardNum, $arFields) {
		$obNiyamaPds = self::GetPds();
		$arFields['Folder'] = self::GetPdsFolderCodeByCardNum($sCardNum);
		return $obNiyamaPds->AddDiscountCardEx($sCardNum, $arFields);
	}

	public static function EditPdsCard($sCardNum, $arFields) {
		// общий метод
		return self::RegisterPdsCard($sCardNum, $arFields);
	}

	public static function GetCardIdentifyData($iUserId) {
		$arReturn = array();
		$sTmpHLEntityClass = self::GetCardsIdentifyEntityClass();
		$arTmpGetListParams = array();
		$arTmpGetListParams['order'] = array(
			'ID' => 'ASC'
		);
		$arTmpGetListParams['filter'] = array(
			'=UF_USER_ID' => $iUserId
		);
		$dbItems = $sTmpHLEntityClass::GetList($arTmpGetListParams);
		if($arItem = $dbItems->Fetch()) {
			$arReturn = $arItem;
		}
		return $arReturn;
	}

	public static function SetCardIdentifyData($iUserId, $sCardNum, $bManualCheck = false) {
		$arReturn = array();
		$sCardNum = trim($sCardNum);
		$iUserId = trim($iUserId);
		if(!strlen($sCardNum) || $iUserId <= 0) {
			return $arReturn;
		}

		$sTmpHLEntityClass = self::GetCardsIdentifyEntityClass();
		$arCurItem = self::GetCardIdentifyData($iUserId);

		$sTmpDate = date('d.m.Y H:i:s');
		$arFields = array(
			'UF_USER_ID' => $iUserId,
			'UF_CARDNUM' => $sCardNum,
			'UF_CHECKWORD' => md5($iUserId.'|'.$sCardNum.'|'.time()),
			'UF_TIMESTAMP_X' => $sTmpDate,
			'UF_MANUAL_CHECK' => $bManualCheck ? 1 : 0,
			'UF_DATE_CREATE' => $sTmpDate,
		);

		if($arCurItem) {
			if($arCurItem['UF_USER_ID'] == $arFields['UF_USER_ID'] && $arCurItem['UF_CARDNUM'] == $arFields['UF_CARDNUM']) {
				unset($arFields['UF_DATE_CREATE']);
			}
			$obTmpResult = $sTmpHLEntityClass::Update($arCurItem['ID'], $arFields);
		} else {
			$obTmpResult = $sTmpHLEntityClass::Add($arFields);
		}
		$arReturn = self::GetCardIdentifyData($iUserId);
		return $arReturn;
	}

	public static function DeleteCardIdentifyData($iUserId) {
		$bReturn = false;
		$iUserId = trim($iUserId);
		if($iUserId <= 0) {
			return $bReturn;
		}

		$sTmpHLEntityClass = self::GetCardsIdentifyEntityClass();
		$arTmpGetListParams = array();
		$arTmpGetListParams['filter'] = array(
			'=UF_USER_ID' => $iUserId
		);
		$dbItems = $sTmpHLEntityClass::GetList($arTmpGetListParams);
		while($arItem = $dbItems->Fetch()) {
			$sTmpHLEntityClass::Delete($arItem['ID']);
			$bReturn = true;
		}

		return $bReturn;
	}

	/**
	 * Устанавливает связь карты с пользователем - добавляет или обновляет элемент с данными по дисконтной карте
	 *
	 * @param array $arPdsCardInfo массив структуры, возращаемый методом CNiyamaPds::GetDiscountCardArray() 
	 * @param integer $iUserId ID пользователя, к которому привязывается карта
	 * @param array $arAddFields дополнительные значения базовых полей для элемента связи (например: ACTIVE, NAME и т.п.)
	 * @param bool $bUpdateIfExists флаг, включающий режим добавления и обновления элемента
	 * @param bool $bCompareUpdateFields флаг, который включает режим сверки текущих полей элемента и устанавливаемых в режиме обновления ($bUpdateIfExists = true)
	 * @return integer 0 - если не было успешных действий добавления или обновления элемента, ID элемента в противном случае
	 */
	public static function AddCardDataElementByPdsInfo($arPdsCardInfo, $iUserId, $arAddFields = array(), $bUpdateIfExists = false, $bCompareUpdateFields = false) {
		$iReturn = 0;
		$arPropFields = array(
			'CARDNUM' => $arPdsCardInfo['Account']['Card'],
			'DISCOUNT' => $arPdsCardInfo['Account']['Discount'],
			'NAME' => '',
			'LAST_NAME' => '',
			'SECOND_NAME' => '',
			'BIRTHDAY' => $arPdsCardInfo['User']['Birthday'],
			'PHONE' => $arPdsCardInfo['User']['Tel1'],
			'EMAIL' => ToLower($arPdsCardInfo['User']['Email']),
			'CITY' => '',
			'STREET' => '',
			'HOME' => '',
			'HOUSING' => '',
			'BUILDING' => '',
			'APARTMENT' => '',
			'OFFERED' => $arPdsCardInfo['Account']['Offered']
		);

		if($arPdsCardInfo['User']['Holder']) {
			$arTmp = explode(' ', $arPdsCardInfo['User']['Holder']);
			switch(count($arTmp)) {
				case 2:
					$arPropFields['LAST_NAME'] = $arTmp[1];
					$arPropFields['NAME'] = $arTmp[0];
				break;

				case 3:
					$arPropFields['LAST_NAME'] = $arTmp[0];
					$arPropFields['NAME'] = $arTmp[1];
					$arPropFields['SECOND_NAME'] = $arTmp[2];
				break;

				default:
					$arPropFields['NAME'] = $arPdsCardInfo['User']['Holder'];
				break;
			}
		}

		// разбор вариантов заполнения адреса
		/*
		Город: Одинцово
		Улица: Маршала Бирюзова
		Дом: 28
		Корпус: 2
		Квартира: 157
		---
		Одинцово
		Улица: бульвар Маршала Крылова
		Дом: 6
		Квартира: 181
		---
		Одинцово, бульвар Маршала Крылова, 6, 181
		*/
		if($arPdsCardInfo['User']['Address']) {
			$bTmp = false;
			if(strpos($arPdsCardInfo['User']['Address'], "\n") !== false) {
				$bTmp = true;
			}
			$arTmp = $bTmp ? explode("\n", $arPdsCardInfo['User']['Address']) : explode(',', $arPdsCardInfo['User']['Address']);
			if($bTmp) {
				$arPropFields['CITY'] = trim($arTmp[0]);
				$arPropFields['CITY'] = stripos($arPropFields['CITY'], 'Город:') === 0 ? trim(substr($arPropFields['CITY'], strlen('Город:'))) : $arPropFields['CITY'];
				unset($arTmp[0]);
				foreach($arTmp as $sTmpVal) {
					if(strlen($sTmpVal)) {
						if(stripos($sTmpVal, 'Улица:') === 0) {
							$arPropFields['STREET'] = trim(substr($sTmpVal, strlen('Улица:')));
						} elseif(stripos($sTmpVal, 'Дом:') === 0) {
							$arPropFields['HOME'] = trim(substr($sTmpVal, strlen('Дом:')));
						} elseif(stripos($sTmpVal, 'Корпус:') === 0) {
							$arPropFields['HOUSING'] = trim(substr($sTmpVal, strlen('Корпус:')));
						} elseif(stripos($sTmpVal, 'Строение:') === 0) {
							$arPropFields['BUILDING'] = trim(substr($sTmpVal, strlen('Строение:')));
						} elseif(stripos($sTmpVal, 'Квартира:') === 0) {
							$arPropFields['APARTMENT'] = trim(substr($sTmpVal, strlen('Квартира:')));
						}
					}
				}
			} else {
				$arPropFields['CITY'] = trim($arTmp[0]);
				$arPropFields['STREET'] = $arTmp[1] ? trim($arTmp[1]) : '';
				$arPropFields['HOME'] = $arTmp[2] ? trim($arTmp[2]) : '';
				switch(count($arTmp)) {
					case 4:
						$arPropFields['APARTMENT'] = $arTmp[3] ? trim($arTmp[3]) : '';
						$arPropFields['APARTMENT'] = $arTmp[3] ? trim($arTmp[3]) : '';
					break;

					case 5:
						$arPropFields['HOUSING'] = $arTmp[3] ? trim($arTmp[3]) : '';
						$arPropFields['APARTMENT'] = $arTmp[4] ? trim($arTmp[4]) : '';
					break;

					case 6:
						$arPropFields['HOUSING'] = $arTmp[3] ? trim($arTmp[3]) : '';
						$arPropFields['BUILDING'] = $arTmp[4] ? trim($arTmp[4]) : '';
						$arPropFields['APARTMENT'] = $arTmp[5] ? trim($arTmp[5]) : '';
					break;
				}

				if(stripos($arPropFields['CITY'], 'Город:') === 0) {
					$arPropFields['CITY'] = trim(substr($arPropFields['CITY'], strlen('Город:')));
				}
				if(stripos($arPropFields['STREET'], 'Улица:') === 0) {
					$arPropFields['STREET'] = trim(substr($arPropFields['STREET'], strlen('Улица:')));
				}
				if(stripos($arPropFields['HOME'], 'Дом:') === 0) {
					$arPropFields['HOME'] = trim(substr($arPropFields['HOME'], strlen('Дом:')));
				}
				if(stripos($arPropFields['HOUSING'], 'Корпус:') === 0) {
					$arPropFields['HOUSING'] = trim(substr($arPropFields['HOUSING'], strlen('Корпус:')));
				}
				if(stripos($arPropFields['BUILDING'], 'Строение:') === 0) {
					$arPropFields['BUILDING'] = trim(substr($arPropFields['BUILDING'], strlen('Строение:')));
				}
				if(stripos($arPropFields['APARTMENT'], 'Квартира:') === 0) {
					$arPropFields['APARTMENT'] = trim(substr($arPropFields['APARTMENT'], strlen('Квартира:')));
				}
			}
		}

		$bTmpAddHistory = true;
		$iReturn = CNiyamaIBlockCardData::Add($arPropFields, $iUserId, $arAddFields, $bTmpAddHistory);
		if(!$iReturn && $bUpdateIfExists) {
			$arCardData = CNiyamaIBlockCardData::GetCardData($iUserId, true, true);
			if($arCardData['ID']) {
				$arDif = self::CompareFields($arCardData, $arPropFields, $arAddFields);
				if(!$bCompareUpdateFields || $arDif) {
					$bTmpAddHistory = $arDif ? true : false;
					$iReturn = CNiyamaIBlockCardData::Update($arCardData['ID'], $arPropFields, $arAddFields, $bTmpAddHistory);
				}
			}
		}

		return $iReturn;
	}

	protected static function CompareFields($arCardData, $arUpdatePropFields, $arUpdateFields) {
		$arDiff = array();
		foreach($arUpdatePropFields as $sPropCode => $mVal) {
			if(isset($arCardData['PROPERTY_'.$sPropCode.'_VALUE'])) {
				if($arCardData['PROPERTY_'.$sPropCode.'_VALUE'] != $mVal) {
					$arDiff[] = 'PROPERTY_'.$sPropCode.'_VALUE';
				}
			}
		}
		$arUpdateFields = is_array($arUpdateFields) ? $arUpdateFields : array();
		foreach($arUpdateFields as $sFieldCode => $mVal) {
			if(isset($arCardData[$sFieldCode])) {
				if($arCardData[$sFieldCode] != $mVal) {
					$arDiff[] = $sFieldCode;
				}
			}
		}

		return $arDiff;
	}

	// проверяет наличие у юзера дисконтной карты, находящейся на идентификации через карточку пользователя, 
	// если таковая есть и все условия выполняются, то привязывает карту к юзеру
	public static function CheckManualIdentify($iUserId, $bNoBonus = false) {
		$iCardDataElementId = 0;
		$arErrors = array();
		$iUserId = intval($iUserId);
		$arIdentify = $iUserId > 0 ? self::GetCardIdentifyData($iUserId) : array();
		if($arIdentify && $arIdentify['UF_MANUAL_CHECK']) {
			$sTmpCheckCard = trim($arIdentify['UF_CARDNUM']);
			$bDelItem = false;
			if(strlen($sTmpCheckCard)) {
				if(!$arErrors) {
					// проверим, нет ли уже привязанных карт
					$arCurUserCard = CNiyamaIBlockCardData::GetCardData($iUserId, true);
					if($arCurUserCard) {
						if($arCurUserCard['ACTIVE'] == 'Y') {
							$arErrors['CARD_ALREADY_ACTIVE'] = 'Уже активирована карта с другим номером';
							$bDelItem = true;
						}
						/*
						elseif($arCurUserCard['PROPERTY_CARDNUM_VALUE'] != $sTmpCheckCard) {
							$arErrors['WRONG_CARD'] = 'Номера активируемой и уже зарегистрированной карт не совпадают';
						}
						*/
					}
				}

				if(!$arErrors) {
					// запросим в ПДС данные карты
					$arPdsCardInfo = self::GetPdsCardInfo($sTmpCheckCard);
					if($arPdsCardInfo && $arPdsCardInfo['Number'] == $sTmpCheckCard) {
						if($arPdsCardInfo['_ERRORS_']) {
							$arErrors = array_merge($arErrors, $arPdsCardInfo['_ERRORS_']);
							$bDelItem = true;
						}

						if(!$arErrors && $arPdsCardInfo['User']) {
							// сверяем данные карты с данными в карточке юзера
							$bOk = false;
							$arUserData = CUsersData::GetDataUserForLK($iUserId);

							if($arUserData) {
								if(strlen($arUserData['PHONE']) && CNiyamaPds::FormatTel($arUserData['PHONE']) == CNiyamaPds::FormatTel($arPdsCardInfo['User']['Tel1'])) {
									if(strlen($arUserData['EMAIL']) && ToLower($arUserData['EMAIL']) == ToLower($arPdsCardInfo['User']['Email'])) {
										$bOk = true;
									}
								}
							}

							// все ок, привязываем карту к юзеру
							if($bOk) {
								$iCardDataElementId = self::AddCardDataElementByPdsInfo($arPdsCardInfo, $iUserId, array('ACTIVE' => 'Y'), true);
								if($iCardDataElementId) {
									if($arPdsCardInfo['Account']['Discount']) {
										CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iUserId, CNiyamaIBlockCardData::GetTrueDiscount($arPdsCardInfo['Account']['Discount']), $bNoBonus);
									}

									$bDelItem = true;
								} else {
									$arErrors['UNKNOWN_ERROR'] = 'Не удалось привязать карту к пользователю';
								}
							}
						}
					}
				}
			} else {
				$arErrors['CARDNUM_EMPTY'] = 'Не задана информация о номере карты';
				$bDelItem = true;
			}

			if($bDelItem) {
				$sTmpHLEntityClass = self::GetCardsIdentifyEntityClass();
				$sTmpHLEntityClass::Delete($arIdentify['ID']);
			}

			return $iCardDataElementId;
		}
	}

	/**
	 * Устанавливает величину скидки по карте. Если устанавливаемая величина меньше значения, хранимого в ПДС, то берется значение скидки в ПДС
	 */
	public static function SetDiscountCardValueByUserId($iUserId, $iSetDiscountValue) {
		$mResultDiscountValue = false;

		$iSetDiscountValue = intval($iSetDiscountValue);
		$iUserId = intval($iUserId);
		if($iSetDiscountValue < 5 || $iUserId < 1) {
			return $mResultDiscountValue;
		}

		// карточка дисконтной карты юзера
		$bRefreshCache = false;
		$arCardData = CNiyamaIBlockCardData::GetCardData($iUserId, false, $bRefreshCache);
		if($arCardData) {
			$sCardNumber = $arCardData['PROPERTY_CARDNUM_VALUE'];
			// данные по дисконтной карте в ПДС
			$arNiyamaCard = self::GetPdsCardInfo($sCardNumber, false);
			if($arNiyamaCard && !$arNiyamaCard['_ERRORS_']) {
				// величина скидки в ПДС
				$iPdsDiscountValue = intval(CNiyamaIBlockCardData::GetTrueDiscount($arNiyamaCard['Account']['Discount']));
				// определяем где больше скидка, на сайте или в ПДС, бОльшую берем как устанавливаемое значение
				$iSetDiscountValue = $iPdsDiscountValue > $iSetDiscountValue ? $iPdsDiscountValue : $iSetDiscountValue;
				// величина скидки в карточке юзера (поле UF_DISCOUNT)
				$iUserCardDiscountValue = CNiyamaIBlockCardData::GetUserDiscountDB($iUserId);
				// определяем какая больше скидка, бОльшую берем как устанавливаемое значение
				$iSetDiscountValue = $iUserCardDiscountValue > $iSetDiscountValue ? $iUserCardDiscountValue : $iSetDiscountValue;

				// определяем ПДС-код скидки. Например, для 5% - 32, для 6% - 34 и т.д.
				$iSetDiscountCode = CNiyamaIBlockCardData::GetDiscountCode($iSetDiscountValue);

				// обновляем скидку в ПДС
				if($iSetDiscountValue != $iPdsDiscountValue) {
// ??? непонятно, какое значение скидки ПДС принимает ???
					$arUpdatePdsFields = array(
						'Discount' => $iSetDiscountCode
						//'Discount' => $iSetDiscountValue
						//'Discount' => $iSetDiscountValue.'%'
					);
					$arUpdatedNiyamaCard = self::EditPdsCard($sCardNumber, $arUpdatePdsFields);
				}

				// сохраняем значение увеличенной скидки в инфоблоке
				// здесь мухлюем с величиной скидки, т.к. ПДС работает с задержкой при апдейте и сразу нужные данные не возвращает
				$arNiyamaCard_ = $arNiyamaCard;
				$arNiyamaCard_['Account']['Discount'] = $iSetDiscountCode;
				$arAddFields = array();
				$bUpdateIfExists = true;
				$bCompareUpdateFields = true;
				self::AddCardDataElementByPdsInfo($arNiyamaCard_, $iUserId, $arAddFields, $bUpdateIfExists, $bCompareUpdateFields);

				// сохраняем значение увеличенной скидки в карточке юзера
				if($iUserCardDiscountValue != $iSetDiscountValue) {
					$GLOBALS['USER_FIELD_MANAGER']->Update('USER', $iUserId, array('UF_DISCOUNT' => $iSetDiscountValue));
					// обновим кэш метода
					CNiyamaIBlockCardData::GetUserDiscountDB($iUserId, true);
				}

				$mResultDiscountValue = $iSetDiscountValue;
			}
		}

		return $mResultDiscountValue;
	}

	/**
	 * Синхронизация данных по дисконтной карте юзера с ПДС
	 */
	public static function SyncUserDiscountCard($iUserId, $bRefreshCache = false) {
		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			return;
		}

		// карточка дисконтной карты юзера
		$arCardData = CNiyamaIBlockCardData::GetCardData($iUserId, false, $bRefreshCache);
		if($arCardData) {
			$sCardNumber = $arCardData['PROPERTY_CARDNUM_VALUE'];
			// данные по дисконтной карте в ПДС
			$arNiyamaCard = self::GetPdsCardInfo($sCardNumber, false);
			if($arNiyamaCard && !$arNiyamaCard['_ERRORS_']) {
				// не будем начислять бонусы за переход на новый уровень скидок
				$bNoBonus = true;

				// скидка в базе ПДС
                $iDiscountPds = intval(CNiyamaIBlockCardData::GetTrueDiscount($arNiyamaCard['Account']['Discount']));
				// скидка в элементе дисконтной карты
				$iDiscountIB = intval($arCardData['TRUE_DISCOUNT']);
				// скидка по заказам на сайте
				$dOrdersSum = CNiyamaDiscountLevels::GetOrdersSumByUser($iUserId);
				$arDiscount = CNiyamaDiscountLevels::FindMaxDiscountBySum($dOrdersSum);
				$iDiscountByOrders = intval($arDiscount['DISCOUNT']);
				// величина скидки в карточке юзера (поле UF_DISCOUNT)
				$iDiscountUser = CNiyamaIBlockCardData::GetUserDiscountDB($iUserId);

				if($iDiscountPds > $iDiscountIB) {
					// если скидка в ПДС больше скидки на сайте, тогда начисляем бонусы
					$bNoBonus = false;
				}

				// какая скидка должна быть (выбираем максимальную)
				$iSetDiscount = $iDiscountPds > $iDiscountIB ? $iDiscountPds : $iDiscountIB;
				//$iSetDiscount = $iDiscountByOrders > $iSetDiscount ? $iDiscountByOrders : $iSetDiscount;
				$iSetDiscount = $iDiscountUser > $iSetDiscount ? $iDiscountUser : $iSetDiscount;

				if($iDiscountPds < $iSetDiscount || $iDiscountIB < $iSetDiscount || $iDiscountUser < $iSetDiscount) {
					self::SetDiscountCardValueByUserId($iUserId, $iSetDiscount);
				}

				// обработчик события метода в случае изменения уровня скидки в свою очередь вызовет CNiyamaDiscountCard::SetDiscountCardValueByUserId()
				CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iUserId, $iSetDiscount, $bNoBonus);
			}
		}
	}

}
<?
/**
 * 
 * Класс утилит для инфблока "Импортируемое меню"
 * @author Sergey Leshchenko, 2014
 * @updated: 14.07.2015
 *
 */
if(!defined('NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT')) {
	define('NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT', true);
}

class CNiyamaIBlockCatalogBaseIts {
	protected static $obLog = null;
	protected static $bLogEnabled = NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT;
	const NIYAMA_FO_IMPORT_IMAGES_DIR_LOCAL = '/tmp/';
	const MAX_RETRIES = 3;
	const CURLOPT_CONNECTTIMEOUT = 5;
	const CURLOPT_TIMEOUT = 50;

	public static function GetLogInstance() {
		if(is_null(self::$obLog)) {
			$sLogFileName = $_SERVER['DOCUMENT_ROOT'].'/_log/_imports/'.__CLASS__.'/'.date('Y-m-d-H-i-s').'.txt';
			$bAddTimestamp = true;
			self::$obLog = new CCustomLog($sLogFileName, false, $bAddTimestamp);
			self::$obLog->SetRowSeparator('');
		}
		return self::$obLog;
	}

	public static function Log($sOpCode, $mLogInfo) {
		if(self::$bLogEnabled) {
			$obLog = self::GetLogInstance();
			$obLog->Log(array($sOpCode, $mLogInfo));
		}
	}

	public static function SetLogEnabled() {
		self::$bLogEnabled = true;
	}

	public static function SetLogDisabled() {
		self::$bLogEnabled = false;
	}

	public static function IsLogEnabled() {
		return self::$bLogEnabled;
	}

	public static function GetIBlockId() {
		static $iReturn = 0;
		if(!$iReturn) {
			$iReturn = CProjectUtils::GetIBlockIdByCode('catalog-base', 'catalog');
		}
		return $iReturn;
	}

	public static function GetSiteMenuIBlockId() {
		static $iReturn = 0;
		if(!$iReturn) {
			$iReturn = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');
		}
		return $iReturn;
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	public static function AddBaseItem($sElementXmlId, $arFields = array()) {
		$iReturnElementId = 0;
		CModule::IncludeModule('iblock');
		$iIBlockId = self::GetIBlockId();

		$arFields['IBLOCK_ID'] = $iIBlockId;
		$arFields['ACTIVE'] = $arFields['ACTIVE'] ? $arFields['ACTIVE'] : 'N';
		$arFields['XML_ID'] = trim($sElementXmlId);
		$arFields['NAME'] = isset($arFields['NAME']) && strlen($arFields['NAME']) ? $arFields['NAME'] : $arFields['XML_ID'];
		if(strlen($arFields['XML_ID'])) {
			$dbItems = CIBlockElement::GetList(
				array(
					'ACTIVE' => 'DESC',
					'ID' => 'ASC',
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'=XML_ID' => $arFields['XML_ID']
				),
				false,
				false,
				array('ID', 'NAME')
			);
			$arItem = $dbItems->Fetch();
			if(!$arItem) {
				$obIBlockElement = new CIBlockElement();
				$iReturnElementId = $obIBlockElement->Add($arFields);
			} else {
				$iReturnElementId = $arItem['ID'];
				$arFields['NAME'] = strlen($arItem['NAME']) ? $arItem['NAME'] : $arFields['NAME'];
				// проверяем и добавляем элемент инфоблока Основное меню (теоретически может отсутствовать)
				self::AddSiteMenuItem($iReturnElementId, $arFields);
			}
		}
		return $iReturnElementId;
	}

	// добавление элемента в инфоблоке основного меню с привязкой к элементу импортируемого меню
	public static function AddSiteMenuItem($iImportedElementId, $arFields) {
		$iReturnElementId = 0;
		CModule::IncludeModule('iblock');
		$iSiteMenuIBlockId = self::GetSiteMenuIBlockId();
		$dbItems = CIBlockElement::GetList(
			array(
				'ID' => 'ASC'
			),
			array(
				'IBLOCK_ID' => $iSiteMenuIBlockId,
				'PROPERTY_IMPORT_ELEMENT' => $iImportedElementId
			),
			false,
			false,
			array('ID')
		);
		$arItem = $dbItems->Fetch();
		if(!$arItem) {
			if(isset($arFields['NAME']) && strlen(trim($arFields['NAME']))) {
				$obIBlockElement = new CIBlockElement();
				// проверяем символьный код
				$sCode = CNiyamaImportUtils::Translit($arFields['NAME']);
				$iTmpCnt = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_ID' => $iSiteMenuIBlockId,
						'=CODE' => $sCode
					),
					array()
				);
				if($iTmpCnt) {
					$sCode = $sCode.'-'.RandString(10);
				}
				$iReturnElementId = $obIBlockElement->Add(
					array(
						'IBLOCK_ID' => $iSiteMenuIBlockId,
						'ACTIVE' => 'N',
						'NAME' => $arFields['NAME'],
						'CODE' => $sCode,
						'PROPERTY_VALUES' => array(
							'IMPORT_ELEMENT' => $iImportedElementId,
						)
					)
				);
			}
		} else {
			$iReturnElementId = $arItem['ID'];
		}
		return $iReturnElementId;
	}

	//
	// Актуализация базового инфоблока каталога (меню блюд) информацией из FastOperator
	//
	public static function ImportData() {
		// логируем операцию
		self::Log('ImportData', 'START');

		$bResult = false;
		CModule::IncludeModule('iblock');
		$iCatalogBaseIBlockId = self::GetIBlockId();
		$obFastOperator = new CNiyamaFastOperator();
		$arCatalogBase = $obFastOperator->GetMenuArray();
		// логируем операцию
		self::Log('GetMenuArray', array('arCatalogBase' => $arCatalogBase));

		if(!$arCatalogBase || !$iCatalogBaseIBlockId) {
			return $bResult;
		}

		$bResult = true;

		$sUpdateTmpIdField = time();
		// работа с секциями
		if($arCatalogBase['Category']) {
			$arSectionsXmlId = array();
			// сформируем массив для дерева секций
			$arTmpCategory = array();
			$arTmpCategoryCheck = array();
			foreach($arCatalogBase['Category'] as $arItem) {
				if(strlen($arItem['Code'])) {
					$sParentCode = strlen($arItem['ParentCode']) ? $arItem['ParentCode'] : 0;
					$arTmpCategory[$sParentCode][$arItem['Code']] = $arItem;
					$arTmpCategoryCheck[$arItem['Code']] = $arItem;
					$arSectionsXmlId[$arItem['Code']] = $sParentCode;
				}
			}
			$iLeftMargin = 0;
			$arCategory = self::GroupsRecursive(array(), $arTmpCategory, 0, 0, $iLeftMargin, $arTmpCategoryCheck);
			if(!empty($arTmpCategoryCheck)) {
				foreach($arTmpCategoryCheck as $arItem) {
					$arItem['_DEPTH_LEVEL_'] = 1;
					$arItem['_LEFT_MARGIN_'] = ++$iLeftMargin;
					$arItem['_RIGHT_MARGIN_'] = ++$iLeftMargin;
					$arCategory[] = $arItem;
				}
			}

			// сортируем
			CProjectUtils::DoSort($arCategory, '_LEFT_MARGIN_', 'ASC');
			// выбираем все секции
			$arCurrentSections = array();
			$dbItems = CIBlockSection::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iCatalogBaseIBlockId,
					'CHECK_PERMISSIONS' => 'N'
				),
				false,
				array(
					'ID', 'XML_ID', 'NAME', 'ACTIVE', 'IBLOCK_SECTION_ID'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$arCurrentSections[$arItem['XML_ID']][$arItem['ID']] = $arItem;
			}
			$arAdded = array();
			$arUpdated = array();
			foreach($arCategory as $arItem) {
				$iParentSectionId = 0;
				if(strlen($arItem['ParentCode'])) {
					if($arCurrentSections[$arItem['ParentCode']]) {
						$arTmp = reset($arCurrentSections[$arItem['ParentCode']]);
						if($arTmp['ID']) {
							$iParentSectionId = $arTmp['ID'];
						}
					} elseif($arAdded[$arItem['ParentCode']]) {
						$iParentSectionId = $arAdded[$arItem['ParentCode']];
					}
				}
				$arFields = array(
					'IBLOCK_ID' => $iCatalogBaseIBlockId,
					'NAME' => $arItem['Name'],
					'IBLOCK_SECTION_ID' => $iParentSectionId,
					'ACTIVE' => 'Y',
					// !!!
					'XML_ID' => $arItem['Code'],
					'TMP_ID' => $sUpdateTmpIdField
				);
				$obIBlockSection = new CIBlockSection();
				if(!$arCurrentSections[$arItem['Code']]) {
					// добавление новой секции
					$iSectionId = $obIBlockSection->Add($arFields);
					$arAdded[$arItem['Code']] = $iSectionId;
					// логируем операцию
					self::Log('IBlockSectionAdd', array('ID' => $iSectionId, 'arFields' => $arFields));
				} else {
					$arTmpItem = reset($arCurrentSections[$arItem['Code']]);
					// обновление секции
					$obIBlockSection->Update($arTmpItem['ID'], $arFields);
					$arUpdated[$arItem['Code']] = $arTmpItem['ID'];
					// логируем операцию
					self::Log('IBlockSectionUpdate', array('ID' => $arTmpItem['ID'], 'arFields' => $arFields));
				}
			}

			// деактивируем секции, которых не было в выгрузке
			foreach($arCurrentSections as $sSectionXmlId => $arSections) {
				if(strlen($sSectionXmlId)) {
					$iTmpSectionId = 0;
					if(isset($arAdded[$sSectionXmlId])) {
						$iTmpSectionId = $arAdded[$sSectionXmlId];
					} elseif($arUpdated[$sSectionXmlId]) {
						$iTmpSectionId = $arUpdated[$sSectionXmlId];
					}
					if($iTmpSectionId && isset($arCurrentSections[$sSectionXmlId][$iTmpSectionId])) {
						unset($arCurrentSections[$sSectionXmlId][$iTmpSectionId]);
						if(empty($arCurrentSections[$sSectionXmlId])) {
							unset($arCurrentSections[$sSectionXmlId]);
						}
					}
				}
			}
			if($arCurrentSections) {
				foreach($arCurrentSections as $sSectionXmlId => $arSections) {
					foreach($arSections as $arTmpItem) {
						if($arTmpItem['ACTIVE'] == 'Y') {
							$obIBlockSection->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
							// логируем операцию
							self::Log('IBlockSectionDeactivate', array('ID' => $arTmpItem['ID'], 'sUpdateTmpIdField' => $sUpdateTmpIdField));
						}
					}
				}
			}
		}

		// работа с элементами
		if($arCatalogBase['Items']) {
			$bWorkFlow = false;
			$bUpdateSearch = true;
			$bResizePictures = true;

			// выборка секций для установки связей
			$arCurrentSections = array();
			$dbItems = CIBlockSection::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iCatalogBaseIBlockId,
					'ACTIVE' => 'Y',
					'CHECK_PERMISSIONS' => 'N',
				),
				false,
				array(
					'ID', 'XML_ID'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if(strlen($arItem['XML_ID']) && !isset($arCurrentSections[$arItem['XML_ID']]))  {
					$arCurrentSections[$arItem['XML_ID']] = $arItem;
				}
			}

			// обновление элементов
			$arUpdateItemsList = array();
			foreach($arCatalogBase['Items'] as $arTmpItem) {
				if(strlen($arTmpItem['Code'])) {
					$arUpdateItemsList[$arTmpItem['Code']] = $arTmpItem;
				}
			}
			if($arUpdateItemsList) {
				$sDownloadImagesUrl = self::NIYAMA_FO_IMPORT_IMAGES_DIR_LOCAL;
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iCatalogBaseIBlockId,
						'=XML_ID' => array_keys($arUpdateItemsList),
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'XML_ID', 'DETAIL_PICTURE', 'PREVIEW_PICTURE'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					if($arUpdateItemsList[$arItem['XML_ID']]) {
						$arTmpUpdateItem = $arUpdateItemsList[$arItem['XML_ID']];
						$arFields = array(
							'NAME' => $arTmpUpdateItem['Name'],
							'ACTIVE' => 'Y',
							'IBLOCK_SECTION_ID' => strlen($arTmpUpdateItem['_category_code_']) && $arCurrentSections[$arTmpUpdateItem['_category_code_']] ? $arCurrentSections[$arTmpUpdateItem['_category_code_']]['ID'] : 0,
							// !!!
							'TMP_ID' => $sUpdateTmpIdField,
						);

						if($sDownloadImagesUrl) {
							$sTmpFileUrl = $sDownloadImagesUrl.$arItem['XML_ID'].'.jpg';
							$rsFile = @fopen($sTmpFileUrl, 'rb');
							if($rsFile) {
								@fclose($rsFile);
								$arTmpFile = CFile::MakeFileArray($sTmpFileUrl);
								$arCurPicture = $arItem['DETAIL_PICTURE'] ? CFile::GetFileArray($arItem['DETAIL_PICTURE']) : array();
								if(!$arItem['PREVIEW_PICTURE'] || !$arCurPicture || $arCurPicture && $arCurPicture['FILE_SIZE'] != $arTmpFile['size']) {
									$arFields['DETAIL_PICTURE'] = $arTmpFile;
								}
							}
						}

						$arTmp = explode('/', $arTmpUpdateItem['Weight']);
						$dCalcWeight = 0;
						foreach($arTmp as $sTmpVal) {
							$dTmpVal = CNiyamaImportUtils::Str2Double($sTmpVal);
							if($dTmpVal > 0) {
								$dCalcWeight += $dTmpVal;
							}
						}
						$arProp = array(
							'SIZE' => $arTmpUpdateItem['Size'],
							'NAME_SHORT' => $arTmpUpdateItem['NameShort'],
							'UNIT_NAME' => $arTmpUpdateItem['UnitName'],
							'WEIGHT_FULL' => $arTmpUpdateItem['Weight'],
							'WEIGHT' => $dCalcWeight,
							'CALORIC' => $arTmpUpdateItem['Energy'],
							'SORT' => $arTmpUpdateItem['Sort'],
							'RECOMMENDED' => $arTmpUpdateItem['Recommended'],
							'PRICE' => $arTmpUpdateItem['Price'],
						);
						$obIBlockElement = new CIBlockElement();
						$obIBlockElement->Update($arItem['ID'], $arFields, $bWorkFlow, $bUpdateSearch, $bResizePictures);
						CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iCatalogBaseIBlockId, $arProp);
						// !!!
						unset($arUpdateItemsList[$arItem['XML_ID']]);

						// логируем операцию
						self::Log('IBlockElementUpdate', array('ID' => $arItem['ID'], 'arFields' => $arFields, 'arProp' => $arProp, 'XML_ID' => $arItem['XML_ID']));
					}
				}
			}

			// добавление элементов
			if($arUpdateItemsList) {
				foreach($arUpdateItemsList as $arTmpUpdateItem) {
					$arFields = array(
						'IBLOCK_ID' => $iCatalogBaseIBlockId,
						'NAME' => $arTmpUpdateItem['Name'],
						'ACTIVE' => 'Y',
						'IBLOCK_SECTION_ID' => strlen($arTmpUpdateItem['_category_code_']) && $arCurrentSections[$arTmpUpdateItem['_category_code_']] ? $arCurrentSections[$arTmpUpdateItem['_category_code_']]['ID'] : 0,
						// !!!
						'XML_ID' => $arTmpUpdateItem['Code'],
						'TMP_ID' => $sUpdateTmpIdField,
					);
					if($sDownloadImagesUrl) {
						$sTmpFileUrl = $sDownloadImagesUrl.$arFields['XML_ID'].'.jpg';
						$arTmpFile = CFile::MakeFileArray($sTmpFileUrl);
						if($arTmpFile['size']) {
							$arFields['DETAIL_PICTURE'] = $arTmpFile;
						}
					}

					$arTmp = explode('/', $arTmpUpdateItem['Weight']);
					$arProp = array(
						'SIZE' => $arTmpUpdateItem['Size'],
						'NAME_SHORT' => $arTmpUpdateItem['NameShort'],
						'UNIT_NAME' => $arTmpUpdateItem['UnitName'],
						'WEIGHT_FULL' => $arTmpUpdateItem['Weight'],
						'WEIGHT' => intval($arTmp[0]),
						'CALORIC' => isset($arTmp[1]) ? intval($arTmp[1]) : '',
						'SORT' => $arTmpUpdateItem['Sort'],
						'RECOMMENDED' => $arTmpUpdateItem['Recommended'],
						'PRICE' => $arTmpUpdateItem['Price'],
					);
					$arFields['PROPERTY_VALUES'] = $arProp;
					$obIBlockElement = new CIBlockElement();
					$iNewElementId = $obIBlockElement->Add($arFields, $bWorkFlow, $bUpdateSearch, $bResizePictures);

					// логируем операцию
					self::Log('IBlockElementAdd', array('ID' => $iNewElementId, 'arFields' => $arFields));
				}
			}

			// деактивируем элементы, которых не было в выгрузке
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iCatalogBaseIBlockId,
					'!TMP_ID' => $sUpdateTmpIdField,
					'CHECK_PERMISSIONS' => 'N',
					'ACTIVE' => 'Y'
				),
				false,
				false,
				array(
					'ID'
				)
			);
			while($arTmpItem = $dbItems->Fetch()) {
				$obIBlockElement = new CIBlockElement();
				$bTmpSuccess = $obIBlockElement->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
				// логируем операцию
				self::Log('IBlockElementDeactivate', array('ID' => $arTmpItem['ID'], 'sUpdateTmpIdField' => $sUpdateTmpIdField, 'bTmpSuccess' => $bTmpSuccess, 'LAST_ERROR' => !$bTmpSuccess ? $obIBlockElement->LAST_ERROR : ''));
			}
		}

		// логируем операцию
		self::Log('ImportData', 'FINISH');
		return $bResult;
	}

	public static function download($url, $baseDir)
	{
		$fn = $baseDir.basename($url);

		$fp = fopen ($fn, 'w+');
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CURLOPT_CONNECTTIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT, self::CURLOPT_TIMEOUT);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$ok = curl_exec($ch);
		$info = curl_getinfo($ch);
		$errno = curl_errno($ch);
		curl_close($ch);
		fclose($fp);

		$result = [
			'ok' => $ok,
			'info' => $info,
			'errno' => $errno
		];

		return $result;
	}

	public static function ImportImages() {
		// логируем операцию
		self::Log('DownloadImages', 'START');

		$bResult = false;

		$obFastOperator = new CNiyamaFastOperator();
		$arCatalogBase = $obFastOperator->GetMenuArray();
		// логируем операцию
		self::Log('GetMenuArray', array('arCatalogBase' => $arCatalogBase));

		if(!$arCatalogBase) {
			return $bResult;
		}

		$bResult = true;

		// работа с элементами
		if($arCatalogBase['Items']) {

			$sDownloadImagesUrl = defined('NIYAMA_FO_IMPORT_IMAGES_DIR') ? NIYAMA_FO_IMPORT_IMAGES_DIR : '';

			foreach ($arCatalogBase['Items'] as $arTmpItem) {
				if (strlen($arTmpItem['Code'])) {
					$sTmpFileUrl = $sDownloadImagesUrl.$arTmpItem['Code'].'.jpg';
					$try = 0;
					$done = array('ok' => 0);
					while(!$done['ok'] && $try < self::MAX_RETRIES) {
						self::Log('DownloadImages', $sTmpFileUrl.' try='.($try+1).' start');
						$done = self::download($sTmpFileUrl, self::NIYAMA_FO_IMPORT_IMAGES_DIR_LOCAL);
						self::Log('DownloadImages', $sTmpFileUrl.' try='.($try+1).' ok='.$done['ok'].' http_code='.$done['info']['http_code'].' errno='.$done['errno']);
						$try++;
					}
				}
			}

		}
		// логируем операцию
		self::Log('DownloadImages', 'FINISH');
		return $bResult;
	}

	private static function GroupsRecursive($arReturn, $arCommonList, $sParentXmlId, $iCurDepthLevel = 0, &$iLeftMargin, &$arTmpCategoryCheck) {
		if($arCommonList[$sParentXmlId]) {
			++$iCurDepthLevel;
			foreach($arCommonList[$sParentXmlId] as $arItem) {
				$iCurKey = count($arReturn);
				$arItem['_DEPTH_LEVEL_'] = $iCurDepthLevel;
				$arItem['_LEFT_MARGIN_'] = ++$iLeftMargin;
				$arItem['_RIGHT_MARGIN_'] = 0;
				$arReturn[$iCurKey] = $arItem;
				if($arCommonList[$arItem['Code']]) {
					$arReturn = self::GroupsRecursive($arReturn, $arCommonList, $arItem['Code'], $iCurDepthLevel, $iLeftMargin, $arTmpCategoryCheck);
				}
				$arReturn[$iCurKey]['_RIGHT_MARGIN_'] = $iLeftMargin + 1;
				$iLeftMargin = $arReturn[$iCurKey]['_RIGHT_MARGIN_'];
				unset($arTmpCategoryCheck[$arItem['Code']]);
			}
		}
		return $arReturn;
	}
}


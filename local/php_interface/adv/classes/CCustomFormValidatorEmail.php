<?
/**
 * Валидатор веб-форм (e-mail с поддержкой кириллицы).
 * Внимание! Для работы необходима библиотека IDNA Convert (http://idnaconv.phlymail.de/)
 * 
 * @author Sergey Leshchenko, 2015
 */


class CCustomFormValidatorEmail {
	public static function GetDescription() {
		return array(
			'NAME' => 'custom_email_validator',
	  		'DESCRIPTION' => 'E-mail',
	  		'TYPES' => array('text'),
			'SETTINGS' => array(__CLASS__, 'GetSettings'),
	  		'CONVERT_TO_DB' => array(__CLASS__, 'ToDB'),
			'CONVERT_FROM_DB' => array(__CLASS__, 'FromDB'),
			'HANDLER' => array(__CLASS__, 'DoValidate')
		);
	}

	public static function GetSettings() {
		return array(
			'ERR_MSG' => array(
				'TITLE' => 'Текст сообщения об ошибке',
				'TYPE' => 'TEXT',
				'DEFAULT' => 'Введен некорректный адрес e-mail',
			),
		);
	}

	public static function ToDB($arParams) {
		return serialize($arParams);
	}

	public static function FromDB($strParams) {
		return unserialize($strParams);
	}

	public static function IsEmail($sEmail = '') {
		$bReturn = false;
		$bReturn = filter_var($sEmail, FILTER_VALIDATE_EMAIL) ? true : false;
		if(!$bReturn && class_exists('idna_convert')) {
			// возможно, содержатся кирилличиские символы
			$obIDN = new idna_convert();
			$sEmail_ = $obIDN->encode($sEmail);
			$bReturn = filter_var($sEmail_, FILTER_VALIDATE_EMAIL) ? true : false;
		}
		return $bReturn;
	}

	public static function DoValidate($arParams, $arQuestion, $arAnswers, $arValues) {
		foreach($arValues as $sValue) {
			if(strlen($sValue)) {
				if(!self::IsEmail($sValue)) {
					$GLOBALS['APPLICATION']->ThrowException($arParams['ERR_MSG']);
					return false;
				}
	  		}
		}
		return true;
	}
}

<?
/**
 * 
 * Тип пользовательского поля Varchar
 * @author Sergey Leshchenko, 2013
 * @updated: 02.12.2013
 * 
 */

class CUFVarchar extends CUserTypeString {
	public function GetUserTypeDescription() {
		return array(
			'USER_TYPE_ID' => 'Varchar',
			'CLASS_NAME' => __CLASS__,
			'DESCRIPTION' => 'Varchar',
			'BASE_TYPE' => 'string',
		);
	}

	public function GetDBColumnType($arUserField) {
		$sReturn = '';
		switch(strtolower($GLOBALS['DB']->type)) {
			case 'mysql':
				$sReturn = 'varchar(255)';
			break;
			case 'oracle':
				$sReturn = 'varchar2(255 char)';
			break;
			case 'mssql':
				$sReturn = 'varchar(255 char)';
			break;
		}
		return $sReturn;
	}
}

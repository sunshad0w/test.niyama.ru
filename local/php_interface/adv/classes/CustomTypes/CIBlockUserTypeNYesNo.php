<?
/**
 * 
 * Юзертайп "Да/Нет" - (базовый тип - N)
 * @author Sergey Leshchenko, 2012
 * 
 */

class CIBlockUserTypeNYesNo {
	public static function GetUserTypeDescription() {
		return array(
			'PROPERTY_TYPE' => 'N',
			'USER_TYPE' => 'YesNo',
			'DESCRIPTION' => 'Да/Нет',
			'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			//'CheckFields' => array(__CLASS__, 'CheckFields'),
			'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
		);
	}

	public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName) {
		if(strlen($arValue['VALUE'])) {
			return $arValue['VALUE'];
		} else {
			return '';
		}
	}

	public static function GetPropertyFieldHtml($arProperty, $arValue, $arHTMLControlName) {
		$sChecked = intval($arValue['VALUE']) == 1 ? ' checked="checked"' : '';
		$sReturn = '';
		$sReturn .= '<input type="hidden" name="'.$arHTMLControlName['VALUE'].'" value="0" />';
		$sReturn .= '<input'.$sChecked.' type="checkbox" name="'.$arHTMLControlName['VALUE'].'" id="'.$arHTMLControlName['VALUE'].'" value="1" />';
		if($arProperty['WITH_DESCRIPTION'] == 'Y') {
			$sReturn .= '<div><input type="text" size="'.$arProperty['COL_COUNT'].'" name="'.$arHTMLControlName['DESCRIPTION'].'" value="'.htmlspecialchars($arValue['DESCRIPTION']).'" /></div>';
		}
		return $sReturn;
	}

	public static function CheckFields($arProperty, $arValue) {
		$arResult = array();
		return $arResult;
	}

	public static function ConvertToDB($arProperty, $arValue) {
		$arValue['VALUE'] = intval($arValue['VALUE']) == 1 ? 1 : 0;
		return $arValue;
	}

	public static function ConvertFromDB($arProperty, $arValue) {
		$arValue['VALUE'] = intval($arValue['VALUE']) == 1 ? 1 : 0;
		return $arValue;
	}
}

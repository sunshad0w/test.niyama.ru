<?php

class CRelatedProducts {

    //описываем поведение пользовательского свойства
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'             => 'E',
            'USER_TYPE'             	=> 'related_products',
            'DESCRIPTION'           	=> 'Похожие товары',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            //'CheckFields' => array(__CLASS__, 'CheckFields'),
            'ConvertToDB'   => array(__CLASS__, 'ConvertToDB'),
            'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
        );
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        $ID = intval($_REQUEST['ID']);
        $rsSkills = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array(
                "IBLOCK_ID" => CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog'),
                "ACTIVE"    => "Y"
            ),
            false,
            false,
            array("ID","NAME")
        );
        //формируем селект с опциями — квалификациями
        $html = '<select name="'.$strHTMLControlName["VALUE"].'">';
        $html .= '<option value="">(выберите блюдо)</option>';
        while ($arSkill = $rsSkills->GetNext()){
            $html .= '<option value="' .$arSkill["ID"]. '"';
            if ($arSkill["ID"] == $value["VALUE"]){
                $html .= 'selected="selected"';
            }
            $html .= '>' .$arSkill["NAME"]. '</option>';
        }
        $html .= '</select>';
        echo $html;

        echo '<label for="sort_in">&nbsp;Сортировка&nbsp;</label>';
        echo '<input id="sort_in" name="'.$strHTMLControlName["DESCRIPTION"].'" value="'.$value["DESCRIPTION"].'"  >';

        echo "<br />";
    }
    public static function ConvertToDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        $arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
        return $arValue;
    }

    public static function ConvertFromDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        $arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
        return $arValue;
    }
    function  GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName) {

    }

} 
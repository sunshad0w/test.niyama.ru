<?
/**
 * 
 * Юзертайп "Привязка к местоположениям модуля Интернет-магазин" - (базовый тип - N)
 * @author Sergey Leshchenko, 2014
 * 
 */

class CIBlockUserTypeSaleLoc {
	protected static $arList = null;

	public static function GetUserTypeDescription() {
		return array(
			'PROPERTY_TYPE' => 'N',
			'USER_TYPE' => 'SaleLoc',
			'DESCRIPTION' => '[SaleLoc] Привязка к местоположениям',
			'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtmlMulty'),
			//'CheckFields' => array(__CLASS__, 'CheckFields'),
			'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
			'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
		);
	}

	protected static function GetListValues() {
		if(is_null($arList)) {
			self::$arList = array();
			CModule::IncludeModule('sale');
			$dbItems = CSaleLocation::GetList(
				array(
					'SORT' => 'ASC',
					'NAME' => 'ASC'
				),
				array(
					'>CITY_ID' => 0
				)
			);
			while($arItem = $dbItems->Fetch()) {
				self::$arList[$arItem['ID']] = array(
					'ID' => $arItem['ID'],
					'OPTION_VALUE' => $arItem['ID'],
					'OPTION_TEXT' => $arItem['CITY_NAME'],
					'LIST_VALUE' => $arItem['CITY_NAME'].' [<a href="/bitrix/admin/sale_location_edit.php?ID='.$arItem['ID'].'">'.$arItem['ID'].'</a>]'
				);
			}
		}
		return self::$arList;
	}

	protected static function GetLocationById($arProperty, $iLocationId) {
		$arLocations = self::GetListValues();
		return isset($arLocations[$iLocationId]) ? $arLocations[$iLocationId] : array();
	}

	public static function GetAdminListViewHTML($arProperty, $arValue, $arHTMLControlName) {
		$sReturn = '';
		if(intval($arValue['VALUE']) > 0) {
			$arItem = self::GetLocationById($arProperty, $arValue['VALUE']);
			if($arItem && $arItem['LIST_VALUE']) {
				$sReturn .= $arItem['LIST_VALUE'];
			} else {
				$sReturn .= '{'.$arValue['VALUE'].'}';
			}
		}
		return $sReturn;
	}

	public static function GetPropertyFieldHtmlMulty($arProperty, $arValue, $arHTMLControlName) {
		$arList = self::GetListValues($arProperty);
		$arTmpVal = array();
		if($arValue) {
			foreach($arValue as $arItem) {
				if($arItem['VALUE']) {
					$arTmpVal[] = $arItem['VALUE'];
				}
			}
		}
		$sReturn = '';
		$sReturn .= '<select multiple="multiple" size="'.($arProperty['MULTIPLE_CNT'] > 0 ? $arProperty['MULTIPLE_CNT'] : 5).'" name="'.$arHTMLControlName['VALUE'].'[]">';
		foreach($arList as $arItem) {
			$sTmpSelected = in_array($arItem['OPTION_VALUE'], $arTmpVal) ? ' selected="selected"' : '';
			$sReturn .= '<option'.$sTmpSelected.' value="'.htmlspecialcharsbx($arItem['OPTION_VALUE']).'">'.htmlspecialcharsbx($arItem['OPTION_TEXT']).'</option>';
		}
		$sReturn .= '</select>';
		if($arProperty['WITH_DESCRIPTION'] == 'Y') {
			//$sReturn .= '<div><input type="text" size="'.$arProperty['COL_COUNT'].'" name="'.$arHTMLControlName['DESCRIPTION'].'" value="'.htmlspecialchars($arValue['DESCRIPTION']).'" /></div>';
		}
		return $sReturn;
	}

	public static function GetPropertyFieldHtml($arProperty, $arValue, $arHTMLControlName) {
		$arList = self::GetListValues($arProperty);

		$sReturn = '';
		$sReturn .= '<select name="'.$arHTMLControlName['VALUE'].'">';
		$sReturn .= '<option value=""> (не установлено) </option>';
		foreach($arList as $arItem) {
			$sTmpSelected = $arItem['OPTION_VALUE'] == $arValue['VALUE'] ? ' selected="selected"' : '';
			$sReturn .= '<option'.$sTmpSelected.' value="'.htmlspecialcharsbx($arItem['OPTION_VALUE']).'">'.htmlspecialcharsbx($arItem['OPTION_TEXT']).'</option>';
		}
		$sReturn .= '</select>';
		if($arProperty['WITH_DESCRIPTION'] == 'Y') {
			$sReturn .= '<div><input type="text" size="'.$arProperty['COL_COUNT'].'" name="'.$arHTMLControlName['DESCRIPTION'].'" value="'.htmlspecialcharsbx($arValue['DESCRIPTION']).'" /></div>';
		}
		return $sReturn;
	}

	public static function CheckFields($arProperty, $arValue) {
		$arResult = array();
		return $arResult;
	}

	public static function ConvertToDB($arProperty, $arValue) {
		$arValue['VALUE'] = intval($arValue['VALUE']);
		$arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
		return $arValue;
	}

	public static function ConvertFromDB($arProperty, $arValue) {
		$arValue['VALUE'] = intval($arValue['VALUE']);
		$arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
		return $arValue;
	}
}

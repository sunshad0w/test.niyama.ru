<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 07.04.2015
 */


class CImportMenuElement {
    public static function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE' => 'E',
            'USER_TYPE'     => 'import_menu_element',
            'DESCRIPTION'   => 'Элемент Импортированного меню',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            //'CheckFields' => array(__CLASS__, 'CheckFields'),
            'ConvertToDB'   => array(__CLASS__, 'ConvertToDB'),
            'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
        );
    }

    public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName) {
        if (!empty($arValue['VALUE'])) {
            $arElement = self::GetElements($arValue['VALUE']);
            $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('catalog-base');
            $arRes = CCustomProject::GetIBlockParentSection($IBLOCK_ID, $arElement['IBLOCK_SECTION_ID']);
            $arCategery = array();
            foreach(array_reverse($arRes) as $item) {
                $arCategery[] = $item['NAME'];
            }
            return $arElement['ID'].' | '.$arElement['ACTIVE'].' | '.$arElement['XML_ID'].' - '.implode(' / ', $arCategery);

        } else {
            return '<b>нет привязки</b>';
        }
    }

    /**
     * GetPropertyFieldHtml
     * @param $arProperty
     * @param $arValue
     * @param $arHTMLControlName
     * @return string
     */
    public static function GetPropertyFieldHtml($arProperty, $arValue, $arHTMLControlName)
    {
        global $APPLICATION;
        if (!empty( $arValue['VALUE'] )) {
            $sReturn = '';
            $arElements = self::GetElements($arValue['VALUE']);
            $sReturn = '<style>
            .CTtable {
            margin:0px;padding:0px;
            width:100%;
            border:1px solid #c4c4c4;
            -moz-border-radius-bottomleft:5px;
            -webkit-border-bottom-left-radius:5px;
            border-bottom-left-radius:5px;
            -moz-border-radius-bottomright:5px;
            -webkit-border-bottom-right-radius:5px;
            border-bottom-right-radius:5px;
            -moz-border-radius-topright:5px;
            -webkit-border-top-right-radius:5px;
            border-top-right-radius:5px;
            -moz-border-radius-topleft:5px;
            -webkit-border-top-left-radius:5px;
            border-top-left-radius:5px;
            }.CTtable table{
                border-collapse: collapse;
                    border-spacing: 0;
                width:100%;
                height:100%;
                margin:0px;padding:0px;
            }.CTtable tr:last-child td:last-child {
                -moz-border-radius-bottomright:5px;
                -webkit-border-bottom-right-radius:5px;
                border-bottom-right-radius:5px;
            }
            .CTtable table tr:first-child td:first-child {
                -moz-border-radius-topleft:5px;
                -webkit-border-top-left-radius:5px;
                border-top-left-radius:5px;
            }
            .CTtable table tr:first-child td:last-child {
                -moz-border-radius-topright:5px;
                -webkit-border-top-right-radius:5px;
                border-top-right-radius:5px;
            }.CTtable tr:last-child td:first-child{
                -moz-border-radius-bottomleft:5px;
                -webkit-border-bottom-left-radius:5px;
                border-bottom-left-radius:5px;
            }.CTtable tr:hover td{
            }
            .CTtable tr:nth-child(odd){ background-color:#d7e7ed; }
            .CTtable tr:nth-child(even)    { background-color:#ffffff; }.CTtable td{
                vertical-align:middle;
            border:1px solid #c4c4c4;
            border-width:0px 1px 1px 0px;
            text-align:left;
            padding:6px;
            font-size:12px;
            font-family:Arial;
            font-weight:normal;
            color:#000000;
            }.CTtable tr:last-child td{
                border-width:0px 1px 0px 0px;
            }.CTtable tr td:last-child{
                border-width:0px 0px 1px 0px;
            }.CTtable tr:last-child td:last-child{
                border-width:0px 0px 0px 0px;
            }
            .CTtable tr:first-child:hover td{
                background:-o-linear-gradient(bottom, #a2cde8 5%, #a2cde8 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #a2cde8), color-stop(1, #a2cde8) );
                background:-moz-linear-gradient( center top, #a2cde8 5%, #a2cde8 100% );
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#a2cde8", endColorstr="#a2cde8");	background: -o-linear-gradient(top,#a2cde8,a2cde8);

                background-color:#a2cde8;
            }
            .CTtable tr:first-child td:first-child{
                border-width:0px 0px 1px 0px;
            }
            .CTtable tr:first-child td:last-child{
                border-width:0px 0px 1px 1px;
            }
            </style>';
            $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode("catalog-base");
            $arRes = CCustomProject::GetIBlockParentSection($IBLOCK_ID,$arElements['IBLOCK_SECTION_ID']);
            $arCategery = array();
            foreach (array_reverse($arRes) as $item){
                $arCategery[] = $item['NAME'];
            }

            $sReturn .= '<div class="CTtable" ><table >';
            $sReturn .= '<tr>';
            $sReturn .= '<td><b> ID</b></td>';
            $sReturn .= '<td><a href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$IBLOCK_ID.'&type=catalog&ID='.$arElements['ID'].'&lang=ru">'.$arElements['ID'].'</a></td>';
            $sReturn .= ' </tr> ';
            $sReturn .= '<td><b>Название</b></td>';
            $sReturn .= '<td>'.$arElements['NAME'].'</td>';
            $sReturn .= ' </tr>';
            $sReturn .= '<tr>';
            $sReturn .= '<td><b>Активен</b></td>';
            $sReturn .= '<td>'.($arElements['ACTIVE'] == 'Y' ? 'Да' : 'Нет').'</td>';
            $sReturn .= ' </tr>';
            $sReturn .= '<tr>';
            $sReturn .= '<td><b>Внешний код</b></td>';
            $sReturn .= '<td>'.$arElements['XML_ID'].'</td>';
            $sReturn .= ' </tr>';
            $sReturn .= ' <tr> ';
            $sReturn .= '<td><b>Категория</b></td>';
            $sReturn .= '<td>'.implode(" / ",$arCategery).' [<a href="/bitrix/admin/iblock_element_admin.php?IBLOCK_ID='.$IBLOCK_ID.'&type=catalog&lang=ru&find_section_section='.$arElements['IBLOCK_SECTION_ID'].'">'.$arElements['IBLOCK_SECTION_ID'].'</a>]</td>';
            $sReturn .= '</tr>';
            foreach ($arElements['PROPERTY'] as $key => $row) {
                $sReturn .= ' <tr> ';
                $sReturn .= '<td ><b>'.$row['NAME'].'</b></td>';
                $sReturn .= '<td>'.$arElements['PROPERTY'][$key]['VALUE'].'</td>';
                $sReturn .= ' </tr> ';
            }
            $sReturn .= '<input type="hidden" name="'.$arHTMLControlName["VALUE"].'" value="'.$arValue['VALUE'].'">';
            $sReturn .= '</table></div>';
            $sReturn .= '<br>';
        }
            $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode("catalog-base","catalog");
                ob_start();
                $strRandControlID = $arHTMLControlName["VALUE"].'_'.mt_rand(0, 10000);
                $control_id = $APPLICATION->IncludeComponent(
                    "bitrix:main.lookup.input",
                    "iblockedit",
                    array(
                        "CONTROL_ID" => preg_replace("/[^a-zA-Z0-9_]/i", "x", $strRandControlID),
                        "INPUT_NAME" => $arHTMLControlName["VALUE"],
                        "INPUT_NAME_STRING" => "inp_".$arHTMLControlName["VALUE"],
                        "INPUT_VALUE_STRING" => htmlspecialcharsback(self::GetValueForAutoComplete($arProperty,$arValue,"","")),
                        "START_TEXT" => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_INVITE'),
                        "MULTIPLE" => $arProperty["MULTIPLE"],
                        "MAX_WIDTH" => 500,
                        "IBLOCK_ID" =>  $IBLOCK_ID,
                        'BAN_SYM' => "",
                        'REP_SYM' => "",
                        'FILTER' => 'Y',
                    ), null, array("HIDE_ICONS" => "Y")
                );

                $APPLICATION->IncludeComponent(
                    'bitrix:main.tree.selector',
                    'iblockedit',
                    array(
                        'INPUT_NAME' => $arHTMLControlName['VALUE'],
                        'ONSELECT' => 'jsMLI_'.$control_id.'.SetValue',
                        'MULTIPLE' => $arProperty['MULTIPLE'],
                        'SHOW_INPUT' => 'N',
                        'SHOW_BUTTON' => 'Y',
                        'GET_FULL_INFO' => 'Y',
                        'START_TEXT' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_LIST_INVITE'),
                        'BUTTON_CAPTION' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_CHOOSE_ELEMENT'),
                        'BUTTON_TITLE' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_CHOOSE_ELEMENT_DESCR'),
                        'NO_SEARCH_RESULT_TEXT' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_NO_SEARCH_RESULT_TEXT'),
                        'IBLOCK_ID' => $IBLOCK_ID,
                        'BAN_SYM' => '',
                        'REP_SYM' => '',
                    ), 
                    null, 
                    array(
                    	"HIDE_ICONS" => "Y"
                    )
                );

                $sReturn .= ob_get_contents();
                ob_end_clean();
            return $sReturn;
    }

    public function GetValueForAutoComplete($arProperty,$arValue,$arBanSym="",$arRepSym="")
    {
        $strResult = '';
        $mxResult = self::GetPropertyValue($arProperty,$arValue);
        if (is_array($mxResult))
        {
            $strResult = htmlspecialcharsbx(str_replace($arBanSym,$arRepSym,$mxResult['~NAME'])).' ['.$mxResult['ID'].']';
        }
        return $strResult;
    }

    protected function GetPropertyValue($arProperty,$arValue)
    {
        $mxResult = false;

        if (0 < intval($arValue['VALUE']))
        {
            $mxResult = self::GetLinkElement($arValue['VALUE'],$arProperty['LINK_IBLOCK_ID']);
            if (is_array($mxResult))
            {
                $mxResult['PROPERTY_ID'] = $arProperty['ID'];
                if (isset($arProperty['PROPERTY_VALUE_ID']))
                {
                    $mxResult['PROPERTY_VALUE_ID'] = $arProperty['PROPERTY_VALUE_ID'];
                }
                else
                {
                    $mxResult['PROPERTY_VALUE_ID'] = false;
                }
            }
        }
        return $mxResult;
    }

    protected function GetLinkElement($intElementID, $intIBlockID)
    {
        static $cache = array();

        $intIBlockID = intval($intIBlockID);
        if (0 >= $intIBlockID)
            $intIBlockID = 0;
        $intElementID = intval($intElementID);
        if (0 >= $intElementID)
            return false;
        if (!isset($cache[$intElementID]))
        {
            $arFilter = array();
            if (0 < $intIBlockID)
                $arFilter['IBLOCK_ID'] = $intIBlockID;
            $arFilter['ID'] = $intElementID;
            $arFilter['SHOW_HISTORY'] = 'Y';
            $rsElements = CIBlockElement::GetList(array(),$arFilter,false,false,array('IBLOCK_ID','ID','NAME'));
            if ($arElement = $rsElements->GetNext())
            {
                $arResult = array(
                    'ID' => $arElement['ID'],
                    'NAME' => $arElement['NAME'],
                    '~NAME' => $arElement['~NAME'],
                    'IBLOCK_ID' => $arElement['IBLOCK_ID'],
                );
                $cache[$intElementID] = $arResult;
            }
            else
            {
                $cache[$intElementID] = false;
            }
        }
        return $cache[$intElementID];
    }

    /**
     * GetElements
     * @param null $iBaseID
     * @return mixed
     */
    function GetElements($iBaseID = null)
    {
        $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode("catalog-base");
        //static $cache = array();
        $IBLOCK_ID = intval($IBLOCK_ID);

//        if(!array_key_exists($IBLOCK_ID, $cache))
//        {
            $cache[$IBLOCK_ID] = array();
            if($IBLOCK_ID > 0)
            {
                $arSelect = array(
                    'ID', 'NAME', 'ACTIVE', 'IBLOCK_SECTION_ID', 'XML_ID',
                    "PROPERTY_SIZE",
                    "PROPERTY_NAME_SHORT",
                    "PROPERTY_UNIT_NAME",
                    "PROPERTY_WEIGHT",
                    "PROPERTY_SORT",
                    "PROPERTY_RECOMMENDED",
                    "PROPERTY_PRICE",
                );
                $arFilter = array(
                    "IBLOCK_ID" => $IBLOCK_ID,
                    "CHECK_PERMISSIONS" => "Y",
                );
                if (!is_null($iBaseID)) {
                    $arFilter['ID'] = $iBaseID;
                }
                $arOrder = array(
                    "NAME" => "ASC",
                    "ID" => "ASC",
                );

                $prArray = array();
                $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID));
                while ($prop_fields = $properties->Fetch())
                {
                    $prArray['PROPERTY_'.$prop_fields["CODE"].'_VALUE'] = $prop_fields["NAME"];
                }
                
                $rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                while($arItem = $rsItems->Fetch()) {
                  foreach ($arItem as $key => $val) {
                      if (array_key_exists($key, $prArray)) {
                          $cache[$IBLOCK_ID]['PROPERTY'][$key] = array(
                          	'NAME' => $prArray[$key],
                          	'VALUE' => $val
                          );
                      } else {
                          $cache[$IBLOCK_ID][$key] = $val;
                      }
                  }
                }
            }
//        }
        return $cache[$IBLOCK_ID];
    }

    /**
     * ConvertToDB
     * @param $arProperty
     * @param $arValue
     * @return mixed
     */
    public static function ConvertToDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        return $arValue;
    }

    /**
     * ConvertFromDB
     * @param $arProperty
     * @param $arValue
     * @return mixed
     */
    public static function ConvertFromDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        return $arValue;
    }

}
<?php

class CBonusBluds {

    //описываем поведение пользовательского свойства
    function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE'             => 'E',
            'USER_TYPE'             	=> 'bonus_bluds',
            'DESCRIPTION'           	=> 'Бонусные блюда',
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),	
			
            //'CheckFields' => array(__CLASS__, 'CheckFields'),
            'ConvertToDB'   => array(__CLASS__, 'ConvertToDB'),
            'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
        );
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {			
		ob_start();
        $ID = intval($_REQUEST['ID']);
        $rsSkills = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array(
                "IBLOCK_ID" => $arProperty['LINK_IBLOCK_ID'],
                "ACTIVE"    => "Y"
            ),
            false,
            false,
            array("ID","NAME","PROPERTY_IMPORT_ELEMENT.PROPERTY_PRICE")
        );
        //формируем селект с опциями — квалификациями
        $html = '<select name="'.$strHTMLControlName["VALUE"].'">';
        $html .= '<option value="">(выберите блюдо)</option>';
        while ($arSkill = $rsSkills->GetNext()){
            $html .= '<option value="' .$arSkill["ID"]. '"';
            if ($arSkill["ID"] == $value["VALUE"]){
                $html .= 'selected="selected"';
            }
            $html .= '>' .$arSkill["NAME"].' ('.$arSkill["PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE"].' руб.)</option>';
        }
        $html .= '</select>';
        echo $html;

        echo '<label for="sort_in">&nbsp;Новая&nbsp;стоимость&nbsp;</label>';
        echo '<input id="sort_in" name="'.$strHTMLControlName["DESCRIPTION"].'" value="'.$value["DESCRIPTION"].'"  >';

        echo "<br />";
		$strResult = ob_get_contents();
		ob_end_clean();
		
		return $strResult;
    }
	
    public static function ConvertToDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        $arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
        return $arValue;
    }

    public static function ConvertFromDB($arProperty, $arValue) {
        $arValue['VALUE'] = intval($arValue['VALUE']);
        $arValue['VALUE'] = $arValue['VALUE'] > 0 ? $arValue['VALUE'] : '';
        return $arValue;
    }
    function  GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName) {

    }

} 
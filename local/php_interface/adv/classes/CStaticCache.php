<?
/**
 * Виртуальный кэш (статические переменные)
 *
 * Предназначен для хранения данных в памяти в течение хита – статический (виртуальный) кэш.
 *
 * @author Sergey Leshchenko, 2012
 * @updated: 31.12.2014
 */

class CStaticCache {
	/**
	 * @ignore
	 */
	private static $arStaticCache = array();
	/**
	 * @ignore
	 */
	private static $arCachePath2Entity = array();

	/**
	 * Сброс всего имеющегося кэша
	 *
	 * @return void
	 */
	public static function FlushAllCache() {
		self::$arStaticCache = array();
	}

	/**
	 * Сброс всего кэша сущности
	 *
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return void
	 */
	public static function FlushEntityCache($sStaticCacheEntity = '-') {
		if(isset(self::$arStaticCache[$sStaticCacheEntity])) {
			unset(self::$arStaticCache[$sStaticCacheEntity]);
		}
	}

	/**
	 * Сброс кэша сущности по заданному ключу
	 *
	 * @param string $sStaticCacheKey ключ сущности кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return void
	 */
	public static function FlushCache($sStaticCacheKey, $sStaticCacheEntity = '-') {
		if(isset(self::$arStaticCache[$sStaticCacheEntity][$sStaticCacheKey])) {
			unset(self::$arStaticCache[$sStaticCacheEntity][$sStaticCacheKey]);
		}
	}

	/**
	 * Установка пустых значений кэша сущности по заданному ключу (инициализация)
	 *
	 * @param string $sStaticCacheKey ключ сущности кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return bool
	 */
	public static function InitCache($sStaticCacheKey, $sStaticCacheEntity = '-') {
		$bReturn = self::IsSetCache($sStaticCacheKey, $sStaticCacheEntity);
		if(!$bReturn) {
			self::SetCacheValue($sStaticCacheKey, '', $sStaticCacheEntity);
		}
		return $bReturn;
	}

	/**
	 * Проверяет, установлен ли кэш для сущности и ключа
	 *
	 * @param string $sStaticCacheKey ключ сущности кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return bool
	 */
	public static function IsSetCache($sStaticCacheKey, $sStaticCacheEntity = '-') {
		return isset(self::$arStaticCache[$sStaticCacheEntity][$sStaticCacheKey]);
	}

	/**
	 * Возвращает хранимое значение кэша сущности по ключу
	 *
	 * @param string $sStaticCacheKey ключ сущности кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return mixed
	 */
	public static function GetCacheValue($sStaticCacheKey, $sStaticCacheEntity = '-') {
		//return strlen($sStaticCacheKey) && isset(self::$arStaticCache[$sStaticCacheKey]) ? self::$arStaticCache[$sStaticCacheKey] : false;
		// ! именно так и нужно возвращать результат !
		return self::$arStaticCache[$sStaticCacheEntity][$sStaticCacheKey];
	}

	/**
	 * Устанавливает значение кэша для ключа сущности
	 *
	 * @param string $sStaticCacheKey ключ сущности кэша
	 * @param mixed $mValue значение кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @param int $iMaxEntityCacheSize максимальное количество значений кэша в пределах сущности. 
	 * Если размер кэша достигнул заданного лимита, то значения будут вытесняться по принципу FIFO. Лимит не проверяется, если задан 0.
	 * @return void
	 */
	public static function SetCacheValue($sStaticCacheKey, $mValue, $sStaticCacheEntity = '-', $iMaxEntityCacheSize = 0) {
		$iMaxEntityCacheSize = intval($iMaxEntityCacheSize);
		self::$arStaticCache[$sStaticCacheEntity][$sStaticCacheKey] = $mValue;
		if($iMaxEntityCacheSize > 0 && count(self::$arStaticCache[$sStaticCacheEntity]) > $iMaxEntityCacheSize) {
			// $sStaticCacheKey - теоретически может быть числовым значением, поэтому использовать array_shift() нельзя
			//array_shift(self::$arStaticCache[$sStaticCacheEntity]);
			self::$arStaticCache[$sStaticCacheEntity] = array_slice(self::$arStaticCache[$sStaticCacheEntity], 1, null, true);
		}
	}

	/**
	 * Добавляет сущности кэша связь с заданным путем
	 *
	 * @param string $sCachePath путь, который нужно привязать к сущности кэша
	 * @param string $sStaticCacheEntity код сущности кэша
	 * @return bool
	 */
	public static function AddCachePath2Entity($sCachePath, $sStaticCacheEntity) {
		if(is_scalar($sCachePath) && is_scalar($sStaticCacheEntity)) {
			self::$arCachePath2Entity[$sCachePath][] = $sStaticCacheEntity;
			return true;
		}
		return false;
	}

	/**
	 * Возвращает сущности кэша, связанные с заданным путем
	 *
	 * @param string $sCachePath путь, для которого нужно вернуть связи с сущностями кэша. Если false, то возвращается массив всех связей
	 * @return array
	 */
	public static function GetCachePath2Entity($sCachePath = false) {
		if($sCachePath === false) {
			return array_unique(self::$arCachePath2Entity);
		} elseif(is_scalar($sCachePath) && isset(self::$arCachePath2Entity[$sCachePath])) {
			return array_unique(self::$arCachePath2Entity[$sCachePath]);
		}
		return array();
	}

	/**
	 * Сбрасывает связи сущности кэша с путями
	 *
	 * @param string $sCachePath путь, для которого нужно очистить связи с сущностями кэша. Если false, то сбрасываются все связи
	 * @return void
	 */
	public static function ClearCachePath2Entity($sCachePath = false) {
		if($sCachePath === false) {
			self::$arCachePath2Entity = array();
		} elseif(is_scalar($sCachePath)) {
			self::$arCachePath2Entity[$sCachePath] = array();
		}
	}

	/**
	 * Сбрасывает весь кэш сущности по заданному пути
	 *
	 * @param string $sCachePath путь, для которого нужно очистить значения всех привязанных сущностей кэша
	 * @return bool
	 */
	public static function FlushEntityCacheByCachePath($sCachePath) {
		$bReturn = false;
		if(is_scalar($sCachePath)) {
			$arEntities = self::GetCachePath2Entity($sCachePath);
			if($arEntities) {
				foreach($arEntities as $sStaticCacheEntity) {
					self::FlushEntityCache($sStaticCacheEntity);
				}
				$bReturn = true;
			}
		}
		return $bReturn;
	}

}

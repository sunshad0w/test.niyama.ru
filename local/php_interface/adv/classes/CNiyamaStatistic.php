<?php

class CNiyamaStatistic {

    /**
     * Добавляем в Фасетный поиск
     * FasStatisticAdd
     * @param $arTmpSection
     * @return bool
     */
    public static function FasStatisticAdd($arTmpSection)
    {
        $iStatFasId = CProjectUtils::GetIBlockIdByCode('fas-search', 'statistic');
        $sFasValue = implode("|", $arTmpSection);
        $arNames = array();
        $arAllElements = CNiyamaCatalog::getAllMenuElements(1);
        foreach ($arTmpSection as $row) {
            if (isset($arAllElements[$row])) {
                $arNames[] = $arAllElements[$row];
            }
        }
        $rsItems = CIBlockElement::GetList(
            array('ID', "ASC"),
            array(
                "PREVIEW_TEXT" => $sFasValue,
                "IBLOCK_ID" => $iStatFasId
            ), false, false, array("ID", "PROPERTY_FAS_COUNT"));
        if (!$arItem = $rsItems->NavNext()) {

            $sStatNames = implode(", ", $arNames);
            if (!empty($sStatNames)) {
                // Если нет то добавляем
                $arFields = array(
                    "ACTIVE" => "Y",
                    "IBLOCK_ID" => $iStatFasId,
                    "NAME" => "Запрос",
                    "PREVIEW_TEXT" => $sFasValue,
                    "DETAIL_TEXT" => $sStatNames,
                    "PROPERTY_VALUES" => array(
                        "FAS_COUNT" => 1,
                    )
                );
                $oElement = new CIBlockElement();
                $idElement = $oElement->Add($arFields, false, false, true);
            }

        } else {
            $sStatNames = implode(", ", $arNames);
            if (!empty($sStatNames)) {
                $arFields = array(
                    "ACTIVE" => "Y",
                    "PREVIEW_TEXT" => $sFasValue,
                    "DETAIL_TEXT" => $sStatNames,
                    "PROPERTY_VALUES" => array(
                        "FAS_COUNT" => intval($arItem['PROPERTY_FAS_COUNT_VALUE']) + 1,
                    )
                );
                $oElement = new CIBlockElement();
                $oElement->Update($arItem['ID'], $arFields);
            }
        }
        return true;
    }


} 
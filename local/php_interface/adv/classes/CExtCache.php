<?
/**
 * Расширенное кэширование
 *
 * Класс-хелпер для кэширования. Поддерживает перезапись кэша, упрощает работу с управляемым (тегированным $GLOBALS['CACHE_MANAGER']) кэшем и статическим кэшем (CStaticCache). 
 * Основное предназначение – для использования в методах проектов, которые предполагают многократный вызов на странице.
 * Класс зависит от CStaticCache.
 *
 * @author Sergey Leshchenko, 2012
 * @updated: 07.04.2015
 */

/**
 * Время жизни кэша по умолчанию
 */
if(!defined('METHODS_DEFAULT_CACHE_TIME')) {
	define('METHODS_DEFAULT_CACHE_TIME', 3600);
}

class CExtCache {
	private $iDefaultCacheTime = METHODS_DEFAULT_CACHE_TIME;
	private $obCache = null;
	private $arCacheIdParams = array();
	private $sCachePath = '/extcache';
	private $bTagCacheEnabled = false;
	private $bTagCacheStarted = false;
	private $arCacheTags = array();
	private $bUseStaticCache = true;
	private $bCacheInited = false;
	private $bOnStartDataCacheInit = false;
	private $bCacheRealInited = false;
	private $bDataCacheStarted = false;
	private $_iCacheTime = 0;
	private $_sCacheType = 'A';
	private $sCacheId = '';
	private $sStaticCacheEntity = '';
	private $bInitCacheReturn = false;
	private $iStaticCacheEntityMaxSize = 0;
	private $bCachePath2StaticCacheAdded = false;
	private $bForceStaticCacheUsing = false;
	private $iTTL = 0;
	// Важно! Нельзя включать этот флаг для кэширования шаблонов или когда что-либо выводится в кэшируемой области!
	private $bForceStartDataCache = true;

	/**
	 * Конструктор
	 *
	 * @param array $arCacheIdParams массив идентификационных параметров кэша, по ним будет генерироваться уникальный идентификатор (ключ) кэша
	 * @param string $sCachePath папка, в которой должен храниться кэш относительно /bitrix/cache/, если не задан, то будет использоваться путь по умолчанию
	 * @param array $arAddCacheTags массив тегов управляемого кэша, если пустой, то управляемый кэш использоваться не будет
	 * @param bool $bUseStaticCache флаг использования статического (CStaticCache) кэша, если false, то результат в статическом кэше сохраняться не будет
	 * @param int $iStaticCacheEntityMaxSize максимальное количество значений кэша в пределах сущности статического кэша
	 * @param bool $bForceStartDataCache флаг принудительного включения кэширования методом CPhpCache::StartDataCache. Нельзя включать этот флаг для кэширования шаблонов или когда что-либо выводится в кэшируемой области - возможно дублирование выводимого контента!
	 * @return void
	 */
	public function __construct($arCacheIdParams = array(), $sCachePath = '', $arAddCacheTags = array(), $bUseStaticCache = true, $iStaticCacheEntityMaxSize = 0, $bForceStartDataCache = true) {
		// путь для сохранения кэша (должен быть определен обязательно)
		$sCachePath = $sCachePath === false ? '' : trim($sCachePath);
		if(!strlen($sCachePath)) {
			$sCachePath = $this->sCachePath;
			//$sCachePath = $GLOBALS['CACHE_MANAGER']->GetCompCachePath($this->sCachePath);
		}
		$sCachePath = ltrim($sCachePath, '/');// для memcache
		$this->sCachePath = rtrim(trim($sCachePath), '/');
		// теги для тегированного кэша
		$this->AddCacheTags($arAddCacheTags);
		// параметры идентификации кэша
		$this->arCacheIdParams = $arCacheIdParams;
		// сохранять ли значения дополнительно в виртуальном кэше
		$this->bUseStaticCache = $bUseStaticCache ? true : false;
		$this->iStaticCacheEntityMaxSize = intval($iStaticCacheEntityMaxSize);

		// флаг принудительного включения кэширования методом StartDataCache (т.е. StartDataCache будет всегда true)
		// Важно! Нельзя включать этот флаг для кэширования шаблонов или когда что-либо выводится в кэшируемой области - возможно дублирование выводимого контента!
		if($bForceStartDataCache) {
			$this->EnableForceCaching();
		} else {
			$this->DisableForceCaching();
		}
	}

	/**
	 * Инициализация кэша
	 *
	 * @param int $iCacheTime время жизни кэша, сек., если не задано или 0, то будет использоваться время кэширования по умолчанию
	 * @param string $sCacheType тип кэширования: A – автокэширование, Y – обычное кэширование, N – отключить кэширование
	 * @return bool
	 */
	public function InitCache($iCacheTime = 0, $sCacheType = 'A') {
		$this->bCacheInited = true;
		$this->_iCacheTime = $iCacheTime;
		$this->_sCacheType = $sCacheType;
		if(!$this->bOnStartDataCacheInit) {
			// если выбран режим использования виртуального (статического) кэша, то первым делом проверим, нет ли в нем готовых данных
			if($this->IsStaticCacheEnabled()) {
				$sStaticCacheKey = $this->GetCacheId();
				$sStaticCacheEntity = $this->GetStaticCacheEntity();
				if(CStaticCache::IsSetCache($sStaticCacheKey, $sStaticCacheEntity)) {
					// данные уже есть, сообщаем об этом
					return true;
				}
			}
		}

		return $this->_initCache();
	}

	private function _initCache() {
		if($this->bCacheInited && !$this->bCacheRealInited) {
			$this->bCacheRealInited = true;

			$iCacheTime = $this->_iCacheTime;
			$sCacheType = $this->_sCacheType;

			$iCacheTime = $this->GetCacheTime($iCacheTime, $sCacheType);
			$sCacheId = $this->GetCacheId();
			$sCachePath = $this->GetCachePath();
			$obCache = $this->GetCacheObject();
			$this->iTTL = $iCacheTime;
			$this->bInitCacheReturn = $obCache->InitCache($iCacheTime, $sCacheId, $sCachePath);
		}
		return $this->bInitCacheReturn;
	}

	/**
	 * Возвращает данные, сохраненные в кэше
	 *
	 * @return mixed
	 */
	public function GetVars() {
		// если выбран режим виртуального кэша, то первым делом проверим его, нет ли в нем сохраненных данных
		if($this->IsStaticCacheEnabled()) {
			$sStaticCacheKey = $this->GetCacheId();
			$sStaticCacheEntity = $this->GetStaticCacheEntity();
			if(CStaticCache::IsSetCache($sStaticCacheKey, $sStaticCacheEntity)) {
				// данные уже есть, сразу отдаем их
				return CStaticCache::GetCacheValue($sStaticCacheKey, $sStaticCacheEntity);
			}
		}
		$obCache = $this->GetCacheObject();
		$mCacheData = $obCache->GetVars();
		if($this->IsStaticCacheEnabled()) {
			CStaticCache::SetCacheValue($sStaticCacheKey, $mCacheData, $sStaticCacheEntity, $this->iStaticCacheEntityMaxSize);
			$this->AddCachePath2StaticCacheEntity($sStaticCacheEntity);
		}
		return $mCacheData;
	}

	/**
	 * Стартует процедуру кэширования
	 *
	 * @param int $iCacheTime время жизни кэша, сек. Значение будет использоваться только в том случае, если предварительно не был вызван метод InitCache(), при этом  если значение 0 или не задано, то будет использоваться время кэширования по умолчанию
	 * @param string $sCacheType тип кэширования. Значение будет использоваться только в том случае, если предварительно не был вызван метод InitCache(), при этом допустимые значения: A – автокэширование, Y – обычное кэширование, N – отключить кэширование
	 * @return bool
	 */
	public function StartDataCache($iCacheTime = 0, $sCacheType = 'A') {
		if($this->IsDataCacheStarted()) {
			return false;
		}

		if(!$this->bCacheInited) {
			$this->bOnStartDataCacheInit = true;
			$this->InitCache($iCacheTime, $sCacheType);
		}

		$bCacheStarted = false;

		if($this->iTTL > 0) {
			// включим флаг для статик кэша
			$this->SetDataCacheStarted();

			$obCache = $this->GetCacheObject();

			$bForceCaching = $this->GetForceCachingState();
			//
			// Внимание! Костыль! 
			// убрать, когда появится нормальная возможность перезаписи кэша
			// (см. Cache::ShouldClearCache() + Cache::InitCache())
			//
			// >>>
			if($bForceCaching) {
				$sSessClearCacheOrig = null;
				if(isset($_SESSION['SESS_CLEAR_CACHE'])) {
					$sSessClearCacheOrig = $_SESSION['SESS_CLEAR_CACHE'];
				}
				$_SESSION['SESS_CLEAR_CACHE'] = 'Y';
			}
			// <<<

			$bCacheStarted = $obCache->StartDataCache();

			// продолжение костыля >>>
			if($bForceCaching) {
				if(is_null($sSessClearCacheOrig)) {
					unset($_SESSION['SESS_CLEAR_CACHE']);
				} else {
					$_SESSION['SESS_CLEAR_CACHE'] = $sSessClearCacheOrig;
				}
			}
			// подстраховка, если фокус с $_SESSION['SESS_CLEAR_CACHE'] не пройдет
			if($bForceCaching && !$bCacheStarted) {
				// очистим кэш и пробуем еще раз стартануть
				$this->CleanCache();
				// !!! здесь кроется причина дублирования выводимого контента (если кэш используется для этих целей) !!!
				$bCacheStarted = $obCache->StartDataCache();
			}
			// <<<

			if($bCacheStarted) {
				// если разрешен тегированный кэш, то запустим его буферизацию
				if($this->IsTagCacheEnabled()) {
					$this->StartTagCache();
				}
			}
		}
		return $bCacheStarted;
	}

	protected function StartTagCache() {
		$sCachePath = $this->GetCachePath();
		if(strlen($sCachePath)) {
			if(defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER'])) {
				$GLOBALS['CACHE_MANAGER']->StartTagCache($sCachePath);
				$this->SetTagCacheStarted();
			}
		}
	}

	/**
	 * Сохраняет результат в кэш
	 *
	 * @param mixed $mCacheData сохраняемые данные в кэше
	 * @param array $arAddCacheTags массив тегов управляемого кэша, которые необходимо связать с данным кэшем
	 * @return bool
	 */
	public function EndDataCache($mCacheData, $arAddCacheTags = array()) {
		$bReturn = false;
		if($this->IsDataCacheStarted()) {
			$bReturn = true;
			$this->SetDataCacheEnded();

			if(!$this->IsTagCacheStarted() && $this->IsTagCacheEnabled()) {
				$this->StartTagCache();
			}
			if($this->IsTagCacheStarted()) {
				if(!empty($arAddCacheTags)) {
					$this->AddCacheTags($arAddCacheTags);
				}
				$arCacheTags = $this->GetCacheTags();
				if(!empty($arCacheTags)) {
					foreach($arCacheTags as $sCacheTag) {
						$GLOBALS['CACHE_MANAGER']->RegisterTag($sCacheTag);
					}
				}
				$GLOBALS['CACHE_MANAGER']->EndTagCache();
				$this->SetTagCacheEnded();
			}

			$obCache = $this->GetCacheObject();
			$obCache->EndDataCache($mCacheData);
		}

		if($this->IsStaticCacheEnabled() && ($this->GetForceStaticCachingUsing() || $this->IsDataCacheStarted())) {
			$sStaticCacheKey = $this->GetCacheId();
			$sStaticCacheEntity = $this->GetStaticCacheEntity();
			CStaticCache::SetCacheValue($sStaticCacheKey, $mCacheData, $sStaticCacheEntity, $this->iStaticCacheEntityMaxSize);
			$this->AddCachePath2StaticCacheEntity($sStaticCacheEntity);
		}

		return $bReturn;
	}

	/**
	 * Отменяет кэширование результата
	 *
	 * @return bool
	 */
	public function AbortDataCache() {
		if(!$this->IsDataCacheStarted()) {
			return false;
		}
		$this->SetDataCacheEnded();

		if($this->IsTagCacheStarted()) {
			$GLOBALS['CACHE_MANAGER']->AbortTagCache();
			$this->SetTagCacheEnded();
		}

		$obCache = $this->GetCacheObject();
		$obCache->AbortDataCache();

		return true;
	}

	/**
	 * Возвращает используемый экземпляр кэша CPHPCache()
	 *
	 * @return CPHPCache
	 */
	public function GetCacheObject() {
		if(!$this->obCache) {
			$this->obCache = new CPHPCache();
		}
		return $this->obCache;
	}

	/**
	 * Проверяет, разрешено ли статическое кэширование результата
	 *
	 * @return bool
	 */
	public function IsStaticCacheEnabled() {
		return $this->bUseStaticCache;
	}

	/**
	 * Устанавливает время жизни кэша по умолчанию
	 *
	 * @param int $iValue время жизни кэша, которое необходимо установить как значение по умолчанию, сек
	 * @return void
	 */
	public function SetDefaultCacheTime($iValue) {
		$iValue = intval($iValue);
		$this->iDefaultCacheTime = $iValue > 0 ? $iValue : 0;
	}

	/**
	 * Возвращает время жизни кэша по умолчанию
	 *
	 * @return int
	 */
	public function GetDefaultCacheTime() {
		return $this->iDefaultCacheTime;
	}

	/**
	 * Возвращает путь к кэшу относительно /bitrix/cache/
	 *
	 * @return string
	 */
	public function GetCachePath() {
		return $this->sCachePath;
	}

	/**
	 * Возвращает время жизни кэша с учетом типа кэширования
	 *
	 * @param int $iCacheTime время жизни кэша, сек., если значение 0 или не задано, то будет использоваться время кэширования по умолчанию
	 * @param string $sCacheType тип кэширования: A – автокэширование, Y – обычное кэширование, N – отключить кэширование
	 * @return int
	 */
	public function GetCacheTime($iCacheTime = 0, $sCacheType = 'A') {
		$iCacheTime = intval($iCacheTime);
		$iCacheTime = $iCacheTime > 0 ? $iCacheTime : $this->GetDefaultCacheTime();
		$sCacheType = $sCacheType != 'Y' && $sCacheType != 'N' ? 'A' : $sCacheType;
		if($sCacheType == 'N' || ($sCacheType == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
			$iCacheTime = 0;
		}
		return $iCacheTime;
	}

	/**
	 * Возвращает идентификатор (ключ) кэша
	 *
	 * @return string
	 */
	public function GetCacheId() {
		if(!$this->sCacheId) {
			$arCacheParams = array(
				$this->sCachePath,
				$this->arCacheIdParams,
				// время жизни кэша не должно подмешиваться в кэш-идентификатор
				//$this->_iCacheTime,
				//$this->_sCacheType,
			);
			$this->sCacheId = md5(serialize($arCacheParams));
		}
		return $this->sCacheId;
	}

	/**
	 * Возвращает установленный код сущности статического кэша
	 *
	 * @return string
	 */
	public function GetStaticCacheEntity() {
		if(!$this->sStaticCacheEntity) {
			$arCacheParams = array(
				$this->sCachePath
			);
			$this->sStaticCacheEntity = md5(serialize($arCacheParams));
		}
		return $this->sStaticCacheEntity;
	}

	/**
	 * Добавление тегов для управляемого кэша
	 *
	 * @param array $arAddCacheTags массив тегов управляемого кэша, которые необходимо связать с данным кэшем
	 * @return void
	 */
	public function AddCacheTags($arAddCacheTags = array()) {
		if(is_array($arAddCacheTags) && !empty($arAddCacheTags)) {
			$arAddCacheTags = array_unique($arAddCacheTags);
			$this->arCacheTags = array_merge($arAddCacheTags, $this->arCacheTags);
			if(!empty($this->arCacheTags)) {
				$this->bTagCacheEnabled = true;
			}
		}
	}

	/**
	 * Возвращает все заданные теги для управляемого кэша
	 *
	 * @return array
	 */
	public function GetCacheTags() {
		return $this->arCacheTags;
	}

	private function SetTagCacheStarted() {
		$this->bTagCacheStarted = true;
	}

	private function SetTagCacheEnded() {
		$this->bTagCacheStarted = false;
	}

	/**
	 * Проверяет, включено ли тегированное (управляемое) кэширование
	 *
	 * @return bool
	 */
	public function IsTagCacheEnabled() {
		return $this->bTagCacheEnabled;
	}

	/**
	 * Проверяет, начато ли управляемое кэширование
	 *
	 * @return bool
	 */
	public function IsTagCacheStarted() {
		return $this->bTagCacheStarted;
	}

	private function SetDataCacheStarted() {
		$this->bDataCacheStarted = true;
	}

	private function SetDataCacheEnded() {
		$this->bDataCacheStarted = false;
	}

	/**
	 * Проверяет, начата ли процедура кэширования
	 *
	 * @return bool
	 */
	public function IsDataCacheStarted() {
		return $this->bDataCacheStarted;
	}

	private function AddCachePath2StaticCacheEntity($sStaticCacheEntity = '') {
		if(!$this->bCachePath2StaticCacheAdded) {
			$sStaticCacheEntity = $sStaticCacheEntity ? $sStaticCacheEntity : $this->GetStaticCacheEntity();
			CStaticCache::AddCachePath2Entity($this->GetCachePath(), $sStaticCacheEntity);
			$this->bCachePath2StaticCacheAdded = true;
		}
	}

	/**
	 * Сбрасывает кэш
	 *
	 * @return void
	 */
	public function CleanCache() {
		$sCacheId = $this->GetCacheId();
		$sCachePath = $this->GetCachePath();
		$obCache = $this->GetCacheObject();
		$obCache->Clean($sCacheId, $sCachePath);
	}

	/**
	 * Отключает принудительный старт кэширования методом StartDataCache
	 *
	 * @return void
	 */
	public function DisableForceCaching() {
		$this->bForceStartDataCache = false;
	}

	/**
	 * Включает принудительный старт кэширования методом StartDataCache
	 *
	 * @return void
	 */
	public function EnableForceCaching() {
		$this->bForceStartDataCache = true;
	}

	/**
	 * Возвращает состояние флага принудительного старта кэширования методом StartDataCache
	 *
	 * @return void
	 */
	public function GetForceCachingState() {
		return $this->bForceStartDataCache;
	}

	/**
	 * Включает режим использования статического кэша, даже когда общий кэш отключен
	 *
	 * @return void
	 */
	public function SetForceStaticCachingUsing() {
		$this->bForceStaticCacheUsing = true;
	}

	/**
	 * Возвращает true, если включен режим использования статического кэша, даже когда общий кэш отключен
	 *
	 * @return bool
	 */
	public function GetForceStaticCachingUsing() {
		return $this->bForceStaticCacheUsing;
	}


}

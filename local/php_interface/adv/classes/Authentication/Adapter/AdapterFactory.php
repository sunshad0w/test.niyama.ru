<?php
/**
 * @author: Kazim Bayramukov
 * @email: bayramukov@adv.ru
 * @copyright Copyright (c) 2014, ADV/web-engineering co.
 */

namespace ADV\Authentication\Adapter;


class AdapterFactory {

    /**
     * @param $site
     * @param $params
     * @throws \Exception
     * @return AbstractAdapter|Facebook|GooglePlus|MailRu|Vkontakte|Odnoklassniki|Yandex
     */
    public static function createUserObject($site, $params) {
        $classname = "ADV\\Authentication\\Adapter\\{$site}";

        if (class_exists($classname)) {
            return new $classname($params);
        } else {
            throw new \Exception("Class {$classname} not found");
        }
    }
}
<?php
/**
 * @author: Kazim Bayramukov
 * @email: bayramukov@adv.ru
 * @copyright Copyright (c) 2014, ADV/web-engineering co.
 */

namespace ADV\Authentication\Adapter;


class Facebook extends AbstractAdapter {

    protected $addCredentials = array(
        'scope' => 'email',
        'display' => 'popup',
    );

    protected $loginDialogUrl = 'https://www.facebook.com/dialog/oauth';

    protected $loginDialogParams = array(
        'scope' => '%s',
        'display' => '%s',
        'client_id' => '%s',
        'redirect_uri' => '%s',
        'response_type' => '%s'
    );

    public $loginDialogWidth  = 656;
    public $loginDialogHeight = 378;

    protected $requestTokenUrl = 'https://graph.facebook.com/oauth/access_token';
    protected $requestTokenMethod = HTTP_METH_GET;
    protected $requestTokenResponseType = self::TOKEN_RESPONSE_TYPE_QUERY;
    protected $requestTokenParams = array(
        'code' => '%s',
        'client_id' => '%s',
        'redirect_uri' => '%s',
        'client_secret' => '%s',
    );


    protected $getUserInfoUrl = 'https://graph.facebook.com/me';
    protected $getUserInfoParams = array(
        'access_token' => 'access_token'
    );

    protected function parserUserInfo($arInfo) {

        $arFields = array(
            "UF_UID"      => 'id',
            "UF_IDENTITY" => 'link',
            "LOGIN"       => 'email',
            "EMAIL"       => 'email',
            "NAME"        => 'first_name',
            "LAST_NAME"   => 'last_name',
            "PERSONAL_GENDER"   => 'gender'
        );

        $arSource = (array)$arInfo;

        foreach ($arFields as $k => &$v) {
            $v = isset($arSource[$v]) ? $arSource[$v]: null;

            if($v === null) {
                unset($arFields[$k]);
                continue;
            }

            if($k == 'PERSONAL_GENDER') {
                $v = $v == 'male' ? 'M' : 'F';
            }
        }
		$class=new \ReflectionClass(__CLASS__);
        $arFields['UF_SERVICES'] = strtolower($class->getShortName());

        return $arFields;


    }

} 
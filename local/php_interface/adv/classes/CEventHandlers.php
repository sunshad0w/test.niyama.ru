<?
/**
 * 
 * Обработчики событий
 *
 */
use \Bitrix\Main\Entity as Entity;

class CEventHandlers {


/**
*OnAfterOrderComplite

*/

	/**
	 * OnBeforeProlog
	 */
	public static function OnBeforePrologHandler() {
		//
		// админам включаем вывод ошибок на экран
		//
		if($GLOBALS['USER']->IsAdmin()) {
			ini_set('display_errors', '1');
		}
	}



	/**
	 * OnProlog
	 */
	public static function OnPrologHandler() {
		//
		// кастомное отключение публички для сайтов
		//
		if(defined('CUSTOM_STOP_SITE') && strlen(CUSTOM_STOP_SITE)) {
			if(!$GLOBALS['USER']->CanDoOperation('edit_other_settings')) {
				$arSites = explode(',', CUSTOM_STOP_SITE);
				foreach($arSites as $sSiteId) {
					if(trim($sSiteId) == SITE_ID) {
						$sDir = $_SERVER['DOCUMENT_ROOT'].BX_PERSONAL_ROOT.'/php_interface/';
						if(file_exists($sDir.LANG.'/site_closed.php')) {
							include($sDir.LANG.'/site_closed.php');
						} elseif(file_exists($sDir.'/include/site_closed.php')) {
							include($sDir.'/include/site_closed.php');
						} else {
							include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/site_closed.php');
						}
						die();
					}
				}
			}
		}
	}

	/**
	 * OnBeforeResizeImage
	 * @param $arSourceFile
	 * @param $arResizeParams
	 * @param $mCallbackData
	 * @param $bNeedResize
	 * @param $sSourceImageFilePath
	 * @param $sCacheImageFileTmp
	 */
	public static function OnBeforeResizeImageHandler($arSourceFile, $arResizeParams, &$mCallbackData, &$bNeedResize, &$sSourceImageFilePath, &$sCacheImageFileTmp) {
		//
		// делаем так, чтобы ресайз изображений работал не только для файлов, находящихся в папке upload
		//
		if(isset($arSourceFile['SRC']) && strlen($arSourceFile['SRC'])) {
			$sSourceImageFilePath = $_SERVER['DOCUMENT_ROOT'].$arSourceFile['SRC'];
		}
	}

	/**
	 * OnEndBufferContent
	 * @param $sBufferContent
	 */
	public static function OnEndBufferContentHandler(&$sBufferContent) {
		//
		// Перенос всех js-скриптов вниз тела страницы
		//
		if(defined('JS_MOVE_FOOTER_FLAG') && JS_MOVE_FOOTER_FLAG) {
			$arData = preg_split('/(<script.*?>.*?<\\/script.*?>)/is', $sBufferContent, -1, PREG_SPLIT_DELIM_CAPTURE);
			$iDataSize = count($arData);
			if($iDataSize < 2) {
				return;
			}
			$arJs = array();
			for($iIdx = 1; $iIdx < $iDataSize; $iIdx += 2) {
				$arJs[] = $arData[$iIdx];
				$arData[$iIdx] = '';
			}
			$sBufferContent = implode('', $arData);
			$iBodyPos = strrpos($sBufferContent, '</body>');
			if($iBodyPos) {
				$sBufferContent = substr($sBufferContent, 0, $iBodyPos).implode('', $arJs).substr($sBufferContent, $iBodyPos);
			} else {
				$sBufferContent .= implode('', $arJs);
			}
		}
	}

	public static function OnEventLogGetAuditTypesHandler() {
		//
		// Кастомные типы для журнала ошибок
		//
		$arAuditTypes = array(
			'NIYAMA_FO_SYNC' => '[NIYAMA_FO_SYNC] Cинхронизация с FO',
			'NIYAMA_PDS_SYNC' => '[NIYAMA_PDS_SYNC] Cинхронизация с PDS'
		);
		return $arAuditTypes;
	}

	/**
	 * OnBuildGlobalMenu
	 * @param $arGlobalMenu
	 * @param $arModuleMenu
	 * @return array
	 */
	public static function OnBuildGlobalMenuHandler(&$arGlobalMenu, &$arModuleMenu) {
		//
		// Кастомное меню для админки
		//
		include_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/adv/admin/menu.php');
		return array();
	}

	//
	// -- main module handlers --
	//

	/**
	 * OnAdminContextMenuShowHandler
	 */
	public static function OnAdminContextMenuShowHandler(&$arContextItems) {
		//
		// Кастомные кнопки в админке
		//
		include_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/adv/admin/context_menu.php');
	}

	/**
	 * OnBeforeUserRegisterHandler (обработчик нужен, чтобы при регистрации не вознакала ошибка отсутствия логина)
	 * @param $arFields
	 */
	public static function OnBeforeUserRegisterHandler(&$arFields) {
		// Устанавливаем Email в качестве логина
		$arFields['LOGIN'] = $arFields['EMAIL'];
	}

	/**
	 * OnBeforeUserAddHandler
	 * @param $arFields
	 */
	public static function OnBeforeUserAddHandler(&$arFields) {
		// Устанавливаем Email в качестве логина
		$arFields['LOGIN'] = $arFields['EMAIL'];

		$arFields['UF_MARKER'] = 'E';
	}


	protected static $arActivateUsers = array();
	/**
	 * OnBeforeUserUpdateHandler
	 * @param $arFields
	 * @return mixed
	 */
	public static function OnBeforeUserUpdateHandler(&$arFields) {
		// Устанавливаем Email в качестве логина
		if($arFields['LOGIN']) {
			unset($arFields['LOGIN']);
		}
		if($arFields['EMAIL']) {
			$arFields['LOGIN'] = $arFields['EMAIL'];
		}

		if($arFields['ID'] && self::$arActivateUsers[$arFields['ID']]) {
			unset(self::$arActivateUsers[$arFields['ID']]);
			$arFields['ACTIVE'] = 'Y';
			$arFields['XML_ID'] = '';
			$arFields['_SKIP_FO_SYNC_'] = 'Y';
		}
		return true;
	}

	/**
	 * OnAfterUserUpdateHandler
	 * @param $arFields
	 */
	public static function OnAfterUserUpdateHandler(&$arFields) {
		// _SKIP_FO_SYNC_ = Y - флаг, который отключает синхронизацию пользователя с системой FastOperator
		if($arFields['_SKIP_FO_SYNC_'] && $arFields['_SKIP_FO_SYNC_'] == 'Y') {
			return;
		}

		CUsersData::SyncUserById($arFields['ID']);
	}

	/**
	 * OnBeforeUserDeleteHandler
	 * @param $user_id
	 */
	public function OnBeforeUserDeleteHandler($user_id) {
		// Проверяем на наличие связей с соцсетями
		$obLink = UsersocTable::getList(
			array(
				'filter' => array(
					'=UF_USER_ID' => $user_id
				)
			)
		);
		while($arLink = $obLink->Fetch()) {
			UsersocTable::delete($arLink['ID']);
		}
	}

	/**
	 * OnBeforeUserLoginHandler
	 * @param $data
	 * @return bool
	 */
	public static function OnBeforeUserLoginHandler($data) {
		CNiyamaCart::SetTmpFuserValue();
	}

	protected static $iAnonymousFUserId = 0;
	/**
	 * OnUserLoginHandler1 этот обработчик выполняется первым
	 */
	public static function OnUserLoginHandler1($iUserId) {
		if(CModule::IncludeModule('sale')) {
			// сохраняем код FUSER_ID анонима
			self::$iAnonymousFUserId = CSaleBasket::GetBasketUserId();
		}
	}

	/**
	 * OnUserLoginHandler2 этот обработчик выполняется после всех обработчиков
	 */
	public static function OnUserLoginHandler2($iUserId) {
		# Переопределяем гостей
		CNiyamaCart::TransferGuests($iUserId);

		// внимание! в модуле sale также имеется обработчик события OnUserLogin, который может изменить значение FUSER_ID,
		// для корректной работы метода наш обработчик должен отработать позже
		CNiyamaOrders::TransferAnonymousOrders($iUserId, self::$iAnonymousFUserId);

		# Устанавливаем при первом логине флаг просмотра модального окна
		CNiyamaModal::SetUserLoginSMType($iUserId);
	}

	/**
	 * После авторизации пользователя
	 * OnAfterUserLoginHandler
	 * @param $fields
	 */
	public static function OnAfterUserLoginHandler(&$arFields) {
        $USER = $GLOBALS['USER'];

	    if(defined('NIYAMA_IGNORE_LOGIN_EVENTS')) {
			return;
		}

		// проводим идентификацию дисконтной карты, если есть таковые на проверке
		$iCurUserId = $arFields['USER_ID'] ? $arFields['USER_ID'] : $GLOBALS['USER']->GetId();
        $bRefreshCache = false;
		if($iCurUserId) {
			// проверим наличие у юзера дисконтной карты, находящейся на идентификации через карточку пользователя, и привяжем ее, если идентификация прошла успешно
			$iCardDataElementId = CNiyamaDiscountCard::CheckManualIdentify($iCurUserId);
			if($iCardDataElementId) {
				$bRefreshCache = true;
			}
		}

		// Проверка на изменения скидки (синхронизация с ПДС)
		if($iCurUserId) {

			CNiyamaDiscountCard::SyncUserDiscountCard($iCurUserId, $bRefreshCache);
            // если логин OK

                $UserID = $USER->GetID();

                $discount = null;
               $discountPDS = CNiyamaIBlockCardData::GetUserDiscountActual();
                $discountUser = CNiyamaIBlockCardData::GetUserDiscountDB();
                if($discountPDS){
                    $discount = $discountPDS;
                } else {
                    $discount = max($discountPDS, $discountUser);
                }



                if($UserID > 0 && $discount > 0)
                {
                    $arGroups = CGroup::GetList(
                        ($by="c_sort"),
                        ($order="desc"),
                        array(
                            "STRING_ID" => "discount_{$discount}%" // находим группу по символьному ИД
                        )
                    );
                    $existDiscountGroupId = null;
                    $newDiscountGroupId = null;
                    if ($arGroup = $arGroups->Fetch())
                    {
                        $existDiscountGroupId = $arGroup["ID"];
                    }
                    if(empty($existDiscountGroupId)){
                        $group = new CGroup;
                        $arFields = Array(
                            "ACTIVE"       => "Y",
                            "C_SORT"       => 100,
                            "NAME"         => "Скидка_{$discount}%",
                            "DESCRIPTION"  => "Группа для пользователей со скидкой {$discount}",
                            "STRING_ID"      => "discount_{$discount}%"
                        );
                        $newDiscountGroupId = $group->Add($arFields);
                        if (strlen($group->LAST_ERROR)>0) AddMessage2Log($group->LAST_ERROR, 'OnAfterUserLoginHandler');
                    }

                    // Выберем все скидки
                    $dbDiscounts = CCatalogDiscount::GetList(
                        array("SORT" => "ASC"),
                        array(
                            "NAME" => "discount_card_{$discount}%",
                            "ACTIVE" => "Y",
                        ),
                        false,
                        false,
                        array(
                            "ID", "VALUE"
                        )
                    );
                    $dReturnDiscountId = null;
                    $dReturnDiscountValue = null;
                    $ID = null;
                    while ($arDiscounts = $dbDiscounts->Fetch())
                    {
                        $dReturnDiscountId = $arDiscounts['ID'];
                        $dReturnDiscountValue = $arDiscounts['VALUE'];
                    }
                    $discountGroupId = empty($existDiscountGroupId)? $newDiscountGroupId : $existDiscountGroupId;
                    if(empty($dReturnDiscountId)){
                        $arFields = array(
                            'SITE_ID' => 's1',
                            'ACTIVE' => 'Y',
                            'NAME' => "discount_card_{$discount}%",
                            'VALUE_TYPE' => 'P',
                            'VALUE' => $discount,
                            'CURRENCY' => 'RUB',
                            'GROUP_IDS' => array($discountGroupId)
                        );

                        $ID = CCatalogDiscount::Add($arFields);
                        if ($ID<=0) {
                            AddMessage2Log('Ошибка при добавлении скидки', 'OnAfterUserLoginHandler');
                            AddMessage2Log("<pre>".print_r($arFields,1)."</pre>", 'OnAfterUserLoginHandler');
                        }
                    }

                    $CurGroups = $USER->GetUserGroupArray();
                    $CurGroups[] = $discountGroupId;
                    $USER->SetUserGroupArray($CurGroups); // добавляем юзера в группу скидки на время его сессии

                } else {
                    AddMessage2Log("userId = {$UserID}", 'OnAfterUserLoginHandler');
                }


		}
	}

	/**
	 * OnBeforeUserSendPasswordHandler
	 */
	public static function OnBeforeUserSendPasswordHandler(&$arFields) {
		//
		// У нас могут быть деактивированные юзеры, импортированные со старого сайта, и CUser::SendPassword() таким юзерам не шлет письма.
		// Костыляем отправку уведомления для изменения пароля
		//
		$dbItems = null;
		if(strlen($arFields['LOGIN'])) {
			$dbItems = CUser::GetByLogin($arFields['LOGIN']);
		}
		if((!$dbItems || !$dbItems->SelectedRowsCount()) && strlen($arFields['EMAIL'])) {
			$dbItems = CUser::GetList(
				$sBy = 'ID',
				$sOrder = 'ASC',
				array(
					'=EMAIL' => $arFields['EMAIL']
				)
			);
		}
		if($dbItems && $dbItems->SelectedRowsCount()) {
			while($arUser = $dbItems->Fetch()) {
				if($arFields['SITE_ID'] === false) {
					$arFields['SITE_ID'] = SITE_ID;
					if(defined('ADMIN_SECTION') && ADMIN_SECTION === true) {
						$arFields['SITE_ID'] = CSite::GetDefSite($arUser['LID']);
					}
				}

				CUser::SendUserInfo($arUser['ID'], $arFields['SITE_ID'], GetMessage('INFO_REQ'), true, 'USER_PASS_REQUEST');
				if(COption::GetOptionString('main', 'event_log_password_request', 'N') === 'Y') {
					CEventLog::Log('SECURITY', 'USER_INFO', 'main', $arUser['ID']);
				}
			}
		}

		$GLOBALS['APPLICATION']->ResetException();
		return false;
	}

	/**
	 * OnBeforeUserChangePasswordHandler
	 */
	public static function OnBeforeUserChangePasswordHandler(&$arFields) {
		//
		// У нас могут быть деактивированные юзеры, импортированные со старого сайта, 
		// таких юзеров нужно активировать при смене пароля через форму "забыл пароль"
		//
		$dbItems = null;
		if(strlen($arFields['LOGIN'])) {
			$arUser = CUser::GetByLogin($arFields['LOGIN'])->Fetch();
			if($arUser) {
				if($arUser['ACTIVE'] == 'N' && $arUser['XML_ID'] == 'check_old_site_auth') {
					self::$arActivateUsers[$arUser['ID']] = $arUser['ID'];
				}
			}
		}
	}


	//
	// -- iblock module handlers --
	//

	/**
	 * OnBeforeIBlockElementAddHandler
	 */
	public static function OnBeforeIBlockElementAddHandler(&$arFields) {
		$bReturn = true;
		// Уровни скидок
		if($bReturn) {
			$bReturn = CNiyamaDiscountLevels::OnBeforeIBlockElementAddHandler($arFields);
		}

		return $bReturn;
	}

	/**
	 * OnAfterIBlockElementAddHandler
	 * @param $arFields
	 */
	public function OnAfterIBlockElementAddHandler(&$arFields) {
		// Устанавливаем связь с основным меню при добавлении импортируемого
		if($arFields['IBLOCK_ID'] == CNiyamaIBlockCatalogBase::GetIBlockId()) {
			CNiyamaIBlockCatalogBase::AddSiteMenuItem($arFields['ID'], $arFields);
		}
	}

	/**
	 * OnBeforeIBlockElementUpdateHandler
	 */
	public static function OnBeforeIBlockElementUpdateHandler(&$arFields) {
		$bReturn = true;
		// Уровни скидок
		if($bReturn) {
			// проверка полей карточки уровня скидок
			$bReturn = CNiyamaDiscountLevels::OnBeforeIBlockElementUpdateHandler($arFields);
		}

		return $bReturn;
	}

	/**
	 * OnAfterIBlockElementUpdateHandler
	 */
	public static function OnAfterIBlockElementUpdateHandler(&$arFields) {
		// Проверяем наличие связи с основным меню при обновлении импортируемого
		if($arFields['IBLOCK_ID'] == CNiyamaIBlockCatalogBase::GetIBlockId()) {
			CNiyamaIBlockCatalogBase::AddSiteMenuItem($arFields['ID'], $arFields);
		}
	}

	/**
	 * OnBeforeIBlockElementDeleteHandler
	 */
	public static function OnBeforeIBlockElementDeleteHandler($iElementId) {
		$bReturn = true;

		$dbItems = CIBlockElement::GetList(
			array(),
			array(
				'ID' => $iElementId
			),
			false,
			false,
			array(
				'IBLOCK_ID', 'ID', 'CODE', 'XML_ID', 'TMP_ID'
			)
		);
		$arFields = $dbItems->Fetch();

		if(!$arFields) {
			return $bReturn;
		}

		// Уровни скидок
		if($bReturn) {
			// проверка возможности удаления карточки уровня скидок
			$bReturn = CNiyamaDiscountLevels::OnBeforeIBlockElementDeleteHandler($iElementId, $arFields);
		}

		return $bReturn;
	}

	/**
	 * OnAfterIBlockElementDeleteHandler
	 */
	public static function OnAfterIBlockElementDeleteHandler($arFields) {
		if($arFields['ID'] && $arFields['IBLOCK_ID'] && $arFields['IBLOCK_ID'] == CNiyamaMedals::GetUserMedalsIBlockId()) {
			CNiyamaMedals::DeleteUserMedalInfoByUserMedalId($arFields['ID']);
		}
	}


	//
	// -- sale module handlers --
	//

	/**
	 * OnBasketAddHandler
	 * @param $iBasketItemId
	 * @param $arFields
	 */
	public static function OnBasketAddHandler($iBasketItemId, $arFields) {
		// обновление стоимости заказа
		CNiyamaOrders::CartChangeHandler($iBasketItemId);
	}

	/**
	 * OnBasketUpdateHandler
	 * @param $iBasketItemId
	 * @param $arFields
	 */
	public static function OnBasketUpdateHandler($iBasketItemId, $arFields) {
		// обновление стоимости заказа
		if(isset($arFields['PROPS']['type_bonus']['VALUE']) && $arFields['PROPS']['type_bonus']['VALUE'] == 'DISCOUNT_MONEY') {
			return;
		}
		CNiyamaOrders::CartChangeHandler($iBasketItemId);
	}

	protected static $arBasketItems2Orders = array();
	/**
	 * OnBeforeBasketDeleteHandler
	 */
	public static function OnBeforeBasketDeleteHandler($iBasketItemId) {
		CModule::IncludeModule('sale');
		$arBasketItem = CSaleBasket::GetById($iBasketItemId);
		if($arBasketItem['ORDER_ID']) {
			self::$arBasketItems2Orders[$iBasketItemId] = $arBasketItem['ORDER_ID'];
		}
	}

	/**
	 * OnBasketDeleteHandler
	 * @param $iBasketItemId
	 */
	public static function OnBasketDeleteHandler($iBasketItemId) {
		// обновление стоимости заказа
		if(self::$arBasketItems2Orders[$iBasketItemId]) {
			CNiyamaOrders::CartChangeHandler($iBasketItemId, self::$arBasketItems2Orders[$iBasketItemId]);
			unset(self::$arBasketItems2Orders[$iBasketItemId]);
		}
	}

	/**
	 * OnSaleCalculateOrderHandler
	 * @param $order
	 */
	public static function OnSaleCalculateOrderHandler(&$order) {

		$basket_id = 0;
		if (!empty($order['BASKET_ITEMS'])) {
			$ar_basket_quanity = array();
			foreach ($order['BASKET_ITEMS'] as $basket) {
				if (!empty($basket['BASKET_ID'])) {
					$ar_basket_quanity[$basket['BASKET_ID']]=$basket['QUANTITY'];
					if ($basket_id <= 0) {
						$basket_id=$basket['BASKET_ID'];
					}
				}
			}
			//$basket_id=$order['BASKET_ITEMS'][0]['BASKET_ID'];
			CModule::IncludeModule('sale');
			$arBasketItem = CSaleBasket::GetById($basket_id);
			if($arBasketItem['ORDER_ID']) {
				$price = CNiyamaCoupons::CalcArPriceByCoupons($arBasketItem['ORDER_ID'], $ar_basket_quanity);
				if($price['DISCOUNT_ALL'] > 0) {
					$order['PRICE'] -= $price['DISCOUNT_ALL'];
					$order['DISCOUNT_VALUE'] = $price['DISCOUNT_ALL'];
					/*
					$bReturn = CSaleOrder::Update(
						$arBasketItem['ORDER_ID'],
						array(
							'PRICE' => $order['PRICE'],
							'DISCOUNT_VALUE' => $order['DISCOUNT_VALUE'],
						)
					);
					*/
				}
			}
		}
	}

	/**
	 * OnSaleStatusOrderHandler
	 * @param $iOrderId
	 * @param $sOrderStatus
	 */
	public static function OnSaleStatusOrderHandler($iOrderId, $sOrderStatus) {


		switch($sOrderStatus) {
			case 'E':
				// [E] Экспорт в FO
                CNiyamaOrders::ExportOrder2FO($iOrderId);
				$arDop = array();
				$arDop['ORDER']['ORDER_ID'] = $iOrderId;
				CNiyamaCoupons::CheckCouponsByTypeEvent('ORDER', 0, $arDop);
				CNiyamaBonus::CheckBonusesByTypeEvent('ORDER', 0, $arDop);
				CNiyamaCoupons::UseCouponsByOrder($iOrderId);
			break;

			case 'R':
				// [R] Принят на стороне FO, отправим письмо юзеру
				CNiyamaOrders::SendNewOrderMessage($iOrderId);
			break;

			case 'F':
				// [F] Выполнен
				CNiyamaCoupons::ActivateNewCouponsByOrder($iOrderId);
				// апдейт уровней скидок по сумме заказов
				CNiyamaDiscountLevels::OnSaleStatusOrderHandler($iOrderId);
			break;

			case 'N':
				// [N] Принят
				// апдейт уровней скидок по сумме заказов
				CNiyamaDiscountLevels::OnSaleStatusOrderHandler($iOrderId);
			break;
		}
	}


	/**
	 * OnSalePayOrderHandler
	 * @param $iOrderId
	 * @param $sPayStatus
	 */
	public static function OnSalePayOrderHandler($iOrderId, $sPayStatus) {
		$arOrder = array();
		if(CModule::IncludeModule('sale')) {
			$arOrder = CSaleOrder::GetById($iOrderId);
		}
		if($arOrder && in_array($arOrder['STATUS_ID'], array('P', 'W'))) {
			CSaleOrder::StatusOrder($arOrder['ID'], 'E');
		}
	}

	/**
	 * OnSaleCancelOrderHandler
	 * @param $iOrderId
	 * @param $sCancelValue
	 * @param $sCancelDescription
	 */
	public static function OnSaleCancelOrderHandler($iOrderId, $sCancelValue, $sCancelDescription) {
		if($sCancelValue == 'Y') {
			// заказ отменен, отправим письмо юзеру
			CNiyamaOrders::SendCancelOrderMessage($iOrderId, $sCancelDescription);			
			CNiyamaCoupons::DeleteCouponsByOrderByCancel($iOrderId);
		}
	}

	//
	// -- vote module handlers --
	//

	/**
	 * onAfterVotingHandler
	 * @param $VOTE_ID
	 * @param $EVENT_ID
	 */
	public static function onAfterVotingHandler($VOTE_ID, $EVENT_ID) {
		CNiyamaUserActions::AddSurvey();
		CNiyamaCoupons::CheckCouponsByTypeEvent('SURVEY');
		CNiyamaBonus::CheckBonusesByTypeEvent('SURVEY');
	}

	//
	// -- search module handlers --
	//

	/**
	 * BeforeIndexHandler
	 * 
	 * @param array $arFields
	 * @return array
	 */
	public static function BeforeIndexHandler($arFields) {
		// для основного меню проверяем флаг активности импортируемого меню
		if($arFields['MODULE_ID'] == 'iblock' && $arFields['PARAM2'] == CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId()) {
			// проверка через intval нужна, т.к. для секций приходит значение формата S1234
			if(intval($arFields['ITEM_ID']) > 0) {
				CModule::IncludeModule('iblock');
				$iCnt = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_ID' => CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId(),
						'ID' => $arFields['ITEM_ID'],
						'ACTIVE' => 'Y',
						'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
						'!SECTION_ID' => false // также проверяем ривязку к разделу
					),
					array()
				);
				if(!$iCnt) {
					// исключим из поиска, для этого сбрасываем TITLE и BODY
					$arFields['TITLE'] = '';
					$arFields['BODY'] = '';
				}
			}
		}
		return $arFields;
	}

	//
	// -- обработчики кастомных событий проекта --
	//

	/**
	 * Событее после регистрации через соцсеть
	 * OnAfterSocUserRegisterHandler
	 * @param $ID
	 * @param $arSocData
	 */
	public static function OnAfterSocUserRegisterHandler($iUserId, $arSocData) {
		# Добавляем событие авторизации через соцСеть
		CNiyamaUserActions::AddAuthSoc();
		// Купоны скидок
		CNiyamaCoupons::CheckCouponsByTypeEvent('AUTHSOC', $iUserId);
		// Бонусы-ниямы
		CNiyamaBonus::CheckBonusesByTypeEvent('AUTHSOC', $iUserId);
	}

	/**
	 * Событие после авторизации через соцсети
	 * OnAfterSocAuthorizeHandler
	 * @param int $iUserId
	 */
	public static function OnAfterSocAuthorizeHandler($iUserId) {
		# Добавляем событие авторизации через соцсеть
		CNiyamaUserActions::AddAuthSoc();
		// Купоны скидок
		CNiyamaCoupons::CheckCouponsByTypeEvent('AUTHSOC', $iUserId);
		// Бонусы-ниямы
		// LSV: отключил, т.к. требуется разовое начисление за привязку аккаунта соцсети, а не за каждую авторизацию
		//CNiyamaBonus::CheckBonusesByTypeEvent('AUTHSOC', $iUserId);
	}

	/**
	 * OnUserSocShareHandler
	 * @param $sSocial
	 * @param $sType
	 */
	public static function OnUserSocShareHandler($sSocial, $sType) {
		if($sType == 'product_detail') {
			# Добавляем событие авторизации через cоцcеть
			CNiyamaUserActions::AddRecWidget();
			CNiyamaCoupons::CheckCouponsByTypeEvent('RECOMENDATIONSOC', 0, array('RECOMENDATIONSOC' => 'RECOMENDATON_FROM_VIDZHET'));
			CNiyamaBonus::CheckBonusesByTypeEvent('RECOMENDATIONSOC', 0, array('RECOMENDATIONSOC' => 'RECOMENDATON_FROM_VIDZHET'));
		} elseif($sType == 'modal_share') {
			CNiyamaUserActions::AddRecLivel();
			CNiyamaCoupons::CheckCouponsByTypeEvent('RECOMENDATIONSOC', 0, array('RECOMENDATIONSOC' => 'TWEET_YROVEN_OR_MEDAL'));
			CNiyamaBonus::CheckBonusesByTypeEvent('RECOMENDATIONSOC', 0, array('RECOMENDATIONSOC' => 'RECOMENDATON_FROM_VIDZHET'));
		}
	}

	/**
	 * OnTransferAnonymousOrdersHandler
	 * @param $iUserId
	 * @param $iFUserId
	 * @param $iTransferOrderIdResult
	 */
	public static function OnTransferAnonymousOrdersHandler($iUserId, $iFUserId, $iTransferOrderIdResult) {
		if($GLOBALS['USER']->IsAuthorized()) {
			CNiyamaCoupons::AddDefCoupons();
		}
	}

	protected static $arSkipSetMedalEvent = array();
	public static function SetSkipMedalEvent($iMedalCardId, $iUserId) {
		self::$arSkipSetMedalEvent[$iMedalCardId.'_'.$iUserId] = $iMedalCardId;
	}

	/**
	 * OnAfterSetMedalHandler
	 */
	public static function OnAfterSetMedalHandler($iMedalCardId, $iUserId, $iUserMedalId, $arMedalInfo) {
		if(isset(self::$arSkipSetMedalEvent[$iMedalCardId.'_'.$iUserId])) {
			unset(self::$arSkipSetMedalEvent[$iMedalCardId.'_'.$iUserId]);
			return;
		}

		$arDop = array();
		$arDop['MEDAL']['MEDAL_ID'] = $iMedalCardId;
		CNiyamaBonus::CheckBonusesByTypeEvent('MEDAL', $iUserId, $arDop);

		# Устанавливаем за медаль попап
		CNiyamaModal::SetSession('TYPE_2', $iUserId, $iMedalCardId);
	}

	/**
	 * OnUserDiscountLevelAddHandler
	 * @param $arDiscountLevel
	 */
	public static function OnUserDiscountLevelAddHandler($arDiscountLevel, $arAddEventParams) {
		if($arDiscountLevel['DISCOUNT_VAL_PERC'] > 0) {
			if($arDiscountLevel['B_ONE_TIME'] != 'Y' && (!isset($arAddEventParams['_SKIP_SET_DISCOUNT_CARD_VALUE_']) || !$arAddEventParams['_SKIP_SET_DISCOUNT_CARD_VALUE_'])) {
				// Сохраняем новое значение дисконта на сайте и передаем в ПДС 
				// установленное в итоге значение может не соответствовать передаваемому (выбирается максимальный процент в ПДС и на сайте)
				$mSettedValue = CNiyamaDiscountCard::SetDiscountCardValueByUserId($arDiscountLevel['USER_ID'], $arDiscountLevel['DISCOUNT_VAL_PERC']);
			}
		}
		// если не нужно начислять бонусы (например, первичная установка скидки по карте)
		if($arDiscountLevel['NO_BONUS']) {
			return;
		}

		CNiyamaCoupons::AddCouponByArrTimeline($arDiscountLevel);

		$dop = array();
		$dop['TIMELINE']['TIMELINE_ID'] = $arDiscountLevel['ID'];
		CNiyamaBonus::CheckBonusesByTypeEvent('TIMELINE', $arDiscountLevel['USER_ID'], $dop);

		// Устанавливаем попап за скидку (только за переход на другой процент скидки, т.к. по остальным типам бонусов попап может выводиться)
		if($arDiscountLevel['DISCOUNT_VAL_PERC'] > 0 && $arDiscountLevel['B_ONE_TIME'] != 'Y') {
			CNiyamaModal::SetSession('TYPE_1', $arDiscountLevel['USER_ID'], $arDiscountLevel['ID']);
		}
	}

	/**
	 * OnAfterCardAddHandler
	 * @param $arCardFields
	 */
	/*
	public static function OnAfterCardAddHandler($arCardFields) {
		if($arCardFields['PROPERTY_VALUES']['USER_ID'] && $arCardFields['PROPERTY_VALUES']['DISCOUNT']) {
			$dDiscountVal = CNiyamaIBlockCardData::GetTrueDiscount($arCardFields['PROPERTY_VALUES']['DISCOUNT']);
			if($dDiscountVal) {
				CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($arCardFields['PROPERTY_VALUES']['USER_ID'], $dDiscountVal);
			}
		}
	}
	*/

	/**
	 * OnAfterCardEditHandler
	 * @param $arCardFields
	 */
	/*
	public static function OnAfterCardEditHandler($arCardFields) {
		if($arCardFields['PROPERTY_VALUES']['USER_ID'] && $arCardFields['PROPERTY_VALUES']['DISCOUNT']) {
			$dDiscountVal = CNiyamaIBlockCardData::GetTrueDiscount($arCardFields['PROPERTY_VALUES']['DISCOUNT']);
			if($dDiscountVal) {
				CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($arCardFields['PROPERTY_VALUES']['USER_ID'], $dDiscountVal);
			}
		}
	}
	*/

       

}

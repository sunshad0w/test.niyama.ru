<?
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 10.09.2015
 */

class CNiyamaCatalog {

	/**
	 * GetCategoriesDetailsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetCategoriesDetailsList($bRefreshCache = false, $bGetNotActive = true) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v1'.($bGetNotActive ? 'Y' : 'N');
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'cats_details_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		// включаем использование режима статического кэширования даже при отключенном общем кэшировании результата
		$obExtCache->SetForceStaticCachingUsing();
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'IBLOCK_ID' => $iIBlockId,
				);
				if(!$bGetNotActive) {
					$arFilter['ACTIVE'] = 'Y';
					$arFilter['GLOBAL_ACTIVE'] = 'Y';
				}

				$dbItems = CIBlockSection::GetList(
					array(
						'ID' => 'ASC',
					),
					$arFilter,
					false,
					array(
						'ID', 'NAME'
					)
				);
				while($arItem = $dbItems->GetNext(true, false)) {
					if(!$arReturn[$arItem['ID']]) {
						// названия и картинки берем из инфоблока импортируемого меню
						$arReturn[$arItem['ID']] = array(
							'ID' => $arItem['ID'],
							'NAME' => $arItem['NAME'],
						);
					}
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * GetCategoryDetail
	 * @param $iCatId
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetCategoryDetail($iCatId, $bGetNotActive = false, $bRefreshCache = false) {
		$arReturn = array();
		$iCatId = intval($iCatId);
		if($iCatId> 0) {
			$arCatsList = self::GetCategoriesDetailsList($bRefreshCache, $bGetNotActive);
			if(isset($arCatsList[$iCatId])) {
				return $arCatsList[$iCatId];
			}
		}
		return array();
	}

	/**
	 * GetProductsDetailsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetProductsDetailsList($bRefreshCache = false, $bGetNotActive = true) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v12'.($bGetNotActive ? 'Y' : 'N');
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'products_details_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		// включаем использование режима статического кэширования даже при отключенном общем кэшировании результата
		$obExtCache->SetForceStaticCachingUsing();
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
			$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
			$iIngredientsIBlockId = CProjectUtils::GetIBlockIdByCode('catalog-ingredients');
			$iExIngredientsIBlockId = CProjectUtils::GetIBlockIdByCode('catalog-ex-ingredients');
			$iSpecialMenuIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-special-menu');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'IBLOCK_ID' => $iIBlockId,
					'CHECK_PERMISSIONS' => 'N',
				);
				if(!$bGetNotActive) {
					$arFilter['ACTIVE'] = 'Y';
					$arFilter['PROPERTY_IMPORT_ELEMENT.ACTIVE'] = 'Y';
				}

				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC',
					),
					$arFilter,
					false,
					false,
					array(
						'ID', 'NAME', 'CODE', 'ACTIVE', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL',
						'DETAIL_TEXT', 'PREVIEW_TEXT', 'SHOW_COUNTER', 'DETAIL_TEXT_TYPE', 'PREVIEW_TEXT_TYPE',

						'PROPERTY_IMPORT_ELEMENT.PROPERTY_SIZE', 'PROPERTY_IMPORT_ELEMENT.PROPERTY_NAME_SHORT', 
						'PROPERTY_IMPORT_ELEMENT.PROPERTY_SORT', 'PROPERTY_IMPORT_ELEMENT.PROPERTY_RECOMMENDED', 'PROPERTY_IMPORT_ELEMENT.PROPERTY_PRICE',

						'PROPERTY_IMPORT_ELEMENT.XML_ID', 'PROPERTY_IMPORT_ELEMENT.NAME', 'PROPERTY_IMPORT_ELEMENT.ACTIVE',
						'PROPERTY_IMPORT_ELEMENT.PREVIEW_PICTURE', 'PROPERTY_IMPORT_ELEMENT.DETAIL_PICTURE', 
						'PROPERTY_IMPORT_ELEMENT.PROPERTY_WEIGHT', 'PROPERTY_IMPORT_ELEMENT.PROPERTY_CALORIC', 'PROPERTY_IMPORT_ELEMENT.PROPERTY_WEIGHT_FULL',

						'PROPERTY_NUM_OF_PIECES', 'PROPERTY_OTHER_DESCRIPTION', 'PROPERTY_MARGINALITY', 'PROPERTY_MARGINALITY_WEIGHT',
						/*
						'PROPERTY_WEIGHT', 'PROPERTY_NUM_OF_CALORIES',
						*/

						'PROPERTY_GENERAL_INGREDIENT.ID', 'PROPERTY_GENERAL_INGREDIENT.NAME',
						'PROPERTY_MORE_INGREDIENT.ID', 'PROPERTY_MORE_INGREDIENT.NAME',
						'PROPERTY_EXCLUDE_INGREDIENT.ID', 'PROPERTY_EXCLUDE_INGREDIENT.NAME',
						'PROPERTY_SPEC_MENU.ID', 'PROPERTY_SPEC_MENU.NAME',
						'PROPERTY_CANONICAL_ITEM'
					)
				);
				while($arItem = $dbItems->GetNext(true, false)) {
					if(!$arReturn[$arItem['ID']]) {
						// названия и картинки берем из инфоблока импортируемого меню
						$arReturn[$arItem['ID']] = array(
							'INFO' => array(
								'_ACTIVE' => $arItem['ACTIVE'],
								//'_PROPERTY_WEIGHT_VALUE' => $arItem['PROPERTY_WEIGHT_VALUE'],
								//'_PROPERTY_NUM_OF_CALORIES' => $arItem['PROPERTY_NUM_OF_CALORIES_VALUE'],
								'_PREVIEW_PICTURE' => $arItem['PREVIEW_PICTURE'],
								'_NAME' => $arItem['NAME'],

								'SECTION' => self::GetCategoryDetail($arItem['IBLOCK_SECTION_ID']),

								'ACTIVE' => $arItem['PROPERTY_IMPORT_ELEMENT_ACTIVE'],
								'PROPERTY_WEIGHT_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_WEIGHT_VALUE'],
								'PROPERTY_WEIGHT_FULL_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_WEIGHT_FULL_VALUE'],
								'PROPERTY_NUM_OF_CALORIES' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_CALORIC_VALUE'],
								'PREVIEW_PICTURE' => $arItem['PROPERTY_IMPORT_ELEMENT_PREVIEW_PICTURE'],
								'DETAIL_PICTURE' => $arItem['PROPERTY_IMPORT_ELEMENT_DETAIL_PICTURE'],
								'NAME' => $arItem['PROPERTY_IMPORT_ELEMENT_NAME'],

								'ID' => $arItem['ID'],
								'CODE' => $arItem['CODE'],
								'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
								'SHOW_COUNTER' => $arItem['SHOW_COUNTER'],
								'FO_XML_ID' => $arItem['PROPERTY_IMPORT_ELEMENT_XML_ID'],
								'DETAIL_TEXT' => $arItem['DETAIL_TEXT'],
								'DETAIL_TEXT_TYPE' => $arItem['DETAIL_TEXT_TYPE'],
								'PREVIEW_TEXT' => $arItem['PREVIEW_TEXT'],
								'PREVIEW_TEXT_TYPE' => $arItem['PREVIEW_TEXT_TYPE'],
								'IBLOCK_SECTION_ID' => $arItem['IBLOCK_SECTION_ID'],
								'PROPERTY_NUM_OF_PIECES' => $arItem['PROPERTY_NUM_OF_PIECES_VALUE'],
								'PROPERTY_IMPORT_ELEMENT_PROPERTY_SIZE_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_SIZE_VALUE'],
								'PROPERTY_IMPORT_ELEMENT_PROPERTY_NAME_SHORT_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_NAME_SHORT_VALUE'],
								//'PROPERTY_IMPORT_ELEMENT_PROPERTY_WEIGHT_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_WEIGHT_VALUE'],
								'PROPERTY_IMPORT_ELEMENT_PROPERTY_SORT_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_SORT_VALUE'],
								'PROPERTY_IMPORT_ELEMENT_PROPERTY_RECOMMENDED_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_RECOMMENDED_VALUE'],
								'PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE' => $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
								'PROPERTY_SPEC_MENU_ID' => $arItem['PROPERTY_SPEC_MENU_ID'],
								'PROPERTY_SPEC_MENU_NAME' => $arItem['PROPERTY_SPEC_MENU_NAME'],

								'PROPERTY_MARGINALITY_VALUE' => $arItem['PROPERTY_MARGINALITY_VALUE'],
								'PROPERTY_MARGINALITY_WEIGHT_VALUE' => $arItem['PROPERTY_MARGINALITY_WEIGHT_VALUE'],
								'PROPERTY_CANONICAL_ITEM_VALUE' => $arItem['PROPERTY_CANONICAL_ITEM_VALUE'],
							),
							'GENERAL_INGREDIENT' => array(),
							'MORE_INGREDIENT' => array(),
							'ALL_INGREDIENT' => array(),
							'EXCLUDE_INGREDIENT' => array(),
						);
					}
					if($arItem['PROPERTY_GENERAL_INGREDIENT_ID']) {
						$arReturn[$arItem['ID']]['GENERAL_INGREDIENT'][$arItem['PROPERTY_GENERAL_INGREDIENT_ID']] = $arItem['PROPERTY_GENERAL_INGREDIENT_NAME'];
						$arReturn[$arItem['ID']]['ALL_INGREDIENT'][$arItem['PROPERTY_GENERAL_INGREDIENT_ID']] = $arItem['PROPERTY_GENERAL_INGREDIENT_NAME'];
					}
					if($arItem['PROPERTY_MORE_INGREDIENT_ID']) {
						$arReturn[$arItem['ID']]['MORE_INGREDIENT'][$arItem['PROPERTY_MORE_INGREDIENT_ID']] = $arItem['PROPERTY_MORE_INGREDIENT_NAME'];
						$arReturn[$arItem['ID']]['ALL_INGREDIENT'][$arItem['PROPERTY_MORE_INGREDIENT_ID']] = $arItem['PROPERTY_MORE_INGREDIENT_NAME'];
					}
					if($arItem['PROPERTY_EXCLUDE_INGREDIENT_ID']) {
						$arReturn[$arItem['ID']]['EXCLUDE_INGREDIENT'][$arItem['PROPERTY_EXCLUDE_INGREDIENT_ID']] = $arItem['PROPERTY_EXCLUDE_INGREDIENT_NAME'];
					}
				}
			}
			// !!! важно добавить теги вручную, т.к. модуль инфоблоков не добавляет сам инфоблоки по фильтруемым связям!!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId, 'iblock_id_'.$iImportMenuIBlockId, 'iblock_id_'.$iIngredientsIBlockId, 'iblock_id_'.$iExIngredientsIBlockId, 'iblock_id_'.$iSpecialMenuIBlockId));
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}

		return $arReturn;
	}

	/**
	 * GetProductDetail
	 * @param $iProductId
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetProductDetail($iProductId, $bGetNotActive = false, $bRefreshCache = false) {
		$arReturn = array();
		$iProductId = intval($iProductId);
		if($iProductId > 0) {
			$arProductsList = self::GetProductsDetailsList($bRefreshCache);
			if(isset($arProductsList[$iProductId])) {
				if($bGetNotActive || ($arProductsList[$iProductId]['INFO']['ACTIVE'] == 'Y') && ($arProductsList[$iProductId]['INFO']['_ACTIVE'] == 'Y')) {
					return $arProductsList[$iProductId];
				}
			}
		}
		return array();
	}

	/**
	 * GetProductDetailByCode
	 * @param $sCode
	 * @param bool $bRefreshCache
	 * @return bool
	 */
	public static function GetProductDetailByCode($sCode, $bGetNotActive = false, $bRefreshCache = false) {
		$sCode = trim($sCode);
		if(strlen($sCode)) {
			$arProductsList = self::GetProductsDetailsList($bRefreshCache);
			foreach($arProductsList as $arItem) {
				if($arItem['INFO']['CODE'] == $sCode) {
					if($bGetNotActive || $arItem['INFO']['ACTIVE'] == 'Y') {
						return $arItem;
					}
					break;
				}
			}
		}
		return false;
	}

	public static function GetProductDetailByFoXmlId($sFoXmlId, $bGetNotActive = false, $bRefreshCache = false) {
		$sFoXmlId = trim($sFoXmlId);
		if(strlen($sFoXmlId)) {
			$arProductsList = self::GetProductsDetailsList($bRefreshCache);
			foreach($arProductsList as $arItem) {
				if($arItem['INFO']['FO_XML_ID'] == $sFoXmlId) {
					if($bGetNotActive || $arItem['INFO']['ACTIVE'] == 'Y') {
						return $arItem;
					}
					break;
				}
			}
		}
		return false;
	}

	public static function GetProductDetailBySkuId($skuId) {
        $result = null;
	    if($skuId){
            $s = CCatalogSku::GetProductInfo($skuId);
            $r = CIBlockElement::GetByID($s['ID']);
            $result = $r->Fetch();
        }
        return $result;
    }

	/**
	 * GetDopCompDetailsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetDopCompDetailsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'dopcomp_details_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$iIBlockId = CProjectUtils::GetIBlockIdByCode('additional_components', 'catalog');
				$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
				$arTypeEnumDop = CCustomProject::GetEnumPropValues($iIBlockId, 'CALC_ALG');
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC',
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'ACTIVE' => 'Y',
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'NAME', 'CODE', 'PREVIEW_PICTURE',

						'PROPERTY_UNIT_TYPE', 'PROPERTY_CALC_ALG', 'PROPERTY_SELL',
						//'PROPERTY_PRICE',
						'PROPERTY_FREE_IMPORT_ELEMENT.ID', 'PROPERTY_FREE_IMPORT_ELEMENT.XML_ID',
						'PROPERTY_PAID_IMPORT_ELEMENT.ID', 'PROPERTY_PAID_IMPORT_ELEMENT.PROPERTY_PRICE', 'PROPERTY_PAID_IMPORT_ELEMENT.XML_ID',
					)
				);

				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'CODE' => $arItem['CODE'],
						'PREVIEW_PICTURE' => $arItem['PREVIEW_PICTURE'],
						'PROPERTY_UNIT_TYPE' => $arItem['PROPERTY_UNIT_TYPE_VALUE'],
						//'PROPERTY_CALC_ALG' => $arItem['PROPERTY_CALC_ALG_VALUE'],
						'PROPERTY_CALC_ALG_XML_ID' => $arTypeEnumDop[$arItem['PROPERTY_CALC_ALG_VALUE']]['XML_ID'],
						'PROPERTY_SELL' => $arItem['PROPERTY_SELL_VALUE'],

						'FREE_PRODUCT_ID' => intval($arItem['PROPERTY_FREE_IMPORT_ELEMENT_ID']),
						'FREE_PRODUCT_XML_ID' => $arItem['PROPERTY_FREE_IMPORT_ELEMENT_XML_ID'],
						'PAID_PRODUCT_ID' => intval($arItem['PROPERTY_PAID_IMPORT_ELEMENT_ID']),
						'PAID_PRODUCT_XML_ID' => $arItem['PROPERTY_PAID_IMPORT_ELEMENT_XML_ID'],
						'PAID_PRODUCT_PRICE' => doubleval($arItem['PROPERTY_PAID_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']),
					);
				}
			}

			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId, 'iblock_id_'.$iImportMenuIBlockId));

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * GetDopCompDetail
	 * @param $iDopCompId
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetDopCompDetail($iDopCompId, $bRefreshCache = false) {
		$arReturn = array();
		$iDopCompId = intval($iDopCompId);
		if($iDopCompId > 0) {
			$arDopCompList = self::GetDopCompDetailsList($bRefreshCache);
			return isset($arDopCompList[$iDopCompId]) ? $arDopCompList[$iDopCompId] : array();
		}
		return array();
	}

	public static function GetDopCompDetailByImpElementId($iImportElementId, $bRefreshCache = false) {
		$arReturn = array();
		$iImportElementId = intval($iImportElementId);
		if($iImportElementId > 0) {
			$arDopCompList = self::GetDopCompDetailsList($bRefreshCache);
			foreach($arDopCompList as $arItem) {
				if($arItem['FREE_PRODUCT_ID'] == $iImportElementId || $arItem['PAID_PRODUCT_ID'] == $iImportElementId) {
					$arReturn = $arItem;
					break;
				}
			}
		}
		return $arReturn;
	}

	/**
	 * getProductDopComp
	 * @param $iProductId
	 * @return array
	 */
	public static function getProductDopComp($iProductId) {
		$arReturn = array();
		$iProductId = intval($iProductId);
		if($iProductId <= 0) {
			return $arReturn;
		}
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iProductId.'|v4';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$iIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
			$iDopCompIBlockId = CProjectUtils::GetIBlockIdByCode('additional_components', 'catalog');
			$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();

			$obExtCache->StartDataCache();
			if($iIBlockId && CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(),
					array(
						'ID' => $iProductId,
						'IBLOCK_ID' => $iIBlockId,
						'ACTIVE' => 'Y',
						'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID',
						'PROPERTY_DOP_COMPONENT',
					)
				);
				if($arItem = $dbItems->Fetch()) {
					if(is_array($arItem['PROPERTY_DOP_COMPONENT_VALUE'])) {
						foreach($arItem['PROPERTY_DOP_COMPONENT_VALUE'] as $iTmpElementId) {
							$arTmpDetail = self::GetDopCompDetail($iTmpElementId);
							if($arTmpDetail) {
								$arReturn[$arTmpDetail['ID']] = $arTmpDetail;
							}
						}
					}
				}
			}
			// !!! важно добавить тег по $iIBlockId вручную, т.к. GetDopCompDetail забирает тег по инфоблоку себе !!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iDopCompIBlockId, 'iblock_id_'.$iIBlockId, 'iblock_id_'.$iImportMenuIBlockId));

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * GetList
	 * @param null $arPSelect
	 * @param null $arPFilter
	 * @param null $arPOrder
	 * @param null $arPNavStartParams
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetList($arPSelect = null, $arPFilter = null, $arPOrder = null, $arPNavStartParams = null, $bRefreshCache = false) {
		$iSiteMenuIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
		$arSelect = array(
			'ID',
			'NAME',
			'CODE',
			'IBLOCK_SECTION_ID',
			'PREVIEW_PICTURE',
			'PREVIEW_TEXT',
			'DETAIL_PAGE_URL',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_SIZE',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_NAME_SHORT',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_SORT',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_RECOMMENDED',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_PRICE',
			'PROPERTY_IMPORT_ELEMENT.NAME',
			'PROPERTY_IMPORT_ELEMENT.PREVIEW_PICTURE',
			'PROPERTY_IMPORT_ELEMENT.DETAIL_PICTURE',

			'PROPERTY_IMPORT_ELEMENT.PROPERTY_WEIGHT',
			'PROPERTY_IMPORT_ELEMENT.PROPERTY_CALORIC',

			'PROPERTY_NUM_OF_PIECES', 'PROPERTY_MARGINALITY',
			'PROPERTY_MARGINALITY_WEIGHT', 'PROPERTY_OTHER_DESCRIPTION',

			// теперь вес и калорийность берутся из импортируемого меню
			/*
			'PROPERTY_WEIGHT', 'PROPERTY_NUM_OF_CALORIES',
			*/

			'PROPERTY_SPEC_MENU.ID',
			'PROPERTY_SPEC_MENU.NAME',
		);
		if (!is_null($arPSelect)) {
			if (is_array($arPSelect)) {
				$arSelect = array_merge($arSelect, $arPSelect);
			}
		}
		$arFilter = array (
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => $iSiteMenuIBlockId,
			'CHECK_PERMISSIONS' => 'N',
			'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y'
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, array($arPFilter));
			}
		}

		$arOrder = array(
			'SHOW_COUNTER' => 'DESC',
		);
		if($arPOrder && is_array($arPOrder)) {
			$arOrder = $arPOrder;
		}

		$arNavStartParams = false;
		if($arPNavStartParams && is_array($arPNavStartParams)) {
			$arNavStartParams = $arPNavStartParams;
		}
		if($arPNavStartParams === false) {
			$arNavStartParams = false;
		}

		$arReturn = array();
		$sCacheAddParam = 'v6'.md5(serialize(array($arFilter, $arOrder, $arSelect, $arNavStartParams)));
		$sCacheId = 'GetList';
		$arAddCacheTags = array(__METHOD__);
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		$bUseStaticCache = true;
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);
		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');

			$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
			$iSpecialMenuIBlockId = CProjectUtils::GetIBlockIdByCodeEx('catalog-special-menu');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$arReturn['ITEMS'] = array();
			$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
			while($arItem = $rsItems->GetNext(true, false)) {
				// названия и картинки берем из инфоблока импортируемого меню
				$arItem['NAME'] = $arItem['PROPERTY_IMPORT_ELEMENT_NAME'];
				$arItem['PREVIEW_PICTURE'] = $arItem['PROPERTY_IMPORT_ELEMENT_PREVIEW_PICTURE'];
				$arItem['DETAIL_PICTURE'] = $arItem['PROPERTY_IMPORT_ELEMENT_DETAIL_PICTURE'];

				$arReturn['ITEMS'][$arItem['ID']]['INFO'] = $arItem;
			}

			$arIDs = array_keys($arReturn['ITEMS']);

			if (!empty($arIDs)) {
				$arTrueIng = self::getAllIngredients($arIDs);
				foreach ($arTrueIng as $id => $row) {
					$arReturn['ITEMS'][$id]['ING'] = $row;
				}
				$arTrueExIng = self::getAllExIngredients($arIDs);
				foreach ($arTrueExIng as $id => $row) {
					$arReturn['ITEMS'][$id]['EX_ING'] = $row;
				}
			}

			// !!! важно добавить тег по $iImportMenuIBlockId вручную, т.к. модуль инфоблоков не добавляет сам инфоблоки по фильтруемым связям!!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iSiteMenuIBlockId, 'iblock_id_'.$iImportMenuIBlockId, 'iblock_id_'.$iSpecialMenuIBlockId));
			if (empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * GetRelatedProducts
	 * @param $sProductCode
	 * @return array
	 */
	public static function GetRelatedProducts($sProductCode) {
		$arReturn = array();
		$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
		$arSelect = array(
			'ID',
			'NAME',
			'CODE',
			'PROPERTY_RELATED_PRODUCTS',
		);
		$rsItems = CIBlockElement::GetList(
			array(
				'PROPERTY_RELATED_PRODUCTS_DESCRIPTION' => 'ASC'
			), 
			array(
				'=CODE' => $sProductCode,
				'IBLOCK_ID' => $IBLOCK_ID
			), 
			false, 
			false, 
			$arSelect
		);
		if($arItem = $rsItems->Fetch()) {
			array_walk(
				$arItem['PROPERTY_RELATED_PRODUCTS_VALUE'], 
				function(&$value, $index) {
					$value = CNiyamaCatalog::GetProductDetail($value);
				}
			);
			$arReturn = $arItem['PROPERTY_RELATED_PRODUCTS_VALUE'];
		}
		return $arReturn;
	}

	/**
	 * Возвращает похожие товары для заданного
	 * @author: t.k
	 * @param $iProductId
	 * @return array
	 */
	public static function GetRelatedProductsById($iProductId) {
		CModule::IncludeModule('iblock');
		$arReturn = array();

		// Сколько нужно выбрать похожих товаров - получаем из опций
		$iMaxCount = intval(CNiyamaCustomSettings::getStringValue('max_num_related_products', 4));
		$iCurCount = 0;
		// Товары, исключаемые из автоматического списка
		$arExclProducts = array();
		if ($GLOBALS['CART_PRODUCT_IDS']) {
			$arExclProducts = array_keys($GLOBALS['CART_PRODUCT_IDS']);
		}

		// Родительская секция товара
		$iSectionId = 0;
		/*
		$arCurProduct = CNiyamaCatalog::GetProductDetail($iProductId);
		$arCurGeneralIng=array();
		$arCurMoreIng=array();
		if ((!empty($arCurProduct['GENERAL_INGREDIENT']))&&(is_array($arCurProduct['GENERAL_INGREDIENT']))){
			$arCurGeneralIng=$arCurProduct['GENERAL_INGREDIENT'];
		}
		if ((!empty($arCurProduct['MORE_INGREDIENT']))&&(is_array($arCurProduct['MORE_INGREDIENT']))){
			$arCurMoreIng=$arCurProduct['MORE_INGREDIENT'];
		}
		*/
		//
		// 1. Сначала выбираем товары, которые прямо заданы в связях "Похожие блюда"
		//
		$iSiteMenuIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
		$arSelect = array(
			'ID',
			'NAME',
			'CODE',
			'PROPERTY_RELATED_PRODUCTS',
			'IBLOCK_SECTION_ID',
			'PROPERTY_RELATED_PRODUCTS_DESCRIPTION',
		);
		$rsItems = CIBlockElement::GetList(
			array(
				'PROPERTY_RELATED_PRODUCTS_DESCRIPTION' => 'ASC',
			), 
			array(
				'ID' => $iProductId,
				'IBLOCK_ID' => $iSiteMenuIBlockId,
			), 
			false,
			false,
			$arSelect
		);

		if ($arItem = $rsItems->Fetch()) {
			$arExclProducts[] = $arItem['ID'];
			$iSectionId = $arItem['IBLOCK_SECTION_ID'];
			$related_products = array();
			$related_description = $arItem['PROPERTY_RELATED_PRODUCTS_DESCRIPTION'];
			asort($related_description);
			foreach ($related_description as $key => $el) {
				$related_products[$key] = $arItem['PROPERTY_RELATED_PRODUCTS_VALUE'][$key];
			}
			foreach ($related_products as $key => $iId) {
				// Если товар есть в корзине, то его не показываем в похожих
				if (in_array($iId, $arExclProducts)) {
					continue;
				}

				$arExclProducts[] = $iId;

				$arProduct = CNiyamaCatalog::GetProductDetail($iId);
				if ($arProduct) {
					$arReturn[] = $arProduct;
					$iCurCount++;
				}

				if ($iCurCount >= $iMaxCount) {
					break;
				}
			}
		}

		//
		// 2. Если лимит не исчерпан, то находим товары, у которых заданный указан в похожих
		//
		if ($iCurCount < $iMaxCount) {
			$arSelect = array(
				'ID',
				'NAME',
				'CODE',
				'PROPERTY_RELATED_PRODUCTS',
			);
			$rsItems = CIBlockElement::GetList(
				array(
					'PROPERTY_MARGINALITY_WEIGHT_VALUE' => 'ASC',
					'SHOW_COUNTER' => 'DESC',
				), 
				array(
					'PROPERTY_RELATED_PRODUCTS' => $iProductId,
					'IBLOCK_ID' => $iSiteMenuIBlockId,
					'ACTIVE' => 'Y',
					'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
					'!ID' => $arExclProducts,
				), 
				false, 
				array(
					'nTopCount' => $iMaxCount - $iCurCount
				), 
				$arSelect
			);

			while ($arItem = $rsItems->Fetch()) {
				$arExclProducts[] = $arItem['ID'];
				$arProduct = self::GetProductDetail($arItem['ID']);
				if ($arProduct) {
					$arReturn[] = $arProduct;
					$iCurCount++;
				}
			}
		}

		//
		// 3. Если лимит не исчерпан, то подбираем похожие товары из всех остальных
		//
		if ($iCurCount < $iMaxCount && $iSectionId /*&& !empty($arCurGeneralIng)*/) {
			$arSelect = array(
				'ID',
				'NAME',
				'CODE',
			);

			$arFilter = array(
				'IBLOCK_ID' => $iSiteMenuIBlockId,
				'ACTIVE' => 'Y',
				'SECTION_ID' => $iSectionId,
				'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
				'!ID' => $arExclProducts,
			);

			/*
			foreach ($arCurGeneralIng as $key=>$el){
				$arFilter[]=array('ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID, 'PROPERTY_GENERAL_INGREDIENT' => $key)));
			}
			*/
			$rsItems = CIBlockElement::GetList(
				array(
					'PROPERTY_MARGINALITY_WEIGHT_VALUE' => 'ASC',
					'SHOW_COUNTER' => 'DESC',
				), 
				$arFilter,
				false, 
				array(
					'nTopCount' => $iMaxCount - $iCurCount
				), 
				$arSelect
			);
			while ($arItem = $rsItems->Fetch()) {
				$arProduct = self::GetProductDetail($arItem['ID']);

				if ($arProduct) {
					$arReturn[] = $arProduct;
					$iCurCount++;
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Рекомендуемые товары для заданного
	 * @author: t.k
	 * @param $sProductCode
	 * @return array
	 */
	public static function GetRecProducts($sProductCode)
	{
		$arReturn = array();

		$iMaxCount = intval(CNiyamaCustomSettings::getStringValue('recommended_products_count', 10));
		$iCurCount = 0;
		// Товары, исключаемые из автоматического списка
		$arExclProducts = array();
		// Если на этом хите уже дергали корзину, то список айдишников должен быть готов
		if (isset($GLOBALS['CART_PRODUCT_IDS']) && is_array($GLOBALS['CART_PRODUCT_IDS'])) {
			if ($GLOBALS['CART_PRODUCT_IDS']) {
				$arExclProducts = array_keys($GLOBALS['CART_PRODUCT_IDS']);
			}
		} else {
			$arCart = CNiyamaCart::getCartList();
			if ($arCart['PRODUCT_IDS']) {
				$arExclProducts = array_keys($arCart['PRODUCT_IDS']);
			}
		}


		$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
		$arSelect = array(
			'ID'
		);
		$rsItems = CIBlockElement::GetList(
			array(
				'ID' => 'ASC'
			), 
			array(
				'IBLOCK_ID' => $IBLOCK_ID,
				'=CODE' => $sProductCode,
			),
			false,
			false,
			$arSelect
		);
		if($arItem = $rsItems->Fetch()) {
			$arExclProducts[] = $arItem['ID'];
		}
		//
		// Сначала получаем вручную заданные связи
		//
		$IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('recommended', 'catalog');
		$arSelect = array(
			'ID',
			'NAME',
			'CODE',
			'PROPERTY_PRODUCTS',
			'PROPERTY_CHEF',
			'PROPERTY_PRODUCT_ID',
		);
		$rsItems = CIBlockElement::GetList(
			array(
				'ID' => 'ASC'
			),
			array(
				'IBLOCK_ID' => $IBLOCK_ID,
				'PROPERTY_PRODUCT_ID.ACTIVE' => 'Y',
				'PROPERTY_PRODUCT_ID.CODE' => $sProductCode,
			),
			false,
			false,
			$arSelect
		);
		if($arItem = $rsItems->Fetch()) {
			$arExclProducts[] = $arItem['PROPERTY_PRODUCT_ID_VALUE'];
			foreach ($arItem['PROPERTY_PRODUCTS_VALUE'] as $key => $iProductId) {

				// Если товар есть в корзине, то его не показываем в рекомендуемых
				if (in_array($iProductId, $arExclProducts)) {
					continue;
				}

				$arExclProducts[] = $iProductId;

				$arProduct = CNiyamaCatalog::GetProductDetail($iProductId);
				if ($arProduct) {
					$arProduct['CHEF'] = $arItem['PROPERTY_PRODUCTS_DESCRIPTION'][$key];
					$arReturn[] = $arProduct;
					$iCurCount++;
				}

				if ($iCurCount >= $iMaxCount) {
					break;
				}
			}
		}
		//
		// Если лимит не исчерпан, то формируем автоматические связи
		//
		if ($iCurCount < $iMaxCount) {

			$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
			$rsItems = CIBlockElement::GetList(
				array(
					'PROPERTY_MARGINALITY_WEIGHT_VALUE' => 'ASC',
					'SHOW_COUNTER' => 'DESC',
				), 
				array(
					'!ID' => $arExclProducts,
					'IBLOCK_ID' => $IBLOCK_ID,
					'ACTIVE' => 'Y',
					'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
					'PROPERTY_MARGINALITY' => 1,
				),
				false,
				array(
					'nTopCount' => $iMaxCount - $iCurCount
				),
				array(
					'ID',
				)
			);
			while ($arItem = $rsItems->Fetch()) {
				$arProduct = CNiyamaCatalog::GetProductDetail($arItem['ID']);
				if ($arProduct) {
					$arProduct['CHEF'] = false;
					$arReturn[] = $arProduct;
				}
			}
		}

		return $arReturn;
	}

	/**
	 * GetAllProductsIngredientsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetAllProductsIngredientsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'GetAllProductsIngredientsList';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');
			$iIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
			$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
			$iIngredientsIBlockId = CProjectUtils::GetIBlockIdByCode('catalog-ingredients');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$dbItems = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
					'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
				), 
				false,
				false,
				array(
					'ID',
					'PROPERTY_GENERAL_INGREDIENT.ID', 'PROPERTY_GENERAL_INGREDIENT.NAME',
					'PROPERTY_MORE_INGREDIENT.ID', 'PROPERTY_MORE_INGREDIENT.NAME',
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PROPERTY_GENERAL_INGREDIENT_ID']) {
					$arReturn[$arItem['ID']]['GENERAL_INGREDIENT'][$arItem['PROPERTY_GENERAL_INGREDIENT_ID']] = $arItem['PROPERTY_GENERAL_INGREDIENT_NAME'];
					$arReturn[$arItem['ID']]['ALL_INGREDIENT'][$arItem['PROPERTY_GENERAL_INGREDIENT_ID']] = $arItem['PROPERTY_GENERAL_INGREDIENT_NAME'];
				}
				if($arItem['PROPERTY_MORE_INGREDIENT_ID']) {
					$arReturn[$arItem['ID']]['GENERAL_INGREDIENT'][$arItem['PROPERTY_MORE_INGREDIENT_ID']] = $arItem['PROPERTY_MORE_INGREDIENT_NAME'];
					$arReturn[$arItem['ID']]['ALL_INGREDIENT'][$arItem['PROPERTY_MORE_INGREDIENT_ID']] = $arItem['PROPERTY_MORE_INGREDIENT_NAME'];
				}
			}

			// !!! важно добавить теги вручную, т.к. модуль инфоблоков не добавляет сам инфоблоки по фильтруемым связям!!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId, 'iblock_id_'.$iImportMenuIBlockId, 'iblock_id_'.$iIngredientsIBlockId));

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function getAllIngredients($arProductsId, $bRefreshCache = false) {
		$arReturn = array();
		$arProductsId = !is_array($arProductsId) ? array($arProductsId) : $arProductsId;
		if(!empty($arProductsId)) {
			$arAllProductsIngredientsList = self::GetAllProductsIngredientsList($bRefreshCache);
			foreach($arProductsId as $iProductId) {
				if($iProductId) {
					$arReturn[$iProductId] = $arAllProductsIngredientsList[$iProductId] ? $arAllProductsIngredientsList[$iProductId] : array();
				}
			}
		}
		return $arReturn;
	}

	/**
	 * GetAllProductsIngredientsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetAllProductsExIngredientsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'GetAllExProductsIngredientsList';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');
			$iIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();

			$iImportMenuIBlockId = CNiyamaIBlockCatalogBase::GetIBlockId();
			$iExIngredientsIBlockId = CProjectUtils::GetIBlockIdByCode('catalog-ex-ingredients');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$dbItems = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
					'PROPERTY_IMPORT_ELEMENT.ACTIVE' => 'Y',
				), 
				false,
				false,
				array(
					'ID',
					'PROPERTY_EXCLUDE_INGREDIENT.ID', 'PROPERTY_EXCLUDE_INGREDIENT.NAME',
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PROPERTY_EXCLUDE_INGREDIENT_ID']) {
					$arReturn[$arItem['ID']]['EXCLUDE_INGREDIENT'][$arItem['PROPERTY_EXCLUDE_INGREDIENT_ID']] = $arItem['PROPERTY_EXCLUDE_INGREDIENT_NAME'];
				}
			}

			// !!! важно добавить теги вручную, т.к. модуль инфоблоков не добавляет сам инфоблоки по фильтруемым связям!!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId, 'iblock_id_'.$iImportMenuIBlockId, 'iblock_id_'.$iExIngredientsIBlockId));

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function getAllExIngredients($arProductsId, $bRefreshCache = false) {
		$arReturn = array();
		$arProductsId = !is_array($arProductsId) ? array($arProductsId) : $arProductsId;
		if(!empty($arProductsId)) {
			$arAllProductsIngredientsList = self::GetAllProductsExIngredientsList($bRefreshCache);
			foreach($arProductsId as $iProductId) {
				if($iProductId) {
					$arReturn[$iProductId] = $arAllProductsIngredientsList[$iProductId] ? $arAllProductsIngredientsList[$iProductId] : array();
				}
			}
		}
		return $arReturn;
	}

	public static function getApseilProduct($faset = array('menu' => array(), 'ex_menu' => array(), 'ings' => array(), 'ex_ings' => array()), $user_id = false) {
		// ???
	}

	/**
	 * getAuctionProducts
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getAuctionProductsList($bRefreshCache = false)
	{
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getAuctionProducts';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {

			$iIBlockId = CProjectUtils::GetIBlockIdByCode('auctions_products', 'catalog');
			$iSiteMenuIBlockId = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			CModule::IncludeModule('iblock');

			$arSelect  = array(
				'ID',
				'NAME',
				'CODE',
				'PRODUCT_ID.SHOW_COUNTER',
				'PROPERTY_PRODUCT_ID'
			);

			$arOrder = array(
				'PRODUCT_ID.SHOW_COUNTER' => 'DESC',
			);

			$arFilter = array (
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => $iIBlockId,
			);

			$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
			while($arItem = $rsItems->Fetch()) {
				$arItem['PRODUCT_DETAIL'] = CNiyamaCatalog::GetProductDetail($arItem['PROPERTY_PRODUCT_ID_VALUE']);
				$arReturn[] = $arItem;
			}

			// !!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId, 'iblock_id_'.$iSiteMenuIBlockId));

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}


	/**
	 * getAuctionProducts
	 * @param bool $isRand
	 * @return mixed
	 */
	public static function getAuctionProducts($isRand = true)
	{
		$arProducts = self::getAuctionProductsList();
		$arExclProducts = array();
		// Если на этом хите уже дергали корзину, то список айдишников должен быть готов
		if (isset($GLOBALS['CART_PRODUCT_IDS']) && is_array($GLOBALS['CART_PRODUCT_IDS'])) {
			if ($GLOBALS['CART_PRODUCT_IDS']) {
				$arExclProducts = array_keys($GLOBALS['CART_PRODUCT_IDS']);
			}
		}
		else {
			$arCart = CNiyamaCart::getCartList();
			if ($arCart['PRODUCT_IDS']) {
				$arExclProducts = array_keys($arCart['PRODUCT_IDS']);
			}
		}
		
		# Исключаем продукты которые есть в корзине
		$arTmpReturn = array();
		foreach ( $arProducts as $arProductsItem ){
			if (!in_array($arProductsItem['PROPERTY_PRODUCT_ID_VALUE'], $arExclProducts)) {
				$arTmpReturn[] = $arProductsItem;
			}
		}
		if ($isRand) {
			if (!empty($arTmpReturn)){
				$rand_keys = array_rand($arTmpReturn, 1);
				return $arTmpReturn[$rand_keys];
			}
			return $arTmpReturn;
		}
		return $arTmpReturn[0];
	}


	public static function getMenuByCode($Code, $bRefreshCache = false)
	{

		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $Code;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getMenuByCode';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/getMenuByCode/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');
			$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$by = "SORT";
			$order = "DESC";
			$obRes = CIBlockSection::GetList(
				array(
					$by => $order
				),
				array(
					'IBLOCK_ID' => $IBLOCK_ID,
					'=CODE' => $Code,
					'GLOBAL_ACTIVE' => 'Y'
				),
				true
			);
			if($arItem = $obRes->GetNext()) {
				if ($arItem['CODE'] == $Code) {
					$arReturn[] = $arItem;
				}
			}
			if (empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}

		if (!empty($arReturn)) {
			return $arReturn[0];
		}
		return false;
	}


	/**
	 * getMenu
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getMenu($bRefreshCache = false)
	{
		$by = "SORT";
		$order = "DESC";
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $by.'|'.$order;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getMenu';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');

			$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$obRes = CIBlockSection::GetList(
				array(
					$by => $order
				), 
				array(
					'IBLOCK_ID' => $IBLOCK_ID,
					'GLOBAL_ACTIVE' => 'Y'
				), 
				true
			);
			while($arItem = $obRes->GetNext()) {
				$arReturn[] = $arItem;
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getCategoriesByFilter
	 * @param array $arFilter
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getCategoriesByFilter($arPFilter = null, $bRefreshCache = false)
	{
		$IBLOCK_ID = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
		$arFilter = array(
			'IBLOCK_ID' => $IBLOCK_ID,
			'GLOBAL_ACTIVE' => 'Y'
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, $arPFilter);
			}
		}

		$by = 'SORT';
		$order = 'DESC';
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v1'.md5(serialize(array($IBLOCK_ID, $arFilter, $by, $order)));
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getCategoriesByFilter';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			$obRes = CIBlockSection::GetList(
				array(
					$by => $order
				), 
				$arFilter, 
				true
			);
			while($arItem = $obRes->GetNext()) {
				$arReturn[] = $arItem;
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getIngredients
	 * @param null $arPSelect
	 * @param null $arPFilter
	 * @param null $arPOrder
	 * @param null $arPNavStartParams
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getIngredients($arPSelect = null, $arPFilter = null, $arPOrder = null, $arPNavStartParams = null,  $bRefreshCache = false)
	{
		$IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('catalog-ingredients', 'catalog');
		$arSelect  = array(
			'ID',
			'NAME',
			'CODE'
		);
		if (!is_null($arPSelect)) {
			if (is_array($arPSelect)) {
				$arSelect = array_merge($arSelect, $arPSelect);
			}
		}
		$arFilter = array (
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => $IBLOCK_ID,
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, $arPFilter);
			}
		}

		$arOrder = array(
			'SORT' => 'ASC',
		);
		if($arPOrder && is_array($arPOrder)) {
			$arOrder = $arPOrder;
		}

		$arNavStartParams = false;
		if($arPNavStartParams && is_array($arPNavStartParams)) {
			$arNavStartParams = $arPNavStartParams;
		}

		$arReturn = array();
		$sCacheAddParam = md5(serialize(array($arFilter, $arOrder, $arSelect, $arNavStartParams)));
		$sCacheId = 'getIngredients';
		$arAddCacheTags = array(__METHOD__);
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		$bUseStaticCache = true;
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);
		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
			while($arItem = $rsItems->Fetch()) {
				$arReturn[] = $arItem;
			}

			if (empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getExIngredients
	 * @param null $arPSelect
	 * @param null $arPFilter
	 * @param null $arPOrder
	 * @param null $arPNavStartParams
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getExIngredients($arPSelect = null, $arPFilter = null, $arPOrder = null, $arPNavStartParams = null,  $bRefreshCache = false)
	{
		$IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('catalog-ex-ingredients', 'catalog');
		$arSelect  = array(
			'ID',
			'NAME',
			'CODE'
		);
		if (!is_null($arPSelect)) {
			if (is_array($arPSelect)) {
				$arSelect = array_merge($arSelect, $arPSelect);
			}
		}
		$arFilter = array (
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => $IBLOCK_ID,
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, $arPFilter);
			}
		}

		$arOrder = array(
			'SORT' => 'ASC',
		);
		if($arPOrder && is_array($arPOrder)) {
			$arOrder = $arPOrder;
		}

		$arNavStartParams = false;
		if($arPNavStartParams && is_array($arPNavStartParams)) {
			$arNavStartParams = $arPNavStartParams;
		}

		$arReturn = array();
		$sCacheAddParam = md5(serialize(array($arFilter, $arOrder, $arSelect, $arNavStartParams)));
		$sCacheId = 'getExIngredients';
		$arAddCacheTags = array(__METHOD__);
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		$bUseStaticCache = true;
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);
		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
			while($arItem = $rsItems->Fetch()) {
				$arReturn[] = $arItem;
			}

			if (empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getSpecMenu
	 * @param null $arPSelect
	 * @param null $arPFilter
	 * @param null $arPOrder
	 * @param null $arPNavStartParams
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function getSpecMenu($arPSelect = null, $arPFilter = null, $arPOrder = null, $arPNavStartParams = null,  $bRefreshCache = false)
	{
		$IBLOCK_ID = CProjectUtils::GetIBlockIdByCodeEx('catalog-special-menu');
		$arSelect  = array(
			'ID',
			'NAME',
			'CODE'
		);
		if (!is_null($arPSelect)) {
			if (is_array($arPSelect)) {
				$arSelect = array_merge($arSelect, $arPSelect);
			}
		}
		$arFilter = array (
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => $IBLOCK_ID,
			'CHECK_PERMISSIONS' => 'N',
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, $arPFilter);
			}
		}

		$arOrder = array(
			'SORT' => 'ASC',
		);
		if($arPOrder && is_array($arPOrder)) {
			$arOrder = $arPOrder;
		}

		$arNavStartParams = array();
		if($arPNavStartParams && is_array($arPNavStartParams)) {
			$arNavStartParams = $arPNavStartParams;
		}

		$arReturn = array();
		$sCacheAddParam = md5(serialize(array($arFilter, $arOrder, $arSelect, $arNavStartParams)));
		$sCacheId = 'getSpecMenu';
		$arAddCacheTags = array(__METHOD__);
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		$bUseStaticCache = true;
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);
		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
			while($arItem = $rsItems->Fetch()) {
				$arReturn[] = $arItem;
			}

			if (empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getAllMenuElements
	 * @return array
	 */
	public static function getAllMenuElements()
	{
		$arReturn = array();
		$arIngredients = self::getIngredients();
		foreach($arIngredients as $arIng) {
			$arReturn[$arIng['ID']] = $arIng['NAME'];
		}
		$arExIngredients = self::getExIngredients();
		foreach($arExIngredients as $arExIng) {
			$arReturn[$arExIng['ID']] = $arExIng['NAME'];
		}		
		$arMenus = self::getMenu();
		foreach($arMenus as $arMenu) {
			$arReturn[$arMenu['ID']] = $arMenu['NAME'];
		}
		$arSpeMenus = self::getSpecMenu();
		foreach($arSpeMenus as $arSpec) {
			$arReturn[$arSpec['ID']] = $arSpec['NAME'];
		}
		return $arReturn;
	}


	public static function getTpNamesOfIngredients()
	{
		$arReturn = array();
		$arIngredients = self::getIngredients(array('PROPERTY_TP_NAME'));
		foreach($arIngredients as $arIng) {
			$arReturn[$arIng['ID']] = $arIng['PROPERTY_TP_NAME_VALUE'];
		}
		return $arReturn;
	}

	public static function getMenuNames()
	{
		$arMenus = self::getMenu();
		foreach($arMenus as $arMenu) {
			$arReturn[$arMenu['ID']] = $arMenu['NAME'];
		}
		return $arReturn;
	}


	/**
	 * Получение ID товаров из заказов сортированых по частоте заказов
	 * GetOrderProductsIds
	 * @param bool $iUserId
	 * @param $bRefreshCache
	 * @return array
	 */
	public static function GetOrderProductsIds($iUserId = false, $bRefreshCache = false) {
		$arReturn = array();
		$iUserId = intval($iUserId);
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iUserId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getOrderProductsIds';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/GetOrderProductsIds/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {

			CModule::IncludeModule('sale');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			
			//
			// LSV: o_O здесь перебирается вся база заказов и каталога, к тому же неправильно определяется статистика по товарам
			// упростил ниже и исправил логику
			//
			/*
			$arProductsIds = array();

			$arFilter = array(
				'@STATUS_ID' => array('E', 'F', 'N', 'P', 'R', 'S', 'T'),
				'LID' => 's1',
				'CANCELED' => 'N'
			);

			if($iUserId > 0) {
				$arFilter['USER_ID'] = $iUserId;
			}
			$dbOrder = CSaleOrder::GetList(
				array(
					'ID' => 'DESC'
				),
				$arFilter,
				false,
				false,
				array(
					'ID',
					'STATUS_ID',
					'PRICE',
					'DATE_INSERT',
					'CANCELED',
					'UF_*'
				)
			);
			while($arOrder = $dbOrder->Fetch()) {
				$arOrderCart = CNiyamaCart::GetCartListByOrderId($arOrder['ID']);

				if($arOrderCart['PRODUCT_IDS'] && is_array($arOrderCart['PRODUCT_IDS'])) {
					foreach($arOrderCart['PRODUCT_IDS'] as $row) {
						if(is_numeric($row)) {
							$arProductsIds[] = $row;
						}
					}
				}
			}

			if($arProductsIds) {
				$arReturn = array_count_values($arProductsIds);
			}
			/*/

			// получим статистику по количетсву заказываемых блюд
			$arFilter = array(
				'@ORDER_STATUS' => array('E', 'F', 'N', 'P', 'R', 'S', 'T'),
				'LID' => 's1',
				'ORDER_CANCELED' => 'N',
				'>PRODUCT_ID' => 1,
				'>ORDER_ID' => 0,
				'>PRICE' => 0
			);
			if($iUserId > 0) {
				$arFilter['USER_ID'] = $iUserId;
			}
			$dbItems = CSaleBasket::GetList(
				array(),
				$arFilter,
				array(
					'PRODUCT_ID',
					'QUANTITY'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PRODUCT_ID'] > 1) {
					$arReturn[$arItem['PRODUCT_ID']] = $arReturn[$arItem['PRODUCT_ID']] ? $arReturn[$arItem['PRODUCT_ID']] : 0;
					$arReturn[$arItem['PRODUCT_ID']] += intval($arItem['CNT'] * $arItem['QUANTITY']);
				}
			}
			//*/

			if($arReturn) {
				arsort($arReturn);
			} else {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}

		return $arReturn;
	}

	public static function GetProductListByFaset($arProductsId = array(), $faset = array('menu' => array(), 'ex_menu' => array(), 'ing' => array(), 'ex_ing' => array(), 'alerg' => array()), $bRefreshCache = false)
	{
		$arPFilter = array();
		$arPFilterMenu = false;
		if (!empty($faset['menu']) && (is_array($faset['menu']))) {
			$arPFilterMenu = true;
			$arPFilter['SECTION_ID'] = $faset['menu'];
			$arPFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		}
		if (!empty($faset['ex_menu']) && (is_array($faset['ex_menu']))) {
			$arPFilter['LOGIC'] = 'AND';
			$arPFilter['!SECTION_ID'] = $faset['ex_menu'];
		}
		$arIngredients=$faset['ing'];
		if (!empty($arIngredients) && (is_array($arIngredients))) {
			if ($arPFilterMenu) {
				$arPFilter['LOGIC'] = 'AND';
				$arPFilter[] = array(
					'LOGIC' => 'OR',
					array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
					array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
				);
			} else {
				// $arPFilter['LOGIC'] = 'OR';
				$arPFilter[] = array(
					'LOGIC' => 'OR',
					array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
					array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
				);
			}
		}
		$arExIngredients = $faset['ex_ing'];
		if (!empty($arExIngredients) && (is_array($arExIngredients))) {
				$IBLOCK_ID_genaral_menu = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
				$arPFilter['LOGIC'] = 'AND';
				$arPFilter[] = array(
					'LOGIC' => 'AND',
					array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_GENERAL_INGREDIENT' => $arExIngredients))),
					array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_MORE_INGREDIENT' => $arExIngredients))),
				);
		}
		$arExIngredients = $faset['alerg'];
		if (!empty($arExIngredients) && (is_array($arExIngredients))) {
			$IBLOCK_ID_genaral_menu = CNiyamaIBlockCatalogBase::GetSiteMenuIBlockId();
			$arPFilter['LOGIC'] = 'AND';
			$arPFilter[] = array(
				'LOGIC' => 'AND',
				array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_EXCLUDE_INGREDIENT' => $arExIngredients))),
			);
		}

		if (!empty($arProductsId)) {
			$arPFilter[] = array(
				'LOGIC' => 'AND',
				array(
					'ID' => $arProductsId
				),
			);
		}
		return self::GetList(null, $arPFilter, null, null, $bRefreshCache);
	}

	public static function GetOffersList() {

        $outputOffersList = array();

        if(CModule::IncludeModule('catalog')) {
            $iblockId = IntVal(CNiyamaIBlockCatalogBase::GetPizzapiIblockID());
            $arProductsList = array();
            $arProductsIds = array();

            $arSelect = Array(
                "ID",
                "NAME",
                "ACTIVE",
                "IBLOCK_SECTION_ID",
                "PREVIEW_PICTURE",
                "DETAIL_PICTURE",
                "PREVIEW_TEXT",
                "CODE",
                "XML_ID",
                "CATALOG_GROUP_2",
                "DETAIL_PAGE_URL"
            );
            $arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_GLOBAL_ACTIVE" => 'Y');
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

            while($ob = $res->GetNext())
            {
                $arProductsList[$ob['ID']] = $ob;
                $arProductsIds[] =  $ob['ID'];
            }

            $offersList = CCatalogSKU::getOffersList(
                $arProductsIds,
                $iblockId,
                $skuFilter = array(),
                $fields = array('NAME', 'ID', 'CODE', 'XML_ID', 'ACTIVE', 'CATALOG_GROUP_2'),
                $propertyFilter = array()
            );



            foreach ($offersList as $product => $offers) {

                foreach ($offers as $offer){
                    if(array_key_exists($product, $arProductsList)) {
                        $offer['PRODUCT'] = $arProductsList[$product];
                        $outputOffersList[$offer['ID']] = $offer;
                    }
                }


            }
        }

        return $outputOffersList;
	}
} 
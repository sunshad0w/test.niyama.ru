<?php

class CPA
{
    const COOKIENAME_PARTNER_NAME = 'cpa_partner';
    const COOKIENAME_TRAFFIC_IDENTIFIER = 'cpa_traffic';

    const ACTIONPAY_STATUS_ACCEPT = 1;
    const ACTIONPAY_STATUS_PROCESSING = 2;
    const ACTIONPAY_STATUS_REJECT = 3;

    public static function checkTraffic()
    {
        if (isset($_GET['utm_source'])) {
            $utm_source = $_GET['utm_source'];
        } else {
            $utm_source = false;
        }
        if (isset($_GET['utm_medium'])) {
            $utm_medium = $_GET['utm_medium'];
        } else {
            $utm_medium = false;
        }
        if (isset($_GET['utm_campaign'])) {
            $utm_campaign = $_GET['utm_campaign'];
        } else {
            $utm_campaign = false;
        }
        if (isset($_GET['utm_content'])) {
            $utm_content = $_GET['utm_content'];
        } else {
            $utm_content = false;
        }
        if (isset($_GET['utm_term'])) {
            $utm_term = $_GET['utm_term'];
        } else {
            $utm_term = false;
        }


        if ($utm_source && $utm_medium && $utm_campaign) {
            $cookies = array(
                'utm_source' => $utm_source,
                'utm_medium' => $utm_medium,
                'utm_campaign' => $utm_campaign,
            );

            switch ($utm_source) {
                case 'actionpay' :
                    $partner = null;
                    $traffic = null;

                    if (isset($_GET['actionpay'])) {
                        $partner = 'actionpay';
                        $traffic = $_GET['actionpay'];

                    } else if (isset($_GET['apclick']) && isset($_GET['apsource'])) {
                        $partner = 'actionpay';
                        $traffic = $_GET['apclick'] . '.' . $_GET['apsource'];
                    }

                    if ($partner && $traffic && $utm_source && $utm_medium && $utm_campaign && $utm_term) {
                        $traffic = htmlspecialchars($traffic);
                        $cookies[self::COOKIENAME_PARTNER_NAME] = $partner;
                        $cookies[self::COOKIENAME_TRAFFIC_IDENTIFIER] = $traffic;
                        $cookies['utm_term'] = $utm_term;
                        self::setCookies($cookies);
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 'gdeslon':
                    if ($utm_term) {
                        if ($_COOKIE['utm_term'] == $utm_term){
                            setcookie('gdeslon_repeat', true, time() + 60 * 60 * 24 * 16, '/');
                        } else {
                            setcookie('gdeslon_repeat', true, time() - 60 * 60 * 24 * 16, '/');
                        }
                        $cookies['utm_term'] = $utm_term;
                    }
                    self::setCookies($cookies);
                    return true;
                    break;
                case 'otclick':
                    if ($utm_content) {
                        $cookies['utm_content'] = $utm_content;
                    }
                    if ($utm_term) {
                        $cookies['utm_term'] = $utm_term;
                    }
                    self::setCookies($cookies);
                    return true;
                    break;

                default:
                    return false;
            }
        } else {
            return false;
        }
    }


    private function setCookies($cookies)
    {
        if (!empty($cookies)) {

            foreach ($cookies as $cookieName => $cookieValue) {
                setcookie($cookieName, $cookieValue, time() + 180 * 86400, '/');
                $_COOKIE[$cookieName] = $cookieValue;
            }

        }
    }


    /**
     * Возвращает имя последнего CPA-партнера, который привел данного пользователя
     * @return string|null
     */
    public static function getLastPartnerName()
    {
        if (isset($_COOKIE) && isset($_COOKIE[self::COOKIENAME_PARTNER_NAME])) {
            return $_COOKIE[self::COOKIENAME_PARTNER_NAME];
        }
        return null;
    }

    /**
     * Возвращает идентификатор перехода последнего CPA-партнера, который привел данного пользователя
     * @return string|null
     */
    public static function getLastTrafficIdentifer()
    {
        if (isset($_COOKIE) && isset($_COOKIE[self::COOKIENAME_TRAFFIC_IDENTIFIER])) {
            return $_COOKIE[self::COOKIENAME_TRAFFIC_IDENTIFIER];
        }
        return null;
    }

    /**
     * Возвращает ссылку на пиксель CPA-системы для уведомления о совершённом заказе
     *
     * @param $partnerName    string    имя CPA-партнера
     * @param $trafficId    string    идентификатор перехода
     * @param $orderId        string    номер заказа для отслеживания CPA-системой
     * @param $orderSum        float    сумма заказа
     * @return string|null
     */
    public static function getPixelUrl($partnerName, $trafficId, $orderId, $orderSum = 0.0, $action_aim_id = 0, $items = array(), $term = "", $aprtLastApclick, $aprtLastApsource, $gdeslonAimID="")
    {
        $merchID = 0;
        switch ($partnerName) {
            case 'actionpay':
                $url = '//apypxl.com/ok/' . $action_aim_id . '.png?actionpay=' . $trafficId . '&apid=' . $orderId . '&price=' . $orderSum;
                break;

            case 'gdeslon':
                $codes = "";
                foreach ($items as $item) {
                    $quantity = $item['QUANTITY'];
                    for ($i = 1; $i <= $quantity; $i++) {
                        $codes .= $item['FO_ID'] . ':' . $item['PRICE'] . ",";

                    }
                }
                $codes=substr($codes, 0, strlen($codes)-1);
                $url = 'https://www.gdeslon.ru/thanks.js?codes=' . $codes . ',' . $gdeslonAimID . ':' . $orderSum .'&order_id=' . $orderId . '&merchant_id=' . $action_aim_id;
                break;

            case 'otclick':
                $url = 'http://track.otclickcpa.ru/SPM0?adv_sub=' . $orderId . '&transaction_id=' . $term;
                break;

            case 'OptiTrack':
                $url = true;
                break;

            default:
                $url = null;
        }
        return $url;
    }

    public static function showPixel($partnerName, $trafficId, $orderId, $orderSum = 0.0, $action_aim_id = 0, $items = array(), $term = "", $aprtLastApclick, $aprtLastApsource, $gdeslonAimID = "")
    {
        $url = self::getPixelUrl($partnerName, $trafficId, $orderId, $orderSum, $action_aim_id, $items, $term, $aprtLastApclick, $aprtLastApsource, $gdeslonAimID);
        $pixel_html = "";
        if (isset($url) && isset($partnerName)) {
            switch ($partnerName) {
                case 'actionpay':
                    if ($_COOKIE['PayOrderID'] != $orderId) {
                        $pixel_html = "<img class='partner_pixel' src='" .$url ."'>";
                        setcookie("PayOrderID", $orderId, time() + 60 * 60 * 24 * 30);
                    }
                    return $pixel_html;
                    break;
                case 'gdeslon':
                    if ($_COOKIE['PayOrderID'] != $orderId) {
                        $pixel_html = '<script type="text/javascript" src="' . $url . '"></script>';
                        setcookie("PayOrderID", $orderId, time() + 60 * 60 * 24 * 30);
                    }
                    return $pixel_html;
                    break;

                case 'otclick':
                    if ($_COOKIE['PayOrderID'] != $orderId) {
                        $pixel_html = "<img class='partner_pixel' src='" .$url ."'>";
                        setcookie("PayOrderID", $orderId, time() + 60 * 60 * 24 * 30);
                    }
                    return $pixel_html;
                    break;

                default:
                    return "";
            }
        }
    }

}
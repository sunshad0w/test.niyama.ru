<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 29.08.2014
 * @time: 4:03
 */


use Bitrix\Main\Entity;

class TmpUsersTable extends Entity\DataManager {

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'niyama_tmpusers';
    }

    /**
     * @return mixed
     */
    public static function GetBaseEntity() {
        return CHLEntity::GetEntityByName('TmpUsersTable');
    }

    /**
     * @return mixed
     */
    public static function GetBaseEntityId() {
        return CHLEntity::GetEntityIdByName('TmpUsersTable');
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'UF_TIMESTAMP_X' => array(
                'data_type' => 'datetime',
            ),
            'UF_MODIFIED_BY' => array(
                'data_type' => 'integer',
            ),
            'UF_XML_ID' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_EMAIL' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateUfTmpEmail'),
            ),
            'UF_TMP_GANDER' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_NAME' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_SECOND_NAME' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_PHONE' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_BIRTH_DATE' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_NIYAMA' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_CARD_NUMBER' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_RATING' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_LEVEL' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_MEDALS' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_SOCIAL' => array(
                'data_type' => 'text',
            ),
            'UF_TMP_ORDERS' => array(
                'data_type' => 'text',
            ),
        );
    }
    public static function validateUfTmpEmail()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }

} 
<?
/**
 * Class UserModalShowTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_TIMESTAMP_X datetime optional
 * <li> UF_MODIFIED_BY int optional
 * <li> UF_XML_ID string optional
 * <li> UF_USER_ID string(255) optional
 * <li> UF_UID string(255) optional
 * <li> UF_SERVICES string optional
 * <li> UF_IDENTITY string optional
 * </ul>
 *
 * @package Bitrix\Usersoc
 **/

use Bitrix\Main\Entity;
use Bitrix\Main\Type as FieldType;

class UserModalShowTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'niyama_usermodalshow';
    }

    /**
     * @return mixed
     */
    public static function GetBaseEntity() {
        return CHLEntity::GetEntityByName('UserModalShow');
    }

    /**
     * @return mixed
     */
    public static function GetBaseEntityId() {
        return CHLEntity::GetEntityIdByName('UserModalShow');
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'UF_TIMESTAMP_X' => array(
                'data_type' => 'datetime',
            ),
            'UF_MODIFIED_BY' => array(
                'data_type' => 'integer',
            ),
            'UF_XML_ID' => array(
                'data_type' => 'text',
            ),
            'UF_USER_ID' => array(
                'data_type' => 'string',
            ),
            'UF_TYPE_ID' => array(
                'data_type' => 'string',
            ),
			'UF_ELEM_ID' => array(//ID связанного елемента
                'data_type' => 'integer',
            ),
        );
    }

    public static function add($param)
    {
        /* Дата создания */
        $param['UF_TIMESTAMP_X'] = new FieldType\DateTime();
        return parent::add($param);
    }

}
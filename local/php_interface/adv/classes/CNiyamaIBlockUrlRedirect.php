<?
/**
 * Инфблок "Ссылки для перенаправления"
 *
 * Генерирует файл /url_redirect.php, который потом подключается и анализируется на странице /404.php
 *
 * @author Sergey Leshchenko, 2015
 * @updated: 10.09.2015
 */

class CNiyamaIBlockUrlRedirect {
	private static $sRedirectFilePath = '/url_redirect.php';

	/**
	 * GetIBlockId
	 */
	public static function GetIBlockId() {
		static $iIBlockId = 0;
		if(!$iIBlockId) {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('url_redirect', 'settings');
		}
		return $iIBlockId;
	}

	/**
	 * GetCurUserPermissions
	 */
	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	/**
	 * OnAfterIBlockElementAddHandler
	 * @param $arFields
	 */
	public function OnAfterIBlockElementAddHandler(&$arFields) {
		if($arFields['IBLOCK_ID'] == self::GetIBlockId()) {
			self::GenRedirectFile();
		}
	}

	/**
	 * OnAfterIBlockElementUpdateHandler
	 */
	public static function OnAfterIBlockElementUpdateHandler(&$arFields) {
		if($arFields['IBLOCK_ID'] == self::GetIBlockId()) {
			self::GenRedirectFile();
		}
	}

	/**
	 * OnAfterIBlockElementDeleteHandler
	 */
	public static function OnAfterIBlockElementDeleteHandler($arFields) {
		if($arFields['ID'] && $arFields['IBLOCK_ID'] && $arFields['IBLOCK_ID'] == self::GetIBlockId()) {
			self::GenRedirectFile();
		}
	}

	protected static function GetFilePath($bGetAbs = false) {
		// DIRECTORY_SEPARATOR
		$sReturn = $bGetAbs ? rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/'.self::$sRedirectFilePath : self::$sRedirectFilePath;
		return $sReturn;
	}

	/**
	 * Генерирует файл, который подключается в коде скрипта 404
	 */
	public static function GenRedirectFile() {
		$bReturn = false;
		$iIBlockId = self::GetIBlockId();
		if(!$iIBlockId) {
			return $bReturn;
		}
		CModule::IncludeModule('iblock');
		$arRedirectList = array();
		$dbItems = CIBlockElement::GetList(
			array(
				'SORT' => 'ASC',
				'ID' => 'ASC'
			),
			array(
				'IBLOCK_ID' => $iIBlockId,
				'ACTIVE' => 'Y',
				'ACTIVE_DATE' => 'Y'
			),
			false,
			false,
			array(
				'IBLOCK_ID', 'ID',
				'PROPERTY_INCOMING_URL', 'PROPERTY_REDIRECT_URL', 'PROPERTY_REDIRECT_STATUS'
			)
		);
		while($arItem = $dbItems->Fetch()) {
			if($arItem['PROPERTY_INCOMING_URL_VALUE'] && $arItem['PROPERTY_REDIRECT_URL_VALUE']) {
				$arRedirectList[$arItem['PROPERTY_INCOMING_URL_VALUE']] = array(
					'url' => $arItem['PROPERTY_REDIRECT_URL_VALUE'],
					'status' => $arItem['PROPERTY_REDIRECT_STATUS_VALUE']
				);
			}
		}

		$sNl = "\n";
		// генерируем содержимое файла
		$sFileCont = '';
		$sFileCont .= '<?'.$sNl;
		$sFileCont .= '// Содержимое файла генерируется динамически из инфоблока url_redirect'.$sNl;
		$sFileCont .= '$arIBlockUrlRedirect = '.var_export($arRedirectList, true).';'.$sNl;
		// сохраняем файл
		$sResultFilePathAbs = self::GetFilePath(true);
		CheckDirPath($sResultFilePathAbs);
		if(file_put_contents($sResultFilePathAbs, $sFileCont)) {
			$bReturn = true;
		}
		return $bReturn;
	}

}

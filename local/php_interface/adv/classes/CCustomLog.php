<?
/**
 * Класс лога
 * 
 * @author Sergey Leshchenko, 2012
 * @updated: 31.12.2014
 */

class CCustomLog {
	private $sAbsLogDir = '';
	private $sLogFilename = '';
	private $sAbsLogFilePath = '';
	private $bLogIfAdmin = false;
	private $bPathInited = false;
	private $bPathCorrect = false;
	private $sRowSeparator = "\n----------------------------------------\n";
	private $bAddTimestamp = true;

	/**
	 * Конструктор
	 *
	 * @param string $sAbsLogFilePath абсолютный путь к файлу лога
	 * @param bool $bLogIfAdmin записывать в лог только если текущий пользователь является администратором сайта, необязательный параметр, по умолчанию false
	 * @param bool $bAddTimestamp добавлять время в запись лога, по умолчанию true
	 * @return void
	 */
	public function __construct($sAbsLogFilePath, $bLogIfAdmin = false, $bAddTimestamp = true) {
		$sAbsLogFilePath = trim($sAbsLogFilePath);
		$this->bLogIfAdmin = $bLogIfAdmin ? true : false;
		$this->bAddTimestamp = $bAddTimestamp ? true : false;

		// проверим, чтобы был задан путь именно к файлу, а не папке
		if(strlen($sAbsLogFilePath) && strrpos($sAbsLogFilePath, '/') != (strlen($sAbsLogFilePath) - 1)) {
			$arPathInfo = pathinfo($sAbsLogFilePath);
			$this->sAbsLogDir = rtrim($arPathInfo['dirname'], '/').'/';
			$this->sLogFilename = $arPathInfo['basename'];
		}
	}

	/**
	 * Добавление записи в лог
	 *
	 * @param mixed $mLogData данные для добавления лога, нескалярные данные будут преобразованы в строку функцией print_r()
	 * @return void
	 */
	public function Log($mLogData) {
		$sLogText = $mLogData;
		if(!is_scalar($sLogText)) {
			$sLogText = print_r($sLogText, true);
		}
		$this->DoLog($sLogText);
	}

	/**
	 * Очистка файла лога
	 *
	 * @return void
	 */
	public function ClearLog() {
		if($this->IsLogPath()) {
			file_put_contents($this->GetLogPath(), '');
		}		
	}

	/**
	 * Удаление файла лога
	 *
	 * @return void
	 */
	public function DeleteLog() {
		$bUnlinked = false;
		if($this->IsLogPath()) {
			$sPath = $this->GetLogPath();
			if(file_exists($sPath)) {
				$bUnlinked = unlink($sPath);
			}
		}
		return $bUnlinked;
	}

	/**
	 * @ignore
	 */
	protected function InitLogPath() {
		if(!$this->bPathInited) {
			$this->bPathInited = true;
			if(strlen($this->sAbsLogDir)) {
				// создаем все 
				CheckDirPath($this->sAbsLogDir);
				if(is_dir($this->sAbsLogDir)) {
					$this->sAbsLogFilePath = $this->sAbsLogDir.$this->sLogFilename;
					$this->bPathCorrect = true;
				}
			}
		}
	}
	
	/**
	 * Устанавливает разделитель строк
	 *
	 * @param string $sRowSeparator строка-разделитель
	 * @return void
	 */
	public function SetRowSeparator($sRowSeparator) {
		if(is_scalar($sRowSeparator)) {
			$this->sRowSeparator = $sRowSeparator;
		}
	}

	/**
	 * Возвращает текущий разделитель строк
	 *
	 * @return string
	 */
	public function GetRowSeparator() {
		return $this->sRowSeparator;
	}

	/**
	 * Проверяет успешность создания файла лога
	 *
	 * @return bool
	 */
	public function IsLogPath() {
		$this->InitLogPath();
		return $this->bPathCorrect;
	}

	/**
	 * Возвращает путь к файлу лога
	 *
	 * @return string
	 */
	public function GetLogPath() {
		$this->InitLogPath();
		return $this->sAbsLogFilePath;
	}

	/**
	 * @ignore
	 */
	protected function DoLog($sLogText, $bLogIfAdmin = false) {
		$bCont = true;
		if($this->IsLogPath()) {
			if($bLogIfAdmin || $this->bLogIfAdmin) {
				if(!$GLOBALS['APPLICATION']->IsAdmin()) {
					$bCont = false;
				}
			}
		
			if($bCont) {
				$sLogSaveText = '';
				if($this->bAddTimestamp) {
					$sLogSaveText .= '['.date('d.m.Y H:i:s').']'."\n";
				}
				$sLogSaveText .= $sLogText."\n".$this->GetRowSeparator();
				file_put_contents($this->GetLogPath(), $sLogSaveText, FILE_APPEND);
			}
		}
	}
}

<?
/**
 *
 * Класс утилит для инфблока "Станции метро"
 * @author Sergey Leshchenko, 2014
 * @updated: 30.10.2014
 *
 */

class CNiyamaIBlockSubway {

	public static function GetIBlockId() {
		return CProjectUtils::GetIBlockIdByCode('subway', 'data');
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	//
	// Актуализация справочника Станции метро из FastOperator
	//
	public static function ImportData() {
		$bResult = false;
		CModule::IncludeModule('iblock');
		$iIBlockId = self::GetIBlockId();
		$obFastOperator = new CNiyamaFastOperator();
		$arImportData = $obFastOperator->GetMetroArray();
		if(!$arImportData['Items'] || !$iIBlockId) {
			return $bResult;
		}

		$bResult = true;

		$sUpdateTmpIdField = time();

		// работа с элементами
		if($arImportData['Items']) {
			// обновление элементов
			$arChunks = array_chunk($arImportData['Items'], 1000);
			foreach($arChunks as $arChunkItem) {
				$arUpdateItemsList = array();
				foreach($arChunkItem as $arTmpItem) {
					// генерируем код сами
					$arTmpItem['Code'] = md5($arTmpItem['CityCode'].'||'.ToUpper($arTmpItem['Name']));
					$arUpdateItemsList[$arTmpItem['Code']] = $arTmpItem;
				}

				if($arUpdateItemsList) {
					$dbItems = CIBlockElement::GetList(
						array(
							'ID' => 'ASC'
						),
						array(
							'IBLOCK_ID' => $iIBlockId,
							'=XML_ID' => array_keys($arUpdateItemsList),
							'CHECK_PERMISSIONS' => 'N',
						),
						false,
						false,
						array(
							'ID', 'XML_ID'
						)
					);

					while($arItem = $dbItems->Fetch()) {
						if($arUpdateItemsList[$arItem['XML_ID']]) {
							$arTmpUpdateItem = $arUpdateItemsList[$arItem['XML_ID']];
							$arFields = array(
								'NAME' => $arTmpUpdateItem['Name'],
								'ACTIVE' => 'Y',
								// !!!
								'TMP_ID' => $sUpdateTmpIdField,
							);
							$arProp = array(
								'CITY_CODE' => $arTmpUpdateItem['CityCode'],
								'LONG' => $arTmpUpdateItem['Long'],
								'LAT' => $arTmpUpdateItem['Lat'],
							);
							$obIBlockElement = new CIBlockElement();
							$obIBlockElement->Update($arItem['ID'], $arFields);
							CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iIBlockId, $arProp);
							// !!!
							unset($arUpdateItemsList[$arItem['XML_ID']]);
						}
					}
				}

				// добавление элементов
				if($arUpdateItemsList) {
					foreach($arUpdateItemsList as $arTmpUpdateItem) {
						$arFields = array(
							'IBLOCK_ID' => $iIBlockId,
							'NAME' => $arTmpUpdateItem['Name'],
							'ACTIVE' => 'Y',
							// !!!
							'XML_ID' => $arTmpUpdateItem['Code'],
							'TMP_ID' => $sUpdateTmpIdField,
						);
						$arProp = array(
							'CITY_CODE' => $arTmpUpdateItem['CityCode'],
							'LONG' => $arTmpUpdateItem['Long'],
							'LAT' => $arTmpUpdateItem['Lat'],
						);
						$arFields['PROPERTY_VALUES'] = $arProp;
						$obIBlockElement = new CIBlockElement();
						$obIBlockElement->Add($arFields);
					}
				}
			}

			// деактивируем элементы, которых не было в выгрузке
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'!TMP_ID' => $sUpdateTmpIdField,
					'CHECK_PERMISSIONS' => 'N',
				),
				false,
				array(
					'ID'
				)
			);
			while($arTmpItem = $dbItems->Fetch()) {
				$obIBlockElement = new CIBlockElement();
				$obIBlockElement->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
			}
		}
		return $bResult;
	}

	public static function GetAllStationsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = self::GetIBlockId();
			$arPropLineEnum = CCustomProject::GetEnumPropValues($iIBlockId, 'LINE');
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
					'CHECK_PERMISSIONS' => 'N',
				);
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					), 
					$arFilter, 
					false,
					false,
					array(
						'ID', 'NAME', 'CODE',
						'PROPERTY_LINE', 'PROPERTY_LONG', 'PROPERTY_LAT', 'PROPERTY_CITY_CODE'
					)
				);
				while($arItem = $dbItems->GetNext(true, false)) {
					$arTmpLine = array();
					if($arPropLineEnum[$arItem['PROPERTY_LINE_VALUE']]) {
						$arTmpLine['NAME'] = $arItem['PROPERTY_LINE_VALUE'];
						$arTmpLine['XML_ID'] = $arPropLineEnum[$arItem['PROPERTY_LINE_VALUE']]['XML_ID'];
					}
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'CODE' => $arItem['CODE'],
						'CITY_CODE' => trim($arItem['PROPERTY_CITY_CODE_VALUE']),
						'LINE' => $arTmpLine,
						'LONG' => $arItem['PROPERTY_LONG_VALUE'],
						'LAT' => $arItem['PROPERTY_LAT_VALUE'],
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetStationsListByFilter($bRefreshCache = false, $arPFilter = array()) {
		$iIBlockId = self::GetIBlockId();
		$arFilter = array(
			'IBLOCK_ID' => $iIBlockId,
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'N',
		);
		if($arPFilter && is_array($arPFilter)) {
			$arFilter = array_merge($arFilter, $arPFilter);
		}
		
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v1'.md5(serialize($arFilter));
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			
			$arPropLineEnum = CCustomProject::GetEnumPropValues($iIBlockId, 'LINE');
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					), 
					$arFilter, 
					false,
					false,
					array(
						'ID', 'NAME', 'CODE',
						'PROPERTY_LINE', 'PROPERTY_LONG', 'PROPERTY_LAT', 'PROPERTY_CITY_CODE'
					)
				);
				while($arItem = $dbItems->GetNext(true, false)) {
					$arTmpLine = array();
					if($arPropLineEnum[$arItem['PROPERTY_LINE_VALUE']]) {
						$arTmpLine['NAME'] = $arItem['PROPERTY_LINE_VALUE'];
						$arTmpLine['XML_ID'] = $arPropLineEnum[$arItem['PROPERTY_LINE_VALUE']]['XML_ID'];
					}
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'CODE' => $arItem['CODE'],
						'CITY_CODE' => trim($arItem['PROPERTY_CITY_CODE_VALUE']),
						'LINE' => $arTmpLine,
						'LONG' => $arItem['PROPERTY_LONG_VALUE'],
						'LAT' => $arItem['PROPERTY_LAT_VALUE'],
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}
}
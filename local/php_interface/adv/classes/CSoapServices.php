<?
/**
 *
 * SOAP
 * @author Sergey Leshchenko, 2014
 * @updated: xx.05.2014
 *
 */

class CSoapServices {
	private $obSoapClient = null;
	private $arParams = array();
	private $arLastRequestTrace = array();

	private function InitParams($arParams) {
		$arParams = is_array($arParams) ? $arParams : array();
		$this->arParams = array();

		$mDefVal = defined('SOAP_SERVICES_ENCODING') ? SOAP_SERVICES_ENCODING : 'UTF-8';
		$this->arParams['SOAP_ENCODING'] = isset($arParams['SOAP_ENCODING']) ? $arParams['SOAP_ENCODING'] : $mDefVal;

		$mDefVal = defined('SOAP_SERVICES_WSDL') ? SOAP_SERVICES_WSDL : '';
		$this->arParams['SOAP_WSDL'] = isset($arParams['SOAP_WSDL']) ? $arParams['SOAP_WSDL'] : $mDefVal;

		$mDefVal = defined('SOAP_SERVICES_LOGIN') ? SOAP_SERVICES_LOGIN : '';
		$this->arParams['SOAP_LOGIN'] = isset($arParams['SOAP_LOGIN']) ? $arParams['SOAP_LOGIN'] : $mDefVal;

		$mDefVal = defined('SOAP_SERVICES_PASSWORD') ? SOAP_SERVICES_PASSWORD : '';
		$this->arParams['SOAP_PASSWORD'] = isset($arParams['SOAP_PASSWORD']) ? $arParams['SOAP_PASSWORD'] : $mDefVal;

		$mDefVal = defined('SOAP_SERVICES_ENABLE_WSDL_CACHE') ? SOAP_SERVICES_ENABLE_WSDL_CACHE : true;
		$this->arParams['SOAP_ENABLE_WSDL_CACHE'] = isset($arParams['SOAP_ENABLE_WSDL_CACHE']) ? $arParams['SOAP_ENABLE_WSDL_CACHE'] : $mDefVal;

		$mDefVal = defined('SOAP_SERVICES_CONNECTION_TIMEOUT') ? SOAP_SERVICES_CONNECTION_TIMEOUT : 10;
		$this->arParams['SOAP_CONNECTION_TIMEOUT'] = isset($arParams['SOAP_CONNECTION_TIMEOUT']) ? intval($arParams['SOAP_CONNECTION_TIMEOUT']) : $mDefVal;
	}

	public function __construct($arParams = array(), $bSaveTrace = false) {
		$this->InitParams($arParams);

		$arOptions = array(
			'encoding' => $this->GetParam('SOAP_ENCODING'),
			'cache_wsdl' => $this->GetParam('SOAP_ENABLE_WSDL_CACHE') ? WSDL_CACHE_BOTH : WSDL_CACHE_NONE, // кэшируем в памяти и на диске
			'connection_timeout' => $this->GetParam('SOAP_CONNECTION_TIMEOUT'),
			'trace' => true, // трассировку выполняем всегда, т.к. возвращаются необходимые данные в заголовках
		);

		if(strlen($this->GetParam('SOAP_LOGIN'))) {
			$arOptions['login'] = $this->GetParam('SOAP_LOGIN');
			$arOptions['password'] = $this->GetParam('SOAP_PASSWORD');
		}

		// сохранять ли информацию для отладки
		$this->bSaveTrace = $bSaveTrace ? true : false;

		$arSoapHeaders = array();

		// подключение
		try {
			$this->obSoapClient = new SOAPClient($this->GetParam('SOAP_WSDL'), $arOptions);
			if(!empty($arSoapHeaders)) {
				$this->obSoapClient->__setSoapHeaders($arSoapHeaders);
			}
		} catch(SOAPFault $obSoapFault) {
			$this->obSoapClient = null;
			$arErr = $this->ParseSoapFault($obSoapFault);
			$this->ThrowException($arErr);
		}
	}

	public function Call($sFunctionName, $arParams = array(), $bReturnHeader = false, $arSoapHeaders = null) {
		$arReturn = array();

		if($this->WasInited()) {
			$this->arLastRequestTrace = array();
			$sResponseHeaders = '';
			try {
				$arReturn['BODY'] = $this->obSoapClient->__soapCall($sFunctionName, $arParams, null, $arSoapHeaders, $arResponseHeader);
				// конвертируем кодировку ответа в кодировку сайта
				if(is_object($arReturn['BODY'])) {
					foreach($arReturn['BODY'] as $sProp => $mVal) {
						$arReturn['BODY']->$sProp = $this->ConvertToSiteCharset($mVal);
					}
				} else {
					$arReturn['BODY'] = $this->ConvertToSiteCharset($arReturn['BODY']);
				}

				if($bReturnHeader) {
					$arReturn['HEADER'] = $arResponseHeader;
				}
			} catch(SOAPFault $obSoapFault) {
				$arReturn['ERROR'] = $this->ParseSoapFault($obSoapFault);
			}
			if($this->bSaveTrace) {
				$this->arLastRequestTrace = array(
					'REQUEST' => $this->obSoapClient->__getLastRequest(),
					'RESPONSE' => $this->obSoapClient->__getLastResponse(),
					'RESPONSE_HEADER' => $arResponseHeader,
					'RESPONSE_HEADER_' => $this->obSoapClient->__getLastResponseHeaders(),
				);
			}
		}
		return $arReturn;
	}

	public function GetParam($sParamName) {
		return isset($this->arParams[$sParamName]) ? $this->arParams[$sParamName] : '';
	}

	protected function ThrowException($arErr) {
		$bReturn = false;
		if(!empty($arErr) && is_array($arErr)) {
			if(array_key_exists('ERROR_MSG', $arErr) && array_key_exists('ERROR_CODE', $arErr)) {
				$obException = new CApplicationException($arErr['ERROR_MSG'], $arErr['ERROR_CODE']);
				$GLOBALS['APPLICATION']->ThrowException($obException);
				$bReturn = true;
			}
		}
		return $bReturn;
	}

	protected function ParseSoapFault($obSoapFault) {
		$arReturn = array(
			'ERROR_CODE' => '',
			'ERROR_MSG' => '',
			'ERROR_DETAILS' => array()
		);
		if($obSoapFault && is_object($obSoapFault) && is_a($obSoapFault, 'SoapFault')) {
			// пробуем нормализовать сообщения об ошибках
			if(isset($obSoapFault->detail) && is_object($obSoapFault->detail) && is_a($obSoapFault->detail, 'stdClass')) {
				foreach($obSoapFault->detail as $sCalssPropCode => $mValue) {
					if(is_object($mValue) && is_a($mValue, 'SoapVar')) {
						if(isset($mValue->enc_value->errorCode)) {
							$arReturn['ERROR_CODE'] = $mValue->enc_value->errorCode;
						}
						if(isset($mValue->enc_value->errorParams) && is_array($mValue->enc_value->errorParams)) {
							foreach($mValue->enc_value->errorParams as $obErrItem) {
								$arReturn['ERROR_DETAILS'][] = (array) $obErrItem;
							}
						}
						if(isset($mValue->enc_value->localizedMessage)) {
							$arReturn['ERROR_MSG'] = $mValue->enc_value->localizedMessage;
						} elseif(isset($mValue->enc_value->message)) {
							$arReturn['ERROR_MSG'] = $mValue->enc_value->message;
						}
					}
				}
			}

			if(!strlen($arReturn['ERROR_CODE'])) {
				$arReturn['ERROR_CODE'] = $obSoapFault->faultcode;
			}

			if(!strlen($arReturn['ERROR_MSG'])) {
				$arReturn['ERROR_MSG'] = $obSoapFault->GetMessage();
			}
			if(!strlen($arReturn['ERROR_MSG'])) {
				$arReturn['ERROR_MSG'] = $obSoapFault->__toString();
			}
			// конвертируем кодировку ответа в кодировку сайта
			$arReturn = $this->ConvertToSiteCharset($arReturn);
		}
		return $arReturn;
	}

	protected function ConvertToSiteCharset($mConvert) {
		if((!defined('BX_UTF') || !BX_UTF) && (strtoupper(SITE_CHARSET) != 'UTF-8')) {
			if(is_array($mConvert)) {
				foreach($mConvert as $sKey => $mValue) {
					if(is_string($mValue)) {
						if(strlen($mValue)) {
							$mConvert[$sKey] = $GLOBALS['APPLICATION']->ConvertCharset($mValue, 'UTF-8', SITE_CHARSET);
						}
					} elseif(is_array($mValue)) {
						$mConvert[$sKey] = $this->ConvertToSiteCharset($mValue);
					}
				}
			} elseif(is_string($mConvert)) {
				$mConvert = $GLOBALS['APPLICATION']->ConvertCharset($mConvert, 'UTF-8', SITE_CHARSET);
			}
		}
		return $mConvert;
	}

	protected function ConvertToUtf8($mConvert) {
		if((!defined('BX_UTF') || !BX_UTF) && (strtoupper(SITE_CHARSET) != 'UTF-8')) {
			if(is_array($mConvert)) {
				foreach($mConvert as $sKey => $mValue) {
					if(is_string($mValue)) {
						if(strlen($mValue)) {
							$mConvert[$sKey] = $GLOBALS['APPLICATION']->ConvertCharset($mValue, SITE_CHARSET, 'UTF-8');
						}
					} elseif(is_array($mValue)) {
						$mConvert[$sKey] = $this->ConvertToUtf8($mValue);
					}
				}
			} elseif(is_string($mConvert)) {
				$mConvert = $GLOBALS['APPLICATION']->ConvertCharset($mConvert, SITE_CHARSET, 'UTF-8');
			}
		}
		return $mConvert;
	}

	public function WasInited() {
		return !empty($this->obSoapClient);
	}

	public function GetSoapClient() {
		return $this->obSoapClient;
	}

	public function GetFunctions() {
		$arFunctions = array();
		if($this->WasInited()) {
			$arFunctions = $this->obSoapClient->__getFunctions();
		}
		return $arFunctions;
	}

	public function GetLastRequestTrace() {
		return $this->arLastRequestTrace;
	}
}

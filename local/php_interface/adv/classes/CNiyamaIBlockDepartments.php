<?
/**
 * 
 * Класс утилит для инфблока "Департаменты"
 * @author Sergey Leshchenko, 2014
 * @updated: 27.07.2014
 *
 */

class CNiyamaIBlockDepartments {

	public static function GetIBlockId() {
		return CProjectUtils::GetIBlockIdByCode('departments', 'data');
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	//
	// Актуализация справочника Департаменты из FastOperator
	//
	public static function ImportData() {
		$bResult = false;
		CModule::IncludeModule('iblock');
		$iIBlockId = self::GetIBlockId();
		$obFastOperator = new CNiyamaFastOperator();
		$arImportData = $obFastOperator->GetDepartmentsArray();

		if(!$arImportData['Items'] || !$iIBlockId) {
			return $bResult;
		}

		$bResult = true;

		$sUpdateTmpIdField = time();

		// работа с элементами
		if($arImportData['Items']) {
			// обновление элементов
			$arChunks = array_chunk($arImportData['Items'], 1000);
			foreach($arChunks as $arChunkItem) {
				$arUpdateItemsList = array();
				foreach($arChunkItem as $arTmpItem) {
					if(strlen($arTmpItem['Code'])) {
						$arUpdateItemsList[$arTmpItem['Code']] = $arTmpItem;
					}
				}

				if($arUpdateItemsList) {
					$dbItems = CIBlockElement::GetList(
						array(
							'ID' => 'ASC'
						),
						array(
							'IBLOCK_ID' => $iIBlockId,
							'=XML_ID' => array_keys($arUpdateItemsList),
							'CHECK_PERMISSIONS' => 'N',
						),
						false,
						false,
						array(
							'ID', 'XML_ID'
						)
					);

					while($arItem = $dbItems->Fetch()) {
						if($arUpdateItemsList[$arItem['XML_ID']]) {
							$arTmpUpdateItem = $arUpdateItemsList[$arItem['XML_ID']];
							$arFields = array(
								'NAME' => $arTmpUpdateItem['Name'],
								'ACTIVE' => 'Y',
								'CODE' => $arTmpUpdateItem['Code'],
								// !!!
								'TMP_ID' => $sUpdateTmpIdField,
							);
							$arProp = array(
								'REMARK' => $arTmpUpdateItem['Remark'],
								'LONG' => $arTmpUpdateItem['Long'],
								'LAT' => $arTmpUpdateItem['Lat'],
							);
							$obIBlockElement = new CIBlockElement();
							$obIBlockElement->Update($arItem['ID'], $arFields);
							CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iIBlockId, $arProp);
							// !!!
							unset($arUpdateItemsList[$arItem['XML_ID']]);
						}
					}
				}

				// добавление элементов
				if($arUpdateItemsList) {
					foreach($arUpdateItemsList as $arTmpUpdateItem) {
						$arFields = array(
							'IBLOCK_ID' => $iIBlockId,
							'NAME' => $arTmpUpdateItem['Name'],
							'ACTIVE' => 'Y',
							// !!!
							'CODE' => $arTmpUpdateItem['Code'],
							'XML_ID' => $arTmpUpdateItem['Code'],
							'TMP_ID' => $sUpdateTmpIdField,
						);
						$arProp = array(
							'REMARK' => $arTmpUpdateItem['Remark'],
							'LONG' => $arTmpUpdateItem['Long'],
							'LAT' => $arTmpUpdateItem['Lat'],
						);
						$arFields['PROPERTY_VALUES'] = $arProp;
						$obIBlockElement = new CIBlockElement();
						$obIBlockElement->Add($arFields);
					}
				}
			}

			// деактивируем элементы, которых не было в выгрузке
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'!TMP_ID' => $sUpdateTmpIdField,
					'CHECK_PERMISSIONS' => 'N',
				),
				false,
				array(
					'ID'
				)
			);
			while($arTmpItem = $dbItems->Fetch()) {
				$obIBlockElement = new CIBlockElement();
				$obIBlockElement->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
			}
		}

		// акутализируем списочное свойство заказов со справочником ресторанов
		self::UpdateSaleOrderProp();

		return $bResult;
	}

	public static function GetAllDepartments($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = self::GetIBlockId();
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if($iIBlockId && CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'NAME', 'XML_ID', 'ACTIVE',
						'PROPERTY_REMARK', 'PROPERTY_LAT', 'PROPERTY_LONG',
						'PROPERTY_LAT_ALT', 'PROPERTY_LONG_ALT'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'ACTIVE' => $arItem['ACTIVE'],
						'NAME' => $arItem['NAME'],
						'XML_ID' => $arItem['XML_ID'],
						'PROPERTY_REMARK' => $arItem['PROPERTY_REMARK_VALUE'],
						'PROPERTY_LAT' => $arItem['PROPERTY_LAT_VALUE'],
						'PROPERTY_LONG' => $arItem['PROPERTY_LONG_VALUE'],
						'PROPERTY_LAT_ALT' => $arItem['PROPERTY_LAT_ALT_VALUE'],
						'PROPERTY_LONG_ALT' => $arItem['PROPERTY_LONG_ALT_VALUE'],
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;

	}

	//
	// Актуализирует списочное свойство заказов RESTAURANT модуля Интернет-магазин значениями справочника департамента
	//
	public static function UpdateSaleOrderProp() {
		$arDepartments = self::GetAllDepartments();
		if(CModule::IncludeModule('sale')) {
			$arSalePropsId = array();
			$dbItems = CSaleOrderProps::GetList(
				array(),
				array(
					'TYPE' => 'SELECT',
					'CODE' => 'DELIVERY_RESTAURANT'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$arSalePropsId[] = $arItem['ID'];
			}

			foreach($arSalePropsId as $iSalePropId) {
				$arCurValues = array();
				$dbItems = CSaleOrderPropsVariant::GetList(
					array(
						'SORT' => 'ASC'
					),
					array(
						'ORDER_PROPS_ID' => $iSalePropId
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arCurValues[$arItem['VALUE']] = $arItem;
				}
				$arAdd = array();
				$arUpdate = array();
				$arDelete = array();
				$iSort = 0;
				foreach($arDepartments as $arItem) {
					$iSort += 100;
					if($arItem['ACTIVE'] == 'Y') {
						$arAdd[$arItem['XML_ID']] = array(
							'ORDER_PROPS_ID' => $iSalePropId,
							'NAME' => $arItem['NAME'],
							'VALUE' => $arItem['XML_ID'],
							'SORT' => $iSort,
							'DESCRIPTION' => ''
						);
					}
				}
				foreach($arCurValues as $arItem) {
					if($arAdd[$arItem['VALUE']]) {
						$arTmp = $arAdd[$arItem['VALUE']];
						if($arItem['NAME'] != $arTmp['NAME'] || $arItem['SORT'] != $arTmp['SORT']) {
							$arUpdate[$arItem['ID']] = $arTmp;
							$arUpdate[$arItem['ID']]['DESCRIPTION'] = $arItem['DESCRIPTION'];
						}
						unset($arAdd[$arItem['VALUE']]);
					} else {
						$arDelete[$arItem['ID']] = $arItem['ID'];
					}
				}
				if($arDelete) {
					foreach($arDelete as $iSalePropVariantId) {
						CSaleOrderPropsVariant::Delete($iSalePropVariantId);
					}
				}
				if($arUpdate) {
					foreach($arUpdate as $iSalePropVariantId => $arFields) {
						CSaleOrderPropsVariant::Update($iSalePropVariantId, $arFields);
					}
				}
				if($arAdd) {
					foreach($arAdd as $arFields) {
						CSaleOrderPropsVariant::Add($arFields);
					}
				}
			}
		}
	}
}

<?
/**
 * 
 * Прослойка между кастомными сущностями и CDBResult
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 *
 */

namespace ADV;

class CEntityDataMangerDBResult extends \CDBResult {

	public function __construct($obResult) {
		parent::__construct($obResult);
	}

	public function Fetch() {
		$arItem = parent::Fetch();
		return $arItem;
	}

	public function GetNext($bHtml = true, $bTilda = true) {
		$arItem = $this->Fetch();
		if($arItem) {
			foreach($arItem as $sFieldName => $mFieldVal) {
				if(is_object($mFieldVal) && method_exists($mFieldVal, 'tostring')) {
					$arItem[$sFieldName] = $mFieldVal->ToString();
				}
				if($bHtml) {
					if($bTilda) {
						$arItem['~'.$sFieldName] = $arItem[$sFieldName];
					}
					if(is_scalar($arItem[$sFieldName])) {
						$arItem[$sFieldName] = htmlspecialcharsbx($arItem[$sFieldName]);
					} elseif(is_array($arItem[$sFieldName])) {
						array_walk_recursive($arItem[$sFieldName], create_function('&$item, $key', '$item = htmlspecialcharsbx($item);'));
					}
				}
			}
		}
		return $arItem;
	}
}

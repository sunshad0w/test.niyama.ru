<?
/**
 * 
 * Кастомный DataManager для постранички
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 *
 */

namespace ADV;
use Bitrix\Main\Entity;

class CEntityDataManger extends Entity\DataManager {

	public static function GetList($arParams = array()) {
		$bSelectAll = true;
		if(isset($arParams['select'])) {
			if(!in_array('*', $arParams['select'])) {
				$bSelectAll = false;
			}
		}
		if($bSelectAll) {
			$arParams['select'][] = '*';
		}

		// постраничка
		$arPagerParams = array();
		if(isset($arParams['arNavParams'])) {
			$arNavParams = $arParams['arNavParams'];
			unset($arParams['arNavParams']);
			if($arNavParams['nPageSize']) {
				$obCDBResult = new CDBResult();
				$arPagerTmp = $obCDBResult->GetNavParams($arNavParams);

				$obPagerQuery = new \Bitrix\Main\Entity\Query(__CLASS__);

				$obPagerQuery->SetSelect(array('ID'));
				if($arParams['filter']) {
					$obPagerQuery->SetFilter($arParams['filter']);
				}
				if($arParams['group']) {
					$obPagerQuery->SetGroup($arParams['group']);
				}

				$dbItems = $obPagerQuery->Exec();

				$arPagerParams['NavPageSize'] = intval($arNavParams['nPageSize']);
				$arPagerParams['NavPageSize'] = $arPagerParams['NavPageSize'] > 0 ? $arPagerParams['NavPageSize'] : 10;
				$arPagerParams['NavRecordCount'] = $dbItems->GetSelectedRowsCount();
				$arPagerParams['NavPageNomer'] = $arPagerTmp['PAGEN'];
				$arPagerParams['NavPageCount'] = ceil($arPagerParams['NavRecordCount'] / $arPagerParams['NavPageSize']);

				$arParams['limit'] = $arPagerParams['NavPageSize'];
				$arParams['offset'] = ($arPagerParams['NavPageNomer'] - 1) * $arPagerParams['NavPageSize'];
			} elseif($arNavParams['nTopCount']) {
				$arParams['limit'] = intval($arNavParams['nTopCount']);
				$arParams['limit'] = $arParams['limit'] > 0 ? $arParams['limit'] : 10;
			}
		}

		$obResult = new \ADV\CEntityDataMangerDBResult(parent::GetList($arParams));

		if($arPagerParams) {
			$obResult->InitNavStartVars($arNavParams);

			// !!! костыль !!!
			$obResult->bNavStart = false;

			$obResult->NavRecordCount = $arPagerParams['NavRecordCount'];
			$obResult->NavPageCount = $arPagerParams['NavPageCount'];
			$obResult->NavPageNomer = $arPagerParams['NavPageNomer'];
		}

		return $obResult;
	}
}

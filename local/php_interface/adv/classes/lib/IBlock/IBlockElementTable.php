<?
/**
 * 
 * Кастомный класс таблицы сущности элемент инфоблока
 * @author Sergey Leshchenko, 2014
 * @updated: xx.06.2014
 *
 */

namespace ADV\IBlock;

class IBlockElementTable extends \ADV\CEntityDataManger {

	public static function GetFilePath() {
		return __FILE__;
	}

	public static function GetTableName() {
		return 'b_iblock_element';
	}

	//
	// дополнительные связи
	//
	public static function GetAddRefMap() {
		static $arMap = array();
		if(!$arMap) {
/*
			$arMap['IBLOCK' => array(
				'data_type' => '\\Bitrix\IBlock\\IBlock',
				'reference' => array(
					'=this.IBLOCK_ID' => 'ref.ID'
				),
			);
*/
		}
		return $arMap;
	}

	public static function GetMap() {
		static $arMap = null;
		if($arMap === null) {
			$arMap = array(
				'ID' => array(
					'data_type' => 'integer',
					'primary' => true,
					'autocomplete' => true,
				),
				'NAME' => array(
					'data_type' => 'string',
				),
				'CODE' => array(
					'data_type' => 'string',
				),
				'IBLOCK_ID' => array(
					'data_type' => 'integer',
				),
				'ACTIVE' => array(
					'data_type' => 'boolean',
					'values' => array('N', 'Y'),
				),
				'ACTIVE_FROM' => array(
					'data_type' => 'datetime',
				),
				'ACTIVE_TO' => array(
					'data_type' => 'datetime',
				),
			);

			// опишем дополнительные связи
			$arAddRefMap = self::GetAddRefMap();
			$arMap = array_merge($arMap, $arAddRefMap);
		}
		return $arMap;
	}

	public static function GetList($arParams = array()) {
		if(defined('BX_COMP_MANAGED_CACHE')) {
			//$GLOBALS['CACHE_MANAGER']->RegisterTag('IBLOCK_');
		}
		return parent::GetList($arParams);
	}
}

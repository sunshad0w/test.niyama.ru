<?
/**
 * Класс инфоблока привязок дисконтных карт к посетителям
 *
 * @updated: 14.04.2015
 */

class CNiyamaIBlockCardData {
	protected static $sCardsHistoryEntityName = 'CardsHistory';

	public static function GetIBlockId() {
		return CProjectUtils::GetIBlockIdByCode('card_data', 'NIYAMA_FOR_USER');
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

	public static function GetCardsHistoryEntity() {
		return CHLEntity::GetEntityByName(self::$sCardsHistoryEntityName);
	}

	public static function GetCardsHistoryEntityClass() {
		$obEntity = self::GetCardsHistoryEntity();
		if($obEntity) {
			return $obEntity->GetdataClass();
		}
		return '';
	}

	/**
	 * GetUserDiscountActual
	 * @param bool $iUserId
	 * @param $bRefreshCache
	 * @return string
	 */
	public static function GetUserDiscountActual($iUserId = 0, $bRefreshCache = false) {
		$sReturn = 0;
		$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
		if($iUserId <= 0) {
			return $sReturn;
		}

		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iUserId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getUserDiscount';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array('DISCOUNT');
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$sReturn = $obExtCache->GetVars();
		} else {

			$iIBlockId = self::GetIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			CModule::IncludeModule('iblock');
			$rsItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
					'=PROPERTY_USER_ID' => $iUserId,
				),
				false,
				false,
				array(
					'ID',
					'PROPERTY_USER_ID', 'PROPERTY_CARDNUM'
				)
			);
			if($arItem = $rsItems->Fetch()) {
				if(!empty($arItem['PROPERTY_CARDNUM_VALUE'])) {
					$arNiyamaCard = CNiyamaDiscountCard::GetPdsCardInfo($arItem['PROPERTY_CARDNUM_VALUE'], false);
					if($arNiyamaCard && !$arNiyamaCard['_ERRORS_']) {
						$sReturn = self::GetTrueDiscount($arNiyamaCard['Account']['Discount']);
					}
				}
			}

			// !!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId));

			if(!$sReturn) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($sReturn);
		}
		return $sReturn;
	}

	/**
	 * getUserDiscountDB
	 * @return int
	 */
	public static function GetUserDiscountDB($iUserId = 0, $bRefreshCache = false) {
		$iReturn = 0;

		$iUserId = intval($iUserId) > 0 ? intval($iUserId) : intval($GLOBALS['USER']->GetId());
		if($iUserId <= 0) {
			return $iReturn;
		}

		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iUserId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getUserDiscountDB';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array('DISCOUNT');
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$iReturn = $obExtCache->GetVars();
		} else {

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			$arSelect['SELECT'] = array('UF_DISCOUNT');
			$dbUser = CUser::GetList(
				$sBy = 'ID',
				$sOrder = 'ASC',
				array(
					'ID' => $iUserId
				),
				$arSelect
			);
			if($arItem = $dbUser->Fetch()) {
				$iReturn = isset($arItem['UF_DISCOUNT']) ? intval($arItem['UF_DISCOUNT']) : 0;
			} else {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($iReturn);
		}
		return $iReturn;
	}

	/**
	 * GetCardNumber
	 * @param $iUserId
	 * @param bool $bRefreshCache
	 * @return string
	 */
	public static function GetCardNumber($iUserId, $bGetActiveOnly = true, $bRefreshCache = false) {
		$arData = self::GetCardData($iUserId, $bGetActiveOnly, $bRefreshCache);
		if($arData) {
			return $arData['PROPERTY_CARDNUM_VALUE'];
		}
		return '';
	}

	/**
	 * GetCardData
	 * @param integer $iUserId
	 * @param bool $bGetNotActive
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetCardData($iUserId = 0, $bGetNotActive = false, $bRefreshCache = false) {
		$arReturn = array();
		$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
		if($iUserId <= 0) {
			return $arReturn;
		}

		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iUserId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'getCardData|v3'.($bGetNotActive ? '|Y' : '|N');
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {

			$iIBlockId = self::GetIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			CModule::IncludeModule('iblock');
			$arTmpFilter = array(
				'IBLOCK_ID' => $iIBlockId,
				'=PROPERTY_USER_ID' => $iUserId,
			);
			if(!$bGetNotActive) {
				$arTmpFilter['ACTIVE'] = 'Y';
			}
			$dbItems = CIBlockElement::GetList(
				array(
					'ACTIVE' => 'DESC',
					'ID' => 'ASC',
				),
				$arTmpFilter,
				false,
				false,
				array(
					'ID', 'DATE_ACTIVE_FROM', 'ACTIVE', 'NAME',
					'PROPERTY_USER_ID', 'PROPERTY_CARDNUM', 'PROPERTY_DISCOUNT',
					'PROPERTY_NAME', 'PROPERTY_LAST_NAME', 'PROPERTY_SECOND_NAME', 'PROPERTY_BIRTHDAY', 'PROPERTY_PHONE',
					'PROPERTY_EMAIL', 'PROPERTY_CITY', 'PROPERTY_STREET', 'PROPERTY_HOME', 'PROPERTY_HOUSING',
					'PROPERTY_BUILDING', 'PROPERTY_APARTMENT', 'PROPERTY_OFFERED'
				)
			);
			if($arItem = $dbItems->Fetch()) {
				if(!empty($arItem['PROPERTY_CARDNUM_VALUE'])) {
					foreach($arItem as $sFieldCode => $mVal) {
						if(strpos($sFieldCode, 'PROPERTY_') === 0) {
							$iPos = strrpos($sFieldCode, '_VALUE');
							if(!$iPos || $iPos != (strlen($sFieldCode) - 6)) {
								unset($arItem[$sFieldCode]);
							}
						}
					}
					$arItem['TRUE_DISCOUNT'] = 0;
					if(!empty($arItem['PROPERTY_DISCOUNT_VALUE'])) {
						$arItem['TRUE_DISCOUNT'] = self::GetTrueDiscount($arItem['PROPERTY_DISCOUNT_VALUE']);
					}
					$arReturn = $arItem;
				}
			}

			// !!!
			$obExtCache->AddCacheTags(array('iblock_id_'.$iIBlockId));

			if (!$arReturn) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * getTrueDiscount
	 * @param $sDiscountCode
	 * @return string
	 */
	public static function GetTrueDiscount($sDiscountCode) {
		// По умолчнию 0
		return CNiyamaCustomSettings::GetStringValue('discount_'.$sDiscountCode, '0');
	}

	/**
	 * getDiscountCode
	 * @param $iTrueDiscount
	 * @return array
	 */
	public static function GetDiscountCode($iTrueDiscount) {
		return CNiyamaCustomSettings::GetStringValue('true_discount_'.$iTrueDiscount, '32');
	}

	public static function AddHistory($arElementFields) {
		if(!$arElementFields['ID'] || !$arElementFields['PROPERTY_VALUES'] || !is_array($arElementFields['PROPERTY_VALUES'])) {
			return false;
		}
		// задаем поля статически, чтобы избежать фаталов при несуществующих в HLB полях
		$arHistoryFields = array(
			'UF_DATE_CREATE' => '',
			'UF_ELEMENT_ID' => '',
			'UF_USER_ID' => '',
			'UF_CARDNUM' => '',
			'UF_NAME' => '',
			'UF_LAST_NAME' => '',
			'UF_SECOND_NAME' => '',
			'UF_BIRTHDAY' => '',
			'UF_PHONE' => '',
			'UF_EMAIL' => '',
			'UF_CITY' => '',
			'UF_STREET' => '',
			'UF_HOME' => '',
			'UF_HOUSING' => '',
			'UF_BUILDING' => '',
			'UF_APARTMENT' => '',
			'UF_DISCOUNT' => '',
			'UF_OFFERED' => '',
		);
		foreach($arElementFields['PROPERTY_VALUES'] as $sPropCode => $mVal) {
			if(array_key_exists('UF_'.$sPropCode, $arHistoryFields)) {
				$arHistoryFields['UF_'.$sPropCode] = $mVal;
			}
		}
		$arHistoryFields['UF_DATE_CREATE'] = date('d.m.Y H:i:s');
		$arHistoryFields['UF_ELEMENT_ID'] = $arElementFields['ID'];

		$sTmpHLEntityClass = self::GetCardsHistoryEntityClass();
		$obTmpResult = $sTmpHLEntityClass::Add($arHistoryFields);
	}

	/**
	 * Add
	 * @param $arFields
	 * @param bool $iUserId
	 * @return bool|int
	 */
	public static function Add($arPropFields, $iUserId = 0, $arFields = array(), $bAddHistory = true) {
		$iReturn = 0;

		$iUserId = $iUserId ? intval($iUserId) : intval($GLOBALS['USER']->GetId());
		if($iUserId <= 0) {
			return $iReturn;
		}

		CModule::IncludeModule('iblock');
		$iIBlockId = self::GetIBlockId();
		$iItemsCnt = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => $iIBlockId,
				'=PROPERTY_USER_ID' => $iUserId,
				//'=PROPERTY_CARDNUM' => $arPropFields['CARDNUM'],
			), 
			array()
		);

		if(!$iItemsCnt) {
			$arPropFields['USER_ID'] = $iUserId;
			$arFields = is_array($arFields) ? $arFields : array();
			$arFields['IBLOCK_ID'] = $iIBlockId;
			$arFields['NAME'] = isset($arFields['NAME']) && strlen(trim($arFields['NAME'])) ? $arFields['NAME'] : $iUserId;
			$arFields['DATE_ACTIVE_FROM'] = isset($arFields['DATE_ACTIVE_FROM']) && strlen(trim($arFields['DATE_ACTIVE_FROM'])) ? $arFields['DATE_ACTIVE_FROM'] : date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')));
			$arFields['ACITVE'] = isset($arFields['ACITVE']) && $arFields['ACITVE'] == 'N' ? 'N' : 'Y';
			$arFields['PROPERTY_VALUES'] = $arPropFields;

			$obIBlockElement = new CIBlockElement();
			$iReturn = intval($obIBlockElement->Add($arFields));
			if($iReturn > 0) {
				$GLOBALS['CACHE_MANAGER']->ClearByTag('DISCOUNT');

				$arFields['ID'] = $iReturn;

				// ведение истории
				if($bAddHistory) {
					self::AddHistory($arFields);
				}

				# Событие добавления карты
				foreach(GetModuleEvents('main', 'OnAfterCardAdd', true) as $arEvent) {
					ExecuteModuleEventEx($arEvent, array($arFields));
				}
			}
		}
		// ! важно возвращать 0, если не было добавления элемента !
		return $iReturn;
	}

	/**
	 * Edit
	 * @param $ID
	 * @param $arFields
	 * @param bool $iUserId
	 * @return bool
	 */
	public static function Update($iElementId, $arPropFields = array(), $arFields = array(), $bAddHistory = true) {
		$iReturn = 0;
		CModule::IncludeModule('iblock');
		$dbItems = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => self::GetIBlockId(),
				'ID' => $iElementId,
			),
			false,
			false,
			array(
				'PROPERTY_USER_ID'
			)
		);
		if($arItem = $dbItems->Fetch()) {
			$iTmpId = intval($arItem['PROPERTY_USER_ID_VALUE']);
			$arPropFields['USER_ID'] = $iTmpId <= 0 && $arPropFields['USER_ID'] && intval($arPropFields['USER_ID']) > 0 ? intval($arPropFields['USER_ID']) : $iTmpId;
			$arFields = is_array($arFields) ? $arFields : array();
			if($arPropFields) {
				$arFields['PROPERTY_VALUES'] = $arPropFields;
			}

			$obIBlockElement = new CIBlockElement();
			$bRes = $obIBlockElement->Update($iElementId, $arFields);
			if($bRes) {
				$GLOBALS['CACHE_MANAGER']->ClearByTag('DISCOUNT');

				$iReturn = $iElementId;
				$arFields['ID'] = $iReturn;

				// ведение истории
				if($bAddHistory) {
					self::AddHistory($arFields);
				}

				foreach(GetModuleEvents('main', 'OnAfterCardEdit', true) as $arEvent) {
					ExecuteModuleEventEx($arEvent, array($arFields));
				}
			}
		}

		// ! важно возвращать 0, если не было обновления элемента !
		return $iReturn;
	}

	/*
	 * Скидки и купоны на скидку
	 */

	// Получить номер купоны по id скидки в заказе (параметр ORDER_DISCOUNT_ID в COUPON_LIST)
	public static function GetCouponByOrderDiscountId($orderDiscountId, $arCouponList) {
        $result = null;
        foreach ($arCouponList as $coupon){
            if($coupon['ORDER_DISCOUNT_ID'] == $orderDiscountId) {
                $result = $coupon['COUPON'];
                break;
            }
        }
        return $result;
	}

}

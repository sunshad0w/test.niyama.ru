<?php

class CNiyamaUserActions {

    public static function AddAll() {
        return false;
    }

    /**
     * Рекомендации
     * AddRecANY
     * @return bool
     */
    public static function AddRecANY()
    {
        global $USER;

        if ($USER->IsAuthorized()) {
			self::AddAll();
            $obAdd = UserActionsTable::add(array(
                "UF_USER_ID" => $USER->GetID(),
                "UF_TYPE_ID" => "RECOMENDATIONSOC_ANY"
            ));
            return ($obAdd->isSuccess()) ? true : false;
        }
        return  false;
    }

    public static function AddRecLivel()
    {
        global $USER;
        self::AddRecANY();
        if ($USER->IsAuthorized()) {
            $obAdd = UserActionsTable::add(array(
                "UF_USER_ID" => $USER->GetID(),
                "UF_TYPE_ID" => "RECOMENDATIONSOC_TWEET_YROVEN_OR_MEDAL"
            ));
            return ($obAdd->isSuccess()) ? true : false;
        }
        return  false;
    }

    public static function AddRecWidget()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            self::AddRecANY();
            $obAdd = UserActionsTable::add(array(
                "UF_USER_ID" => $USER->GetID(),
                "UF_TYPE_ID" => "RECOMENDATIONSOC_RECOMENDATON_FROM_VIDZHET"
            ));
            return ($obAdd->isSuccess()) ? true : false;
        }
        return false;
    }

    /**
     * Участия в Опросах
     */

    public static function AddSurvey()
    {
        self::AddAll();
        global $USER;
        if ($USER->IsAuthorized()) {
            $obAdd = UserActionsTable::add(array(
                "UF_USER_ID" => $USER->GetID(),
                "UF_TYPE_ID" => "SURVEY"
            ));
            return ($obAdd->isSuccess()) ? true : false;
        }
        return false;
    }

    /**
     * Авторизация через соцсети
     * @return bool
     */
	public static function AddAuthSoc() {
		self::AddAll();
		if($GLOBALS['USER']->IsAuthorized()) {
			$obAdd = UserActionsTable::add(
				array(
					'UF_USER_ID' => $GLOBALS['USER']->GetID(),
					'UF_TYPE_ID' => 'AUTHSOC'
				)
			);
			return $obAdd->isSuccess() ? $obAdd->GetId() : 0;
		}
		return false;
	}

}

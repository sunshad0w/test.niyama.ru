<?php

/**
 * Class CUsersData
 */
class CUsersData
{

	public function GetDataUserForLK($iUserId) {
		$res = false;
		$iUserId = intval($iUserId);
		if($iUserId > 0) {
			$rsUser = CUser::GetByID($iUserId);
			$arUser = $rsUser->Fetch();
			if($arUser) {
				$res = array(
					'ID' => $arUser['ID'],
					'NAME' => trim($arUser['NAME']),
					'LAST_NAME' => trim($arUser['LAST_NAME']),
					'EMAIL' => trim($arUser['EMAIL']),
					'AVATAR' => $arUser['PERSONAL_PHOTO'],
					'PHONE' => trim($arUser['PERSONAL_PHONE']),
					'SECOND_NAME' => trim($arUser['SECOND_NAME']),
				);
			}
		}
		return  $res;
	}

	public function SaveDataUserForLK($user_id, $data){
		$res=false;
		if ((!empty($user_id))&&(!empty($data))){
			$user = new CUser;
			$fields = Array(
				'NAME' 			=> $data['NAME'],
				'LAST_NAME' 	=> $data['LAST_NAME'],
				'EMAIL' 		=> $data['EMAIL'],
				'PERSONAL_PHOTO'=> $data['AVATAR'],
				'PERSONAL_PHONE'=> $data['PHONE'],
			 );
			$res=$user->Update($user_id, $fields);
		}
		return  $res;
	}

    /**
     * getDefaultAvatarList
     * @param bool $bRefreshCache
     * @return array
     */
    public static function getDefaultAvatarList($bRefreshCache = false)
    {
        $arReturn = array();
        // дополнительный параметр для ID кэша
        $sCacheAddParam = "";
        // идентификатор кэша (обязательный и уникальный параметр)
        $sCacheId = 'getDefaultAvatarList';
        // массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
        $arAddCacheTags = array(__METHOD__);
        // путь для сохранения кэша
        $sCachePath = '/'.__CLASS__.'/getDefaultAvatarList/';
        // сохранять ли значения дополнительно в виртуальном кэше
        $bUseStaticCache = true;
        // максимальное количество записей виртуального кэша
        $iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
        // соберем в массив идентификационные параметры кэша
        $arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

        $obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
        if(!$bRefreshCache && $obExtCache->InitCache()) {
	        $arReturn = $obExtCache->GetVars();
        } else {
            if (CModule::IncludeModule("iblock")) {
        	    $iIBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_AVATARS', 'NIYAMA_FOR_USER');

		        // открываем кэшируемый участок
		        $obExtCache->StartDataCache();

            	$arFilter["IBLOCK_ID"] = $iIBlockId;

	            $rsItems = CIBlockElement::GetList(
    	            array("SORT"=>"ASC"),
        	        $arFilter,
            	    false,
	                false,
    	            array(
        	            "ID",
            	        "NAME",
                	    "PREVIEW_PICTURE"
	                )
    	        );
        	    while ($arItem = $rsItems->Fetch()){
            	    $arReturn[] = $arItem;
	            }

    	        if (empty($arReturn)){
        	        $obExtCache->AbortDataCache();
	            }

    	    	// закрываем кэшируемый участок
	        	$obExtCache->EndDataCache($arReturn);
            }
        }
        return $arReturn;
    }

    /**
     * getFirstDefaultAvatar
     * @return bool
     */
    public static function getFirstDefaultAvatar()
    {
        $arAvatarData = self::getDefaultAvatarList();
        if ( !empty($arAvatarData)){
            if (!empty($arAvatarData[0]['PREVIEW_PICTURE'])){
                return $arAvatarData[0]['PREVIEW_PICTURE'];
            }
        }
        return false;
    }

    /**
     * getUserName
     * @return mixed
     */
    public static function getUserName() {
        global $USER;
        $gFirstName = $USER->GetFirstName();
        return (!empty($gFirstName) ) ? $USER->GetFirstName() : $USER->GetEmail();
    }

    /**
     * getPersonalPhoto
     * @param bool $UserId
     * @return mixed
     */
    public static function getPersonalPhoto($UserId = false)
    {
        global $USER;
        $UserId = ($UserId) ? $UserId : $USER->GetID();
        $rsUser = CUser::GetByID($UserId);
        $arUser = $rsUser->Fetch();
        if (!empty($arUser) )
        {
            if ($UserId == $arUser['ID']){
                if (!empty($arUser['PERSONAL_PHOTO'])) {
                return $arUser['PERSONAL_PHOTO'];
                } else {
                    return self::getFirstDefaultAvatar();
                }
            }
        }
        return false;
    }

    /**
     * getPersonalPhotoSrc
     * @param bool $UsderId
     * @return bool
     */
    public static function getPersonalPhotoSrc($UsderId = false)
    {
        $iPersonalPhoto = self::getPersonalPhoto($UsderId);
        if (!empty($iPersonalPhoto)){
            $arUsAva =  CFile::GetFileArray($iPersonalPhoto);
            if(!empty( $arUsAva['SRC'])) {
                return $arUsAva['SRC'];
            }
        }
        return false;
    }

    /**
     * getAddressList
     * @param bool $iUserId
     * @param bool $bRefreshCache
     * @return array
     */
    public static function getAddressList($iUserId = false,$bRefreshCache= false)
    {
        CModule::IncludeModule('iblock');
        # Получаем ID пользователя
        $iUserId = ($iUserId) ? $iUserId : $GLOBALS['USER']->GetID();

        $arSubWays = CNiyamaIBlockSubway::GetAllStationsList($bRefreshCache);

        $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
        $arSelect = array(
            'ID',
            'PROPERTY_USER_ID',
            'PROPERTY_REGION',
			'PROPERTY_CITY_DOP',
            'PROPERTY_SUBWAY',
            'PROPERTY_ADDRESS',
			'PROPERTY_HOUSE',
            'PROPERTY_HOME',
            'PROPERTY_PORCH',
			'PROPERTY_INTERCOM',
			'PROPERTY_FLOOR',
			'PROPERTY_WITHIN_MKAD',
            'PROPERTY_IS_DEFAULT',
        );

        $rsItems = CIBlockElement::GetList(
        	array(
            	'PROPERTY_IS_DEFAULT' => 'DESC'
	        ), 
	        array(
	            'IBLOCK_ID' => $IBLOCK_ID,
    	        '=PROPERTY_USER_ID' => $iUserId
        	), 
        	false,
        	false,
        	$arSelect
        );

        while($arItem = $rsItems->GetNext()) {
            $arItem['SUBWAY_DATA'] = $arSubWays[$arItem['PROPERTY_SUBWAY_VALUE']];
			$arItem['PROPERTY_WITHIN_MKAD'] = 1;
            $arReturn[$arItem['ID']] = $arItem;
        }

        return $arReturn;
    }
	
	public static function getDefaultAddressByUser($iUserId = false, $bRefreshCache = false) {
		$arReturn = array();
        CModule::IncludeModule("iblock");

        if($iUserId || isset($GLOBALS['USER'])) {
	        # Получаем ID пользователя
    	    $iUserId = $iUserId ? intval($iUserId) : $GLOBALS['USER']->GetId();
			if($iUserId <= 0) {
				return $arReturn;
			}

	        //$arSubWays = CNiyamaIBlockSubway::GetAllStationsList($bRefreshCache);
	        $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
    	    $arSelect = array(
        	    'ID',
            	'PROPERTY_USER_ID',
	            'PROPERTY_REGION',
				'PROPERTY_CITY_DOP',
        	    'PROPERTY_SUBWAY',
            	'PROPERTY_ADDRESS',
				'PROPERTY_HOUSE',
    	        'PROPERTY_HOME',
        	    'PROPERTY_PORCH',
				'PROPERTY_INTERCOM',
				'PROPERTY_FLOOR',
				'PROPERTY_WITHIN_MKAD',
            	'PROPERTY_IS_DEFAULT',
	        );

    	    $dbItems = CIBlockElement::GetList(
        		array(
	        	    'ID' => 'ASC'
	    	    ), 
    		    array(
	    	        'IBLOCK_ID' => $IBLOCK_ID,
    	    	    '=PROPERTY_USER_ID' => $iUserId,
					'PROPERTY_IS_DEFAULT' => 1
		        ), 
		        false, 
	    	    false, 
	        	$arSelect
			);
    	    if($arItem = $dbItems->GetNext()) {
        	    //$arItem['SUBWAY_DATA'] = $arSubWays[$arItem['PROPERTY_SUBWAY_VALUE']];
				$arItem['PROPERTY_WITHIN_MKAD'] = 1;
	            $arReturn = $arItem;
    	    }
        }
        return $arReturn;
    }


    public static function GetAddressByID($iAdressId)
    {
        $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
        $arSelect = array(
            "ID",
            "PROPERTY_USER_ID",
            "PROPERTY_REGION",
			"PROPERTY_CITY_DOP",
            "PROPERTY_SUBWAY",
            "PROPERTY_ADDRESS",
			"PROPERTY_HOUSE",
            "PROPERTY_HOME",
            "PROPERTY_PORCH",
			"PROPERTY_INTERCOM",
			"PROPERTY_FLOOR",
			"PROPERTY_WITHIN_MKAD",
            "PROPERTY_IS_DEFAULT",
        );

        $rsItems = CIBlockElement::GetList(
        	array(), 
			array(
            	"ID" => $iAdressId,
	            "IBLOCK_ID" => $IBLOCK_ID,
	        ), 
	        false, 
	        false, 
	        $arSelect
		);

        if($arItem = $rsItems->GetNext()) {
			$arItem['PROPERTY_WITHIN_MKAD'] = 1;
            $arReturn = $arItem;
        }
        return $arReturn;
    }

    /**
     * addAddress
     * @param $REGION
     * @param $SUBWAY
     * @param $ADDRESS
     * @param $HOME
     * @param $PORCH
     * @param $IS_DEFAULT
     * @param bool $iUserId
     * @return bool|int
     */
    public static function addAddress($REGION, $CITY_DOP, $SUBWAY, $ADDRESS, $HOUSE, $HOME, $PORCH, $INTERCOM, $FLOOR, $WITHIN_MKAD, $IS_DEFAULT, $iUserId = false)
    {
        $iUserId = ($iUserId) ? $iUserId : $GLOBALS['USER']->GetID();
        $iIBlockId = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');



            if (intval($IS_DEFAULT) > 0){
                if (self::UpdateDefaultAddress($iUserId) ){
                    $IS_DEFAULT = 1;
                } else {
                    $IS_DEFAULT = 0;
                }
            }


        $addParams = array(
            "IBLOCK_ID"       =>  $iIBlockId,
            "NAME"            => "Адрес_".$iUserId,
            "PROPERTY_VALUES" => array(
                "USER_ID"  => $iUserId,
                "REGION"   => $REGION,
				"CITY_DOP" => $CITY_DOP,
                "SUBWAY"   => $SUBWAY,
                "ADDRESS"  => $ADDRESS,
				"HOUSE"  => $HOUSE,
                "HOME"     => $HOME,
                "PORCH"    => $PORCH,
				"INTERCOM" => $INTERCOM,
				"FLOOR"    => $FLOOR,
				"WITHIN_MKAD" => 1,
                "IS_DEFAULT" => $IS_DEFAULT
            )
        );

        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
        $res = $el->Add($addParams);

        # Добавляем событие добавления гостя
        foreach(GetModuleEvents("main", "OnAfterAddressAdd", true) as $arEvent) {
            ExecuteModuleEventEx($arEvent, Array($res));
        }
        return $res;
    }

    /**
     * updateAddress
     * @param $ID
     * @param $REGION
     * @param $SUBWAY
     * @param $ADDRESS
     * @param $HOME
     * @param $PORCH
     * @param $IS_DEFAULT
     * @param bool $iUserId
     * @return bool
     */
    public static function updateAddress($ID, $REGION, $CITY_DOP, $SUBWAY, $ADDRESS, $HOUSE, $HOME, $PORCH, $INTERCOM, $FLOOR, $WITHIN_MKAD, $IS_DEFAULT, $iUserId = false)
    {		
        $iUserId = ($iUserId) ? $iUserId : $GLOBALS['USER']->GetID();

        if (intval($IS_DEFAULT) > 0){
            if (self::UpdateDefaultAddress($iUserId) ){
                $IS_DEFAULT = 1;
            } else {
                $IS_DEFAULT = 0;
            }
        }

        $addParams = array(
            "PROPERTY_VALUES" => array(
                "USER_ID"  => $iUserId,
                "REGION"   => $REGION,
				"CITY_DOP" => $CITY_DOP,
                "SUBWAY"   => $SUBWAY,
                "ADDRESS"  => $ADDRESS,
			 	"HOUSE"  => $HOUSE,
                "HOME"     => $HOME,
                "PORCH"    => $PORCH,
				"INTERCOM" => $INTERCOM,
				"FLOOR"    => $FLOOR,
				"WITHIN_MKAD" => 1,
                "IS_DEFAULT" => $IS_DEFAULT
            )
        );
        CModule::IncludeModule("iblock");
        $el = new CIBlockElement;
// !!! Зачем так обновлять свойства?
        $res = $el->Update($ID, $addParams);

        return $res;
    }

    /**
     * deleteAddress
     * @param $ID
     * @return bool
     */
    public static function deleteAddress($ID)
    {
        if (CModule::IncludeModule('iblock') ) {
            $iIBlockId = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
            $el = new CIBlockElement;
            $res = $el->Delete($ID);
        }
        return $res;
    }

    /**
     * UpdateDefaultAddress
     * @param bool $iUserId
     * @return bool
     */
    public static function UpdateDefaultAddress($iUserId = false)
    {
        CModule::IncludeModule('iblock');
        $iReturn = 0;
        $IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
        $iUserId = ($iUserId) ? $iUserId : $GLOBALS['USER']->GetID();

        $rsItems = CIBlockElement::GetList(
        	array(
            	'PROPERTY_IS_DEFAULT' => 'ASC'
	        ), 
	        array(
	            'IBLOCK_ID' => $IBLOCK_ID,
    	        '=PROPERTY_USER_ID' => $iUserId,
        	    'PROPERTY_IS_DEFAULT' => 1
	        ), 
	        false, 
	        false, 
			array('ID')
		);
        if ($arItem = $rsItems->Fetch()) {
			CIBlockElement::SetPropertyValuesEx($arItem['ID'], $IBLOCK_ID, array('IS_DEFAULT' => 0));
           	$iReturn = $arItem['ID'];
        } else {
            # Если не нашли
            $iReturn = 1;
        }

        return $iReturn;
    }

    /**
     * check
     * @param $Login
     * @return bool
     */
    public static function checkSync($Login) {
        $rsUser = CUser::GetByLogin($Login);
        $arUser = $rsUser->Fetch();
        if(self::needSync(false, $arUser)) {
            if (self::syncUserById(false, $arUser)) {
                return true;
            }
            return false;
        }
		return true;
    }

    /**
     * IsExistsUser
     * @param $login
     * @return bool
     */
    public static function IsExistsUser($login)
    {
        $rsUser = CUser::GetByLogin($login);
        $arUser = $rsUser->Fetch();
        if ($arUser){
            if ($arUser['LOGIN'] == $login){
                return true;
            }
        }
        return false;
    }

    /**
     * needSync
     * @param bool $UserId
     * @param bool $arUser
     * @return bool
     */
    public static function needSync($iUserId = false, $arUser = array()) {
        if (!$arUser) {
            $iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetID();
            $arUser = CUser::GetByID($iUserId)->Fetch();
        }
        if(!empty($arUser)) {
            $sUFMarker = !empty($arUser['UF_MARKER']) ? trim($arUser['UF_MARKER']) : '';
            if($sUFMarker == 'Y') {
				return false;
            }
	        return true;
        }
        return false;
    }

    // PERSONAL_BIRTHDAY
	public static function GetPersonalBirthday($mUserId = false, $arUser = array()) {
		$sReturn = '';
		if($arUser && isset($arUser['PERSONAL_BIRTHDAY'])) {
			$sReturn = trim($arUser['PERSONAL_BIRTHDAY']);
		} else {
			$iUserId = $mUserId === false ? $GLOBALS['USER']->GetId() : intval($mUserId);
			$sCacheEntity = 'niyama_personal_birthday';
			if($iUserId > 0) {
				if(CStaticCache::IsSetCache($iUserId, $sCacheEntity)) {
					$sReturn = CStaticCache::GetCacheValue($iUserId, $sCacheEntity);
				} else {
					$arUser = CUser::GetByID($iUserId)->Fetch();
					$sReturn = trim($arUser['PERSONAL_BIRTHDAY']);
					CStaticCache::SetCacheValue($iUserId, $sReturn, $sCacheEntity, 100);
				}
			}
		}
		return $sReturn;
	}

    /**
     * GetDiscount
     * @param bool $UserId
     * @return bool
     */
	public static function GetDiscount($iUserId = false) {
		return CNiyamaIBlockCardData::GetUserDiscountDB($iUserId);
    }


	/**
	 * SyncUserById
	 * @param int $UserId
	 * @param array $arUser
	 * @return bool
	 */
	public static  function SyncUserById($iUserId = 0, $arUser = array(), $sSetPassw = '') {
		$bReturn = false;
		if(!$arUser) {
			$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
			$arUser = $iUserId > 0 ? CUser::GetById($iUserId)->Fetch() : array();
		} else {
			$iUserId = $arUser['ID'];
		}
		if(!empty($arUser)) {
			$obFO = new CNiyamaFastOperator();
			// получим массив из текущих данных в FO
			$arCustomerRequest = $obFO->GetCustomerArray($arUser['LOGIN']);

			// сформируем массив по данным на сайте
			$arSendFields = array(
				'Login' => $arUser['LOGIN']
			);

			if(strlen($sSetPassw)) {
				$arSendFields['Pwd'] = $sSetPassw;
			}

			$sFio = '';
			if(strlen($arUser['NAME'])) {
				$sFio .= $arUser['NAME'];
			}
			if(strlen($arUser['LAST_NAME'])) {
				$sFio .= !empty($sFio) ? ' ' : '';
				$sFio .= $arUser['LAST_NAME'];
			}
			if(strlen($sFio)) {
				$arSendFields['FIO'] = $sFio;
			}

			if(!$arSendFields['FIO'] && $arCustomerRequest && $arCustomerRequest['Customer']['FIO']) {
				$arSendFields['FIO'] = $arCustomerRequest['Customer']['FIO'];
			}

			if(!$arSendFields['PersonType'] && $arCustomerRequest && $arCustomerRequest['Customer']['PersonType']) {
				$arSendFields['PersonType'] = $arCustomerRequest['Customer']['PersonType'];
			}

			// приведем номер телефона к формату 111 222-33-44
			$sFormatedTel = $arUser['PERSONAL_PHONE'] ? CNiyamaPds::FormatTel($arUser['PERSONAL_PHONE']) : '';
			$arTmpTel = $sFormatedTel ? explode(' ', $sFormatedTel) : array();
			$sTelCode = $arTmpTel ? $arTmpTel[0] : '';
			$sTelNum = $arTmpTel ? CCustomProject::GetNumsOnly($arTmpTel[1]) : '';
			if(strlen($sTelNum)) {
				$arSendFields['Phones'] = array(
					array(
						'Code' => $sTelCode,
						'Number' => $sTelNum,
						'Remark' => 'PERSONAL_PHONE'
					)
				);
			}

			if(!$arSendFields['Phones'] && $arCustomerRequest && $arCustomerRequest['Phones'] && is_array($arCustomerRequest['Phones'])) {
				$arSendFields['Phones'] = array();
				foreach($arCustomerRequest['Phones'] as $arTpmPhone) {
					$sFormatedTel = $arTpmPhone['Number'] ? CNiyamaPds::FormatTel($arTpmPhone['Number']) : '';
					$arTmpTel = $sFormatedTel ? explode(' ', $sFormatedTel) : array();
					$sTelCode = $arTmpTel ? $arTmpTel[0] : '';
					$sTelNum = $arTmpTel ? CCustomProject::GetNumsOnly($arTmpTel[1]) : '';
					$arSendFields['Phones'][] = array(
						'Code' => $sTelCode,
						'Number' => $sTelNum,
						'Remark' => $arTpmPhone['Remark']
					);
				}
			}

			$arAddress = self::GetDefaultAddressByUser($iUserId);
			if($arAddress && !empty($arAddress)) {
				$sSityName = '';
				if($arAddress['PROPERTY_REGION_VALUE']) {
					$arDeliveryRegions = CNiyamaOrders::GetDeliveryRegions();
					if($arDeliveryRegions[$arAddress['PROPERTY_REGION_VALUE']]) {
						$sSityName = $arDeliveryRegions[$arAddress['PROPERTY_REGION_VALUE']]['NAME'];
					}
				}
				if(!strlen($sSityName)) {
					$sSityName = $arAddress['PROPERTY_CITY_DOP_VALUE'];
				}
				$arSendFields['Addresses'] = array(
					array(
						// город
						'CityName' => $sSityName,
						// станция метро
						'StationName' => $arAddress['PROPERTY_SUBWAY_VALUE'],
						// улица
						'StreetName' => $arAddress['PROPERTY_ADDRESS_VALUE'],
						// дом
						'House' => $arAddress['PROPERTY_HOUSE_VALUE'],
						// корпус
						'Corpus' => '',
						// строение
						'Building' => '',
						// квартира
						'Flat' => $arAddress['PROPERTY_HOME_VALUE'],
						// подъезд
						'Porch' => $arAddress['PROPERTY_PORCH_VALUE'],
						// этаж
						'Floor' => $arAddress['PROPERTY_FLOOR_VALUE'],
						// код домофона
						'DoorCode' => $arAddress['PROPERTY_INTERCOM_VALUE'],
						// комната
						'Room' => '',
						// офис
						'Office' => '',
						// комментарий
						'Remark' => 'DEFAULT_ADDRESS'
					),
				);
			}
			if(!$arSendFields['Addresses'] && $arCustomerRequest && $arCustomerRequest['Addresses'] && is_array($arCustomerRequest['Addresses'])) {
				$arSendFields['Addresses'] = array();
				foreach($arCustomerRequest['Addresses'] as $arTpmAddress) {
					$arSendFields['Addresses'][] = $arTpmAddress;
				}
			}

			$sCardNum = CNiyamaIBlockCardData::GetCardNumber($iUserId);
			if($sCardNum) {
				$arSendFields['CardsDiscount'] = $sCardNum;
			}

			$sMarkerVal = 'E';
			if($obFO->EditCustomer($arSendFields)) {
				$bReturn = true;
				$sMarkerVal = 'Y';
			}

			if($GLOBALS['USER_FIELD_MANAGER']->Update('USER', $iUserId, array('UF_MARKER' => $sMarkerVal))) {
				CEventLog::Add(
					array(
						'SEVERITY' => 'SECURITY',
						'AUDIT_TYPE_ID' => 'NIYAMA_FO_SYNC',
						'MODULE_ID' => 'main',
						'ITEM_ID' => $iUserId,
						'DESCRIPTION' => 'Обновлен маркер синхронизации для пользователя ['.$iUserId.'] как '.$sMarkerVal,
					)
				);
			}
		}
		return $bReturn;
	}
	
	public static function GetValByUfName($UF, $user_id = 0) {
		if ((empty($user_id))||($user_id<=0)){	
			global $USER;
			$user_id=$USER->GetID();
		}
		$result='';
		if ($user_id<=0){
			return $result;
		}
		$typeUF='UF_'.$UF;
		$arFilter = array("ID" => $user_id);
		$arParams["SELECT"] = array($typeUF);
		$arRes = CUser::GetList(($by="id"), ($order="asc"),$arFilter,$arParams);
     	if ($res = $arRes->Fetch()) {
			if (!empty($res[$typeUF])){
				$result=$res[$typeUF];
			}
     	}
		return $result;
	}


	/*
	 * Гостевой пользователь
	 */

    public static function GetGuestUserId() {

        $GUEST_EMAIL_OPTION = 'guest_email';

        $guestEmail  = CNiyamaCustomSettings::GetStringValue($GUEST_EMAIL_OPTION);
        $userId = null;
        if($guestEmail){
            $by="";
            $order="";
            $filter = Array
            (
                '=EMAIL' => $guestEmail,
            );
            $rsUsers = CUser::GetList($by, $order, $filter); // выбираем пользователей
            if ($rsUsers->SelectedRowsCount()>0) {
                $user = $rsUsers->Fetch(); // печатаем постраничную навигацию
                if($user) {
                    $userId = $user['ID'];
                }
            } else {
                CEventLog::Add(array(
                    "SEVERITY" => "WARNING",
                    "AUDIT_TYPE_ID" => "Гостевой пользователь",
                    "MODULE_ID" => "Анонимные пользователь",
                    "ITEM_ID" => 123,
                    "DESCRIPTION" => "Не найден гостевой пользователь с почтой {$guestEmail}",
                ));
                $user = new CUser;
                $randString = randString(11, array(
                    "abcdefghijklnmopqrstuvwxyz",
                    "ABCDEFGHIJKLNMOPQRSTUVWX­YZ",
                    "0123456789",
                    "!@#\$%^&*()",
                ));
                $arFields = Array(
                    "NAME"              => "Анонимный пользователь",
                    "EMAIL"             => $guestEmail,
                    "LOGIN"             => $guestEmail,
                    "ACTIVE"            => "Y",
                    "GROUP_ID"          => array(3,4),
                    "PASSWORD"          => $randString,
                    "CONFIRM_PASSWORD"  => $randString,

                );
                $ID = $user->Add($arFields);
                if (intval($ID) > 0)
                    $userId = $ID;
                else {
                    CEventLog::Add(array(
                        "SEVERITY" => "WARNING",
                        "AUDIT_TYPE_ID" => "Создание гостевого пользователя",
                        "MODULE_ID" => "Анонимные пользователь",
                        "ITEM_ID" => 123,
                        "DESCRIPTION" => "Ошибка при попытке добавления нового гостевого пользователя с почтой {$guestEmail}",
                    ));
                }
            }

        }
        return $userId;
    }

    public static function isGuest() {
        global $USER;
        if($USER->IsAuthorized()) {
            $currentUserId = $USER->GetID();
            $guestUserId = self::GetGuestUserId();
            if($guestUserId == $currentUserId){
                return true;
            }
        }
        return false;
    }

    /*
	 * Гостевой пользователь
	 */



}
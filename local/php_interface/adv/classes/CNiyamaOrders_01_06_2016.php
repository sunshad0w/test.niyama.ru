<?
/**
 * Система заказов сайта Нияма
 * 
 * @author Sergey Leshchenko, 2014
 * @updated: 22.09.2015
 */

class CNiyamaOrders {
	public static $arAcquiroPayIdentities = array(
		'card' => 'Банковская карта',
		'wm' => 'WebMoney',
		'qiwi' => 'QIWI',
		'rbkmoney' => 'RBK Money',
		'yandexmoney' => 'Яндекс.Деньги',
		'mailru' => 'Деньги@Mail.ru',
		'euroset' => 'Евросеть',
		'rapida' => 'Rapida',
		'contact' => 'CONTACT',
		'western_union' => 'Western Union',
		'mobpay' => 'Мобильный платеж',
		'mobpay_beeline' => 'Мобильный платеж Билайн',
		'mobpay_megafon' => 'Мобильный платеж Мегафон',
		'mobpay_mts' => 'Мобильный платеж МТС',
		'eleksnet' => 'Терминал Элекснет',
	);

	public static function GetAcquiroPayMethodName($sPaymentIdentity, $sDefValue = '') {
		return strlen($sPaymentIdentity) && isset(self::$arAcquiroPayIdentities[$sPaymentIdentity]) ? self::$arAcquiroPayIdentities[$sPaymentIdentity] : $sDefValue;
	}

	public static function FormatPrice($dPrice, $sPattern = '#PRICE# <span class="rouble">a </span>') {
		return str_replace('#PRICE#', CProjectUtils::FormatNum($dPrice), $sPattern);
	}

	//
	// Возвращает список регионов доставки (местоположения)
	//
	public static function GetDeliveryRegions($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('sale')) {
				$dbItems = CSaleLocation::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					array(
						'>CITY_ID' => 0
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'ACTIVE' => 'Y',
						'NAME' => $arItem['CITY_NAME']
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetDeliveryRegionsByFilter($bRefreshCache = false, $arPFilter = null) {
		
		$arFilter = array(
			'>CITY_ID' => 0
		);
		if (!is_null($arPFilter)) {
			if (is_array($arPFilter)) {
				$arFilter = array_merge($arFilter, $arPFilter);
			}
		}
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'v2'.md5(serialize($arFilter));
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('sale')) {
				$dbItems = CSaleLocation::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					$arFilter
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'ACTIVE' => 'Y',
						'NAME' => $arItem['CITY_NAME']
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetPrivateClientPersonTypeId($bRefreshCache = false) {
		$iReturn = 0;
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$iReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('sale')) {
				$dbItems = CSalePersonType::GetList(
					array(
						'ID' => 'ASC'
					),
					array(
						'LID' => 's1',
						'ACTIVE' => 'Y',
						'=NAME' => 'Физическое лицо'
					)
				);
				if($arItem = $dbItems->Fetch()) {
					$iReturn = $arItem['ID'];
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($iReturn);
		}
		return $iReturn;
	}

	// перенос корзины от анонимного покупателя к зарегистрированному
	// (кейс: аноним набрал корзину и при оформлении заказа авторизовался)
	// Внимание! В модуле sale имеется обработчик события OnUserLogin, который может изменить значение FUSER_ID,
	// для корректной работы метода наш обработчик должен отработать позже
	public static function TransferAnonymousOrders($iCurUserId, $iAnonymousFUserId) {
		$bWasTransfering = false;
		if(CModule::IncludeModule('sale')) {
			$iAnonymousUserId = CSaleUser::GetAnonymousUserId();
			if($iAnonymousUserId && $iCurUserId != $iAnonymousUserId) {
				if($iAnonymousFUserId) {
					// получим заказ-черновик анонима, который нужно перевести на текущего юзера
					$iTransferOrderId = 0;
					$arDelOrders = array();
					$dbItems = CSaleOrder::GetList(
						array(
							'ID' => 'DESC'
						),
						array(
							'LID' => 's1',
							'STATUS_ID' => 'D', // здесь, может, тоже нужно проверять статус заказа P (ожидает оплаты)?
							'USER_ID' => $iAnonymousUserId,
							'ADDITIONAL_INFO' => $iAnonymousFUserId
						),
						false,
						false,
						array(
							'ID'
						)
					);
					while($arItem = $dbItems->Fetch()) {
						if(!$iTransferOrderId) {
							$iTransferOrderId = $arItem['ID'];
						} else {
							$arDelOrders[] = $arItem['ID'];
						}
					}

					if($iTransferOrderId) {
						// все заказы-черновики текущего юзера удаляем
						$dbItems = CSaleOrder::GetList(
							array(
								'ID' => 'DESC'
							),
							array(
								'LID' => 's1',
								'STATUS_ID' => 'D', // ну и здесь P (ожидает оплаты)?
								'USER_ID' => $iCurUserId,
							),
							false,
							false,
							array(
								'ID'
							)
						);
						while($arItem = $dbItems->Fetch()) {
							$arDelOrders[] = $arItem['ID'];
						}

						// получим FUSER_ID авторизовавшегося юзера
						$iCurFUserId = CSaleBasket::GetBasketUserId();
						if($iCurFUserId <= 0) {
							$arTmpItem = CSaleUser::GetList(
								array(
									'USER_ID' => $iCurUserId
								)
							);
							if($arTmpItem) {
								$iCurFUserId = $arTmpItem['ID'];
							}
						}
						if($iCurFUserId <= 0) {
							$iCurFUserId = CSaleUser::Add();
						}

						// переводим заказ-черновик анонима на текущего юзера
						$iTransferOrderIdResult = 0;
						if($iCurFUserId) {
							if($iAnonymousFUserId != $iCurFUserId) {
								$dbItems = CSaleBasket::GetList(
									array(
										'ID' => 'DESC'
									),
									array(
										'LID' => 's1',
										'FUSER_ID' => $iAnonymousFUserId,
									),
									false,
									false,
									array(
										'ID'
									)
								);
								while($arItem = $dbItems->Fetch()) {
									CSaleBasket::Update($arItem['ID'], array('FUSER_ID' => $iCurFUserId));
								}
							}
							$iTransferOrderIdResult = CSaleOrder::Update(
								$iTransferOrderId,
								array(
									'USER_ID' => $iCurUserId,
									'ADDITIONAL_INFO' => $iCurFUserId
								)
							);
						}

						if($iTransferOrderIdResult) {
							$bWasTransfering = true;
							if($arDelOrders) {
								$arDelOrders = array_unique($arDelOrders);
								foreach($arDelOrders as $iDelOrderId) {
									CSaleOrder::Delete($iDelOrderId);
								}
							}

							$arEventHandlers = GetModuleEvents('main', 'OnTransferAnonymousOrders', true);
							foreach($arEventHandlers as $arEvent) {
								ExecuteModuleEventEx($arEvent, array($iCurUserId, $iCurFUserId, $iTransferOrderIdResult));
							}

						}
					}
				}
			}
		}
		return $bWasTransfering;
	}

	//
	// возвращает ID заказа-черновика
	//
	public static function GetDraftOrderExact($iFUserId, $iUserId = 0, $bRefreshCache = false) {
		static $arReturnOrder = array();

		$iFUserId = intval($iFUserId);
		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			$iUserId = CSaleUser::GetAnonymousUserId();
			//$iUserId = self::GetUserIdByFUserId($iFUserId);
		}

		$sCacheKey = $iFUserId.'_'.$iUserId;
		if($arReturnOrder[$sCacheKey] && !$bRefreshCache) {
			return $arReturnOrder[$sCacheKey];
		}
		$arReturnOrder[$sCacheKey] = array();

		CModule::IncludeModule('sale');

		if($iUserId && $iFUserId) {
			$dbItems = CSaleOrder::GetList(
				array(
					'ID' => 'DESC'
				),
				array(
					'LID' => 's1',
					//'STATUS_ID' => 'D',
					'@STATUS_ID' => array('D', 'P'),
					'USER_ID' => $iUserId,
					'ADDITIONAL_INFO' => $iFUserId
				)
			);
			$arReturnOrder[$sCacheKey] = $dbItems->Fetch();
			if(!$arReturnOrder[$sCacheKey]) {
				$iOrderId = CSaleOrder::Add(
					array(
						'LID' => 's1',
						'PERSON_TYPE_ID' => self::GetPrivateClientPersonTypeId(),
						'PAYED' => 'N',
						'CANCELED' => 'N',
						'STATUS_ID' => 'D',
						'PRICE' => 0,
						'CURRENCY' => 'RUB',
						'USER_ID' => $iUserId,
						'ADDITIONAL_INFO' => $iFUserId
					)
				);

				if($iOrderId) {
					// привязываем к заказу текущую корзину
					CSaleBasket::OrderBasket($iOrderId, $iFUserId, 's1');
					$dbItems = CSaleOrder::GetList(
						array(
							'ID' => 'ASC'
						),
						array(
							'ID' => $iOrderId
						)
					);
					$arReturnOrder[$sCacheKey] = $dbItems->Fetch();
				}
			}
		}
		return $arReturnOrder[$sCacheKey];
	}

// 
// to do: подумать над оптимизацией - метод вызывается несколько раз за хит
//
	//
	// возвращает ID заказа-черновика, если у пользователя корзина имеет товары
	//
	public static function GetDraftOrder($iFUserId, $iUserId = 0, $bRefreshCache = false) {
		$arReturn = array();
		if($iFUserId) {
			CModule::IncludeModule('sale');
			$iBasketItemsCnt = CSaleBasket::GetList(
				array(),
				array(
					'FUSER_ID' => $iFUserId,
					'LID' => 's1',
					//'ORDER_STATUS' => 'D'
					'@ORDER_STATUS' => array('D', 'P'),
				),
				array()
			);

			if($iBasketItemsCnt > 0) {
				$arReturn = self::GetDraftOrderExact($iFUserId, $iUserId, $bRefreshCache);
			}
		}
		return $arReturn;
	}

	public static function GetUserIdByFUserId($iFUserId, $bRefreshCache = false) {
		static $arStatCache = array();
		if(!isset($arStatCache[$iFUserId]) || $bRefreshCache) {
			$iUserIdByFUser = CSaleUser::GetUserId($iFUserId);
			if($iUserIdByFUser) {
				$arStatCache[$iFUserId] = $iUserIdByFUser;
			} else {
				$arStatCache[$iFUserId] = CSaleUser::GetAnonymousUserId();
			}
		}
		return $arStatCache[$iFUserId];
	}

	public static function GetFUserIdByUserId($iUserId) {
		static $arStatCache = array();
		if(!isset($arStatCache[$iUserId]) || $bRefreshCache) {
			CModule::IncludeModule('sale');
			$arItem = CSaleUser::GetList(array('USER_ID' => $iUserId));
			if(!$arItem) {
				//
				// !!! Использование метода CSaleUser::_Add() может в будущем вызвать проблемы, т.к. похоже, что это защищенный метод, 
				// но без этого никак не создать запись FUSER по ID юзера
				//
				$arFields = array(
					'=DATE_INSERT' => $GLOBALS['DB']->GetNowFunction(),
					'=DATE_UPDATE' => $GLOBALS['DB']->GetNowFunction(),
					'USER_ID' => $iUserId,
					'CODE' => md5(time().RandString(10)),
				);
				$arStatCache[$iUserId] = CSaleUser::_Add($arFields);
			} else {
				$arStatCache[$iUserId] = $arItem['ID'];
			}
		}
		return $arStatCache[$iUserId];
	}

	public static function GetCurUserDraftOrder($bRefreshCache = false) {
		CModule::IncludeModule('sale');
		$iFUserId = CSaleBasket::GetBasketUserId();
		$iUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetId() : 0;
		return self::GetDraftOrder($iFUserId, $iUserId, $bRefreshCache);
	}

	public static function GetUserDraftOrderId($iFUserId, $iUserId, $bRefreshCache = false) {
		$arDraftOrder = self::GetDraftOrder($iFUserId, $iUserId, $bRefreshCache);
		return $arDraftOrder['ID'] ? $arDraftOrder['ID'] : false;
	}

	public static function GetCurUserDraftOrderId($bRefreshCache = false) {
		$arDraftOrder = self::GetCurUserDraftOrder($bRefreshCache);
		return $arDraftOrder['ID'] ? $arDraftOrder['ID'] : false;
	}

	public static function GetCurUserDraftOrderNumber($bRefreshCache = false) {
		$arDraftOrder = self::GetCurUserDraftOrder($bRefreshCache);
		return $arDraftOrder['ACCOUNT_NUMBER'] ? $arDraftOrder['ACCOUNT_NUMBER'] : '';
	}

	//
	// Возвращает список способов оплаты
	//
	public static function GetPayments($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('sale')) {
				$dbItems = CSalePaySystem::GetList(
					array(
						'SORT' => 'ASC',
					),
					array(
					),
					false,
					false,
					array(
						'ID', 'NAME', 'ACTIVE', 'SORT', 'DESCRIPTION', 
						'PSA_NAME', 'PSA_ID', 'PSA_PERSON_TYPE_ID', 'PSA_LOGOTIP'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arItem['IS_CASH'] = $arItem['NAME'] == 'Наличные' ? 'Y' : 'N';
					$arReturn[$arItem['ID']] = $arItem;
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetCurDeliveryTimePeriod() {
		$sReturn = date('H:i').'-'.date('H:i', (time() + 1800));
		return $sReturn;
	}

	public static function ParseTimePeriodString($sParseStr) {
		$arReturn = array(
			'LEFT_TIME_H' => 0,
			'LEFT_TIME_M' => 0,
			'LEFT_TIME' => 0,
			'RIGHT_TIME_H' => 23,
			'RIGHT_TIME_M' => 59,
			'RIGHT_TIME' => 23 * 60 + 59,
		);
		$sTmpTime = trim($sParseStr);
		if(strlen($sTmpTime)) {
			$arTmp = explode('-', $sTmpTime);
			if(count($arTmp) == 2) {
				$iLeftH = 0;
				$iLeftM = 0;
				$iRightH = 0;
				$iRightM = 0;
				$arTmp2 = explode(':', trim($arTmp[0]));
				if(count($arTmp2) == 2) {
					$iLeftH = intval($arTmp2[0]);
					$iLeftH = $iLeftH > 23 || $iLeftH < 0 ? 0 : $iLeftH;
					$iLeftM = intval($arTmp2[1]);
					$iLeftM = $iLeftM > 59 || $iLeftM < 0 ? 0 : $iLeftM;
				}
				$arTmp2 = explode(':', trim($arTmp[1]));
				if(count($arTmp2) == 2) {
					$iRightH = intval($arTmp2[0]);
					$iRightH = $iRightH > 23 || $iRightH < 0 ? 0 : $iRightH;
					$iRightM = intval($arTmp2[1]);
					$iRightM = $iRightM > 59 || $iRightM < 0 ? 0 : $iRightM;
				}
				$arReturn['LEFT_TIME_H'] = $iLeftH;
				$arReturn['LEFT_TIME_M'] = $iLeftM;
				$arReturn['LEFT_TIME'] = $iLeftH * 60 + $iLeftM;
				$arReturn['RIGHT_TIME_H'] = $iRightH;
				$arReturn['RIGHT_TIME_M'] = $iRightM;
				$arReturn['RIGHT_TIME'] = $iRightH * 60 + $iRightM;
			}
		}
		return $arReturn;
	}

	public static function GetDeliveryMatrix($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('delivery', 'orders');
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'ACTIVE' => 'Y'
					),
					false,
					false,
					array(
						'ID', 'IBLOCK_ID', 'NAME',
						'PROPERTY_*'
					)
				);
				while($obItem = $dbItems->GetNextElement(false, false)) {
					$arItem = $obItem->GetFields();
					$arProp = $obItem->GetProperties();
					$arReturn['ZONES'][$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'R_REGION' => $arProp['R_REGION']['VALUE'],
						'B_MKAD' => $arProp['B_MKAD']['VALUE'],
						'S_TIME_PERIOD' => $arProp['S_TIME_PERIOD']['VALUE'],
						'D_MIN_ORDER_PRICE' => doubleval($arProp['D_MIN_ORDER_PRICE']['VALUE']),
						'D_DELIVERY_PRICE' => doubleval($arProp['D_DELIVERY_PRICE']['VALUE']),
					);
					$arTmp = self::ParseTimePeriodString($arProp['S_TIME_PERIOD']['VALUE']);
					$arReturn['ZONES'][$arItem['ID']]['LEFT_TIME_H'] = $arTmp['LEFT_TIME_H'];
					$arReturn['ZONES'][$arItem['ID']]['LEFT_TIME_M'] = $arTmp['LEFT_TIME_M'];
					$arReturn['ZONES'][$arItem['ID']]['LEFT_TIME'] = $arTmp['LEFT_TIME'];
					$arReturn['ZONES'][$arItem['ID']]['RIGHT_TIME_H'] = $arTmp['RIGHT_TIME_H'];
					$arReturn['ZONES'][$arItem['ID']]['RIGHT_TIME_M'] = $arTmp['RIGHT_TIME_M'];
					$arReturn['ZONES'][$arItem['ID']]['RIGHT_TIME'] = $arTmp['RIGHT_TIME'];

					if($arProp['R_REGION']['VALUE'] && is_array($arProp['R_REGION']['VALUE'])) {
						foreach($arProp['R_REGION']['VALUE'] as $iTmpVal) {
							$arReturn['REGION2ZONE'][$iTmpVal][$arItem['ID']] = $arItem['ID'];
						}
					}
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetOrderPrices($iOrderId) {
		$arReturn = array(
			'ORDER_STATUS_ID' => '',
			'ORDER_PRICE' => 0,
			'ORDER_DISCOUNT_VALUE' => 0,
		);
		CModule::IncludeModule('sale');
		$arOrder = CSaleOrder::GetById($iOrderId);
		if($arOrder) {
			$arReturn['ORDER_STATUS_ID'] = $arOrder['STATUS_ID'];
			$dbItems = CSaleBasket::GetList(
				array(),
				array(
					'ORDER_ID' => $iOrderId,
					'CAN_BUY' => 'Y',
					'DELAY' => 'N',
					'LID' => 's1',
				),
				false,
				false,
				array(
					'QUANTITY', 'PRICE', 'ID'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				if($arItem['PRICE']) {
					$arReturn['ORDER_PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
				}
			}
			$arPrice = CNiyamaCoupons::CalcArPriceByCoupons($iOrderId, array());
			if($arPrice['DISCOUNT_ALL'] > 0) {
				$arReturn['ORDER_PRICE'] -= $arPrice['DISCOUNT_ALL'];
				$arReturn['ORDER_DISCOUNT_VALUE']= $arPrice['DISCOUNT_ALL'];
			}
		}

		return $arReturn;
	}

	//
	// Выполняет расчет стоимости и возможности доставки заказа курьером по переданным параметрам
	// $arParams = array(
	//	'LOCATION_ID' => 'id местоположения доставки',
	//	'ORDER_PRICE' => 'сумма заказа',
	//	'B_WITHIN_MKAD' => 'в пределах МКАД',
	//	'TIME_PERIOD' => 'период доставки hh:mm-hh:mm',
	// )
	//
	public static function CalcOrderCourierDelivery($arParams) {
		$arReturn = array(
			'DELIVERY_AVAILABLE' => 'N',
			'DELIVERY_PRICE' => 0,
			'FREE_DELIVERY_BONUS' => 'N',
			'ORDER_PRICE_LEFT' => 0, // сумма, которой не хватает для доставки
		);

		$arDeliveryMatrix = self::GetDeliveryMatrix();
		if($arParams['LOCATION_ID'] && $arDeliveryMatrix['REGION2ZONE'][$arParams['LOCATION_ID']]) {
			$sTimePeriod = isset($arParams['TIME_PERIOD']) ? trim($arParams['TIME_PERIOD']) : '';
			$sTimePeriod = strlen($sTimePeriod) ? $sTimePeriod : self::GetCurDeliveryTimePeriod();
			$arTimePeriod = CNiyamaOrders::ParseTimePeriodString($sTimePeriod);
			foreach($arDeliveryMatrix['REGION2ZONE'][$arParams['LOCATION_ID']] as $iZoneId) {
				$arCurZone = $arDeliveryMatrix['ZONES'][$iZoneId];
				if(!$arCurZone['B_MKAD'] || ($arCurZone['B_MKAD'] && $arParams['B_WITHIN_MKAD'])) {
					$bInPeriod = CProjectUtils::IsTimeInPeriod($arTimePeriod['LEFT_TIME'], $arCurZone['LEFT_TIME'], $arCurZone['RIGHT_TIME']);
					if($bInPeriod) {
						if($arCurZone['D_MIN_ORDER_PRICE'] <= $arParams['ORDER_PRICE']) {
							$arReturn['DELIVERY_AVAILABLE'] = 'Y';
							$arReturn['DELIVERY_PRICE'] = $arCurZone['D_DELIVERY_PRICE'];
						} else {
							$dTmp = $arCurZone['D_MIN_ORDER_PRICE'] - $arParams['ORDER_PRICE'];
							$arReturn['ORDER_PRICE_LEFT'] = $arReturn['ORDER_PRICE_LEFT'] > 0 && $arReturn['ORDER_PRICE_LEFT'] < $dTmp ? $arReturn['ORDER_PRICE_LEFT'] : $dTmp;
						}
					}
				}
			}
		}

		// бесплатная доставка по таймлайну скидок текущего заказа
		$arTmp = CNiyamaIBlockCartLine::GetLineByPriceVal($dOrderPrice);
		$arTimeLine = array();
		foreach($arTmp as $arTmpItem) {
			if($arTmpItem['ACTIVE'] == 'Y') {
				$arTimeLine[$arTmpItem['XML_ID']] = $arTmpItem['XML_ID'];
			}
		}
		$sFreeDeliveryCode = '';
		if($arTimeLine['free_delivery']) {
			$sFreeDeliveryCode = 'free_delivery';
		} elseif($arTimeLine['free_delivery_mkad']) {
			$sFreeDeliveryCode = 'free_delivery_mkad';
		}

		switch($sFreeDeliveryCode) {
			case 'free_delivery':
				$arReturn['DELIVERY_PRICE'] = 0;
				$arReturn['FREE_DELIVERY_BONUS'] = 'Y';
			break;

			case 'free_delivery_mkad':
				if($arParams['B_WITHIN_MKAD']) {
					$arReturn['DELIVERY_PRICE'] = 0;
					$arReturn['FREE_DELIVERY_BONUS'] = 'Y';
				}
			break;
		}

		return $arReturn;
	}

	public static function CartChangeHandler($iBasketItemId, $iOrderId = 0) {
		$bReturn = false;
		// обновим цену заказа
		$iBasketItemId = intval($iBasketItemId);
		$iOrderId = intval($iOrderId);
		if($iBasketItemId <= 0 && $iOrderId <= 0) {
			return $bReturn;
		}
		CModule::IncludeModule('sale');
		if($iOrderId <= 0) {
			$arBasketItem = CSaleBasket::GetById($iBasketItemId);
			if($arBasketItem['ORDER_ID']) {
				$iOrderId = $arBasketItem['ORDER_ID'];
			}
		}

		if($iOrderId) {
			$arOrderPrices = self::GetOrderPrices($iOrderId);
			if(in_array($arOrderPrices['ORDER_STATUS_ID'], array('D', 'P'))) {
				$bReturn = CSaleOrder::Update(
					$iOrderId,
					array(
						'PRICE' => $arOrderPrices['ORDER_PRICE'],
						'DISCOUNT_VALUE' => $arOrderPrices['ORDER_DISCOUNT_VALUE'],
					)
				);
			}
		}
		return $bReturn;
	}

	// устанавливает/обновляет значения свойств заказа (кроме файловых типов)
	public static function SetOrderProps($iOrderId, $iPersonTypeId, $arOrderProps, &$arErrors, $iPaySystemId = 0, $sDeliveryId = '') {
		CModule::IncludeModule('sale');
		$arIDs = array();
		$dbResult = CSaleOrderPropsValue::GetList(
			array(),
			array(
				'ORDER_ID' => $iOrderId
			),
			false,
			false,
			array('ID', 'ORDER_PROPS_ID')
		);
		while($arResult = $dbResult->Fetch()) {
			$arIDs[$arResult['ORDER_PROPS_ID']] = $arResult['ID'];
		}

		$arFilter = array(
			'PERSON_TYPE_ID' => $iPersonTypeId,
			'ACTIVE' => 'Y'
		);

		if($iPaySystemId != 0) {
			$arFilter['RELATED']['PAYSYSTEM_ID'] = $iPaySystemId;
			$arFilter['RELATED']['TYPE'] = 'WITH_NOT_RELATED';
		}

		if(strlen($sDeliveryId)) {
			$arFilter['RELATED']['DELIVERY_ID'] = $sDeliveryId;
			$arFilter['RELATED']['TYPE'] = 'WITH_NOT_RELATED';
		}

		$dbOrderProperties = CSaleOrderProps::GetList(
			array(
				'SORT' => 'ASC'
			),
			$arFilter,
			false,
			false,
			array(
				'ID', 'TYPE', 'NAME', 'CODE', 'USER_PROPS', 'SORT'
			)
		);
		while($arOrderProperty = $dbOrderProperties->Fetch()) {

			$mCurVal = $arOrderProps[$arOrderProperty['CODE']];
			if(($arOrderProperty['TYPE'] == 'MULTISELECT') && is_array($mCurVal)) {
				$mCurVal = implode(',', $mCurVal);
			} elseif(is_array($mCurVal)) {
				$mCurVal = implode(',', $mCurVal);
			}

			if(strlen($mCurVal)) {
				$arFields = array(
					'ORDER_ID' => $iOrderId,
					'ORDER_PROPS_ID' => $arOrderProperty['ID'],
					'NAME' => $arOrderProperty['NAME'],
					'CODE' => $arOrderProperty['CODE'],
					'VALUE' => $mCurVal
				);

				if(array_key_exists($arOrderProperty['ID'], $arIDs)) {
					CSaleOrderPropsValue::Update($arIDs[$arOrderProperty['ID']], $arFields);
					unset($arIDs[$arOrderProperty['ID']]);
				} else {
					CSaleOrderPropsValue::Add($arFields);
				}
			}
		}
	}

	public static function _GetOrderPropValue($arOrderPropsValue, $sPropCode, $bTrim = true) {
		$sValue = '';
		if($arOrderPropsValue[$sPropCode]) {
			$arTmp = reset($arOrderPropsValue[$sPropCode]);
			$sValue = $bTrim ? trim($arTmp['VALUE']) : $arTmp['VALUE'];
		}
		return $sValue;
	}

	//
	// Возвращает массив с информацией о статусе заказа для сайта по коду статуса FO
	//
	public static function GetSiteOrderStatusByFoStatusCode($iFastOperatorOrderStatusId) {
		$arReturn = array(
			'STATUS_ID' => '',
			'CANCELED' => 'N',
		);
		switch($iFastOperatorOrderStatusId) {
			case '2':
				// 2 - принят
				$arReturn['STATUS_ID'] = 'N';
			break;

			case '4':
				// 4 - исполнен
				$arReturn['STATUS_ID'] = 'F';
			break;

			case '6':
				// 6 - отказан
				$arReturn['CANCELED'] = 'Y';
			break;

			case '9':
			case '12':
				// 12 - отложен
				// 9 - отложен-уточнение
				$arReturn['STATUS_ID'] = 'S';
			break;

			case '15':
				// 15 - доставляется
				$arReturn['STATUS_ID'] = 'T';
			break;

			default:
				// 1 - новый
			break;
		}
		return $arReturn;
	}

	// возвращает код города "другой"
	public static function GetOtherCityId() {
		static $iCityOtherId = null;
		if(is_null($iCityOtherId)) {
			$iCityOtherId = intval(CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other', '27'));
		}
		return $iCityOtherId;
	}

	public static function IsAnonymousUserId($iCheckUserId) {
		static $iAnonymousUserId = false;
		if($iAnonymousUserId === false) {
			CModule::IncludeModule('sale');
			$iAnonymousUserId = CSaleUser::GetAnonymousUserId();
		}
		return $iAnonymousUserId == $iCheckUserId;
	}

	public static function GetOrderGuestsList($iOrderOwnerId, $iOrderFUserId, $bRefreshCache = false) {
		$arReturn = array();
		$iOrderOwnerId = intval($iOrderOwnerId);
		$iOrderFUserId = intval($iOrderFUserId);
		if(self::IsAnonymousUserId($iOrderOwnerId)) {
			// если анонимный покупатель, то берем список гостей по коду корзины FUSER_ID
			$arReturn = $iOrderFUserId ? CNiyamaCart::GetGuestsList(0, $bRefreshCache, true, true, $iOrderFUserId) : array();
		} else {
			// если зарегистрированный покупатель, то берем список гостей по ID юзера
			$arReturn = $iOrderOwnerId ? CNiyamaCart::GetGuestsList($iOrderOwnerId, $bRefreshCache, false) : array();
		}
		return $arReturn;
	}

	//
	// Экспорт заказа в FO
	//
	public static function ExportOrder2FO($iOrderId) {


		$bReturn = false;
		CModule::IncludeModule('sale');
		$arOrderData = self::GetOrderById($iOrderId);


		if($arOrderData && !$arOrderData['ORDER']['UF_RECEIVED_FROM_FO']) {
			$sOrderDesc = '';
			$sNl = "\n";
			//
			// Customer
			//
			$arUser = CUser::GetById($arOrderData['ORDER']['USER_ID'])->Fetch();
			$sFio = '';
			$sFio = strlen($sFio) ? $sFio : trim(self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_NAME').' '.self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_LAST_NAME'));
			$iOrderFUserId = intval($arOrderData['ORDER']['ADDITIONAL_INFO']);
			$arGuestsList = self::GetOrderGuestsList($arOrderData['ORDER']['USER_ID'], $iOrderFUserId, false);
			if($arUser['ACTIVE'] == 'Y' && !self::IsAnonymousUserId($arOrderData['ORDER']['USER_ID'])) {
				$sLogin = $arUser['EMAIL'];
				$sFio = strlen($sFio) ? $sFio : trim($arOrderData['ORDER']['USER_NAME'].' '.$arOrderData['ORDER']['USER_LAST_NAME']);
			} else {
				$sLogin = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_EMAIL');
			}
			$sFio = strlen($sFio) ? $sFio : 'anonymous';
			$arXmlCustomer = array(
				'Login' => $sLogin,
				'FIO' => $sFio,
			);

			$sOrderDesc .= 'Имя: '.$sFio.$sNl;

			//
			// Phone
			//
			// важно! Телефон не должен быть длиннее 10 символов, иначе FO не примет запрос
			$sTmpValOrig = trim(self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_PHONE'));
			$sTmpVal = CCustomProject::GetNumsOnly(CNiyamaPds::FormatTel($sTmpValOrig));
			if(strlen($sTmpVal) == 10) {
				$sPhoneCode = substr($sTmpVal, 0, 3);
				$sPhoneNumber = substr($sTmpVal, 3);
			} else {
				$sPhoneCode = '';
				$sPhoneNumber = strlen($sTmpVal) <= 10 ? $sTmpVal : '-';
			}
			$arXmlPhone = array(
				'Code' => $sPhoneCode, // код
				'Number' => $sPhoneNumber, // номер
				'Remark' => $sTmpValOrig // комментарий
			);

			$sOrderDesc .= 'Телефон: +7 ('.$sPhoneCode.') '.$sPhoneNumber.$sNl;

			//
			// Address
			//
			$arXmlAddress = array();
			$sXmlType = '';
			$sDeliveryType = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_TYPE');
			if($sDeliveryType == 'courier') {
				$sXmlType = 'доставка курьером';
				$arXmlAddress = array(
					'CityName' => '', // город
					'StationName' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_SUBWAY'), // станция метро
					'StreetName' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_ADDRESS'), // улица
					'House' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_HOUSE'), // дом
					'Corpus' => '', // корпус
					'Building' => '', // строение
					'Flat' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_APART'), // квартира
					'Porch' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_PORCH'), // подъезд
					'Floor' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_FLOOR'), // этаж
					'DoorCode' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_INTERCOM'), // код домофона
					'Room' => '', // комната
					'Office' => '', // офис
					'Remark' => '' // комментарий
				);

				// CityName
				$iCityOtherId = self::GetOtherCityId();
				$arDeliveryRegions = self::GetDeliveryRegions();
				$sTmpVal = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_REGION');
				if($sTmpVal == $iCityOtherId) {
					$arXmlAddress['CityName'] = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_CITY_DOP');
				} else {
					$arXmlAddress['CityName'] = $sTmpVal && $arDeliveryRegions[$sTmpVal] ? $arDeliveryRegions[$sTmpVal]['NAME'] : '';
				}

				// StreetName, House
				/*
				$sStreetName = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_ADDRESS');
				$arTmpVal = explode(',', $sStreetName);
				$sHouse = '';
				if(count($arTmpVal) == 2 && strlen(trim($arTmpVal[0]))) {
					$sStreetName = trim($arTmpVal[0]);
					$sHouse = trim($arTmpVal[1]);
				}
				$arXmlAddress['StreetName'] = $sStreetName;
				$arXmlAddress['House'] = $sHouse;
				*/

				// для итогового комментария к заказу
				$arTmp = array();
				if($arXmlAddress['CityName']) {
					$arTmp[] = $arXmlAddress['CityName'];
				}
				if($arXmlAddress['StationName']) {
					$arTmp[] = 'метро '.$arXmlAddress['StationName'];
				}
				if($arXmlAddress['StreetName']) {
					$arTmp[] = $arXmlAddress['StreetName'];
				}
				if($arXmlAddress['House']) {
					$arTmp[] = 'дом '.$arXmlAddress['House'];
				}
				if($arXmlAddress['Flat']) {
					$arTmp[] = 'кв. '.$arXmlAddress['Flat'];
				}
				if($arXmlAddress['Porch']) {
					$arTmp[] = 'подъезд '.$arXmlAddress['Porch'];
				}
				if($arXmlAddress['Floor']) {
					$arTmp[] = 'этаж '.$arXmlAddress['Floor'];
				}
				if($arXmlAddress['DoorCode']) {
					$arTmp[] = 'код домофона '.$arXmlAddress['DoorCode'];
				}
				$sOrderDesc .= implode(', ', $arTmp).$sNl;

			} else {
				$sXmlType = 'забрать из ресторана';
				// Department
				$sXmlDepartmentCode = self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_RESTAURANT');
				$sTmpRestaurantName = $sXmlDepartmentCode;
				if($sXmlDepartmentCode) {
					$arTmpRestaurants = self::GetDeliveryRestaurantsList();
					foreach($arTmpRestaurants as $arTmpItem) {
						if($arTmpItem['DEPARTMENT_XML_ID'] == $sXmlDepartmentCode) {
							$sTmpRestaurantName = $arTmpItem['NAME'].', г.'.$arTmpItem['CITY_NAME'];
							break;
						}
					}
				}
				$sOrderDesc .= 'Самовывоз из ресторана: '.$sTmpRestaurantName.$sNl;
			}

			// QtyPerson, Products, Bonuses
			$iXmlQtyPerson = $arOrderData['ORDER_CART']['PERSONS_CNT_ALT'];
			$arXmlProducts = array();
			$arXmlBonuses = array();
			$arTableTypes = array('current', 'guest', 'common');
			//$sOrderDesc .= 'Список товаров: '.$sNl;
			$iTmpIdx = 0;
			$arDiscounts = array();
			foreach($arTableTypes as $sTableType) {
				if($arOrderData['ORDER_CART'][$sTableType]) {
					foreach($arOrderData['ORDER_CART'][$sTableType] as $iGuestId => $arGuestTable) {
						if($arGuestTable['ITEMS']) {

							$sTableName = 'Мой стол';
							$sGuestName = 'Мой стол';
							switch($sTableType) {
								case 'common':
									$sTableName = 'Дополнительно на всех';
									$sGuestName = 'Дополнительно на всех';
								break;

								case 'guest':
									$sTableName = 'Стол гостя';
									$sGuestName = 'Гость';
									if($arGuestsList[$iGuestId]) {
										$sTableName = 'Стол '.$arGuestsList[$iGuestId]['NAME'];
										$sGuestName = $arGuestsList[$iGuestId]['NAME'];
									}
									$sTableName .= ' ['.$iGuestId.']';
								break;
							}
							$sOrderDescGuestTitle .= $sTableName.': '.$sNl;
							$iTmpIdx = 0;

							foreach($arGuestTable['ITEMS'] as $arItem) {
								$arTmp = self::GetProductDataByBasketItem($arItem, false, $arOrderData['ORDER']['USER_ID']);
								if($arTmp && $arTmp['_EXPORT_TO_FO_'] == 'Y' && $arTmp['TYPE'] != 'coupon') {
									if($arItem['PRICE'] <= 0) {
										$sTmpDiscType = '';
										$sTmpDiscTypeDesc = '';
										if($arTmp['TYPE'] == 'add_comp') {
											$sTmpDiscType = ' || доп.компонент';
											$sTmpDiscTypeDesc = 'доп.компонент';
										} elseif($arItem['PROPS']['BY_COUPON'] && $arItem['PROPS']['BY_COUPON'] == 'Y') {
											$sTmpDiscType = ' || по купону';
											$sTmpDiscTypeDesc = 'по купону';
										}
										$arXmlBonuses[] = array(
											'Code' => $arTmp['FO_XML_ID'],
											'Qty' => intval($arItem['QUANTITY']),
											'Remark' => $sTableName.' || '.$arTmp['NAME'].' || '.intval($arItem['QUANTITY']).' x 0.00 руб.'.$sTmpDiscType,
											'Guest' => $sGuestName,
										);
										// заголовок с именем гостя (один раз вставляется)
										$sOrderDesc .= $sOrderDescGuestTitle;
										$sOrderDescGuestTitle = '';

										$sOrderDesc .= ++$iTmpIdx.'. '.$arTmp['NAME'].' 0.00 x '.intval($arItem['QUANTITY']).'шт. = 0.00 ('.$sTmpDiscTypeDesc.')'.$sNl;
									} else {
										$sTmpDiscType = '';
										$dTmpDiscount = '';
										if($arItem['PROPS']['BY_COUPON'] && $arItem['PROPS']['BY_COUPON'] == 'Y' && $arItem['PROPS']['OLD_PRICE']) {
											$dTmpDiscount = $arItem['PROPS']['OLD_PRICE'] - $arItem['PRICE'];
											$sTmpDiscType = ' || по купону';
										}
										$arXmlProducts[] = array(
											'Code' => $arTmp['FO_XML_ID'],
											'Qty' => intval($arItem['QUANTITY']),
											'DiscountPercent' => '',
											'Discount' => $dTmpDiscount,
											'Remark' => $sTableName.' || '.$arTmp['NAME'].' || '.intval($arItem['QUANTITY']).' x '.$arItem['PRICE'].' руб.'.$sTmpDiscType,
											'Guest' => $sGuestName,
										);
										// заголовок с именем гостя (один раз вставляется)
										$sOrderDesc .= $sOrderDescGuestTitle;
										$sOrderDescGuestTitle = '';

										$sOrderDesc .= ++$iTmpIdx.'. '.$arTmp['NAME'].' '.$arItem['PRICE'].' x '.intval($arItem['QUANTITY']).'шт. = '.number_format(($arItem['PRICE'] * intval($arItem['QUANTITY'])), 2, '.', '').$sNl;
									}
								} elseif($arTmp['TYPE'] == 'coupon') {
									$arDiscounts[] = array(
										'BASKET_ITEM' => $arItem,
										'EXT_DATA' => $arTmp,
										'TABLE_NAME' => $sTableName,
										'GUEST_NAME' => $sGuestName,
									);
								}
							}
						}
					}
				}
			}

			// расшифровка скидок
			$arTmpDiscountDesc = array();
			if($arDiscounts) {
				foreach($arDiscounts as $arDiscountItem) {
					if($arDiscountItem['BASKET_ITEM']['PROPS']['type_bonus']) {
						switch($arDiscountItem['BASKET_ITEM']['PROPS']['type_bonus']) {
							case 'DISCOUNT_MONEY':
								if($arDiscountItem['BASKET_ITEM']['PROPS']['type_coupon'] == 'COUNT_RUB') {
									// бонусы-рубли
									$arTmpDiscountDesc['rub'][] = 'Скидка бонусы-рубли - '.$arDiscountItem['BASKET_ITEM']['PROPS']['val_coupon_used'].' руб., применена к столу "'.$arDiscountItem['TABLE_NAME'].'"';
								} elseif($arDiscountItem['BASKET_ITEM']['PROPS']['type_coupon'] == 'COUNT_NIYAM') {
									// бонусы-ниямы
									$iNiyamCoeff = isset($arDiscountItem['BASKET_ITEM']['PROPS']['niyam_coeff']) ? intval($arDiscountItem['BASKET_ITEM']['PROPS']['niyam_coeff']) : 0;
									$iNiyamCoeff = $iNiyamCoeff > 0 ? $iNiyamCoeff : intval(CNiyamaCustomSettings::GetStringValue('koefficient_niam_by_rub', 100));
									$iNiyam2Rub = $iNiyamCoeff > 0 ? ceil($arDiscountItem['BASKET_ITEM']['PROPS']['val_coupon_used'] / $iNiyamCoeff) : 0;
									$arTmpDiscountDesc['rub'][] = 'Скидка бонусы-ниямы - '.$iNiyam2Rub.' руб. ('.$arDiscountItem['BASKET_ITEM']['PROPS']['val_coupon_used'].' ниям), применена к столу "'.$arDiscountItem['TABLE_NAME'].'"';
								}
							break;

							case 'CONST_DISCOUNT':
								// скидка по дисконтной карте (берем ту скидку, которая была на момент заказа)
								$sTmpCarNum = isset($arDiscountItem['BASKET_ITEM']['PROPS']['discount_card_num']) ? $arDiscountItem['BASKET_ITEM']['PROPS']['discount_card_num'] : '';
								$sTmpCarNum = strlen($sTmpCarNum) ? $sTmpCarNum : '-';
								$arTmpDiscountDesc['perc']['discount_card'] = 'Скидка по карте N'.$sTmpCarNum.', '.$arDiscountItem['BASKET_ITEM']['PROPS']['val_coupon'].'%';
								if($arDiscountItem['BASKET_ITEM']['PROPS']['no_sum'] == 'Y') {
									$arTmpDiscountDesc['perc']['discount_card'] .= ' (не суммируется)';
								}
							break;

							case 'DISCOUNT_PERSENT':
								// разовые скидки в %
								$arTmpDiscountDesc['perc']['discount_percent'] = 'Разовая скидка '.$arDiscountItem['BASKET_ITEM']['PROPS']['val_coupon'].'%';
								if($arDiscountItem['BASKET_ITEM']['PROPS']['no_sum'] == 'Y') {
									$arTmpDiscountDesc['perc']['discount_percent'] .= ' (не суммируется)';
								}
							break;
						}
					}
				}
			}
			$sDiscountDesc = '';
			if($arOrderData['ORDER_CART']['DISCOUNT_RUB']['persent']) {
				if($arTmpDiscountDesc['perc']) {
					$sTmp = '';
					if(count($arTmpDiscountDesc['perc']) > 1 && $arOrderData['ORDER_CART']['DISCOUNT_PERCENT']) {
						$sTmp = '. Итого: '.$arOrderData['ORDER_CART']['DISCOUNT_PERCENT'].'%';
					}
					$sDiscountDesc .= implode('; ', $arTmpDiscountDesc['perc']).$sTmp.' - '.$arOrderData['ORDER_CART']['DISCOUNT_RUB']['persent'].' руб.'.$sNl;
				} else {
					$sDiscountDesc .= 'Скидка на заказ '.$arOrderData['ORDER_CART']['DISCOUNT_PERCENT'].'% - '.$arOrderData['ORDER_CART']['DISCOUNT_RUB']['persent'].' руб.'.$sNl;
				}
			}
			if($arTmpDiscountDesc['rub']) {
				$sDiscountDesc .= implode($sNl, $arTmpDiscountDesc['rub']).$sNl;
			}
			if($arOrderData['ORDER_CART']['DISCOUNT_RUB']['all']) {
				$sDiscountDesc .= 'Общий размер скидки: '.$arOrderData['ORDER_CART']['DISCOUNT_RUB']['all'].' руб.'.$sNl;
			}

			// ChangeAmount
			$sXmlRemarkMoney = trim(self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_CHANGE_AMOUNT'));

			// TimePlan
			$sTimePlanDateVal = trim(self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_BY_DATE'));
			$sTimePlanTimeVal = trim(self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'DELIVERY_BY_TIME'));
			$sDescTimePlan = trim($sTimePlanDateVal.' '.$sTimePlanTimeVal);
			$sXmlTimePlan = '';
			if(strlen($sTimePlanDateVal)) {
				// у нас передается дата в формате 20.11.2014 00:30 - 01:00 приводим к формату 20.11.2014 00:30:00
				$arTmp = explode('-', $sTimePlanTimeVal);
				if(count($arTmp) == 2 && strlen(trim($arTmp[0])) == 5 && strlen(trim($arTmp[1])) == 5) {
					$sXmlTimePlan = $sTimePlanDateVal.' '.trim($arTmp[0]).':00';
				} elseif(count($arTmp) == 1 && strlen(trim($arTmp[0])) == 8) {
					// время сразу передали в нужном формате
					$sXmlTimePlan = trim($sTimePlanDateVal.' '.$sTimePlanTimeVal);
				}
			}

			$sOrderDesc .= 'Итого: '.number_format(($arOrderData['ORDER']['PRICE'] + $arOrderData['ORDER']['DISCOUNT_VALUE']), 2, '.', '').$sNl; 
			$sOrderDesc .= 'Скидка: '.$arOrderData['ORDER']['DISCOUNT_VALUE'].$sNl; 
			$sOrderDesc .= 'К оплате: '.$arOrderData['ORDER']['PRICE'].$sNl;
			if($sXmlRemarkMoney) {
				$sOrderDesc .= 'Подготовить сдачу с суммы: '.$sXmlRemarkMoney.$sNl;
			}

			if($sDescTimePlan) {
				$sOrderDesc .= 'Заказ к определенной дате и времени: '.$sDescTimePlan.$sNl;
			}

			// PayMethod
			$arPayments = self::GetPayments();
			$arCurPayment = $arOrderData['ORDER']['PAY_SYSTEM_ID'] && $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']] ? $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']] : array();
			$sXmlPayMethod = $arCurPayment ? $arCurPayment['NAME'] : '';

			// Remark
			$sXmlRemark = '';
			if($sDiscountDesc) {
				$sXmlRemark .= strlen($sXmlRemark) ? $sNl : '';
				$sXmlRemark .= $sDiscountDesc;
			}

			if(strlen($sXmlTimePlan)) {
				$sXmlRemark .= strlen($sXmlRemark) ? $sNl : '';
				$sXmlRemark .= 'Заказ к определенной дате и времени: '.$sXmlTimePlan;
			}

			if($arCurPayment && $arCurPayment['IS_CASH'] != 'Y') {
				// в $arOrderData['ORDER']['PS_STATUS_MESSAGE'] при оплате через AcquiroPay должен передаваться символьный код выбранного способа оплаты
				$sTmpPayMethod = self::GetAcquiroPayMethodName($arOrderData['ORDER']['PS_STATUS_MESSAGE'], $arOrderData['ORDER']['PS_STATUS_MESSAGE']);
				$sTmpPayMethod = strlen($sTmpPayMethod) ? $sTmpPayMethod : '(нет данных о способе оплаты)';
				$sTmpPayedSum = strlen($arOrderData['ORDER']['PS_SUM']) ? $arOrderData['ORDER']['PS_SUM'].'р' : '(нет данных об оплаченной сумме)';
				$sTmpPayStatus = $arOrderData['ORDER']['PS_STATUS'] == 'Y' ? 'платеж успешно завершен' : 'платеж не получен';

				$sXmlRemark .= strlen($sXmlRemark) ? $sNl : '';
				$sXmlRemark .= 'Информация об оплате заказа: '.$sTmpPayMethod.' - '.$sTmpPayedSum.' ['.$sTmpPayStatus.']';
			}

			if($arOrderData['ORDER']['USER_DESCRIPTION']) {
				$sXmlRemark .= strlen($sXmlRemark) ? $sNl : '';
				$sXmlRemark .= 'Комментарий к заказу: '.$arOrderData['ORDER']['USER_DESCRIPTION'];
				$sOrderDesc .= 'Комментарий к заказу: '.$arOrderData['ORDER']['USER_DESCRIPTION'].$sNl; 
			}
			$arFields = array(
				'OrderSource' => $arOrderData['ORDER']['ID'], // код заказа
				'DiscountPercent' => '',
				'Discount' => $arOrderData['ORDER']['DISCOUNT_VALUE'] ? doubleval($arOrderData['ORDER']['DISCOUNT_VALUE']) : '', // скидка в рублях на заказ
				'PayMethod' => $sXmlPayMethod,
				'QtyPerson' => $iXmlQtyPerson,
				'Type' => $sXmlType,
				'RemarkMoney' => intval($sXmlRemarkMoney),
				// неправильно обрабатывается в FO
				//'TimePlan' => $sXmlTimePlan,
				'Department' => $sXmlDepartmentCode,
				'Customer' => $arXmlCustomer,
				'Address' => $arXmlAddress,
				'Phone' => $arXmlPhone,
				'Products' => $arXmlProducts,
				'Bonuses' => $arXmlBonuses,
				'Remark' => $sXmlRemark // комментарий
			);
			// проверим, чтобы юзер существовал в FO
			$obFO = new CNiyamaFastOperator();
			$bCustomerOk = true;
			$arCustomerRequest = $obFO->GetCustomerArray($arFields['Customer']['Login']);
			if(!$arCustomerRequest) {
				// добавим юзера
				$bCustomerOk = $obFO->EditCustomer(
					array(
						'Login' => $arFields['Customer']['Login'],
						'Pwd' => CProjectUtils::GenPassword(8),
						'FIO' => $arFields['Customer']['FIO'],
						'Phones' => array(
							array(
								'Code' => $arFields['Phone']['Code'],
								'Number' => $arFields['Phone']['Number'],
							)
						)
					)
				);
			}
			$bUpdateFlag = true;
			if($bCustomerOk) {
				$arRequestResult = $obFO->AddOrder($arFields, $sOrderDesc);

				if($arRequestResult['ID']) {
					$arUpdateFields = array(
						'UF_FO_WAS_SEND' => 1,
						'UF_FO_ORDER_ID' => $arRequestResult['ID'],
						'UF_FO_ORDER_CODE' => $arRequestResult['Code'],
						'UF_FO_USER_LOGIN' => $arFields['Customer']['Login']
					);
					if(!$arOrderData['ORDER']['UF_ACCEPT_DATE']) {
						// для того, чтобы можно было начислить бонусы за отложенный заказ
						$arUpdateFields['UF_ACCEPT_DATE'] = date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')));
					}

					// сначала меняем поля, а затем статус!
					$bReturn = CSaleOrder::Update($arOrderData['ORDER']['ID'], $arUpdateFields);
					CSaleOrder::StatusOrder($arOrderData['ORDER']['ID'], 'R');
					$bUpdateFlag = false;
				} else {
					CEventLog::Log('', 'NIYAMA_FO_SYNC', '', 'Ошибка передачи заказа в FO. ORDER_ID: '.$iOrderId, CProjectUtils::GetExceptionString());
				}
			} else {
				CEventLog::Log('', 'NIYAMA_FO_SYNC', '', 'Ошибка регистрации покупателя в FO. ORDER_ID: '.$iOrderId, CProjectUtils::GetExceptionString());
			}

			if($bUpdateFlag) {
				$arUpdateFields = array(
					'UF_FO_WAS_SEND' => 1,
					'UF_FO_USER_LOGIN' => $arFields['Customer']['Login']
				);
				if(!$arOrderData['ORDER']['UF_ACCEPT_DATE']) {
					// для того, чтобы можно было начислить бонусы за отложенный заказ
					$arUpdateFields['UF_ACCEPT_DATE'] = date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')));
				}
				CSaleOrder::Update($arOrderData['ORDER']['ID'], $arUpdateFields);
			}

			// логирование
			if(defined('_NIYAMA_LOG_FO_ADD_ORDER_') && _NIYAMA_LOG_FO_ADD_ORDER_) {
				if(class_exists('CCustomLog')) {
					$arLogInfo['arFields'] = $arResult;
					$obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/ExportOrder2FO/'.date('Y-m-d_H-i-s').'_'.$arOrderData['ORDER']['ID'].'.log');
					$obCustomLog->Log(
						array(
							'FO_CUSTOMER_REQUEST' => $arCustomerRequest,
							'CUSTOMER_OK' => $bCustomerOk,
							'FO_ADD_ORDER_FIELDS' => array(
								'arFields' => $arFields, 
								'sOrderDesc' => $sOrderDesc
							),
							'FO_ADD_ORDER_RESULT' => $arRequestResult,
							'SITE_ORDER_UPDATE_FIELDS' => $arUpdateFields ? $arUpdateFields : '-',
							'SITE_ORDER_DATA' => $arOrderData
						)
					);
				}
			}

		}
		return $bReturn;
	}

	//
	// Импорт заказов пользователя из FastOperator по логину (обновление или добавление)
	// @param string $sFoUserLogin - логин в FO (e-mail)
	//
	public static function ImportUserOrdersFO($sFoUserLogin) {
		$obFO = new CNiyamaFastOperator();
		$arUserOrders = $obFO->GetOrdersArray($sFoUserLogin);
		if($arUserOrders) {
			CModule::IncludeModule('sale');
			$arOrdersIdList = array();
			foreach($arUserOrders['Orders'] as $arFastOperatorOrder) {
				if($arFastOperatorOrder['ID']) {
					$arOrdersIdList[$arFastOperatorOrder['ID']] = $arFastOperatorOrder['ID'];
				}
			}

			$arFoOrders2SiteOrders = array();
			if($arOrdersIdList) {
				$dbItems = CSaleOrder::GetList(
					array(
						'ID' => 'ASC'
					),
					array(
						'UF_FO_ORDER_ID' => array_keys($arOrdersIdList)
					),
					false,
					false,
					array(
						'ID', 'UF_FO_ORDER_ID'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					if($arItem['UF_FO_ORDER_ID']) {
						$arFoOrders2SiteOrders[$arItem['UF_FO_ORDER_ID']] = $arItem['ID'];
					}
				}
			}

			$dbItems = CUser::GetList(
				$sBy = 'ID',
				$sOrder = 'ASC',
				array(
					'EMAIL' => $sFoUserLogin
				),
				array(
					'FIELDS' => array(
						'ID', 'LOGIN', 'EMAIL'
					)
				)
			);
			$arUser = $dbItems->Fetch();
			if($arUser['ID']) {
				$iFUserId = self::GetFUserIdByUserId($arUser['ID']);

				foreach($arUserOrders['Orders'] as $arFastOperatorOrder) {
					if($arFastOperatorOrder['ID']) {
						$iOrderId = 0;
						if($arFoOrders2SiteOrders[$arFastOperatorOrder['ID']]) {
							$iOrderId = $arFoOrders2SiteOrders[$arFastOperatorOrder['ID']];
						} else {
							if($iFUserId) {
								$arTmpStatusInfo = self::GetSiteOrderStatusByFoStatusCode($arFastOperatorOrder['StatusCode']);
								// Amount: сумма по строкам заказа, TotalClearAmount: итоговая сумма к оплате (т.е. из суммы по строкам вычитаются все скидки и бонусы и добавляется оплата доставки)
								$dPriceDelivery = doubleval($arFastOperatorOrder['DeliverySum']) > 0 ? doubleval($arFastOperatorOrder['DeliverySum']) : 0;
								$dOrderPrice = doubleval($arFastOperatorOrder['TotalClearAmount']) > 0 ? doubleval($arFastOperatorOrder['TotalClearAmount']) : 0;
								$dOrderBasePrice = doubleval($arFastOperatorOrder['Amount']) > 0 ? doubleval($arFastOperatorOrder['Amount']) : 0;
								$dOrderDiscountValue = $dOrderBasePrice - $dOrderPrice - $dPriceDelivery;
								$dOrderDiscountValue = $dOrderDiscountValue > 0 ? $dOrderDiscountValue : 0;

								$arFields = array(
									'LID' => 's1',
									'PERSON_TYPE_ID' => self::GetPrivateClientPersonTypeId(),
									'PAYED' => 'N',
									'CANCELED' => $arTmpStatusInfo['CANCELED'],
									'REASON_CANCELED' => $arTmpStatusInfo['CANCELED'] == 'Y' ? $arFastOperatorOrder['CancelName'] : '',
									'STATUS_ID' => $arTmpStatusInfo['STATUS_ID'] ? $arTmpStatusInfo['STATUS_ID'] : 'R', // [R] - Принят на стороне FO
									'USER_ID' => $arUser['ID'],
									'ADDITIONAL_INFO' => $iFUserId,
									'UF_RECEIVED_FROM_FO' => 1,
									'UF_FO_ORDER_ID' => $arFastOperatorOrder['ID'],
									'UF_FO_ORDER_CODE' => $arFastOperatorOrder['Code'],
									'UF_FO_USER_LOGIN' => $sFoUserLogin,
									'UF_FO_DATE_CREATE' => $arFastOperatorOrder['DateCreate'],

									'PRICE_DELIVERY' => $dPriceDelivery,
									'PRICE' => $dOrderPrice,
									'CURRENCY' => 'RUB',
									'DISCOUNT_VALUE' => $dOrderDiscountValue,
									'USER_DESCRIPTION' => isset($arFastOperatorOrder['Remark']) ? trim($arFastOperatorOrder['Remark']) : '',
								);

								$iOrderId = CSaleOrder::Add($arFields);
							}
						}
						if($iOrderId) {
							self::UpdateOrderByImportedData($iOrderId, $arFastOperatorOrder);
						}
					}
				}
			}
		}
	}

	public static function UpdateOrderByImportedData($iOrderId, $arFastOperatorOrder) {
		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && $arFastOperatorOrder) {
			$arOrderData = self::GetOrderById($iOrderId);
			$arUpdateOrderFields = array();
			$arUpdateOrderProps = array();

			// сверим значения полей заказа из FO и сайта
			// Amount: сумма по строкам заказа, TotalClearAmount: итоговая сумма к оплате (т.е. из суммы по строкам вычитаются все скидки и бонусы и добавляется оплата доставки)
			$dPriceDelivery = doubleval($arFastOperatorOrder['DeliverySum']) > 0 ? doubleval($arFastOperatorOrder['DeliverySum']) : 0;
			$dOrderPrice = doubleval($arFastOperatorOrder['TotalClearAmount']) > 0 ? doubleval($arFastOperatorOrder['TotalClearAmount']) : 0;
			$dOrderBasePrice = doubleval($arFastOperatorOrder['Amount']) > 0 ? doubleval($arFastOperatorOrder['Amount']) : 0;
			$dOrderDiscountValue = $dOrderBasePrice - $dOrderPrice - $dPriceDelivery;
			$dOrderDiscountValue = $dOrderDiscountValue > 0 ? $dOrderDiscountValue : 0;
			$arTmp = array(
				'LID' => 's1',
				'PERSON_TYPE_ID' => self::GetPrivateClientPersonTypeId(),
				'PRICE_DELIVERY' => $dPriceDelivery,
				'PRICE' => $dOrderPrice,
				'CURRENCY' => 'RUB',
				'DISCOUNT_VALUE' => $dOrderDiscountValue,
				'USER_DESCRIPTION' => isset($arFastOperatorOrder['Remark']) ? trim($arFastOperatorOrder['Remark']) : '',
				'UF_FO_DATE_CREATE' => $arFastOperatorOrder['DateCreate'],
			);
			foreach($arTmp as $sTmpFieldCode => $sTmpFieldVal) {
				if(isset($arOrderData['ORDER'][$sTmpFieldCode])) {
					if($arOrderData['ORDER'][$sTmpFieldCode] != $sTmpFieldVal) {
						$arUpdateOrderFields[$sTmpFieldCode] = $sTmpFieldVal;
					}
				} else {
					$arUpdateOrderFields[$sTmpFieldCode] = $sTmpFieldVal;
				}
			}

			// актуализация статуса
			$bCancel = false;
			$sCancelDescription = '';
			$arTmpStatusInfo = self::GetSiteOrderStatusByFoStatusCode($arFastOperatorOrder['StatusCode']);
			$sUpdateStatus = $arTmpStatusInfo['STATUS_ID'];
			if($arTmpStatusInfo['CANCELED'] == 'Y') {
				$sCancelDescription = $arFastOperatorOrder['CancelName'];
				$bCancel = true;
			} elseif(!strlen($arTmpStatusInfo['STATUS_ID'])) {
				// 1 - новый
				// для обновления даты
				$arUpdateOrderFields['UF_FO_WAS_SEND'] = 1;
			}

			if($sUpdateStatus && $sUpdateStatus == $arOrderData['ORDER']['STATUS_ID']) {
				$sUpdateStatus = '';
			}

			if($bCancel && $arOrderData['ORDER']['CANCELED'] == 'Y') {
				$bCancel = false;
				if($arOrderData['ORDER']['REASON_CANCELED'] != $sCancelDescription) {
					$arUpdateOrderFields['REASON_CANCELED'] = $sCancelDescription;
				}
			}

			// сверим значения свойств заказа из FO и сайта
			// $arFastOperatorOrder
			$sCustomerPhone = '';
			if(isset($arFastOperatorOrder['PhoneCode']) && strlen(trim($arFastOperatorOrder['PhoneCode']))) {
				$sCustomerPhone .= '+7 ('.trim($arFastOperatorOrder['PhoneCode']).')';
			}
			if(isset($arFastOperatorOrder['PhoneNum']) && strlen(trim($arFastOperatorOrder['PhoneNum']))) {
				$sCustomerPhone .= ' '.trim($arFastOperatorOrder['PhoneNum']);
			}
			$arTmp = array(
				//'CUSTOMER_NAME' => '',
				//'CUSTOMER_LAST_NAME' => '',
				'CUSTOMER_PHONE' => trim($sCustomerPhone),
				'DELIVERY_FO_ADDRESS' => isset($arFastOperatorOrder['Address']) ? trim($arFastOperatorOrder['Address']) : '',
				//'CUSTOMER_EMAIL' => '',
				//'DELIVERY_REGION' => '',
				//'DELIVERY_SUBWAY' => '',
				//'DELIVERY_ADDRESS' => '',
				//'DELIVERY_APART' => '',
				//'DELIVERY_PORCH' => '',
				//'DELIVERY_INTERCOM' => '',
				//'DELIVERY_FLOOR' => '',
				//'DELIVERY_BY_DATE' => '',
				//'DELIVERY_BY_TIME' => '',
				//'DELIVERY_TYPE' => '',
				//'DELIVERY_RESTAURANT' => '',
				//'DELIVERY_CHANGE_AMOUNT' => '',
				//'DELIVERY_WITHIN_MKAD' => '',
				//'DELIVERY_HOUSE' => '',
			);
			foreach($arTmp as $sTmpFieldCode => $sTmpFieldVal) {
				if(isset($arOrderData['ORDER_PROPS'][$sTmpFieldCode])) {
					foreach($arOrderData['ORDER_PROPS'][$sTmpFieldCode] as $mValKey => $arTmpVal) {
						if($arTmpVal['VALUE'] != $sTmpFieldVal) {
							$arUpdateOrderProps[$sTmpFieldCode][] = $sTmpFieldVal;
						}
					}
				} else {
					$arUpdateOrderProps[$sTmpFieldCode][] = $sTmpFieldVal;
				}
			}

			// выполним обновление полей, свойств и статуса заказа, если есть отличия
			if($arUpdateOrderProps) {
				self::SetOrderProps($iOrderId, $arOrderData['ORDER']['PERSON_TYPE_ID'], $arUpdateOrderProps, $arErrors = array());
				$arUpdateOrderFields['LID'] = 's1';
			}
			if($arUpdateOrderFields) {
				CSaleOrder::Update($iOrderId, $arUpdateOrderFields);
			}
			if(strlen($sUpdateStatus)) {
				CSaleOrder::StatusOrder($iOrderId, $sUpdateStatus);
			}
			if($bCancel) {
				CSaleOrder::CancelOrder($iOrderId, 'Y', $sCancelDescription);
			}

			// работаем с позициями корзины
			if(isset($arFastOperatorOrder['Products'])) {
				$arNewBasketItems = array();
				foreach($arFastOperatorOrder['Products'] as $arTmp) {
					// ADVWARRANT-170: попросили не добавлять в заказ блюда без кода
					//$arTmp['Code'] = strlen($arTmp['Code']) ? $arTmp['Code'] : '-';
					if(strlen($arTmp['Code'])) {
						$arNewBasketItems[] = $arTmp;
					}
				}

				if($arNewBasketItems && is_array($arOrderData['ORDER_CART'])) {
					$arTableTypes = array('current', 'guest', 'common');
					foreach($arTableTypes as $sTableType) {
						if($arOrderData['ORDER_CART'][$sTableType]) {
							foreach($arOrderData['ORDER_CART'][$sTableType] as $iGuestId => $arGuestTable) {
								if($arGuestTable['ITEMS']) {
									foreach($arGuestTable['ITEMS'] as $arItem) {
										$arTmp = self::GetProductDataByBasketItem($arItem, true, $arOrderData['ORDER']['USER_ID']);
										if($arTmp && $arTmp['_EXPORT_TO_FO_'] == 'Y') {
											if($arTmp['TYPE'] != 'coupon') {
												$bOk = false;
												foreach($arNewBasketItems as $mKey => $arFoBasketItem) {
													if($arFoBasketItem['Code'] == $arTmp['FO_XML_ID']) {
														if($arFoBasketItem['Qty'] == $arItem['QUANTITY']) {
															if($arFoBasketItem['Price'] == $arItem['PRICE']) {
																unset($arNewBasketItems[$mKey]);
																$bOk = true;
																break;
															}
														}
													}
												}
												if(!$bOk) {
													break 3;
												}
											}
										}
									}
								}
							}
						}
					}
				}

				if($arNewBasketItems || empty($arFastOperatorOrder['Products'])) {
					// есть различия в корзине, удаляем старые позиции и добавляем те, которые пришли из FO
					$dbBasketItems = CSaleBasket::GetList(
						array(),
						array(
							'ORDER_ID' => $iOrderId
						),
						false,
						false,
						array('ID')
					);
					while($arItem = $dbBasketItems->Fetch()) {
						CSaleBasket::Delete($arItem['ID']);
					}

					foreach($arFastOperatorOrder['Products'] as $arFoProduct) {
						// ADVWARRANT-170: попросили не добавлять в заказ блюда без кода
						//$arFoProduct['Code'] = strlen($arFoProduct['Code']) ? $arFoProduct['Code'] : '-'; // чтобы не было пустых корзин в заказах
						if(strlen($arFoProduct['Code'])) {
							self::AddProduct2Order($iOrderId, $arOrderData['ORDER']['USER_ID'], $arFoProduct, true);
						}
					}
				}
			}
		}
	}

	protected static function AddProduct2Order($iOrderId, $iUserId, $arFoProduct, $bAddNewIfNotExists = false) {
		$iBasketItemId = 0;
		$arProduct = array();
		if($arFoProduct['Code']) {
			$arProduct = CNiyamaCatalog::GetProductDetailByFoXmlId($arFoProduct['Code'], true);
			if(!$arProduct && $bAddNewIfNotExists) {
				$iTmpId = CNiyamaIBlockCatalogBase::AddBaseItem($arFoProduct['Code']);
				if($iTmpId) {
					$arProduct = CNiyamaCatalog::GetProductDetailByFoXmlId($arFoProduct['Code'], true, true);
				}
			}
		}
		if($arProduct) {
			$iFUserId = self::GetFUserIdByUserId($iUserId);
			$arFields = array(
				'PRICE' => $arFoProduct['Price'],
				'QUANTITY' => $arFoProduct['Qty'],
				'ORDER_ID' => $iOrderId,
				'FUSER_ID' => $iFUserId,

				'PRODUCT_ID' => $arProduct['INFO']['ID'],
				'NAME' => $arProduct['INFO']['FO_XML_ID'] == '-' && strlen($arFoProduct['Remark']) ? $arProduct['INFO']['NAME'].' ('.$arFoProduct['Remark'].')' : $arProduct['INFO']['NAME'],
				'DETAIL_PAGE_URL' => $arProduct['INFO']['DETAIL_PAGE_URL'],
				'WEIGHT' => $arProduct['INFO']['PROPERTY_WEIGHT_VALUE'],
				'CURRENCY' => 'RUB',
				'LID' => 's1',
				'CAN_BUY' => 'Y',
				'DELAY' => 'N',
				'CALLBACK_FUNC' => '', // !!!
				'ORDER_CALLBACK_FUNC' => '', // !!!
				'CANCEL_CALLBACK_FUNC' => '', // !!!
				'PAY_CALLBACK_FUNC' => '', // !!!
				'PRODUCT_PROVIDER_CLASS' => '', // !!!
				'PRODUCT_XML_ID' => '',
				'NOTES' => 'product',
			);
			$iBasketItemId = CSaleBasket::Add($arFields);
		}

		return $iBasketItemId;
	}

	public static function GetProductDataByBasketItem($arBasketItem, $bGetNotActive = false, $iUserId = 0) {
		$arReturn = array();
		$arTmp = array();
		switch($arBasketItem['PROPS']['type']) {
			case 'dop_component':
				$arTmp = CNiyamaCatalog::GetDopCompDetailByImpElementId($arBasketItem['PRODUCT_ID']);
				if($arTmp) {
					$arReturn = array(
						'TYPE' => 'add_comp',
						'ID' => $arTmp['ID'],
						'NAME' => $arTmp['NAME'],
						'IMG_ID' => $arTmp['PREVIEW_PICTURE'],
						'FO_XML_ID' => $arBasketItem['PRODUCT_ID'] == $arTmp['FREE_PRODUCT_ID'] ? $arTmp['FREE_PRODUCT_XML_ID'] : $arTmp['PAID_PRODUCT_XML_ID'],
						'PRICE' => $arBasketItem['PRODUCT_ID'] == $arTmp['FREE_PRODUCT_ID'] ? 0 : $arTmp['PAID_PRODUCT_PRICE'],
						'ACTIVE' => 'Y',
						'_MISC_DATA_' => $arTmp,
						'_EXPORT_TO_FO_' => 'Y'
					);
				}
			break;

			case 'cupon':
				// Внимание! Для корректной работы методу необходимо передать ID пользователя
				if($arBasketItem['PROPS']['type_coupon']) {
					$arTmp = CNiyamaCoupons::GetCouponById($arBasketItem['PROPS']['type_coupon'], $iUserId);
				} else {
					$arTmp = CNiyamaCoupons::GetCouponById($arBasketItem['PRODUCT_ID'], $iUserId);
				}
				$arReturn = array(
					'TYPE' => 'coupon',
					'ID' => '',
					'NAME' => '',
					'IMG_ID' => '',
					'FO_XML_ID' => '',
					'PRICE' => '',
					'ACTIVE' => 'Y',
					'_MISC_DATA_' => $arTmp ? $arTmp : array(),
					'_EXPORT_TO_FO_' => 'N'
				);
			break;

			case 'guest_count':
				$arReturn = array(
					'TYPE' => 'guest_count',
					'ID' => '',
					'NAME' => 'Количество гостей',
					'IMG_ID' => '',
					'FO_XML_ID' => '',
					'PRICE' => 0,
					'ACTIVE' => 'Y',
					'_EXPORT_TO_FO_' => 'N'
				);
			break;

			default:
				$arTmp = CNiyamaCatalog::GetProductDetail($arBasketItem['PRODUCT_ID'], $bGetNotActive);
				if($arTmp) {
					$arReturn = array(
						'TYPE' => 'dish',
						'ID' => $arTmp['INFO']['ID'],
						'NAME' => $arTmp['INFO']['NAME'],
						'IMG_ID' => $arTmp['INFO']['PREVIEW_PICTURE'],
						'FO_XML_ID' => $arTmp['INFO']['FO_XML_ID'],
						'PRICE' => doubleval($arTmp['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']),
						'ACTIVE' => $arTmp['INFO']['ACTIVE'],
						'_MISC_DATA_' => $arTmp,
						'_EXPORT_TO_FO_' => 'Y'
					);
				}
			break;
		}
		return $arReturn;
	}

	public static function RefreshCartAddComps($iOrderId) {
		$arCartAddComp = CNiyamaCart::GetCartDopCompList($iOrderId);
		$arImpElementId2BasketItemId = array();
		if($arCartAddComp['CUR_CART']) {
			$arTableTypes = array('current', 'guest', 'common');
			foreach($arTableTypes as $sTableType) {
				if($arCartAddComp['CUR_CART'][$sTableType]) {
					foreach($arCartAddComp['CUR_CART'][$sTableType] as $iGuestId => $arGuestTable) {
						if($arGuestTable['ITEMS']) {
							foreach($arGuestTable['ITEMS'] as $arItem) {
								if($arItem['PROPS']['type'] == 'dop_component') {
									$arImpElementId2BasketItemId[$arItem['PRODUCT_ID']][$arItem['ID']] = array(
										'ID' => $arItem['ID'],
										'PRICE' => $arItem['PRICE'],
										'QUANTITY' => $arItem['QUANTITY']
									);
								}
							}
						}
					}
				}
			}
		}
		$arChngQnt = array();
		$arDelete = array();
		$arFreeAdd = array();
		if($arCartAddComp['ADD_COMP']) {
			foreach($arCartAddComp['ADD_COMP'] as $arItem) {
				if($arItem['FREE']['PRODUCT_ID']) {
					if(!isset($arFreeAdd[$arItem['FREE']['PRODUCT_ID']])) {
						$arFreeAdd[$arItem['FREE']['PRODUCT_ID']] = 0;
					}
					$arFreeAdd[$arItem['FREE']['PRODUCT_ID']] += $arItem['FREE_COUNT'];
				}
			}
		}

		if($arImpElementId2BasketItemId) {
			foreach($arImpElementId2BasketItemId as $iImpElementId => $arBasketItems) {
				$iTmpQnt = 0;
				$bWasPaid = false;
				foreach($arCartAddComp['ADD_COMP'] as $arItem) {
					if($arItem['FREE']['PRODUCT_ID'] == $iImpElementId) {
						$iTmpQnt += $arItem['FREE_COUNT'];
					} elseif($arItem['PAID']['PRODUCT_ID'] == $iImpElementId) {
						$bWasPaid = true;
					}
				}

				if(!$arFreeAdd[$iImpElementId]) {
					// компонент не предлагается бесплатно
					if(!$bWasPaid) {
						// данный компонент есть в корзине как бесплатный, удалим
						foreach($arBasketItems as $arTmpItem) {
							$arDelete[$arTmpItem['ID']] = $arTmpItem['ID'];
						}
					}
				} else {
					// компонент предлагается бесплатно
					if($iTmpQnt) {
						// в корзине есть уже этот компонент в определенном количестве, сравним с расчетным 
						// (берем только первый попавшийся элемент корзины и приводим его к актуальному состоянию, остальные удаляем)
						$arFirstBasketItem = reset($arBasketItems);
						if($arFirstBasketItem['QUANTITY'] != $iTmpQnt) {
							$arChngQnt[$arFirstBasketItem['ID']] = $iTmpQnt;
						}
						unset($arFreeAdd[$iImpElementId]);
						unset($arBasketItems[$arFirstBasketItem['ID']]);
						if($arBasketItems) {
							foreach($arBasketItems as $arTmpItem) {
								$arDelete[$arTmpItem['ID']] = $arTmpItem['ID'];
							}
						}
					} else {
						// компонент в корзине есть как платный, удалим его, а бесплатный добавим
						foreach($arBasketItems as $arTmpItem) {
							$arDelete[$arTmpItem['ID']] = $arTmpItem['ID'];
						}
					}
				}
			}
		}

		if($arDelete) {
			foreach($arDelete as $iBasketItemId) {
				CNiyamaCart::RemoveFromCart($iBasketItemId);
			}
		}
		if($arChngQnt) {
			foreach($arChngQnt as $iBasketItemId => $iQuantity) {
				CNiyamaCart::SetQuantity($iBasketItemId, $iQuantity);
			}
		}
		if($arFreeAdd) {
			foreach($arFreeAdd as $iProductId => $iQuantity) {
				CNiyamaCart::AddToCart($iProductId, $iQuantity, false, 'dop_component');
			}
		}
	}

	public static function GetOrderListProps($bGroupByValue = true) {
		$arReturn = array();
		CModule::IncludeModule('sale');
		$dbItems = CSaleOrderProps::GetList(
			array(),
			array(
				'PERSON_TYPE_ID' => self::GetPrivateClientPersonTypeId(),
				'TYPE' => 'SELECT'
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arReturn[$arItem['CODE']] = array(
				'ID' => $arItem['ID'],
				'NAME' => $arItem['NAME'],
				'CODE' => $arItem['CODE'],
				'VALUES' => array()
			);
		}
		foreach($arReturn as $arItem) {
			$dbItems = CSaleOrderPropsVariant::GetList(
				array(
					'SORT' => 'ASC'
				),
				array(
					'ORDER_PROPS_ID' => $arItem['ID'],
					'TYPE' => 'SELECT'
				)
			);
			while($arVariantItem = $dbItems->Fetch()) {
				$sTmpKey = $bGroupByValue ? $arVariantItem['VALUE'] : $arVariantItem['ID'];
				$arReturn[$arItem['CODE']]['VALUES'][$sTmpKey] = $arVariantItem;
			}
		}
		return $arReturn;
	}

	public static function GetDeliveryRestaurantsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('restaurants-list', 'restaurants');
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'ACTIVE' => 'Y',
						'CHECK_PERMISSIONS' => 'N',
					),
					false, 
					false,
					array(
						'ID', 'NAME',
						'PROPERTY_R_CITY.ID', 'PROPERTY_R_CITY.NAME',
						'PROPERTY_R_RESTAURANT.ID', 'PROPERTY_R_RESTAURANT.XML_ID',
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'NAME' => $arItem['NAME'],
						'CITY_ID' => $arItem['PROPERTY_R_CITY_ID'],
						'CITY_NAME' => $arItem['PROPERTY_R_CITY_NAME'],
						'DEPARTMENT_ID' => $arItem['PROPERTY_R_RESTAURANT_ID'],
						'DEPARTMENT_XML_ID' => $arItem['PROPERTY_R_RESTAURANT_XML_ID'],
					);
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	//
	// Возвращает полную карточку заказа
	//
	public static function GetOrderById($iOrderId, $bCheckCurUserAccess = false, $bGetCartItems = true) {
		$arReturn = array();
		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			$dbItems = CSaleOrder::GetList(
				array(),
				array(
					'ID' => $iOrderId
				),
				false,
				false,
				array('*', 'UF_*')
			);
			if($arTmp = $dbItems->Fetch()) {
				$arReturn['ORDER'] = $arTmp;
				$arReturn['ORDER_PROPS'] = array();
				$dbPropItems = CSaleOrderPropsValue::GetOrderProps($iOrderId);
				while($arItem = $dbPropItems->Fetch()) {
					$arReturn['ORDER_PROPS'][$arItem['CODE']][$arItem['ID']] = $arItem;
				}
				if($bGetCartItems) {
					$arReturn['ORDER_CART'] = CNiyamaCart::GetCartListByOrderId($iOrderId);
				}

				if($bCheckCurUserAccess) {
					$arReturn['CAN_ACCESS'] = 'N';
					if($GLOBALS['USER']->IsAuthorized()) {
						if($arReturn['ORDER']['USER_ID'] == $GLOBALS['USER']->GetId()) {
							$arReturn['CAN_ACCESS'] = 'Y';
						}
					} else {
						$iAnonymousUserId = CSaleUser::GetAnonymousUserId();
						if($arReturn['ORDER']['USER_ID'] == $iAnonymousUserId) {
							$iFUserId = CSaleBasket::GetBasketUserId();
							if($arReturn['ORDER']['ADDITIONAL_INFO'] == $iFUserId) {
								$arReturn['CAN_ACCESS'] = 'Y';
							}
						}
					}
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает краткую карточку заказа
	//
	public static function GetOrderMiniById($iOrderId, $bCheckCurUserAccess = false) {
		return self::GetOrderById($iOrderId, $bCheckCurUserAccess, false);
	}

	public static function GetOrderDeliveryTypeById($iOrderId, $bCheckCurUserAccess = false) {
		$CUSTOMER_DELIVERY_TYPE = '';
		$ar_order = self::GetOrderById($iOrderId, $bCheckCurUserAccess, false);
		$type = self::_GetOrderPropValue($ar_order['ORDER_PROPS'], 'DELIVERY_TYPE');
		if(!empty($type)) {
			$CUSTOMER_DELIVERY_TYPE = $type;
		} elseif(isset($_SESSION['NIYAMA']['ORDER']['CUSTOMER_DELIVERY_TYPE'])) {
			$CUSTOMER_DELIVERY_TYPE = $_SESSION['NIYAMA']['ORDER']['CUSTOMER_DELIVERY_TYPE'];
		}
		return $CUSTOMER_DELIVERY_TYPE;
	}

	public static function GetStatusesList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('sale')) {
				$dbStatuses = CSaleStatus::GetList(
					array(
						'SORT' => 'ASC'
					),
					array(
						'LID' => 'ru'
					)
				);
				while($arStatuses = $dbStatuses->Fetch()) {
					$arReturn[$arStatuses['ID']] = $arStatuses;
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}

		return $arReturn;
	}

	public static function GetStatusById($sStatusId, $bRefreshCache = false) {
		$arStatuses = self::GetStatusesList($bRefreshCache);
		if($arStatuses && isset($arStatuses[$sStatusId])) {
			return $arStatuses[$sStatusId];
		}
		return false;
	}

	//
	// to do: $iCouponId
	//
	public static function GetUserOrders($iUserId, $iCouponId = 0, $arStatuses = array('N', 'F')) {
		$arReturn = array();
		$iUserId = intval($iUserId);
		$iCouponId = intval($iCouponId);
		if($iUserId <= 0) {
			return $arReturn;
		}

		if(CModule::IncludeModule('sale')) {
			// заказы
			$dbItems = CSaleOrder::GetList(
				array(),
				array(
					'USER_ID' => $iUserId,
					'@STATUS_ID' => $arStatuses,
					'CANCELED' => 'N'
				),
				false,
				false,
				array(
					//'*',
					'ID', 'PRICE', 'STATUS_ID', 'PAYED', 'DATE_PAYED',
					'PRICE_DELIVERY', 'PRICE', 'PAY_SYSTEM_ID', 'DISCOUNT_VALUE',
					'DATE_INSERT', 'DATE_UPDATE',
					'ADDITIONAL_INFO', 'UF_*'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$arItem['_BEFORE_DELIVERY_TIME_SEC_'] = 0;
				$arItem['_DELIVERY_TYPE_CODE_'] = '';
				$arItem['PROPS'] = array();
				$arReturn[$arItem['ID']]['ORDER_INFO'] = $arItem;
				$arReturn[$arItem['ID']]['ORDER_CART'] = array();
			}

			if($arReturn) {
				// свойства заказов
				$dbItems = CSaleOrderPropsValue::GetList(
					array(),
					array(
						'@ORDER_ID' => array_keys($arReturn),
					),
					false,
					false,
					array(
						'*'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ORDER_ID']]['ORDER_INFO']['PROPS'][$arItem['CODE']] = $arItem['VALUE'];
				}

				// определим кол-во часов перед доставкой
				foreach($arReturn as $iOrderId => $arOrderData) {
					if($arOrderData['ORDER_INFO']['PROPS']['DELIVERY_TYPE']) {
						$arReturn[$iOrderId]['ORDER_INFO']['_DELIVERY_TYPE_CODE_'] = $arOrderData['ORDER_INFO']['PROPS']['DELIVERY_TYPE'];
					}
					if($arOrderData['ORDER_INFO']['PROPS']['DELIVERY_BY_DATE'] && $arOrderData['ORDER_INFO']['UF_ACCEPT_DATE']) {
						$iTimestampDelivery = 0;
						$sDateTime = trim($arOrderData['ORDER_INFO']['PROPS']['DELIVERY_BY_DATE']);
						if(strlen($sDateTime)) {
							$obTmpDate = date_create($sDateTime);
							$iTimestampDelivery = $obTmpDate ? $obTmpDate->Format('U') : 0;
						}
						if($iTimestampDelivery && $arOrderData['ORDER_INFO']['PROPS']['DELIVERY_BY_TIME']) {
							$arTmp = self::ParseTimePeriodString($arOrderData['ORDER_INFO']['PROPS']['DELIVERY_BY_TIME']);
							$iTimestampDelivery += ($arTmp['LEFT_TIME'] * 60);
						}
						if($iTimestampDelivery) {
							$obTmpDate = date_create($arOrderData['ORDER_INFO']['UF_ACCEPT_DATE']);
							$iTimestampCreate = $obTmpDate ? $obTmpDate->Format('U') : 0;
							if($iTimestampCreate) {
								$arReturn[$iOrderId]['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] = $iTimestampDelivery - $iTimestampCreate;
								if($arReturn[$iOrderId]['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] <= 0) {
									$arReturn[$iOrderId]['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] = 0;
								}
							}
						}
					}
				}

				// позиции заказов
				$dbItems = CSaleBasket::GetList(
					array(),
					array(
						'@ORDER_ID' => array_keys($arReturn),
					),
					false,
					false,
					array(
						'ID', 'PRICE', 'PRODUCT_ID', 'QUANTITY', 'ORDER_ID',
						'DISCOUNT_PRICE', 'DETAIL_PAGE_URL'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arItem['PROPS'] = array();
					$arReturn[$arItem['ORDER_ID']]['ORDER_CART'][$arItem['ID']] = $arItem;
					$arTmpBasketItemsId[$arItem['ID']] = $arItem['ID'];
				}
				// свойства позиций заказов
				foreach($arReturn as $iOrderId => $arOrderData) {
					if($arOrderData['ORDER_CART']) {
						$dbItems = CSaleBasket::GetPropsList(
							array(),
							array(
								'@BASKET_ID' => array_keys($arOrderData['ORDER_CART']),
							),
							false,
							false,
							array('*')
						);
						while($arItem = $dbItems->Fetch()) {
							$arReturn[$iOrderId]['ORDER_CART'][$arItem['BASKET_ID']]['PROPS'][$arItem['CODE']] = $arItem['VALUE'];
						}
					}
				}
			}

		}
		return $arReturn;
	}

	public static function GetDeliveryTimeByOrderId($iOrderId) {
		$arReturn = array();
		$iOrderId = intval($iOrderId);
		if($iOrderId <= 0) {
			return $arReturn;
		}
		if(CModule::IncludeModule('sale')) {
			// заказы
			$dbItems = CSaleOrder::GetList(
				array(),
				array(
					'ID' => $iOrderId,
				),
				false,
				false,
				array(
					//'*',
					'ID', 'PRICE', 'STATUS_ID', 'PAYED', 'DATE_PAYED',
					'PRICE_DELIVERY', 'PRICE', 'PAY_SYSTEM_ID', 'DISCOUNT_VALUE',
					'DATE_INSERT', 'DATE_UPDATE',
					'ADDITIONAL_INFO', 'UF_*'
				)
			);
			if($arItem = $dbItems->Fetch()) {
				$arItem['_BEFORE_DELIVERY_TIME_SEC_'] = 0;
				$arItem['_DELIVERY_TYPE_CODE_'] = '';
				$arItem['PROPS'] = array();
				$arReturn['ORDER_INFO'] = $arItem;
			}

			if($arReturn) {
				// свойства заказов
				$dbItems = CSaleOrderPropsValue::GetList(
					array(),
					array(
						'@ORDER_ID' => $iOrderId,
					),
					false,
					false,
					array(
						'*'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn['ORDER_INFO']['PROPS'][$arItem['CODE']] = $arItem['VALUE'];
				}

				// определим кол-во часов перед доставкой
				if($arReturn['ORDER_INFO']['PROPS']['DELIVERY_TYPE']) {
					$arReturn['ORDER_INFO']['_DELIVERY_TYPE_CODE_'] = $arReturn['ORDER_INFO']['PROPS']['DELIVERY_TYPE'];
				}
				if($arReturn['ORDER_INFO']['PROPS']['DELIVERY_BY_DATE'] && $arReturn['ORDER_INFO']['UF_ACCEPT_DATE']) {
					$iTimestampDelivery = 0;
					$sDateTime = trim($arReturn['ORDER_INFO']['PROPS']['DELIVERY_BY_DATE']);
					if(strlen($sDateTime)) {
						$obTmpDate = date_create($sDateTime);
						$iTimestampDelivery = $obTmpDate ? $obTmpDate->Format('U') : 0;
					}
					if($iTimestampDelivery && $arReturn['ORDER_INFO']['PROPS']['DELIVERY_BY_TIME']) {
						$arTmp = self::ParseTimePeriodString($arReturn['ORDER_INFO']['PROPS']['DELIVERY_BY_TIME']);
						$iTimestampDelivery += ($arTmp['LEFT_TIME'] * 60);
					}
					if($iTimestampDelivery) {
						$obTmpDate = date_create($arReturn['ORDER_INFO']['UF_ACCEPT_DATE']);
						$iTimestampCreate = $obTmpDate ? $obTmpDate->Format('U') : 0;
						if($iTimestampCreate) {
							$arReturn['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] = $iTimestampDelivery - $iTimestampCreate;
							if($arReturn['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] <= 0) {
								$arReturn['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_'] = 0;
							}
						}
					}
				}
			}

		}
		return $arReturn;
	}

	/**
	 * CheckGuest
	 * @param int $iGuestId
	 * @param int $iUserId
	 * @return bool
	 */
	public static function CheckGuest($iGuestId, $iUserId = false) {
		$bIsCheck = false;
		$iUserId = ($iUserId) ? intval($iUserId) : $GLOBALS['USER']->GetID();
		if($iUserId <= 0) {
			return $bIsCheck;
		}
		CModule::IncludeModule('sale');
		$dbOrder = CSaleOrder::GetList(
			array(
				'ID' => 'DESC'
			),
			array(
				'USER_ID' => $iUserId,
				'@STATUS_ID' => array('D', 'E', 'F', 'N', 'P', 'R', 'S', 'T'), 
				'LID' => 's1'
			),
			false,
			false,
			array(
				'ID'
			)
		);
		while($arOrder = $dbOrder->Fetch()) {
			$arOrderDetail = CNiyamaOrders::GetOrderById($arOrder['ID']);
			if(isset($arOrderDetail['ORDER_CART']['guest'][$iGuestId])) {
				$bIsCheck = true;
				break;
			}
		}
		return $bIsCheck;
	}

	public static function SendNewOrderMessage($iOrderId) {
		$bReturn = false;
		$iOrderId = intval($iOrderId);
		if($iOrderId <= 0) {
			return $bReturn;
		}
		CModule::IncludeModule('sale');
		$arOrderData = self::GetOrderById($iOrderId);
		$sHtmlCart = '';
		$arCouponsEachGuest = array();
		if(!empty($arOrderData['ORDER_CART'])) {
			if(!empty($arOrderData['ORDER_CART']['common'][0]['ITEMS'])) {
				foreach($arOrderData['ORDER_CART']['common'][0]['ITEMS'] as $mKey => $arProd) {
					if($arProd['PROPS']['type'] == 'cupon') {
						if(CNiyamaCoupons::GetTypeForAll($arProd['PRODUCT_ID'])) {
							$arCouponsEachGuest[$mKey] = $arProd;
							unset($arOrderData['ORDER_CART']['common'][0]['ITEMS'][$mKey]);
						}
					}
				}
			}
			$arTables = array('current', 'guest', 'common');
			$iOrderFUserId = intval($arOrderData['ORDER']['ADDITIONAL_INFO']);
			$arGuestsList = self::GetOrderGuestsList($arOrderData['ORDER']['USER_ID'], $iOrderFUserId, false);
			foreach($arTables as $sTable) {
				if(!empty($arOrderData['ORDER_CART'][$sTable])) {
					foreach($arOrderData['ORDER_CART'][$sTable] as $iGuestId => $arCart) {
						$sTmpCurTableName = '';
						switch($sTable) {
							case 'guest':
								$sTmpCurTableName = isset($arGuestsList[$iGuestId]['NAME']) ? trim($arGuestsList[$iGuestId]['NAME']) : '';
								$sTmpCurTableName = strlen($sTmpCurTableName) ? $sTmpCurTableName : 'Стол гостя';
							break;

							case 'current':
								$sTmpCurTableName = 'Мой стол:';
							break;

							case 'common':
								$sTmpCurTableName = 'Общий стол:';
							break;
						}
						$arCouponsInTable = array();

						$sCurTableItmes = '';
						$iCurPos = 0;
						if(!empty($arCart['ITEMS'])) {
							foreach($arCart['ITEMS'] as $arItem) {
								switch($arItem['PROPS']['type']) {
									# Купон
									case 'cupon':
										$arCouponsInTable[] = $arItem;
									break;

									# Дополнительный компонент
									case 'dop_component':
										$arDopCompDetail = CNiyamaCatalog::GetDopCompDetailByImpElementId($arItem['PRODUCT_ID'], true);
										if(!empty($arDopCompDetail)) {
											$sCurTableItmes .= ++$iCurPos.'. '.$arDopCompDetail['NAME'].' '.number_format($arItem['PRICE'], 2).' &times; '.intval($arItem['QUANTITY']).' шт. = '.number_format($arItem['PRICE'] * $arItem['QUANTITY'], 2).'<br>';
										}
									break;

									# Блюдо
									default:
										$arProduct = CNiyamaCatalog::GetProductDetail($arItem['PRODUCT_ID'], true);
										if($arProduct['INFO']) {
											$sCurTableItmes .= ++$iCurPos.'. '.$arProduct['INFO']['NAME'].' '.number_format($arItem['PRICE'], 2).' &times; '.intval($arItem['QUANTITY']).' шт. = '.number_format($arItem['PRICE'] * $arItem['QUANTITY'], 2).'<br>';
										}
									break;
								}
							}
						}

						if(strlen($sCurTableItmes) && !empty($arCouponsEachGuest)) {
							$arCouponsInTable = array_merge($arCouponsInTable, $arCouponsEachGuest);
						}
						if(!empty($arCouponsInTable)) {
							foreach($arCouponsInTable as $arItem) {
								$sDiscName = '';
								switch($arItem['PRODUCT_ID']) {
									case 'DISCOUNT':
										$sDiscName = 'Постоянная скидка '. $arItem['PROPS']['val_coupon'].'%';
									break;

									case 'COUNT_NIYAM':
										$sDiscName = $arItem['PROPS']['val_coupon_used'].' НИЯМ';
									break;

									case 'COUNT_RUB':
										$sDiscName = $arItem['PROPS']['val_coupon_used'].' РУБЛЕЙ';
									break;

									default:
										$arCouponDetail = CNiyamaCoupons::GetCouponById($arItem['PRODUCT_ID']);
										$sDiscName = $arCouponDetail['NAME'];
									break;
								}
								if(strlen($sDiscName)) {
									$sCurTableItmes .= ++$iCurPos.'. Купон - '.$sDiscName.'<br>';
								}
							}
						}
						if(strlen($sCurTableItmes)) {
							$sHtmlCart .= $sTmpCurTableName.'<br>';
							$sHtmlCart .= $sCurTableItmes;
						}
						$sHtmlCart .= '<br>';
					}
				}
			}
			if(!empty($arOrderData['ORDER_CART']['PERSONS_DOP_CNT']) && intval($arOrderData['ORDER_CART']['PERSONS_DOP_CNT']) > 0) {
				$sHtmlCart .= 'Количество гостей: '.intval($arOrderData['ORDER_CART']['PERSONS_DOP_CNT']).'<br>';
			}
		}

		echo $sHtmlCart;
		if($arOrderData['ORDER']) {
			$arFields = array(
				'FO_ORDER_ID' => $arOrderData['ORDER']['UF_FO_ORDER_ID'],
				'UF_FO_ORDER_CODE' => $arOrderData['ORDER']['UF_FO_ORDER_CODE'],
				'UF_ACCEPT_DATE' => $arOrderData['ORDER']['UF_ACCEPT_DATE'],
				'UF_FO_USER_LOGIN' => $arOrderData['ORDER']['UF_FO_USER_LOGIN'],

				'CUSTOMER_NAME' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_NAME'),
				'CUSTOMER_LAST_NAME' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_LAST_NAME'),
				'CUSTOMER_EMAIL' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_EMAIL'),

				'ORDER_ID' => $iOrderId,
				'ORDER_USER_NAME' => $arOrderData['ORDER']['USER_NAME'],
				'ORDER_USER_LAST_NAME' => $arOrderData['ORDER']['USER_LAST_NAME'],
				'ORDER_USER_EMAIL' => $arOrderData['ORDER']['USER_EMAIL'],
				'ORDER_HTML' => $sHtmlCart,

				'MESSAGE_DATE' => date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat('SHORT', SITE_ID))),
				'TOTAL_PRICE' => $arOrderData['ORDER']['PRICE'] + $arOrder['PRICE_DELIVERY'],
				'PRICE' => $arOrderData['ORDER']['PRICE'],
				'PRICE_DELIVERY' => $arOrderData['ORDER']['PRICE_DELIVERY'],
				'BCC' => COption::GetOptionString('sale', 'order_email', 'order@'.SITE_SERVER_NAME),
				'SALE_EMAIL' => COption::GetOptionString('sale', 'order_email', 'order@'.SITE_SERVER_NAME),
			);
			$sEventName = 'NIYAMA_NEW_ORDER';
			$bSend = true;
			if($bSend) {
				$obEvent = new CEvent();
				$obEvent->SendImmediate($sEventName, 's1', $arFields);
				$bReturn = true;
			}
		}
		return $bReturn;
	}

	public static function SendCancelOrderMessage($iOrderId, $sCancelDescription = '') {
		$bReturn = false;
		$iOrderId = intval($iOrderId);
		if($iOrderId <= 0) {
			return $bReturn;
		}
		CModule::IncludeModule('sale');
		$arOrderData = self::GetOrderById($iOrderId);
		if($arOrderData['ORDER']) {
			$arFields = array(
				'FO_ORDER_ID' => $arOrderData['ORDER']['UF_FO_ORDER_ID'],
				'UF_FO_ORDER_CODE' => $arOrderData['ORDER']['UF_FO_ORDER_CODE'],
				'UF_ACCEPT_DATE' => $arOrderData['ORDER']['UF_ACCEPT_DATE'],
				'UF_FO_USER_LOGIN' => $arOrderData['ORDER']['UF_FO_USER_LOGIN'],

				'CUSTOMER_NAME' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_NAME'),
				'CUSTOMER_LAST_NAME' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_LAST_NAME'),
				'CUSTOMER_EMAIL' => self::_GetOrderPropValue($arOrderData['ORDER_PROPS'], 'CUSTOMER_EMAIL'),

				'ORDER_ID' => $iOrderId,
				'ORDER_USER_NAME' => $arOrderData['ORDER']['USER_NAME'],
				'ORDER_USER_LAST_NAME' => $arOrderData['ORDER']['USER_LAST_NAME'],
				'ORDER_USER_EMAIL' => $arOrderData['ORDER']['USER_EMAIL'],

				'MESSAGE_DATE' => date($GLOBALS['DB']->DateFormatToPHP(CLang::GetDateFormat('SHORT', SITE_ID))),
				'TOTAL_PRICE' => $arOrderData['ORDER']['PRICE'] + $arOrder['PRICE_DELIVERY'],
				'PRICE' => $arOrderData['ORDER']['PRICE'],
				'PRICE_DELIVERY' => $arOrderData['ORDER']['PRICE_DELIVERY'],
				'BCC' => COption::GetOptionString('sale', 'order_email', 'order@'.SITE_SERVER_NAME),
				'SALE_EMAIL' => COption::GetOptionString('sale', 'order_email', 'order@'.SITE_SERVER_NAME),

				'CANCEL_DESCRIPTION' => $sCancelDescription
			);
			$sEventName = 'NIYAMA_CANCEL_ORDER';
			$bSend = true;
			if($bSend) {
				$obEvent = new CEvent();
				$obEvent->SendImmediate($sEventName, 's1', $arFields);
				$bReturn = true;
			}
		}
		return $bReturn;
	}

	public static function GetRepeatProductsInOrderByUser($iUser = 0, $iCountOrder = 3) {
		$arProducts = array();
		if ($iUser <= 0) {
			$iUser= ($GLOBALS['USER']->IsAuthorized()) ? $GLOBALS['USER']->GetID() : 0;
		}
		if ($iUser <= 0) {
			return $arProducts;
		}
		CModule::IncludeModule('sale');
		$dbOrder = CSaleOrder::GetList(
			array(
				'ID' => 'DESC'
			),
			array(
				'USER_ID' => $iUser,
				'@STATUS_ID' => array('N', 'F'),
				'LID' => 's1'
			),
			false,
			array(
				'nTopCount' => $iCountOrder
			),
			array(
				'ID',
			)
		);
		$arOrders = array();
		while ($arOrder = $dbOrder->Fetch()) {
			$arOrders[] = $arOrder['ID'];
		}
		if (!empty($arOrders)) {
			$dbOrder = CSaleBasket::GetList(
				array(
					'ID' => 'DESC'
				),
				array(
					'ORDER_ID' => $arOrders,
				),
				array(
					'PRODUCT_ID',
					'MAX' => 'ID'
				),
				false,
				array(
					'ID',
					'PRODUCT_ID',
					'ORDER_ID',
				)
			);
			$arOrders = array();

			$arBasketItems = array();
			while ($arOrder = $dbOrder->Fetch()) {
				if ($arOrder['CNT'] > 1) {
					$arBasketItems[$arOrder['ID']] = $arOrder['PRODUCT_ID'];
				}
			}

			if(!empty($arBasketItems)) {
				$arPropsID = array();
				$dbProp = CSaleBasket::GetPropsList(
					array(
						'SORT' => 'ASC',
						'ID' => 'ASC'
					),
					array(
						'BASKET_ID' => array_keys($arBasketItems),
						'CODE' => 'type',
						'VALUE' => CNiyamaCart::$arCartItemType[0]
					)
				);
				while($arProp = $dbProp->Fetch()) {
					$arProducts[] = $arBasketItems[$arProp['BASKET_ID']];
				}
			}
		}
		return $arProducts;
	}

}

<?php

class CNiyamaCustomSettings {

	public static function GetAllItems($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __METHOD__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			CModule::IncludeModule('iblock');
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('options');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			$dbItems = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y'
				),
				false,
				false,
				array(
					'ID', 'CODE',
					'PROPERTY_STRING_VALUE',
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$arReturn[ToUpper($arItem['CODE'])] = array(
					'ID' => $arItem['ID'],
					'CODE' => $arItem['CODE'],
					'VALUE' => $arItem['PROPERTY_STRING_VALUE_VALUE']
				);
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

    /**
     * getStringValue
     * @param $key
     * @param $default_value
     * @param bool $bRefreshCache
     * @return array
     */
	public static function GetStringValue($sOptionCode, $sDefaultValue = '', $bRefreshCache = false) {
		$sReturn = $sDefaultValue;
		$sOptionCode = ToUpper(trim($sOptionCode));
		if(strlen($sOptionCode)) {
			$arAllItems = self::GetAllItems($bRefreshCache);
			if($arAllItems[$sOptionCode]) {
				$sReturn = $arAllItems[$sOptionCode]['VALUE'];
			}
		}
		return $sReturn;
	}

    /**
    setStringValue
     * @param $name
     * @param $key
     * @param $value
     * @return bool|int
     */
    public static function setStringValue($name,$key,$value)
    {
        CModule::IncludeModule('iblock');
        $iblockID = CProjectUtils::GetIBlockIdByCode("options");

        $el = new CIBlockElement;
        $PROP['STRING_VALUE'] = $value;
        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $GLOBALS['USER']->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => $iblockID,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $name,
            "ACTIVE"         => "Y",
            "CODE"           => $key
        );
        $rsItems = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>$iblockID, 'ACTIVE'=>'Y','CODE'=>$key), false, false, array());
        if(!$arItem = $rsItems->Fetch()) {
            if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
              return $PRODUCT_ID;
            } else {
               return false;
            }
        } else {
            $el->Update($arItem['ID'],$arLoadProductArray);
            return $arItem['ID'];
        }
        return false;
    }

} 
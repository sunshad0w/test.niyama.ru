<?php
use Bitrix\Main\Entity;

class CPizzaPiActionPayTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return $a= __FILE__;
    }
    public static function getTableName()
    {
        return 'CPizzaPiActionPayTable_table';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'ORDER_ID' => array(
                'data_type' => 'integer',
            ),
            'PRICE' => array(
                'data_type' => 'integer',
            ),
            'STATUS' => array(
                'data_type' => 'integer',
            ),
            'AIM' => array(
                'data_type' => 'integer',
            ),
            'CLICK' => array(
                'data_type' => 'string'
            ),
            'SOURCE' => array(
                'data_type' => 'string'
            ),
            'DATE' => array(
                'data_type' => 'datetime'
            ),
        );
    }
}
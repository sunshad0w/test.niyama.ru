<?

class CNiyamaCart {

	private static $CookieName = 'CURRENT_GUEST_ID';

	private static $OrderCookieName = 'CURRENT_ORDER_DATA';
	private static $OrderGuestSessionName = 'CURRENT_ORDER_GUEST_ID';

	protected static $arBasketItemsRecalcFlags = array();

	public static $sSessionName = 'CART_REMOVE_DATA';
	public static $sCurrentTabName = 'Мой стол ';

	# Массив значени типов элементов заказа/корзины
	public static $arCartItemType = array('product', 'cupon', 'dop_component', 'guest_count');

	# Вкладка На всех
	public static $sForAllName = 'На всех';
	public static $iForAllGuestID = 1;
	public static $sForAllAvatar = '/images/niamafall.png';
	# Вкладка дополнительно на всех
	public static $sOtherForAllName = 'Дополнительно на всех';
	public static $iOtherForAllGuestID = 2;
	public static $sOtherForAllAvatar  = '/images/niamafall.png';


	/**
	 * AddToCart
	 * @param $iProductID
	 * @param int $iQuantity
	 * @param bool $iGuestID
	 * @param string $sType
	 * @param bool $bGetNotActiveProducts
	 * @return bool|int
	 */
	public static function AddToCart($iProductID, $iQuantity = 1, $iGuestID = false, $sType = 'product', $bGetNotActiveProducts = false, $iUserId = 0) {
		$iReturn = 0;
		$sCouponID = $iProductID;
		$iProductID = intval($iProductID);

		if($sType == 'cupon') {
			$iProductID = $iProductID > 0 ? $iProductID : 1;
		}
		if($sType == 'guest_count') {
			$iProductID = $iProductID > 0 ? $iProductID : 1;
		}

		$iGuestID = intval($iGuestID);
		$iGuestID = $iGuestID > 0 && $sType != 'dop_component' ? $iGuestID : 0;
		$iQuantity = $iQuantity > 0 ? $iQuantity : 1;
		if($iProductID > 0 && CModule::IncludeModule('sale')) {
			// т.к. корзина сразу привязывается к заказу, то обновляем количество подобным способом
			$arTableTypes = array('current');
			if($sType == 'dop_component') {
				$arTableTypes = array('common');
			} elseif($sType == 'guest_count') {
				$arTableTypes = array('common');
			} elseif($iGuestID > 0) {
				if($iGuestID == self::$iForAllGuestID) {
					$arTableTypes = array('common');
				} else {
					$arTableTypes = array('current', 'guest');
				}
			}
			$arCart = self::GetCartList();
			// гость с id = 1 в корзине имеет id = 0
			$iCheckPersonId = $iGuestID == self::$iForAllGuestID ? 0 : $iGuestID;

			foreach($arTableTypes as $sTableType) {
				if($arCart[$sTableType]) {
					foreach($arCart[$sTableType] as $iPersonId => $arPersonTable) {
						if($iPersonId == $iCheckPersonId && $arPersonTable['ITEMS']) {
							foreach($arPersonTable['ITEMS'] as $mKey => $arItem) {
								if($sType == 'cupon') {
									if ($iProductID == 1) {
										if ($arItem['PROPS']['type_coupon'] == $sCouponID) {
											if ($arItem['PROPS']['type_bonus'] == 'DISCOUNT_MONEY') {
												CNiyamaCart::SetValCouponUsed($arItem['ID'], $iQuantity);
											}
											$iReturn = $arItem['ID'];
											break;
										}
									} else {
										if($arItem['PRODUCT_ID'] == $iProductID) {
											self::SetQuantity($arItem['ID'], 1);
											$iReturn = $arItem['ID'];
											break;
										}	
									}
								} elseif($sType == 'guest_count') {
									if($arItem['PRODUCT_ID'] == $iProductID) {
										self::SetQuantity($arItem['ID'], $iQuantity);
										$iReturn = $arItem['ID'];
										break;
									}
								} else {
									if($arItem['PRODUCT_ID'] == $iProductID) {
										self::SetQuantity($arItem['ID'], ($arItem['QUANTITY'] + $iQuantity));
										$iReturn = $arItem['ID'];
										break;
									}
								}
								/*
								if($arItem['PRODUCT_ID'] == $iProductID) {
									if($sType == 'cupon') {
										// купон только один может быть
										self::SetQuantity($arItem['ID'], 1);
									} else {
										self::SetQuantity($arItem['ID'], ($arItem['QUANTITY'] + $iQuantity));
									}
									$iReturn = $arItem['ID'];
									break;
								}
								*/
							}
						}
						if($iReturn) {
							break;
						}
					}
				}
				if($iReturn) {
					break;
				}
			}

			if($iReturn) {
				return $iReturn;
			}

			$iUserId = intval($iUserId);
			if($iUserId <= 0) {
				$iUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetId() : 0;
			}
			$iFUserId = $iUserId > 0 ? CNiyamaOrders::GetFUserIdByUserId($iUserId) : CSaleBasket::GetBasketUserId();

			//
			// добавляем новый товар в корзину
			//
			$arCartProperty = array();

			# Устанавливаем св-во - Гость
			if($iGuestID) {
				$arCartProperty[] = array(
					'NAME' => 'Гость',
					'CODE' => 'guest',
					'VALUE' => $iGuestID,
				);
			}

			$arFieldsTmp = array();

			// для позиций корзины, добавляемых купоном на блюда
			$arFieldsByCoupon = array();

			# Устанавливаем тип
			$sTypeValue = '';
			switch ($sType) {

				# Купон
				case 'cupon':
					$sTypeValue = 'cupon';

					$arFieldsTmp = CNiyamaCoupons::GetCouponById($sCouponID);
					$prod_id = intval($sCouponID) > 0 ? intval($sCouponID) : 1;
					$arFieldsTmp['PRODUCT_ID'] = $prod_id;
					$arFieldsTmp['PRICE'] = 0;

					// для бонуса по блюдам запомним здесь набор свойств, чтобы не попали свойства описания бонуса
					$arCartPropertyByCoupon_ = $arCartProperty;

					// сохраним номер карты
					if($arFieldsTmp['BONUS_TYPE'] == 'CONST_DISCOUNT') {
						$sTmpCarNum = $iUserId ? CNiyamaIBlockCardData::GetCardNumber($iUserId, true) : '';
						$arCartProperty[] = array(
							'NAME' => 'Номер дисконтной карты',
							'CODE' => 'discount_card_num',
							'VALUE' => $sTmpCarNum,
						);
					}

					if($arFieldsTmp['BONUS_TYPE'] == 'DISCOUNT_MONEY' && $arFieldsTmp['MONEY_TYPE'] == 'NIYAM') {
						$sTmpCarNum = $iUserId ? CNiyamaIBlockCardData::GetCardNumber($iUserId, true) : '';
						$arCartProperty[] = array(
							'NAME' => 'Коэффициент конвертации ниямы в рубли',
							'CODE' => 'niyam_coeff',
							'VALUE' => intval(CNiyamaCustomSettings::GetStringValue('koefficient_niam_by_rub', 100)),
						);
					}

					$arCartProperty[] = array(
						'NAME' => 'Тип бонуса',
						'CODE' => 'type_bonus',
						'VALUE' => $arFieldsTmp['BONUS_TYPE'],
					);
					$arCartProperty[] = array(
						'NAME' => 'Тип купона',
						'CODE' => 'type_coupon',
						'VALUE' => $iProductID != 1 ? $iProductID : $sCouponID,
					);
					$arCartProperty[] = array(
						'NAME' => 'Значение бонуса',
						'CODE' => 'val_coupon',
						'VALUE' => $arFieldsTmp['BONUS_VAL'],
					);
					$arCartProperty[] = array(
						'NAME' => 'Использовано бонусов',
						'CODE' => 'val_coupon_used',
						'VALUE' => $iQuantity,
					);
					$arCartProperty[] = array(
						'NAME' => 'Не суммировать с другими скидками',
						'CODE' => 'no_sum',
						'VALUE' => $arFieldsTmp['BONUS_NO_SUM'],
					);

					if($arFieldsTmp['BONUS_TYPE'] == 'BLUDO' && empty($arFieldsTmp['BLUDS'])) {
						$arCartProperty[] = array(
							'NAME' => 'ID Купона',
							'CODE' => 'ID_COUPON_BLUDO',
							'VALUE' => $prod_id,
						);

						foreach($arFieldsTmp['BLUDS'] as $blud) {
							$arCartPropertyByCoupon = $arCartPropertyByCoupon_;
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'Тип',
								'CODE' => 'type',
								'VALUE' => 'product',
							);

							$arProduct = CNiyamaCatalog::GetProductDetail($blud['ID']);
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'Старая цена',
								'CODE' => 'OLD_PRICE',
								'VALUE' => $blud['OLD_PRICE'],
							);
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'Скидка в %',
								'CODE' => 'DISCOUNT',
								'VALUE' => $blud['DISCOUNT'],
							);
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'По купону',
								'CODE' => 'BY_COUPON',
								'VALUE' => 'Y',
							);
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'Отображать в корзине',
								'CODE' => 'SHOW_IN_CART',
								'VALUE' => ($arFieldsTmp['VISUAL'] == 'COMPLECT') ? 'N' : 'Y',
							);
							$arCartPropertyByCoupon[] = array(
								'NAME' => 'ID Купона',
								'CODE' => 'ID_COUPON_BLUDO',
								'VALUE' => $prod_id,
							);

							$arFieldsByCoupon[] = array(
								'PRODUCT_ID' => $arProduct['INFO']['ID'],
								'PRICE' => $blud['NEW_PRICE'],
								'NAME' => $blud['NAME'],
								'DETAIL_PAGE_URL' => $arProduct['INFO']['DETAIL_PAGE_URL'],
								'WEIGHT' => $arProduct['INFO']['PROPERTY_WEIGHT_VALUE'], // Вес Блюда
								'PROP' => $arCartPropertyByCoupon,
							);
						}
					}
				break;

				# Дополнительный компонент
				case 'dop_component':
					$sTypeValue = 'dop_component';
					$arDopCompDetail = CNiyamaCatalog::GetDopCompDetailByImpElementId($iProductID);
					$arFieldsTmp = array(
						'PRODUCT_ID' => $iProductID,
						'PRICE' => $arDopCompDetail['FREE_PRODUCT_ID'] == $iProductID ? 0 : $arDopCompDetail['PAID_PRODUCT_PRICE'],
						'NAME' => $arDopCompDetail['NAME'],
						'DETAIL_PAGE_URL' => '',
						'WEIGHT' => '',
					);
				break;

				case 'guest_count':
					$sTypeValue = 'guest_count';
					$arFieldsTmp = array(
						'PRODUCT_ID' => $iProductID,
						'PRICE' => 0,
						'NAME' => 'Количество гостей',
						'DETAIL_PAGE_URL' => '',
						'WEIGHT' => '',
					);
				break;

				# Продукт
				default:
					# Устанавливаем тип элемента (Блюдо)
					$sTypeValue = 'product';
					$arProduct = CNiyamaCatalog::GetProductDetail($iProductID, $bGetNotActiveProducts);
					$arFieldsTmp = array(
						'PRODUCT_ID' => $arProduct['INFO']['ID'],
						'PRICE' => $arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
						'NAME' => $arProduct['INFO']['NAME'],
						'DETAIL_PAGE_URL' => $arProduct['INFO']['DETAIL_PAGE_URL'],
						'WEIGHT' => $arProduct['INFO']['PROPERTY_WEIGHT_VALUE'], //Вес Блюда
					);
				break;
			}

			$arCartProperty[] = array(
				'NAME' => 'Тип',
				'CODE' => 'type',
				'VALUE' => $sTypeValue,
			);

			$arDraftOrder = CNiyamaOrders::GetDraftOrderExact($iFUserId, $iUserId);

			$iDraftOrderId = isset($arDraftOrder['ID']) ? $arDraftOrder['ID'] : 0;
			if($iDraftOrderId) {
				# Базовая информация
				$arFieldsBase = array(
					'CURRENCY' => 'RUB',
					'PRODUCT_XML_ID' => '',
					'QUANTITY' => $iQuantity,
					'LID' => 's1',
					'DELAY' => 'N',
					'CAN_BUY' => 'Y',
					'DIMENSIONS' => '',
					'NOTES' => $sType,
					'PROPS' => $arCartProperty,
					'ORDER_ID' => $iDraftOrderId, // связь с заказом-черновиком (заказ-черновик нужен, чтобы юзер мог оформить заказ по телефону, продиктовав номер заказа)
				);
				// если добавляется бонус по блюдам
				if (!empty($arFieldsByCoupon)) {
					foreach ($arFieldsByCoupon as $arBonusDishItem) {
						$arFields = array_merge($arFieldsBase, $arBonusDishItem);
						$arFields['PROPS'] = $arBonusDishItem['PROPS'];
						$arFields['FUSER_ID'] = $iFUserId;
						CSaleBasket::Add($arFields);
					}
				}
				$arFields = array_merge($arFieldsBase, $arFieldsTmp);
				$arFields['FUSER_ID'] = $iFUserId;
				$iReturn = CSaleBasket::Add($arFields);
			}
		}
		return $iReturn;
	}

	protected static function GetBasketItems($iOrderId, $arSelectFields = array()) {
		$arReturn = array();
		$arReturn['ITEMS'] = array();
		$arReturn['TOTAL_PRICE'] = 0;
		if(!$arSelectFields) {
			$arSelectFields = array('ID', 'PRODUCT_ID', 'QUANTITY', 'PRICE', 'WEIGHT', 'DISCOUNT_PRICE', 'USER_ID', 'FUSER_ID');
		} else {
			$arSelectFields[] = 'ID';
			$arSelectFields[] = 'PRODUCT_ID';
			$arSelectFields = array_unique($arSelectFields);
		}

		$dbBasketItems = CSaleBasket::GetList(
			array(
				'ID' => 'ASC',
				'NAME' => 'ASC'
			),
			array(
				'ORDER_ID' => $iOrderId,
				'LID' => 's1',
			),
			false,
			false,
			$arSelectFields
		);
		while($arItem = $dbBasketItems->Fetch()) {
			$arReturn['ITEMS'][$arItem['ID']] = $arItem;
		}

		if($arReturn['ITEMS']) {
			$arPropsId = array();
			$dbProp = CSaleBasket::GetPropsList(
				array(
					'SORT' => 'ASC',
					'ID' => 'ASC'
				),
				array(
					'BASKET_ID' => array_keys($arReturn['ITEMS']), 
					'!CODE' => array('CATALOG.XML_ID', 'PRODUCT.XML_ID')
				)
			);
			while($arProp = $dbProp->GetNext()) {
				$arPropsId[$arProp['BASKET_ID']][$arProp['CODE']] = $arProp['VALUE'];
			}

			foreach($arReturn['ITEMS'] as $iBasketItemId => $arBasketItem) {
				$arReturn['TOTAL_PRICE'] += $arBasketItem['PRICE'] * $arBasketItem['QUANTITY'];
				$arReturn['ITEMS'][$iBasketItemId]['PROPS'] = $arPropsId[$iBasketItemId];
			}
		}
		return $arReturn;
	}

	/**
	 * Актуализация позиций корзины заданного заказа (только для заказов в статусе черновика).
	 * Метод использует внутреннее кэширование на время хита.
	 *
	 * @param int $iOrderId ID заказа
	 * @param bool $bForceRecalc обновить внутренний кэш
	 * @return void
	 */
	public static function RecalcBasketItems($iOrderId, $bForceRecalc = false) {
		$iOrderId = intval($iOrderId);
		if($iOrderId <= 0) {
			return;
		}
		if($bForceRecalc || !self::$arBasketItemsRecalcFlags[$iOrderId]) {
			self::$arBasketItemsRecalcFlags[$iOrderId] = true;

			CModule::IncludeModule('sale');
			$arOrder = CSaleOrder::GetById($iOrderId);
			if($arOrder && in_array($arOrder['STATUS_ID'], array('D', 'P'))) {
				$bWasConstDiscount = false;
				$bWasDishItems = false;
				$arGetBasketItemsResult = self::GetBasketItems($iOrderId);
				foreach($arGetBasketItemsResult['ITEMS'] as $arItem) {
					$arTmp = CNiyamaOrders::GetProductDataByBasketItem($arItem, true, $arOrder['USER_ID']);
					if(!$arTmp) {
						// какая-то неведомая фигня в корзине, удаляем ее
						CSaleBasket::Delete($arItem['ID']);
					} else {
						if($arTmp['TYPE']) {
							switch($arTmp['TYPE']) {
								case 'add_comp':
								case 'dish':
									$bWasDishItems = true;

									// обновляем цены
									$arUpdateFields = array();
									if($arItem['PRICE'] != $arTmp['PRICE']) {
										$arUpdateFields['PRICE'] = $arTmp['PRICE'];
									}
									if(isset($arTmp['_MISC_DATA_']['INFO']['PROPERTY_WEIGHT_VALUE']) && $arItem['WEIGHT'] != $arTmp['_MISC_DATA_']['INFO']['PROPERTY_WEIGHT_VALUE']) {
										$arUpdateFields['WEIGHT'] = $arTmp['_MISC_DATA_']['INFO']['PROPERTY_WEIGHT_VALUE'];
									}
									if($arUpdateFields) {
										CSaleBasket::Update($arItem['ID'], $arUpdateFields);
									}
								break;
								case 'coupon':
									if($arTmp['_MISC_DATA_'] && $arTmp['_MISC_DATA_']['BONUS_TYPE'] == 'CONST_DISCOUNT') {
										$bWasConstDiscount = true;
									}
								break;
							}

						}
					}
				}

				// добавляем купон постоянной скидки по дисконтной карте, если таковая есть и юзер не аноним
				if($bWasDishItems && !$bWasConstDiscount && $arOrder['USER_ID']) {
					if($arOrder['USER_ID'] != CSaleUser::GetAnonymousUserId()) {
						$arTmpDisc = CNiyamaCoupons::GetCouponByType(CNiyamaCoupons::$CONST_DISCOUNT, $arOrder['USER_ID']);
						if($arTmpDisc && $arTmpDisc['DISCOUNT_PER'] > 0) {
							CNiyamaCart::AddToCart($arTmpDisc['ID'], 1, self::$iForAllGuestID, 'cupon');
						}
					}
				}
			}
		}
	}

	/**
	 * GetCartListByOrderId
	 * @param $iOrderId
	 * @return array
	 */
	public static function GetCartListByOrderId($iOrderId) {
		$arReturn = array();

		$iOrderId = intval($iOrderId);

		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			// обновим корзину (удалим устаревшие записи и актуализируем цены)
			self::RecalcBasketItems($iOrderId, false);

			$arGetBasketItemsResult = self::GetBasketItems($iOrderId);
			$arBasketItems = $arGetBasketItemsResult['ITEMS'];
			$arReturn['TOTAL_PRICE'] = $arGetBasketItemsResult['TOTAL_PRICE'];
			if($arBasketItems) {
				$arReturn['PERSONS_CNT'] = 0;

				# Обработка окончательного массива
				$arReturn['current'] = array();
				$arReturn['guest'] = array();
				$arReturn['common'] = array();
				$arReturn['PRODUCT_IDS'] = array();
				$arReturn['GUEST_CNT'] = array();
				$arPersons = array();
				$arGuests = array();
				$arCurret = array();
				foreach($arBasketItems as $key => $arItem) {
					$iPersonId = 0;

					$sTableType = 'current';
					if($arItem['PROPS']['type'] == 'cupon') {
						$arItem['PRODUCT_ID'] = $arItem['PROPS']['type_coupon'];
					}
					if($arItem['PROPS']['type'] == 'guest_count') {
						$arReturn['GUEST_CNT'] = $arItem;
						continue;
					}
					if(self::isCommonTab($arItem)) {
						$sTableType = 'common';
					} elseif($arItem['PROPS']['type'] == 'cupon' && CNiyamaCoupons::GetTypeForAll($arItem['PRODUCT_ID'], $arItem['USER_ID'])) {
						$sTableType = 'common';
					} elseif($arItem['PROPS']['guest']) {
						$iPersonId = $arItem['PROPS']['guest'];
						$sTableType = 'guest';
					}
					if($sTableType == 'guest' || $sTableType == 'current') {
						$arPersons[$iPersonId] = $iPersonId;
					}
					if($sTableType == 'guest') {
						$arGuests[$iPersonId] = $iPersonId;
					} elseif($sTableType == 'current') {
						$arCurret[$iPersonId] = $iPersonId;
					}
					$arReturn[$sTableType][$iPersonId]['PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
					$arReturn[$sTableType][$iPersonId]['ITEMS'][$key] = $arItem;

					$arReturn['PRODUCT_IDS'][$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
				}

				$arReturn['PERSONS_CNT'] = count($arPersons);
				$arReturn['GUESTS_CNT'] = count($arGuests);
				$arReturn['CURRENT_CNT'] = count($arCurret);
				if(!empty($arReturn['GUEST_CNT']['QUANTITY']) && $arReturn['GUEST_CNT']['QUANTITY'] > 0) {
					$arReturn['PERSONS_CNT'] = $arReturn['GUEST_CNT']['QUANTITY'];
					$arReturn['PERSONS_DOP_CNT'] = (int)$arReturn['GUEST_CNT']['QUANTITY'];
				}
				$arReturn['PERSONS_CNT'] = $arReturn['PERSONS_CNT'] > 0 ? $arReturn['PERSONS_CNT'] : 1;
				// Количество персон определяется или числом добавленных к заказу персон, или расчетным количеством персон, если пользователь не добавлял персон к заказу
				$arReturn['PERSONS_CNT_ALT'] = $arReturn['PERSONS_CNT'] > 1 ? $arReturn['PERSONS_CNT'] : self::GetPersonsCountByOrderPrice($arReturn['TOTAL_PRICE']);
			}

			CNiyamaCoupons::RecalculateCartByCoupons($arReturn, $iOrderId);
		}

		return $arReturn;
	}

	public static function GetCartListByOrderIdSimple($iOrderId) {
		$arReturn = array();

		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			// обновим корзину (удалим устаревшие записи и актуализируем цены)
			self::RecalcBasketItems($iOrderId, false);

			$arGetBasketItemsResult = self::GetBasketItems($iOrderId, array('ID', 'PRODUCT_ID', 'QUANTITY', 'PRICE', 'WEIGHT', 'DISCOUNT_PRICE'));
			$arReturn = $arGetBasketItemsResult['ITEMS'];
		}
		return $arReturn;
	}

	public static function GetCartListByOrderIdWithoutBonusBluds($iOrderId) {
		$arReturn = array();

		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			// обновим корзину (удалим устаревшие записи и актуализируем цены)
			self::RecalcBasketItems($iOrderId, false);

			$arBasketItems = array();
			$dbBasketItems = CSaleBasket::GetList(
				array(
					'NAME' => 'ASC',
					'ID' => 'ASC'
				),
				array(
					'ORDER_ID' => $iOrderId,
					'LID' => 's1',
				),
				false,
				false,
				array('ID', 'PRODUCT_ID', 'QUANTITY', 'PRICE', 'WEIGHT', 'DISCOUNT_PRICE')
			);

			while($arItem = $dbBasketItems->Fetch()) {
				$arBasketItems[$arItem['ID']] = $arItem;
			}

			if($arBasketItems) {
				$arPropsID = array();
				$dbProp = CSaleBasket::GetPropsList(
					array(
						'SORT' => 'ASC', 
						'ID' => 'ASC'
					),
					array(
						'BASKET_ID' => array_keys($arBasketItems), 
						'!CODE' => array('CATALOG.XML_ID', 'PRODUCT.XML_ID')
					)
				);
				while($arProp = $dbProp->GetNext()) {
					$arPropsID[$arProp['BASKET_ID']][$arProp['CODE']] = $arProp['VALUE'];
				}

				$arReturn['TOTAL_PRICE'] = 0;
				$arReturn['PERSONS_CNT'] = 0;
				foreach($arBasketItems as $key => $row) {
					if ((!empty($arPropsID[$row['ID']]['BY_COUPON'])) && ($arPropsID[$row['ID']]['BY_COUPON'] == 'Y')) {
						unset($arBasketItems[$key]);
					} else {
						$arReturn['TOTAL_PRICE'] += $row['PRICE'] * $row['QUANTITY'];
						$arBasketItems[$key]['PROPS'] = $arPropsID[$row['ID']];
					}
				}

				# Обработка окончательного массива
				$arReturn['current'] = array();
				$arReturn['guest'] = array();
				$arReturn['common'] = array();
				$arReturn['PRODUCT_IDS'] = array();
				$arReturn['GUEST_CNT'] = array();
				$arPersons = array();
				$arGuests = array();
				$arCurret = array();
				foreach($arBasketItems as $key => $arItem) {
					$iPersonId = 0;
					$sTableType = 'current';

					if ($arItem['PROPS']['type'] == 'cupon') {
						$arItem['PRODUCT_ID'] = $arItem['PROPS']['type_coupon'];
					}
					if ($arItem['PROPS']['type'] == 'guest_count') {
						$arReturn['GUEST_CNT'] = $arItem;
						continue;
					}
					if(self::isCommonTab($arItem)) {
						$sTableType = 'common';
					} elseif(($arItem['PROPS']['type'] == 'cupon') && (CNiyamaCoupons::GetTypeForAll($arItem['PRODUCT_ID']))) {
						$sTableType = 'common';
					} elseif($arItem['PROPS']['guest']) {
						$iPersonId = $arItem['PROPS']['guest'];
						$sTableType = 'guest';
					}
					if($sTableType == 'guest' || $sTableType == 'current') {
						$arPersons[$iPersonId] = $iPersonId;
					}
					if($sTableType == 'guest'){
						$arGuests[$iPersonId] = $iPersonId;
					}elseif($sTableType == 'current'){
						$arCurret[$iPersonId] = $iPersonId;
					}
					$arReturn[$sTableType][$iPersonId]['PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
					$arReturn[$sTableType][$iPersonId]['ITEMS'][$key] = $arItem;
					
					$arReturn['PRODUCT_IDS'][$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
				}
				$arReturn['PERSONS_CNT'] = count($arPersons);
				$arReturn['GUESTS_CNT'] = count($arGuests);
				$arReturn['CURRENT_CNT'] = count($arCurret);
				if ((!empty($arReturn['GUEST_CNT']['QUANTITY'])) && ($arReturn['GUEST_CNT']['QUANTITY'] > 0)) {
					$arReturn['PERSONS_CNT'] = $arReturn['GUEST_CNT']['QUANTITY'];
					$arReturn['PERSONS_DOP_CNT'] = (int)$arReturn['GUEST_CNT']['QUANTITY'];
				}
				$arReturn['PERSONS_CNT'] = $arReturn['PERSONS_CNT'] > 0 ? $arReturn['PERSONS_CNT'] : 1;
				// Количество персон определяется или числом добавленных к заказу персон, или расчетным количеством персон, если пользователь не добавлял персон к заказу
				$arReturn['PERSONS_CNT_ALT'] = $arReturn['PERSONS_CNT'] > 1 ? $arReturn['PERSONS_CNT'] : self::GetPersonsCountByOrderPrice($arReturn['TOTAL_PRICE']);
			}

			CNiyamaCoupons::RecalculateCartByCoupons($arReturn, $iOrderId);
		}

		return $arReturn;
	}

	public static function GetCartListWithCalcNewQuantity($iOrderId, $ar_basket_quanity = array()) {
		$arReturn = array();

		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			// обновим корзину (удалим устаревшие записи и актуализируем цены)
			self::RecalcBasketItems($iOrderId, false);

			$arBasketItems = array();
			$dbBasketItems = CSaleBasket::GetList(
				array(
					'NAME' => 'ASC',
					'ID' => 'ASC'
				),
				array(
					'ORDER_ID' => $iOrderId,
					'LID' => 's1',
				),
				false,
				false,
				array('ID', 'PRODUCT_ID', 'QUANTITY', 'PRICE', 'WEIGHT')
			);
			while($arItem = $dbBasketItems->Fetch()) {
				if ((isset($ar_basket_quanity[$arItem['ID']])) && ($ar_basket_quanity[$arItem['ID']] >= 0)) {
					$arItem['QUANTITY'] = $ar_basket_quanity[$arItem['ID']];
				}
				$arBasketItems[$arItem['ID']] = $arItem;
			}

			if($arBasketItems) {
				$arPropsID = array();
				$dbProp = CSaleBasket::GetPropsList(
					array(
						'SORT' => 'ASC', 
						'ID' => 'ASC'
					),
					array(
						'BASKET_ID' => array_keys($arBasketItems), 
						'!CODE' => array('CATALOG.XML_ID', 'PRODUCT.XML_ID')
					)
				);
				while($arProp = $dbProp->GetNext()) {
					$arPropsID[$arProp['BASKET_ID']][$arProp['CODE']] = $arProp['VALUE'];
				}

				$arReturn['TOTAL_PRICE'] = 0;
				$arReturn['PERSONS_CNT'] = 0;
				foreach($arBasketItems as $key => $row) {
					$arReturn['TOTAL_PRICE'] += $row['PRICE'] * $row['QUANTITY'];
					$arBasketItems[$key]['PROPS'] = $arPropsID[$row['ID']];
				}

				# Обработка окончательного массива
				$arReturn['current'] = array();
				$arReturn['guest'] = array();
				$arReturn['common'] = array();
				$arPersons = array();
				$arPersons = array();
				$arGuests = array();
				$arCurret = array();
				foreach($arBasketItems as $key => $arItem) {
					$iPersonId = 0;
					$sTableType = 'current';

					if ($arItem['PROPS']['type'] == 'cupon') {
						$arItem['PRODUCT_ID'] = $arItem['PROPS']['type_coupon'];
					}
					if(self::isCommonTab($arItem)) {
						$sTableType = 'common';
					} elseif(($arItem['PROPS']['type'] == 'cupon') && (CNiyamaCoupons::GetTypeForAll($arItem['PRODUCT_ID']))) {
						$sTableType = 'common';
					} elseif($arItem['PROPS']['guest']) {
						$iPersonId = $arItem['PROPS']['guest'];
						$sTableType = 'guest';
					}
					if($sTableType == 'guest' || $sTableType == 'current') {
						$arPersons[$iPersonId] = $iPersonId;
					}
					if($sTableType == 'guest') {
						$arGuests[$iPersonId] = $iPersonId;
					} elseif($sTableType == 'current') {
						$arCurret[$iPersonId] = $iPersonId;
					}
					$arReturn[$sTableType][$iPersonId]['PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
					$arReturn[$sTableType][$iPersonId]['ITEMS'][$key] = $arItem;
				}

				$arReturn['PERSONS_CNT'] = count($arPersons);
				$arReturn['GUESTS_CNT'] = count($arGuests);
				$arReturn['CURRENT_CNT'] = count($arCurret);
				if(!empty($arReturn['GUEST_CNT']['QUANTITY']) && $arReturn['GUEST_CNT']['QUANTITY'] > 0) {
					$arReturn['PERSONS_CNT'] = $arReturn['GUEST_CNT']['QUANTITY'];
					$arReturn['PERSONS_DOP_CNT'] = (int)$arReturn['GUEST_CNT']['QUANTITY'];
				}
				$arReturn['PERSONS_CNT'] = $arReturn['PERSONS_CNT'] > 0 ? $arReturn['PERSONS_CNT'] : 1;
				// Количество персон определяется или числом добавленных к заказу персон, или расчетным количеством персон, если пользователь не добавлял персон к заказу
				$arReturn['PERSONS_CNT_ALT'] = $arReturn['PERSONS_CNT'] > 1 ? $arReturn['PERSONS_CNT'] : self::GetPersonsCountByOrderPrice($arReturn['TOTAL_PRICE']);
			}
			CNiyamaCoupons::RecalculateCartByCoupons($arReturn,$iOrderId);
		}

		return $arReturn;
	}

	// возвращает расчетное число персон по сумме заказа
	public static function GetPersonsCountByOrderPrice($dOrderPrice) {
		$dOrderSumByPerson = intval(CNiyamaCustomSettings::GetStringValue('order_sum_by_person', '1000'));
		$iReturn = 1;
		if($dOrderSumByPerson > 0) {
			$iReturn = ceil($dOrderPrice / $dOrderSumByPerson);
		}
		return $iReturn;
	}

	/**
	 * Получение дополнителных компонетов для корзины
	 * GetCartDopCompList
	 * @param $iOrderId
	 * @return array
	 */
	public static function GetCartDopCompList($iOrderId) {
		# Получаем список товаров в корзине
		$arReturn = array(
			'CUR_CART' => array(),
			'ADD_COMP' => array()
		);
		$arProductsCnt = array();
		$arProductsQuantity = array();
		$arReturn['CUR_CART'] = self::GetCartListByOrderId($iOrderId);
		$arTableTypes = array('current', 'guest', 'common');
		$arPieces = array();
		foreach($arTableTypes as $sTableType) {
			if($arReturn['CUR_CART'][$sTableType]) {
				foreach($arReturn['CUR_CART'][$sTableType] as $iGuestId => $arGuestTable) {
					if($arGuestTable['ITEMS']) {
						foreach($arGuestTable['ITEMS'] as $arItem) {
							if($arItem['PROPS']['type'] == 'product') {
								if(!isset($arProductsCnt[$arItem['PRODUCT_ID']])) {
									$arProductsCnt[$arItem['PRODUCT_ID']] = 0;
									$arProductsQuantity[$arItem['PRODUCT_ID']] = 0;
								}
								$arProductsCnt[$arItem['PRODUCT_ID']] += 1;
								$arProductsQuantity[$arItem['PRODUCT_ID']] += $arItem['QUANTITY'];
								// Получаем информацию по ID
								$arProductData = CNiyamaCatalog::GetProductDetail($arItem['PRODUCT_ID']);
								if(!isset($arPieces[$arItem['PRODUCT_ID']])) {
									//$arPieces[$arItem['PRODUCT_ID']] = intval($arProductData['INFO']['PROPERTY_NUM_OF_PIECES']);
									// временное решение, т.к. количество доп.компонента в настройках блюда еще нет
									$arPieces[$arItem['PRODUCT_ID']] = 1;
								}
							}
						}
					}
				}
			}
		}
		$iPersonsCnt = $arReturn['CUR_CART']['PERSONS_CNT_ALT'];

		# Получаем список доп.компонентов для товаров
		$arDopCompList = array();
		$arCount = array();
		$arProducts2Comp = array();
		foreach($arProductsCnt as $iProductElementId => $iDishCnt) {
			$arDopComp = CNiyamaCatalog::getProductDopComp($iProductElementId);
			foreach($arDopComp as $arDopCompItem) {
				$arDopCompItem['PRODUCT_ID'] = $arDopCompItem['ID'];
				$arReturn['ADD_COMP'][$arDopCompItem['ID']] = array(
					'INFO' => array(
						'ID' => $arDopCompItem['ID'],
						'NAME' => $arDopCompItem['NAME'],
						'IMG_ID' => $arDopCompItem['PREVIEW_PICTURE'],
						'PROPERTY_UNIT_TYPE' => $arDopCompItem['PROPERTY_UNIT_TYPE'],
						'PROPERTY_CALC_ALG_XML_ID' => $arDopCompItem['PROPERTY_CALC_ALG_XML_ID'],
						'PROPERTY_SELL' => $arDopCompItem['PROPERTY_SELL'],
					),
					'FREE' => array(
						'PRODUCT_ID' => $arDopCompItem['FREE_PRODUCT_ID'],
						'XML_ID' => $arDopCompItem['FREE_PRODUCT_XML_ID'],
						'PRICE' => 0,
					),
					'PAID' => array(
						'PRODUCT_ID' => $arDopCompItem['PAID_PRODUCT_ID'],
						'XML_ID' => $arDopCompItem['PAID_PRODUCT_XML_ID'],
						'PRICE' => doubleval($arDopCompItem['PAID_PRODUCT_PRICE']),
					),
				);
				if(!isset($arCount[$arDopCompItem['ID']])) {
					$arCount[$arDopCompItem['ID']] = 0;
				}
				$arCount[$arDopCompItem['ID']] += $iDishCnt;
				$arProducts2Comp[$arDopCompItem['ID']][$iProductElementId] = $iProductElementId;
			}
		}


		foreach($arReturn['ADD_COMP'] as $iDopCompElementId => $arItem) {
			if(!isset($arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'])) {
				$arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'] = 0;
			}

			switch($arItem['INFO']['PROPERTY_CALC_ALG_XML_ID']) {
				# По количеству для блюда
				case 'ALG_3':
					foreach($arProducts2Comp[$iDopCompElementId] as $iProductElementId) {
						//$arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'] += $arPieces[$iProductElementId] ? $arPieces[$iProductElementId] * $arProductsCnt[$iProductElementId] : $arProductsCnt[$iProductElementId];
						$arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'] += $arPieces[$iProductElementId] ? $arPieces[$iProductElementId] * $arProductsQuantity[$iProductElementId] : $arProductsQuantity[$iProductElementId];
					}
				break;

				# По кол-ву персон
				case 'ALG_2':
					$arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'] = $iPersonsCnt;
				break;

				# По кол-ву блюд
				default:
					$arReturn['ADD_COMP'][$iDopCompElementId]['FREE_COUNT'] = $arCount[$iDopCompElementId];
				break;
			}

			$arReturn['ADD_COMP'][$iDopCompElementId]['CAN_BUY_MORE'] = 'N';
			$arReturn['ADD_COMP'][$iDopCompElementId]['MORE_PRICE'] = 0;
			if($arItem['INFO']['PROPERTY_SELL'] && $arItem['PAID']['PRICE']) {
				$arReturn['ADD_COMP'][$iDopCompElementId]['CAN_BUY_MORE'] = 'Y';
				$arReturn['ADD_COMP'][$iDopCompElementId]['MORE_PRICE'] = $arItem['PAID']['PRICE'];
			}
		}
		return $arReturn;
	}

	/**
	 * setGuest
	 * @param $iBasketItemId
	 * @param bool $iGuestID
	 */
	public static function setGuest($iBasketItemId, $iGuestID = null)
	{
		$iBasketItemId = intval($iBasketItemId);
		if ($iBasketItemId > 0) {
			if (CModule::IncludeModule("sale")) {

				$dbProp = CSaleBasket::GetPropsList(
					array("SORT" => "ASC", "ID" => "ASC"),
					array("BASKET_ID" => $iBasketItemId, "!CODE" => array("CATALOG.XML_ID", "PRODUCT.XML_ID"))
				);
				$arCartProperty = array();
				while ($arProp = $dbProp->GetNext()) {

					if ($arProp['CODE'] != "guest") {
						$arCartProperty[] = array(
							"NAME" => $arProp['NAME'],
							"CODE" => $arProp['CODE'],
							"VALUE" => $arProp['VALUE'],
						);
					}
				}

				if (!is_null($iGuestID)) {
					$arCartProperty[] = array(
						"NAME" => "Гость",
						"CODE" => "guest",
						"VALUE" => $iGuestID,
					);
				}

				$arUpdFields = array(
					"PROPS" => $arCartProperty,
				);

				CSaleBasket::Update($iBasketItemId, $arUpdFields);
			}
		}
	}

	/**
	 * GetTotalPrice
	 * @return array
	 */
	public static function GetTotalPrice() {
		CModule::IncludeModule('sale');

		//$iFUserId = intval(CSaleBasket::GetBasketUserID(true));
		$arFilter = array(
			'CAN_BUY' => 'Y', 
			'DELAY' => 'N', 
			'SUBSCRIBE' => 'N',
			// LSV: Если задается ORDER_ID, то зачем еще $iFUserId?
			//'FUSER_ID' => $iFUserId, 
			'LID' => 's1',
			'ORDER_ID' => CNiyamaOrders::GetCurUserDraftOrderId(),
		);

		$dbBasket = CSaleBasket::GetList(
			array(),
			$arFilter,
			false,
			false,
			array(
				'QUANTITY', 'PRICE', 'CURRENCY', 'DISCOUNT_PRICE', 'WEIGHT', 'VAT_RATE',
				'ID', 'SET_PARENT_ID', 'PRODUCT_ID', 'CATALOG_XML_ID', 'PRODUCT_XML_ID',
				'PRODUCT_PROVIDER_CLASS', 'TYPE'
			)
		);
		$arBasketItems = array();
		while($arItem = $dbBasket->Fetch()) {
			if(CSaleBasketHelper::IsSetItem($arItem)) {
				continue;
			}
			$arBasketItems[] = $arItem;
		}

		$dTotalPrice = 0;
		if($arBasketItems) {
			$arOrder = self::CalculateOrder($arBasketItems);
			$dTotalPrice = $arOrder['ORDER_PRICE'];
		}

		return array(
			'NUM_PRODUCTS' => count($arBasketItems),
			'TOTAL_PRICE' => $dTotalPrice
		);
	}

	/**
	 * CalculateOrder
	 * @param $arBasketItems
	 * @return array
	 */
	private function CalculateOrder($arBasketItems) {
		$dTotalPrice = 0;
		$dTotalWeight = 0;

		foreach($arBasketItems as $arItem) {
			$dTotalPrice += $arItem['PRICE'] * $arItem['QUANTITY'];
			$dTotalWeight += $arItem['WEIGHT'] * $arItem['QUANTITY'];
		}

		$arOrder = array(
			'SITE_ID' => 's1',
			'ORDER_PRICE' => $dTotalPrice,
			'ORDER_WEIGHT' => $dTotalWeight,
			'BASKET_ITEMS' => $arBasketItems
		);

		if (is_object($GLOBALS['USER'])) {
			$arOrder['USER_ID'] = $GLOBALS['USER']->GetId();
			$arErrors = array();
			CSaleDiscount::DoProcessOrder($arOrder, array(), $arErrors);
		}

		return $arOrder;
	}

	/**
	 * Проверка на общую вкладку
	 * IsCommonTab
	 * @param $arItem
	 * @return bool
	 */
	private static function IsCommonTab($arItem) {
		# Если доп компонент
		if($arItem['PROPS']['type'] == 'dop_component') {
			return true;
		} elseif($arItem['PROPS']['type'] == CNiyamaCart::$arCartItemType[3]) {
			return true;
		} elseif($arItem['PROPS']['guest'] == self::$iForAllGuestID) {
			return true;
		}
	   return false;
	}

	/**
	 * GetCartList
	 * @param int $iFUserId
	 * @param int $iUserId
	 * @return array
	 */
	public static function GetCartList($iFUserId = 0, $iUserId = 0) {
		$arReturn = array();
		if(CModule::IncludeModule('sale')) {
			$iUserId = intval($iUserId);
			$iUserId = $iUserId > 0 ? $iUserId : $GLOBALS['USER']->GetId();

			$iFUserId = intval($iFUserId);
			if($iFUserId <= 0) {
				if($iUserId > 0) {
					$iFUserId = CNiyamaOrders::GetFUserIdByUserId($iUserId);
				}
				$iFUserId = $iFUserId > 0 ? $iFUserId : CSaleBasket::GetBasketUserID();
			}

			$iOrderId = CNiyamaOrders::GetUserDraftOrderId($iFUserId, $iUserId);
			$arReturn = self::GetCartListByOrderId($iOrderId);
		}
		return $arReturn;
	}

	public static function getCartListSimple($iFUserId = 0, $iUserId = 0)
	{
		$arReturn = array();
		if (CModule::IncludeModule("sale")) {
			$iFUserId = intval($iFUserId);
			$iFUserId = $iFUserId > 0 ? $iFUserId : CSaleBasket::GetBasketUserID();
			$iUserId = intval($iUserId);
			$iUserId = $iUserId > 0 ? $iUserId : $GLOBALS['USER']->GetId();
			$iOrderId = CNiyamaOrders::GetUserDraftOrderId($iFUserId, $iUserId);
			$arReturn = self::GetCartListByOrderIdSimple($iOrderId);
		}
		return $arReturn;
	}

	public static function getSubPrice($arCartItems)
	{
		if (!self::getGuestID()) {
			return ($arCartItems['current'][0]['PRICE']) ? $arCartItems['current'][0]['PRICE'] : '0';
		} else {
			return $arCartItems['guest'][self::getGuestID()]['PRICE'] ? $arCartItems['guest'][self::getGuestID()]['PRICE'] : '0';
		}
	}

	/**
	 * getGuestID
	 */
	public static function getGuestID()
	{
		return $GLOBALS['APPLICATION']->get_cookie(self::$CookieName);
	}

	/**
	 * Если false то записываем к текущему пользователю
	 *
	 * setGuestID
	 * @param $iGuestID
	 */
	public static function SetGuestID($iGuestID = false) {
		return $GLOBALS['APPLICATION']->set_cookie(self::$CookieName, $iGuestID);
	}

	public static function GetCurrentOrderData() {
		if($GLOBALS['USER']->IsAuthorized()) {
			 return $_SESSION[self::$OrderCookieName];
		}
		return false;
	}

	public static function SetCurrentOrderData($arCurrentOrderData) {
		if($GLOBALS['USER']->IsAuthorized()) {
			if(!$arCurrentOrderData) {
				self::SetOrderGuestId(false);
			}
			return $_SESSION[self::$OrderCookieName] = $arCurrentOrderData;
		}
		return false;
	}

	/**
	 * getGuestID
	 */
	public static function getOrderGuestID()
	{
		if ($GLOBALS['USER']->IsAuthorized() && self::getCurrentOrderData()) {
			return $_SESSION[self::$OrderGuestSessionName];
		}
		return false;
	}

	/**
	 * setOrderGuestID
	 * @param bool $iGuestID
	 * @return bool
	 */
	public static function setOrderGuestID($iGuestID = false)
	{
		if ($GLOBALS['USER']->IsAuthorized() && self::getCurrentOrderData()) {
			return $_SESSION[self::$OrderGuestSessionName] = $iGuestID;
		}
		return false;
	}

	/**
	 * SetQuantity
	 * @param $iBasketItemId
	 * @param $iQuantity
	 */
	public static function SetQuantity($iBasketItemId, $iQuantity) {
		$iQuantity = intval($iQuantity);
		if ($iQuantity > 0) {
			if (CModule::IncludeModule("sale")) {

				// Получаем позицию корзины
				$dbBasketItems = CSaleBasket::GetList(
					array(),
					array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
						"LID" => 's1',
						"ID"  => $iBasketItemId,
					),
					false,
					false,
					array("ID", "QUANTITY")
				);
				if ($arItem = $dbBasketItems->Fetch()) {
					if ($arItem['QUANTITY'] != $iQuantity) {
						$arUpdFields = array(
							"QUANTITY" => $iQuantity,
						);
						CSaleBasket::Update($iBasketItemId, $arUpdFields);
					}
				}
			}
		}
	}

	public static function ChangeProduct($iBasketItemId, $iNewProductId) {
		$iNewProductId = intval($iNewProductId);
		if ($iNewProductId > 0) {
			if (CModule::IncludeModule("sale")) {

				// Получаем позицию корзины
				$dbBasketItems = CSaleBasket::GetList(
					array(),
					array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
						"LID" => 's1',
						"ID"  => $iBasketItemId,
					),
					false,
					false,
					array("ID", "PRODUCT_ID")
				);
				if ($arItem = $dbBasketItems->Fetch()) {
					if ($arItem['PRODUCT_ID'] != $iNewProductId) {
						$arProduct = CNiyamaCatalog::GetProductDetail($iNewProductId);
						if (!empty($arProduct)) {
							$arUpdFields = array(
								"PRODUCT_ID" => $arProduct['INFO']['ID'],
								"PRICE"	  => $arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
								"NAME"	   => $arProduct['INFO']['NAME'],
								"DETAIL_PAGE_URL" => $arProduct['INFO']['DETAIL_PAGE_URL'],
								"WEIGHT"	 => $arProduct['INFO']['PROPERTY_WEIGHT_VALUE'], //Вес Блюда
							);
							CSaleBasket::Update($iBasketItemId, $arUpdFields);
						}
					}
				}
			}
		}
	}

	public static function SetValCouponUsed($iBasketItemId, $val=0)
	{
		$iBasketItemId = intval($iBasketItemId);
		if ($iBasketItemId > 0) {
			if (CModule::IncludeModule("sale")) {

				$dbProp = CSaleBasket::GetPropsList(
					array("SORT" => "ASC", "ID" => "ASC"),
					array("BASKET_ID" => $iBasketItemId, "!CODE" => array("CATALOG.XML_ID", "PRODUCT.XML_ID"))
				);
				$arCartProperty = array();
				while ($arProp = $dbProp->GetNext()) {
						$arCartProperty[$arProp['CODE']] = array(
							"NAME" => $arProp['NAME'],
							"CODE" => $arProp['CODE'],
							"VALUE" => $arProp['VALUE'],
						);
				}

				if ((int)$arCartProperty['val_coupon_used'] != (int)$val) {
					$arCartProperty['val_coupon_used']['VALUE'] = (int)$val;
					$PROPS = array();
					foreach ($arCartProperty as $el_prop) {
						$PROPS[] = $el_prop;
					}
					$arUpdFields = array(
						"PROPS" => $arCartProperty,
					);
	
					CSaleBasket::Update($iBasketItemId, $arUpdFields);
				}
			}
		}
	}

	public static function GetValCouponUsed($iBasketItemId)
	{
		$val=0;
		$iBasketItemId = intval($iBasketItemId);
		if ($iBasketItemId > 0) {
			if (CModule::IncludeModule("sale")) {

				$dbProp = CSaleBasket::GetPropsList(
					array("SORT" => "ASC", "ID" => "ASC"),
					array("BASKET_ID" => $iBasketItemId, "CODE" => array("val_coupon_used"))
				);
				$arCartProperty = array();
				if ($arProp = $dbProp->GetNext()) {
					$val = $arProp['VALUE'];
				}
				
			}
		}
		return $val;
	}
	

	/**
	 * ReduceQuantity
	 * @param $iBasketItemId
	 * @param $iQuantity
	 */
	public static function ReduceQuantity($iBasketItemId, $iQuantity) {
		$iQuantity = intval($iQuantity);
		if ($iQuantity > 0) {
			if (CModule::IncludeModule("sale")) {
				// Получаем позицию корзины
				$dbBasketItems = CSaleBasket::GetList(
					array(),
					array(
						"LID" => 's1',
						"ID" => $iBasketItemId,
					),
					false,
					false,
					array("ID", "QUANTITY")
				);
				if ($arItem = $dbBasketItems->Fetch()) {
					$iNewQuantity = $arItem['QUANTITY'] - $iQuantity;
					if ($iNewQuantity > 0) {
						$arUpdFields = array(
							"QUANTITY" => $iNewQuantity,
						);
						CSaleBasket::Update($iBasketItemId, $arUpdFields);
					}
					else {
						CSaleBasket::Delete($iBasketItemId);
					}
				}
			}
		}
	}

	/**
	 * RemoveFromCart
	 * @param $iBasketItemId
	 * @return bool
	 */
	public static function RemoveFromCart($iBasketItemId) {
		$bReturn = false;
		if (CModule::IncludeModule("sale")) {

			// Получаем позицию корзины
			$dbBasketItems = CSaleBasket::GetList(
				array(),
				array(
					"LID" => 's1',
					"ID" => $iBasketItemId,
				),
				false,
				false,
				array("ID")
			);
			if ($arItem = $dbBasketItems->Fetch()) {
				
				$arPropsID = array();
				$dbProp = CSaleBasket::GetPropsList(
					array(),
					array(
						'BASKET_ID' => $iBasketItemId, 
						'!CODE' => array('CATALOG.XML_ID', 'PRODUCT.XML_ID')
					)
				);
				while($arProp = $dbProp->GetNext()) {
					$arPropsID[$arProp['CODE']] = $arProp['VALUE'];
				}

				if ((!empty($arPropsID['ID_COUPON_BLUDO'])) && ($arPropsID['ID_COUPON_BLUDO'] > 0)) {
					$cart = self::getCartListSimple();
					foreach ($cart as $key => $el) {
						if ($el['PROPS']['ID_COUPON_BLUDO'] == $arPropsID['ID_COUPON_BLUDO']) {
							$bReturn = CSaleBasket::Delete($el['ID']);//удаляем все блюда связанные с купоном
						}
					}
				} else {
					// Удаляем
					$bReturn = CSaleBasket::Delete($iBasketItemId);
				}
			}
		}
		return $bReturn;
	}

	/**
	 * clearCart
	 */
	public static function clearCart()
	{
		if (CModule::IncludeModule("sale")) {

			$dbBasketItems = CSaleBasket::GetList(
				array(
					"NAME" => "ASC",
					"ID" => "ASC"
				),
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => 's1',
					'ORDER_ID' => CNiyamaOrders::GetCurUserDraftOrderId(),
				),
				false,
				false,
				array("ID")
			);
			while ($arItem = $dbBasketItems->Fetch())
			{
				CSaleBasket::Delete($arItem['ID']);
			}
		}
	}

	/**
	 * GetGuestsList
	 * @return array
	 */
	public static function GetGuestsList($iOwnerUserId = 0, $bRefreshCache = false, $bFilterByFUser = true, $bFilterActive = true, $iFUserId = 0) {
		$arReturn = array();
		if(CModule::IncludeModule('sale')) {
			$iOwnerUserId = intval($iOwnerUserId);
			if($iOwnerUserId <= 0 && $iFUserId <= 0) {
				$iOwnerUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetId() : 0;
			}
			if(!$iOwnerUserId || $iFUserId > 0) {
				// страхуемся, чтобы не получилась выборка всех гостей для незареганых юзеров
				$bFilterByFUser = true;
			}
			if($bFilterByFUser && $iFUserId <= 0) {
				$iFUserId = $iOwnerUserId ? CNiyamaOrders::GetFUserIdByUserId($iOwnerUserId) : CSaleBasket::GetBasketUserId();
			}

			// дополнительный параметр для ID кэша
			$sCacheAddParam = $iOwnerUserId.'|'.$iFUserId.'|'.($bFilterByFUser ? 'Y' : 'N').'|'.($bFilterActive ? 'Y' : 'N');
			// идентификатор кэша (обязательный и уникальный параметр)
			$sCacheId = __CLASS__.'||'.__FUNCTION__;
			// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
			$arAddCacheTags = array('getGuestsList');
			// путь для сохранения кэша
			$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
			// сохранять ли значения дополнительно в виртуальном кэше
			$bUseStaticCache = true;
			// максимальное количество записей виртуального кэша
			$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
			// соберем в массив идентификационные параметры кэша
			$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

			$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
			if(!$bRefreshCache && $obExtCache->InitCache()) {
				$arReturn = $obExtCache->GetVars();
			} else {
				$arFilter = array();
				$arFilter['IBLOCK_ID'] = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_PERSON', 'NIYAMA_FOR_USER');
				if($bFilterActive) {
					$arFilter['ACTIVE'] = 'Y';
				}
				$arFilter['PROPERTY_USER_ID'] = $iOwnerUserId ? $iOwnerUserId : false;
				if($bFilterByFUser) {
					$arFilter['=PROPERTY_FUSER_ID'] = $iFUserId;
				}
				// открываем кэшируемый участок
				$obExtCache->StartDataCache();
				if(CModule::IncludeModule('iblock')) {
					$dbItems = CIBlockElement::GetList(
						array(
							'ID' => 'ASC'
						),
						$arFilter,
						false,
						false,
						array(
							'ID', 'NAME', 'PREVIEW_PICTURE',
							'PROPERTY_FUSER_ID', 'PROPERTY_USER_ID',
						)
					);
					while($arItem = $dbItems->GetNext(false, false)) {
						$arReturn[$arItem['ID']] = $arItem;
					}
				}

				if(empty($arReturn)) {
					$obExtCache->AbortDataCache();
				}

				// закрываем кэшируемый участок
				$obExtCache->EndDataCache($arReturn);
			}
		}
		return $arReturn;
	}

	/**
	 * Возвращает список гостей ТЕКУЩЕГО юзера
	 *
	 * @return array
	 */
	public static function GetCurUserGuestsList() {
		$iOwnerUserId = $GLOBALS['USER'] ? intval($GLOBALS['USER']->GetId()) : 0;
		if($iOwnerUserId > 0) {
			return self::GetGuestsList($iOwnerUserId, false, false);
		} else {
			return self::GetGuestsList();
		}
	}

	/**
	 * Возвращает гостя ТЕКУЩЕГО юзера
	 *
	 * getGuestById
	 * @param $iGuestId
	 * @return bool
	 */
	public static function GetGuestById($iGuestId) {
		$arGuestList = self::GetCurUserGuestsList();
		if($arGuestList) {
			if(isset($arGuestList[$iGuestId])) {
				return $arGuestList[$iGuestId];
			}
		}
		return false;
	}

	/**
	 * SetTmpFuserValue
	 */
	public static function SetTmpFuserValue() {
		$_SESSION["TMP_FUSER_VALUE"] = false;
		if(!$_SESSION['TMP_FUSER_VALUE']) {
			$cookie_name = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
			$ID = $_SESSION['SALE_USER_ID'];

			if(is_numeric($ID)) {
				$ID = intval($ID);
			}

			if(!strlen($ID) || $ID == 0) {
				$ID = $_COOKIE[$cookie_name.'_SALE_UID'];
			}

			if(COption::GetOptionString('sale', 'encode_fuser_id', 'N') == 'Y') {
				$arRes = CSaleUser::GetList(array('CODE' => $ID));
				if(!empty($arRes)) {
					$ID = $arRes['ID'];
				}
			}

			if($ID > 0) {
				$_SESSION['TMP_FUSER_VALUE'] = $ID;
			}
		}
	}

	/**
	 * TransferGuests
	 * @param $UserId
	 */
	public static function TransferGuests($iUserId)
	{
		if (CModule::IncludeModule('sale') && CModule::IncludeModule('iblock')) {

			$iLastFuserId = $_SESSION['TMP_FUSER_VALUE'];
			if (intval($iLastFuserId) > 0) {

				$iFUserId = $iUserId ? CNiyamaOrders::GetFUserIdByUserId($iUserId) : CSaleBasket::GetBasketUserID();

				# чистим TMP_FUSER_VALUE
				$_SESSION['TMP_FUSER_VALUE'] = false;

				# Переназначаем Гостей
				$iIBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_PERSON', 'NIYAMA_FOR_USER');
				$rsItems = CIBlockElement::GetList(
					array(),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'PROPERTY_FUSER_ID' => $iLastFuserId,
						'PROPERTY_USER_ID' => false,
					),
					false,
					false,
					array(
						'ID', 'NAME', 'PREVIEW_PICTURE',
						'PROPERTY_FUSER_ID', 'PROPERTY_USER_ID',
					)
				);
				while ($arItem = $rsItems->Fetch()) {
					if ($arItem['PROPERTY_FUSER_ID_VALUE'] == $iLastFuserId) {

						$el = new CIBlockElement;
						$res = $el->Update(
							$arItem['ID'], 
							array(
								'PROPERTY_VALUES' => array(
									'FUSER_ID' => $iFUserId,
									'USER_ID' => $iUserId
								)
							)
						);

					}
				}
			}
		}
	}

	/**
	 * addGuest
	 * @param $Name
	 * @param bool $arFilesAvatar
	 * @param bool $iAvatarId
	 * @return bool|int
	 */
	public static function AddGuest($Name, $arFilesAvatar = false, $iAvatarId = false)
	{
		if (CModule::IncludeModule('sale') && CModule::IncludeModule('iblock')) {

			$iUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetID() : 0;
			$iFUserId = $iUserId ? CNiyamaOrders::GetFUserIdByUserId($iUserId) : CSaleBasket::GetBasketUserID();
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_PERSON', 'NIYAMA_FOR_USER');

			# Добавляем аватар
			if (!$arFilesAvatar) {
				# Если не передан id аватара
				if (!$iAvatarId) {
					# Получаем первый аватар
					$iAvatarId = CUsersData::getFirstDefaultAvatar();
				}
				$arFirstAva = CFile::GetFileArray($iAvatarId);
				if(!empty($arFirstAva['SRC'])) {
					$arFirstAva = CFile::MakeFileArray($arFirstAva['SRC']);
				}
			} else {
				# Если мы добавляем своё фото
				$arFirstAva = $arFilesAvatar;
			}

			$addParams = array(
				'IBLOCK_ID' => $iIBlockId,
				'NAME' => $Name,
				'PREVIEW_PICTURE' => $arFirstAva,
				'PROPERTY_VALUES' => array(
					'FUSER_ID' => $iFUserId,
					'USER_ID' => $iUserId
				)
			);
			$el = new CIBlockElement();
			$res = $el->Add($addParams);

			# Событие добавления гостя
			foreach(GetModuleEvents('main', 'OnAfterGuestAdd', true) as $arEvent) {
				ExecuteModuleEventEx($arEvent, array($res));
			}
			if (intval($res) > 0) {
				//$GLOBALS['CACHE_MANAGER']->ClearByTag('iblock_id_'. $iIBlockId);
			}
			return $res;
		}
	}

	/**
	 * deleteGuest
	 * @param $ID
	 * @return bool
	 */
	public static function deleteGuest($ID)
	{
		if(CModule::IncludeModule('iblock')) {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_PERSON', 'NIYAMA_FOR_USER');
			$el = new CIBlockElement();
			$res = $el->Update($ID, array('ACTIVE' => 'N'));
			//$GLOBALS['CACHE_MANAGER']->ClearByTag("iblock_id_". $iIBlockId);
		}
		return $res;
	}

	/**
	 * UpdateGuest
	 * @param $ID
	 * @param $Name
	 * @param bool $arFilesAvatar
	 * @param bool $iAvatarId
	 * @return bool|int
	 */
	public static function UpdateGuest($ID, $Name, $arFilesAvatar = false, $iAvatarId = false)
	{
		if (CModule::IncludeModule('sale') && CModule::IncludeModule('iblock')) {

			$iIBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_PERSON', 'NIYAMA_FOR_USER');
			$arFirstAva = false;
			# Добавляем аватар
			if (!$arFilesAvatar) {
				# Если не передан id аватара
				if (!$iAvatarId) {
					# Получаем первый аватар
				   // $iAvatarId = CUsersData::getFirstDefaultAvatar();
				}
				if ($iAvatarId > 0) {
					$arFirstAva = CFile::GetFileArray($iAvatarId);
					if(!empty($arFirstAva['SRC'])) {
						$arFirstAva = CFile::MakeFileArray($arFirstAva['SRC']);
					}
				}
			} else {
				# Если мы добавляем свою фото
				$arFirstAva = $arFilesAvatar;
			}
			
			$addParams = array(
				'NAME' => $Name,
			);
			if ($arFirstAva !== false) {
				$addParams['PREVIEW_PICTURE'] = $arFirstAva;
			}

			$el = new CIBlockElement;
			$res = $el->Update($ID, $addParams);

			if (intval($res) > 0){
			   // $GLOBALS['CACHE_MANAGER']->ClearByTag("iblock_id_". $iIBlockId);
			}

			return $res;
		}
	}

	/**
	 * getItemData
	 * @param $BasketId
	 * @return array|bool
	 */
	public static function getItemData($BasketId)
	{
		$arFields = false;
		if (CModule::IncludeModule("sale")) {
			$arBasketItems = array();
			$dbBasketItems = CSaleBasket::GetList(
				array(
					'NAME' => 'ASC',
					'ID'   => 'ASC'
				),
				array(
					'ID' => $BasketId,
				),
				false,
				false,
				array('ID', 'PRODUCT_ID', 'QUANTITY', 'PRICE', 'WEIGHT', 'ORDER_ID', 'FUSER_ID')
			);
			if($arItem = $dbBasketItems->Fetch()) {

				$arBasketItems[$arItem['ID']] = $arItem;

				$arPropsID = array();
				$dbProp = CSaleBasket::GetPropsList(
					array(
						'SORT' => 'ASC',
						'ID' => 'ASC'
					),
					array(
						'BASKET_ID' => $BasketId,
						'!CODE' => array('CATALOG.XML_ID', 'PRODUCT.XML_ID')
					)
				);
				while($arProp = $dbProp->GetNext()) {
					$arBasketItems[$arProp['BASKET_ID']]['PROP'] = $arProp;
				}
			}

			$arProduct = CNiyamaCatalog::GetProductDetail($arBasketItems[$BasketId]['PRODUCT_ID']);

			// добавляем новый товар в корзину
			$arCartProperty = array();
			if(isset($arBasketItems[$BasketId]['PROP'])) {
				$arCartProperty[] = array(
					"NAME"  => $arBasketItems[$BasketId]['PROP']['NAME'],
					"CODE"  => $arBasketItems[$BasketId]['PROP']['CODE'],
					"VALUE" => $arBasketItems[$BasketId]['PROP']['VALUE'],
				);
			}
			$arBasketItems = $arBasketItems[$BasketId];

			$arFields = array(
				"PRODUCT_ID" => $arBasketItems['PRODUCT_ID'],
				"PRICE" => $arBasketItems['PRICE'],
				"CURRENCY" => "RUB",
				"PRODUCT_XML_ID" => "",
				"QUANTITY" => $arBasketItems['QUANTITY'],
				"LID" => 's1',
				"DELAY" => "N",
				"CAN_BUY" => "Y",
				"NAME" => $arProduct['INFO']['NAME'],
				"DETAIL_PAGE_URL" => $arProduct['INFO']['DETAIL_PAGE_URL'],
				"WEIGHT" => $arProduct['INFO']['PROPERTY_WEIGHT_VALUE'], //Вес Блюда
				"DIMENSIONS" => "",
				"PROPS" => $arCartProperty,
			);

			if (!empty($arBasketItems['ORDER_ID'])){
				$arFields['ORDER_ID'] = $arBasketItems['ORDER_ID'];
			}

			$arFields['FUSER_ID'] = $arBasketItems['FUSER_ID'];
		}
		return $arFields;
	}


	/**
	 * getGuestOrderInfo
	 * @param $iGuestId
	 * @param bool $iUserId
	 * @return array|bool
	 */
	public static function getGuestOrderInfo($iGuestId, $iUserId = false)
	{

		$iUserId = ($iUserId) ? intval($iUserId) : $GLOBALS['USER']->GetID();
		if($iUserId <= 0) {
			return false;
		}
		CModule::IncludeModule('sale');
		$dbOrder = CSaleOrder::GetList(
			array(
				'ID' => 'DESC'
			),
			array(
				'USER_ID' => $iUserId,
				'@STATUS_ID' => array('E', 'F', 'N', 'P', 'R', 'S', 'T'),
				'LID' => 's1'
			),
			false,
			false,
			array(
				'ID'
			)
		);
		$iOrderCount = 0;
		$iProductsCount = 0;
		while($arOrder = $dbOrder->Fetch()) {
			$arOrderDetail = CNiyamaOrders::GetOrderById($arOrder['ID']);
			if(isset($arOrderDetail['ORDER_CART']['guest'][$iGuestId])) {
				$iOrderCount++;
				$iProductsCount += count($arOrderDetail['ORDER_CART']['guest'][$iGuestId]['ITEMS']);
			}
		}

		return array(
			'ORDER_COUNT' => $iOrderCount,
			'PRODUCT_COUNT' => $iProductsCount
		);
	}

	/**
	 * getUserOrderInfo
	 * @param bool $iUserId
	 * @return array|bool
	 */
	public static function getUserOrderInfo($iUserId = false)
	{

		$iUserId = ($iUserId) ? intval($iUserId) : $GLOBALS['USER']->GetID();
		if($iUserId <= 0) {
			return false;
		}
		CModule::IncludeModule('sale');
		$dbOrder = CSaleOrder::GetList(
			array(
				'ID' => 'DESC'
			),
			array(
				'USER_ID' => $iUserId,
				'@STATUS_ID' => array('E', 'F', 'N', 'P', 'R', 'S', 'T'),
				'LID' => 's1'
			),
			false,
			false,
			array(
				'ID'
			)
		);
		$iOrderCount  = 0;
		$iProductsCount = 0;
		$iGuestsCount = 0;
		while($arOrder = $dbOrder->Fetch()) {
			$arOrderDetail = CNiyamaOrders::GetOrderById($arOrder['ID']);
			$iOrderCount++;
			$iProductsCount += count($arOrderDetail['ORDER_CART']['PRODUCT_IDS']);
			$iGuestsCount += count($arOrderDetail['ORDER_CART']['PERSONS_CNT']);
		}

		return array(
			"ORDER_COUNT" => $iOrderCount,
			"PRODUCT_COUNT" => $iProductsCount,
			"GUEST_COUNT" => $iGuestsCount
		);
	}

	public static function getApSailProduct($productsInCart = null, $iUser = 0, $bRefreshCache = false){
		if (is_null($productsInCart)) {
			$productsInCart = $GLOBALS['CART_PRODUCT_IDS'];
		}

		// авторизированнный пользователь
		$arProducts = CNiyamaOrders::getRepeatProductsInOrderByUser($iUser);

		$arProductsActive = array();
		$arUpSailProducts = array();
		if (!empty($arProducts)) {
			foreach ($arProducts as $prod_id) {
				if (!in_array($prod_id,$productsInCart)) {
					$prod_detail = CNiyamaCatalog::GetProductDetail($prod_id,false,$bRefreshCache);
					if (!empty($prod_detail)) {
						$arProductsActive[$prod_id] = $prod_detail;
					}
				}
			}

			$faset = $_SESSION['NIYAMA']['FASET'];

			if (!empty($arProductsActive)) {
				$arProductsActiveFacet = CNiyamaCatalog::GetProductListByFaset(array_keys($arProductsActive), $faset, $bRefreshCache);
				if (!empty($arProductsActiveFacet['ITEMS'])) {
					foreach($arProductsActiveFacet['ITEMS'] as $key => $prod){
						$arUpSailProducts[$key] = $arProductsActive[$key];
					}
				} else {
					$arUpSailProducts = $arProductsActive;
				}
			}
		}
		if (empty($arUpSailProducts)) {
			
			$arCatInCart = array();
			if (!empty($productsInCart)) {
				foreach ($productsInCart as $prod_id) {
					$prod_detail = CNiyamaCatalog::GetProductDetail($prod_id, false, $bRefreshCache);
					if (!empty($prod_detail)) {
						$arCatInCart[] = $prod_detail['INFO']['IBLOCK_SECTION_ID'];
					}	
				}
			} 
			$filterCat=array('UF_UPSAILCAT' => 1);
			if (!empty($arCatInCart)) {
				$filterCat = array('!ID' => $arCatInCart, 'UF_UPSAILCAT' => 1);
			}
			$arActiveCat = array();
			$arTempActiveCat = CNiyamaCatalog::getCategoriesByFilter($filterCat,$bRefreshCache);
			if (!empty($arTempActiveCat)) {
				foreach ($arTempActiveCat as $cat) {
					$arActiveCat[] = $cat['ID'];
				}
			}
			if (!empty($arActiveCat)) {
				$arProductsFromCat = array();
				$iRandomCatId = 0;
				$faset = $_SESSION['NIYAMA']['FASET'];

				$faset_ex=array(
					'menu'=>array(),
					'ex_menu'=>$faset['ex_menu'],
					'ing'=>array(),
					'ex_ing'=>$faset['ex_ing'],
					'alerg'=>$faset['alerg'],
				);
				while ((empty($arProductsFromCat))&&(!empty($arActiveCat))) {
					$iRandomKey=array_rand($arActiveCat);
					$iRandomCatId=$arActiveCat[$iRandomKey];
					unset($arActiveCat[$iRandomKey]);
					$faset_ex=array(
						'menu'=>array($iRandomCatId),
						'ex_menu'=>$faset['ex_menu'],
						'ing'=>array(),
						'ex_ing'=>$faset['ex_ing'],
						'alerg'=>$faset['alerg'],
					); 
					$arProductsExFacet=CNiyamaCatalog::GetProductListByFaset(array(),$faset_ex,$bRefreshCache);   	
					if (!empty($arProductsExFacet['ITEMS'])){
						foreach($arProductsExFacet['ITEMS'] as $key=>$prod){
							$arProductsFromCat[$key]=$prod;
						}
					}
				}
				$faset['menu']=array($iRandomCatId);
				$arProductsCatFacet=CNiyamaCatalog::GetProductListByFaset(array(),$faset,$bRefreshCache); 
				if (!empty($arProductsCatFacet['ITEMS'])){
					foreach($arProductsCatFacet['ITEMS'] as $key=>$prod){
						$arUpSailProducts[$key]=$prod;
					}
				}else{
					$arUpSailProducts=$arProductsFromCat;
				}
			}
		}
		$arUpSailProductsFinal=array();
		if (!empty($arUpSailProducts)){
			foreach($arUpSailProducts as $key=>$prod){
				$arUpSailProductsFinal[$prod['INFO']['ID']]=($prod['INFO']['PROPERTY_MARGINALITY_WEIGHT_VALUE']>0)?$prod['INFO']['PROPERTY_MARGINALITY_WEIGHT_VALUE']:1;
			}
		}
		$iUpSailProductId=false;
		$arReturn=array();
		if (!empty($arUpSailProductsFinal)){
			$iUpSailProductId=CCustomProject::GetRandomByWeight($arUpSailProductsFinal);
			$arReturn=$arUpSailProducts[$iUpSailProductId];	
		}
		return $arReturn;
	}

}
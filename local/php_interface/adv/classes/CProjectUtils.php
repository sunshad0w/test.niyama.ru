<?
/**
 * Универсальные утилиты для проектов
 * 
 * @author Sergey Leshchenko, 2014
 * @updated: 15.01.2015
 */

// Для корректной работы требуется вызвать CProjectUtils::AddCommonEventHandlers() в init.php

class CProjectUtils {
	/**
	 * @ignore
	 */
	private static $bWasEpilog = false;

	/**
	 * Добавляет обработчики событий, необходимые для корректной работы некоторых методов класса
	 *
	 * @return void
	 */
	public static function AddCommonEventHandlers() {
		static $bInited = false;
		if(!$bInited) {
			$bInited = true;
			// сброс кэша
			AddEventHandler('main', 'OnAfterGroupAdd', array(__CLASS__, 'OnAfterGroupAddHandler'));
			AddEventHandler('main', 'OnAfterGroupUpdate', array(__CLASS__, 'OnAfterGroupUpdateHandler'));
			AddEventHandler('main', 'OnGroupDelete', array(__CLASS__, 'OnGroupDeleteHandler'));

			// отложенный сброс тегированного кэша
			AddEventHandler('main', 'OnEpilog', array(__CLASS__, 'OnEpilogHandler'));
			// принудительный минимальный эпилог
			register_shutdown_function(create_function('', 'CProjectUtils::ShutdownCallback();'));

			AddEventHandler('iblock', 'OnAfterIBlockAdd', array(__CLASS__, 'OnAfterIBlockAddHandler'));
			AddEventHandler('iblock', 'OnAfterIBlockUpdate', array(__CLASS__, 'OnAfterIBlockUpdateHandler'));
			AddEventHandler('iblock', 'OnIBlockDelete', array(__CLASS__, 'OnIBlockDeleteHandler'));
		}
	}

	/**
	 * Возвращает ID активного инфоблока по символьному коду
	 * 
	 * @param string $sIBlockCode код инфоблока, для которого нужно получить ID
	 * @param string $sIBlockTypeCode тип инфоблока, для которого нужно получить ID. Необязательный параметр, по умолчанию тип проверяться не будет
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return int
	 */
	public static function GetIBlockIdByCode($sIBlockCode, $sIBlockTypeCode = '', $bRefreshCache = false) {
		$iReturnId = 0;
		$sIBlockTypeCode = trim($sIBlockTypeCode);
		$sCacheKey = $sIBlockCode.'|'.$sIBlockTypeCode.'|active';
		$sCacheEntity = 'iblock_id_by_code';
		if(!$bRefreshCache && CStaticCache::IsSetCache($sCacheKey, $sCacheEntity)) {
			$iReturnId = CStaticCache::GetCacheValue($sCacheKey, $sCacheEntity);
		} else {
			$arIBlockList = self::GetIBlocksList($bRefreshCache);
			if($arIBlockList[$sIBlockCode]) {
				if(strlen($sIBlockTypeCode)) {
					$iReturnId = isset($arIBlockList[$sIBlockCode][$sIBlockTypeCode]) ? $arIBlockList[$sIBlockCode][$sIBlockTypeCode] : 0;
				} else {
					$iReturnId = isset($arIBlockList[$sIBlockCode]) ? reset($arIBlockList[$sIBlockCode]) : 0;
				}
			}
			CStaticCache::SetCacheValue($sCacheKey, $iReturnId, $sCacheEntity, 100);
		}
		return $iReturnId;
	}
	
	/**
	 * Возвращает ID деактивированного инфоблока по символьному коду
	 * 
	 * @param string $sIBlockCode код инфоблока, для которого нужно получить ID
	 * @param string $sIBlockTypeCode тип инфоблока, для которого нужно получить ID. Необязательный параметр, по умолчанию тип проверяться не будет
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return int
	 */
	public static function GetDisabledIBlockIdByCode($sIBlockCode, $sIBlockTypeCode = '', $bRefreshCache = false) {
		$iReturnId = 0;
		$sIBlockTypeCode = trim($sIBlockTypeCode);
		$sCacheKey = $sIBlockCode.'|'.$sIBlockTypeCode.'|disabled';
		$sCacheEntity = 'iblock_id_by_code';
		if(!$bRefreshCache && CStaticCache::IsSetCache($sCacheKey, $sCacheEntity)) {
			$iReturnId = CStaticCache::GetCacheValue($sCacheKey, $sCacheEntity);
		} else {
			$arIBlockList = self::GetDisabledIBlocksList($bRefreshCache);
			if($arIBlockList[$sIBlockCode]) {
				if(strlen($sIBlockTypeCode)) {
					$iReturnId = isset($arIBlockList[$sIBlockCode][$sIBlockTypeCode]) ? $arIBlockList[$sIBlockCode][$sIBlockTypeCode] : 0;
				} else {
					$iReturnId = isset($arIBlockList[$sIBlockCode]) ? reset($arIBlockList[$sIBlockCode]) : 0;
				}
			}
			CStaticCache::SetCacheValue($sCacheKey, $iReturnId, $sCacheEntity, 100);
		}
		return $iReturnId;
	}

	/**
	 * Возвращает ID инфоблока по символьному коду
	 * 
	 * @param string $sIBlockCode код инфоблока, для которого нужно получить ID
	 * @param string $sIBlockTypeCode тип инфоблока, для которого нужно получить ID. Необязательный параметр, по умолчанию тип проверяться не будет
	 * @param bool $bGetActiveOnly возвращать ID только активных инфоблоков, по умолчанию false - будут участововать в т.ч. деактивированные инфоблоки
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return int
	 */
	public static function GetIBlockIdByCodeEx($sIBlockCode, $sIBlockTypeCode = '', $bGetActiveOnly = false, $bRefreshCache = false) {
		$iReturnId = self::GetIBlockIdByCode($sIBlockCode, $sIBlockTypeCode, $bRefreshCache);
		if(!$iReturnId && !$bGetActiveOnly) {
			$iReturnId = self::GetDisabledIBlockIdByCode($sIBlockCode, $sIBlockTypeCode, $bRefreshCache);
		}
		return $iReturnId;
	}

	/**
	 * Возвращает массив всех активных инфоблоков системы
	 *
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return array
	 */
	public static function GetIBlocksList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'active';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'iblocks_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		// iblock_id_new - системный тег добавления нового инфоблока
		// использовать тегированный кэш не будем, т.к. будет выполняться постоянный сброс при любом действии над элементами инфоблоков
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/iblocks_list/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'ACTIVE' => 'Y',
					'CHECK_PERMISSIONS' => 'N'
				);
				$dbItems = CIBlock::GetList(array('ID' => 'ASC'), $arFilter, false);
				while($arItem = $dbItems->Fetch()) {
					if(strlen($arItem['CODE'])) {
						$arReturn[$arItem['CODE']][$arItem['IBLOCK_TYPE_ID']] = $arItem['ID'];
					}
				}
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * Возвращает массив всех деактивированных инфоблоков системы
	 *
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return array
	 */
	public static function GetDisabledIBlocksList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = 'disabled';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'iblocks_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		// iblock_id_new - системный тег добавления нового инфоблока
		// использовать тегированный кэш не будем, т.к. будет выполняться постоянный сброс при любом действии над элементами инфоблоков
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/iblocks_list/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$arFilter = array(
					'ACTIVE' => 'N',
					'CHECK_PERMISSIONS' => 'N'
				);
				$dbItems = CIBlock::GetList(array('ID' => 'ASC'), $arFilter, false);
				while($arItem = $dbItems->Fetch()) {
					if(strlen($arItem['CODE'])) {
						$arReturn[$arItem['CODE']][$arItem['IBLOCK_TYPE_ID']] = $arItem['ID'];
					}
				}
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * @ignore
	 */
	public static function OnAfterIBlockAddHandler(&$arFields) {
		self::ClearIBlocksListCache();
	}

	/**
	 * @ignore
	 */
	public static function OnAfterIBlockUpdateHandler(&$arFields) {
		if(array_key_exists('CODE', $arFields)) {
			self::ClearIBlocksListCache();
		}
	}

	/**
	 * @ignore
	 */
	public static function OnIBlockDeleteHandler($iIBlockId) {
		self::ClearIBlocksListCache();
	}

	/**
	 * Очищает кэш методов GetIBlocksList и GetIBlockIdByCode
	 *
	 * @return void
	 */
	public static function ClearIBlocksListCache() {
		// без слеша в конце!
		$sTmpCachePath = '/'.__CLASS__.'/iblocks_list';
		CStaticCache::FlushEntityCacheByCachePath($sTmpCachePath);
		BXClearCache($sTmpCachePath);
		CStaticCache::FlushEntityCache('iblock_id_by_code');
	}

	/**
	 * Облегченный эпилог. Выполняет критические операции, когда не требуется подключение полного эпилога (событий, отправки e-mail писем). 
	 * Сохраняет управляемый кэш и закрывает соединение к БД. Зачастую используется в конце AJAX-обработчиков.
	 *
	 * @return void
	 */
	public static function AjaxEpilog() {
		if(!self::$bWasEpilog) {
			self::$bWasEpilog = true;
			ob_start();
			// сохранение управляемого кэша
			CCacheManager::_Finalize();
			// закроем соединение с базой
			$GLOBALS['DB']->Disconnect();
			$tmp = ob_end_clean();
		}
	}

	/**
	 * Обрабатывает параметры устанавливаемого времени жизни кэша с учетом текущего режима автокэширования. Обычно используется в компонентах.
	 *
	 * @param int $iCacheTime устанавливаемое время жизни кэша.
	 * @param string $sCacheType устанавливаемый тип кэширования: A – авто (проверяется текущий параметр автокэширования), N – никогда не кэшировать, Y – всегда кэшировать.
	 * @param int $iDef время жизни кэша по умолчанию, если в параметре iCacheTime поступят невалидные данные.
	 * @return int
	 */
	public static function GetCacheTime($iCacheTime = 0, $sCacheType = 'A', $iDef = 0) {
		$iCacheTime = intval($iCacheTime);
		$iCacheTime = $iCacheTime > 0 ? $iCacheTime : $iDef;
		$sCacheType = $sCacheType != 'Y' && $sCacheType != 'N' ? 'A' : $sCacheType;
		if($sCacheType == 'N' || ($sCacheType == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
			$iCacheTime = 0;
		}
		return $iCacheTime;
	}

	/**
	 * Создает и передает описание ошибки в CAdminException
	 *
	 * @param array $arErrors массив с описанием ошибок
	 * @param $bClearIfEmpty обнулить текущий CAdminException, если массив arErrors пустой
	 * @return void
	 */
	public static function SetAppErrors($arErrors, $bClearIfEmpty = false) {
		$arErrors = is_array($arErrors) ? $arErrors : array($arErrors);
		if(!empty($arErrors)) {
			$arMsg = array();
			foreach($arErrors as $mErrId => $sErrText) {
				$arMsg[] = array(
					'id' => $mErrId,
					'text' => $sErrText
				);
			}
			$obErr = new CAdminException($arMsg);
			$GLOBALS['APPLICATION']->ThrowException($obErr);
		} elseif($bClearIfEmpty) {
			$GLOBALS['APPLICATION']->ResetException();
		}
	}

	/**
	 * Возвращает текущее строковое описание ошибки CAdminException
	 *
	 * @param string $sDefMsg сообщение об ошибке по умолчанию
	 * @return string
	 */
	public static function GetExceptionString($sDefMsg = 'Unknown error') {
		$obTmpException = $GLOBALS['APPLICATION']->GetException();
		return $obTmpException ? $obTmpException->GetString() : $sDefMsg;
	}

	/**
	 * @ignore
	 */
	public static function GetExeptionString($sDefMsg = 'Unknown error') {
		return self::GetExceptionString($sDefMsg);
	}

	/**
	 * @ignore
	 */
	private static $sUasortBy;
	/**
	 * @ignore
	 */
	private static $iUasortDirection;

	/**
	 * @ignore
	 */
	private static function UasortCmp($a, $b) {
		return ($a[self::$sUasortBy] < $b[self::$sUasortBy] ? -1 : 1) * self::$iUasortDirection;
	}

	/**
	 * @ignore
	 */
	private static function UasortCmpStr($a, $b) {
		return (strnatcmp($a[self::$sUasortBy], $b[self::$sUasortBy]) < 1 ? -1 : 1)  * self::$iUasortDirection;
	}

	/**
	 * Сортировка многомерного массива по ключу
	 *
	 * @param array &$arResult многомерный массив, который необходимо отсортировать. Результат передается по ссылке
	 * @param string $sBy ключ многомерного массива, по которому необходимо выполнить сортировку
	 * @param string $sOrder направление сортировки: asc – по возрастанию, desc – по убыванию
	 * @param bool $bStrSort сортировать значения по алгоритму "natural ordering"
	 * @return void
	 */
	public static function DoSort(&$arResult, $sBy, $sOrder = 'asc', $bStrSort = false) {
		if(strlen($sBy)) {
			$sOrder = strtolower($sOrder);
			self::$iUasortDirection = $sOrder == 'desc' ? -1 : 1;
			self::$sUasortBy = $sBy;
			if($bStrSort) {
				uasort($arResult, array(__CLASS__, 'UasortCmp'));
			} else {
				uasort($arResult, array(__CLASS__, 'UasortCmpStr'));
			}
		}
	}

	/**
	 * Склонение числительных
	 *
	 * @param int $sINubmer числительное, для которого необходимо вернуть результат
	 * @param string $s0 вариант, если бы числительное значение было "0"
	 * @param string $s1 вариант, если бы числительное значение было "1"
	 * @param string $s2 вариант, если бы числительное значение было "2"
	 * @return string
	 */
	public static function PrintCardinalNumberRus($sINubmer, $s0 = '', $s1 = '', $s2 = '') {
		$iLength = strlen($sINubmer);
		$iDec = 0;
		$iNubmer = intval($sINubmer);
		if($iLength > 1) {
			$iNubmer = intval(substr($sINubmer, ($iLength - 1)));
			$iDec = intval(substr($sINubmer, ($iLength - 2), 1));
		}
		if($iNubmer > 4 || $iNubmer == 0 || $iDec == 1) {
			return $s0;
		} elseif($iNubmer == 1) {
			return $s1;
		} else {
			return $s2;
		}
	}

	/**
	 * Отсечение ненужных данных объекта CDBResult. Используется в компонентах для минимизации объема кэша
	 *
	 * @param CDBResult $rsItems объект CDBResult
	 * @return 
	 */
	public static function CutNavResult($rsItems) {
		$result = array();
		if($rsItems && is_object($rsItems)) {
			$result['bNavStart'] = $rsItems->bNavStart;
			$result['bShowAll'] = $rsItems->bShowAll;
			$result['NavPageCount'] = $rsItems->NavPageCount;
			$result['NavPageNomer'] = $rsItems->NavPageNomer;
			$result['NavPageSize'] = $rsItems->NavPageSize;
			$result['NavShowAll'] = $rsItems->NavShowAll;
			$result['NavRecordCount'] = $rsItems->NavRecordCount;
			$result['bFirstPrintNav'] = $rsItems->bFirstPrintNav;
			$result['PAGEN'] = $rsItems->PAGEN;
			$result['SIZEN'] = $rsItems->SIZEN;
			$result['bFromArray'] = $rsItems->bFromArray;
			$result['bFromLimited'] = $rsItems->bFromLimited;
			$result['sSessInitAdd'] = $rsItems->sSessInitAdd;
			$result['nPageWindow'] = $rsItems->nPageWindow;
			$result['nSelectedCount'] = $rsItems->nSelectedCount;
			$result['bDescPageNumbering'] = $rsItems->bDescPageNumbering;
			$result['arUserMultyFields'] = $rsItems->arUserMultyFields;
			$result['SqlTraceIndex'] = $rsItems->SqlTraceIndex;
			$result['nStartPage'] = $rsItems->nStartPage;
			$result['nEndPage'] = $rsItems->nEndPage;
		}
		return $result;
	}

	/**
	 * Возвращает ЧПУ-переменные текущей страницы
	 *
	 * @param array $arAdd переменные, которые необходимо добавить к возвращаемому результату
	 * @param string $add добавлять служебные параметры show_page_exec_time, show_include_exec_time, show_sql_stat
	 * @return array
	 */
	public static function GetSefParams($arAdd = array(), $add = 'Y') {
		static $result = false;
		if($result === false) {
			$result = array();
			$query_str = $_SERVER['QUERY_STRING'];
			// в QUERY_STRING теоретически должны быть только параметры через "&" но в БУС это не так, тут может быть и "?" :)
			// преобразуем "?" в "&"
			$query_str = str_replace('?', '&', $query_str);
			if(!empty($query_str)) {
				// получим массив параметров вместе с ЧПУшными
				$arSEF_q_params = array();
				parse_str($query_str, $arSEF_query);
				$arSEF_q_params = array_keys($arSEF_query);

				// берем параметры, которые реально в запросе заданы в виде параметров
				$arSEF_u_params = array();
				$r_uri = $_SERVER['REQUEST_URI'];
				$arURI = parse_url($r_uri);
				if(isset($arURI['query'])) {
					// заменим "?" на "&" - если умник будет ручками параметры ставить
					$uri_query = str_replace('?', '&', $arURI['query']);
					// получим массив параметров без ЧПУшных (если их ручками не написали)
					parse_str($uri_query, $arSEF_uri);
					$arSEF_u_params = array_keys($arSEF_uri);
				}
				// вычитаем два массива - расхождение будет ЧПУ-параметрами
				$result = array_diff($arSEF_q_params, $arSEF_u_params);
			}
		}
		if($add == 'Y') {
			$result[] = 'show_page_exec_time';
			$result[] = 'show_include_exec_time';
			$result[] = 'show_sql_stat';
		}

		return array_merge($result, $arAdd);
	}

	/**
	 * Возвращает массив $_GET без указанных параметров
	 *
	 * @param array $arDelParams параметры, которые необходимо удалить
	 * @return string
	 */
	public static function DeleteGetParams($arDelParams = array()) {
		$arFinalGet = $_GET;
		if(empty($arFinalGet)) {
			return '';
		}
		foreach($arDelParams as $sParam) {
			if($arFinalGet) {
				if(strpos($sParam, '[') !== false) {
					$arTmp = explode('[', $sParam);
				} else {
					$arTmp = array($sParam);
				}
				$iLastIdx = count($arTmp) - 1;
				$arGetSearch =& $arFinalGet;
				foreach($arTmp as $iCurIdx => $sParamKey) {
					$sParamKey = trim($sParamKey, ']');
					if(array_key_exists($sParamKey, $arGetSearch)) {
						if($iCurIdx == $iLastIdx) {
							unset($arGetSearch[$sParamKey]);
							break;
						}
						$arGetSearch =& $arGetSearch[$sParamKey];
					} else {
						break;
					}
				}
			}
		}

		return $arFinalGet ? trim(http_build_query($arFinalGet, '', '&')) : '';
	}

	/**
	 * Аналог CMain::GetCurPageParam(), дополнительно может удалять параметры многомерных массивов
	 *
	 * @param string $sAddParams строка параметров, которые необходимо добавить к возвращаемому результату
	 * @param array $arDelParams массив удаляемых параметров
	 * @param bool $bDelSefParams удалять из результата ЧПУ-параметры
	 * @param bool $bGetIndexPage указывает, нужно ли для индексной страницы раздела возвращать путь, заканчивающийся на "index.php". 
	 * Если значение параметра равно true, то возвращается путь с "index.php", иначе - путь, заканчивающийся на "/". По умолчанию - null
	 * @return string
	 */
	public static function GetCurPageParam($sAddParams = '', $arDelParams = array(), $bDelSefParams = false, $bGetIndexPage = null) {
		if($bDelSefParams) {
			$arDelParams = self::GetSefParams($arDelParams);
		}
		$sUrlPath = $GLOBALS['APPLICATION']->GetCurPage($bGetIndexPage);
		$sAddQueryString = self::DeleteGetParams($arDelParams);

		if(strlen($sAddQueryString)) {
			$sAddParams = $sAddParams ? $sAddParams.'&'.$sAddQueryString : $sAddQueryString;
		}

		$sUrlPath = strlen($sAddParams) ? $sUrlPath.'?'.$sAddParams : $sUrlPath;
		return $sUrlPath;
	}

	/**
	 * Возвращает размер файла строкой
	 *
	 * @param float $dSize размер файла, для которого необходимо вернуть строку
	 * @param array $arWords массив для передачи строковых обозначений единиц измерения размеров файлов
	 * @return string
	 */
	public static function GetStrFileSize($dSize, $arWords = array('#BYTE#' => ' B', '#KBYTE#' => ' KB', '#MBYTE#' => ' MB', '#GBYTE#' => ' GB')) {
		$arWords['#BYTE#'] = isset($arWords['#BYTE#']) ? $arWords['#BYTE#'] : '#BYTE#';
		$arWords['#KBYTE#'] = isset($arWords['#KBYTE#']) ? $arWords['#KBYTE#'] : '#KBYTE#';
		$arWords['#MBYTE#'] = isset($arWords['#MBYTE#']) ? $arWords['#MBYTE#'] : '#MBYTE#';
		$arWords['#GBYTE#'] = isset($arWords['#GBYTE#']) ? $arWords['#GBYTE#'] : '#GBYTE#';

		if($dSize < 1024) {
			return str_replace('#BYTE#', $arWords['#BYTE#'], $dSize.'#BYTE#');
		}

		$dSize = round($dSize / 1024);

		if($dSize < 1024) {
			return str_replace('#KBYTE#', $arWords['#KBYTE#'], $dSize.'#KBYTE#');
		}

		$dSize = round($dSize / 1024);
		if($dSize < 1024) {
			return str_replace('#MBYTE#', $arWords['#MBYTE#'], $dSize.'#MBYTE#');
		}

		$dSize = round($dSize / 1024, 2);
		return str_replace('#GBYTE#', $arWords['#GBYTE#'], $dSize.'#GBYTE#');
	}

	/**
	 * Вывод кастомного свойства страницы (используются отложенные функции)
	 *
	 * @param string $sPropName название свойства страницы, значение которого необходимо вывести
	 * @param string $sPreStr строка, которая выводится перед значением, если значение непустое
	 * @param string $sPostStr строка, которая выводится после значения, если значение непустое
	 * @param string $sDefaultVal значение по умолчанию
	 * @return void
	 */
	public static function ShowCustomPageProperty($sPropName = false, $sPreStr = '', $sPostStr = '', $sDefaultVal = '') {
		$GLOBALS['APPLICATION']->AddBufferContent(array(__CLASS__, 'GetCustomPagePropertyValue'), $sPropName, $sPreStr, $sPostStr, $sDefaultVal);
	}

	/**
	 * @ignore
	 */
	public static function GetCustomPagePropertyValue($sPropName = false, $sPreStr = '', $sPostStr = '', $sDefaultVal = '') {
		$sResult = '';
		$sDefaultVal = is_string($sDefaultVal) ? $sDefaultVal : '';
		if(!empty($sPropName)) {
			$sResult = $GLOBALS['APPLICATION']->GetProperty($sPropName);
			$sResult = is_string($sResult) ? $sResult : '';
		}
		$sResult = strlen($sResult) ? $sResult : $sDefaultVal;
		
		if(strlen($sResult)) {
			$sResult = $sPreStr.$sResult.$sPostStr;
		}
		return $sResult;
	}

	/**
	 * Возращает true если ОС веб-окружения является Windows
	 *
	 * @return true
	 */
	public static function IsWindows() {
		static $bWin = 0;
		if($bWin === 0) {
			$bWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
		}
		return $bWin;
	}

	/**
	 * Выполнение команды в фоновом режиме
	 *
	 * @param string $sCmd shell-команда, которую необходимо выполнить в фоновом режиме
	 * @return void
	 */
	public static function ExecInBackground($sCmd) {
		if(self::IsWindows()) {
			pclose(popen('start /B '.$sCmd, 'r'));
		} else {
			exec($sCmd.' > /dev/null &');
		}
	}

	/**
	 * Очистка текущего кэша опций сайта (COption)
	 *
	 * @return void
	 */
	public static function ClearOptionsCache() {
		$GLOBALS['MAIN_OPTIONS'] = array();
		$GLOBALS['CACHE_MANAGER']->Clean('b_option');
	}

	/**
	 * Очистка массива от пустых значений
	 *
	 * @param array $arArray массив, который необходимо очистить от пустых значений
	 * @param bool $bCheckPositiveInt приводить значения к целочисленному типу и проверять значения на позитивность
	 * @return array
	 */
	public static function ClearArray($arArray, $bCheckPositiveInt = false) {
		$arReturn = is_array($arArray) ? $arArray : array();
		if(!empty($arReturn)) {
			foreach($arReturn as $mKey => $mVal) {
				if($bCheckPositiveInt) {
					$mVal = intval($mVal);
					if($mVal > 0) {
						$arReturn[$mKey] = $mVal;
					} else {
						unset($arReturn[$mKey]);
					}
				} else {
					if($mVal == '') {
						unset($arReturn[$mKey]);
					}
				}
			}
		}
		return $arReturn;
	}

	/**
	 * По пути файла возвращает массив, подобный возвращаемому массиву методом CFile::GetByID()
	 *
	 * @param string $sFilePath путь к файлу, для которого необходимо вернуть результат
	 * @param bool $bCheckImage проверять, чтобы тип файла был изображением, по умолчанию тип проверяется
	 * @return array
	 */
	public static function GetFileArrayByPath($sFilePath, $bCheckImage = true) {
		static $sUploadDirName;
		if(!$sUploadDirName) {
			$sUploadDirName = COption::GetOptionString('main', 'upload_dir', 'upload');
		}
		$arReturn = array();
		if(strlen($sFilePath)) {
			$arTmpFileArray = CFile::MakeFileArray($sFilePath);
			if($arTmpFileArray['name']) {
				$bIsImage = CFile::IsImage($arTmpFileArray['name']);
				if(!$bCheckImage || $bIsImage) {
					$arPathInfo = pathinfo($arTmpFileArray['tmp_name']);
					$sSubDir = $arPathInfo['dirname'];
					$sFileSrc = $arTmpFileArray['tmp_name'];
					if(strpos($sSubDir, $_SERVER['DOCUMENT_ROOT']) === 0) {
						// отсечем из пути DOCUMENT_ROOT
						$sSubDir = trim(substr($sSubDir, strlen($_SERVER['DOCUMENT_ROOT'])), '/');
						$sFileSrc = '/'.ltrim(substr($sFileSrc, strlen($_SERVER['DOCUMENT_ROOT'])), '/');
					}
					if(strpos($sSubDir, $sUploadDirName) === 0) {
						// отсечем из пути папку upload, если файл находится в ней
						$sSubDir = trim(substr($sSubDir, strlen($sUploadDirName)), '/');
					}

					$arReturn = array(
						'MODULE_ID' => 'main',
						'FILE_NAME' => $arTmpFileArray['name'],
						'ORIGINAL_NAME' => $arTmpFileArray['name'],
						'FILE_SIZE' => $arTmpFileArray['size'],
						'CONTENT_TYPE' => $arTmpFileArray['type'],
						'SRC' => $sFileSrc,
						'SUBDIR' => $sSubDir,
						'WIDTH' => '',
						'HEIGHT' => '',
					);
					if($bIsImage) {
						$arImgSize = getimagesize($arTmpFileArray['tmp_name']);
						if(!empty($arImgSize)) {
							$arReturn['WIDTH'] = $arImgSize[0];
							$arReturn['HEIGHT'] = $arImgSize[1];
						} else {
							if($bCheckImage) {
								$arReturn = array();
							}
						}
					}
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Генерация пароля
	 *
	 * @param int $iLength длина генерируемого пароля, по умолчанию 6 символов
	 * @return string
	 */
	public static function GenPassword($iLength = 6) {
		$sAlphabet = 'qwertyuiop12345asdfghjklzxcvbnm67890';
		$sPassword = substr(str_shuffle($sAlphabet), 0, $iLength);
		return $sPassword;
	}

	/**
	 * Возвращает массив всех групп пользователей сайта. Ключами массива являются ID групп
	 *
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return 
	 */
	public static function GetUsersGroups($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'users_groups';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/users_groups/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			$dbItems = CGroup::GetList(
				$sBy = 'id',
				$sOrder = 'asc',
				array(),
				'N'
			);
			while($arItem = $dbItems->Fetch()) {
				$arItem['~STRING_ID'] = $arItem['STRING_ID'];
				$arItem['STRING_ID'] = strtoupper($arItem['STRING_ID']);
				$arReturn[$arItem['ID']] = $arItem;
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * Возвращает группу пользователей сайта по ее символьному коду
	 *
	 * @param string $sCode символьный код группы, для которой необходимо вернуть результат
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return array
	 */
	public static function GetUsersGroupByCode($sCode = '', $bRefreshCache = false) {
		$arReturn = array();
		$sCode = strtoupper(trim($sCode));
		if(!strlen($sCode)) {
			return $arReturn;
		}
		$sCacheKey = $sCode.'|';
		$sCacheEntity = 'user_group_by_code';
		if(!$bRefreshCache && CStaticCache::IsSetCache($sCacheKey, $sCacheEntity)) {
			$arReturn = CStaticCache::GetCacheValue($sCacheKey, $sCacheEntity);
		} else {
			$arGroupsList = self::GetUsersGroups($bRefreshCache);
			if($arGroupsList) {
				foreach($arGroupsList as $arItem) {
					if($arItem['STRING_ID'] == $sCode) {
						$arReturn = $arItem;
						break;
					}
				}
			}
			CStaticCache::SetCacheValue($sCacheKey, $arReturn, $sCacheEntity, 100);
		}
		return $arReturn;
	}

	/**
	 * Проверяет, входит ли текущий пользователь в заданные символьными кодами группы. Возвращает массив совпавших групп
	 *
	 * @param mixed $mCodes символьный код группы или массив групп, принадлежность к которым необходимо проверить текущего пользователя
	 * @param bool $bCheckActive проверять активность групп
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return array
	 */
	public static function CheckUserGroupsByCode($mCodes, $bCheckActive = true, $bRefreshCache = false) {
		$arReturn = array();
		$arCodes = is_array($mCodes) ? $mCodes : array($mCodes);
		if($GLOBALS['USER'] && $arCodes) {
			$arUserGroups = $GLOBALS['USER']->GetUserGroupArray();
			foreach($arCodes as $sCode) {
				$arGroup = self::GetUsersGroupByCode($sCode);
				if($arGroup && in_array($arGroup['ID'], $arUserGroups)) {
					if(!$bCheckActive || $arGroup['ACTIVE'] == 'Y') {
						$arReturn[$sCode] = $arGroup;
					}
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Возвращает логин по ID пользователя
	 *
	 * @param int $iUserId ID пользователя, для которого необходимо получить логин
	 * @param bool $bRefreshCache перезаписывать ли валидный кэш. Необязательный параметр, по умолчанию перезапись валидного кэша выполняться не будет
	 * @return string
	 */
	public static function GetUserLoginById($iUserId, $bRefreshCache = false) {
		$sReturn = '';
		$iUserId = intval($iUserId);
		if($iUserId < 1) {
			return $sReturn;
		}
		$sCacheKey = $iUserId.'|';
		$sCacheEntity = 'user_login_by_id';
		if(!$bRefreshCache && CStaticCache::IsSetCache($sCacheKey, $sCacheEntity)) {
			$sReturn = CStaticCache::GetCacheValue($sCacheKey, $sCacheEntity);
		} else {
			$dbItems = CUser::GetList(
				$sBy = 'id',
				$sOrder = 'asc',
				array(
					'ID_EQUAL_EXACT' => $iUserId
				),
				array(
					'FIELDS' => array('LOGIN')
				)
			);
			if($arItem = $dbItems->Fetch()) {
				$sReturn = $arItem['LOGIN'];
			}
			CStaticCache::SetCacheValue($sCacheKey, $sReturn, $sCacheEntity, 100);
		}
		return $sReturn;
	}

	/**
	 * @ignore
	 */
	public static function OnAfterGroupAddHandler(&$arFields) {
		self::ClearUsersCache();
	}

	/**
	 * @ignore
	 */
	public static function OnAfterGroupUpdateHandler($iGroupId, &$arFields) {
		self::ClearUsersCache();
	}

	/**
	 * @ignore
	 */
	public static function OnGroupDeleteHandler($iGroupId) {
		self::ClearUsersCache();
	}

	/**
	 * Очистка кэша метода GetUsersGroupByCode
	 *
	 * @return void
	 */
	public static function ClearUsersCache() {
		// без слеша в конце!
		$sTmpCachePath = '/'.__CLASS__.'/users_groups';
		CStaticCache::FlushEntityCacheByCachePath($sTmpCachePath);
		BXClearCache($sTmpCachePath);
		CStaticCache::FlushEntityCache('user_group_by_code');
	}

	/**
	 * @ignore
	 */
	public static function Query($sQuery) {
		$dbResult = $GLOBALS['DB']->Query($sQuery, false);
		return $dbResult;
	}

	/**
	 * Конвертирует строковое значение даты, которое может разобрать класс DateTime, в формат, пригодный для передачи в фильтр выборки
	 *
	 * @param string $sDateTime строковое значение даты, которое сможет разобрать класс CDateTime
	 * @param bool $bSetDefaultNow если первый параметр не задан, то брать текущую дату
	 * @return string
	 */
	public static function GetDateTimeFieldFilter($sDateTime = '', $bSetDefaultNow = true) {
		$sReturn = '';
		$sDateTime = trim($sDateTime);
		if(strlen($sDateTime)) {
			$obTmpDate = date_create($sDateTime);
			$sReturn = $obTmpDate ? $obTmpDate->Format($GLOBALS['DB']->DateFormatToPhp(FORMAT_DATETIME)) : '';
		} elseif($bSetDefaultNow) {
			$sReturn = date($GLOBALS['DB']->DateFormatToPhp(FORMAT_DATETIME));
		}
		return $sReturn;
	}

	/**
	 * @ignore
	 */
	private static $arDefferredClearTags = array();

	/**
	 * Добавление тега для отложенной очистки управляемого кэша. 
	 * Метод отложенной очистки кэша может быть полезен, например, в скриптах импорта, чтобы операция сброса кэша инициировалась не на каждой операции изменения записи, 
	 * а лишь один раз в конце скрипта. Метод отложенной очистки управляемого кэша может применяться для тех сущностей, 
	 * для которых автоматический сброс кэша не предусмотрен в ядре, например, для highloadblock
	 *
	 * @param string $sCacheTag тег, для которого необходимо очистить кэш с отложенной процедурой очистки
	 * @return void
	 */
	public static function AddDefferredClearTag($sCacheTag) {
		if(!isset(self::$arDefferredClearTags[$sCacheTag])) {
			self::$arDefferredClearTags[$sCacheTag] = $sCacheTag;
		}
	}

	/**
	 * Выполнение отложенной очистки управляемого (тегированного) кэша
	 *
	 * @return void
	 */
	public static function ClearTagCacheDefferred() {
		if(self::$arDefferredClearTags) {
			if(defined('BX_COMP_MANAGED_CACHE')) {
				foreach(self::$arDefferredClearTags as $sCacheTag) {
					$GLOBALS['CACHE_MANAGER']->ClearByTag($sCacheTag);
				}
			}
		}
		self::$arDefferredClearTags = array();
	}

	/**
	 * @ignore
	 */
	public static function OnEpilogHandler() {
		self::$bWasEpilog = true;
		self::ClearTagCacheDefferred();
	}

	/**
	 * @ignore
	 */
	public static function ShutdownCallback() {
		// ! поcле OnAfterEpilog разрывается соединение с БД !
		if(!self::$bWasEpilog) {
			self::ClearTagCacheDefferred();
			self::AjaxEpilog();
		}
	}

	/**
	 * Форматирование числа, устанавливает заданный разделитель тысяч и десятичный знак
	 *
	 * @param double $dFormatNum число, которое необходимо отформатировать
	 * @param string $sThousandDelimiter разделитель тысяч
	 * @param string $sDecimalPoint десятичный разделитель
	 * @return string
	 */
	public static function FormatNum($dFormatNum, $sThousandDelimiter = ' ', $sDecimalPoint = '.') {
		$sReturn = '';
		$dFormatNum = floatval($dFormatNum);
		$arTmp = explode('.', $dFormatNum);
		$sFract = $arTmp[1];
		$sReturn = number_format($arTmp[0], 0, '.', ' ');
		if($sThousandDelimiter != ' ') {
			$sReturn = str_replace(' ', $sThousandDelimiter, $sReturn);
		}
		if(intval($sFract)) {
			$sReturn .= $sDecimalPoint.$sFract;
		}
		return $sReturn;
	}

	/**
	 * Обработчик для индексации файлов с включаемыми областями (вводится дополнительный файл .search.php)
	 *
	 * @param array $arFields массив полей, передаваемый из события BeforeIndex
	 * @return array
	 */
	public static function IndexIncFilesHandler($arFields) {
		static $arStaticCache = array();
		if($arFields['MODULE_ID'] == 'main') {
			$arPath = pathinfo($arFields['URL']);
			if($arPath['dirname'] && $arPath['basename']) {
				if(isset($arStaticCache[$arPath['dirname']])) {
					$arIncSearchFiles = $arStaticCache[$arPath['dirname']];
				} else {
					$arIncSearchFiles = array();
					$sIncSearchFilePathAbs = $_SERVER['DOCUMENT_ROOT'].rtrim($arPath['dirname'], '/').'/.search.php';
					if(file_exists($sIncSearchFilePathAbs)) {
						try {
							include($sIncSearchFilePathAbs);
						} catch(exception $e) {
							// nothing
						}
						$arIncSearchFiles = is_array($arIncSearchFiles) ? $arIncSearchFiles : array();
					}
					$arStaticCache[$arPath['dirname']] = $arIncSearchFiles;
					if(count($arStaticCache) > 50) {
						$arStaticCache = array_slice($arStaticCache, 1, null, true);
					}
				}
				if($arIncSearchFiles[$arPath['basename']] && is_array($arIncSearchFiles[$arPath['basename']])) {
					$arModifTimes = array();
					$arModifTimes[] = MakeTimeStamp($arFields['DATE_CHANGE']);
					foreach($arIncSearchFiles[$arPath['basename']] as $sIncFilePath) {
						$sIncFilePathAbs = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim($sIncFilePath, '/');
						if(file_exists($sIncFilePathAbs)) {
							$sFileContent = file_get_contents($sIncFilePathAbs);
							$arFields['BODY'] .= "\n".CSearch::KillTags($sFileContent);
							$arModifTimes[] = filemtime($sIncFilePathAbs);
						}
					}
					$arFields['DATE_CHANGE'] = date('d.m.Y H:i:s', max($arModifTimes));
				}
			}
		}

		return $arFields;
	}

	/**
	 * Распаковка zip-архива
	 *
	 * @param string $sZipFilePath абсолютный путь к файлу zip-архива
	 * @param string $sTargetDirPath абсолютный путь к папке, куда следует распаковать архив
	 * @param bool $bReturnFilesInfo возвращать ли информацию о файлах архива
	 * @return array
	 */
	public static function UnzipFile($sZipFilePath, $sTargetDirPath, $bReturnFilesInfo = false) {
		$arErrors = array();
		$arReturn = array(
			'SUCCESS' => false
		);
		if(!strlen($sZipFilePath)) {
			$arErrors['EMPTY_FILE_NAME'] = 'Имя архива не указано';
		}
		if(!strlen($sTargetDirPath)) {
			$arErrors['EMPTY_TARGET_PATH'] = 'Путь для распаковки архива не указан';
		}
		if(empty($arErrors)) {
			if(file_exists($sZipFilePath)) {
				$rsFile = zip_open($sZipFilePath);
				if($rsFile && is_resource($rsFile)) {
					$sTargetDirPath = rtrim($sTargetDirPath, '/').'/';
					CheckDirPath($sTargetDirPath);
					$arTmpFileErrors = array();
					while($mZipEntry = zip_read($rsFile)) {
						if(zip_entry_open($rsFile, $mZipEntry, 'r')) {
							$sEntryName = zip_entry_name($mZipEntry);
							$sEntryPath = $sTargetDirPath.$sEntryName;
							if(strrpos($sEntryPath, '/') == (strlen($sEntryPath) - 1)) {
								// в архиве есть директория - создаем
								CheckDirPath($sEntryPath);
							} else {
								$mResult = file_put_contents($sEntryPath, zip_entry_read($mZipEntry, zip_entry_filesize($mZipEntry)));
								if($mResult === false) {
									$arTmpFileErrors[] = $sEntryPath;
								} else {
									if($bReturnFilesInfo) {
										$arReturn['FILES'][] = $sEntryPath;
									}
								}
							}
							zip_entry_close($mZipEntry);
						}
					}
					zip_close($rsFile);
					if(!empty($arTmpFileErrors)) {
						$arErrors['UNZIP_FAILS_ERROR'] = 'Ошибка распаковки файлов: '."\n".implode("\n", $arTmpFileErrors);
						if($bReturnFilesInfo) {
							$arReturn['ERR_FILES'] = $arTmpFileErrors;
						}
					}
				} else {
					$arErrors['ZIP_NOT_OPEN'] = 'Ошибка открытия файла архива: '.$sZipFilePath;					
				}
			} else {
				$arErrors['FILE_NOT_EXISTS'] = 'Архив не найден: '.$sZipFilePath;
			}
		}

		if(!empty($arErrors)) {
			self::SetAppErrors($arErrors);
		} else {
			$arReturn['SUCCESS'] = true;
		}

		return $arReturn;
	}

	/**
	 * Находится ли проверяемое время в заданном промежутке
	 *
	 * @param int $iCheckTimeSec метка времени, которую необходимо проверить на вхождение в заданный промежуток
	 * @param int $iPeriodLeftTimeSec метка времени, задающая левую границу промежутка
	 * @param int $iPeriodRightTimeSec метка времени, задающая правую границу промежутка
	 * @return bool
	 */
	public static function IsTimeInPeriod($iCheckTimeSec, $iPeriodLeftTimeSec, $iPeriodRightTimeSec) {
		$bInPeriod = false;
		if($iPeriodLeftTimeSec <= $iPeriodRightTimeSec) {
			// если задан промежуток, например: 6:00 - 12:00
			if($iCheckTimeSec >= $iPeriodLeftTimeSec && $iCheckTimeSec <= $iPeriodRightTimeSec) {
				$bInPeriod = true;
			}
		} else {
			// если задан промежуток, например: 22:00 - 6:00
			if($iCheckTimeSec >= $iPeriodLeftTimeSec || $iCheckTimeSec <= $iPeriodRightTimeSec) {
				$bInPeriod = true;
			}
		}
		return $bInPeriod;
	}

	/**
	 * Преобразовывает строковое значение к числовому
	 *
	 * @param string $sVal строковое значение, которое необходимо преобразовать
	 * @return float
	 */
	public static function Str2Double($sVal) {
		$dReturn = str_replace(',', '.', $sVal);
		$dReturn = preg_replace('/[^\d\-\.]/i', '', $dReturn);
		return doubleval($dReturn);
	}
}

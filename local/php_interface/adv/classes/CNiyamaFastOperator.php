<?
/**
 * Web-сервисы системы FastOperator Нияма
 *
 * @author Sergey Leshchenko, 2014
 * @updated: 14.07.2015
 */

class CNiyamaFastOperator {
	private $obSoapService = null;
	private $bLogErrors = false;
	private $obCustomLog = null;

	public function __construct($arConnectParams = array(), $bLogErrors = true) {
		if($bLogErrors) {
			$this->bLogErrors = true;
		}

		$arSoapParams = $this->ExtendConnectParams($arConnectParams);
		$this->obSoapService = new CSoapServices($arSoapParams);

		$bConnected = true;
		if(!$this->obSoapService->WasInited()) {
			$bConnected = false;
		} elseif(!$this->IsConnectionSuccess()) {
			$bConnected = false;
		}
		if(!$bConnected) {
			// если по основному адресу не полчилось подключиться, то пробуем через резервный
			$this->LogError(
				array(
					'type' => 'WARNING',
					'msg' => 'Connection failed',
					'arSoapParams' => $arSoapParams,
				)
			);
			// сбросим исключение по предыдущей попытке коннекта
			$GLOBALS['APPLICATION']->ResetException();
			$mDefVal = defined('NIYAMA_FO_SERVICE_WSDL_RESERVE') ? NIYAMA_FO_SERVICE_WSDL_RESERVE : '';
			$arSoapParams['SOAP_WSDL'] = isset($arConnectParams['SOAP_WSDL_RESERVE']) ? $arConnectParams['SOAP_WSDL_RESERVE'] : $mDefVal;
			$this->obSoapService = new CSoapServices($arSoapParams);
		}
	}

	private function LogError($mLogData) {
		if($this->bLogErrors) {
			if(!$this->obCustomLog) {
				$this->obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/fast_operator_err_'.date('d_m_Y').'.log');
			}
			$this->obCustomLog->Log($mLogData);
		}
	}

	private function ExtendConnectParams($arParams) {
		$arReturn = array();
		$arParams = is_array($arParams) ? $arParams : array();

		$mDefVal = defined('NIYAMA_FO_SERVICE_ENCODING') ? NIYAMA_FO_SERVICE_ENCODING : 'UTF-8';
		$arReturn['SOAP_ENCODING'] = isset($arParams['SOAP_ENCODING']) ? $arParams['SOAP_ENCODING'] : $mDefVal;

		$mDefVal = defined('NIYAMA_FO_SERVICE_WSDL') ? NIYAMA_FO_SERVICE_WSDL : '';
		$arReturn['SOAP_WSDL'] = isset($arParams['SOAP_WSDL']) ? $arParams['SOAP_WSDL'] : $mDefVal;

		$mDefVal = defined('NIYAMA_FO_SERVICE_LOGIN') ? NIYAMA_FO_SERVICE_LOGIN : '';
		$arReturn['SOAP_LOGIN'] = isset($arParams['SOAP_LOGIN']) ? $arParams['SOAP_LOGIN'] : $mDefVal;

		$mDefVal = defined('NIYAMA_FO_SERVICE_PASSWORD') ? NIYAMA_FO_SERVICE_PASSWORD : '';
		$arReturn['SOAP_PASSWORD'] = isset($arParams['SOAP_PASSWORD']) ? $arParams['SOAP_PASSWORD'] : $mDefVal;

		$mDefVal = defined('NIYAMA_FO_SERVICE_ENABLE_WSDL_CACHE') ? NIYAMA_FO_SERVICE_ENABLE_WSDL_CACHE : true;
		$arReturn['SOAP_ENABLE_WSDL_CACHE'] = isset($arParams['SOAP_ENABLE_WSDL_CACHE']) ? $arParams['SOAP_ENABLE_WSDL_CACHE'] : $mDefVal;

		$mDefVal = defined('NIYAMA_FO_SERVICE_CONNECTION_TIMEOUT') ? NIYAMA_FO_SERVICE_CONNECTION_TIMEOUT : 10;
		$arReturn['SOAP_CONNECTION_TIMEOUT'] = isset($arParams['SOAP_CONNECTION_TIMEOUT']) ? intval($arParams['SOAP_CONNECTION_TIMEOUT']) : $mDefVal;
		
		return $arReturn;
	}

	public function GetSoapService() {
		return $this->obSoapService;
	}

	public function IsClientInited() {
		$obSoapService = $this->GetSoapService();
		return $obSoapService->WasInited();
	}

	public function IsConnectionSuccess() {
		$bReturn = false;
		$arErr = array();
		$obSoapService = $this->GetSoapService();
		if($this->IsClientInited()) {
			$arRequestParams = array();
			$arTmpResult = $obSoapService->Call('GetVersion', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif(isset($arTmpResult['BODY']->GetVersionResult)) {
				//$sReturn = $arTmpResult['BODY']->GetVersionResult;
				$bReturn = true;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);
		}
		return $bReturn;
	}

	public function ThrowException($arErr) {
		CProjectUtils::SetAppErrors($arErr);
	}

	//
	// Выполняет запрос к веб-сервису, возвращает xml меню блюд
	//
	public function GetMenu($arRequestParams = false) {
		$sReturn = '';
		$arErr = array();
		if($this->IsClientInited()) {
			$obSoapService = $this->GetSoapService();

			$arTmpResult = $obSoapService->Call('GetMenu', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif($arTmpResult['BODY'] && $arTmpResult['BODY']->GetMenuResult && strlen($arTmpResult['BODY']->GetMenuResult)) {
				$sReturn = $arTmpResult['BODY']->GetMenuResult;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $sReturn;
	}

	public function GetMenuArray($arParams) {
		$arReturn = array();
		$sResultXml = $this->GetMenu($arParams);
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml);
			if($obXmlContent) {
				// DeletedCodes
				$arReturn['DeletedCodes'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/Menu/DeletedCodes/Item');
				foreach($arTmpXmlItems as $obXmlItem) {
					$sTmpVal = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code'));
					if(strlen($sTmpVal)) {
						$arReturn['DeletedCodes'][] = $sTmpVal;
					}
				}
				// Category + Category Items
				$arReturn['Category'] = array();
				$arReturn['Items'] = array();
				$arTmpPath = array(
					'/Menu/Category',
					'/Menu/Category/Category',
				);
				foreach($arTmpPath as $sTmpXpathVal) {
					$arTmpXmlItems = $obXmlContent->Xpath($sTmpXpathVal);
					foreach($arTmpXmlItems as $obXmlItem) {
						$sTmpCategoryCode = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code'));
						if(strlen($sTmpCategoryCode)) {
							$arReturn['Category'][] = array(
								'Code' => $sTmpCategoryCode,
								'ParentCode' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'ParentCode')),
								'Level' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Level')),
								'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Name')),
								'Sort' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Sort')),
								'Color' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Color')),
							);
						}
						$arTmpXmlItems2 = $obXmlItem->Xpath('Items/Item');
						foreach($arTmpXmlItems2 as $obXmlItem2) {
							$sTmpItemCode = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code'));
							if(strlen($sTmpItemCode)) {
								$arReturn['Items'][] = array(
									'Code' => $sTmpItemCode,
									'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Name')),
									'Price' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Price')),
									'Size' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Size'))),
									'NameShort' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'NameShort')),
									'UnitName' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'UnitName')),
									'Weight' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Weight')),
									'Energy' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Energy')),
									'Sort' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Sort')),
									'Recommended' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Recommended')),
									'_category_code_' => $sTmpCategoryCode
								);
							}
						}
					}
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает информацию о пользователе
	//
	public function GetCustomer($sUserLogin) {
		$sReturn = '';
		if(strlen($sUserLogin)) {
			if($this->IsClientInited()) {
				$obSoapService = $this->GetSoapService();
				$arRequestParams = array(
					array(
						'Login' => $sUserLogin
					)
				);
				$arTmpResult = $obSoapService->Call('GetCustomer', $arRequestParams, false);
				if($arTmpResult['ERROR']) {
					$arErr = $arTmpResult['ERROR'];
				} elseif(isset($arTmpResult['BODY']->GetCustomerResult) && strlen($arTmpResult['BODY']->GetCustomerResult)) {
					$sReturn = $arTmpResult['BODY']->GetCustomerResult;
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'SOAP_ERROR',
					'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
				);
			}
			if($arErr) {
				$this->ThrowException($arErr);

				$this->LogError(
					array(
						'method' => __FUNCTION__,
						'arErr' => $arErr,
						'sUserLogin' => $sUserLogin,
						'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
						'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
					)
				);

			}
		}
		return $sReturn;
	}

	public function GetCustomerArray($sUserLogin) {
		$arReturn = array();
		$sResultXml = $this->GetCustomer($sUserLogin);
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml, null, LIBXML_NOCDATA);
			if($obXmlContent) {
				$arTmpXmlItems = $obXmlContent->Xpath('/Customer');
				foreach($arTmpXmlItems as $obXmlRootItem) {
					$arTmp['Customer'] = array(
						'Code' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'Code')),
						'FIO' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'FIO')),
						'PersonType' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'PersonType')),
						'OrderCount' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'OrderCount'))),
						'OrderSum' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'OrderSum'))),
						'LastOrder' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'LastOrder')),
						'OrdersDiscount' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlRootItem, 'OrdersDiscount'))),
					);
					$arTmp['Addresses'] = array();
					$arTmpXmlItems = $obXmlRootItem->Xpath('Addresses/Address');
					foreach($arTmpXmlItems as $obXmlItem) {
						$arTmp['Addresses'][] = array(
							'CityCode' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'CityCode')),
							'StationName' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'StationName')),
							'StreetName' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'StreetName')),
							'House' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'House')),
							'Corpus' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Corpus')),
							'Building' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Building')),
							'Flat' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Flat')),
							'Porch' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Porch')),
							'Floor' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Floor')),
							'DoorCode' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DoorCode')),
							'Room' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Room')),
							'Office' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Office')),
							'Remark' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark')),
						);
					}

					$arTmp['Phones'] = array();
					$arTmpXmlItems = $obXmlRootItem->Xpath('Phones/Phone');
					foreach($arTmpXmlItems as $obXmlItem) {
						$arTmp['Phones'][] = array(
							'Number' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Number')),
							'Remark' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark')),
						);
					}
					$arReturn[] = $arTmp;
				}

				$sUserLogin = trim($sUserLogin);
				if(strlen($sUserLogin) && $arReturn) {
					// запрос конкретного юзера
					$arReturn = reset($arReturn);
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Редактирование/добавление пользователя
	 *	$arFields = array(
	 *		'Login' => 'user@domain.name',
	 *		'Pwd' => 'password',
	 *		'FIO' => 'ФИО',
	 *		'PersonType' => 'PersonType',
	 *		'Addresses' => array(
	 *			array(
	 *				'CityName' => 'город',
	 *				'StationName' => 'станция метро',
	 *				'StreetName' => 'улица',
	 *				'House' => 'дом',
	 *				'Corpus' => 'корпус',
	 *				'Building' => 'строение',
	 *				'Flat' => 'квартира',
	 *				'Porch' => 'подъезд',
	 *				'Floor' => 'этаж',
	 *				'DoorCode' => 'код домофона',
	 *				'Room' => 'комната',
	 *				'Office' => 'офис',
	 *				'Remark' => 'комментарий'
	 *			),
	 *		),
	 *		'Phones' => array(
	 *			array(
	 *				'Code' => '000',
	 *				'Number' => '1234567890',
	 *				'Remark' => 'комментарий'
	 *			),
	 *			array(
	 *				'Code' => '',
	 *				'Number' => '',
	 *				'Remark' => ''
	 *			),
	 *		)
	 *	)
	 * return bool
	 */
	public function EditCustomer($arFields) {
		$bReturn = false;
		$arErr = $this->CheckCustomerFieldsXml($arFields);
		if(empty($arErr)) {
			if($this->IsClientInited()) {
				$obSoapService = $this->GetSoapService();
				$arRequestParams = array(
					array(
						'XML' => $this->GetCustomerFieldsXml($arFields)
					)
				);
				$arTmpResult = $obSoapService->Call('EditCustomer', $arRequestParams, false);
				if($arTmpResult['ERROR']) {
					$arErr = $arTmpResult['ERROR'];
				} elseif($arTmpResult['BODY'] && isset($arTmpResult['BODY']->EditCustomerResult)) {
					$bReturn = $arTmpResult['BODY']->EditCustomerResult ? true : false;
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'SOAP_ERROR',
					'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
				);
			}
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'arFields' => $arFields,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $bReturn;
	}

	protected function CheckCustomerFieldsXml(&$arFields) {
		$arReturn = array();
		if(!isset($arFields['Login']) || !strlen(trim($arFields['Login']))) {
			$arReturn['LOGIN_EMPTY'] = 'Не задан e-mail (логин)';
		} else {
			$arFields['Login'] = trim($arFields['Login']);
		}
		if($arFields['Login'] && !check_email($arFields['Login'])) {
			$arReturn['LOGIN_WRONG'] = 'Некорректно задан e-mail (логин)';
		}

		if(isset($arFields['Pwd']) && !strlen(trim($arFields['Pwd']))) {
			$arReturn['PWD_EMPTY'] = 'Не задан пароль';
		} elseif(isset($arFields['Pwd'])) {
			$arFields['Pwd'] = trim($arFields['Pwd']);
		}
		return $arReturn;
	}

	protected function GetCustomerFieldsXml($arFields) {
		$sReturn = '';
		$arTmp = array();
		if(isset($arFields['Login'])) {
			$arTmp[] = 'Login="'.htmlspecialcharsbx($arFields['Login']).'"';
		}
		if(isset($arFields['Pwd'])) {
			$arTmp[] = 'Pwd="'.htmlspecialcharsbx($arFields['Pwd']).'"';
		}
		if(isset($arFields['FIO'])) {
			$arTmp[] = 'FIO="'.htmlspecialcharsbx($arFields['FIO']).'"';
		}
		if(isset($arFields['PersonType'])) {
			$arTmp[] = 'PersonType="'.htmlspecialcharsbx($arFields['PersonType']).'"';
		}
		$sReturn .= '<Customer '.implode(' ', $arTmp).'>';

		if($arFields['Addresses']) {
			$sReturn .= '<Addresses>';
			foreach($arFields['Addresses'] as $arAddrFields) {
				$arTmp = array();
				if(isset($arAddrFields['CityName'])) {
					$arTmp[] = 'CityName="'.htmlspecialcharsbx($arAddrFields['CityName']).'"';
				}
				if(isset($arAddrFields['StationName'])) {
					$arTmp[] = 'StationName="'.htmlspecialcharsbx($arAddrFields['StationName']).'"';
				}
				if(isset($arAddrFields['StreetName'])) {
					$arTmp[] = 'StreetName="'.htmlspecialcharsbx($arAddrFields['StreetName']).'"';
				}
				if(isset($arAddrFields['House'])) {
					$arTmp[] = 'House="'.htmlspecialcharsbx($arAddrFields['House']).'"';
				}
				if(isset($arAddrFields['Corpus'])) {
					$arTmp[] = 'Corpus="'.htmlspecialcharsbx($arAddrFields['Corpus']).'"';
				}
				if(isset($arAddrFields['Building'])) {
					$arTmp[] = 'Building="'.htmlspecialcharsbx($arAddrFields['Building']).'"';
				}
				if(isset($arAddrFields['Flat'])) {
					$arTmp[] = 'Flat="'.htmlspecialcharsbx($arAddrFields['Flat']).'"';
				}
				if(isset($arAddrFields['Porch'])) {
					$arTmp[] = 'Porch="'.htmlspecialcharsbx($arAddrFields['Porch']).'"';
				}
				if(isset($arAddrFields['Floor'])) {
					$arTmp[] = 'Floor="'.htmlspecialcharsbx($arAddrFields['Floor']).'"';
				}
				if(isset($arAddrFields['DoorCode'])) {
					$arTmp[] = 'DoorCode="'.htmlspecialcharsbx($arAddrFields['DoorCode']).'"';
				}
				if(isset($arAddrFields['Room'])) {
					$arTmp[] = 'Room="'.htmlspecialcharsbx($arAddrFields['Room']).'"';
				}
				if(isset($arAddrFields['Office'])) {
					$arTmp[] = 'Office="'.htmlspecialcharsbx($arAddrFields['Office']).'"';
				}
				$sReturn .= '<Addresses '.implode(' ', $arTmp).'>';
				if(isset($arAddrFields['Remark'])) {
					$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arAddrFields['Remark']).']]></Remark>';
				}
				$sReturn .= '</Addresses>';
			}
			$sReturn .= '</Addresses>';
		} else {
			$sReturn .= '<Addresses />';
		}

		if($arFields['Phones']) {
			$sReturn .= '<Phones>';
			foreach($arFields['Phones'] as $arPhoneFields) {
				$arTmp = array();
				if(isset($arPhoneFields['Code'])) {
					$arTmp[] = 'Code="'.htmlspecialcharsbx($arPhoneFields['Code']).'"';
				}
				if(isset($arPhoneFields['Number'])) {
					$arTmp[] = 'Number="'.htmlspecialcharsbx($arPhoneFields['Number']).'"';
				}
				$sReturn .= '<Phone '.implode(' ', $arTmp).'>';
				if(isset($arPhoneFields['Remark'])) {
					$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arPhoneFields['Remark']).']]></Remark>';
				}
				$sReturn .= '</Phone>';
			}
			$sReturn .= '</Phones>';
		} else {
			$sReturn .= '<Phones />';
		}

		$sReturn .= '</Customer>';

		return $sReturn;
	}

	/**
	 * Добавляет заказ клиента
	 *	$arFields = array(
	 *		'OrderSource' => 'код заказа',
	 *		'DiscountPercent' => 'процент скидки',
	 *		'Discount' => 'фиксированная сумма скидки',
	 *		'PayMethod' => 'метод оплаты (Наличные/Безнал/Yandex Money/…)',
	 *		'QtyPerson' => 'количество персон',
	 *		'Type' => 'тип заказа (Доставка/С собой/…)',
	 *		'ChangeAmount' => 'сумма, с которой потребуется дать сдачу',
	 *		'TimePlan' => 'время, к которому заказ должен быть готов',
	 *		'Department' => 'код подразделения, в случае доставки "С собой"',
	 *		'Customer' => array(
	 *			'Login' => 'e-mail клиента',
	 *			'FIO' => 'ФИО клиента',
	 *		),
	 *		'Address' => array(
	 *			'CityName' => 'город',
	 *			'StationName' => 'станция метро',
	 *			'StreetName' => 'улица',
	 *			'House' => 'дом',
	 *			'Corpus' => 'корпус',
	 *			'Building' => 'строение',
	 *			'Flat' => 'квартира',
	 *			'Porch' => 'подъезд',
	 *			'Floor' => 'этаж',
	 *			'DoorCode' => 'код домофона',
	 *			'Room' => 'комната',
	 *			'Office' => 'офис',
	 *			'Remark' => 'комментарий'
	 *		),
	 *		'Phone' => array(
	 *			'Code' => '000',
	 *			'Number' => '1234567890',
	 *			'Remark' => 'комментарий'
	 *		),
	 *		'Products' => array(
	 *			array(
	 *				'Code' => '',
	 *				'Qty' => '',
	 *				'DiscountPercent' => '',
	 *				'Discount' => '',
	 *				'Remark' => 'комментарий'
	 *			)
	 *		),
	 *		'Bonuses' => array(
	 *			array(
	 *				'Code' => '',
	 *				'Qty' => '',
	 *				'Remark' => 'комментарий'
	 *			)
	 *		),
	 *		'Remark' => 'комментарий'
	 *	)
	 *
	 * return array('Code', 'ID');
	 */
	public function AddOrder($arFields, $sOrderDesc = '') {
		$arReturn = array();
		
		$arErr = $this->CheckAddOrderFieldsXml($arFields);
		if(empty($arErr)) {
			$obSoapService = $this->GetSoapService();
			if($this->IsClientInited()) {
				$arRequestParams = array(
					array(
						'Order' => $this->GetAddOrderFieldsXml($arFields),
						'OrderText' => $sOrderDesc
					)
				);
				$arTmpResult = $obSoapService->Call('AddOrder', $arRequestParams, false);

				if($arTmpResult['ERROR']) {
					$arErr = $arTmpResult['ERROR'];
				} elseif(isset($arTmpResult['BODY']->AddOrderResult) && strlen($arTmpResult['BODY']->AddOrderResult)) {
					$sReturn = $arTmpResult['BODY']->AddOrderResult;
					$obXmlContent = simplexml_load_string($sReturn);
					if($obXmlContent) {
						$arReturn['Code'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlContent, 'Code');
						$arReturn['ID'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlContent, 'ID');
					} else {
						$arErr = array(
							'ERROR_CODE' => 'UNKNOWN_XML_ERROR',
							'ERROR_MSG' => 'Не удалось распознать XML ответа'
						);
					}
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'SOAP_ERROR',
					'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
				);
			}
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'arFields' => $arFields,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $arReturn;
	}

	protected function CheckAddOrderFieldsXml(&$arFields) {
		$arReturn = array();
		if(!isset($arFields['Customer']) || empty($arFields['Customer'])) {
			$arReturn['CUSTOMER_EMPTY'] = 'Не задан покупатель';
		}
		if($arFields['Customer']['Login'] && !check_email($arFields['Customer']['Login'])) {
			$arReturn['LOGIN_WRONG'] = 'Некорректно задан e-mail (логин) покупателя';
		}

		//if(!isset($arFields['Address']) || empty($arFields['Address'])) {
		//	$arReturn['ADDRESS_EMPTY'] = 'Не задан адрес доставки';
		//}

		if(!isset($arFields['Phone']) || empty($arFields['Phone'])) {
			$arReturn['PHONE_EMPTY'] = 'Не задан телефон покупателя';
		}

		if(!isset($arFields['Products']) || empty($arFields['Products'])) {
			$arReturn['PRODUCTS_EMPTY'] = 'Не заданы заказываемые позиции меню';
		}

		return $arReturn;
	}

	protected function GetAddOrderFieldsXml($arFields) {
		$sReturn = '';
		$arTmp = array();
		if(isset($arFields['OrderSource'])) {
			$arTmp[] = 'OrderSource="'.htmlspecialcharsbx(trim($arFields['OrderSource'])).'"';
		}
		if(isset($arFields['DiscountPercent']) && doubleval($arFields['DiscountPercent']) > 0) {
			$arTmp[] = 'DiscountPercent="'.doubleval($arFields['DiscountPercent']).'"';
		}
		if(isset($arFields['Discount']) && doubleval($arFields['Discount']) > 0) {
			$arTmp[] = 'Discount="'.doubleval($arFields['Discount']).'"';
		}
		if(isset($arFields['PayMethod'])) {
			$arTmp[] = 'PayMethod="'.htmlspecialcharsbx($arFields['PayMethod']).'"';
		}
		if(isset($arFields['QtyPerson']) && intval($arFields['QtyPerson']) > 0) {
			$arTmp[] = 'QtyPerson="'.intval($arFields['QtyPerson']).'"';
		}
		if(isset($arFields['Type'])) {
			$arTmp[] = 'Type="'.htmlspecialcharsbx($arFields['Type']).'"';
		}
		if(isset($arFields['ChangeAmount']) && doubleval($arFields['ChangeAmount']) > 0) {
			$arTmp[] = 'ChangeAmount="'.doubleval($arFields['ChangeAmount']).'"';
		}
		if(isset($arFields['TimePlan'])) {
			$arTmp[] = 'TimePlan="'.htmlspecialcharsbx($arFields['TimePlan']).'"';
		}
		if(isset($arFields['Department'])) {
			$arTmp[] = 'Department="'.htmlspecialcharsbx($arFields['Department']).'"';
		}
		if(isset($arFields['RemarkMoney'])) {
			$arTmp[] = 'RemarkMoney="'.htmlspecialcharsbx($arFields['RemarkMoney']).'"';
		}

		$sReturn .= '<Order '.implode(' ', $arTmp).'>';

		if($arFields['Customer']) {
			$arCustomerFields = $arFields['Customer'];
			$arTmp = array();
			if(isset($arCustomerFields['Login'])) {
				$arTmp[] = 'Login="'.htmlspecialcharsbx($arCustomerFields['Login']).'"';
			}
			if(isset($arCustomerFields['FIO'])) {
				$arTmp[] = 'FIO="'.htmlspecialcharsbx($arCustomerFields['FIO']).'"';
			}

			$sReturn .= '<Customer '.implode(' ', $arTmp).'>';
			if(isset($arCustomerFields['Remark']) && strlen($arCustomerFields['Remark'])) {
				$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arCustomerFields['Remark']).']]></Remark>';
			}
			$sReturn .= '</Customer>';
		} else {
			$sReturn .= '<Customer />';
		}

		if($arFields['Address']) {
			$arAddrFields = $arFields['Address'];

			$arTmp = array();
			if(isset($arAddrFields['CityName'])) {
				$arTmp[] = 'CityName="'.htmlspecialcharsbx($arAddrFields['CityName']).'"';
			}
			if(isset($arAddrFields['StationName'])) {
				$arTmp[] = 'StationName="'.htmlspecialcharsbx($arAddrFields['StationName']).'"';
			}
			if(isset($arAddrFields['StreetName'])) {
				$arTmp[] = 'StreetName="'.htmlspecialcharsbx($arAddrFields['StreetName']).'"';
			}
			if(isset($arAddrFields['House'])) {
				$arTmp[] = 'House="'.htmlspecialcharsbx($arAddrFields['House']).'"';
			}
			if(isset($arAddrFields['Corpus'])) {
				$arTmp[] = 'Corpus="'.htmlspecialcharsbx($arAddrFields['Corpus']).'"';
			}
			if(isset($arAddrFields['Building'])) {
				$arTmp[] = 'Building="'.htmlspecialcharsbx($arAddrFields['Building']).'"';
			}
			if(isset($arAddrFields['Flat'])) {
				$arTmp[] = 'Flat="'.htmlspecialcharsbx($arAddrFields['Flat']).'"';
			}
			if(isset($arAddrFields['Porch'])) {
				$arTmp[] = 'Porch="'.htmlspecialcharsbx($arAddrFields['Porch']).'"';
			}
			if(isset($arAddrFields['Floor'])) {
				$arTmp[] = 'Floor="'.htmlspecialcharsbx($arAddrFields['Floor']).'"';
			}
			if(isset($arAddrFields['DoorCode'])) {
				$arTmp[] = 'DoorCode="'.htmlspecialcharsbx($arAddrFields['DoorCode']).'"';
			}
			if(isset($arAddrFields['Room'])) {
				$arTmp[] = 'Room="'.htmlspecialcharsbx($arAddrFields['Room']).'"';
			}
			if(isset($arAddrFields['Office'])) {
				$arTmp[] = 'Office="'.htmlspecialcharsbx($arAddrFields['Office']).'"';
			}

			$sReturn .= '<Address '.implode(' ', $arTmp).'>';
			if(isset($arAddrFields['Remark']) && strlen($arAddrFields['Remark'])) {
				$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arAddrFields['Remark']).']]></Remark>';
			}
			$sReturn .= '</Address>';
		} else {
			$sReturn .= '<Address />';
		}

		if($arFields['Phone']) {
			$arPhoneFields = $arFields['Phone'];
			$arTmp = array();
			if(isset($arPhoneFields['Code'])) {
				$arTmp[] = 'Code="'.htmlspecialcharsbx($arPhoneFields['Code']).'"';
			}
			if(isset($arPhoneFields['Number'])) {
				$arTmp[] = 'Number="'.htmlspecialcharsbx($arPhoneFields['Number']).'"';
			}
			$sReturn .= '<Phone '.implode(' ', $arTmp).'>';
			if(isset($arPhoneFields['Remark']) && strlen($arPhoneFields['Remark'])) {
				$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arPhoneFields['Remark']).']]></Remark>';
			}
			$sReturn .= '</Phone>';
		} else {
			$sReturn .= '<Phone />';
		}


		$sReturn .= '<Products>';
		if($arFields['Products']) {
			foreach($arFields['Products'] as $arProdFields) {
				$arTmp = array();
				if(isset($arProdFields['Code'])) {
					$arTmp[] = 'Code="'.htmlspecialcharsbx($arProdFields['Code']).'"';
				}
				$arProdFields['Qty'] = $arProdFields['Qty'] && intval($arProdFields['Qty']) > 0 ? intval($arProdFields['Qty']) : 1;
				$arTmp[] = 'Qty="'.htmlspecialcharsbx($arProdFields['Qty']).'"';

				if(isset($arProdFields['Guest'])) {
					$arTmp[] = 'Guest="'.htmlspecialcharsbx(trim($arProdFields['Guest'])).'"';
				}

				if(isset($arProdFields['DiscountPercent']) && doubleval($arProdFields['DiscountPercent']) > 0) {
					$arTmp[] = 'DiscountPercent="'.doubleval($arProdFields['DiscountPercent']).'"';
				}

				if(isset($arProdFields['Discount']) && doubleval($arProdFields['Discount']) > 0) {
					$arTmp[] = 'Discount="'.doubleval($arProdFields['Discount']).'"';
				}

				$sReturn .= '<Product '.implode(' ', $arTmp).'>';
				if(isset($arProdFields['Remark']) && strlen($arProdFields['Remark'])) {
					$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arProdFields['Remark']).']]></Remark>';
				}
				$sReturn .= '</Product>';
			}
		}
		if($arFields['Bonuses']) {
			foreach($arFields['Bonuses'] as $arProdFields) {
				$arTmp = array();
				if(isset($arProdFields['Code'])) {
					$arTmp[] = 'Code="'.htmlspecialcharsbx($arProdFields['Code']).'"';
				}

				if(isset($arProdFields['Guest'])) {
					$arTmp[] = 'Guest="'.htmlspecialcharsbx(trim($arProdFields['Guest'])).'"';
				}

				$arProdFields['Qty'] = $arProdFields['Qty'] && intval($arProdFields['Qty']) > 0 ? intval($arProdFields['Qty']) : 1;
				$arTmp[] = 'Qty="'.htmlspecialcharsbx($arProdFields['Qty']).'"';

				$sReturn .= '<Bonus '.implode(' ', $arTmp).'>';
				if(isset($arProdFields['Remark']) && strlen($arProdFields['Remark'])) {
					$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arProdFields['Remark']).']]></Remark>';
				}
				$sReturn .= '</Bonus>';
			}
		}
		$sReturn .= '</Products>';

		if(isset($arFields['Remark'])) {
			$sReturn .= '<Remark><![CDATA['.htmlspecialcharsbx($arFields['Remark']).']]></Remark>';
		}

		$sReturn .= '</Order>';

		return $sReturn;
	}

	//
	// Возвращает список заказов клиента
	//
	public function GetOrders($sUserLogin, $sOrderCode = '') {
		$sReturn = '';
		if(strlen($sUserLogin)) {
			$obSoapService = $this->GetSoapService();
			if($this->IsClientInited()) {
				$arTmpParams = array(
					'Login' => $sUserLogin,
				);
				if(strlen($sOrderCode)) {
					$arTmpParams['OrderCode'] = $sOrderCode;
				}
				$arRequestParams = array($arTmpParams);
				$arTmpResult = $obSoapService->Call('GetOrders', $arRequestParams, false);
				if($arTmpResult['ERROR']) {
					$arErr = $arTmpResult['ERROR'];
				} elseif(isset($arTmpResult['BODY']->GetOrdersResult) && strlen($arTmpResult['BODY']->GetOrdersResult)) {
					$sReturn = $arTmpResult['BODY']->GetOrdersResult;
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'SOAP_ERROR',
					'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
				);
			}
			if($arErr) {
				$this->ThrowException($arErr);

				$this->LogError(
					array(
						'method' => __FUNCTION__,
						'arErr' => $arErr,
						'sUserLogin' => $sUserLogin,
						'sOrderCode' => $sOrderCode,
						'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
						'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
					)
				);

			}
		}
		return $sReturn;
	}

	public function GetOrdersArray($sUserLogin, $sOrderCode = '') {
		$arReturn = array();
		$sResultXml = $this->GetOrders($sUserLogin, $sOrderCode);
		if(strlen($sResultXml)) {
			$bGetExactOrder = strlen($sOrderCode) ? true : false;
			$obXmlContent = @simplexml_load_string($sResultXml, null, LIBXML_NOCDATA);
			if($obXmlContent) {
				$arReturn['Orders'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/Orders/Order');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arOrderItem = array();
					// Amount: сумма по строкам заказа, TotalClearAmount: итоговая сумма к оплате (т.е. из суммы по строкам вычитаются все скидки и  бонусы и добавляется оплата доставки).
					$arOrderItem = array(
						'ID' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'OrderID'),
						'RN' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'RN'),
						'Code' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code'),
						'DateCreate' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DateCreate'),
						'Status' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'State'),
						'StatusCode' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'StateCode'),
						'Amount' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Amount')),
						'DiscountPercent' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DiscountPercent')),
						'DeliverySum' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DeliverySum')),
						'TotalClearAmount' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'TotalClearAmount')),
						'PhoneCode' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'PhoneCode'),
						'PhoneNum' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'PhoneNum'),
						'Remark' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark'),
						'Address' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Address'),
						'AddressRemark' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'AddressRemark'),
						'QtyPerson' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'QtyPerson')),
						'CancelName' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'CancelName'),
					);

					$arOrderItem['Products'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Products/Product');
					foreach($arTmpXmlItems2 as $obXmlItem2) {
						$arProductItem = array();
						$arProductItem['Code'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code');
						$arProductItem['Qty'] = CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Qty'));
						$arProductItem['Price'] = CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Price'));
						$arProductItem['UnitName'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'UnitName');
						$arProductItem['IsBonus'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'IsBonus');
						$arProductItem['Remark'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Remark');
						$arProductItem['Guest'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Packet');
						$arOrderItem['Products'][] = $arProductItem;
					}

					/*
					$arOrderItem['Customer'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Customer');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Customer']['Login'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Login'));
						$arOrderItem['Customer']['Pwd'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Pwd'));
						$arOrderItem['Customer']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Customer']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Phone'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Phone');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Phone']['Code'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code'));
						$arOrderItem['Phone']['Number'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Number'));
						$arOrderItem['Phone']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Phone']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Address'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Address');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Address']['CityName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'CityName'));
						$arOrderItem['Address']['StationName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'StationName'));
						$arOrderItem['Address']['StreetName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'StreetName'));
						$arOrderItem['Address']['House'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'House'));
						$arOrderItem['Address']['Corpus'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Corpus'));
						$arOrderItem['Address']['Building'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Building'));
						$arOrderItem['Address']['Flat'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Flat'));
						$arOrderItem['Address']['Porch'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Porch'));
						$arOrderItem['Address']['Floor'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Floor'));
						$arOrderItem['Address']['DoorCode'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'DoorCode'));
						$arOrderItem['Address']['Room'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Room'));
						$arOrderItem['Address']['Office'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Office'));
						$arOrderItem['Address']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Address']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Bonus'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Products/Bonus');
					foreach($arTmpXmlItems2 as $obXmlItem2) {
						$arBonusItem = array();
						$arBonusItem['Code'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code'));
						$arBonusItem['Qty'] = CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Qty')));
						$arBonusItem['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arBonusItem['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
						$arOrderItem['Bonus'][] = $arBonusItem;
					}

					$arTmpXmlItems2 = $obXmlItem->Xpath('Remark');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Remark'));
					}
					*/

					if(!$bGetExactOrder || $arOrderItem['Code'] == $sOrderCode) {
						$arReturn['Orders'][] = $arOrderItem;
					}
				}
			}
		}
		return $arReturn;
	}

	public function GetOrdersNew($sUserLogin, $sOrderCode = '') {
		$sReturn = '';
		if(strlen($sUserLogin)) {
			$obSoapService = $this->GetSoapService();
			if($this->IsClientInited()) {
				$arTmpParams = array(
					'Login' => $sUserLogin,
				);
				if(strlen($sOrderCode)) {
					$arTmpParamsNew['OrderCode'] = $sOrderCode;
				}
				$arRequestParams = array($arTmpParamsNew);
				$arTmpResult = $obSoapService->Call('GetOrders', $arRequestParams, false);
				if($arTmpResult['ERROR']) {
					$arErr = $arTmpResult['ERROR'];
				} elseif(isset($arTmpResult['BODY']->GetOrdersResult) && strlen($arTmpResult['BODY']->GetOrdersResult)) {
					$sReturn = $arTmpResult['BODY']->GetOrdersResult;
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'SOAP_ERROR',
					'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
				);
			}
			if($arErr) {
				$this->ThrowException($arErr);

				$this->LogError(
					array(
						'method' => __FUNCTION__,
						'arErr' => $arErr,
						'sUserLogin' => $sUserLogin,
						'sOrderCode' => $sOrderCode,
						'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
						'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
					)
				);

			}
		}
		return $sReturn;
	}

	public function GetOrdersArrayNew($sUserLogin, $sOrderCode = '') {
		$arReturn = array();
		$sResultXml = $this->GetOrdersNew($sUserLogin, $sOrderCode);
		if(strlen($sResultXml)) {
			$bGetExactOrder = strlen($sOrderCode) ? true : false;
			$obXmlContent = @simplexml_load_string($sResultXml, null, LIBXML_NOCDATA);
			if($obXmlContent) {
				$arReturn['Orders'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/Orders/Order');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arOrderItem = array();
					// Amount: сумма по строкам заказа, TotalClearAmount: итоговая сумма к оплате (т.е. из суммы по строкам вычитаются все скидки и  бонусы и добавляется оплата доставки).
					$arOrderItem = array(
						'ID' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'OrderID'),
						'RN' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'RN'),
						'Code' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code'),
						'DateCreate' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DateCreate'),
						'Status' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'State'),
						'StatusCode' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'StateCode'),
						'Amount' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Amount')),
						'DiscountPercent' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DiscountPercent')),
						'DeliverySum' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'DeliverySum')),
						'TotalClearAmount' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'TotalClearAmount')),
						'PhoneCode' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'PhoneCode'),
						'PhoneNum' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'PhoneNum'),
						'Remark' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark'),
						'Address' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Address'),
						'AddressRemark' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'AddressRemark'),
						'QtyPerson' => CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'QtyPerson')),
						'CancelName' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'CancelName'),
					);

					$arOrderItem['Products'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Products/Product');
					foreach($arTmpXmlItems2 as $obXmlItem2) {
						$arProductItem = array();
						$arProductItem['Code'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code');
						$arProductItem['Qty'] = CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Qty'));
						$arProductItem['Price'] = CNiyamaImportUtils::Str2Double(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Price'));
						$arProductItem['UnitName'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'UnitName');
						$arProductItem['IsBonus'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'IsBonus');
						$arProductItem['Remark'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Remark');
						$arProductItem['Guest'] = CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Packet');
						$arOrderItem['Products'][] = $arProductItem;
					}

					/*
					$arOrderItem['Customer'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Customer');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Customer']['Login'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Login'));
						$arOrderItem['Customer']['Pwd'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Pwd'));
						$arOrderItem['Customer']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Customer']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Phone'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Phone');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Phone']['Code'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code'));
						$arOrderItem['Phone']['Number'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Number'));
						$arOrderItem['Phone']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Phone']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Address'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Address');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Address']['CityName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'CityName'));
						$arOrderItem['Address']['StationName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'StationName'));
						$arOrderItem['Address']['StreetName'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'StreetName'));
						$arOrderItem['Address']['House'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'House'));
						$arOrderItem['Address']['Corpus'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Corpus'));
						$arOrderItem['Address']['Building'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Building'));
						$arOrderItem['Address']['Flat'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Flat'));
						$arOrderItem['Address']['Porch'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Porch'));
						$arOrderItem['Address']['Floor'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Floor'));
						$arOrderItem['Address']['DoorCode'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'DoorCode'));
						$arOrderItem['Address']['Room'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Room'));
						$arOrderItem['Address']['Office'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Office'));
						$arOrderItem['Address']['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arOrderItem['Address']['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
					}

					$arOrderItem['Bonus'] = array();
					$arTmpXmlItems2 = $obXmlItem->Xpath('Products/Bonus');
					foreach($arTmpXmlItems2 as $obXmlItem2) {
						$arBonusItem = array();
						$arBonusItem['Code'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Code'));
						$arBonusItem['Qty'] = CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Qty')));
						$arBonusItem['Remark'] = '';
						$arTmpXmlItems3 = $obXmlItem2->Xpath('Remark');
						$obXmlItem3 = reset($arTmpXmlItems3);
						if($obXmlItem3) {
							$arBonusItem['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem3, 'Remark'));
						}
						$arOrderItem['Bonus'][] = $arBonusItem;
					}

					$arTmpXmlItems2 = $obXmlItem->Xpath('Remark');
					$obXmlItem2 = reset($arTmpXmlItems2);
					if($obXmlItem2) {
						$arOrderItem['Remark'] = trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'Remark'));
					}
					*/

					if(!$bGetExactOrder || $arOrderItem['Code'] == $sOrderCode) {
						$arReturn['Orders'][] = $arOrderItem;
					}
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает справочник ресторанов
	//
	public function GetDepartments() {
		$sReturn = '';
		$obSoapService = $this->GetSoapService();
		if($this->IsClientInited()) {
			$arRequestParams = array();
			$arTmpResult = $obSoapService->Call('GetDepartments', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif(isset($arTmpResult['BODY']->GetDepartmentsResult) && strlen($arTmpResult['BODY']->GetDepartmentsResult)) {
				$sReturn = $arTmpResult['BODY']->GetDepartmentsResult;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $sReturn;
	}

	public function GetDepartmentsArray() {
		$arReturn = array();
		$sResultXml = $this->GetDepartments();
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml);
			if($obXmlContent) {
				$arReturn['Items'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/xml/Department');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arReturn['Items'][] = array(
						'Code' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code')),
						'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Name')),
						'Remark' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark')),
						'Long' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Long'))),
						'Lat' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Lat'))),
					);
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает справочник городов
	//
	public function GetCities() {
		$sReturn = '';
		$obSoapService = $this->GetSoapService();
		if($this->IsClientInited()) {
			$arRequestParams = array();
			$arTmpResult = $obSoapService->Call('GetCities', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif(isset($arTmpResult['BODY']->GetCitiesResult) && strlen($arTmpResult['BODY']->GetCitiesResult)) {
				$sReturn = $arTmpResult['BODY']->GetCitiesResult;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $sReturn;
	}

	public function GetCitiesArray() {
		$arReturn = array();
		$sResultXml = $this->GetCities();
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml);
			if($obXmlContent) {
				$arReturn['Items'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/xml/Item');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arReturn['Items'][] = array(
						'Code' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Code')),
						'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Name')),
					);
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает справочник улиц городов
	//
	public function GetStreets($sCityCode = '') {
		$sReturn = '';
		$obSoapService = $this->GetSoapService();
		if($this->IsClientInited()) {
			$arRequestParams = array();
			if(strlen($sCityCode)) {
				$arRequestParams[] = array(
					'CityCode' => $sCityCode
				);
			}
			$arTmpResult = $obSoapService->Call('GetStreets', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif(isset($arTmpResult['BODY']->GetStreetsResult) && strlen($arTmpResult['BODY']->GetStreetsResult)) {
				$sReturn = $arTmpResult['BODY']->GetStreetsResult;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'sCityCode' => $sCityCode,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $sReturn;
	}

	public function GetStreetsArray($sCityCode = '') {
		$arReturn = array();
		$sResultXml = $this->GetStreets($sCityCode);
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml);
			if($obXmlContent) {
				$arReturn['Items'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/xml/Item');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arReturn['Items'][] = array(
						'CityCode' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'CityCode')),
						'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Name')),
						'Remark' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Remark')),

					);
				}
			}
		}
		return $arReturn;
	}

	//
	// Возвращает станции метро
	//
	public function GetMetro($sCityCode = '') {
		$sReturn = '';
		$obSoapService = $this->GetSoapService();
		if($this->IsClientInited()) {
			$arRequestParams = array();
			if(strlen($sCityCode)) {
				$arRequestParams[] = array(
					'CityCode' => $sCityCode
				);
			}
			$arTmpResult = $obSoapService->Call('GetMetro', $arRequestParams, false);
			if($arTmpResult['ERROR']) {
				$arErr = $arTmpResult['ERROR'];
			} elseif(isset($arTmpResult['BODY']->GetMetroResult) && strlen($arTmpResult['BODY']->GetMetroResult)) {
				$sReturn = $arTmpResult['BODY']->GetMetroResult;
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'SOAP_ERROR',
				'ERROR_MSG' => 'Соединение с веб-сервисом не установлено'
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);

			$this->LogError(
				array(
					'method' => __FUNCTION__,
					'arErr' => $arErr,
					'sCityCode' => $sCityCode,
					'arTmpResult' => isset($arTmpResult) ? $arTmpResult : '-',
					'arRequestParams' => isset($arRequestParams) ? $arRequestParams : '-',
				)
			);

		}
		return $sReturn;
	}

	public function GetMetroArray($sCityCode = '') {
		$arReturn = array();
		$sResultXml = $this->GetMetro($sCityCode);
		if(strlen($sResultXml)) {
			$obXmlContent = simplexml_load_string($sResultXml);
			if($obXmlContent) {
				$arReturn['Items'] = array();
				$arTmpXmlItems = $obXmlContent->Xpath('/xml/Item');
				foreach($arTmpXmlItems as $obXmlItem) {
					$arReturn['Items'][] = array(
						'CityCode' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'CityCode')),
						'Name' => trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Name')),
						'Long' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Long'))),
						'Lat' => CNiyamaImportUtils::Str2Double(trim(CNiyamaImportUtils::GetXmlAttrValue($obXmlItem, 'Lat'))),
					);
				}
			}
		}
		return $arReturn;
	}
}

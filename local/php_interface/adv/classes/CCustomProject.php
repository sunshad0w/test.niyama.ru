<?
/**
 * Уникальные утилиты для проекта
 *
 * @updated: 21.07.2015
 */

class CCustomProject {

    # Название ссесии для определения режима просмотра каталога
    private static $sModeSessionName = 'NIYAMA_PRODUCTS_MODE';
    # Коды режимов просмотра
    private static $arMode = array('other', 'classic');

	public static function GetOctolineFormJsCall($sOnclickAdd = '') {
		$sReturn = 'onclick="window.open(\'http://niyama.s7.octoline.ru/widgets/callme_new.php?service_instance_id=3091\', \'CallMe\', \'width=350, height=380, resizable=no, toolbar=0, status=0, location=0, directories=0, menubar=0, scrollbars=0\');'.$sOnclickAdd.'"';
		return $sReturn;
	}

	public static function GetOctolineFormLink($sLinkText = 'Обратный звонок', $sAddAttr = '', $sCssClass = '', $sOnclickAdd = '') {
		$sAddAttr = $sAddAttr ? ' '.trim($sAddAttr) : '';
		$sCssClass = strlen($sCssClass) ? $sCssClass : 'feedback__link centrifuge__item';
		$sAddAttr .= ' '.self::GetOctolineFormJsCall($sOnclickAdd);
		$sReturn = '<a class="'.$sCssClass.'" href="javascript:void(0)"'.$sAddAttr.'>'.$sLinkText.'</a>';
		return $sReturn;
	}

	public static function IfIBTypeByID($sIBTypeId) {
		CModule::IncludeModule('iblock');
		$res = CIBlockType::GetByID($sIBTypeId);
		if($IB = $res->Fetch()) {
			return true;
		}
		return false;
	}

	public static function GetEnumPropValues($iIBlockId, $sPropCode) {
		static $arStatCache = array();
		if(!isset($arStatCache[$iIBlockId][$sPropCode])) {
			$arStatCache[$iIBlockId][$sPropCode] = array();
			if($iIBlockId > 0 && strlen($sPropCode)) {
				CModule::IncludeModule('iblock');
				$dbItems = CIBlockPropertyEnum::GetList(
					array(
						'ID' => 'ASC'
					), 
					array(
						'IBLOCK_ID' => $iIBlockId,
						'CODE' => $sPropCode
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arStatCache[$iIBlockId][$sPropCode][$arItem['VALUE']] = $arItem;
				}
			}
		}
		return $arStatCache[$iIBlockId][$sPropCode];
	}

	public static function GetEnumPropValueId($iIBlockId, $sPropCode, $sValue) {
		$arValues = self::GetEnumPropValues($iIBlockId, $sPropCode);
		return isset($arValues[$sValue]) ? $arValues[$sValue]['ID'] : 0;
	}

    /**
     * Устанавливаем режим по умолчанию.
     * GetDefaultMode
     * @return mixed
     */
    public static function GetDefaultMode()
    {
        return self::$arMode[0];
    }

    /**
     * Получение режима просмотра.
     * GetMode
     * @return mixed
     */
    public static function GetMode()
    {
        return (isset($_SESSION[self::$sModeSessionName]) && (!empty($_SESSION[self::$sModeSessionName])) )? $_SESSION[self::$sModeSessionName] : self::GetDefaultMode();
    }

    /**
     * Установка режима по индексу
     * SetMode
     * @param $iIndexOfMode
     * @return mixed
     */
    public static function SetMode($iIndexOfMode)
    {
        if (isset($_SESSION[self::$sModeSessionName])){
            unset($_SESSION[self::$sModeSessionName]);
        }
        $_SESSION[self::$sModeSessionName] = self::$arMode[$iIndexOfMode];
        return $_SESSION[self::$sModeSessionName];
    }

    /**
     * Проверка на режим по Индексу
     * isMode
     * @param $iIndexOfMode
     * @return bool
     */
    public static function isMode($iIndexOfMode)
    {
      return (self::GetMode() == self::$arMode[$iIndexOfMode]);
    }

    /**
     * Меняем режим просмотра
     * ChangeMode
     * @return mixed
     */
    public static function ChangeMode()
    {
        if (self::isMode(0)){
            return self::SetMode(1);
        } else {
            return self::SetMode(0);
        }
    }

	//
	// Возвращает дерево секций инфоблока
	//
	public static function GetIBlockSectionsTree($iIBlockId, $bRefreshCache = false) {
		$arReturn = array();
		$iIBlockId = intval($iIBlockId);
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iIBlockId.'|v3';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'sections_tree';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/sections_tree/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockSection::GetList(
					array(
						'left_margin' => 'asc'
					),
					array(
						'IBLOCK_ID' => $iIBlockId
					),
					false,
					array(
						'ID', 'NAME', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'XML_ID', 'DEPTH_LEVEL',
						'SORT', 'IBLOCK_SECTION_ID', 'ACTIVE', 'GLOBAL_ACTIVE', 'CODE', 'SECTION_PAGE_URL','DESCRIPTION'
					)
				);
				while($arItem = $dbItems->GetNext(true, false)) {
					$arReturn[$arItem['ID']] = $arItem;
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	//
	// По ID секции инфоблока возвращает список дочерних секций
	//
	public static function GetIBlockChildSections($iIBlockId, $iSectionId = 0, $bRefreshCache = false) {
		$iIBlockId = intval($iIBlockId);
		$sCacheEntity = 'sections_childs_'.$iIBlockId;
		$arReturn = array();
		$iSectionId = intval($iSectionId);
		if($iSectionId > 0) {
			if(CStaticCache::IsSetCache($iSectionId, $sCacheEntity)) {
				$arReturn = CStaticCache::GetCacheValue($iSectionId, $sCacheEntity);
			} else {
				$arTree = self::GetIBlockSectionsTree($iIBlockId, $bRefreshCache);
				if(isset($arTree[$iSectionId])) {
					$arRoot = $arTree[$iSectionId];
					$arReturn[$iSectionId] = $arRoot;
					$iRightMargin = $arRoot['RIGHT_MARGIN'];
					foreach($arTree as $arItem) {
						if($arItem['LEFT_MARGIN'] > $arRoot['LEFT_MARGIN']) {
							if($arItem['RIGHT_MARGIN'] < $arRoot['RIGHT_MARGIN']) {
								$arReturn[$arItem['ID']] = $arItem;
							}
						}
						if($arItem['LEFT_MARGIN'] > $arRoot['RIGHT_MARGIN']) {
							break;
						}
					}
				}
				CStaticCache::SetCacheValue($iSectionId, $arReturn, $sCacheEntity, 100);
			}
		}
		return $arReturn;
	}

    public static function GetIBlockParentSection($iIBlockId, $iSectionId = 0, $bRefreshCache = false) {
        $iIBlockId = intval($iIBlockId);
        $sCacheEntity = 'sections_parent_'.$iIBlockId;
        $arReturn = array();
        $iSectionId = intval($iSectionId);
        if($iSectionId > 0) {
            if(CStaticCache::IsSetCache($iSectionId, $sCacheEntity)) {
                $arReturn = CStaticCache::GetCacheValue($iSectionId, $sCacheEntity);
            } else {
                $arTree = self::GetIBlockSectionsTree($iIBlockId, $bRefreshCache);
                if(isset($arTree[$iSectionId])) {
                    $arRoot = $arTree[$iSectionId];
                    $arReturn[$iSectionId] = $arRoot;
                    $iRightMargin = $arRoot['RIGHT_MARGIN'];
                    foreach($arTree as $arItem) {
                        if($arItem['LEFT_MARGIN'] < $arRoot['LEFT_MARGIN']) {
                            if($arItem['RIGHT_MARGIN'] > $arRoot['RIGHT_MARGIN']) {
                                $arReturn[$arItem['ID']] = $arItem;
                            }
                        }
                        if($arItem['LEFT_MARGIN'] > $arRoot['RIGHT_MARGIN']) {
                            break;
                        }
                    }
                }
                CStaticCache::SetCacheValue($iSectionId, $arReturn, $sCacheEntity, 100);
            }
        }
        return $arReturn;
    }

	//
	// Возвращает ID веб-формы по ее коду
	//
	public static function GetFormIdByCode($sFormCode, $bRefreshCache = false) {
		$iReturnId = 0;
		$sFormCode = trim($sFormCode);
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $sFormCode;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'web_forms';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/web_forms/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$iReturnId = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('form')) {
				$dbItems = CForm::GetBySId($sFormCode);
				$arItem = $dbItems->Fetch();
				$iReturnId = $arItem['ID'];
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($iReturnId);
		}
		return $iReturnId;
	}

	//
	// Возвращает список ресторанов (инфоблок Список ресторанов)
	//
	public static function GetRestaurantsList($iIBlockId = 0, $bRefreshCache = false) {
		$arReturn = array();
		$iIBlockId = intval($iIBlockId);
		$iIBlockId = $iIBlockId > 0 ? $iIBlockId : CProjectUtils::GetIBlockIdByCode('restaurants-list', 'restaurants');
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iIBlockId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'restaurants_list';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/restaurants_list/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'NAME', 'ACTIVE',
						'PROPERTY_S_ADDRESS', 'PROPERTY_R_CITY'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					unset($arItem['PROPERTY_S_ADDRESS_DESCRIPTION'], $arItem['PROPERTY_S_ADDRESS_VALUE_ID']);
					unset($arItem['PROPERTY_R_CITY_DESCRIPTION'], $arItem['PROPERTY_R_CITY_VALUE_ID']);
					$arReturn[$arItem['ID']] = $arItem;
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	//
	// Возвращает список городов ресторанов (инфоблок Города ресторанов)
	//
	public static function GetRestaurantsCitiesList($iIBlockId = 0, $bRefreshCache = false) {
		$arReturn = array();
		$iIBlockId = intval($iIBlockId);
		$iIBlockId = $iIBlockId > 0 ? $iIBlockId : CProjectUtils::GetIBlockIdByCode('restaurants-cities', 'restaurants');
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iIBlockId;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'restaurants_cities';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array(__METHOD__);
		// путь для сохранения кэша
		$sCachePath = '/restaurants_cities/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'SORT' => 'ASC',
						'NAME' => 'ASC'
					),
					array(
						'IBLOCK_ID' => $iIBlockId,
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'NAME', 'ACTIVE',
						'PROPERTY_S_NAME_RP'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					unset($arItem['PROPERTY_S_NAME_RP_DESCRIPTION'], $arItem['PROPERTY_S_NAME_RP_VALUE_ID']);
					$arReturn[$arItem['ID']] = $arItem;
				}
			}
			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function KladrGetCity($sCityName, $sKladrToken, $sKladrKey, $bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = $sCityName.'||'.$sKladrToken.'||'.$sKladrKey;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = __CLASS__.'||'.__FUNCTION__;
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array();
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = 100;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {
			include_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/lib/kladr.php');
			// Инициализация api, в качестве параметров указываем токен и ключ для доступа к сервису
			$obKladr = new Kladr\Api($sKladrToken, $sKladrKey);

			$obKladrCityQuery = new Kladr\Query();
			$obKladrCityQuery->ContentName = $sCityName;
			$obKladrCityQuery->ContentType = Kladr\ObjectType::City;
			$obKladrCityQuery->WithParent = false;
			$obKladrCityQuery->Limit = 10;
			$arCitiesResult = $obKladr->QueryToArray($obKladrCityQuery);

			if($arCitiesResult) {
				foreach($arCitiesResult as $arItem) {
					if($arItem['type'] == 'Город') {
						$arReturn = $arItem;
						break;
					}
				}
			}

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function GetNumsOnly($sStr) {
		return preg_replace('#[^0-9]#', '', $sStr);		
	}
	
	/**
	 * Выборка случайного элемента с учетом веса
	 *
	 * @param array $weights массив весов (array('ID1'=>'вес1','ID2'=>'вес2))
	 * @return mixed выбранный элемент
	 */	
	public static function GetRandomByWeight($weights) {
		$total = array_sum($weights);
		$n = 0;	
		$num = mt_rand(1, $total);

		foreach($weights as $i => $weight) {
			$n += $weight;	
			if ($n >= $num) {
				return $i;
			}
		}		
	}

	/**
	 * Обновление свойств элемента корзины
	 *
	 * @param $iBasketItemId
	 * @param $arPropsUpdate
	 * @return bool
	 */
	public static function UpdateBasketPropsById($iBasketItemId, $arPropsUpdate) {
		$bResult = false;
		$iBasketItemId = intval($iBasketItemId);
		if($iBasketItemId > 0 && CModule::IncludeModule('sale')) {
			$arSetProps = array();
			$dbProp = CSaleBasket::GetPropsList(array(), array('BASKET_ID' => $iBasketItemId));
			while($arProp = $dbProp->Fetch()) {
				if(array_key_exists($arProp['CODE'], $arPropsUpdate)) {
					if($arPropsUpdate[$arProp['CODE']] === null) {
						// удаляем свойство
						continue;
					}
					$arProp['VALUE'] = $arPropsUpdate[$arProp['CODE']];
				}

				$arSetProps[] = array(
					'NAME' => $arProp['NAME'],
					'CODE' => $arProp['CODE'],
					'VALUE' => $arProp['VALUE'],
					'SORT' => $arProp['SORT'],
				);
			}

			$bResult = CSaleBasket::Update(
				$iBasketItemId, 
				array(
					'PROPS' => $arSetProps
				)
			);
		}
		return $bResult;
	}

	/**
	 * Возвращает кратное значение числа
	 *
	 * @param float $dNum Число, для которого необходимо определить кратное значение
	 * @param float $dDenorm Число, которому должен быть кратен первый параметр
	 * @return float
	 */
	public static function GetDivisibleValue($dNum, $dDenorm) {
		$dNum = doubleval($dNum);
		$dDenorm = doubleval($dDenorm);
		if($dDenorm != 0) {
			$r = fmod($dNum, $dDenorm);
			if($r != 0) {
				$dNum = $dNum - $r;
			}
		}
		return $dNum;
	}

	/**
	 * Возвращает код js-функции отправки события в Google-аналитику
	 */
	public static function GetGoogleAnalyticsEvent($sGooleParam3 = '', $sGooleParam4 = '', $sElementEvent = 'onclick', $sGooleParam1 = 'send', $sGooleParam2 = 'event') {
		$sReturn = '';
		$sGooleParam1 = $sGooleParam1 ? $sGooleParam1 : 'send';
		$sGooleParam2 = $sGooleParam2 ? $sGooleParam2 : 'event';

		$sFunc = "ga('$sGooleParam1', '$sGooleParam2', '$sGooleParam3', '$sGooleParam4')";
		if(strlen($sElementEvent)) {
			// onclick="ga(...)"
			$sReturn .= $sElementEvent.'="'.$sFunc.'; return true;"';
		} else {
			$sReturn .= $sFunc;
		}
		return $sReturn;
	}

	/**
	 * Возвращает код js-функции отправки события в Янедкс.Аналитику
	 */
	public static function GetYanexAnalyticsEvent($sGoalName = '', $sElementEvent = 'onclick', $sYaCounterFuncName = '') {
		$sReturn = '';

		$sYaCounterFuncName = strlen($sYaCounterFuncName) ? $sYaCounterFuncName : '';
		$sYaCounterFuncName = !strlen($sYaCounterFuncName) && defined('YANDEX_ANALYTICS_FUNC_NAME') && strlen(YANDEX_ANALYTICS_FUNC_NAME) ? YANDEX_ANALYTICS_FUNC_NAME : $sYaCounterFuncName;
		$sYaCounterFuncName = !strlen($sYaCounterFuncName) ? 'yaCounter24799106' : $sYaCounterFuncName;

		static $arJsFunc = array();
		$sJsFuncName = $sYaCounterFuncName.'ReachGoal';
		if(!$arJsFunc[$sJsFuncName]) {
			$arJsFunc[$sJsFuncName] = true;
			ob_start();
			?><script type="text/javascript">
				function <?=$sJsFuncName?>(sGoalName) {
					var bSetOnLoad = false;
					try {
						window.<?=$sYaCounterFuncName?>.reachGoal(sGoalName);
					} catch(e) {
						bSetOnLoad = true;
					}
					if(bSetOnLoad) {
						window.onload = function() {
							window.<?=$sYaCounterFuncName?>.reachGoal(sGoalName);
						}
					}
				}
			</script><?
			$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());
		}

		$sFunc = "$sJsFuncName('$sGoalName')";
		if(strlen($sElementEvent)) {
			// onclick="yaCounter23920336ReachGoal(...)"
			$sReturn .= $sElementEvent.'="'.$sFunc.'; return true;"';
		} else {
			$sReturn .= $sFunc;
		}
		return $sReturn;
	}

	/**
	 * Возвращает код js-функции отправки события в аналитику Янедкса и/или Google
	 */
	public static function GetAnalyticsEvents($arParams, $sElementEvent = 'onclick') {
		$sReturn = '';
		$sFunc1 = '';
		if($arParams['ga']) {
			$sFunc1 = self::GetGoogleAnalyticsEvent(
				$arParams['ga']['param3'], 
				$arParams['ga']['param4'], 
				'', 
				isset($arParams['ga']['param1']) ? $arParams['ga']['param1'] : '', 
				isset($arParams['ga']['param2']) ? $arParams['ga']['param2'] : ''
			);
		}
		$sFunc2 = '';
		if($arParams['ya']) {
			$sFunc2 = self::GetYanexAnalyticsEvent(
				$arParams['ya']['param1'], 
				'', 
				isset($arParams['ya']['func']) ? $arParams['ya']['func'] : ''
			);
		}

		$arTmp = array();
		if($sFunc1) {
			$arTmp[] = $sFunc1;
		}
		if($sFunc2) {
			$arTmp[] = $sFunc2;
		}
		if($arTmp) {
			$sFunc = implode('; ', $arTmp);
			if(strlen($sElementEvent)) {
				// onclick="ga(...); ya(...)"
				$sReturn .= $sElementEvent.'="'.$sFunc.'; return true;"';
			} else {
				$sReturn .= $sFunc;
			}
		}
		return $sReturn;
	}

	/**
	 *  Хелпер для вывода кода js-функций отправки событий в аналитику
	 */
	public static function GetAnalyticsByCode($sCode, $sElementEvent = 'onclick') {
		$sReturn = '';
		switch($sCode) {
			case 'link-telefon':
				$sReturn = self::GetAnalyticsEvents(array('ga' => array('param3' => 'link', 'param4' => 'telefon')), $sElementEvent);
			break;

			case 'link-quality_control':
				$sReturn = self::GetAnalyticsEvents(array('ga' => array('param3' => 'link', 'param4' => 'quality_control')), $sElementEvent);
			break;

			case 'link-back_call':
				$sReturn = self::GetAnalyticsEvents(array('ga' => array('param3' => 'link', 'param4' => 'back_call')), $sElementEvent);
			break;

			case 'control-knopka_otpravit':
				$sReturn = self::GetAnalyticsEvents(array('ga' => array('param3' => 'control', 'param4' => 'knopka_otpravit')), $sElementEvent);
			break;

			case 'control-forma_otpravlena':
				$sReturn = self::GetAnalyticsEvents(array('ga' => array('param3' => 'control', 'param4' => 'forma_otpravlena')), $sElementEvent);
			break;
		}
		return $sReturn;
	}
}

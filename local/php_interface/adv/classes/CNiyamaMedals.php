<?

class CNiyamaMedals {
	protected static $sUserMedalsInfoEntityName = 'UserMedalsInfo';

	public static function GetUserMedalsIBlockId() {
		static $iIBlockId = 0;
		if(!$iIBlockId) {
			$iIBlockId = CProjectUtils::GetIBlockIdByCode('users_medals', 'NIYAMA_DISCOUNTS');
		}
		return $iIBlockId;
	}

	/**
	 * CalculateAgent
	 */
	public static function CalculateAgent($iUsersCntByStep = 500) {
		$iUsersCntByStep = intval($iUsersCntByStep);
		$iUsersCntByStep = $iUsersCntByStep > 0 ? $iUsersCntByStep : 500;
		$dbItems = CUser::GetList(
			$sBy = 'UF_MEDALS_CALC_DATE',
			$sOrder = 'ASC',
			array(
				'ACTIVE' => 'Y',
				'LAST_LOGIN_1' => date('d.m.Y', (time() - (86400 * 365))) // выбираем только тех юзеров, которые заходили на сайт хотя бы год назад
			),
			array(
				'NAV_PARAMS' => array('nTopCount' => $iUsersCntByStep),
				'FIELDS' => array('ID')
			)
		);
		$arCheckMedals = self::GetMedalsList();
		while($arUser = $dbItems->Fetch()) {
			self::CalculateUserMedals($arUser['ID'], $arCheckMedals);
		}
	}

	public static function CalculateUserMedals($iUserId, $arCheckMedals = array()) {
		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			return;
		}
		$arCheckMedals = $arCheckMedals ? $arCheckMedals : self::GetMedalsList();
		foreach($arCheckMedals as $arMedalItem) {
			self::CalculateMedals($arMedalItem['ID'], $arMedalItem, $iUserId, true);
		}
		$obUser = new CUser();
		$obUser->Update($iUserId, array('UF_MEDALS_CALC_DATE' => date('d.m.Y H:i:s'), '_SKIP_FO_SYNC_' => 'Y'));
	}

	/**
	 * Получаем список медалей
	 * GetMedalsList
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetMedalsList($bRefreshCache = false) {
		$arReturn = array();
		// дополнительный параметр для ID кэша
		$sCacheAddParam = '';
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'GetMedalsList';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array('MEDALSLIST');
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/GetMedalsList/';
		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam, 'v2');

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {

			$iISBlockId = CProjectUtils::GetIBlockIdByCode('medals', 'NIYAMA_DISCOUNTS');
			$arTypeBASE_TYPE_IMG = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_TYPE_IMG');
			$arTypeBASE_TYPE_ACTION = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_TYPE_ACTION');
			$arTypeBASE_PERIOD_ACTION = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_PERIOD_ACTION');
			$arTypeORDER_TYPE_DELIVERY = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_TYPE_DELIVERY');
			$arTypeORDER_COUNT_BLUDS_YSLOVIE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_COUNT_BLUDS_YSLOVIE');
			$arTypeORDER_SOSTAV_BLUDS = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_SOSTAV_BLUDS');
			$arTypeORDER_IN_WHAT = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_IN_WHAT');
			$arTypeORDER_SUMM_ZACAZ_YSLOVIE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_SUMM_ZACAZ_YSLOVIE');
			$arTypeRECOMENDATIONSOC_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'RECOMENDATIONSOC_TYPE');

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();

			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC',
					),
					array(
						'IBLOCK_ID' => $iISBlockId,
						'ACTIVE' => 'Y',
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 'TIMESTAMP_X', 'ACTIVE', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT',  'DATE_CREATE',
						'PROPERTY_MEDAL_ID',
						'PROPERTY_BASE_STATUS_TYPE', 'PROPERTY_BASE_TYPE_IMG', 'PROPERTY_BASE_TYPE_IMG_DOWNLOAD',
						'PROPERTY_BASE_TYPE_ACTION', 'PROPERTY_BASE_COUNT_ACTION', 'PROPERTY_BASE_PERIOD_ACTION',
						'PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY', 'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S',
						'PROPERTY_BASE_TIME_ACTION_S', 'PROPERTY_BASE_TIME_ACTION_DO',
						'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO', 'PROPERTY_ORDER_TYPE_DELIVERY',
						'PROPERTY_ORDER_TYPE_BLUDS', 'PROPERTY_ORDER_INGRIDIENTS_BLUDS', 'PROPERTY_ORDER_SPEC_MENU_BLUDS',
						'PROPERTY_BASE_NUMBER_OF_ACTIONS',

						'PROPERTY_ORDER_BLUDS', 'PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE', 'PROPERTY_ORDER_COUNT_BLUDS',
						'PROPERTY_ORDER_SOSTAV_BLUDS', 'PROPERTY_ORDER_IN_WHAT', 'PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE',
						'PROPERTY_ORDER_SUMM_ZACAZ', 'PROPERTY_RECOMENDATIONSOC_TYPE'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					$arReturn[$arItem['ID']] = array(
						'ID' => $arItem['ID'],
						'ACTIVE' => $arItem['ACTIVE'],
						'TIMESTAMP_X' => $arItem['TIMESTAMP_X'],
						'DATE_CREATE' => $arItem['DATE_CREATE'],
						'_START_DATE_' => $arItem['DATE_CREATE'],
						'_START_DATE_TIMESTAMP_' => MakeTimeStamp($arItem['DATE_CREATE']),
						'NAME' => $arItem['NAME'],
						'PREVIEW_TEXT' => $arItem['PREVIEW_TEXT'],
						'DETAIL_TEXT' => $arItem['DETAIL_TEXT'],
						'PROPERTY_MEDAL_ID' => $arItem['PROPERTY_MEDAL_ID_VALUE'],
						'PROPERTY_BASE_STATUS_TYPE' => $arItem['PROPERTY_BASE_STATUS_TYPE_VALUE'],
						'PROPERTY_BASE_TYPE_IMG' => $arTypeBASE_TYPE_IMG[$arItem['PROPERTY_BASE_TYPE_IMG_VALUE']]['XML_ID'],
						'PROPERTY_BASE_TYPE_IMG_DOWNLOAD' => $arItem['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_VALUE'],
						'PROPERTY_BASE_TYPE_ACTION' => $arTypeBASE_TYPE_ACTION[$arItem['PROPERTY_BASE_TYPE_ACTION_VALUE']]['XML_ID'],
						'PROPERTY_BASE_COUNT_ACTION' => $arItem['PROPERTY_BASE_COUNT_ACTION_VALUE'],
						'PROPERTY_BASE_PERIOD_ACTION' => $arTypeBASE_PERIOD_ACTION[$arItem['PROPERTY_BASE_PERIOD_ACTION_VALUE']]['XML_ID'],
						'PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY' => $arItem['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY_VALUE'],
						'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S' => $arItem['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S_VALUE'],
						'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO' => $arItem['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO_VALUE'],
						'PROPERTY_BASE_TIME_ACTION_S' => $arItem['PROPERTY_BASE_TIME_ACTION_S_VALUE'],
						'PROPERTY_BASE_TIME_ACTION_DO' => $arItem['PROPERTY_BASE_TIME_ACTION_DO_VALUE'],
						'PROPERTY_BASE_NUMBER_OF_ACTIONS' => $arItem['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE'],
						'PROPERTY_ORDER_TYPE_DELIVERY' => $arTypeORDER_TYPE_DELIVERY[$arItem['PROPERTY_ORDER_TYPE_DELIVERY_VALUE']]['XML_ID'],
						'PROPERTY_ORDER_TYPE_BLUDS' => $arItem['PROPERTY_ORDER_TYPE_BLUDS_VALUE'],
						'PROPERTY_ORDER_INGRIDIENTS_BLUDS' => $arItem['PROPERTY_ORDER_INGRIDIENTS_BLUDS_VALUE'],
						'PROPERTY_ORDER_SPEC_MENU_BLUDS' => $arItem['PROPERTY_ORDER_SPEC_MENU_BLUDS_VALUE'],
						'PROPERTY_ORDER_BLUDS' => $arItem['PROPERTY_ORDER_BLUDS_VALUE'],
						'PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE' => $arTypeORDER_COUNT_BLUDS_YSLOVIE[$arItem['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE_VALUE']]['XML_ID'],
						'PROPERTY_ORDER_COUNT_BLUDS' => $arItem['PROPERTY_ORDER_COUNT_BLUDS_VALUE'],
						'PROPERTY_ORDER_SOSTAV_BLUDS' => $arTypeORDER_SOSTAV_BLUDS[$arItem['PROPERTY_ORDER_SOSTAV_BLUDS_VALUE']]['XML_ID'],
						'PROPERTY_ORDER_IN_WHAT' => $arTypeORDER_IN_WHAT[$arItem['PROPERTY_ORDER_IN_WHAT_VALUE']]['XML_ID'],
						'PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE' => $arTypeORDER_SUMM_ZACAZ_YSLOVIE[$arItem['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE_VALUE']]['XML_ID'],
						'PROPERTY_ORDER_SUMM_ZACAZ' => $arItem['PROPERTY_ORDER_SUMM_ZACAZ_VALUE'],
						'PROPERTY_RECOMENDATIONSOC_TYPE' => $arTypeRECOMENDATIONSOC_TYPE[$arItem['PROPERTY_RECOMENDATIONSOC_TYPE_VALUE']]['XML_ID']
					);
				}
			}
			if(empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	/**
	 * GetMedalDetail
	 * @param $iMedalId
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetMedalDetail($iMedalId, $bRefreshCache = false) {
		$iMedalId = intval($iMedalId);
		if($iMedalId > 0) {
			$arMedalList = self::GetMedalsList($bRefreshCache);
			return isset($arMedalList[$iMedalId]) ? $arMedalList[$iMedalId] : array();
		}
		return array();
	}

	public static function CheckMedalActive($arMedalData) {
		$bCheck = false;
		if(!empty($arMedalData) && $arMedalData['ACTIVE'] == 'Y' && $arMedalData['PROPERTY_BASE_STATUS_TYPE']) {
			$bCheck = true;
		}
		return $bCheck;
	}

	/**
	 * CheckMedalPeriod
	 * @param $arMedalData
	 * @param $iActionTimestamp
	 * @return bool
	 */
	public static function CheckMedalPeriod($arMedalData, $iActionTimestamp, $iUserId = 0) {
		# Период действия
		$bCheckPeriod = false;

		if($arMedalData['_START_DATE_TIMESTAMP_'] > $iActionTimestamp) {
			return $bCheckPeriod;
		}

		switch($arMedalData['PROPERTY_BASE_PERIOD_ACTION']) {
			case 'MONTH':
				// За месяц
				$iNextMonthTimestamp = strtotime('+ 1 months', $iActionTimestamp);
				if($iActionTimestamp <= $iNextMonthTimestamp) {
					$bCheckPeriod = true;
				}
			break;

			case 'BIRTHDAY':
				// В день рождения
				$sUserBirthday = CUsersData::GetPersonalBirthday($iUserId);
				$sCurDate = date('m.d', $iActionTimestamp);
				$iDaysPeriodLeft = isset($arMedalData['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY']) ? intval($arMedalData['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY']) : 0;
				$iDaysPeriodLeft = $iDaysPeriodLeft > 0 ? $iDaysPeriodLeft : 0;
				$iDaysPeriodRight = $iDaysPeriodLeft;
				$iLeftTmstmp = strtotime('-'.$iDaysPeriodLeft.' day', $iActionTimestamp);
				$iRightTmstmp = strtotime('+'.$iDaysPeriodLeft.' day', $iActionTimestamp);
				$sLeftDate = date('m.d', $iLeftTmstmp);
				$sRightDate = date('m.d', $iRightTmstmp);
				if(strlen($sUserBirthday)) {
					$obTmpDate = date_create($sUserBirthday);
					$sUserBirthday = $obTmpDate ? $obTmpDate->Format('m.d') : '';
				}
				if(strlen($sUserBirthday)) {
					if($sLeftDate > $sRightDate) {
						// когда промежуток приходится на начало года: 12 - 01 месяцы, например
						if($sLeftDate <= $sUserBirthday || $sRightDate >= $sUserBirthday) {
							$bCheckPeriod = true;
						}
					} else {
						// обычный промежуток: 01 - 02 месяцы, например
						if($sLeftDate <= $sUserBirthday && $sRightDate >= $sUserBirthday) {
							$bCheckPeriod = true;
						}
					}
				}
			break;

			case 'CUSTOM':
				// Выбрать период
				$iCS = strtotime($arMedalData['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S']);
				$iCDO = strtotime($arMedalData['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO']);
				if($iActionTimestamp >= $iCS && $iActionTimestamp <= $iCDO) {
					$bCheckPeriod = true;
				}
			break;

			case 'ANY':
				// Любой период
				$bCheckPeriod = true;
			break;

			default:
				// Не задано = любой период
				$bCheckPeriod = true;
			break;
		}

		$bCheckTime = false;
		if($bCheckPeriod) {
			if(empty($arMedalData['PROPERTY_BASE_TIME_ACTION_S']) && empty($arMedalData['PROPERTY_BASE_TIME_ACTION_DO'])) {
				$bCheckTime = true;
			} else {
				$sFormatTime = '%H:%M:%S';
				// c
				$sStart = !empty($arMedalData['PROPERTY_BASE_TIME_ACTION_S']) ? $arMedalData['PROPERTY_BASE_TIME_ACTION_S'] : '00:00:00';
				$arStart = strptime($sStart, $sFormatTime);
				$iStartSec = ($arStart['tm_hour'] * 60 + $arStart['tm_min']) * 60 + $arStart['tm_sec'];

				// по
				$sEnd = !empty($arMedalData['PROPERTY_BASE_TIME_ACTION_S']) ? $arMedalData['PROPERTY_BASE_TIME_ACTION_DO'] : '23:59:59';
				$arEnd = strptime($sEnd, $sFormatTime);
				$iEndSec = ($arEnd['tm_hour'] * 60 + $arEnd['tm_min']) * 60 + $arEnd['tm_sec'];

				// время выполнения операции, для которой определяется возможность получения медали
				$sActionTime = strftime($sFormatTime, $iActionTimestamp);
				$arActionTime = strptime($sActionTime, $sFormatTime);
				$iActionSec = ($arActionTime['tm_hour'] * 60 + $arActionTime['tm_min']) * 60 + $arActionTime['tm_sec'];

				if(CProjectUtils::IsTimeInPeriod($iActionSec, $iStartSec, $iEndSec)) {
					$bCheckTime = true;
				}
			}
		}

		return ($bCheckPeriod && $bCheckTime);
	}

	protected static function IsActual($arMedalData, $iUserId) {
		$bReturn = false;
		$iMaxCountMedals = empty($arMedalData['PROPERTY_BASE_COUNT_ACTION']) ? 1 : $arMedalData['PROPERTY_BASE_COUNT_ACTION'];
		$iUserMedalsCnt = self::GetUserMedalsCnt($arMedalData['ID'], $iUserId);
		if($iUserMedalsCnt < $iMaxCountMedals) {
			$bReturn = true;
		}
		return $bReturn;
	}

	/**
	 * CalculateMedals
	 * @param $iMedalId
	 * @param array $arMedalData
	 * @param bool $iUserId
	 * @param bool $bCacheLastUserOrders
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function CalculateMedals($iMedalId, $arMedalData = array(), $iUserId = false, $bCacheLastUserOrders = false, $bRefreshCache = false) {
		$arReturn = array();

		$iUserId = $iUserId === false ? $GLOBALS['USER']->GetId() : $iUserId;
		if(!$arMedalData) {
			$arMedalData = self::GetMedalDetail($iMedalId, $bRefreshCache);
		}
		if(!$arMedalData) {
			return $arReturn;
		}

		CModule::IncludeModule('sale');
		$bCheckIncCount = true;
		$arActionsCheck = array();
		$arOrdersCheck = array();

//_log_array(array('$arMedalData' => $arMedalData), __FUNCTION__);

		// Проверяем на активнось
		if(self::CheckMedalActive($arMedalData) && self::IsActual($arMedalData, $iUserId)) {

			// если медаль данного типа ранее уже получалась юзером, то получим информацию об уже задействованных заказах, товарах, действиях, чтобы их исключить при текущей проверки достижения медали
			$arMedalData['_ALREADY_USED_ENTITIES_'] = self::GetUserMedalInfo($arMedalData['ID'], $iUserId);

			switch($arMedalData['PROPERTY_BASE_TYPE_ACTION']) {
				case 'ALL':
					// Все типы действий
					/*
					$arActionsCheck = self::IsSocAuth($iUserId, $arMedalData);
					$arActionsCheck = self::IsSocRecommendationLivel($iUserId, $arMedalData);
					$arActionsCheck = self::IsSocRecommendationWidget($iUserId, $arMedalData);
					$arActionsCheck = self::IsSurvey($iUserId, $arMedalData);

					$bSetMedal = ($bSocAuth && $bRecLevel && $bRecWidget && $bSurvey) ? true : false;
					*/
				break;

				case 'AUTHSOC':
					// Авторизация через соцсеть
					$arActionsCheck = self::IsSocAuth($iUserId, $arMedalData);
				break;

				case 'SURVEY':
					// Участие в опросе
					$arActionsCheck = self::IsSurvey($iUserId, $arMedalData);
				break;

				case 'ORDER':
					// За заказ
					$arOrdersCheck = self::CheckOrders($iUserId, $arMedalData, $bCacheLastUserOrders);
				break;

				case 'RECOMENDATIONSOC':
					// Рекомендация в соцсетях
					if($arMedalData['PROPERTY_RECOMENDATIONSOC_TYPE'] == 'ANY') {
						# Если любая рекомендация
						$arActionsCheck = self::IsSocRecommendationLivel($iUserId, $arMedalData);
						if(!$arActionsCheck) {
							$arActionsCheck = self::IsSocRecommendationWidget($iUserId, $arMedalData);
						}
					} elseif($arMedalData['PROPERTY_RECOMENDATIONSOC_TYPE'] == 'TWEET_YROVEN_OR_MEDAL') {
						# Если рекомендовали через Поздравление
						$arActionsCheck = self::IsSocRecommendationLivel($iUserId, $arMedalData);
					} elseif($arMedalData['PROPERTY_RECOMENDATIONSOC_TYPE'] == 'RECOMENDATON_FROM_VIDZHET') {
						# Если мы рекомедовали через виджет
						$arActionsCheck = self::IsSocRecommendationWidget($iUserId, $arMedalData);
					}
				break;
			}
		}

//_log_array(array('$arActionsCheck' => $arActionsCheck), '$arActionsCheck');

		// выпишем медали за какие-то действия (авторизация через соцсети, участие в опросе и т.п.)
		if($arActionsCheck && $arActionsCheck['SUCCESS_ACTIONS']) {
			$iRepsRequired = isset($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) && intval($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) > 1 ? intval($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) : 1;
			if(count($arActionsCheck['SUCCESS_ACTIONS']) >= $iRepsRequired) {
				// разделим по количеству действий установленного типа - по сути, столько раз можно выписать медаль
				$arChunks = array_chunk($arActionsCheck['SUCCESS_ACTIONS'], $iRepsRequired);
				foreach($arChunks as $arTmpActions) {
					// укажем ID действий, чтобы за них больше нельзя было получить медаль
					$arTmpMedalInfo = array();
					foreach($arTmpActions as $iTmpActionId) {
						$arTmpMedalInfo[] = array(
							'UF_ACTION_ID' => $iTmpActionId
						);
					}
					$iNewUserMedalId = intval(self::SetMedal($arMedalData['ID'], $iUserId, $arTmpMedalInfo));
					if($iNewUserMedalId == -1) {
						// $iNewUserMedalId == -1 - лимит по медалям уже исчерпан, не будем дальше делать попытки добавить медаль
						break;
					} elseif($iNewUserMedalId > 0) {
						$arReturn[] = $iNewUserMedalId;
					}
				}
			}
		}

		// выпишем медали за заказы
		if($arOrdersCheck) {
			// инфа по заказам уже приходит разбитая на медали
			foreach($arOrdersCheck as $arTmpMedal) {
				if($arTmpMedal['ORDERS']) {
					// укажем ID заказов (и товаров, если заданы), чтобы за них больше нельзя было получить медаль
					$arTmpMedalInfo = array();
					foreach($arTmpMedal['ORDERS'] as $iTmpOrderId => $arRepInfo) {
						$arTmpMedalInfo[] = array(
							'UF_ORDER_ID' => $iTmpOrderId,
							'UF_PRODUCT_ID' => $arRepInfo['PRODUCT_ID'] ? $arRepInfo['PRODUCT_ID'] : ''
						);
					}
					$iNewUserMedalId = intval(self::SetMedal($arMedalData['ID'], $iUserId, $arTmpMedalInfo));
					if($iNewUserMedalId == -1) {
						// $iNewUserMedalId == -1 - лимит по медалям уже исчерпан, не будем дальше делать попытки добавить медаль
						break;
					} elseif($iNewUserMedalId > 0) {
						$arReturn[] = $iNewUserMedalId;
					}
				}
			}
		}

		return $arReturn;
	}

	/**
	 * CheckOrders
	 * @param int $iUserId
	 * @param array $arMedalData
	 * @param bool $bCacheLastUserOrders
	 * @return array
	 */
	protected static function CheckOrders($iUserId = false, $arMedalData = array(), $bCacheLastUserOrders = false) {
		$arReturn = array();
		// блюда, которые внесли вклад в получение медали. Представляет собой многомерный массив, 
		// где: первый ключ - индекс выполнения условия (для подсчета количества действий), второй - ID заказа, значение - массив с доп.описаниями (пока только PRODUCT_ID)
		$arWithdrawOrders = array();

		$iUserId = $iUserId === false ? intval($GLOBALS['USER']->GetId()) : intval($iUserId);
		if($iUserId <= 0) {
			return $arReturn;
		}

		CModule::IncludeModule('sale');

		static $arStaticLastUserCache = array();

		$arWorkOrders = array();
		if(!$bCacheLastUserOrders || !$arStaticLastUserCache['USER_ID'] || $arStaticLastUserCache['USER_ID'] != $iUserId) {
			$arStaticLastUserCache['USER_ID'] = $iUserId;
			$arStaticLastUserCache['USER_ORDERS'] = array();
			$arStaticLastUserCache['WORK_ORDERS'] = array();
			// по умолчанию делаем общую выборку по заказам юзера
			$arTmpFilter = array(
				'USER_ID' => $iUserId,
				'CANCELED' => 'N', 
				'@STATUS_ID' => array('F', 'N'),
			);
			if(!$bCacheLastUserOrders) {
				// если не сохраняем в статическом кэше, то сразу отфильтруем по дате начала активности медали, также дата проверяется в self::CheckMedalPeriod()
				$arTmpFilter['>=DATE_INSERT'] = $arMedalData['_START_DATE_'];
			}
			$dbOrder = CSaleOrder::GetList(
				array(
					// сортировка важна такая!
					'DATE_INSERT' => 'ASC', 
					'ID' => 'ASC'
				),
				$arTmpFilter,
				false,
				false,
				array('ID', 'DATE_INSERT')
			);
			while($arOrd = $dbOrder->Fetch()) {
				if($arOrd['DATE_INSERT']) {
					$arOrd['_iActionTimestamp_'] = strtotime($arOrd['DATE_INSERT']);
					$arStaticLastUserCache['USER_ORDERS'][] = $arOrd;
				}
			}
		}

		foreach($arStaticLastUserCache['USER_ORDERS'] as $iIndexNum => $arOrd) {
			if(self::CheckMedalPeriod($arMedalData, $arOrd['_iActionTimestamp_'], $iUserId)) {
				$arTmpOrder = $arStaticLastUserCache['WORK_ORDERS'][$arOrd['ID']] ? $arStaticLastUserCache['WORK_ORDERS'][$arOrd['ID']] : CNiyamaOrders::GetOrderById($arOrd['ID']);
				if($arTmpOrder['ORDER_PROPS']) {
					// оставим только свойство DELIVERY_TYPE
					if($arTmpOrder['ORDER_PROPS']['DELIVERY_TYPE']) {
						$arTmpOrder['ORDER_PROPS'] = array(
							'DELIVERY_TYPE' => $arTmpOrder['ORDER_PROPS']['DELIVERY_TYPE']
						);
					} else {
						$arTmpOrder['ORDER_PROPS'] = array();
					}
				}
				if($bCacheLastUserOrders && !isset($arStaticLastUserCache['WORK_ORDERS'][$arOrd['ID']])) {
					$arStaticLastUserCache['WORK_ORDERS'][$arOrd['ID']] = $arTmpOrder;
				}
				if($arTmpOrder) {
					$arWorkOrders[] = $arTmpOrder;
				}
			}
		}
		if(!$bCacheLastUserOrders) {
			$arStaticLastUserCache['USER_ID'] = 0;
			$arStaticLastUserCache['USER_ORDERS'] = array();
			$arStaticLastUserCache['WORK_ORDERS'] = array();
		}

		//
		// Тип доставки (выпадающий список с единичным выбором). Значениями списка являются: самовывоз, с курьером;
		//
		$sCheckDeliveryType = '';
		if($arMedalData['PROPERTY_ORDER_TYPE_DELIVERY'] == 'SAMOVYVOZ') {
			$sCheckDeliveryType = 'self';
		} elseif($arMedalData['PROPERTY_ORDER_TYPE_DELIVERY'] == 'CURIER') {
			$sCheckDeliveryType = 'courier';
		}


//_log_array(array('$iUserId' => $iUserId, '$arMedalData' => $arMedalData, '$arWorkOrders' => $arWorkOrders, '$sCheckDeliveryType' => $sCheckDeliveryType), '$arWorkOrders1');

		if($sCheckDeliveryType) {
			foreach($arWorkOrders as $iKey => $arOrd) {
				if(CNiyamaOrders::_GetOrderPropValue($arOrd['ORDER_PROPS'], 'DELIVERY_TYPE') != $sCheckDeliveryType) {
					unset($arWorkOrders[$iKey]);
				}
			}
		}

//_log_array(array($iUserId, $arMedalData, $arWorkOrders), '$arWorkOrders2');


		//
		// Сумма заказа. Для блока может задаваться набор свойств:
		//  Условие (список с единичным выбором):
		//   Сумма равна. Условие выполнено, только если сумма заказа равна заданной сумме;
		//   Минимальная сумма. Условие выполнено, только если сумма заказа равна или больше заданной сумме;
		//   Максимальная сумма. Условие выполнено, только если сумма заказа равна или меньше заданной суммы.
		//  Сумма в рублях.
		//
		if(!empty($arWorkOrders)) {
			if(!empty($arMedalData['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE'])) {
				if($arMedalData['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE'] == 'RAVNA') {
					foreach($arWorkOrders as $iKey => $arOrd) {
						if($arOrd['ORDER_CART']['TOTAL_PRICE'] != $arMedalData['PROPERTY_ORDER_SUMM_ZACAZ']) {
							unset($arWorkOrders[$iKey]);
						}
					}
				} elseif($arMedalData['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE'] == 'MIN') {
					foreach($arWorkOrders as $iKey => $arOrd) {
						// если сумма заказа меньше заданной суммы, то условие не выполнено
						if($arOrd['ORDER_CART']['TOTAL_PRICE'] < $arMedalData['PROPERTY_ORDER_SUMM_ZACAZ']) {
							unset($arWorkOrders[$iKey]);
						}
					}
				} elseif($arMedalData['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE'] == 'MAX') {
					foreach($arWorkOrders as $iKey => $arOrd) {
						// если сумма заказа больше заданной суммы, то условие не выполнено
						if($arOrd['ORDER_CART']['TOTAL_PRICE'] > $arMedalData['PROPERTY_ORDER_SUMM_ZACAZ']) {
							unset($arWorkOrders[$iKey]);
						}
					}
				}
			}
		}

		//
		// Сведения о блюдах. Для блока доступны следующие параметры:
		//  - Список всех активных параметров, заданных для фасетного поиска (тип блюда, ингредиенты, специальное меню). 
		//  Для каждого параметра выводится поле с выпадающим списком всех активных  значений для множественного выбора. 
		//  По умолчанию для каждого параметра задается значение «Все <название параметра>» (например, «Все типы блюд»). 
		//  - Блюда (выпадающий список с множественным выбором). Список содержит названия всех активных блюд. По умолчанию для параметра задается значение «Все блюда». 
		//  Списки значений параметра «Блюда» и всех параметров, заданных для фасетного поиска, должны быть взаимозависимыми. То есть, если для параметра «Ингредиент» выбрано значение «Рыба», 
		//  то в списке параметра «Блюда» должны остаться названия блюд, которые связаны со значение «Рыба».
		//
		// Пример:
		// Условие по ингредиентам: икра
		// Количество блюд: 5
		// Состав: только разные позиции из списка
		// В каком заказе: подряд в каждом
		// Сумма заказа: мин. 2000
		// У юзера 7 заказов, включающих по одному разному блюду с икрой подряд, но один из заказов, который четвертый по счету, имеет сумму меньше 2 тыс - значит юзер не получает медали.
		//
		$arSearchProducts = array();
		if($arWorkOrders) {
			// сформируем фильтры для получения списка блюд, которые будем искать в заказах
			$arPFilter = array();
			// фильтр по типам блюд
			$arMedalData['PROPERTY_ORDER_TYPE_BLUDS'] = !empty($arMedalData['PROPERTY_ORDER_TYPE_BLUDS']) && !is_array($arMedalData['PROPERTY_ORDER_TYPE_BLUDS']) ? array($arMedalData['PROPERTY_ORDER_TYPE_BLUDS']) : $arMedalData['PROPERTY_ORDER_TYPE_BLUDS'];
			if(!empty($arMedalData['PROPERTY_ORDER_TYPE_BLUDS']) && is_array($arMedalData['PROPERTY_ORDER_TYPE_BLUDS'])) {
				$arPFilter['SECTION_ID'] = $arMedalData['PROPERTY_ORDER_TYPE_BLUDS'];
				$arPFilter['INCLUDE_SUBSECTIONS'] = 'Y';
			}

			// фильтр по специальному меню
			$arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS'] = !empty($arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS']) && !is_array($arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS']) ? array($arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS']) : $arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS'];
			if(!empty($arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS']) && is_array($arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS'])) {
				$arPFilter['PROPERTY_SPEC_MENU.ID'] = $arMedalData['PROPERTY_ORDER_SPEC_MENU_BLUDS'];
			}

			// фильтр по ингредиентам
			$arFilterByIngs = array();
			$arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS'] = !empty($arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS']) && !is_array($arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS']) ? array($arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS']) : $arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS'];
			if(!empty($arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS']) && is_array($arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS'])) {
				$arFilterByIngs = $arMedalData['PROPERTY_ORDER_INGRIDIENTS_BLUDS'];
			}

			// фильтр по заданным блюдам
			$arMedalData['PROPERTY_ORDER_BLUDS'] = !empty($arMedalData['PROPERTY_ORDER_BLUDS']) && !is_array($arMedalData['PROPERTY_ORDER_BLUDS']) ? array($arMedalData['PROPERTY_ORDER_BLUDS']) : $arMedalData['PROPERTY_ORDER_BLUDS'];
			if(!empty($arMedalData['PROPERTY_ORDER_BLUDS']) && is_array($arMedalData['PROPERTY_ORDER_BLUDS'])) {
				$arPFilter['ID'] = $arMedalData['PROPERTY_ORDER_BLUDS'];
			}

			if($arPFilter || $arFilterByIngs) {
				// получим список блюд, которые будем искать в заказах
				$arMenuItems = CNiyamaCatalog::GetList(null, $arPFilter, null, false);
				if($arMenuItems['ITEMS']) {
					foreach($arMenuItems['ITEMS'] as $arItem) {
						$bOk = true;
						if($arFilterByIngs) {
							$arMissIngs = $arItem['ING']['ALL_INGREDIENT'] ? array_diff($arFilterByIngs, array_keys($arItem['ING']['ALL_INGREDIENT'])) : $arFilterByIngs;
							if($arMissIngs) {
								$bOk = false;
							}
						}
						if($bOk) {
							$arSearchProducts[$arItem['INFO']['ID']] = $arItem['INFO']['ID'];
						}
					}
				}
			} else {
				// если не заданы условия для фильтра, то считаем, что условие выполнено, 
				// текущий список заказов записываем на счет получения медали
				$iReps = 0;
				foreach($arWorkOrders as $arOrd) {
					$arWithdrawOrders[$iReps][$arOrd['ORDER']['ID']] = array();
					++$iReps;
				}
			}
		}

//_log_array(array($arMedalData, $arWithdrawOrders, $arSearchProducts), 'faset');
		$bCheckSequence = false;
		//
		// Блок фильтрации, если были заданы сведения о блюдах и такие блюда были найдены
		//
		if($arSearchProducts) {
			$arInOrder = array();

			// соберем массивы ранее использованных товаров и заказов
			$arUsedOrders = array();
			$arUsedOrdersProducts = array();
			if($arMedalData['_ALREADY_USED_ENTITIES_']) {
				foreach($arMedalData['_ALREADY_USED_ENTITIES_'] as $arTmpItem) {
					if($arTmpItem['UF_PRODUCT_ID'] && $arTmpItem['UF_ORDER_ID']) {
						// это для тех случаев, когда следует исключать лишь определенные товары из определенных заказов
						$arUsedOrdersProducts[$arTmpItem['UF_PRODUCT_ID']][$arTmpItem['UF_ORDER_ID']] = $arTmpItem['UF_ORDER_ID'];
					}
					if(!$arTmpItem['UF_PRODUCT_ID'] && $arTmpItem['UF_ORDER_ID']) {
						// это для тех случаев, когда следует исключать полностью весь заказ
						$arUsedOrders[$arTmpItem['UF_ORDER_ID']] = $arTmpItem['UF_ORDER_ID'];
					}
				}
			}

			$arTmpTableTypes = array('current', 'guest', 'common');
			foreach($arWorkOrders as $arOrder) {
				$iTmpOrderId = $arOrder['ORDER']['ID'];
				if($arUsedOrders[$iTmpOrderId]) {
					// заказ уже использован при получении этой медали
					continue;
				}
				foreach($arTmpTableTypes as $sTable) {
					if(!empty($arOrder['ORDER_CART'][$sTable])) {
						foreach($arOrder['ORDER_CART'][$sTable] as $arTable) {
							if($arTable['ITEMS'] && is_array($arTable['ITEMS'])) {
								foreach($arTable['ITEMS'] as $arProd) {
									if($arUsedOrdersProducts[$arProd['PRODUCT_ID']][$iTmpOrderId]) {
										// товар заказа уже использован при получении этой медали
										continue;
									}

// !!!
// Здесь еще нужно добавить проверку на то, что товар не бонусный
// !!!
									if($arSearchProducts[$arProd['PRODUCT_ID']]) {
										if(!isset($arInOrder[$iTmpOrderId][$arProd['PRODUCT_ID']])) {
											$arInOrder[$iTmpOrderId][$arProd['PRODUCT_ID']] = 0;
										}
										$arInOrder[$iTmpOrderId][$arProd['PRODUCT_ID']] += $arProd['QUANTITY'];
									}
								}
							}
						}
					}
				}
			}

//_log_array(array($arMedalData, $arInOrder), '$arInOrder');
			if($arInOrder) {
				// условие по количеству блюд
				$iCheckQnt = isset($arMedalData['PROPERTY_ORDER_COUNT_BLUDS']) ? intval($arMedalData['PROPERTY_ORDER_COUNT_BLUDS']) : 0;
				$iCheckQnt = $iCheckQnt > 0 ? $iCheckQnt : 0;

				// Условие "В каком заказе"
				$arMedalData['PROPERTY_ORDER_IN_WHAT'] = strlen($arMedalData['PROPERTY_ORDER_IN_WHAT']) ? $arMedalData['PROPERTY_ORDER_IN_WHAT'] : 'ANY';
				// Условие "Состав"
				$arMedalData['PROPERTY_ORDER_SOSTAV_BLUDS'] = strlen($arMedalData['PROPERTY_ORDER_SOSTAV_BLUDS']) ? $arMedalData['PROPERTY_ORDER_SOSTAV_BLUDS'] : 'ANY';

				$iReps = 0;
				if($arMedalData['PROPERTY_ORDER_IN_WHAT'] == 'SUCCESSION') {
					$bCheckSequence = true;
				}

				// работаем по условию "Состав"
				switch($arMedalData['PROPERTY_ORDER_SOSTAV_BLUDS']) {
					// ---
					// Состав: Любые позиции из списка
					// Если заданы значения параметров фасетного поиска, то условие выполняется, если заказаны разные и/или одинаковые наименования блюд с указанными признаками;
					case 'ANY':
						if(!strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
							// если не задано условие, то считаем, что каждый заказ из текущего списка выполнил условие
							// Пример медали: "За отвагу" - в случае заказа особо острых блюд
							// Пример медали: "Медаль энергетика" - за заказ энергетического напитка
							foreach($arInOrder as $iTmpOrderId => $arOrderProducts) {
								$arWithdrawOrders[$iReps][$iTmpOrderId] = array();
								++$iReps;
							}
							continue;
						}

						// работаем по условию "В каком заказе"
						switch($arMedalData['PROPERTY_ORDER_IN_WHAT']) {
							// В каком заказе: В любом заказе
							// Для выполнения условия учитываются все заказы пользователя, в которых были заказаны указанные названия блюд, блюда с выбранными признаками.
							// Если задано определенное количество блюд, которое нужно заказать, то для выполнения условия суммируются все блюда с заданными признаками во всех заказах;
							case 'ANY':
							// В каком заказе: Подряд в каждом заказе
							// Условие выполнено, если в каждом заказе были блюда с указанными признаками. 
							// Если задано количество действий (заказов), то считается, сколько раз подряд было заказано блюдо с указанными признаками. 
							case 'SUCCESSION':
								if(strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									// иначе проверяем соответствие заданному условию
									// Пример медали: "Медаль 4 сыра" - за заказ 4 блюд с сыром
									$iTmpCount = 0;
									$arTmpOrdersChain = array();
									$bTmpSuccess = false;
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpProductId => $iTmpProductQnt) {
											$iTmpCount += $iTmpProductQnt;
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											// запоминаем заказ, который внес вклад в достижение общего количества (заказов может быть много)
											$arTmpOrdersChain[$iOrderId] = $iOrderId;
											if($iTmpCount > $iCheckQnt) {
												$bTmpSuccess = true;
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											if($iTmpCount == $iCheckQnt) {
												// запоминаем заказ, который выполнил условие
												$arTmpOrdersChain[$iOrderId] = $iOrderId;
												$bTmpSuccess = true;
											}
											// здесь обнуляем счетчик на каждом заказе
											$iTmpCount = 0;
										}

										if($bTmpSuccess) {
											foreach($arTmpOrdersChain as $iTmpOrderId) {
												$arWithdrawOrders[$iReps][$iTmpOrderId] = array();
											}
											++$iReps;
											$arTmpOrdersChain = array();
											$bTmpSuccess = false;
											$iTmpCount = 0;
										}
									}
								}
							break;

							// В каком заказе: В одном заказе
							// Условие выполнено, если указанные названия блюд, блюда с выбранными признаками, указанное количество блюд и т.д. были заказаны в одном заказе;
							case 'ONE':
								if(strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									// иначе проверяем соответствие заданному условию
									// Пример медали: "Супа-стар" - за заказ 3 супов одновременно
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										$bTmpSuccess = false;
										$iTmpCount = 0;
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpProductId => $iTmpProductQnt) {
											$iTmpCount += $iTmpProductQnt;
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											if($iTmpCount > $iCheckQnt) {
												$bTmpSuccess = true;
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											if($iTmpCount == $iCheckQnt) {
												$bTmpSuccess = true;
											}
										}

										if($bTmpSuccess) {
											$arWithdrawOrders[$iReps][$iOrderId] = array();
											++$iReps;
										}
									}
								}
							break;
						}
					break;

					// ---
					// Состав: Только разные позиции из списка
					// Если заданы значения параметров фасетного поиска, то условие выполняется, если заказаны разные наименования блюд с указанными признаками;
					case 'DIFFERENT':
						if(count($arSearchProducts) <= 1) {
							// если товар всего один, то условие по поиску разных позиций не имеет логики - условие получение медали задано некорректно
							continue;
						}
						// работаем по условию "В каком заказе"
						switch($arMedalData['PROPERTY_ORDER_IN_WHAT']) {
							// В каком заказе: В любом заказе
							// Для выполнения условия учитываются все заказы пользователя, в которых были заказаны указанные названия блюд, блюда с выбранными признаками.
							// Если задано определенное количество блюд, которое нужно заказать, то для выполнения условия суммируются все блюда с заданными признаками во всех заказах;
							case 'ANY':
							// В каком заказе: Подряд в каждом заказе
							// Условие выполнено, если в каждом заказе были блюда с указанными признаками. 
							// Если задано количество действий (заказов), то считается, сколько раз подряд было заказано блюдо с указанными признаками. 
							case 'SUCCESSION':
								if(!strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									// если не задано условие, то включаем только те заказы, которые содержат все возможные блюда из списка
									// Пример медали: "За вклад в сушиедство" - за то, что заказаны все возможные позиции суши
									// Пример медали: "I love NY" - за заказ ролла «Нью Йорк», чизкейка «Нью Йорк»
									$arTmpProductsLeft = $arSearchProducts;
									$arTmpOrdersChain = array();
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(isset($arTmpProductsLeft[$iTmpRpoductId])) {
												unset($arTmpProductsLeft[$iTmpRpoductId]);
												$arTmpOrdersChain[$iOrderId] = $iOrderId;
											}
											if(!$arTmpProductsLeft) {
												foreach($arTmpOrdersChain as $iTmpOrderId) {
													$arWithdrawOrders[$iReps][$iTmpOrderId] = $iTmpOrderId;
												}
												++$iReps;
												$arTmpProductsLeft = $arSearchProducts;
												$arTmpOrdersChain = array();
												break;
											}
										}
									}
								} else {
									// иначе проверяем соответствие заданному условию
									// Пример медали: "Некоторые любят погорячее" - за заказ 5 разных горячих блюд
									// Пример медали: "За острые ощущения" - За заказ 5 разных острых блюд
									$arTmpOrdersChain = array();
									$arTmpProductsUsed = array();
									$bTmpSuccess = false;
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(!$arTmpProductsUsed[$iTmpRpoductId]) {
												$arTmpProductsUsed[$iTmpRpoductId] = $iTmpRpoductId;
												// запоминаем заказ, который внес вклад в достижение общего количества (заказов может быть много)
												$arTmpOrdersChain[$iOrderId] = $iOrderId;
											}
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											if(count($arTmpProductsUsed) > $iCheckQnt) {
												$bTmpSuccess = true;
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											if(count($arTmpProductsUsed) == $iCheckQnt) {
												// запоминаем заказ, который выполнил условие
												$arTmpOrdersChain = array();
												$arTmpOrdersChain[$iOrderId] = $iOrderId;
												$bTmpSuccess = true;
											}
											// здесь обнуляем счетчик на каждом заказе
											$arTmpProductsUsed = array();
										}

										if($bTmpSuccess) {
											foreach($arTmpOrdersChain as $iTmpOrderId) {
												$arWithdrawOrders[$iReps][$iTmpOrderId] = array();
											}
											++$iReps;
											$arTmpProductsUsed = array();
											$arTmpOrdersChain = array();
											$bTmpSuccess = false;
										}
									}
								}
							break;

							// В каком заказе: В одном заказе
							// Условие выполнено, если указанные названия блюд, блюда с выбранными признаками, указанное количество блюд и т.д. были заказаны в одном заказе;
							case 'ONE':
								// примеры медалей такие же как и для условий "В любом заказе", только в пределах одного заказа
								if(!strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										$arTmpProductsLeft = $arSearchProducts;
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(isset($arTmpProductsLeft[$iTmpRpoductId])) {
												unset($arTmpProductsLeft[$iTmpRpoductId]);
											}
											if(!$arTmpProductsLeft) {
												$arWithdrawOrders[$iReps][$iOrderId] = array();
												++$iReps;
												break;
											}
										}
									}
								} else {
									// иначе проверяем соответствие заданному условию
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										$bTmpSuccess = false;
										$arTmpProductsUsed = array();
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(!$arTmpProductsUsed[$iTmpRpoductId]) {
												$arTmpProductsUsed[$iTmpRpoductId] = $iTmpRpoductId;
											}
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											// запоминаем заказ, который внес вклад в достижение общего количества (заказов может быть много)
											if(count($arTmpProductsUsed) > $iCheckQnt) {
												$bTmpSuccess = true;
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											if(count($arTmpProductsUsed) == $iCheckQnt) {
												$bTmpSuccess = true;
											}
										}

										if($bTmpSuccess) {
											$arWithdrawOrders[$iReps][$iOrderId] = array();
											++$iReps;
										}
									}
								}
							break;
						}
					break;

					// ---
					// Состав: Только одинаковые позиции из списка
					// Если заданы значения параметров фасетного поиска, то условие выполняется, если заказаны одинаковые наименования блюд с указанными признаками;
					case 'SAME':
						// работаем по условию "В каком заказе"
						switch($arMedalData['PROPERTY_ORDER_IN_WHAT']) {
							// В каком заказе: В любом заказе
							// Для выполнения условия учитываются все заказы пользователя, в которых были заказаны указанные названия блюд, блюда с выбранными признаками.
							// Если задано определенное количество блюд, которое нужно заказать, то для выполнения условия суммируются все блюда с заданными признаками во всех заказах;
							case 'ANY':
							// В каком заказе: Подряд в каждом заказе
							// Условие выполнено, если в каждом заказе были блюда с указанными признаками. 
							// Если задано количество действий (заказов), то считается, сколько раз подряд было заказано блюдо с указанными признаками. 
							case 'SUCCESSION':
								if(!strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									// ???
									// возможно, условие задано неверно
								} else {
									// иначе проверяем соответствие заданному условию
									// Пример медали: медаль дается за заказ 3 одинаковых суши. Без разницы в одном заказе или подряд.
									$arTmpOrdersChain = array();
									$arTmpProductsUsed = array();
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(!$arTmpProductsUsed[$iTmpRpoductId]) {
												$arTmpProductsUsed[$iTmpRpoductId] = 0;
											}
											$arTmpProductsUsed[$iTmpRpoductId] += $iTmpProductQnt;
											$arTmpOrdersChain[$iTmpRpoductId][$iOrderId] = $iOrderId;
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											foreach($arTmpProductsUsed as $iTmpRpoductId => $iTmpProductQnt) {
												if($iTmpProductQnt > $iCheckQnt) {
													foreach($arTmpOrdersChain[$iTmpRpoductId] as $iTmpOrderId) {
														$arWithdrawOrders[$iReps][$iTmpOrderId] = array('PRODUCT_ID' => $iTmpRpoductId);
													}
													++$iReps;
													// сбрасываем счетчики только по товару, который удовлетворил условиям
													$arTmpProductsUsed[$iTmpRpoductId] = 0;
													$arTmpOrdersChain[$iTmpRpoductId] = array();
												}
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											foreach($arTmpProductsUsed as $iTmpRpoductId => $iTmpProductQnt) {
												if($iTmpProductQnt == $iCheckQnt) {
													foreach($arTmpOrdersChain[$iTmpRpoductId] as $iTmpOrderId) {
														$arWithdrawOrders[$iReps][$iTmpOrderId] = array('PRODUCT_ID' => $iTmpRpoductId);
													}
													++$iReps;
													// сбрасываем счетчики только по товару, который удовлетворил условиям
													$arTmpProductsUsed[$iTmpRpoductId] = 0;
													$arTmpOrdersChain[$iTmpRpoductId] = array();
												}
											}
										}
									}
								}
							break;

							// В каком заказе: В одном заказе
							// Условие выполнено, если указанные названия блюд, блюда с выбранными признаками, указанное количество блюд и т.д. были заказаны в одном заказе;
							case 'ONE':
								// примеры медалей такие же как и для условий "В любом заказе", только в пределах одного заказа
								if(!strlen($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'])) {
									// ???
									// возможно, условие задано неверно
								} else {
									// иначе проверяем соответствие заданному условию
									// Пример медали: медаль дается за заказ 3 одинаковых суши. Без разницы в одном заказе или подряд.
									foreach($arInOrder as $iOrderId => $arOrderProducts) {
										$arTmpProductsUsed = array();
										// суммируем все блюда, попадающие под условия в рамках одного заказа
										foreach($arOrderProducts as $iTmpRpoductId => $iTmpProductQnt) {
											if(!$arTmpProductsUsed[$iTmpRpoductId]) {
												$arTmpProductsUsed[$iTmpRpoductId] = 0;
											}
											$arTmpProductsUsed[$iTmpRpoductId] += $iTmpProductQnt;
										}

										if($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'BOLSHE') {
											foreach($arTmpProductsUsed as $iTmpRpoductId => $iTmpProductQnt) {
												if($iTmpProductQnt > $iCheckQnt) {
													$arWithdrawOrders[$iReps][$iOrderId] = array('PRODUCT_ID' => $iTmpRpoductId);
													++$iReps;
												}
											}
										} elseif($arMedalData['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE'] == 'RAVNO') {
											foreach($arTmpProductsUsed as $iTmpRpoductId => $iTmpProductQnt) {
												if($iTmpProductQnt == $iCheckQnt) {
													$arWithdrawOrders[$iReps][$iOrderId] = array('PRODUCT_ID' => $iTmpRpoductId);
													++$iReps;
												}
											}
										}
									}
								}
							break;
						}
					break;
				}
			}
		}
		$iRepsRequired = isset($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) && intval($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) > 1 ? intval($arMedalData['PROPERTY_BASE_NUMBER_OF_ACTIONS']) : 1;

//_log_array(array('$arWithdrawOrders' => $arWithdrawOrders, '$bCheckSequence' => $bCheckSequence ? 'Y' : 'N', '$iRepsRequired' => $iRepsRequired), '$arWithdrawOrders');

		if($arWithdrawOrders && count($arWithdrawOrders) >= $iRepsRequired) {
			$arRepsChunks = array();
			if($bCheckSequence) {
				$arOrders2Idx = array();
				foreach($arWorkOrders as $iIdx => $arOrder) {
					$arOrders2Idx[$arOrder['ORDER']['ID']] = $iIdx;
				}
//_log_array($arOrders2Idx, '$arOrders2Idx');
				$iPrevIdx = -1;
				$arCurChunk = array();
				foreach($arWithdrawOrders as $iRepIdx => $arRepOrders) {
					// проверим, чтобы в пределах одного выполнения условия все заказы шли подряд
					$bFail = false;
					foreach($arRepOrders as $iOrderId => $arRepInfo) {
						if($iPrevIdx < 0) {
							$iPrevIdx = $arOrders2Idx[$iOrderId];
						} else {
							if($iPrevIdx + 1 == $arOrders2Idx[$iOrderId]) {
								$iPrevIdx = $arOrders2Idx[$iOrderId];
							} else {
								$bFail = true;
								break;
							}
						}
					}
//_log_array(array($arRepOrders, $iPrevIdx, $bFail), '$arRepOrders');
					if($bFail) {
						// начинаем отслеживать последовательность сначала
						$iPrevIdx = -1;
						$arCurChunk = array();
					} else {
						$arCurChunk[] = $iRepIdx;
						if(count($arCurChunk) == $iRepsRequired) {
							$arRepsChunks[] = $arCurChunk;
							// начинаем отслеживать последовательность сначала
							$iPrevIdx = -1;
							$arCurChunk = array();
						}
					}
				}

			} else {
				$arCurChunk = array();
				foreach($arWithdrawOrders as $iRepIdx => $arRepOrders) {
					$arCurChunk[] = $iRepIdx;
					if(count($arCurChunk) == $iRepsRequired) {
						$arRepsChunks[] = $arCurChunk;
						$arCurChunk = array();
					}
				}
			}

//_log_array(array($arRepsChunks), '$arRepsChunks');
			if($arRepsChunks) {
				foreach($arRepsChunks as $iIdx => $arChunk) {
					foreach($arChunk as $iTmpRepIdx) {
						if($arWithdrawOrders[$iTmpRepIdx]) {
							foreach($arWithdrawOrders[$iTmpRepIdx] as $iOrderId => $arRepInfo) {
								// count($arReturn) - столько медалей может получить юзер
								$arReturn[$iIdx]['ORDERS'][$iOrderId] = $arRepInfo;
							}
						}
					}
				}
			}

		}

//_log_array(array('$arReturn' => $arReturn), '$arReturn');
		return $arReturn;
	}

	private static function CheckAction($sActionType, $iUserId = false, $arMedalData = array()) {
		$arReturn = array();
		$iUserId = $iUserId === false ? $GLOBALS['USER']->GetId() : intval($iUserId);
		if($iUserId <= 0) {
			return $arReturn;
		}

		// соберем массивы ранее использованных действий
		$arUsedActions = array();
		if($arMedalData['_ALREADY_USED_ENTITIES_']) {
			foreach($arMedalData['_ALREADY_USED_ENTITIES_'] as $arTmpItem) {
				if($arTmpItem['UF_ACTION_ID']) {
					$arUsedActions[$arTmpItem['UF_ACTION_ID']] = $arTmpItem['UF_ACTION_ID'];
				}
			}
		}

		$arQueryParams = array(
			'filter' => array(
				'=UF_USER_ID' => $iUserId,
				'=UF_TYPE_ID' => $sActionType
			)
		);
		if($arUsedActions) {
			$arQueryParams['filter']['!ID'] = array_keys($arUsedActions);
		}

		$obGet = UserActionsTable::GetList($arQueryParams);
		if($obGet->GetSelectedRowsCount()) {
			while($arGet = $obGet->Fetch()) {
				$iActionTimestamp = strtotime($arGet['UF_TIMESTAMP_X']);
				if(self::CheckMedalPeriod($arMedalData, $iActionTimestamp, $iUserId)) {
					$arReturn['SUCCESS_ACTIONS'][$arGet['ID']] = $arGet['ID'];
				}
			}
		}
		return $arReturn;
	}

	protected static function IsSocAuth($iUserId = false, $arMedalData = array()) {
		return self::CheckAction('AUTHSOC', $iUserId, $arMedalData);
	}

	protected static function IsSocRecommendationWidget($iUserId = false, $arMedalData = array()) {
		return self::CheckAction('RECOMENDATIONSOC_RECOMENDATON_FROM_VIDZHET', $iUserId, $arMedalData);
	}

	protected static function IsSurvey($iUserId = false, $arMedalData = array()) {
		return self::CheckAction('SURVEY', $iUserId, $arMedalData);
	}

	protected static function IsSocRecommendationLivel($iUserId = false, $arMedalData = array()) {
		return self::CheckAction('RECOMENDATIONSOC_TWEET_YROVEN_OR_MEDAL', $iUserId, $arMedalData);
	}

	/**
	 * GetUserMedals
	 * @param bool $iUserId
	 * @param string $uniq
	 * @param bool $bRefreshCache
	 * @return array
	 */
	public static function GetUserMedals($iUserId = false, $sUniq = 'Y', $bRefreshCache = false) {
		$arReturn = array();

		$iUserId = $iUserId === false ? $GLOBALS['USER']->GetId() : $iUserId;
		if($iUserId <= 0) {
			return $arReturn;
		}

		// дополнительный параметр для ID кэша
		$sCacheAddParam = $iUserId.'|'.$sUniq;
		// идентификатор кэша (обязательный и уникальный параметр)
		$sCacheId = 'GetUserMedals';
		// массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
		$arAddCacheTags = array('USERMEDAL');
		// путь для сохранения кэша
		$sCachePath = '/'.__CLASS__.'/GetUserMedals/';

		// сохранять ли значения дополнительно в виртуальном кэше
		$bUseStaticCache = true;
		// максимальное количество записей виртуального кэша
		$iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
		// соберем в массив идентификационные параметры кэша
		$arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

		$obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
		if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
		} else {

			$iIBlockId = self::GetUserMedalsIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			if(CModule::IncludeModule('iblock')) {
				$dbItems = CIBlockElement::GetList(
					array(
						'ID' => 'ASC',
					),
					array(
						'=PROPERTY_USER_ID' => $iUserId,
						'IBLOCK_ID' => $iIBlockId,
						'ACTIVE' => 'Y',
						'CHECK_PERMISSIONS' => 'N',
					),
					false,
					false,
					array(
						'ID', 
						'PROPERTY_MEDAL_ID', 'PROPERTY_MEDAL_ID.NAME', 'PROPERTY_MEDAL_ID.PREVIEW_TEXT',
						'PROPERTY_MEDAL_ID.PROPERTY_BASE_TYPE_IMG_DOWNLOAD'
					)
				);
				while($arItem = $dbItems->Fetch()) {
					if($sUniq == 'Y') {
						$arReturn[$arItem['PROPERTY_MEDAL_ID_VALUE']] = $arItem;
					} else {
						$arReturn[] = $arItem;
					}
				}
			}

			if(empty($arReturn)) {
				$obExtCache->AbortDataCache();
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
		}
		return $arReturn;
	}

	public static function ClearCache($bClearOnlyUsersCache = false) {
		$GLOBALS['CACHE_MANAGER']->ClearByTag('USERMEDAL');
		// без слеша в конце!
		CStaticCache::FlushEntityCacheByCachePath('/'.__CLASS__.'/GetUserMedals');
		if(!$bClearOnlyUsersCache) {
			$GLOBALS['CACHE_MANAGER']->ClearByTag('MEDALSLIST');
			// без слеша в конце!
			CStaticCache::FlushEntityCacheByCachePath('/'.__CLASS__.'/GetMedalsList');
		}
	}

	/**
	 * SetMedal
	 * @param int $iMedalId
	 * @param int $iUserId
	 * @return mixed
	 */
	public static function SetMedal($iMedalId, $iUserId = false, $arMedalInfo = array(), $bCheckMedal = true) {
		$mUserMedalId = 0;
		if(!is_numeric($iMedalId)) {
			return $mUserMedalId;
		}
		$iMedalId = intval($iMedalId);
		if($iMedalId <= 0) {
			return $mUserMedalId;
		}
		
		$iUserId = $iUserId === false ? $GLOBALS['USER']->GetId() : $iUserId;
		if($iUserId <= 0) {
			return $mUserMedalId;
		}

		CModule::IncludeModule('iblock');

		$bAccess = true;

		// Получаем кол-во медалей пользователя
		if($bCheckMedal) {
			$bAccess = false;
			$arMedal = self::GetMedalDetail($iMedalId);
			$iCountMedals = empty($arMedal['PROPERTY_BASE_COUNT_ACTION']) ? 1 : $arMedal['PROPERTY_BASE_COUNT_ACTION'];
			$iUserCount = self::GetUserMedalsCnt($iMedalId, $iUserId);
			if(!$iUserCount || $iUserCount < $iCountMedals) {
				$bAccess = true;
			}
		}

		if($bAccess) {
			$iIBlockId = self::GetUserMedalsIBlockId();
			$arProp = array(
				'USER_ID' => $iUserId,
				'MEDAL_ID' => $iMedalId
			);
			$arLoadProductArray = array(
				'MODIFIED_BY' => $iUserId,
				'IBLOCK_SECTION_ID' => false,
				'IBLOCK_ID' => $iIBlockId,
				'PROPERTY_VALUES' => $arProp,
				'NAME' => 'Медаль',
				'ACTIVE' => 'Y',
			);

			$obIBlockElement = new CIBlockElement();
			if($mUserMedalId = $obIBlockElement->Add($arLoadProductArray)) {
				if($arMedalInfo) {
					$sTmpHLEntityClass = self::GetUserMedalsInfoEntityClass();

					foreach($arMedalInfo as $arMedalInfoFields) {
						$arSetMedalInfoFields = array(
							'UF_USER_ID' => $iUserId,
							'UF_MEDAL_ID' => $iMedalId,
							'UF_USER_MEDAL_ID' => $mUserMedalId,
							'UF_ORDER_ID' => isset($arMedalInfoFields['UF_ORDER_ID']) ? intval($arMedalInfoFields['UF_ORDER_ID']) : '',
							'UF_PRODUCT_ID' => isset($arMedalInfoFields['UF_PRODUCT_ID']) ? intval($arMedalInfoFields['UF_PRODUCT_ID']) : '',
							'UF_ACTION_ID' => isset($arMedalInfoFields['UF_ACTION_ID']) ? intval($arMedalInfoFields['UF_ACTION_ID']) : '',
						);
						if($arSetMedalInfoFields['UF_ORDER_ID'] || $arSetMedalInfoFields['UF_PRODUCT_ID'] || $arSetMedalInfoFields['UF_ACTION_ID']) {
							$sTmpHLEntityClass::Add($arSetMedalInfoFields);
						}
					}
				}
				self::ClearCache(true);

				$arEventHandlers = GetModuleEvents('main', 'OnAfterSetMedal', true);
				foreach($arEventHandlers as $arEvent) {
					ExecuteModuleEventEx($arEvent, array($iMedalId, $iUserId, $mUserMedalId, $arMedalInfo));
				}
			}
		} else {
			$mUserMedalId = -1;
		}
		return $mUserMedalId;
	}

	/**
	 * GetUserMedalsCnt
	 * @param $iMedalID
	 * @param bool $iUserID
	 * @return int
	 */
	public static function GetUserMedalsCnt($iMedalId, $iUserId = false) {
		$iItemsCnt = 0;
		$iUserId = $iUserId === false ? $GLOBALS['USER']->GetId() : $iUserId;
		if($iUserId <= 0) {
			return $iItemsCnt;
		}
		$iMedalId = intval($iMedalId);
		if($iMedalId <= 0) {
			return $iItemsCnt;
		}

		if(CModule::IncludeModule('iblock')) {
			$arReturn = array();
			$iIBlockId = self::GetUserMedalsIBlockId();
			$iItemsCnt = CIBlockElement::GetList(
				array(),
				array(
					'=PROPERTY_MEDAL_ID' => $iMedalId,
					'=PROPERTY_USER_ID' => $iUserId,
					'IBLOCK_ID' => $iIBlockId,
					'ACTIVE' => 'Y',
					'CHECK_PERMISSIONS' => 'N',
				),
				array()
			);
		}
		return $iItemsCnt;
	}

	/**
	 * getMedalByName
	 * @param $sName
	 * @return bool|string
	 */
	public static function GetMedalByName($sName) {
		$arReturn = array();
		$arMedals = self::GetMedalsList();
		if(!empty($arMedals)) {
			$sNameLower = ToLower($sName);
			foreach($arMedals as $arItem) {
				if(ToLower($arItem['NAME']) == $sNameLower) {
					$arReturn = $arItem;
					break;
				}
			}
		}
		return $arReturn ? $arReturn : false;
	}

	protected static function CheckOrderSum($arOrder, $arMedalData) {
		$iTotalPrice = $arOrder['ORDER_CART']['TOTAL_PRICE'];
	}


	public static function GetUserMedalsInfoEntity() {
		return CHLEntity::GetEntityByName(self::$sUserMedalsInfoEntityName);
	}

	public static function GetUserMedalsInfoEntityClass() {
		$obEntity = self::GetUserMedalsInfoEntity();
		if($obEntity) {
			return $obEntity->GetdataClass();
		}
		return '';
	}

	/**
	 * Возвращает информацию о событиях (ID заказов, товаров, действий), за которые пользватель получил медаль
	 * @param int $iMedalId ID элемента карточки медали
	 * @param int $iUserId ID пользователя
	 * @return array
	 */
	public static function GetUserMedalInfo($iMedalId, $iUserId) {
		$arReturn = array();
		$iUserId = intval($iUserId);
		if($iUserId <= 0) {
			return $arReturn;
		}
		$iMedalId = intval($iMedalId);
		if($iMedalId <= 0) {
			return $arReturn;
		}

		$sTmpHLEntityClass = self::GetUserMedalsInfoEntityClass();
		$arTmpGetListParams = array();
		$arTmpGetListParams['order'] = array(
			'ID' => 'ASC'
		);
		$arTmpGetListParams['filter'] = array(
			'=UF_USER_ID' => $iUserId,
			'=UF_MEDAL_ID' => $iMedalId
		);
		$dbItems = $sTmpHLEntityClass::GetList($arTmpGetListParams);
		while($arItem = $dbItems->Fetch()) {
			$arItem['UF_TIMESTAMP_X'] = is_object($arItem['UF_TIMESTAMP_X']) ? $arItem['UF_TIMESTAMP_X']->ToString() : $arItem['UF_TIMESTAMP_X'];
			$arReturn[$arItem['ID']] = $arItem;
		}
		return $arReturn;
	}

	public static function DeleteUserMedalInfoByUserMedalId($iUserMedalId) {
		$iUserMedalId = intval($iUserMedalId);
		if($iUserMedalId <= 0) {
			return;
		}

		$sTmpHLEntityClass = self::GetUserMedalsInfoEntityClass();
		$arTmpGetListParams = array();
		$arTmpGetListParams['order'] = array(
			'ID' => 'ASC'
		);
		$arTmpGetListParams['filter'] = array(
			'=UF_USER_MEDAL_ID' => $iUserMedalId
		);
		$dbItems = $sTmpHLEntityClass::GetList($arTmpGetListParams);
		while($arItem = $dbItems->Fetch()) {
			$sTmpHLEntityClass::Delete($arItem['ID']);
		}
	}

}

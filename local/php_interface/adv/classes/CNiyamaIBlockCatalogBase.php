<?
/**
 *
 * Класс утилит для инфблока "Импортируемое меню"
 * @author Sergey Leshchenko, 2014
 * @updated: 14.07.2015
 *
 */
if(!defined('NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT')) {
    define('NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT', true);
}

class CNiyamaIBlockCatalogBase
{
    protected static $obLog = null;
    protected static $bLogEnabled = NIYAMA_IBLOCK_IMPORT_LOG_DEFAULT;

    public static function GetLogInstance()
    {
        if (is_null(self::$obLog)) {
            $sLogFileName = $_SERVER['DOCUMENT_ROOT'] . '/_log/_imports/' . __CLASS__ . '/' . date('Y-m-d-H-i-s') . '.txt';
            $bAddTimestamp = true;
            self::$obLog = new CCustomLog($sLogFileName, false, $bAddTimestamp);
            self::$obLog->SetRowSeparator('');
        }
        return self::$obLog;
    }

    public static function Log($sOpCode, $mLogInfo)
    {
        if (self::$bLogEnabled) {
            $obLog = self::GetLogInstance();
            $obLog->Log(array($sOpCode, $mLogInfo));
        }
    }

    public static function SetLogEnabled()
    {
        self::$bLogEnabled = true;
    }

    public static function SetLogDisabled()
    {
        self::$bLogEnabled = false;
    }

    public static function IsLogEnabled()
    {
        return self::$bLogEnabled;
    }

    public static function GetIBlockId()
    {
        static $iReturn = 0;
        if (!$iReturn) {
            $iReturn = CProjectUtils::GetIBlockIdByCode('catalog-base', 'catalog');
        }
        return $iReturn;
    }

    public static function GetPizzapiIblockID(){
        static $iReturn = 0;
        if (!$iReturn) {
            $iReturn = CProjectUtils::GetIBlockIdByCode('catalog', 'catalog');
        }
        return $iReturn;
    }

    public static function GetSiteMenuIBlockId()
    {
        static $iReturn = 0;
        if (!$iReturn) {
            $iReturn = CProjectUtils::GetIBlockIdByCode('catalog', 'catalog');
        }
        return $iReturn;
    }

    public static function GetCurUserPermissions()
    {
        CModule::IncludeModule('iblock');
        return CIBlock::GetPermission(self::GetIBlockId());
    }

    public static function AddBaseItem($sElementXmlId, $arFields = array())
    {
        $iReturnElementId = 0;
        CModule::IncludeModule('iblock');
        $iIBlockId = self::GetIBlockId();

        $arFields['IBLOCK_ID'] = $iIBlockId;
        $arFields['ACTIVE'] = $arFields['ACTIVE'] ? $arFields['ACTIVE'] : 'N';
        $arFields['XML_ID'] = trim($sElementXmlId);
        $arFields['NAME'] = isset($arFields['NAME']) && strlen($arFields['NAME']) ? $arFields['NAME'] : $arFields['XML_ID'];
        if (strlen($arFields['XML_ID'])) {
            $dbItems = CIBlockElement::GetList(
                array(
                    'ACTIVE' => 'DESC',
                    'ID' => 'ASC',
                ),
                array(
                    'IBLOCK_ID' => $iIBlockId,
                    '=XML_ID' => $arFields['XML_ID']
                ),
                false,
                false,
                array('ID', 'NAME')
            );
            $arItem = $dbItems->Fetch();
            if (!$arItem) {
                $obIBlockElement = new CIBlockElement();
                $iReturnElementId = $obIBlockElement->Add($arFields);
            } else {
                $iReturnElementId = $arItem['ID'];
                $arFields['NAME'] = strlen($arItem['NAME']) ? $arItem['NAME'] : $arFields['NAME'];
                // проверяем и добавляем элемент инфоблока Основное меню (теоретически может отсутствовать)
                self::AddSiteMenuItem($iReturnElementId, $arFields);
            }
        }
        return $iReturnElementId;
    }

    // добавление элемента в инфоблоке основного меню с привязкой к элементу импортируемого меню
    public static function AddSiteMenuItem($iImportedElementId, $arFields)
    {
        $iReturnElementId = 0;
        CModule::IncludeModule('iblock');
        $iSiteMenuIBlockId = self::GetSiteMenuIBlockId();
        $dbItems = CIBlockElement::GetList(
            array(
                'ID' => 'ASC'
            ),
            array(
                'IBLOCK_ID' => $iSiteMenuIBlockId,
                'PROPERTY_IMPORT_ELEMENT' => $iImportedElementId
            ),
            false,
            false,
            array('ID')
        );
        $arItem = $dbItems->Fetch();
        if (!$arItem) {
            if (isset($arFields['NAME']) && strlen(trim($arFields['NAME']))) {
                $obIBlockElement = new CIBlockElement();
                // проверяем символьный код
                $sCode = CNiyamaImportUtils::Translit($arFields['NAME']);
                $iTmpCnt = CIBlockElement::GetList(
                    array(),
                    array(
                        'IBLOCK_ID' => $iSiteMenuIBlockId,
                        '=CODE' => $sCode
                    ),
                    array()
                );
                if ($iTmpCnt) {
                    $sCode = $sCode . '-' . RandString(10);
                }
                $iReturnElementId = $obIBlockElement->Add(
                    array(
                        'IBLOCK_ID' => $iSiteMenuIBlockId,
                        'ACTIVE' => 'N',
                        'NAME' => $arFields['NAME'],
                        'CODE' => $sCode,
                        'PROPERTY_VALUES' => array(
                            'IMPORT_ELEMENT' => $iImportedElementId,
                        )
                    )
                );
            }
        } else {
            $iReturnElementId = $arItem['ID'];
        }
        return $iReturnElementId;
    }


    //
    // Актуализация инфоблока каталога Пицца-Пи (меню блюд) информацией из FastOperator.
    //

    public static function ImportDataPizza()
    {
        self::Log('ImportDataPizza', 'START');

        $bResult = false;

        $IBLOCK = 58; //ID нашего инфоблока
        $skuIblockID = 60; //ID инфоблока офферов

        $arImportCataegoryes = array();
        $arImportItemsPizza = array();
        $arImportItemsOther = array();
        $PRICE_TYPE_ID = 2;

        CModule::IncludeModule("catalog");
        $arCatalog = CCatalog::GetByID($skuIblockID);
        $intSKUProperty = $arCatalog['SKU_PROPERTY_ID'];

        //получение списка разделов и товаров по бренду Пицца пи
        $obFastOperator = new CNiyamaFastOperator();
        $arParams = array('parameters' => array('Brand' => 100000000)); // передаем параметр бренда
        $arCatalogBase = $obFastOperator->GetMenuArray($arParams); //получаем список разделов и товаров


        // создаем и формируем массив с разделами полученными из ФО.
        foreach ($arCatalogBase['Category'] as $category) {
            if ($category['ParentCode'] == 10000597) {
                if ($category['Code'] == 100000667) {
                    $category['Name'] = 'Пицца';
                    $arImportCataegoryes[] = $category;
                } else if ($category['Code'] == 100000668) {
                    continue;
                } else {
                    $arImportCataegoryes[] = $category;
                }
            }
        }

        // получение и обновление разделов
        $IBlockSection = new CIBlockSection;
        //self::UpdateSection($arImportCataegoryes); //обновляем фокод секций;

        $currentSection = self::GetSectionList($IBLOCK); //получаем список текущих секций из инфоблока Пицца-пи

        //создаем разделы  в инфоблоке "каталог Пицца-Пи"
        $IBlockSection = new CIBlockSection; // создаем экземпляр класса и создаем разделы
        //добавляем разделы
        foreach ($arImportCataegoryes as $category) {
            if (empty($currentSection)) {
              continue;
            } else if (in_array($category['Code'], $currentSection)) {
                continue;
            } else {
                $arSectionData = array(
                    'ACTIVE' => 'Y',
                    'NAME' => $category['Name'],
                    'IBLOCK_ID' => $IBLOCK,
                    'CODE' => CUtil::translit($category['Name'], "ru", array()),
                    'EXTERNAL_ID' => $category['Code']
                );
                $IBlockSection->Add($arSectionData);
            }
        }

        //// работа с Товарами
        foreach ($arCatalogBase['Items'] as $item) {
            $item['NameShort'] = CUtil::translit(self::MinimizeName($item['Name']), "ru", array());

            if ($item['_category_code_'] == 100000667 || $item['_category_code_'] == 100000668) {
                $group = substr($item['Size'],2);
                $size = substr($item['Size'],0,2);
                $item['Group'] = $group;
                $item['Lenght'] = $size;

                if (empty($arImportItemsPizza[$item['Group']])) {
                    $arOffer = array(
                        'NAME' => $item['Name'],
                        'IBLOCK_ID' => $skuIblockID,
                        'ACTIVE' => 'Y',
                        'CODE' => $item['NameShort'],
                        'EXTERNAL_ID' => $item['Code'],
                        'WEIGHT' => $item['Weight'],
                        'PRICE' => $item['Price'],
                        'LENGTH' => $size,
                        'GROUP' => $group,

                    );
                    $arItem = array(
                        'NAME' => $item['Name'],
                        'IBLOCK_ID' => $IBLOCK,
                        'CODE' => $item['NameShort'],
                        'EXTERNAL_ID' => $item['Code'],
                        'ACTVIE' => 'Y',
                        'IBLOCK_SECTION_ID' => '100000667',
                        'OFFERS' => array($arOffer)
                    );
                    $arImportItemsPizza[$item['Group']] = $arItem;
                } else {
                    $arOffer = array(
                        'NAME' => $item['Name'],
                        'IBLOCK_ID' => $skuIblockID,
                        'ACTIVE' => 'Y',
                        'CODE' => $item['NameShort'],
                        'EXTERNAL_ID' => $item['Code'],
                        'WEIGHT' => $item['Weight'],
                        'PRICE' => $item['Price'],
                        'LENGTH' => $size,
                        'GROUP' => $group
                    );
                    $arImportItemsPizza[$item['Group']]['OFFERS'][] = $arOffer;
                }

            } else {
                //массив с остальными товарами за исключением пиццы
                $arOffer = array(
                    'NAME' => $item['Name'],
                    'IBLOCK_ID' => $skuIblockID,
                    'ACTIVE' => 'Y',
                    'CODE' => $item['NameShort'],
                    'EXTERNAL_ID' => $item['Code'],
                    'WEIGHT' => $item['Weight'],
                    'PRICE' => $item['Price'],

                );
                $arItem = array(
                    'NAME' => $item['Name'],
                    'IBLOCK_ID' => $IBLOCK,
                    'CODE' => $item['NameShort'],
                    'EXTERNAL_ID' => $item['Code'],
                    'ACTVIE' => 'Y',
                    'IBLOCK_SECTION_ID' => $item['_category_code_'],
                    'OFFERS' => array($arOffer)
                );
                $arImportItemsOther[$item['NameShort']] = $arItem;
            }
        }

        $el = new CIblockElement;
        $obCatElement = new CCatalogProduct();

// обновил существующие товары
        $allItems = array_merge($arImportItemsPizza, $arImportItemsOther);
        $data = self::GetDataForUpdate($IBLOCK);
        $dataSKU = self::GetDataForUpdate($skuIblockID);
        /*
        self::updateItem($data, $allItems, $IBLOCK);
        self::updateItem($dataSKU, $arImportItemsOther, $skuIblockID);


//Необходимо обновить офферы

// дальше происходит обновление уже существующих  ОФФЕРОВ
        $el = new CIblockElement;
        $obCatElement = new CCatalogProduct();

        foreach ($arImportItemsPizza as $foItem) {
            foreach ($foItem['OFFERS'] as $offerID => $offer) {
                foreach ($dataSKU as $key => $item) {
                    if (self::MinimizeName($item) == self::MinimizeName($offer['NAME'])) {
                        $tmp = true;
                        $arItemData = array(
                            'IBLOCK_ID' => $IBLOCK,
                            'NAME' => $offer['NAME'],
                            'ACTIVE' => 'Y',
                            'CODE' => $offer['CODE'],
                            'EXTERNAL_ID' => $offer['EXTERNAL_ID'],
                        );

                        $catElementField = array(
                            'AVAILABLE' => 'Y',
                            'CAN_BUY_ZERO' => 'Y',
                            'QUANTITY' => 1,
                            'QUANTITY_TRACE' => 'N',
                            'WEIGHT' => $offer['WEIGHT'],
                            'LENGTH' => $offer['LENGTH']
                        );


                        unset($dataSKU[$key]);

                        $res = $el->Update($key, $arItemData);
                        $q = $obCatElement->Update($key, $catElementField);
                        break;
                    }
                }
            }
        }



//ДАЛЬШЕ ПРОИСХОДИТ  ИМПОРТ ТАМ ТИПО ВСЕ ОК типа на деве ничего у меня не отвалилось


*/

        //получаем список уже имеющихся разделов
        $SecID = array();
        $arFilter = array(
            'IBLOCK_ID' => $IBLOCK,
        );
        $IBlockSection = CIblockSection::getList(array(), $arFilter, false);
        while ($Sec = $IBlockSection->Fetch()) {
            $SecID[$Sec['EXTERNAL_ID']] = $Sec['ID'];

        }

        //получаем свойство привязки оффера к элементу
        CModule::IncludeModule("catalog");
        $arCatalog = CCatalog::GetByID($skuIblockID);
        $intSKUProperty = $arCatalog['SKU_PROPERTY_ID'];


        //получаем текущие элементы
        $currentItems = array();
        $arCurrentItem = array();
        $skudb = CIBlockElement::getList(array(), array('IBLOCK_ID' => $skuIblockID), false);
        while ($item = $skudb->GetNext()) {
            $currentItems[] = $item['EXTERNAL_ID'];
            $arCurrentItem[$item['EXTERNAL_ID']] = $item['ID'];
        }

        $obCatElement = new CCatalogProduct();
        $obPriceElement = new CPrice();

        //добавляем gпиццу и торговые предложения к ним
        $IBlockElement = new CIBlockElement();
        foreach ($arImportItemsPizza as $item) {
            if (!in_array($item['EXTERNAL_ID'], $currentItems)) {
                if (array_key_exists($item['IBLOCK_SECTION_ID'], $SecID)) {
                    $SectionID = $SecID[$item['IBLOCK_SECTION_ID']];
                }

                $arItemData = array(
                    'IBLOCK_ID' => $IBLOCK,
                    'IBLOCK_SECTION_ID' => $SectionID,
                    'NAME' => $item['NAME'],
                    'ACTIVE' => 'Y',
                    'CODE' => $item['CODE'],
                    'EXTERNAL_ID' => $item['EXTERNAL_ID'],
                );

                $id = $IBlockElement->add($arItemData);
                $arProp[$intSKUProperty] = $id;

                //установка опции "показывать блок размера для пиццы
                $selectProp['PIZZA_TRUE'] = true;
                $IBlockElement->SetPropertyValuesEx($id, 58, $selectProp);

                //добавление оффера
                foreach ($item['OFFERS'] as $offer) {

                    $arOfferData = array(
                        'NAME' => $offer['NAME'],
                        'IBLOCK_ID' => $skuIblockID,
                        'CODE' => $offer['CODE'],
                        'EXTERNAL_ID' => $offer['EXTERNAL_ID'],
                        'ACTIVE' => 'Y',
                        'PROPERTY_VALUES' => $arProp,
                    );
                    $ofID = $IBlockElement->add($arOfferData);

                    //добавление цены
                    $arPriceFields = array(
                        "PRODUCT_ID" => $ofID,
                        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                        "PRICE" => $offer['PRICE'],
                        "CURRENCY" => "RUB",

                    );
                    $obPriceElement->Add($arPriceFields, true);

                    //добавление каталожных свойств
                    $catElementField = array(
                        'ID' => $ofID,
                        'AVAILABLE' => 'Y',
                        'CAN_BUY_ZERO' => 'Y',
                        'QUANTITY' => 1,
                        'QUANTITY_TRACE' => 'N',
                        'WEIGHT' => $offer['WEIGHT'],
                        'LENGTH' => $offer['LENGTH']
                    );

                    $obCatElement->Add($catElementField, false);


                }
            } else {
                foreach ($item['OFFERS'] as $offer) {
                    if (array_key_exists($offer['EXTERNAL_ID'], $arCurrentItem)) {
                        $itemId = $arCurrentItem[$offer['EXTERNAL_ID']];
                        $updateFields = array(
                            'WEIGHT' => $offer['WEIGHT'],
                        );

                        CCatalogProduct::Update($itemId, $updateFields);

                        //получение ID цены для апдейта цен
                        $db_res = CPrice::GetList( array(), array("PRODUCT_ID" => $itemId, "CATALOG_GROUP_ID" => $PRICE_TYPE_ID));
                        $ar_res = $db_res->Fetch();

                        $arPriceFields = array(
                            "PRODUCT_ID" => $itemId,
                            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                            "PRICE" => $offer['PRICE'],
                            "CURRENCY" => "RUB",
                        );
                        CPrice::Update($ar_res['ID'], $arPriceFields, true);
                    }
                }
            }

        }

        //добавляем остальные товары
        foreach ($arImportItemsOther as $item) {
            if (!in_array($item['EXTERNAL_ID'], $currentItems)) {

                if (array_key_exists($item['IBLOCK_SECTION_ID'], $SecID)) {
                    $SectionID = $SecID[$item['IBLOCK_SECTION_ID']];
                }

                $arItemData = array(
                    'IBLOCK_ID' => $IBLOCK,
                    'IBLOCK_SECTION_ID' => $SectionID,
                    'NAME' => $item['NAME'],
                    'ACTIVE' => 'Y',
                    'CODE' => $item['CODE'],
                    'EXTERNAL_ID' => $item['EXTERNAL_ID'],
                );
                $id = $IBlockElement->add($arItemData);
                $arProp[$intSKUProperty] = $id;

                foreach ($item['OFFERS'] as $offer) {

                    $arOfferData = array(
                        'NAME' => $offer['NAME'],
                        'IBLOCK_ID' => $skuIblockID,
                        'CODE' => $offer['CODE'],
                        'EXTERNAL_ID' => $offer['EXTERNAL_ID'],
                        'ACTIVE' => 'Y',
                        'PROPERTY_VALUES' => $arProp,
                    );
                    $ofID = $IBlockElement->add($arOfferData);

                    //добавление цены
                    $arPriceFields = array(
                        "PRODUCT_ID" => $ofID,
                        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                        "PRICE" => $offer['PRICE'],
                        "CURRENCY" => "RUB",

                    );
                    $obPriceElement->Add($arPriceFields, true);

                    //добавление каталожных свойств
                    $catElementField = array(
                        'ID' => $ofID,
                        'AVAILABLE' => 'Y',
                        'CAN_BUY_ZERO' => 'Y',
                        'QUANTITY' => 1,
                        'QUANTITY_TRACE' => 'N',
                        'WEIGHT' => $offer['WEIGHT'],
                        'LENGTH' => $offer['LENGTH']
                    );

                    $obCatElement->Add($catElementField, false);
                }
            } else {
                foreach ($item['OFFERS'] as $offer) {
                    if (array_key_exists($offer['EXTERNAL_ID'], $arCurrentItem)) {
                        $itemId = $arCurrentItem[$offer['EXTERNAL_ID']];
                        $updateFields = array(
                            'WEIGHT' => $offer['WEIGHT'],
                        );
                        CCatalogProduct::Update($itemId, $updateFields);

                        //получение ID цены для апдейта цен
                        $db_res = CPrice::GetList( array(), array("PRODUCT_ID" => $itemId, "CATALOG_GROUP_ID" => $PRICE_TYPE_ID));
                        $ar_res = $db_res->Fetch();

                        $arPriceFields = array(
                            "PRODUCT_ID" => $itemId,
                            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                            "PRICE" => $offer['PRICE'],
                            "CURRENCY" => "RUB",

                        );
                        CPrice::Update($ar_res['ID'],$arPriceFields, true);
                    }
                }
            }
        }
        $bResult = true;
        var_dump($bResult);
        return $bResult;
    }

    public static function UpdateSection($arImportCataegoryes){
        $IBLOCK = 58;
        $IBlockSection = new CIBlockSection;

        $curSections = array();

        $tree = CIBlockSection::GetTreeList(
            $arFilter = Array('IBLOCK_ID' => $IBLOCK),
            $arSelect = Array()
        );
        while ($section = $tree->GetNext()) {
            $curSections[$section['NAME']] = $section['ID'];
        }

        foreach ($curSections as $key => $section) {
            foreach ($arImportCataegoryes as $category) {
                if ($key == $category['Name']) {
                    $arSectionData = array(

                        'NAME' => $category['Name'],
                        'IBLOCK_ID' => $IBLOCK,
                        'CODE' => CUtil::translit($category['Name'], "ru", array()),
                        'EXTERNAL_ID' => $category['Code']
                    );
                    $res = $IBlockSection->Update($section, $arSectionData);

                }
            }

        }
    }




    public static function updateItem($currentItems, $foItems, $IBLOCK)
    {
        $el = new CIblockElement;


            foreach ($currentItems as $key => $item) {

                foreach ($foItems as $foItem) {
                    if ($item == $foItem['NAME']) {
                        $arItemData = array(
                            'IBLOCK_ID' => $IBLOCK,
                            'NAME' => $foItem['NAME'],
                            'ACTIVE' => 'Y',
                            'CODE' => $foItem['CODE'],
                            'EXTERNAL_ID' => $foItem['EXTERNAL_ID'],
                        );

                        $res = $el->Update($key, $arItemData);

                    }
                }
            }
            return 1;


    }


    public static function GetDataForUpdate($IBLOCK)
    {
        $arItemID = array();
        $res = CIBlockElement::getList(array(), array('IBLOCK_ID' => intval($IBLOCK)), false);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arItemID[$arFields['ID']] = $arFields['NAME'];
        }
        return $arItemID;

    }





    //
    // Актуализация базового инфоблока каталога (меню блюд) информацией из FastOperator
    //
    public static function ImportData()
    {
        //self::ImportDataPizza();
        // логируем операцию
        self::Log('ImportData', 'START');

        $bResult = false;
        CModule::IncludeModule('iblock');
        $iCatalogBaseIBlockId = self::GetIBlockId();


        $obFastOperator = new CNiyamaFastOperator();
        $arCatalogBase = $obFastOperator->GetMenuArray();

        // логируем операцию
        self::Log('GetMenuArray', array('arCatalogBase' => $arCatalogBase));

        if (!$arCatalogBase || !$iCatalogBaseIBlockId) {
            return $bResult;
        }

        $bResult = true;

        $sUpdateTmpIdField = time();
        // работа с секциями

        if ($arCatalogBase['Category']) {
            $arSectionsXmlId = array();
            // сформируем массив для дерева секций
            $arTmpCategory = array();
            $arTmpCategoryCheck = array();
            foreach ($arCatalogBase['Category'] as $arItem) {
                if (strlen($arItem['Code'])) {
                    $sParentCode = strlen($arItem['ParentCode']) ? $arItem['ParentCode'] : 0;
                    $arTmpCategory[$sParentCode][$arItem['Code']] = $arItem;
                    $arTmpCategoryCheck[$arItem['Code']] = $arItem;
                    $arSectionsXmlId[$arItem['Code']] = $sParentCode;
                }
            }

            $iLeftMargin = 0;
            $arCategory = self::GroupsRecursive(array(), $arTmpCategory, 0, 0, $iLeftMargin, $arTmpCategoryCheck);
            if (!empty($arTmpCategoryCheck)) {
                foreach ($arTmpCategoryCheck as $arItem) {
                    $arItem['_DEPTH_LEVEL_'] = 1;
                    $arItem['_LEFT_MARGIN_'] = ++$iLeftMargin;
                    $arItem['_RIGHT_MARGIN_'] = ++$iLeftMargin;
                    $arCategory[] = $arItem;
                }
            }

            // сортируем
            CProjectUtils::DoSort($arCategory, '_LEFT_MARGIN_', 'ASC');
            // выбираем все секции
            $arCurrentSections = array();
            $dbItems = CIBlockSection::GetList(
                array(
                    'ID' => 'ASC'
                ),
                array(
                    'IBLOCK_ID' => $iCatalogBaseIBlockId,
                    'CHECK_PERMISSIONS' => 'N'
                ),
                false,
                array(
                    'ID', 'XML_ID', 'NAME', 'ACTIVE', 'IBLOCK_SECTION_ID'
                )
            );
            while ($arItem = $dbItems->Fetch()) {
                $arCurrentSections[$arItem['XML_ID']][$arItem['ID']] = $arItem;
            }


            $arAdded = array();
            $arUpdated = array();
            foreach ($arCategory as $arItem) {
                $iParentSectionId = 0;
                if (strlen($arItem['ParentCode'])) {
                    if ($arCurrentSections[$arItem['ParentCode']]) {
                        $arTmp = reset($arCurrentSections[$arItem['ParentCode']]);
                        if ($arTmp['ID']) {
                            $iParentSectionId = $arTmp['ID'];
                        }
                    } elseif ($arAdded[$arItem['ParentCode']]) {
                        $iParentSectionId = $arAdded[$arItem['ParentCode']];
                    }
                }


                $arFields = array(
                    'IBLOCK_ID' => $iCatalogBaseIBlockId,
                    'NAME' => $arItem['Name'],
                    'IBLOCK_SECTION_ID' => $iParentSectionId,
                    'ACTIVE' => 'Y',
                    'XML_ID' => $arItem['Code'],
                    'TMP_ID' => $sUpdateTmpIdField
                );
                $obIBlockSection = new CIBlockSection();
                if (!$arCurrentSections[$arItem['Code']]) {
                    // добавление новой секции

                    $iSectionId = $obIBlockSection->Add($arFields);

                    $arAdded[$arItem['Code']] = $iSectionId;
                    // логируем операцию
                    self::Log('IBlockSectionAdd', array('ID' => $iSectionId, 'arFields' => $arFields));
                } else {
                    $arTmpItem = reset($arCurrentSections[$arItem['Code']]);
                    // обновление секции
                    $obIBlockSection->Update($arTmpItem['ID'], $arFields);
                    $arUpdated[$arItem['Code']] = $arTmpItem['ID'];
                    // логируем операцию
                    self::Log('IBlockSectionUpdate', array('ID' => $arTmpItem['ID'], 'arFields' => $arFields));
                }
            }

            // деактивируем секции, которых не было в выгрузке
            foreach ($arCurrentSections as $sSectionXmlId => $arSections) {
                if (strlen($sSectionXmlId)) {
                    $iTmpSectionId = 0;
                    if (isset($arAdded[$sSectionXmlId])) {
                        $iTmpSectionId = $arAdded[$sSectionXmlId];
                    } elseif ($arUpdated[$sSectionXmlId]) {
                        $iTmpSectionId = $arUpdated[$sSectionXmlId];
                    }
                    if ($iTmpSectionId && isset($arCurrentSections[$sSectionXmlId][$iTmpSectionId])) {
                        unset($arCurrentSections[$sSectionXmlId][$iTmpSectionId]);
                        if (empty($arCurrentSections[$sSectionXmlId])) {
                            unset($arCurrentSections[$sSectionXmlId]);
                        }
                    }
                }
            }
            if ($arCurrentSections) {
                foreach ($arCurrentSections as $sSectionXmlId => $arSections) {
                    foreach ($arSections as $arTmpItem) {
                        if ($arTmpItem['ACTIVE'] == 'Y') {
                            $obIBlockSection->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
                            // логируем операцию
                            self::Log('IBlockSectionDeactivate', array('ID' => $arTmpItem['ID'], 'sUpdateTmpIdField' => $sUpdateTmpIdField));
                        }
                    }
                }
            }
        }

        // работа с элементами
        if ($arCatalogBase['Items']) {
            $bWorkFlow = false;
            $bUpdateSearch = true;
            $bResizePictures = true;

            // выборка секций для установки связей
            $arCurrentSections = array();
            $dbItems = CIBlockSection::GetList(
                array(
                    'ID' => 'ASC'
                ),
                array(
                    'IBLOCK_ID' => $iCatalogBaseIBlockId,
                    'ACTIVE' => 'Y',
                    'CHECK_PERMISSIONS' => 'N',
                ),
                false,
                array(
                    'ID', 'XML_ID'
                )
            );
            while ($arItem = $dbItems->Fetch()) {
                if (strlen($arItem['XML_ID']) && !isset($arCurrentSections[$arItem['XML_ID']])) {
                    $arCurrentSections[$arItem['XML_ID']] = $arItem;
                }
            }

            // обновление элементов
            $arUpdateItemsList = array();
            foreach ($arCatalogBase['Items'] as $arTmpItem) {
                if (strlen($arTmpItem['Code'])) {
                    $arUpdateItemsList[$arTmpItem['Code']] = $arTmpItem;
                }
            }
            if ($arUpdateItemsList) {
                $sDownloadImagesUrl = defined('NIYAMA_FO_IMPORT_IMAGES_DIR') ? NIYAMA_FO_IMPORT_IMAGES_DIR : '';
                $dbItems = CIBlockElement::GetList(
                    array(
                        'ID' => 'ASC'
                    ),
                    array(
                        'IBLOCK_ID' => $iCatalogBaseIBlockId,
                        '=XML_ID' => array_keys($arUpdateItemsList),
                        'CHECK_PERMISSIONS' => 'N',
                    ),
                    false,
                    false,
                    array(
                        'ID', 'XML_ID', 'DETAIL_PICTURE', 'PREVIEW_PICTURE'
                    )
                );
                while ($arItem = $dbItems->Fetch()) {
                    if ($arUpdateItemsList[$arItem['XML_ID']]) {
                        $arTmpUpdateItem = $arUpdateItemsList[$arItem['XML_ID']];
                        $arFields = array(
                            'NAME' => $arTmpUpdateItem['Name'],
                            'ACTIVE' => 'Y',
                            'IBLOCK_SECTION_ID' => strlen($arTmpUpdateItem['_category_code_']) && $arCurrentSections[$arTmpUpdateItem['_category_code_']] ? $arCurrentSections[$arTmpUpdateItem['_category_code_']]['ID'] : 0,
                            // !!!
                            'TMP_ID' => $sUpdateTmpIdField,
                        );

                        if ($sDownloadImagesUrl) {
                            $sTmpFileUrl = $sDownloadImagesUrl . $arItem['XML_ID'] . '.jpg';
                            $rsFile = @fopen($sTmpFileUrl, 'rb');
                            if ($rsFile) {
                                @fclose($rsFile);
                                $arTmpFile = CFile::MakeFileArray($sTmpFileUrl);
                                $arCurPicture = $arItem['DETAIL_PICTURE'] ? CFile::GetFileArray($arItem['DETAIL_PICTURE']) : array();
                                if (!$arItem['PREVIEW_PICTURE'] || !$arCurPicture || $arCurPicture && $arCurPicture['FILE_SIZE'] != $arTmpFile['size']) {
                                    $arFields['DETAIL_PICTURE'] = $arTmpFile;
                                }
                            }
                        }

                        $arTmp = explode('/', $arTmpUpdateItem['Weight']);
                        $dCalcWeight = 0;
                        foreach ($arTmp as $sTmpVal) {
                            $dTmpVal = CNiyamaImportUtils::Str2Double($sTmpVal);
                            if ($dTmpVal > 0) {
                                $dCalcWeight += $dTmpVal;
                            }
                        }
                        $arProp = array(
                            'SIZE' => $arTmpUpdateItem['Size'],
                            'NAME_SHORT' => $arTmpUpdateItem['NameShort'],
                            'UNIT_NAME' => $arTmpUpdateItem['UnitName'],
                            'WEIGHT_FULL' => $arTmpUpdateItem['Weight'],
                            'WEIGHT' => $dCalcWeight,
                            'CALORIC' => $arTmpUpdateItem['Energy'],
                            'SORT' => $arTmpUpdateItem['Sort'],
                            'RECOMMENDED' => $arTmpUpdateItem['Recommended'],
                            'PRICE' => $arTmpUpdateItem['Price'],
                        );
                        $obIBlockElement = new CIBlockElement();
                        $obIBlockElement->Update($arItem['ID'], $arFields, $bWorkFlow, $bUpdateSearch, $bResizePictures);
                        CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iCatalogBaseIBlockId, $arProp);
                        // !!!
                        unset($arUpdateItemsList[$arItem['XML_ID']]);

                        // логируем операцию
                        self::Log('IBlockElementUpdate', array('ID' => $arItem['ID'], 'arFields' => $arFields, 'arProp' => $arProp, 'XML_ID' => $arItem['XML_ID']));
                    }
                }
            }

            // добавление элементов
            if ($arUpdateItemsList) {
                foreach ($arUpdateItemsList as $arTmpUpdateItem) {


                    $arFields = array(
                        'IBLOCK_ID' => $iCatalogBaseIBlockId,
                        'NAME' => $arTmpUpdateItem['Name'],
                        'ACTIVE' => 'Y',
                        'IBLOCK_SECTION_ID' => strlen($arTmpUpdateItem['_category_code_']) && $arCurrentSections[$arTmpUpdateItem['_category_code_']] ? $arCurrentSections[$arTmpUpdateItem['_category_code_']]['ID'] : 0,
                        // !!!

                        'XML_ID' => $arTmpUpdateItem['Code'],
                        'TMP_ID' => $sUpdateTmpIdField,
                    );
                    if ($sDownloadImagesUrl) {
                        $sTmpFileUrl = $sDownloadImagesUrl . $arFields['XML_ID'] . '.jpg';
                        $arTmpFile = CFile::MakeFileArray($sTmpFileUrl);
                        if ($arTmpFile['size']) {
                            $arFields['DETAIL_PICTURE'] = $arTmpFile;
                        }
                    }

                    $arTmp = explode('/', $arTmpUpdateItem['Weight']);
                    $arProp = array(
                        'SIZE' => $arTmpUpdateItem['Size'],
                        'NAME_SHORT' => $arTmpUpdateItem['NameShort'],
                        'UNIT_NAME' => $arTmpUpdateItem['UnitName'],
                        'WEIGHT_FULL' => $arTmpUpdateItem['Weight'],
                        'WEIGHT' => intval($arTmp[0]),
                        'CALORIC' => isset($arTmp[1]) ? intval($arTmp[1]) : '',
                        'SORT' => $arTmpUpdateItem['Sort'],
                        'RECOMMENDED' => $arTmpUpdateItem['Recommended'],
                        'PRICE' => $arTmpUpdateItem['Price'],
                    );
                    $arFields['PROPERTY_VALUES'] = $arProp;
                    $obIBlockElement = new CIBlockElement();

                    $iNewElementId = $obIBlockElement->Add($arFields, $bWorkFlow, $bUpdateSearch, $bResizePictures);

                    // логируем операцию
                    self::Log('IBlockElementAdd', array('ID' => $iNewElementId, 'arFields' => $arFields));
                }
            }

            // деактивируем элементы, которых не было в выгрузке
            $dbItems = CIBlockElement::GetList(
                array(
                    'ID' => 'ASC'
                ),
                array(
                    'IBLOCK_ID' => $iCatalogBaseIBlockId,
                    '!TMP_ID' => $sUpdateTmpIdField,
                    'CHECK_PERMISSIONS' => 'N',
                    'ACTIVE' => 'Y'
                ),
                false,
                false,
                array(
                    'ID'
                )
            );

            while ($arTmpItem = $dbItems->Fetch()) {
                $obIBlockElement = new CIBlockElement();
                $bTmpSuccess = $obIBlockElement->Update($arTmpItem['ID'], array('ACTIVE' => 'N', 'TMP_ID' => $sUpdateTmpIdField));
                // логируем операцию
                self::Log('IBlockElementDeactivate', array('ID' => $arTmpItem['ID'], 'sUpdateTmpIdField' => $sUpdateTmpIdField, 'bTmpSuccess' => $bTmpSuccess, 'LAST_ERROR' => !$bTmpSuccess ? $obIBlockElement->LAST_ERROR : ''));
            }
        }

        self::Log('ImportData', 'FINISH');
        return $bResult;
    }

//Возвращает список разделов в инфоблоке
    private static function GetSectionList($iblock, $sectionid = null /*$import = null*/)
    {
        $arResult = array();
        $arFilter = Array(
            'IBLOCK_ID' => $iblock,
            'SECTION_ID' => $sectionid,
        );
        $db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false);
        while ($arSect = $db_list->GetNext()) {

            $arResult[$arSect['ID']] = $arSect['XML_ID'];

        }
        return $arResult;
    }

    private static function MinimizeName($name)
    {
        $nameLower = strtolower($name);
        $searchebleName = str_replace(' ', '', $nameLower);
        return $searchebleName;
    }


    private static function GroupsRecursive($arReturn, $arCommonList, $sParentXmlId, $iCurDepthLevel = 0, &$iLeftMargin, &$arTmpCategoryCheck) {
        if($arCommonList[$sParentXmlId]) {
            ++$iCurDepthLevel;
            foreach($arCommonList[$sParentXmlId] as $arItem) {
                $iCurKey = count($arReturn);
                $arItem['_DEPTH_LEVEL_'] = $iCurDepthLevel;
                $arItem['_LEFT_MARGIN_'] = ++$iLeftMargin;
                $arItem['_RIGHT_MARGIN_'] = 0;
                $arReturn[$iCurKey] = $arItem;
                if($arCommonList[$arItem['Code']]) {
                    $arReturn = self::GroupsRecursive($arReturn, $arCommonList, $arItem['Code'], $iCurDepthLevel, $iLeftMargin, $arTmpCategoryCheck);
                }
                $arReturn[$iCurKey]['_RIGHT_MARGIN_'] = $iLeftMargin + 1;
                $iLeftMargin = $arReturn[$iCurKey]['_RIGHT_MARGIN_'];
                unset($arTmpCategoryCheck[$arItem['Code']]);
            }
        }
        return $arReturn;
    }
}

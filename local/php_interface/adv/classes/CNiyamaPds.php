<?
/**
 * Web-сервисы системы ПДС Нияма
 *
 * @author Sergey Leshchenko, 2014
 * @updated: 14.04.2015
 */

class CNiyamaPds {
	private $arConnectParams = array();

	public function __construct($arConnectParams = array()) {
		$this->InitConnectParams($arConnectParams);
	}

	private function InitConnectParams($arParams) {
		$this->arConnectParams = array();

		$arParams = is_array($arParams) ? $arParams : array();

		$mDefVal = defined('NIYAMA_PDS_SERVICE_DOMAIN') ? NIYAMA_PDS_SERVICE_DOMAIN : '';
		$this->arConnectParams['DOMAIN'] = isset($arParams['DOMAIN']) ? $arParams['DOMAIN'] : $mDefVal;

		$mDefVal = defined('NIYAMA_PDS_SERVICE_PORT') ? NIYAMA_PDS_SERVICE_PORT : '80';
		$this->arConnectParams['PORT'] = isset($arParams['PORT']) ? $arParams['PORT'] : $mDefVal;

		$mDefVal = defined('NIYAMA_PDS_SERVICE_HTTPS') ? NIYAMA_PDS_SERVICE_HTTPS : false;
		$this->arConnectParams['HTTPS'] = isset($arParams['HTTPS']) ? $arParams['HTTPS'] : $mDefVal;

		$mDefVal = defined('NIYAMA_PDS_SERVICE_LOGIN') ? NIYAMA_PDS_SERVICE_LOGIN : '';
		$this->arConnectParams['LOGIN'] = isset($arParams['LOGIN']) ? $arParams['LOGIN'] : $mDefVal;

		$mDefVal = defined('NIYAMA_PDS_SERVICE_PASSWORD') ? NIYAMA_PDS_SERVICE_PASSWORD : '';
		$this->arConnectParams['PASSWORD'] = isset($arParams['PASSWORD']) ? $arParams['PASSWORD'] : $mDefVal;

		$mDefVal = defined('NIYAMA_PDS_SERVICE_TIMEOUT') ? NIYAMA_PDS_SERVICE_TIMEOUT : 20;
		$this->arConnectParams['TIMEOUT'] = isset($arParams['TIMEOUT']) ? intval($arParams['TIMEOUT']) : $mDefVal;
	}

	public function GetParam($sParamCode) {
		return isset($this->arConnectParams[$sParamCode]) ? $this->arConnectParams[$sParamCode] : '';
	}

	public function ThrowException($arErr) {
		CProjectUtils::SetAppErrors($arErr);
	}

	protected function ConvertToUtf8($mConvert) {
		if(is_array($mConvert)) {
			foreach($mConvert as $sKey => $mValue) {
				if(is_string($mValue)) {
					if(strlen($mValue)) {
						$mConvert[$sKey] = $GLOBALS['APPLICATION']->ConvertCharset($mValue, 'Windows-1251', 'UTF-8');
					}
				} elseif(is_array($mValue)) {
					$mConvert[$sKey] = $this->ConvertToUtf8($mValue);
				}
			}
		} elseif(is_string($mConvert)) {
			$mConvert = $GLOBALS['APPLICATION']->ConvertCharset($mConvert, 'Windows-1251', 'UTF-8');
		}
		return $mConvert;
	}

	protected function ConvertToWindows1251($mConvert) {
		if(is_array($mConvert)) {
			foreach($mConvert as $sKey => $mValue) {
				if(is_string($mValue)) {
					if(strlen($mValue)) {
						$mConvert[$sKey] = $GLOBALS['APPLICATION']->ConvertCharset($mValue, 'UTF-8', 'Windows-1251');
					}
				} elseif(is_array($mValue)) {
					$mConvert[$sKey] = $this->ConvertToWindows1251($mValue);
				}
			}
		} elseif(is_string($mConvert)) {
			$mConvert = $GLOBALS['APPLICATION']->ConvertCharset($mConvert, 'UTF-8', 'Windows-1251');
		}
		return $mConvert;
	}

	public function HttpRequest($sRequestMethod = 'GET', $sRequestUri = '', $sRequestParams = '', $sContentType = 'N') {
		$arReturn = array();
		$sRequestMethod = strtoupper($sRequestMethod);
		$sRequestUri = '/'.ltrim(trim($sRequestUri), '/');
		$obHTTP = new CHTTP();
		$obHTTP->http_timeout = $this->GetParam('TIMEOUT');
		$obHTTP->Query(
			$sRequestMethod,
			$this->GetParam('DOMAIN'),
			$this->GetParam('PORT'),
			$sRequestUri.($sRequestMethod == 'GET' ? ((strpos($sRequestUri, '?') === false ? '?' : '&').$sRequestParams) : ''),
			$sRequestMethod == 'POST' ? $sRequestParams : false,
			'',
			$sContentType
		);

		$arReturn['errno'] = $obHTTP->errno;
		$arReturn['errstr'] = $obHTTP->errstr;
		$arReturn['result'] = $obHTTP->result;

		return $arReturn;
	}

	//
	// Выполняет запрос к веб-сервису, возвращает xml c информацией по дисконтной карте Нияма
	//
	public function GetDiscountCard($sCardCode = '', $bGetTransact = false) {
		$sReturn = '';

		$sCardCode = str_replace('&', '&amp;', $sCardCode); // подстрахуемся, чтобы через & другие параметры не передали
		$sCardCode = htmlspecialcharsbx(trim($sCardCode));
		$sRequestParams = 'card='.$sCardCode;
		if($bGetTransact) {
			$sRequestParams .= '&trans=1';
		}
		$arTmpResult = $this->HttpRequest('GET', '/', $sRequestParams);
		$arErr = array();
		if(!$arTmpResult['errstr']) {
			if($arTmpResult['result'] && strlen($arTmpResult['result'])) {
				$sReturn = $arTmpResult['result'];
			} else {
				$arErr = array(
					'ERROR_CODE' => 'UNKNOWN_ERROR',
					'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
				);
			}
		} else {
			$arErr = array(
				'ERROR_CODE' => 'REQUEST_ERROR',
				'ERROR_MSG' => $arTmpResult['errstr']
			);
		}
		if($arErr) {
			$this->ThrowException($arErr);
		}
		return $sReturn;
	}

	public function GetDiscountCardArray($sCardCode = '', $bGetTransact = false) {
		$arReturn = array();
		$sResultXml = $this->GetDiscountCard($sCardCode, $bGetTransact);
		if(strlen($sResultXml)) {
			$obXmlContent = @simplexml_load_string($sResultXml);
			if($obXmlContent) {
				$arTmpXmlItems = $obXmlContent->Xpath('/Card');
				if($arTmpXmlItems) {
					$obXmlItem = reset($arTmpXmlItems);
					$arReturn['Number'] = trim($obXmlItem->Number);
					if($obXmlItem->GetErrorText) {
						$arReturn['GetErrorText'] = trim($obXmlItem->GetErrorText);
					} else {
						$arReturn['User'] = array();
						if($obXmlItem->User) {
							$obXmlTmp = $obXmlItem->User;
							$arReturn['User']['Holder'] = trim($obXmlTmp->Holder);
							$arReturn['User']['Birthday'] = trim($obXmlTmp->Birthday);
							$arReturn['User']['Female'] = trim($obXmlTmp->Female);
							$arReturn['User']['Tel1'] = trim($obXmlTmp->Tel1);
							$arReturn['User']['Tel2'] = trim($obXmlTmp->Tel2);
							$arReturn['User']['Email'] = trim($obXmlTmp->Email);
							$arReturn['User']['Address'] = trim($obXmlTmp->Address);
							$arReturn['User']['DopInfo'] = trim($obXmlTmp->DopInfo);
						}

						$arReturn['Account'] = array();
						if($obXmlItem->Account) {
							$obXmlTmp = $obXmlItem->Account;
							$arReturn['Account']['Account'] = trim($obXmlTmp->Account);
							$arReturn['Account']['Card'] = trim($obXmlTmp->Card);
							$arReturn['Account']['Scheme'] = trim($obXmlTmp->Scheme);
							$arReturn['Account']['Offered'] = trim($obXmlTmp->Offered);
							$arReturn['Account']['Expired'] = trim($obXmlTmp->Expired);
							$arReturn['Account']['Deleted'] = trim($obXmlTmp->Deleted);
							$arReturn['Account']['Locked'] = trim($obXmlTmp->Locked);
							$arReturn['Account']['Seize'] = trim($obXmlTmp->Seize);
							$arReturn['Account']['Discount'] = trim($obXmlTmp->Discount);
							$arReturn['Account']['Bonus'] = trim($obXmlTmp->Bonus);
							$arReturn['Account']['PayLimit'] = trim($obXmlTmp->PayLimit);
							$arReturn['Account']['Folder'] = trim($obXmlTmp->Folder);
							$arReturn['Account']['Unpay'] = trim($obXmlTmp->Unpay);
							$arReturn['Account']['Balance'] = trim($obXmlTmp->Balance);
						}

						if($bGetTransact) {
							$arReturn['Transaction'] = array();
							$arTmpXmlItems2 = $obXmlItem->Xpath('Transaction/info');
							foreach($arTmpXmlItems2 as $obXmlItem2) {
								$arReturn['Transaction'][] = array(
									'check' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'check'),
									'name' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'name'),
									'money' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'money'),
									'type' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'type'),
									'date' => CNiyamaImportUtils::GetXmlAttrValue($obXmlItem2, 'date'),
								);
							}
						}
					}
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Добавляет новую дисконтную карту Нияма
	 *	$arFields = array(
	 *		'tel1' => 'телефон 1', // *
	 *		'tel2' => 'телефон 2',
	 *		'email' => 'e-mail клиента', // *
	 *		'address' => 'адрес',
	 *		'birthday' => 'день рождения',
	 *		'holder' => 'ФИО',
	 *		'expired' => 'срок действия',
	 *		'discount' => 'код скидки', // *
	 *		'bonus' => 'номер бонуса',
	 *		'scheme' => 'тип счёта',
	 *		'paylimit' => 'лимит оплат для счетов c лимитом',
	 *		'folder' => 'идентификатор отдела',
	 *		'DiscountTypeLimit' => '?',
	 *		'DiscountLimit' => '?',
	 *		'DiscountLevelName' => '?',
	 *		'Balance' => 'текущий остаток',
	 *		'Seize' => 'карту изъять? (yes/no)',
	 *	)
	 * return string
	 */
	public function AddDiscountCard($sCardNum, $arFields) {
		$sReturn = '';
		// Первый параметр всегда card, далее если карта есть, он его меняет, если нет - создает
		$arTmpFields['card'] = trim($sCardNum);
		$arTmpFields['card'] = str_replace('&', '&amp;', $sCardNum); // подстрахуемся, чтобы через & другие параметры не передали
		$arTmpFields['card'] = htmlspecialcharsbx(trim($arTmpFields['card']));
		$arTmpFields['redact'] = '1';
		if(isset($arFields['tel1'])) {
			$arFields['tel1'] = self::FormatTel($arFields['tel1']);
		}

		$arFields = array_merge($arTmpFields, $arFields);
		$arFields = CProjectUtils::ClearArray($arFields);
		$arErr = $this->CheckDiscountCardFields($arFields, true);
		if(empty($arErr)) {
			$sRequestParams = http_build_query($arFields);
			$arTmpResult = $this->HttpRequest('GET', '/', $sRequestParams);
			$arErr = array();
			if(!$arTmpResult['errstr']) {
				if($arTmpResult['result'] && strlen($arTmpResult['result'])) {
					$sReturn = $arTmpResult['result'];
				} else {
					$arErr = array(
						'ERROR_CODE' => 'UNKNOWN_ERROR',
						'ERROR_MSG' => 'Веб-сервис вернул неизвестный ответ'
					);
				}
			} else {
				$arErr = array(
					'ERROR_CODE' => 'REQUEST_ERROR',
					'ERROR_MSG' => $arTmpResult['errstr']
				);
			}
		}
		if($arErr) {
			$this->ThrowException($arErr);
		}
		return $sReturn;
	}

	// делает то же самое, что и AddDiscountCard, только возвращает результат в виде массива
	public function AddDiscountCardEx($sCardNum, $arFields) {
		$arReturn = array();
		$sResultXml = $this->AddDiscountCard($sCardNum, $arFields);
		if(strlen($sResultXml)) {
			if(strpos(trim($sResultXml), '<?xml ') !== 0) {
				$arReturn['NoXml'] = trim($sResultXml);
			} else {
				$obXmlContent = simplexml_load_string($sResultXml);
				if($obXmlContent) {
					$arTmpXmlItems = $obXmlContent->Xpath('/Card');
					if($arTmpXmlItems) {
						$obXmlItem = reset($arTmpXmlItems);
						$arReturn['Number'] = trim($obXmlItem->Number);
						if($obXmlItem->GetErrorText) {
							$arReturn['GetErrorText'] = trim($obXmlItem->GetErrorText);
						} else {
							$arReturn['User'] = array();
							if($obXmlItem->User) {
								$obXmlTmp = $obXmlItem->User;
								$arReturn['User']['Holder'] = trim($obXmlTmp->Holder);
								$arReturn['User']['Birthday'] = trim($obXmlTmp->Birthday);
								$arReturn['User']['Female'] = trim($obXmlTmp->Female);
								$arReturn['User']['Tel1'] = trim($obXmlTmp->Tel1);
								$arReturn['User']['Tel2'] = trim($obXmlTmp->Tel2);
								$arReturn['User']['Email'] = trim($obXmlTmp->Email);
								$arReturn['User']['Address'] = trim($obXmlTmp->Address);
								$arReturn['User']['DopInfo'] = trim($obXmlTmp->DopInfo);
							}

							$arReturn['Account'] = array();
							if($obXmlItem->Account) {
								$obXmlTmp = $obXmlItem->Account;
								$arReturn['Account']['Account'] = trim($obXmlTmp->Account);
								$arReturn['Account']['Card'] = trim($obXmlTmp->Card);
								$arReturn['Account']['Scheme'] = trim($obXmlTmp->Scheme);
								$arReturn['Account']['Offered'] = trim($obXmlTmp->Offered);
								$arReturn['Account']['Expired'] = trim($obXmlTmp->Expired);
								$arReturn['Account']['Deleted'] = trim($obXmlTmp->Deleted);
								$arReturn['Account']['Locked'] = trim($obXmlTmp->Locked);
								$arReturn['Account']['Seize'] = trim($obXmlTmp->Seize);
								$arReturn['Account']['Discount'] = trim($obXmlTmp->Discount);
								$arReturn['Account']['Bonus'] = trim($obXmlTmp->Bonus);
								$arReturn['Account']['PayLimit'] = trim($obXmlTmp->PayLimit);
								$arReturn['Account']['Folder'] = trim($obXmlTmp->Folder);
								$arReturn['Account']['Unpay'] = trim($obXmlTmp->Unpay);
								$arReturn['Account']['Balance'] = trim($obXmlTmp->Balance);
							}
						}
					}
				}
			}
		}
		return $arReturn;
	}

	/**
	 * Редактирует дисконтную карту Нияма
	 *	$arFields = array(
	 *		'Tel1' => 'телефон 1', // *
	 *		'Tel2' => 'телефон 2',
	 *		'Email' => 'e-mail клиента', // *
	 *		'Address' => 'адрес',
	 *		'Birthday' => 'день рождения',
	 *		'Holder' => 'ФИО',
	 *		'Expired' => 'срок действия',
	 *		'Discount' => 'код скидки', // *
	 *		'Bonus' => 'номер бонуса',
	 *		'Scheme' => 'тип счёта',
	 *		'PayLimit' => 'лимит оплат для счетов c лимитом',
	 *		'Folder' => 'идентификатор отдела',
	 *		'DiscountTypeLimit' => '?',
	 *		'DiscountLimit' => '?',
	 *		'DiscountLevelName' => '?',
	 *		'Balance' => 'текущий остаток',
	 *		'Seize' => 'карту изъять? (yes/no)',
	 *	)
	 * return string
	 */
	public function EditDiscountCard($sCardCode, $arFields) {
		// общий метод для редактирования и создания новой карты
		return $this->AddDiscountCard($sCardCode, $arFields);
	}

	// делает то же самое, что и EditDiscountCard, только возвращает результат в виде массива
	public function EditDiscountCardEx($sCardCode, $arFields) {
		// общий метод для редактирования и создания новой карты
		return $this->AddDiscountCardEx($sCardCode, $arFields);
	}

	protected function CheckDiscountCardFields(&$arFields, $bNew = true) {
		$arReturn = array();
		if(!$bNew && (!isset($arFields['card']) || !strlen(trim($arFields['card'])))) {
			$arReturn['CARD_NUM_EMPTY'] = 'Не задан номер карты';
		}
		return $arReturn;
	}

	// приводит номер телефона к формату 111 222-33-44
	public static function FormatTel($sVal) {
		$sReturn = '';
		$sVal = trim($sVal);
		// удаляем код страны
		if(strpos($sVal, '+7') === 0) {
			$sVal = substr($sVal, 2);
		}
		// удаляем междугородный код внутри страны
		if(strpos($sVal, '8 ') === 0) {
			$sVal = substr($sVal, 2);
		}
		// оставляем только цифры
		$sVal = CCustomProject::GetNumsOnly($sVal);
		// удаляем первую семерку, если похоже, что забыли плюс
		if(strlen($sVal) == 11 && strpos($sVal, '7') === 0) {
			$sVal = substr($sVal, 1);
		}
		if(strlen($sVal) == 11 && strpos($sVal, '8') === 0) {
			$sVal = substr($sVal, 1);
		}

		$sReturn = $sVal;
		if(strlen($sVal) >= 7) {
			$arVal = str_split($sVal, '1');
			if(count($arVal) == 10) {
				$sReturn = $arVal[0].$arVal[1].$arVal[2].' '.$arVal[3].$arVal[4].$arVal[5].'-'.$arVal[6].$arVal[7].'-'.$arVal[8].$arVal[9];
			} elseif(count($arVal) == 7) {
				// оставим возможность для указания номеров дефолт-сити
				$sReturn = $arVal[0].$arVal[1].$arVal[2].'-'.$arVal[3].$arVal[4].'-'.$arVal[5].$arVal[6];
			}
		}

		return $sReturn;
	}
}

<?

class CNiyamaCoupons {
	public static $arTypeIB = array('user', 'main');
	public static $BONUS_NIYAM = 'COUNT_NIYAM';
	public static $BONUS_RUB = 'COUNT_RUB';
	public static $CONST_DISCOUNT = 'DISCOUNT';
	public static $CONST_DISCOUNT_SUM = 'DISCOUNT_NO_SUM';


	public static function GetIbByType($typeIB) {
		$res = false;
		switch($typeIB) {
			case 'user':
				$res = 'IB_NIYAMA_CUPONS_USERS';
			break;

			case 'main':
				$res = 'IB_NIYAMA_CUPONS';
			break;
		}
		return $res;
	}

	public static function GetArrEnumByProp($prop_code,$name_IB) {
		$arReturn=array();
		$sCacheEntity = 'arr_enum_'.$name_IB;
		if(CStaticCache::IsSetCache($prop_code, $sCacheEntity)) {
			$arReturn = CStaticCache::GetCacheValue($prop_code, $sCacheEntity);
		} else {
			CModule::IncludeModule('iblock');
			$db_enum_list = CIBlockProperty::GetPropertyEnum($prop_code, array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
			while($ar_enum_list = $db_enum_list->GetNext()) {
				$arReturn[$ar_enum_list['ID']] = $ar_enum_list['XML_ID'];
			}
			CStaticCache::SetCacheValue($prop_code, $arReturn, $sCacheEntity, 100);
		}
		return $arReturn;
	}

	public static function GetArrEnumXmlIdByProp($prop_code,$name_IB) {
		$arReturn = array();
		$sCacheEntity = 'arr_enum_xml_'.$name_IB;
		if(CStaticCache::IsSetCache($prop_code, $sCacheEntity)) {
			$arReturn = CStaticCache::GetCacheValue($prop_code, $sCacheEntity);
		} else {
			CModule::IncludeModule('iblock');
			$db_enum_list = CIBlockProperty::GetPropertyEnum($prop_code, array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
			while($ar_enum_list = $db_enum_list->GetNext()) {
				$arReturn[$ar_enum_list['XML_ID']]=$ar_enum_list['ID'];
			}
			CStaticCache::SetCacheValue($prop_code, $arReturn, $sCacheEntity, 100);
		}
		return $arReturn;
	}

	public static function GetCouponById($coupon_id, $user_id = 0) {
		$result = array();
		if($coupon_id == self::$BONUS_NIYAM || $coupon_id == self::$BONUS_RUB || $coupon_id == self::$CONST_DISCOUNT) {
			return self::GetCouponByType($coupon_id, $user_id);
		}
		if(substr($coupon_id, 0, 3) == 'NEW') {
			$typeIB = self::$arTypeIB[1];
			$coupon_id = substr($coupon_id, 3);
		} else {
			$typeIB = self::$arTypeIB[0];
		}
		$coupon_id = intval($coupon_id);
		$name_IB = self::GetIbByType($typeIB);

		if(!CModule::IncludeModule('iblock') || !$name_IB || $coupon_id <= 0) {
			return $result;
		}

		$type_bludo = array();
		/*
		$db_enum_list = CIBlockProperty::GetPropertyEnum('TYPE', array(), array('IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_BLUDA')));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$type_bludo[$ar_enum_list['ID']] = $ar_enum_list['XML_ID'];
		}
		*/
		$type_img = self::GetArrEnumByProp('BASE_TYPE_IMG', $name_IB);
		$type_bonus = self::GetArrEnumByProp('BASE_BONUS_TYPE', $name_IB);
		$type_money = self::GetArrEnumByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE', $name_IB);
		$type_visual = self::GetArrEnumByProp('BASE_BONUS_TYPE_BLUDO_VISIUAL',$name_IB);
		$sum_discount = self::GetArrEnumByProp('BASE_BONUS_NO_SUM', $name_IB);

		/*
		$type_img=array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("BASE_TYPE_IMG", array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$type_img[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
		}
		$type_bonus=array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("BASE_BONUS_TYPE", array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$type_bonus[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
		}
		$type_money=array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE", array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$type_money[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
		}
		$type_visual=array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("BASE_BONUS_TYPE_BLUDO_VISIUAL", array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$type_visual[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
		}
		$sum_discount=array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("BASE_BONUS_NO_SUM", array(), array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
		while($ar_enum_list = $db_enum_list->GetNext()) {
			$sum_discount[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
		}
		*/

		$arFilter = array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB), 
			'ID' => $coupon_id,
		);
		$dbItems = CIBlockElement::GetList(
			array(
				'active_from' => 'DESC'
			), 
			$arFilter, 
			false,
			false,
			array(
				'ID', 'ACTIVE', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PREVIEW_TEXT',

				'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H', 'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V',
				'PROPERTY_COUPON_ID', 'PROPERTY_BASE_TYPE_IMG', 'PROPERTY_BASE_BONUS_TYPE', 
				'PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT', 'PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE', 
				'PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY', 'PROPERTY_BASE_BONUS_TYPE_BLUDO', 
				'PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL', 'PROPERTY_BASE_BONUS_NO_SUM',
			)
		);
		while($ar_fields = $dbItems->GetNext()) {
			$bluds = array();
			$pics = array();
			$sum_old_priie = 0;
			$sum_new_priie = 0;
			if(!empty($ar_fields['PROPERTY_BASE_BONUS_TYPE_BLUDO_VALUE'])) {
				foreach($ar_fields['PROPERTY_BASE_BONUS_TYPE_BLUDO_VALUE'] as $key => $bludo_ID) {
					$bludo = CNiyamaCatalog::GetProductDetail($bludo_ID);
					if(!empty($bludo)) {
						$blud_ar = array(
							'NEW_PRICE' => $ar_fields['PROPERTY_BASE_BONUS_TYPE_BLUDO_DESCRIPTION'][$key],
							'OLD_PRICE' => $bludo['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
							'DISCOUNT' => (1 - $ar_fields['PROPERTY_BASE_BONUS_TYPE_BLUDO_DESCRIPTION'][$key] / $bludo['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']) * 100,
							'IMG' => $bludo['INFO']['PREVIEW_PICTURE'],
							'ID' => $bludo['INFO']['ID'],
							'NAME' => $bludo['INFO']['NAME'],
						);
						if(!empty($bludo['INFO']['PREVIEW_PICTURE'])) {
							$pics[] = $bludo['INFO']['PREVIEW_PICTURE'];
						}
						$bluds[] = $blud_ar;
						$sum_old_priie += $blud_ar['OLD_PRICE'];
						$sum_new_priie += $blud_ar['NEW_PRICE'];
					}
				}
			}
			$result = array(
				'ID' => $ar_fields['ID'],
				'ACTIVE' => $ar_fields['ACTIVE'],
				'NAME' => $ar_fields['NAME'],
				//'IMG1' => $ar_fields['PREVIEW_PICTURE'],
				//'IMG2' => $ar_fields['DETAIL_PICTURE'],
				'IMG1' => $ar_fields['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H_VALUE'],
				'IMG2' => $ar_fields['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V_VALUE'],
				'TEXT' => $ar_fields['PREVIEW_TEXT'],
				'BLUDS' => $bluds,
				'PICS_BLUDS' => $pics,
				'SUM_OLD_PRICE' => $sum_old_priie,
				'SUM_NEW_PRICE' => $sum_new_priie,
				'TYPE_IMG' => $type_img[$ar_fields['PROPERTY_BASE_TYPE_IMG_ENUM_ID']],
				'BONUS_TYPE' => $type_bonus[$ar_fields['PROPERTY_BASE_BONUS_TYPE_ENUM_ID']],
				'DISCOUNT_PER' => $ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT_VALUE'],
				'MONEY_TYPE' => $type_money[$ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE_ENUM_ID']],
				'MONEY' => $ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_VALUE'],
				'VISUAL' => $type_visual[$ar_fields['PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL_ENUM_ID']],
				'BONUS_NO_SUM' => $sum_discount[$ar_fields['PROPERTY_BASE_BONUS_NO_SUM_ENUM_ID']] == 'Yes' ? 'Y' : 'N',
			);
			if(!empty($result['MONEY'])) {
				$result['BONUS_VAL'] = $result['MONEY'];
			} elseif(!empty($result['DISCOUNT_PER'])) {
				$result['BONUS_VAL'] = $result['DISCOUNT_PER'];
			}
		}
		return $result;
	}

	public static function GetCouponByType($type, $user_id = 0) {
		// LSV: странное условие. Здесь предусматривается возможность выборки юзера по сложной логике?
		if(empty($user_id) || $user_id <= 0) {
			$user_id = $GLOBALS['USER']->GetID();
		}
		$result = false;
		if(empty($user_id)) {
			return $result;
		}

		$typeUF = 'UF_'.$type;
		$arFilter = array('ID' => $user_id);
		$arParams['SELECT'] = array($typeUF);
		$arRes = CUser::GetList($by = 'id', $order = 'asc', $arFilter, $arParams);
	 	if($res = $arRes->Fetch()) {
			if($res[$typeUF] > 0) {
				switch($type) {
					case self::$BONUS_NIYAM:
						$result = array(
							'NAME' => 'Бонусы ниямы',
							'ACTIVE' => 'Y',
							'TYPE_IMG' => 'DEFAULT',
							'BONUS_TYPE' => 'DISCOUNT_MONEY',
							'MONEY_TYPE' => 'NIYAM',
							'MONEY' => $res[$typeUF],
							'BONUS_VAL' => $res[$typeUF],
							'ID' => $type,
							'TEXT' => CNiyamaCustomSettings::getStringValue("text_coupon_by_niam", '')
						);
					break;

					case self::$BONUS_RUB:
						$result = array(
							'NAME' => 'Бонусы рубли',
							'ACTIVE' => 'Y',
							'TYPE_IMG' => 'DEFAULT',
							'BONUS_TYPE' => 'DISCOUNT_MONEY',
							'MONEY_TYPE' => 'MONEY',
							'MONEY' => $res[$typeUF],
							'BONUS_VAL' => $res[$typeUF],
							'ID' => $type,
							'TEXT' => CNiyamaCustomSettings::getStringValue("text_coupon_by_rub", '')
						);
					break;

					case self::$CONST_DISCOUNT:
						$no_sum = 'Y';
						$UF_no_sum = 'UF_'.self::$CONST_DISCOUNT_SUM;
						$arFilter_no_sum = array('ID' => $user_id);
						$arParams_no_sum['SELECT'] = array($UF_no_sum);
						$arRes_no_sum = CUser::GetList($by = 'id', $order = 'asc', $arFilter_no_sum, $arParams_no_sum);
						if($res_no_sum = $arRes_no_sum->Fetch()) {
							$no_sum = $res_no_sum[$UF_no_sum] == 1 ? 'Y' : 'N';
						}

						$result = array(
							'NAME' => 'Постоянная скидка',
							'ACTIVE' => 'Y',
							'TYPE_IMG' => 'DEFAULT',
							'BONUS_TYPE' => 'CONST_DISCOUNT',
							'DISCOUNT_PER' => $res[$typeUF],
							'BONUS_VAL' => $res[$typeUF],
							'ID' => $type,
							'BONUS_NO_SUM' => $no_sum,
						);
					break;
				}
			}
	 	}
		return $result;
	}

	public static function GetCouponValByType($type, $user_id = 0) {
		if((empty($user_id))||($user_id<=0)) {
			global $USER;
			$user_id=$USER->GetID();
		}
		$result=0;
		if($user_id<=0) {
			return $result;
		}
		$typeUF='UF_'.$type;
		$arFilter = array("ID" => $user_id);
		$arParams["SELECT"] = array($typeUF);
		$arRes = CUser::GetList(($by="id"), ($order="asc"),$arFilter,$arParams);
	 	if($res = $arRes->Fetch()) {
			if($res[$typeUF]>0) {
				return $res[$typeUF];
			}
	 	}
		return $result;
	}

	public static function GetListDetailByUserId($user_id) {
		$result=array();
		if((!empty($user_id))&&($user_id>0)) {
			CModule::IncludeModule('iblock');
			$name_IB=self::GetIbByType(self::$arTypeIB[0]);
			$type_bonus=self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE',$name_IB);
			$arFilter = array(
				"LOGIC" => "OR",
				array(
					"IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
					'=PROPERTY_USER_ID'=>$user_id,
					'>=PROPERTY_BASE_QUANITY'=>1,
					'!PROPERTY_BASE_BONUS_TYPE'=>array($type_bonus['DISCOUNT_MONEY'])
				),
				array(
					"IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
					'=PROPERTY_USER_ID'=>$user_id,
					'>=PROPERTY_BASE_QUANITY'=>1,
					'ACTIVE'=> 'N',
					'PROPERTY_BASE_BONUS_TYPE'=>array($type_bonus['DISCOUNT_MONEY'])
				),
			);
			$res = CIBlockElement::GetList(array("active_from"=>"DESC"), $arFilter, false,false, array("ID"));
			while($ar_fields = $res->GetNext()) {
				$result[]=self::GetCouponById($ar_fields['ID'],$user_id);
			}

			$base_coupon=array(
				self::$BONUS_NIYAM,
				self::$BONUS_RUB,
				self::$CONST_DISCOUNT
			);
			foreach ($base_coupon as $type) {
				$coupon=self::GetCouponByType($type,$user_id);
				if(!empty($coupon)) {
					$result[]=$coupon;
				}
			}
		}

		return $result;
	}

	public static function GetListByUserId($user_id) {
		$result = array();

		if(CModule::IncludeModule('iblock')) {
			$user_id = intval($user_id);
			if($user_id > 0) {
				$name_IB=self::GetIbByType(self::$arTypeIB[0]);
				$type_bonus=self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE',$name_IB);
				$arFilter = array(
					'LOGIC' => 'OR',
					array(
						'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
						'=PROPERTY_USER_ID' => $user_id,
						'>=PROPERTY_BASE_QUANITY' => 1,
						'!PROPERTY_BASE_BONUS_TYPE' => array($type_bonus['DISCOUNT_MONEY'])
					),
					array(
						'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
						'=PROPERTY_USER_ID' => $user_id,
						'>=PROPERTY_BASE_QUANITY' => 1,
						'ACTIVE' => 'N',
						'PROPERTY_BASE_BONUS_TYPE' => array($type_bonus['DISCOUNT_MONEY'])
					),
				);

				$res = CIBlockElement::GetList(array('active_from' => 'DESC'), $arFilter, false,false, array('ID'));
				while($ar_fields = $res->GetNext()) {
					$result[] = $ar_fields['ID'];
				}
			}
		}
		$result[] = self::$BONUS_NIYAM;
		$result[] = self::$BONUS_RUB;
		$result[] = self::$CONST_DISCOUNT;
		return $result;
	}

	public static function GetListInOrderByUserId($user_id, $order_id) {

		$result=array();
		if(CModule::IncludeModule('iblock')) {
			if((!empty($user_id))&&($user_id>0)&&(!empty($order_id))&&($order_id>0)) {
				$arFilter = array(
					'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
					'=PROPERTY_USER_ID' => $user_id,
					'PROPERTY_ORDER_ID' => $order_id,
					'>=PROPERTY_BASE_QUANITY' => 1,
				);

				$res = CIBlockElement::GetList(array("active_from"=>"DESC"), $arFilter, false,false, array("ID"));
				while($ar_fields = $res->GetNext()) {
					$result[]=$ar_fields['ID'];
				}
			}
		}
		return $result;
	}

	public static function GetListCurUser() {
		return self::GetListByUserId($GLOBALS['USER']->GetID());
	}

	public static function GetTypeForAll($id_coupon, $user_id=0) {
		$coupon=self::GetCouponById($id_coupon, $user_id);
		if($coupon['BONUS_TYPE']=='DISCOUNT_PERSENT') {
			return true;
		} elseif($coupon['BONUS_TYPE']=='CONST_DISCOUNT') {
			return true;
		} else {
			return false;
		}
	}

	public static function GetCuoponsForGuest($cartList) {
		$ar_coupons_each_guest=array();

		if(!empty($cartList['common'][0]['ITEMS'])) {
			foreach($cartList['common'][0]['ITEMS'] as $key=>$prod) {
				if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[1]) {
					if(CNiyamaCoupons::GetTypeForAll($prod['PRODUCT_ID'])) {
						$ar_coupons_each_guest[$key]=$prod;
					}
				}
			}
		}
		return $ar_coupons_each_guest;
	}

	public static function GetMaxPercentDiscount($cartList, $iOrderId = 0) {
		$max = 0;
		$sum = 0;
		$const_discount = 0;
		$const_discount_id = 0;
		if(!empty($cartList['common'][0]['ITEMS'])) {
			foreach($cartList['common'][0]['ITEMS'] as $key => $prod) {
				// если купон
				if($prod['PROPS']['type'] == 'cupon') {
					if($prod['PROPS']['type_bonus'] == 'DISCOUNT_PERSENT' || $prod['PROPS']['type_bonus'] == 'CONST_DISCOUNT') {
						if($prod['PROPS']['no_sum'] == 'Y') {
							if($prod['PROPS']['val_coupon'] > $max) {
								$max = $prod['PROPS']['val_coupon'];
							}
						} else {
							$sum += $prod['PROPS']['val_coupon'];
						}
						if($prod['PROPS']['type_bonus'] == 'CONST_DISCOUNT') {
							$const_discount = $prod['PROPS']['val_coupon'];
							$const_discount_id = $prod['ID'];
						}
					}
				}
			}
		}

		if($sum > $max) {
			$max = $sum;
		}

		$max_discount = intval(CNiyamaCustomSettings::GetStringValue('max_discount_by_order', 15));
		$max_discount_vip = intval(CNiyamaCustomSettings::GetStringValue('max_discount_vip_by_order', 45));
		$CUSTOMER_DELIVERY_TYPE = '';
		if($iOrderId > 0) {
			$CUSTOMER_DELIVERY_TYPE = CNiyamaOrders::GetOrderDeliveryTypeById($iOrderId);
		}
		if(!empty($CUSTOMER_DELIVERY_TYPE) && $CUSTOMER_DELIVERY_TYPE == 'self') {
			if($const_discount > $max_discount_vip) {
				$const_discount = $max_discount_vip;
			}
			if($const_discount > $max_discount) {
				$max = $max_discount = $const_discount;
			}
		}

		if($max > $max_discount) {
			$max = $max_discount;
		}
		if($const_discount > $max_discount) {
			$const_discount = $max_discount;
		}

		if($const_discount_id > 0) {
			if($cartList['common'][0]['ITEMS'][$const_discount_id]['PROPS']['val_coupon_used'] != $const_discount) {
				CNiyamaCart::SetValCouponUsed($const_discount_id, $const_discount);
			}
		}
		return $max;
	}

	/*
	$price Стоимость стола
	$discount Скидка в процентах
	$bonus_rub Сумма всех бонусов в руб для текщего стола
	*/
	public static function GetMaxNiamDiscount($price, $discount,$bonus_rub_used) {
		/*$max_discount_money = intval(CNiyamaCustomSettings::GetStringValue('max_discount_by_order', 15)) - $discount;
		if($max_discount_money < 0) {
			return 0;
		}*/

                $max_discount_money = intval(CNiyamaCustomSettings::GetStringValue('max_discount_by_order', 15));
		$max_discount_cfg = CNiyamaCustomSettings::getStringValue('max_discount_order_by_niam', 10);
		if($max_discount_money > $max_discount_cfg) {
			$max_discount_money = $max_discount_cfg;
		}
		$max_price = ceil($price * ($max_discount_money / 100)) - $bonus_rub_used;
		if($max_price < 0) {
			$max_price = 0;
		}
		return $max_price;
	}

	public static function GetMaxMoneyDiscount($price, $discount, $bonus_rub_used) {
		$max_discount_money = intval(CNiyamaCustomSettings::GetStringValue('max_discount_by_order', 15)) - $discount;
		if($max_discount_money < 0) {
			return 0;
		}
		$max_price = ceil($price * ($max_discount_money / 100)) - $bonus_rub_used;
		if($max_price < 0) {
			$max_price = 0;
		}
		return $max_price;
	}

	public static function SearchInCartNiamOrRub(&$cartList, $ids_bonus, $discount_per) {
		if(empty($cartList['DISCOUNT_RUB'])) {
			$cartList['DISCOUNT_RUB']= array(
				'all' => 0,
				'niam' => 0,
				'rub' => 0,
			);
		}
		if(!empty($ids_bonus)) {
			$koefficient_niam_by_rub = intval(CNiyamaCustomSettings::GetStringValue('koefficient_niam_by_rub', 100));
			foreach($ids_bonus as $id_cart => $item) {
				$bDeleteCurItem = false;
				if(!empty($cartList[$item['stol']][$item['guest']]['ITEMS'][$id_cart])) {
					$el =& $cartList[$item['stol']][$item['guest']];
					if(empty($el['DISCOUNT_RUB'])) {
						$el['DISCOUNT_RUB'] = array(
							'all' => 0,
							'niam' => 0,
							'rub' => 0,
						);
					}
					if(empty($el['OLD_PRICE'])) {
						$el['OLD_PRICE'] = $el['PRICE'];
					}
					$prod =& $cartList[$item['stol']][$item['guest']]['ITEMS'][$id_cart];
					if($prod['PROPS']['type'] == 'cupon') {
						if($prod['PROPS']['type_coupon'] == self::$BONUS_NIYAM) {
							// максимальная сумма заказа, которая может быть списана ниямами, руб.
							$max_niam_rub = self::GetMaxNiamDiscount($el['OLD_PRICE'], $discount_per, $el['DISCOUNT_RUB']['all']);
							// максимальная сумма заказа, которая может быть списана ниямами, баллы
							$max_niam = $max_niam_rub * $koefficient_niam_by_rub;
							// сделаем значение кратным $koefficient_niam_by_rub
							$max_niam = CCustomProject::GetDivisibleValue($max_niam, $koefficient_niam_by_rub);

							$prod['PROPS']['val_coupon'] -= $cartList['DISCOUNT_RUB']['niam'];
							// сделаем значение кратным $koefficient_niam_by_rub
							$prod['PROPS']['val_coupon'] = CCustomProject::GetDivisibleValue($prod['PROPS']['val_coupon'], $koefficient_niam_by_rub);

							// сделаем значения кратными $koefficient_niam_by_rub
							$prod['PROPS']['val_coupon_used'] = CCustomProject::GetDivisibleValue($prod['PROPS']['val_coupon_used'], $koefficient_niam_by_rub);

							if($prod['PROPS']['val_coupon'] > $max_niam) {
								$prod['PROPS']['val_coupon'] = $max_niam;
							} else {
								$max_niam_rub = ceil($prod['PROPS']['val_coupon'] / $koefficient_niam_by_rub);
							}

							$niam_val_coupon_used = $prod['PROPS']['val_coupon_used'];
							$niam_val_coupon_used_in_rub = ceil($prod['PROPS']['val_coupon_used'] / $koefficient_niam_by_rub);
							if($niam_val_coupon_used_in_rub > $prod['PROPS']['val_coupon']) {
								$niam_val_coupon_used_in_rub = $max_niam_rub;
								$niam_val_coupon_used = $prod['PROPS']['val_coupon'];
							}

							$el['DISCOUNT_RUB']['all'] += (int)$niam_val_coupon_used_in_rub;
							$cartList['DISCOUNT_RUB']['all'] += (int)$niam_val_coupon_used_in_rub;
							$el['DISCOUNT_RUB']['niam'] += $niam_val_coupon_used;
							$cartList['DISCOUNT_RUB']['niam'] += $niam_val_coupon_used;
							if($el['DISCOUNT_RUB']['niam'] != $prod['PROPS']['val_coupon_used']) {
								if($el['DISCOUNT_RUB']['niam'] > 0) {
									CNiyamaCart::SetValCouponUsed($prod['ID'], $el['DISCOUNT_RUB']['niam']);
								} else {
									// нулевой купон, удаляем его
									$bDeleteCurItem = true;
								}
							}
						} elseif($prod['PROPS']['type_coupon'] == self::$BONUS_RUB) {
							$max_rub = self::GetMaxMoneyDiscount($el['OLD_PRICE'], $discount_per, $el['DISCOUNT_RUB']['all']);
							$prod['PROPS']['val_coupon'] -= $cartList['DISCOUNT_RUB']['rub'];
							if($prod['PROPS']['val_coupon'] > $max_rub) {
								$prod['PROPS']['val_coupon'] = $max_rub;
							}
							$rub_val_coupon_used = $prod['PROPS']['val_coupon_used'];
							if($rub_val_coupon_used > $prod['PROPS']['val_coupon']) {
								$rub_val_coupon_used = $prod['PROPS']['val_coupon'];
							}
							$el['DISCOUNT_RUB']['all'] += $rub_val_coupon_used;
							$cartList['DISCOUNT_RUB']['all'] += $rub_val_coupon_used;
							$el['DISCOUNT_RUB']['rub'] += $rub_val_coupon_used;
							$cartList['DISCOUNT_RUB']['rub'] += $rub_val_coupon_used;
							if($el['DISCOUNT_RUB']['rub'] != $prod['PROPS']['val_coupon_used']) {
								CNiyamaCart::SetValCouponUsed($prod['ID'], $el['DISCOUNT_RUB']['rub']);
							}
						}
					}

					if($bDeleteCurItem) {
						CSaleBasket::Delete($prod['ID']);
						unset($prod);
					}

					$el['PRICE'] = (ceil($el['OLD_PRICE'] * (1 - $discount_per / 100))) - $el['DISCOUNT_RUB']['all'];
				}
			}
		}
		//$cartList['TOTAL_PRICE']=($cartList['OLD_TOTAL_PRICE']*(1-$discount_per/100))-$cartList['DISCOUNT_RUB']['all'];
		return;
	}

	public static function GetIdsBonus($item, &$ids_bonus, $stol, $guest) {
		if(!empty($item['ITEMS'])) {
			foreach($item['ITEMS'] as $key => &$prod) {
				if(($prod['PROPS']['type_coupon'] == self::$BONUS_NIYAM) || ($prod['PROPS']['type_coupon'] == self::$BONUS_RUB)) {
					$ids_bonus[$prod['ID']] = array(
						'stol' => $stol,
						'guest' => $guest
					);
				}
			}
			// !!! unlink
			unset($prod);
		}
	}

	public static function RecalculateCartByCoupons(&$cartList, $iOrderId = 0) {
		$max_discount = intval(self::GetMaxPercentDiscount($cartList, $iOrderId));

		$cartList['DISCOUNT_PERCENT'] = $max_discount;
		$cartList['DISCOUNT_TOTAL_RUB'] = 0;
		$cartList['OLD_TOTAL_PRICE'] = $cartList['TOTAL_PRICE'];
		$cartList['TOTAL_PRICE'] = 0;
		$ids_bonus = array();
		$discountByPercentInRub = 0;

		$cartList['common'][0]['OLD_PRICE'] = $cartList['common'][0]['PRICE'];
		$cartList['current'][0]['OLD_PRICE'] = $cartList['current'][0]['PRICE'];
		if(!empty($cartList['guest'])) {
			foreach($cartList['guest'] as $guest_id => $guet_ar) {
				$cartList['guest'][$guest_id]['OLD_PRICE'] = $cartList['guest'][$guest_id]['PRICE'];
			}
		}

		if($max_discount > 0) {
			$discount_rub = floor($cartList['common'][0]['PRICE'] * ($max_discount / 100));
			$discountByPercentInRub += $discount_rub;
			$cartList['common'][0]['PRICE'] = $cartList['common'][0]['PRICE'] - $discount_rub;

			$discount_rub = floor($cartList['current'][0]['PRICE'] * ($max_discount / 100));
			$discountByPercentInRub += $discount_rub;
			$cartList['current'][0]['PRICE'] = $cartList['current'][0]['PRICE'] - $discount_rub;
			if(!empty($cartList['guest'])) {
				foreach($cartList['guest'] as $guest_id => $guet_ar) {
					$discount_rub = floor($cartList['guest'][$guest_id]['PRICE'] * ($max_discount / 100));
					$discountByPercentInRub += $discount_rub;
					$cartList['guest'][$guest_id]['PRICE'] = $cartList['guest'][$guest_id]['PRICE'] - $discount_rub;
				}
			}
		}

		//$total=array('niam' => 0, 'rub' => 0, 'all' => 0, 'percent' => 0);
		self::GetIdsBonus($cartList['common'][0], $ids_bonus, 'common', 0);
		self::GetIdsBonus($cartList['current'][0], $ids_bonus, 'current', 0);
		if(!empty($cartList['guest'])) {
			foreach($cartList['guest'] as $guest_id => $guet_ar) {
				self::GetIdsBonus($cartList['guest'][$guest_id], $ids_bonus, 'guest', $guest_id);
			}
		}
		ksort($ids_bonus);

		self::SearchInCartNiamOrRub($cartList, $ids_bonus, $max_discount);

		$cartList['DISCOUNT_RUB']['persent'] = $discountByPercentInRub;
		$cartList['DISCOUNT_RUB']['all'] += $discountByPercentInRub;

		$cartList['TOTAL_PRICE'] += $cartList['common'][0]['PRICE'];
		$cartList['TOTAL_PRICE'] += $cartList['current'][0]['PRICE'];
		if(!empty($cartList['guest'])) {
			foreach($cartList['guest'] as $guest_id => $guet_ar) {
				$cartList['TOTAL_PRICE'] += $cartList['guest'][$guest_id]['PRICE'];
			}
		}
		/*
		if($iOrderId > 0) {
			CSaleOrder::Update(
				$iOrderId,
				array(
					'PRICE' => $cartList['TOTAL_PRICE'],
					'DISCOUNT_VALUE' => $cartList['DISCOUNT_RUB']['all'],
				)
			);
		}
		*/
		return true;
	}

	public static function CalcArPriceByCoupons($iOrderId, $ar_basket_quanity) {
		$arReturn = array(
			'DISCOUNT_ALL' => 0,
			'TOTAL_PRICE' => 0,
			'OLD_TOTAL_PRICE' => 0
		);
		$res = CNiyamaCart::GetCartListWithCalcNewQuantity($iOrderId, $ar_basket_quanity);

		if($res['DISCOUNT_RUB']['all'] > 0) {
			$arReturn['DISCOUNT_ALL'] = $res['DISCOUNT_RUB']['all'];
		}
		if($res['TOTAL_PRICE'] > 0) {
			$arReturn['TOTAL_PRICE'] = $res['TOTAL_PRICE'];
		}
		if($res['OLD_TOTAL_PRICE'] > 0) {
			$arReturn['OLD_TOTAL_PRICE'] = $res['OLD_TOTAL_PRICE'];
		}

		return $arReturn;
	}

	public static function AddDefCoupons() {
		CNiyamaCart::AddToCart('DISCOUNT', 1, CNiyamaCart::$iForAllGuestID, CNiyamaCart::$arCartItemType[1]);
	}

	public static function GetIDsCouponsByCart() {
		$ids_coupon = array();
		$cart = CNiyamaCart::GetCartList();

		self::GetIDsCouponsByCartUserArr($cart['current'][0], $ids_coupon);
		if($cart['guest']) {
			foreach($cart['guest'] as $user) {
				self::GetIDsCouponsByCartUserArr($user, $ids_coupon);
			}
		}
		self::GetIDsCouponsByCartUserArr($cart['common'][0], $ids_coupon);
		return $ids_coupon;
	}

	public static function GetIDsCouponsByCartUserArr($userArr, &$ids_coupon = array()) {
		if(!empty($userArr['ITEMS'])) {
			foreach($userArr['ITEMS'] as $el) {
				if($el['PROPS']['type'] == CNiyamaCart::$arCartItemType[1]) {
					if(isset($ids_coupon[$el['PRODUCT_ID']])) {
						if($el['PROPS']['type_bonus'] == 'DISCOUNT_MONEY') {
							$ids_coupon[$el['PRODUCT_ID']]['PROPS']['val_coupon_used']+=$el['PROPS']['val_coupon_used'];
						}
					} else {
						$ids_coupon[$el['PRODUCT_ID']] = $el;
					}
				}
			}
		}
		return $ids_coupon;
	}

	public static function AddCouponByArrTimeline($arDiscountLevel) {
		CModule::IncludeModule('iblock');
		$name_IB = self::GetIbByType('user');
		$arr_coupons = array();
		$user_fields = array();
		$defCoupon = array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB),
			'ACTIVE' => 'Y',
			'NAME' => $arDiscountLevel['NAME'],
			'PREVIEW_TEXT' => $arDiscountLevel['PREVIEW_TEXT'],
			'DETAIL_TEXT' => $arDiscountLevel['DETAIL_TEXT'],
		);
		$defProp = array(
			'COUPON_ID' => $arDiscountLevel['ID'],
			'USER_ID' => $arDiscountLevel['USER_ID'],
			'BASE_QUANITY' => 1,
		);
		$type_bonus = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE', $name_IB);
		$type_img = self::GetArrEnumXmlIdByProp('BASE_TYPE_IMG', $name_IB);
		$type_money = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE', $name_IB);
		$type_visual = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_BLUDO_VISIUAL', $name_IB);
		$sum_discount = self::GetArrEnumXmlIdByProp('BASE_BONUS_NO_SUM', $name_IB);

		if($arDiscountLevel['DISCOUNT_VAL_PERC'] > 0) {
			if($arDiscountLevel['B_ONE_TIME'] == 'Y') {
				// единоразовая скидка
				$addCoupon = $defCoupon;
				$addProp = $defProp;
				$addProp['BASE_BONUS_TYPE'] = array('VALUE' => $type_bonus['DISCOUNT_PERSENT']);
				$addProp['BASE_TYPE_IMG'] = array('VALUE' => $type_img['DEFAULT']);
				$addProp['BASE_BONUS_TYPE_DISCOUNT_PERSENT'] = $arDiscountLevel['DISCOUNT_VAL_PERC'];
				$addProp['BASE_BONUS_NO_SUM'] = array('VALUE' => $arDiscountLevel['B_SINGLE'] == 'Y' ? $sum_discount['Yes'] : false);
				$addCoupon['PROPERTY_VALUES'] = $addProp;
				$arr_coupons[] = $addCoupon;
			} else {
				// постоянная скидка
				$val_coupon = self::GetCouponValByType(self::$CONST_DISCOUNT);
				if($val_coupon < $arDiscountLevel['DISCOUNT_VAL_PERC']) {
					$user_fields['UF_'.self::$CONST_DISCOUNT] = $arDiscountLevel['DISCOUNT_VAL_PERC'];
					$user_fields['UF_'.self::$CONST_DISCOUNT_SUM] = $arDiscountLevel['B_SINGLE'] == 'Y' ? true : false;
				}
			}
		}
		if($arDiscountLevel['DISCOUNT_VAL_RUB'] > 0) {
			// скидка в рублях
			$addCoupon = $defCoupon;
			$addProp = $defProp;
			$addProp['BASE_BONUS_TYPE'] = array('VALUE' => $type_bonus['DISCOUNT_MONEY']);
			$addProp['BASE_TYPE_IMG'] = array('VALUE' => $type_img['DEFAULT']);
			$addProp['BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE'] = array('VALUE' => $type_money['MONEY']);
			$addProp['BASE_BONUS_TYPE_DISCOUNT_MONEY'] = $arDiscountLevel['DISCOUNT_VAL_RUB'];
			$addProp['BASE_BONUS_NO_SUM'] = array('VALUE' => ($arDiscountLevel['B_SINGLE'] == 'Y') ? $sum_discount['Yes'] : false);
			$addCoupon['PROPERTY_VALUES'] = $addProp;
			$arr_coupons[] = $addCoupon;

			$val_coupon = self::GetCouponValByType(self::$BONUS_RUB);
			$user_fields['UF_'.self::$BONUS_RUB] = $val_coupon + $arDiscountLevel['DISCOUNT_VAL_RUB'];
		}
		if($arDiscountLevel['NIYAM_CNT'] > 0) {
			// скидка в ниямах
			$addCoupon = $defCoupon;
			$addProp = $defProp;
			$addProp['BASE_BONUS_TYPE'] = array('VALUE' => $type_bonus['DISCOUNT_MONEY']);
			$addProp['BASE_TYPE_IMG'] = array('VALUE' => $type_img['DEFAULT']);
			$addProp['BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE'] = array('VALUE' => $type_money['NIYAM']);
			$addProp['BASE_BONUS_TYPE_DISCOUNT_MONEY'] = $arDiscountLevel['NIYAM_CNT'];
			$addProp['BASE_BONUS_NO_SUM'] = array('VALUE' => ($arDiscountLevel['B_SINGLE']=='Y') ? $sum_discount['Yes'] : false);
			$addCoupon['PROPERTY_VALUES'] = $addProp;
			$arr_coupons[] = $addCoupon;
			$val_coupon = self::GetCouponValByType(self::$BONUS_NIYAM);
			$user_fields['UF_'.self::$BONUS_NIYAM] = $val_coupon + $arDiscountLevel['NIYAM_CNT'];
		}
		if($arDiscountLevel['BONUS_DISH'] > 0) {
			// бесплатное блюдо
			$addCoupon = $defCoupon;
			$addProp = $defProp;
			$addProp['BASE_BONUS_TYPE'] = array('VALUE' => $type_bonus['BLUDO']);
			$addProp['BASE_TYPE_IMG'] = array('VALUE' => $type_img['BLUDA']);
			$addProp['BASE_BONUS_TYPE_BLUDO'] = array($arDiscountLevel['BONUS_DISH']);
			$addProp['BASE_BONUS_TYPE_BLUDO_VISIUAL'] = array('VALUE' => $type_visual['CARTS']);
			$addProp['BASE_BONUS_NO_SUM'] = array('VALUE' => ($arDiscountLevel['B_SINGLE'] == 'Y') ? $sum_discount['Yes'] : false);
			$addCoupon['PROPERTY_VALUES'] = $addProp;
			$arr_coupons[] = $addCoupon;
		}

		if(!empty($arr_coupons)) {
			$obIBlockElement = new CIBlockElement();
			foreach ($arr_coupons as $coupon) {
				$result = $obIBlockElement->Add($coupon);
			}
		}
		if(!empty($user_fields)) {
			$user_fields['_SKIP_FO_SYNC_'] = 'Y';
			$obUser = new CUser();
			$obUser->Update($arDiscountLevel['USER_ID'], $user_fields);
		}
	}

	public static function GetListCouponsByTypeEvent($typeEvent) {
		$name_IB = self::GetIbByType(self::$arTypeIB[1]);

		$arTypeBASE_TYPE_ACTION_XML_ID = self::GetArrEnumXmlIdByProp('BASE_TYPE_ACTION',$name_IB);
		$iISBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS', 'NIYAMA_DISCOUNTS');
		$arTypeBASE_TYPE_IMG = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_TYPE_IMG');
		$arTypeBASE_TYPE_ACTION = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_TYPE_ACTION');
		$arTypeBASE_PERIOD_ACTION = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_PERIOD_ACTION');
		$arTypeORDER_TYPE_DELIVERY = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_TYPE_DELIVERY');
		$arTypeORDER_COUNT_BLUDS_YSLOVIE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_COUNT_BLUDS_YSLOVIE');
		$arTypeORDER_SOSTAV_BLUDS = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_SOSTAV_BLUDS');
		$arTypeORDER_IN_WHAT = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_IN_WHAT');
		$arTypeORDER_SUMM_ZACAZ_YSLOVIE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_SUMM_ZACAZ_YSLOVIE');
		$arTypeRECOMENDATIONSOC_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'RECOMENDATIONSOC_TYPE');
		$arTypeORDER_TIME_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_TIME_TYPE');
		$arTypeBASE_BONUS_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_BONUS_TYPE');
		$arTypeBASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE');
		$arTypeBASE_BONUS_TYPE_BLUDO_VISIUAL = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_BONUS_TYPE_BLUDO_VISIUAL');
		$arTypeBASE_BONUS_NO_SUM = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_BONUS_NO_SUM');
		$arTypeBASE_BONUS_TYPEACTIVE = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_BONUS_TYPEACTIVE');

		/*
		$arTypeBASE_TYPE_IMG = self::GetArrEnumXmlIdByProp('BASE_TYPE_IMG',$name_IB);
		$arTypeBASE_TYPE_ACTION = self::GetArrEnumXmlIdByProp('BASE_TYPE_ACTION',$name_IB);
		$arTypeBASE_PERIOD_ACTION = self::GetArrEnumXmlIdByProp('BASE_PERIOD_ACTION',$name_IB);
		$arTypeORDER_TYPE_DELIVERY = self::GetArrEnumXmlIdByProp('ORDER_TYPE_DELIVERY',$name_IB);
		$arTypeORDER_COUNT_BLUDS_YSLOVIE = self::GetArrEnumXmlIdByProp('ORDER_COUNT_BLUDS_YSLOVIE',$name_IB);
		$arTypeORDER_SOSTAV_BLUDS = self::GetArrEnumXmlIdByProp('ORDER_SOSTAV_BLUDS',$name_IB);
		$arTypeORDER_IN_WHAT = self::GetArrEnumXmlIdByProp('ORDER_IN_WHAT',$name_IB);
		$arTypeORDER_SUMM_ZACAZ_YSLOVIE = self::GetArrEnumXmlIdByProp('ORDER_SUMM_ZACAZ_YSLOVIE',$name_IB);
		$arTypeRECOMENDATIONSOC_TYPE = self::GetArrEnumXmlIdByProp('RECOMENDATIONSOC_TYPE',$name_IB);
		$arTypeORDER_TIME_TYPE = self::GetArrEnumXmlIdByProp('ORDER_TIME_TYPE',$name_IB);
		$arTypeBASE_BONUS_TYPE = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE',$name_IB);
		$arTypeBASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE',$name_IB);
		$arTypeBASE_BONUS_TYPE_BLUDO_VISIUAL = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_BLUDO_VISIUAL',$name_IB);
		$arTypeBASE_BONUS_NO_SUM = self::GetArrEnumXmlIdByProp('BASE_BONUS_NO_SUM',$name_IB);
		$arTypeBASE_BONUS_TYPEACTIVE = self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPEACTIVE',$name_IB);
		*/

		if(CModule::IncludeModule('iblock')) {

			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC',
				),
				array(
					'IBLOCK_ID' => $iISBlockId,
					'ACTIVE' => 'Y',
					'CHECK_PERMISSIONS' => 'N',
					'PROPERTY_BASE_TYPE_ACTION' =>array($arTypeBASE_TYPE_ACTION_XML_ID[$typeEvent],$arTypeBASE_TYPE_ACTION_XML_ID['ALL']),
				),
				false,
				false,
				array(
					'ID',
					'TIMESTAMP_X',
					'ACTIVE',
					'NAME',
					'PREVIEW_TEXT',
					'DETAIL_TEXT',

					'PROPERTY_BASE_TYPE_IMG',
					'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H',
					'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V',
					'PROPERTY_BASE_TYPE_ACTION', 
					'PROPERTY_BASE_COUNT_ACTION', 
					'PROPERTY_BASE_NUMBER_OF_ACTIONS',
					'PROPERTY_BASE_PERIOD_ACTION',
					'PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY', 
					'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S',
					'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO', 
					'PROPERTY_BASE_TIME_ACTION_S',
					'PROPERTY_BASE_TIME_ACTION_DO',

					'PROPERTY_ORDER_TIME_TYPE',
					'PROPERTY_ORDER_TYPE_DELIVERY',
					'PROPERTY_ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY',
					'PROPERTY_ORDER_TYPE_BLUDS', 
					'PROPERTY_ORDER_INGRIDIENTS_BLUDS',
					'PROPERTY_ORDER_SPEC_MENU_BLUDS',
					'PROPERTY_ORDER_BLUDS',
					'PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE',
					'PROPERTY_ORDER_COUNT_BLUDS',
					'PROPERTY_ORDER_SOSTAV_BLUDS',
					'PROPERTY_ORDER_IN_WHAT',
					'PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE',
					'PROPERTY_ORDER_SUMM_ZACAZ',

					'PROPERTY_RECOMENDATIONSOC_TYPE',

					"PROPERTY_BASE_BONUS_TYPE",
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT",
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE",
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY",
					"PROPERTY_BASE_BONUS_TYPE_BLUDO",
					"PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL",
					"PROPERTY_BASE_BONUS_NO_SUM",
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$bluds=array();
				foreach ($arItem['PROPERTY_BASE_BONUS_TYPE_BLUDO_VALUE'] as $key=>$bludo) {
					$bluds[]=array(
						"VALUE" => $bludo,
						"DESCRIPTION" => $arItem['PROPERTY_BASE_BONUS_TYPE_BLUDO_DESCRIPTION'][$key]
					);
				}

				$arReturn[$arItem['ID']] = array(
					'ID' => $arItem['ID'],
					'ACTIVE' => $arItem['ACTIVE'],
					'TIMESTAMP_X' => $arItem['TIMESTAMP_X'],
					'NAME' => $arItem['NAME'],
					'PREVIEW_TEXT' => $arItem['PREVIEW_TEXT'],
					'DETAIL_TEXT' => $arItem['DETAIL_TEXT'],

					'PROPERTY_BASE_TYPE_IMG' => $arTypeBASE_TYPE_IMG[$arItem['PROPERTY_BASE_TYPE_IMG_VALUE']]['XML_ID'],
					'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H' => $arItem['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H_VALUE'],
					'PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V' => $arItem['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V_VALUE'],
					'PROPERTY_BASE_TYPE_ACTION' => $arTypeBASE_TYPE_ACTION[$arItem['PROPERTY_BASE_TYPE_ACTION_VALUE']]['XML_ID'],
					'PROPERTY_BASE_COUNT_ACTION' => ($arItem['PROPERTY_BASE_COUNT_ACTION_VALUE']>=1)?$arItem['PROPERTY_BASE_COUNT_ACTION_VALUE']:1,
					'PROPERTY_BASE_NUMBER_OF_ACTIONS' => ($arItem['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE']>=1)?$arItem['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE']:1,
					'PROPERTY_BASE_PERIOD_ACTION' => $arTypeBASE_PERIOD_ACTION[$arItem['PROPERTY_BASE_PERIOD_ACTION_VALUE']]['XML_ID'],
					'PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY' => $arItem['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY_VALUE'],
					'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S' => $arItem['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S_VALUE'],
					'PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO' => $arItem['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO_VALUE'],
					'PROPERTY_BASE_TIME_ACTION_S' => $arItem['PROPERTY_BASE_TIME_ACTION_S_VALUE'],
					'PROPERTY_BASE_TIME_ACTION_DO' => $arItem['PROPERTY_BASE_TIME_ACTION_DO_VALUE'],

					'PROPERTY_ORDER_TIME_TYPE' => $arTypeORDER_TIME_TYPE[$arItem['PROPERTY_ORDER_TIME_TYPE_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_TYPE_DELIVERY' => $arTypeORDER_TYPE_DELIVERY[$arItem['PROPERTY_ORDER_TYPE_DELIVERY_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY' => $arItem['PROPERTY_ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY_VALUE'],
					'PROPERTY_ORDER_TYPE_BLUDS' => $arItem['PROPERTY_ORDER_TYPE_BLUDS_VALUE'],
					'PROPERTY_ORDER_INGRIDIENTS_BLUDS' => $arItem['PROPERTY_ORDER_INGRIDIENTS_BLUDS_VALUE'],
					'PROPERTY_ORDER_SPEC_MENU_BLUDS' => $arItem['PROPERTY_ORDER_SPEC_MENU_BLUDS_VALUE'],
					'PROPERTY_ORDER_BLUDS' => $arItem['PROPERTY_ORDER_BLUDS_VALUE'],
					'PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE' => $arTypeORDER_COUNT_BLUDS_YSLOVIE[$arItem['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_COUNT_BLUDS' => $arItem['PROPERTY_ORDER_COUNT_BLUDS_VALUE'],
					'PROPERTY_ORDER_SOSTAV_BLUDS' => $arTypeORDER_SOSTAV_BLUDS[$arItem['PROPERTY_ORDER_SOSTAV_BLUDS_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_IN_WHAT' => $arTypeORDER_IN_WHAT[$arItem['PROPERTY_ORDER_IN_WHAT_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE' => $arTypeORDER_SUMM_ZACAZ_YSLOVIE[$arItem['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE_VALUE']]['XML_ID'],
					'PROPERTY_ORDER_SUMM_ZACAZ' => $arItem['PROPERTY_ORDER_SUMM_ZACAZ_VALUE'],

					'PROPERTY_RECOMENDATIONSOC_TYPE' => $arTypeRECOMENDATIONSOC_TYPE[$arItem['PROPERTY_RECOMENDATIONSOC_TYPE_VALUE']]['XML_ID'],

					"PROPERTY_BASE_BONUS_TYPE" => $arTypeBASE_BONUS_TYPE[$arItem['PROPERTY_BASE_BONUS_TYPE_VALUE']]['XML_ID'],
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT" => $arItem['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT_VALUE'],
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE" => $arTypeBASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE[$arItem['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE_VALUE']]['XML_ID'],
					"PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY" => $arItem['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_VALUE'],
					"PROPERTY_BASE_BONUS_TYPE_BLUDO" => $bluds,					
					"PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL" => $arTypeBASE_BONUS_TYPE_BLUDO_VISIUAL[$arItem['PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL_VALUE']]['XML_ID'],
					"PROPERTY_BASE_BONUS_NO_SUM" => $arTypeBASE_BONUS_NO_SUM[$arItem['PROPERTY_BASE_BONUS_NO_SUM_VALUE']]['XML_ID'],
					"PROPERTY_BASE_BONUS_TYPEACTIVE"=>$arTypeBASE_BONUS_TYPEACTIVE[$arItem['PROPERTY_BASE_BONUS_TYPEACTIVE_VALUE']]['XML_ID'],
				);
			}
		}
		return $arReturn;
	}

	public static function CheckCouponsByTypeEvent($typeEvent, $user_id=0, $dop_event=array()) {
		global $USER;
		if($user_id<=0) {
			if($typeEvent=='ORDER') {
				if(!empty($dop_event['ORDER']['ORDER_ID'])&&$dop_event['ORDER']['ORDER_ID']>0) {
					$order=CNiyamaOrders::GetOrderById($dop_event['ORDER']['ORDER_ID']);
				}
				else{
					return false;
				}
			}
			if($iUserId=$order['ORDER']['USER_ID']) {
				$user_id=$iUserId;
			} elseif($USER->IsAuthorized()) {
				if($iUserId=$USER->GetID()) {
					$user_id=$iUserId;
				}
			}
		}	
		if 	($user_id<=0) {
			return false;
		}
		$list_coupons=self::GetListCouponsByTypeEvent($typeEvent);
		if(!empty($list_coupons)) {
			foreach ($list_coupons as $id_coupon=>$coupon) {
				if(self::CheckCouponPeriod($coupon,$user_id)) {
					if(self::CheckCouponTime($coupon,$dop_event['ORDER']['ORDER_ID'])) {
						$arr_users_coupon=self::GetArUserCoupon($id_coupon,$user_id);
						if(self::CheckCouponTypeAction($coupon,$dop_event,$arr_users_coupon,$typeEvent)) {
							// проверка на количество полученных купонов
							if(self::CheckCouponCount($coupon['PROPERTY_BASE_COUNT_ACTION'], $arr_users_coupon['PROPERTY_BASE_COUNT_ACTION'])) {
								self::AddCouponByEvent($coupon,$arr_users_coupon,$user_id,$typeEvent,$dop_event['ORDER']['ORDER_ID']);
							}
						}
					}
				}
			}
		}
	}

	public static function CheckCouponPeriod($coupon,$user_id) {
		 #Период действия
		$bCheckPeriod = false;
		# если не любой
		if($coupon['PROPERTY_BASE_PERIOD_ACTION'] == "ANY" || (empty($coupon['PROPERTY_BASE_PERIOD_ACTION'])) ) {
			$bCheckPeriod = true;
		} else {
			# за месяц
			$iCurrentTimestump = time();//strtotime($coupon['TIMESTAMP_X']);
			if($coupon['PROPERTY_BASE_PERIOD_ACTION'] == "MONTH") {
				$iMonthEnd = strtotime("now");
				$iCurrneMonthEnd = strtotime("+ 1 months",$iCurrentTimestump);
				if($iMonthEnd <= $iCurrneMonthEnd) {
					$bCheckPeriod = true;
				}
			} else if($coupon['PROPERTY_BASE_PERIOD_ACTION'] == "BIRTHDAY") {
				$dUserBirthday = CUsersData::GetPersonalBIRTHDAY($user_id);
				$iUserBirthday = strtotime($dUserBirthday);
				if(!empty($coupon['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY'])) {
					$befBD = strtotime($coupon['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY']+ " - day",$iUserBirthday);
					$afBD  = strtotime($coupon['PROPERTY_BASE_PERIOD_ACTION_BIRTHDAY_DOPDAY']+ " + day",$iUserBirthday);
					if( ($iCurrentTimestump > $befBD) && ($iCurrentTimestump <= $afBD ) ) {
						$bCheckPeriod = true;
					}
				}
			} else if($coupon['PROPERTY_BASE_PERIOD_ACTION'] == "CUSTOM") {
				$iCS = strtotime($coupon['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_S']);
				$iCDO = strtotime($coupon['PROPERTY_BASE_PERIOD_ACTION_CUSTOM_DO']);
				if( (strtotime("now") >= $iCS) && (strtotime("now") <=  $iCDO  ) ) {
					$bCheckPeriod = true;
				}
			}
		}
		return $bCheckPeriod;
	}

	public static function CheckCouponTime($coupon,$order_id) {
		$bCheckTime = false;
		# если не любой

		if(empty($coupon['PROPERTY_ORDER_TIME_TYPE']) || ($coupon['PROPERTY_ORDER_TIME_TYPE']=='DIAPOZON')) {
			if((empty($coupon['PROPERTY_BASE_TIME_ACTION_S'])) && (empty($coupon['PROPERTY_BASE_TIME_ACTION_DO']))) {
				$bCheckTime = true;
			} else {
				$format_time = '%H:%M:%S';
				$start=(!empty($coupon['PROPERTY_BASE_TIME_ACTION_S']))?$coupon['PROPERTY_BASE_TIME_ACTION_S']:'00:00:00';
				$start_ar=strptime($start, $format_time);
				$start_sec=($start_ar['tm_hour']*60+$start_ar['tm_min'])*60+$start_ar['tm_sec'];
				$end=(!empty($coupon['PROPERTY_BASE_TIME_ACTION_S']))?$coupon['PROPERTY_BASE_TIME_ACTION_DO']:'23:59:59';
				$end_ar=strptime($end, $format_time);
				$end_sec=($end_ar['tm_hour']*60+$end_ar['tm_min'])*60+$end_ar['tm_sec'];
				$cur_time=strftime($format_time);
				$cur_time_ar=strptime($cur_time, $format_time);
				$cur_time_ar_sec=($cur_time_ar['tm_hour']*60+$cur_time_ar['tm_min'])*60+$cur_time_ar['tm_sec'];
				if(($cur_time_ar_sec>=$start_sec) && ($cur_time_ar_sec<=$end_sec)) {
					$bCheckTime = true;
				} else {
					$bCheckTime = false;
				}
			}

		} elseif($coupon['PROPERTY_ORDER_TIME_TYPE']=='COUNT_HOURS_DELIVERY') {
			$order_id = intval($order_id);
			if($order_id > 0 && CModule::IncludeModule('sale')) {
				$arRes=CNiyamaOrders::GetDeliveryTimeByOrderId($order_id);
				if  ($arRes['ORDER_INFO']['_DELIVERY_TYPE_CODE_']=='courier') {
					$coupon_dilevery_time=($coupon['PROPERTY_ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY']>0)?$coupon['PROPERTY_ORDER_TIME_TYPE_COUNT_HOURS_DELIVERY']*3600:1;
					if($arRes['ORDER_INFO']['_BEFORE_DELIVERY_TIME_SEC_']>=$coupon_dilevery_time) {
						$bCheckTime = true;
					}
				}
			}
		}
		return $bCheckTime;
	}

	//$dop_event['RECOMENDATIONSOC']
	//$dop_event['ORDER']['ORDER_ID']
	//$dop_event['ORDER']['BONUS_TYPEACTIVE']='NEXT' (CURRENT по умл.)
	public static function CheckCouponTypeAction($coupon, $dop_event, $arr_users_coupon, $typeEvent) { 
		$returnFlag=false;

		switch ($typeEvent) {
			/*
			case 'ALL':
				$returnFlag=CheckCouponTypeActionALL($coupon, $dop_event);
			break;
			*/
			case 'ORDER':
				$returnFlag=self::CheckCouponTypeActionORDER($coupon, $dop_event['ORDER'],$arr_users_coupon);
			break;

			case 'AUTHSOC':
				$returnFlag=self::CheckCouponTypeActionAUTHSOC($coupon);
			break;

			case 'SURVEY':
				$returnFlag=self::CheckCouponTypeActionSURVEY($coupon);
			break;

			case 'RECOMENDATIONSOC':
				$returnFlag=self::CheckCouponTypeActionRECOMENDATIONSOC($coupon, $dop_event['RECOMENDATIONSOC']);
			break;
		}

		return $returnFlag;
	}

	public static function CheckCouponTypeActionORDER($coupon,$dop_event,$arr_users_coupon) {
		$returnFlag=false;
		if((empty($dop_event['ORDER_ID'])) || ($dop_event['ORDER_ID']<=0)) {
			return false;
		}
		if($coupon['PROPERTY_BASE_BONUS_TYPEACTIVE']=='CURRENT') {
			if($dop_event['BONUS_TYPEACTIVE']=='NEXT') {
				return false;
			}
		} elseif($coupon['PROPERTY_BASE_BONUS_TYPEACTIVE']=='NEXT') {
			if((empty($dop_event['BONUS_TYPEACTIVE']))||($dop_event['BONUS_TYPEACTIVE']!='NEXT')) {
				return false;
			}
		}

		$order=CNiyamaOrders::GetOrderById($dop_event['ORDER_ID']);

		$price=(int)$order['ORDER']['PRICE'];

		if($coupon['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE']=='RAVNA') {
			if($price!=$coupon['PROPERTY_ORDER_SUMM_ZACAZ']) {
				return false;
			}
		}elseif($coupon['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE']=='MIN') {
			if($price<$coupon['PROPERTY_ORDER_SUMM_ZACAZ']) {
				return false;
			}
		}elseif($coupon['PROPERTY_ORDER_SUMM_ZACAZ_YSLOVIE']=='MAX') {
			if($price>$coupon['PROPERTY_ORDER_SUMM_ZACAZ']) {
				return false;
			}
		}
		$DELIVERY_TYPE='';
		foreach ($order['ORDER_PROPS']['DELIVERY_TYPE'] as $el) {
			if($el['CODE'] == 'DELIVERY_TYPE') {
				$DELIVERY_TYPE=$el['VALUE'];
			}
		}

		if($coupon['PROPERTY_ORDER_TYPE_DELIVERY']=='SAMOVYVOZ') {
			if($DELIVERY_TYPE!='self') {
				return false;
			}
		} elseif($coupon['PROPERTY_ORDER_TYPE_DELIVERY']=='CURIER') {
			if($DELIVERY_TYPE!='courier') {
				return false;
			}
		}

		$bluds_coupon=array();
		if(!empty($coupon['PROPERTY_ORDER_BLUDS']) && (is_array($coupon['PROPERTY_ORDER_BLUDS']))) {	
			foreach ($coupon['PROPERTY_ORDER_BLUDS'] as $el)
			$bluds_coupon[$el]=$el;
		} else {
			$bluds_coupon=self::GetProductByFasetFilter($coupon['PROPERTY_ORDER_TYPE_BLUDS'],$coupon['PROPERTY_ORDER_SPEC_MENU_BLUDS'],$coupon['PROPERTY_ORDER_INGRIDIENTS_BLUDS']);
		}

		$bluds=self::GetProductByArrOrderAndFilter($order,$bluds_coupon);
		$count_bluds=0;
		if(empty($coupon['PROPERTY_ORDER_SOSTAV_BLUDS']) || ($coupon['PROPERTY_ORDER_SOSTAV_BLUDS']=='ANY')) {
			foreach ($bluds as $quanity) {
				$count_bluds+=$quanity;
			}
		} elseif($coupon['PROPERTY_ORDER_SOSTAV_BLUDS']=='DIFFERENT') {
			$count_bluds=sizeof($bluds);
		} elseif($coupon['PROPERTY_ORDER_SOSTAV_BLUDS']=='SAME') {
			$count_bluds=max($bluds);
		}

		CModule::IncludeModule('iblock');
		$count_bluds_coupon=(!empty($coupon['PROPERTY_ORDER_COUNT_BLUDS']))?$coupon['PROPERTY_ORDER_COUNT_BLUDS']:0;
		if(empty($coupon['PROPERTY_ORDER_IN_WHAT']) || ($coupon['PROPERTY_ORDER_IN_WHAT']=='ANY')) {
			if(empty($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']) || ($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='BOLSHE')) {
				if($count_bluds+$arr_users_coupon['PROPERTY_COUNT_ORDER_BLUDS']>=$count_bluds_coupon) {
					//сбрасываем
					if($arr_users_coupon['ID']!==false) {
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "COUNT_ORDER_BLUDS", 0);
					}
					return true;
				} else {
					return false;
				}
			} elseif($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='RAVNO') {
				if($count_bluds+$arr_users_coupon['PROPERTY_COUNT_ORDER_BLUDS']==$count_bluds_coupon) {
					// сбрасываем
					if($arr_users_coupon['ID']!==false) {
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "COUNT_ORDER_BLUDS", 0);
					}
					return true;
				} else {
					if($count_bluds+$arr_users_coupon['PROPERTY_COUNT_ORDER_BLUDS']>$count_bluds_coupon) {
						// сбрасываем
						if($arr_users_coupon['ID']!==false) {
							CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "COUNT_ORDER_BLUDS", 0);
						}
					}
					return false;
				}
			}
		} elseif($coupon['PROPERTY_ORDER_IN_WHAT']=='ONE') {
			if(empty($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']) || ($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='BOLSHE')) {
				if($count_bluds>=$count_bluds_coupon) {
					return true;
				} else {
					return false;
				}
			} elseif($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='RAVNO') {
				if($count_bluds==$count_bluds_coupon) {
					return true;
				} else {
					return false;
				}
			}
		} elseif($coupon['PROPERTY_ORDER_IN_WHAT']=='SUCCESSION') {
			if(empty($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']) || ($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='BOLSHE')) {
				if($count_bluds>=$count_bluds_coupon) {
					return true;
				} else {
					//обнуляем
					if($arr_users_coupon['ID']!==false) {
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "BASE_NUMBER_OF_ACTIONS_ORDER", 0);
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "BASE_NUMBER_OF_ACTIONS", 0);
					}
					return false;
				}
			} elseif($coupon['PROPERTY_ORDER_COUNT_BLUDS_YSLOVIE']=='RAVNO') {
				if($count_bluds==$count_bluds_coupon) {
					return true;
				} else {
					// обнуляем
					if($arr_users_coupon['ID']!==false) {
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "BASE_NUMBER_OF_ACTIONS_ORDER", 0);
						CIBlockElement::SetPropertyValueCode($arr_users_coupon['ID'], "BASE_NUMBER_OF_ACTIONS", 0);
					}
					return false;
				}
			}
		}
		return true;
	}

	public static function GetProductByFasetFilter($type=array(),$spec=array(),$ing=array()) {
		$arReturn=array();
		# Фильтр
		$arPFilter = array();
		
		if((!empty($type))||(!empty($spec))||(!empty($ing))) {
			# Меню
			if(!empty($type) && (is_array($type))) {
				foreach($type as $key=>$el) {
					if(empty($el)) {
						unset($type[$key]);
					}
				}
				$arPFilter['SECTION_ID'] = $type;
				$arPFilter['INCLUDE_SUBSECTIONS']  = "Y";
			}
			# Специальное меню
			if(!empty($spec) && (is_array($spec))) {
				foreach($spec as $key=>$el) {
					if(empty($el)) {
						unset($spec[$key]);
					}
				}
				$arPFilter['PROPERTY_SPEC_MENU.ID'] = $spec;
			}
			CModule::IncludeModule('iblock');

			# Ингредиенты
			if(!empty($ing) && (is_array($ing))) {
				foreach($ing as $key=>$el) {
					if(empty($el)) {
						unset($ing[$key]);
					}
				}
				$arPFilter[] = array(
					"LOGIC" => "OR",
					array("?PROPERTY_GENERAL_INGREDIENT" => implode(" && ",$ing)),
					array("?PROPERTY_MORE_INGREDIENT"	=> implode(" && ",$ing))
				);
			}
		}

		$arMenuItems = CNiyamaCatalog::GetList(null,$arPFilter,null,false);

		foreach($arMenuItems['ITEMS'] as $el) {
			$arReturn[$el['INFO']['ID']]=$el['INFO']['ID'];
		}
		return $arReturn;
	}

	public static function GetProductByArrOrderAndFilter($order, $filter) {
		$bluds = array();

		if($order['ORDER_CART']['current'][0]['ITEMS'] && is_array($order['ORDER_CART']['current'][0]['ITEMS'])) {
			foreach($order['ORDER_CART']['current'][0]['ITEMS'] as $prod) {
				if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[0]) {
					if($prod['QUANTITY'] >= 1 && isset($filter[$prod['PRODUCT_ID']])) {
						$bluds[$prod['PRODUCT_ID']] += $prod['QUANTITY'];
					}
				}
			}
		}

		if($order['ORDER_CART']['guest'] && is_array($order['ORDER_CART']['guest'])) {
			foreach($order['ORDER_CART']['guest'] as $guest_id => $guest) {
				foreach($guest['ITEMS'] as $prod) {
					if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[0]) {
						if($prod['QUANTITY'] >= 1 && isset($filter[$prod['PRODUCT_ID']])) {
							$bluds[$prod['PRODUCT_ID']] += $prod['QUANTITY'];
						}
					}
				}
			}
		}

		if($order['ORDER_CART']['common'][0]['ITEMS'] && is_array($order['ORDER_CART']['common'][0]['ITEMS'])) {
			foreach($order['ORDER_CART']['common'][0]['ITEMS'] as $prod) {
				if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[0]) {
					if($prod['QUANTITY'] >= 1 && isset($filter[$prod['PRODUCT_ID']])) {
						$bluds[$prod['PRODUCT_ID']] += $prod['QUANTITY'];
					}
				}
			}
		}

		return 	$bluds;	
	}

	public static function CheckCouponTypeActionAUTHSOC($coupon) {
		return true;
	}

	public static function CheckCouponTypeActionSURVEY($coupon) {
		return true;
	}

	public static function CheckCouponTypeActionRECOMENDATIONSOC($coupon, $dop_event) {
		//$dop_event "TWEET_YROVEN_OR_MEDAL" or "RECOMENDATON_FROM_VIDZHET" array('RECOMENDATIONSOC'=>TWEET_YROVEN_OR_MEDAL)
		if((empty($coupon['PROPERTY_RECOMENDATIONSOC_TYPE']))||($coupon['PROPERTY_RECOMENDATIONSOC_TYPE'] == "ANY")) {
			# Если любая рекомендация
			return true;
		} else if($coupon['PROPERTY_RECOMENDATIONSOC_TYPE'] == $dop_event) {
			return true;
		} else {
			return false;
		}
	}

	public static function CheckCouponCount($max_count,$cur_count) {
		if($cur_count>=$max_count) {
			return false;
		} else {
			return true;
		}
	}

	public static function AddCouponByEvent($coupon,$arr_users_coupon, $user_id,$typeEvent,$order_id) {
		$count_number_of_action=$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_'.$typeEvent]+1;//кол-во выполненных действий
		if($coupon['PROPERTY_BASE_TYPE_ACTION']=='ALL') {
			$count_number_of_action_arr=array(
				'ORDER'=>$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'],
				'AUTHSOC'=>$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'],
				'SURVEY'=>$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'],
				'RECOMENDATIONSOC'=>$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC']
			);
			$count_number_of_action_arr[$typeEvent]	=$count_number_of_action;
			$count_number_of_action_min=min($count_number_of_action_arr);
			$quanity_add_curr=floor($count_number_of_action_min/$coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']);//кол-во новых купонов к получению
			$quanity_add=$quanity_add_curr+$arr_users_coupon['PROPERTY_BASE_QUANITY']; //новое количество купонов
		} else {
			$quanity_add_curr=floor($count_number_of_action/$coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']);//кол-во новых купонов к получению
			$quanity_add=$quanity_add_curr+$arr_users_coupon['PROPERTY_BASE_QUANITY']; //новое количество купонов
		}
		if(($arr_users_coupon['PROPERTY_BASE_COUNT_ACTION']+$quanity_add_curr)>$coupon['PROPERTY_BASE_COUNT_ACTION']) {//если кол-во полученных купонов + новое количество купонов > возможного количества 
			$quanity_add_curr=$coupon['PROPERTY_BASE_COUNT_ACTION']-$arr_users_coupon['PROPERTY_BASE_COUNT_ACTION']; 
			$quanity_add=$quanity_add_curr+$arr_users_coupon['PROPERTY_BASE_QUANITY'];
			//$quanity_add=$coupon['PROPERTY_BASE_COUNT_ACTION']-$arr_users_coupon['PROPERTY_BASE_COUNT_ACTION'];
			//$quanity_add_curr=$quanity_add-$arr_users_coupon['PROPERTY_BASE_COUNT_ACTION']; 
		}
		if($quanity_add_curr>0) {
			$count_number_of_action_add=$count_number_of_action%$coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS'];//новое кол-во действий	
		} else {
			$count_number_of_action_add=$count_number_of_action;//новое кол-во действий
		}

		$count_action_add=$arr_users_coupon['PROPERTY_BASE_COUNT_ACTION']+$quanity_add_curr;//новое кол-во полученных купонов

		$name_IB=self::GetIbByType(self::$arTypeIB[0]);
		$addCoupon=array();
		$user_fields = array();
		$type_bonus=self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE',$name_IB);
		$type_img=self::GetArrEnumXmlIdByProp('BASE_TYPE_IMG',$name_IB);
		$type_money=self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE',$name_IB);
		$type_visual=self::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_BLUDO_VISIUAL',$name_IB);
		$sum_discount=self::GetArrEnumXmlIdByProp('BASE_BONUS_NO_SUM',$name_IB);
		if($quanity_add_curr>0) {
			if($coupon['PROPERTY_BASE_TYPE_ACTION']=='ALL') {
				$ORDER=$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER']-($coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']*$quanity_add_curr);
				$AUTHSOC=$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC']-($coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']*$quanity_add_curr);
				$SURVEY=$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY']-($coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']*$quanity_add_curr);
				$RECOMENDATIONSOC=$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC']-($coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS']*$quanity_add_curr);
				$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER']=($ORDER>0)?$ORDER:0;
				$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC']=($AUTHSOC>0)?$AUTHSOC:0;
				$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY']=($SURVEY>0)?$SURVEY:0;
				$arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC']=($RECOMENDATIONSOC>0)?$RECOMENDATIONSOC:0;
			}
		}
		$defCoupon=array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB),
			'ACTIVE' => 'Y',
			"NAME" => $coupon["NAME"],
			"PREVIEW_TEXT" => $coupon["PREVIEW_TEXT"],
			"DETAIL_TEXT" => $coupon["DETAIL_TEXT"],
		);
		if($quanity_add_curr>0) {
			if(($coupon['PROPERTY_BASE_TYPE_ACTION']=='ALL')||(($coupon['PROPERTY_BASE_TYPE_ACTION']=='ORDER'))) {
				$defCoupon['ACTIVE']='N';
			}
		}

		$defProp = array(
			"COUPON_ID"=> $coupon["ID"],
			'USER_ID' => $user_id,
			'ORDER_ID' => $order_id > 0 ? $order_id : 0,
			'BASE_TYPE_IMG' => array("VALUE" => $type_img[$coupon['PROPERTY_BASE_TYPE_IMG']]),
			'BASE_BONUS_TYPE' => array("VALUE" => $type_bonus[$coupon['PROPERTY_BASE_BONUS_TYPE']]),
			'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE' => array("VALUE" => $type_money[$coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE']]),
			'BASE_BONUS_TYPE_BLUDO_VISIUAL' => array("VALUE" => $type_visual[$coupon['PROPERTY_BASE_BONUS_TYPE_BLUDO_VISIUAL']]),
			'BASE_BONUS_NO_SUM' => array("VALUE" => $sum_discount[$coupon['PROPERTY_BASE_BONUS_NO_SUM']]),

			'BASE_TYPE_IMG_DOWNLOAD_H' => $coupon['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_H'],
			'BASE_TYPE_IMG_DOWNLOAD_V' => $coupon['PROPERTY_BASE_TYPE_IMG_DOWNLOAD_V'],
			'BASE_BONUS_TYPE_DISCOUNT_PERSENT' => $coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_PERSENT'],
			'BASE_BONUS_TYPE_DISCOUNT_MONEY' => $coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY'],
			'BASE_BONUS_TYPE_BLUDO' => $coupon['PROPERTY_BASE_BONUS_TYPE_BLUDO'],

			'BASE_COUNT_ACTION' => $count_action_add,
			'BASE_NUMBER_OF_ACTIONS' => (!empty($count_number_of_action_add))?$count_number_of_action_add:0,
			'BASE_QUANITY' => $quanity_add,
			'BASE_NUMBER_OF_ACTIONS_ORDER' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'],
			'BASE_NUMBER_OF_ACTIONS_AUTHSOC' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'],
			'BASE_NUMBER_OF_ACTIONS_SURVEY' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'],
			'BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'],
		);
		$addCoupon=$defCoupon;
		$addCoupon['PROPERTY_VALUES']=$defProp;

		$addCoupon['PROPERTY_VALUES']['BASE_NUMBER_OF_ACTIONS_'.$typeEvent]=(!empty($count_number_of_action_add))?$count_number_of_action_add:0;

		if($quanity_add_curr>0) {
			if(($coupon['PROPERTY_BASE_TYPE_ACTION']!='ALL')&&(($coupon['PROPERTY_BASE_TYPE_ACTION']!='ORDER'))) {
				if($coupon['PROPERTY_BASE_BONUS_TYPE']=='DISCOUNT_MONEY') {
					if($coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE']=='NIYAM') {
						$val_coupon=self::GetCouponValByType(self::$BONUS_NIYAM,$user_id);
						$user_fields['UF_'.self::$BONUS_NIYAM]=$val_coupon+(int)$coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY']*$quanity_add_curr;
					} else {
						$val_coupon=self::GetCouponValByType(self::$BONUS_RUB,$user_id);
						$user_fields['UF_'.self::$BONUS_RUB]=$val_coupon+(int)$coupon['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY']*$quanity_add_curr;
					}
				}
			}
		}

		if(!empty($addCoupon)) {
			CModule::IncludeModule('iblock');
			$obIBlockElement = new CIBlockElement();
			if(($arr_users_coupon['ID']!==false)&&($arr_users_coupon['ID']>0)) {
				$result=$obIBlockElement->Update($arr_users_coupon['ID'],$addCoupon);
			} else {
				$result=$obIBlockElement->Add($addCoupon);
			}
		}
		if(!empty($user_fields)) {
			$user = new CUser;
			$user->Update($user_id, $user_fields);
		}
	}

	public static function GetArUserCoupon($id_coupon,$user_id) {
		CModule::IncludeModule('iblock');
		$result=array(
			'ID' => false,
			'user_id'=>$user_id,
			'PROPERTY_BASE_COUNT_ACTION' => 0, //кол-во полученных купонов
			'PROPERTY_BASE_NUMBER_OF_ACTIONS' => 0, //кол-во действий совершенных
			'PROPERTY_BASE_QUANITY' => 0, //кол-во купонов неиспользованных		
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => 0,
			'PROPERTY_COUNT_ORDER_BLUDS' => 0,
		);
		$arFilter = array(
			"IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
			'PROPERTY_USER_ID'=>$user_id,
			'PROPERTY_COUPON_ID'=>$id_coupon,
		);
		$res = CIBlockElement::GetList(array("active_from"=>"DESC"), $arFilter, false,false, array('ID','PROPERTY_BASE_COUNT_ACTION','PROPERTY_BASE_NUMBER_OF_ACTIONS','PROPERTY_BASE_QUANITY','PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER', 'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC','PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY','PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC','PROPERTY_COUNT_ORDER_BLUDS'));
		if($ar_fields = $res->GetNext()) {
			$result=array(
				'ID' => $ar_fields['ID'],
				'user_id'=>$user_id,
				'PROPERTY_BASE_COUNT_ACTION' => $ar_fields['PROPERTY_BASE_COUNT_ACTION_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE'],
				'PROPERTY_BASE_QUANITY' => $ar_fields['PROPERTY_BASE_QUANITY_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC_VALUE'],
				'PROPERTY_COUNT_ORDER_BLUDS' => $ar_fields['PROPERTY_COUNT_ORDER_BLUDS_VALUE'],
			);
		}
		return $result;
	}

	public static function UseCouponsByOrder($iOrderId) {
		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			$order = CNiyamaOrders::GetOrderById($iOrderId);
			$cartList = $order['ORDER_CART'];
			$user_id = $order['ORDER']['USER_ID'];
			self::DeActivateCuponsByList($cartList['common'][0]['ITEMS']);
			self::DeActivateCuponsByList($cartList['current'][0]['ITEMS']);
			if(!empty($cartList['guest'])) {
				foreach($cartList['guest'] as $guest_id=>$guet_ar) {
					self::DeActivateCuponsByList($cartList['guest'][$guest_id]['ITEMS']);
				}
			}
			$user_fields=array();
			if($cartList['DISCOUNT_RUB']['niam']>0) {
				$val_coupon=self::GetCouponValByType(self::$BONUS_NIYAM,$user_id);
				$new_val_coupon=$val_coupon-(int)$cartList['DISCOUNT_RUB']['niam'];
				if($new_val_coupon<0) {
					$new_val_coupon=0;
				}
				$user_fields['UF_'.self::$BONUS_NIYAM]=$new_val_coupon;
			}
			if($cartList['DISCOUNT_RUB']['rub']>0) {
				$val_coupon=self::GetCouponValByType(self::$BONUS_RUB,$user_id);
				$new_val_coupon=$val_coupon-(int)$cartList['DISCOUNT_RUB']['rub'];
				if($new_val_coupon<0) {
					$new_val_coupon=0;	
				}
				$user_fields['UF_'.self::$BONUS_RUB]=$new_val_coupon;
			}
			if(!empty($user_fields)) {
				$user = new CUser;
				$user->Update($user_id, $user_fields);
			}
			
		}
	}

	public static function DeActivateCuponsByList($list) {
		if(!empty($list)) {
			foreach ($list as $prod) {
				if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[1]) {
					if(($prod['PROPS']['type_bonus'] !='DISCOUNT_MONEY') &&($prod['PROPS']['type_bonus'] !='CONST_DISCOUNT')) {
						self::DecCouponQuanity($prod['PRODUCT_ID']);
					}
				}
			}
		}
		
	}

	public static function DecCouponQuanity($iCouponId) {
		$VALUE=0;
		$res = CIBlockElement::GetProperty(CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'), $iCouponId, "sort", "asc", array("CODE" => "BASE_QUANITY"));
		if($ob = $res->GetNext()) {
			$VALUE = $ob['VALUE'];
		}
		if($VALUE>=1) {
			$VALUE=$VALUE-1;
		}
		CIBlockElement::SetPropertyValueCode($iCouponId, "BASE_QUANITY", $VALUE);
	}

	public static function ActivateNewCouponsByOrder($iOrderId) {
		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			$order=CNiyamaOrders::GetOrderById($iOrderId);
			$user_id=$order['ORDER']['USER_ID'];
			$order_id=$order['ORDER']['ID'];
			if(CModule::IncludeModule('iblock')) {
				if((!empty($user_id))&&($user_id>0)&&(!empty($order_id))&&($order_id>0)) {
					$arFilter = array(
						"IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
						'PROPERTY_USER_ID'=>$user_id,
						'PROPERTY_ORDER_ID'=>$iOrderId,
						"ACTIVE" => "N"
					);
					$name_IB=self::GetIbByType(self::$arTypeIB[0]);
					$type_bonus=self::GetArrEnumByProp('BASE_BONUS_TYPE',$name_IB);
					$type_money=self::GetArrEnumByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE',$name_IB);

					$user_fields=array();
					$el = new CIBlockElement;
					$res = CIBlockElement::GetList(array(), $arFilter, false,false, array('ID', 'PROPERTY_BASE_BONUS_TYPE','PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE','PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY'));
					while($ar_fields = $res->GetNext()) {
						if($type_bonus[$ar_fields['PROPERTY_BASE_BONUS_TYPE_ENUM_ID']]=='DISCOUNT_MONEY') {
							if($type_money[$ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE_ENUM_ID']]=='NIYAM') {
								$val_coupon=self::GetCouponValByType(self::$BONUS_NIYAM,$user_id);
								$user_fields['UF_'.self::$BONUS_NIYAM]=$val_coupon+$ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_VALUE'];
							} elseif($type_money[$ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE_ENUM_ID']]=='MONEY') {
								$val_coupon=self::GetCouponValByType(self::$BONUS_RUB,$user_id);
								$user_fields['UF_'.self::$BONUS_RUB]=$val_coupon+$ar_fields['PROPERTY_BASE_BONUS_TYPE_DISCOUNT_MONEY_VALUE'];
							}
						}
						$el->Update($ar_fields['ID'], array("ACTIVE" => "Y"));
					}
					if(!empty($user_fields)) {
						$user = new CUser;
						$user->Update($user_id, $user_fields);
					}
				}
			}
		}
	}

	public static function DeleteCouponsByOrderByCancel($iOrderId) {
		$iOrderId = intval($iOrderId);
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			$order=CNiyamaOrders::GetOrderById($iOrderId);
			$user_id=$order['ORDER']['USER_ID'];
			$order_id=$order['ORDER']['ID'];
			if(CModule::IncludeModule('iblock')) {
				if((!empty($user_id))&&($user_id>0)&&(!empty($order_id))&&($order_id>0)) {
					$arFilter = array(
						"IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_CUPONS_USERS'),
						'PROPERTY_USER_ID'=>$user_id,
						'PROPERTY_ORDER_ID'=>$iOrderId,
						"ACTIVE" => "N"
					);
					$el = new CIBlockElement;
					$res = CIBlockElement::GetList(array(), $arFilter, false,false, array('ID', 'IBLOCK_ID', 'PROPERTY_BASE_COUNT_ACTION','PROPERTY_BASE_QUANITY'));
					while($ar_fields = $res->GetNext()) {
						$COUNT_ACTION=($ar_fields['PROPERTY_BASE_COUNT_ACTION_VALUE']>0)?$ar_fields['PROPERTY_BASE_COUNT_ACTION_VALUE']-1:0;
						$QUANITY=($ar_fields['PROPERTY_BASE_QUANITY_VALUE']>0)?$ar_fields['PROPERTY_BASE_QUANITY_VALUE']-1:0;
						$el->SetPropertyValues($ar_fields['ID'], $ar_fields['IBLOCK_ID'], array('BASE_COUNT_ACTION'=>$COUNT_ACTION,'BASE_QUANITY'=>$QUANITY,'ORDER_ID'=>''));
						$el->Update($ar_fields['ID'], array("ACTIVE" => "Y"));
					}
				}
			}
		}
	}
}

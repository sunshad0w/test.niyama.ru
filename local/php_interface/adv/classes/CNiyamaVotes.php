<?
/**
 * 
 * Опросы
 * @author t.k, 2014
 *
 */


class CNiyamaVotes {
	//
	// Возвращает ID активного опроса из заданной группы,
	// в котором текущий пользователь еще не принимал участия (вернее, попадает в ограничения опроса)
	//
	public static function GetActiveBySID($iSID) {
		if (CModule::IncludeModule("vote")) {
			$rsChannel = CVoteChannel::GetList($by, $order,	array(
				'SID' => $iSID,
				'SID_EXACT_MATCH' => 'Y',
				'SITE' => SITE_ID,
				'ACTIVE' => 'Y',
				'HIDDEN' => 'N',
			), $is_filtered);
			if ($rsChannel && ($arChannel = $rsChannel->Fetch())) {
				$voteUserID = ($_SESSION["VOTE_USER_ID"] ? $_SESSION["VOTE_USER_ID"] : intval($GLOBALS["APPLICATION"]->get_cookie("VOTE_USER_ID")));
				
				$rsVotes = CVote::GetList($by = 's_c_sort', $order = 'asc', array(
					'CHANNEL_ID' => $arChannel['ID'],
					'LAMP' => 'green',
				), $is_filtered);
				
				while ($arVote = $rsVotes->Fetch()) {
					$bVoted = CVote::UserAlreadyVote($arVote['ID'], $voteUserID, $arVote['UNIQUE_TYPE'], $arVote['KEEP_IP_SEC'], $GLOBALS["USER"]->GetID());
					if (!$bVoted)
						return $arVote['ID'];
				}
			}
		}
		return 0;
	}
}

<?
/**
 *
 * Обработчики агентов
 *
 */
use \Bitrix\Main\Entity as Entity;

/**
 * Class CAgentHandlers
 */
class CAgentHandlers {

	/**
	 * Актуализация каталога (меню блюд)
	 * @return string
	 */
	public static function UpdateCatalogBase() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}
		if(!CNiyamaImportUtils::IsFlagLocked('niyama_imp_catalog_base')) {
			CNiyamaImportUtils::SetLockingFlag('niyama_imp_catalog_base', 10800);
			CNiyamaIBlockCatalogBase::ImportData();
			CNiyamaImportUtils::FlushLockingFlag('niyama_imp_catalog_base');
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * Доотправка заказов в FO, которые не удалось отправить ранее по каким-либо причинам
	 */
	public static function ExportFailOrders2FO() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		if(CModule::IncludeModule('sale')) {
			$arFilter = array(
				'STATUS_ID' => 'E',
				'CANCELED' => 'N',
				// проверяем заказы в течение суток
				'>=DATE_INSERT' => date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')), (time() - 86400)),
				'UF_FO_WAS_SEND' => 1
			);
			$dbItems = CSaleOrder::GetList(
				array(
					'DATE_UPDATE' => 'asc'
				),
				$arFilter,
				false,
				array(
					'nTopCount' => 10
				),
				array(
					'ID', 'DATE_UPDATE'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$bResult = CNiyamaOrders::ExportOrder2FO($arItem['ID']);
				if(!$bResult) {
					CSaleOrder::Update(
						$arItem['ID'], 
						array(
							'UF_FO_WAS_SEND' => 1,
						)
					);
				}
			}
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * Актуализация заказов данными, принимаемых из FO
	 */
	public static function UpdateOrderStatuses() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		if(CModule::IncludeModule('sale')) {
			$obFO = new CNiyamaFastOperator();

			$arFilter = array(
				'STATUS_ID' => array(
					'R', // Принят на стороне FO
					'N', // Принят
					'S', // Отложен
					'T', // Доставляется
				),
				'CANCELED' => 'N',
				// проверяем заказы в течение двух недель
				'>=DATE_INSERT' => date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')), (time() - (14 * 86400))),
				'UF_FO_WAS_SEND' => 1,
				'UF_RECEIVED_FROM_FO' => 0,
			);
			$dbItems = CSaleOrder::GetList(
				array(
					'DATE_UPDATE' => 'asc'
				),
				$arFilter,
				false,
				array(
					'nTopCount' => 20
				),
				array(
					'ID', 'DATE_UPDATE', 'UF_FO_ORDER_CODE', 'UF_FO_USER_LOGIN'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$bCancel = false;
				$sCancelDescription = '';
				if($arItem['UF_FO_USER_LOGIN'] && $arItem['UF_FO_ORDER_CODE']) {
					$arFastOperatorOrders = $obFO->GetOrdersArrayNew($arItem['UF_FO_USER_LOGIN'], $arItem['UF_FO_ORDER_CODE']);
					if($arFastOperatorOrders['Orders'] && is_array($arFastOperatorOrders['Orders'])) {
						foreach($arFastOperatorOrders['Orders'] as $arFastOperatorOrder) {
							CNiyamaOrders::UpdateOrderByImportedData($arItem['ID'], $arFastOperatorOrder);
						}
					}
				} else {
					$bCancel = true;
					$sCancelDescription = 'Невозможно получить статус заказа';
				}
				if($bCancel) {
					CSaleOrder::CancelOrder($arItem['ID'], 'Y', $sCancelDescription);
				}
			}
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * импорт заказов пользователей из FO
	 */
	public static function ImportUsersOrders() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		@set_time_limit(3600);

		$dbItems = CUser::GetList(
			$sBy = 'UF_ORDERS_IMP_DATE',
			$sOrder = 'ASC',
			array(
				'ACTIVE' => 'Y',
				'LAST_LOGIN_1' => date('d.m.Y', (time() - (86400 * 30))) // выбираем только тех юзеров, которые заходили на сайт хотя бы месяц назад
			),
			array(
				'NAV_PARAMS' => array(
					'nTopCount' => 20
				),
				'FIELDS' => array('ID', 'LOGIN', 'EMAIL')
			)
		);
		while($arItem = $dbItems->Fetch()) {
			CNiyamaOrders::ImportUserOrdersFO($arItem['EMAIL']);
			$obUser = new CUser();
			$obUser->Update($arItem['ID'], array('UF_ORDERS_IMP_DATE' => date('d.m.Y H:i:s'), '_SKIP_FO_SYNC_' => 'Y'));
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	public static function SyncUserWithFO() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		$dbItems = CUser::GetList(
			$sBy = 'ID',
			$sOrder = 'ASC',
			array(
				'ACTIVE' => 'Y',
				'UF_MARKER' => 'E'
			),
			array(
				'SELECT' => array('UF_*')
			)
		);
		while($arItem = $dbItems->Fetch()) {
			CUsersData::SyncUserById(false, $arItem);
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * CalculateMedals
	 * @return string
	 */
	public static function CalculateMedals() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		CNiyamaMedals::CalculateAgent();

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * очистка устаревших записей идентификации дисконтных карт
	 */
	public static function ClearCardsIdentify() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		// удаляем все записи, которые последний раз изменялись больше недели назад
		// и находятся не в ручном режиме идентификации
		$sTmpHLEntityClass = CNiyamaDiscountCard::GetCardsIdentifyEntityClass();
		$arTmpGetListParams = array();
		$arTmpGetListParams['order'] = array(
			'ID' => 'ASC'
		);
		$arTmpGetListParams['filter'] = array(
			'<UF_TIMESTAMP_X' => date('d.m.Y', (time() - (86400 * 7))),
			'UF_MANUAL_CHECK' => 0
		);
		$dbItems = $sTmpHLEntityClass::GetList($arTmpGetListParams);
		while($arItem = $dbItems->Fetch()) {
			$sTmpHLEntityClass::Delete($arItem['ID']);
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

	/**
	 * активация записей для новых зарегистрированных карт и удаление старых записей
	 */
	public static function CheckCardsActivationQueue() {
file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' '.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		$bUnsetUser = false;
		if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
			$GLOBALS['USER'] = new CUser();
			$bUnsetUser = true;
		}

		if(CModule::IncludeModule('iblock')) {
			// проверяем записи за последние два дня
			$sFilterDate = date('d.m.Y', (time() - (86400 * 2)));
			$dbItems = CIBlockElement::GetList(
				array(
					'TIMESTAMP_X' => 'ASC'
				),
				array(
					'IBLOCK_ID' => CNiyamaIBlockCardData::GetIBlockId(),
					'ACTIVE' => 'N',
					'=CODE' => 'check_queue',
					'!PROPERTY_CARDNUM' => false,
					'>=DATE_ACTIVE_FROM' => $sFilterDate
				),
				false,
				array(
					'nTopCount' => '20'
				),
				array(
					'ID', 'NAME', 'PROPERTY_CARDNUM'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$bActivate = false;
				if(strlen(trim($arItem['PROPERTY_CARDNUM_VALUE']))) {
					$arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo(trim($arItem['PROPERTY_CARDNUM_VALUE']));
					if($arPdsCardInfo['Account']) {
						$bActivate = true;
					}
				}

				$obIBlockelement = new CIBlockElement();
				if(!$bActivate) {
					// чтобы обновить timestamp_x
					$obIBlockelement->Update($arItem['ID'], array('NAME' => $arItem['NAME']));
				} else {
					$obIBlockelement->Update($arItem['ID'], array('ACTIVE' => 'Y', 'CODE' => ''));
				}
			}
			// удаляем устаревшие записи
			$dbItems = CIBlockElement::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'IBLOCK_ID' => CNiyamaIBlockCardData::GetIBlockId(),
					'ACTIVE' => 'N',
					'=CODE' => 'check_queue',
					'<DATE_ACTIVE_FROM' => $sFilterDate
				),
				false,
				array(
					'nTopCount' => '20'
				),
				array(
					'ID'
				)
			);
			while($arItem = $dbItems->Fetch()) {
				$obIBlockelement = new CIBlockElement();
				$obIBlockelement->Delete($arItem['ID']);
			}
		}

		if($bUnsetUser) {
			unset($GLOBALS['USER']);
		}

file_put_contents('/var/log/agents.log', date('Y-m-d H:i:s').' /'.__METHOD__.' '.posix_getpid()."\n", FILE_APPEND);
		return __METHOD__.'();';
	}

}
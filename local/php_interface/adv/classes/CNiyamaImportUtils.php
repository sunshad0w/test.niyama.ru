<?
/**
 * Класс утилит иморта данных
 *
 * @author Sergey Leshchenko, 2014
 * @updated: 26.03.2015
 */

class CNiyamaImportUtils {
	//
	// Возвращает список файлов и директорий, находящихся в заданной директории
	//
	public static function GetDirItems($sDir) {
		$arReturn = is_dir($sDir) ? scandir($sDir) : array();
		if($arReturn) {
			if($arReturn[0] == '.') {
				array_shift($arReturn); // удаляем из массива '.'
			}
			if($arReturn[0] == '..') {
				array_shift($arReturn); // удаляем из массива '..'
			}
		}
		return $arReturn;
	}

	//
	// Преобразует строковое значение числа
	//
	public static function Str2Double($sVal) {
		$dReturn = str_replace(',', '.', $sVal);
		$dReturn = preg_replace('/[^\d\-\.]/i', '', $dReturn);
		return doubleval($dReturn);
	}

	//
	// Запись в журнал событий битрикса
	// @return void
	//
	public static function EventLog($sEventId, $sLogMsg = '', $sAuditType = 'NIYAMA_IMPORT_ERRORS', $sModule = '', $sSeverity = '') {
		// запишем в журнал событий битрикса
		CEventLog::Log($sSeverity, $sAuditType, $sModule, $sEventId, $sLogMsg);
	}

	//
	// Транслитерация строки
	// @param string $sText - текст, который необходимо транслитерировать
	// @return string
	//
	public static function Translit($sText) {
		return CUtil::Translit(
			$sText, 
			'ru',
			array(
				'max_len' => 1000,
				'change_case' => 'L', // 'L' - toLower, 'U' - toUpper, false - do not change
				'replace_space' => '_',
				'replace_other' => '_',
				'delete_repeat_replace' => true,
				'safe_chars' => '',
			)
		);
	}

	// Хелпер. Возвращает значение атрибута объекта (для парсинга xml-тегов)
	public static function GetXmlAttrValue($mObject, $sAttr, $bTrim = true) {
		if(isset($mObject[$sAttr])) {
			return ($bTrim ? trim(strval($mObject[$sAttr])) : strval($mObject[$sAttr]));
		}
		return '';
	}

	// Хелпер. Возвращает значение атрибута объекта с типом данных "дата/время"
	public static function GetXmlDatetimeValue($mObject, $sAttr) {
		$sReturn = '';
		$sReturn = CProjectUtils::GetDateTimeFieldFilter(self::GetXmlAttrValue($mObject, $sAttr), false);
		return $sReturn;
	}

	// Хелпер. Возвращает статус признака обновления: true - нужно предварительно очистить всю таблицу, false - изменение/дополнение
	public static function GetXmlClearBeforeStatus($sXmlElementName, $arUpdateFlags) {
		$bClearBefore = false;
		if(isset($arUpdateFlags[$sXmlElementName]) && strlen($arUpdateFlags[$sXmlElementName]) == 1 && strval($arUpdateFlags[$sXmlElementName]) === '0') {
			$bClearBefore = true;
		}
		return $bClearBefore;
	}

	public static function FlushLockingFlag($sOptionCode) {
		// ! важно указать сайт параметра, иначе БУС будет писать и читать параметры как для разных сайтов !
		return COption::SetOptionString('main', $sOptionCode, '0', '', 's1');
	}

	public static function SetLockingFlag($sOptionCode, $iTimeoutSec) {
		// ! важно указать сайт параметра, иначе БУС будет писать и читать параметры как для разных сайтов !
		return COption::SetOptionString('main', $sOptionCode, (time() + $iTimeoutSec), '', 's1');
	}

	public static function GetLockingFlagTimeout($sOptionCode) {
		// ! важно указать сайт параметра, иначе БУС будет писать и читать параметры как для разных сайтов !
		return intval(COption::GetOptionString('main', $sOptionCode, '', 's1'));
	}

	public static function IsFlagLocked($sOptionCode) {
		$bReturn = false;
		$iNextTimestmp = self::GetLockingFlagTimeout($sOptionCode);
		if($iNextTimestmp > time()) {
			$bReturn = true;
		}
		return $bReturn;
	}

}

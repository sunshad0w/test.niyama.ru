<?

class CNiyamaModal {

	public static $sTmpSessionName = 'NIYAMA_SHOWMODAL';

	/**
	 * GetEnumXml
	 * @param $sXmlId
	 * @param $IBLOCK_ID
	 * @return array
	 */
	private static function GetEnumXml($sXmlId, $iIBlockId) {
		$arReturn = array();
		$arTypeEnumType = CCustomProject::GetEnumPropValues($iIBlockId, 'TYPE'); 
		foreach($arTypeEnumType as $k => $v) {
			$arReturn[$v['XML_ID']] = $v['VALUE'];
		}
		return $arReturn[$sXmlId];
	}

	/**
	 * getModal
	 * @param $sType
	 * @return array
	 */
	public static function GetModal($sType) {
		CModule::IncludeModule('iblock');
		$arReturn = array();
		$iIBlockId = CProjectUtils::GetIBlockIdByCode('modals', 'settings');
		$dbItems = CIBlockElement::GetList(
			array(
				'ID' => 'ASC'
			),
			array(
				'IBLOCK_ID' => $iIBlockId,
				'ACTIVE' => 'Y',
				'PROPERTY_TYPE_VALUE' => self::GetEnumXml($sType, $iIBlockId),
			),
			false,
			false,
			array(
				'ID', 'NAME', 'CODE', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DETAIL_TEXT',
				'PROPERTY_TYPE', 'PROPERTY_VIDEO'
			)
		);
		if($arItem = $dbItems->GetNext()) {
			$arReturn = $arItem;
		}
		return $arReturn;
	}

	/**
	 * viewModal
	 * @param $sType
	 */
	public static function ViewModal($sType, $iElemId = 0, $iModalShowItemId = 0) {
		$arModal = self::GetModal($sType);
		$arModal['sTYPE'] = $sType;
		$arModal['iModalShowItemId'] = $iModalShowItemId;
		$iElemId = intval($iElemId);
		if($iElemId > 0) {
			$arModal['ELEM_ID'] = $iElemId;
			if($sType == 'TYPE_2') {
				$arModal['ELEM_ARR'] = CNiyamaMedals::GetMedalDetail($iElemId);
			}			
		}
		if(!empty($arModal)) {
			$GLOBALS['APPLICATION']->IncludeComponent(
				'adv:system.empty',
				'niyama.1.0.modal',
				array(
					'DATA' => $arModal
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}
	}

	public static function IsSetModalShow($sType, $iUserId = false) {
		$bSetMedal = false;
		$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
		$obGet = UserModalShowTable::GetList(
			array(
				'filter' => array(
					'=UF_USER_ID' => $iUserId,
					'=UF_TYPE_ID' => $sType
				)
			)
		);
		if($obGet->getSelectedRowsCount() > 0) {
			$bSetMedal = true;
		}
		return $bSetMedal;
	}

	/**
	 * GetModalShow
	 * @param bool $iUserId
	 * @return array
	 */
	public static function GetModalShow($iUserId = false) {
		$arReturn = array();
		$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
		$obGet = UserModalShowTable::GetList(
			array(
				'filter' => array(
					'=UF_USER_ID' => $iUserId,
				)
			)
		);
		if($obGet->GetSelectedRowsCount() > 0) {
			while($arGet = $obGet->Fetch()) {
				$arReturn[] = $arGet;
			}
		}
		return $arReturn;
	}

	public static function InitModal($iUserId = false) {
		if($GLOBALS['USER']->IsAuthorized()) {
			$iUserId = $iUserId ? $iUserId : $GLOBALS['USER']->GetId();
			$arShowModal = self::GetModalShow($iUserId);
			if(!empty($arShowModal)) {
				self::ViewModal($arShowModal[0]['UF_TYPE_ID'], $arShowModal[0]['UF_ELEM_ID']);
				$obDel = UserModalShowTable::Delete($arShowModal[0]['ID']);
				if(!$obDel->isSuccess()) {
					// ?
				}
			}
		}
	}

	public static function SetSession($sType, $iUserId = false, $iElemId = 0) {
		$iUserId = $iUserId === false ? intval($GLOBALS['USER']->GetId()) : intval($iUserId);
		if($iUserId) {
			UserModalShowTable::Add(
				array(
					'UF_USER_ID' => $iUserId,
					'UF_TYPE_ID' => $sType,
					'UF_ELEM_ID' => $iElemId
				)
			);
		}
	}

	/**
	 * setUserLoginSMType
	 * @param $iUserId
	 */
	public static function SetUserLoginSMType($iUserId) {
		$filter = array(
			'ID' => $iUserId,
		);
		$rsUsers = CUser::GetList($by = 'id', $order = 'asc', $filter);
		if ($arItem = $rsUsers->Fetch()) {
			if($arItem['ID'] == $iUserId && $arItem['ADMIN_NOTES'] != 'sm') {
				$obUser = new CUser();
				$fields = array(
					'ADMIN_NOTES' => 'sm',
					'_SKIP_FO_SYNC_' => 'Y'
				);
				$obUser->Update($iUserId, $fields);
				CNiyamaModal::SetSession('TYPE_3', $iUserId);
			}
		}
	}
}

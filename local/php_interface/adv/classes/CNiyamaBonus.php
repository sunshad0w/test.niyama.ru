<?php

class CNiyamaBonus {
	public static $arTypeIB = array('user', 'main');

	public static function GetIbByType($typeIB) {
		$res = false;

		switch ($typeIB){
			case 'user':
				$res = 'IB_NIYAMA_BONUS_NIYAM_USERS';
			break;

			case 'main':
				$res = 'IB_NIYAMA_BONUS_NIYAM';
			break;
		}
		return $res;
	}

	public static function GetArrEnumByProp($prop_code,$name_IB) {
		$arReturn=array();
		$sCacheEntity = 'arr_enum_'.$name_IB;
		if(CStaticCache::IsSetCache($prop_code, $sCacheEntity)) {
			$arReturn = CStaticCache::GetCacheValue($prop_code, $sCacheEntity);
		} else {
			$db_enum_list = CIBlockProperty::GetPropertyEnum($prop_code, Array(), Array("IBLOCK_ID"=>CProjectUtils::GetIBlockIdByCode($name_IB)));
			while($ar_enum_list = $db_enum_list->GetNext()){
				$arReturn[$ar_enum_list['ID']]=$ar_enum_list['XML_ID'];
			}
			CStaticCache::SetCacheValue($prop_code, $arReturn, $sCacheEntity, 100);
		}
		return $arReturn;
	}

	public static function GetArrEnumXmlIdByProp($prop_code, $name_IB) {
		$arReturn=array();
		$sCacheEntity = 'arr_enum_xml_'.$name_IB;
		if(CStaticCache::IsSetCache($prop_code, $sCacheEntity)) {
			$arReturn = CStaticCache::GetCacheValue($prop_code, $sCacheEntity);
		} else {
			$db_enum_list = CIBlockProperty::GetPropertyEnum($prop_code, array(), array('IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB)));
			while($ar_enum_list = $db_enum_list->Fetch()) {
				$arReturn[$ar_enum_list['XML_ID']] = $ar_enum_list['ID'];
			}
			CStaticCache::SetCacheValue($prop_code, $arReturn, $sCacheEntity, 100);
		}
		return $arReturn;
	}

	public static function GetListBonusesByTypeEvent($typeEvent) {
		$arReturn = array();
		if(!CModule::IncludeModule('iblock')) {
			return $arReturn;
		}
		$iISBlockId = CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_BONUS_NIYAM', 'NIYAMA_DISCOUNTS');

		$name_IB = self::GetIbByType('main');
		$arTypeBASE_TYPE_ACTION_XML_ID = self::GetArrEnumXmlIdByProp('BASE_TYPE_ACTION', $name_IB);

		$arTypeBASE_TYPE_ACTION = CCustomProject::GetEnumPropValues($iISBlockId, 'BASE_TYPE_ACTION');
		$arTypeORDER_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_TYPE');
		$arTypeORDER_COUNT_NIAM = CCustomProject::GetEnumPropValues($iISBlockId, 'ORDER_COUNT_NIAM');
		$arTypeMEDAL_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'MEDAL_TYPE');
		$arTypeTIMELINE_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'TIMELINE_TYPE');
		$arTypeRECOMENDATIONSOC_TYPE = CCustomProject::GetEnumPropValues($iISBlockId, 'RECOMENDATIONSOC_TYPE');

		$dbItems = CIBlockElement::GetList(
			array(
				'ID' => 'ASC',
			),
			array(
				'IBLOCK_ID' => $iISBlockId,
				'ACTIVE' => 'Y',
				'CHECK_PERMISSIONS' => 'N',
				'PROPERTY_BASE_TYPE_ACTION' => array(
					$arTypeBASE_TYPE_ACTION_XML_ID[$typeEvent], 
					$arTypeBASE_TYPE_ACTION_XML_ID['ALL']
				),
			),
			false,
			false,
			array(
				'ID', 'TIMESTAMP_X', 'ACTIVE', 'NAME',

				'PROPERTY_BASE_TYPE_ACTION',
				'PROPERTY_ORDER_TYPE',
				'PROPERTY_ORDER_COUNT_NIAM',
				'PROPERTY_MEDAL_TYPE',
				'PROPERTY_MEDAL_LIST',
				'PROPERTY_TIMELINE_TYPE',
				'PROPERTY_TIMELINE_LIST',
				'PROPERTY_RECOMENDATIONSOC_TYPE',
				'PROPERTY_BASE_NUMBER_OF_ACTIONS',
				'PROPERTY_BASE_COUNT_NIYAM',
			)
		);
		while($arItem = $dbItems->Fetch()) {
			$arReturn[$arItem['ID']] = array(
				'ID' => $arItem['ID'],
				'ACTIVE' => $arItem['ACTIVE'],
				'TIMESTAMP_X' => $arItem['TIMESTAMP_X'],
				'NAME' => $arItem['NAME'],

				'PROPERTY_BASE_TYPE_ACTION' => isset($arTypeBASE_TYPE_ACTION[$arItem['PROPERTY_BASE_TYPE_ACTION_VALUE']]) ? $arTypeBASE_TYPE_ACTION[$arItem['PROPERTY_BASE_TYPE_ACTION_VALUE']]['XML_ID'] : '',
				'PROPERTY_ORDER_TYPE' => isset($arTypeORDER_TYPE[$arItem['PROPERTY_ORDER_TYPE_VALUE']]) ? $arTypeORDER_TYPE[$arItem['PROPERTY_ORDER_TYPE_VALUE']]['XML_ID'] : '',
				'PROPERTY_ORDER_COUNT_NIAM' => isset($arTypeORDER_COUNT_NIAM[$arItem['PROPERTY_ORDER_COUNT_NIAM_VALUE']]) ? $arTypeORDER_COUNT_NIAM[$arItem['PROPERTY_ORDER_COUNT_NIAM_VALUE']]['XML_ID'] : '',
				'PROPERTY_MEDAL_TYPE' => isset($arTypeMEDAL_TYPE[$arItem['PROPERTY_MEDAL_TYPE_VALUE']]) ? $arTypeMEDAL_TYPE[$arItem['PROPERTY_MEDAL_TYPE_VALUE']]['XML_ID'] : '',
				'PROPERTY_MEDAL_LIST' => $arItem['PROPERTY_MEDAL_LIST_VALUE'],
				'PROPERTY_TIMELINE_TYPE' => isset($arTypeTIMELINE_TYPE[$arItem['PROPERTY_TIMELINE_TYPE_VALUE']]) ? $arTypeTIMELINE_TYPE[$arItem['PROPERTY_TIMELINE_TYPE_VALUE']]['XML_ID'] : '',
				'PROPERTY_TIMELINE_LIST' => $arItem['PROPERTY_TIMELINE_LIST_VALUE'],
				'PROPERTY_RECOMENDATIONSOC_TYPE' => isset($arTypeRECOMENDATIONSOC_TYPE[$arItem['PROPERTY_RECOMENDATIONSOC_TYPE_VALUE']]) ? $arTypeRECOMENDATIONSOC_TYPE[$arItem['PROPERTY_RECOMENDATIONSOC_TYPE_VALUE']]['XML_ID'] : '',
				'PROPERTY_BASE_NUMBER_OF_ACTIONS' => doubleval($arItem['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE']) >= 1 ? $arItem['PROPERTY_BASE_NUMBER_OF_ACTIONS_VALUE'] : 1,
				'PROPERTY_BASE_COUNT_NIYAM' => doubleval($arItem['PROPERTY_BASE_COUNT_NIYAM_VALUE']) >= 0 ? $arItem['PROPERTY_BASE_COUNT_NIYAM_VALUE'] : 0,
			);
		}

		return $arReturn;
	}

	public static function CheckBonusesByTypeEvent($typeEvent, $user_id = 0, $dop_event = array()) {
		if ($user_id <= 0) {
			if ($typeEvent == 'ORDER') {
				if (!empty($dop_event['ORDER']['ORDER_ID']) && $dop_event['ORDER']['ORDER_ID'] > 0) {
					$order = CNiyamaOrders::GetOrderById($dop_event['ORDER']['ORDER_ID']);
				} else {
					return false;
				}
			}
			if($order && $order['ORDER']['USER_ID']) {
				$iUserId = $order['ORDER']['USER_ID'];
				$user_id = $iUserId;
			} elseif($GLOBALS['USER']->IsAuthorized()) {
				$iUserId = $GLOBALS['USER']->GetID();
				$user_id = $iUserId;
			}
		}

		if($user_id <= 0) {
			return false;
		}
		$list_bonuses = self::GetListBonusesByTypeEvent($typeEvent);

		if(!empty($list_bonuses)) {
			foreach($list_bonuses as $id_bonus => $bonus) {
				$bonus['typeEvent'] = $typeEvent;
				$bonus['dop_event'] = $dop_event;
				$bonus['arr_users_bonus'] = self::GetArUserBonus($id_bonus, $user_id);

				if(self::CheckBonusTypeAction($bonus)) {
					self::AddBonusByEvent($bonus);
				}
			}
		}
	}

	// $dop_event['RECOMENDATIONSOC']
	// $dop_event['ORDER']['ORDER_ID']
	// $dop_event['ORDER']['BONUS_TYPEACTIVE']='NEXT' (CURRENT по умл.)
	public static function CheckBonusTypeAction($bonus) {
		$returnFlag = false;
		switch ($bonus['typeEvent']) {
			case 'ORDER':
				$returnFlag = self::CheckBonusTypeActionORDER($bonus);
			break;

			case 'AUTHSOC':
				$returnFlag = self::CheckBonusTypeActionAUTHSOC($bonus);
			break;

			case 'SURVEY':
				$returnFlag = self::CheckBonusTypeActionSURVEY($bonus);
			break;

			case 'MEDAL':
				$returnFlag = self::CheckBonusTypeActionMEDAL($bonus);//$dop_event['MEDAL']['MEDAL_ID']
			break;

			case 'TIMELINE':
				$returnFlag = self::CheckBonusTypeActionTIMELINE($bonus);//, $dop_event['TIMELINE']['TIMELINE_ID']
			break;

			case 'RECOMENDATIONSOC':
				$returnFlag = self::CheckBonusTypeActionRECOMENDATIONSOC($bonus);
			break;
		}
		return $returnFlag;
	}

	public static function CheckBonusTypeActionORDER($bonus) {
		if(!isset($bonus['dop_event']['ORDER']['ORDER_ID']) || $bonus['dop_event']['ORDER']['ORDER_ID'] <= 0) {
			return false;
		}
		if(empty($bonus['PROPERTY_ORDER_TYPE']) || $bonus['PROPERTY_ORDER_TYPE'] == 'COUNT_N') {
			return true;
		} elseif($bonus['PROPERTY_ORDER_TYPE'] == 'BIRTHDAY') {
			$sCurDate = date('m.d');
			$sUserBirthday = $bonus['arr_users_bonus']['user_id'] ? CUsersData::GetPersonalBirthday($bonus['arr_users_bonus']['user_id']) : '';
			if(strlen($sUserBirthday)) {
				$obTmpDate = date_create($sUserBirthday);
				$sUserBirthday = $obTmpDate ? $obTmpDate->Format('m.d') : '';
			}
			if(strlen($sUserBirthday)) {
				if($sUserBirthday == $sCurDate) {
					return true;
				} elseif($sUserBirthday == '02.29' && $sCurDate == '03.01') {
					// те, у кого д.р. 29 февраля, в невисокосные годы будут праздновать его первого марта 
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public static function CheckBonusTypeActionAUTHSOC($bonus) {
		return true;
	}

	public static function CheckBonusTypeActionSURVEY($bonus) {
		return true;
	}

	public static function CheckBonusTypeActionMEDAL($bonus) {
		if (empty($bonus['PROPERTY_MEDAL_TYPE']) || ($bonus['PROPERTY_MEDAL_TYPE'] == 'COUNT_N')) {
			return true;
		} elseif($bonus['PROPERTY_MEDAL_TYPE'] == 'LIST') {
			if (!(is_array($bonus['PROPERTY_MEDAL_LIST']))) {
				$bonus['PROPERTY_MEDAL_LIST'] = array();
			}
			if (empty($bonus['PROPERTY_MEDAL_LIST'])) {
				return true;
			}
			if (in_array($bonus['dop_event']['MEDAL']['MEDAL_ID'], $bonus['PROPERTY_MEDAL_LIST'])) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function CheckBonusTypeActionTIMELINE($bonus) {
		if (empty($bonus['PROPERTY_TIMELINE_TYPE']) || ($bonus['PROPERTY_TIMELINE_TYPE'] == 'COUNT_N')) {
			// за каждый N уровень
			return true;
		} elseif($bonus['PROPERTY_TIMELINE_TYPE'] == 'LIST') {
			if (!is_array($bonus['PROPERTY_TIMELINE_LIST'])) {
				$bonus['PROPERTY_TIMELINE_LIST'] = array();
			}
			if (empty($bonus['PROPERTY_TIMELINE_LIST'])) {
				return true;
			}
			if (in_array($bonus['dop_event']['TIMELINE']['TIMELINE_ID'], $bonus['PROPERTY_TIMELINE_LIST'])) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function CheckBonusTypeActionRECOMENDATIONSOC($bonus){
		// $dop_event "TWEET_YROVEN_OR_MEDAL" or "RECOMENDATON_FROM_VIDZHET" array('RECOMENDATIONSOC'=>TWEET_YROVEN_OR_MEDAL)
		if(empty($bonus['PROPERTY_RECOMENDATIONSOC_TYPE']) || $bonus['PROPERTY_RECOMENDATIONSOC_TYPE'] == 'ANY') {
			# Если любая рекомендация
			return true;
		} elseif($bonus['PROPERTY_RECOMENDATIONSOC_TYPE'] == $bonus['dop_event']['RECOMENDATIONSOC']) {
			return true;
		} else {
			return false;
		}
	}

	public static function AddBonusByEvent($bonus) {
		$typeEvent = $bonus['typeEvent'];
		$arr_users_bonus = $bonus['arr_users_bonus'];
		$user_id = intval($arr_users_bonus['user_id']);
		if($user_id <= 0) {
			return;
		}
		$order_id = isset($bonus['dop_event']['ORDER']['ORDER_ID']) ? intval($bonus['dop_event']['ORDER']['ORDER_ID']) : 0;
		$NUMBER_OF_ACTIONS = $bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS'];
		$count_number_of_action = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_'.$typeEvent] + 1; // кол-во выполненных действий
		if($bonus['PROPERTY_BASE_TYPE_ACTION'] == 'ALL') {
			$count_number_of_action_arr = array(
				'ORDER' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'],
				'AUTHSOC' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'],
				'SURVEY' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'],
				'RECOMENDATIONSOC' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'],
				'MEDAL' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL'],
				'TIMELINE' => $arr_users_coupon['PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE']
			);
			$count_number_of_action_arr[$typeEvent] = $count_number_of_action;
			$count_number_of_action_min = min($count_number_of_action_arr);
			$quanity_add_curr = floor($count_number_of_action_min / $NUMBER_OF_ACTIONS); // кол-во новых купонов к получению
			$quanity_add = $quanity_add_curr + $arr_users_bonus['PROPERTY_BASE_QUANITY']; // новое количество купонов
		} else {
			$quanity_add_curr = floor($count_number_of_action / $NUMBER_OF_ACTIONS); // кол-во новых купонов к получению
			$quanity_add = $quanity_add_curr + $arr_users_bonus['PROPERTY_BASE_QUANITY']; // новое количество купонов
		}

		if ($quanity_add_curr > 0) {
			$count_number_of_action_add = $count_number_of_action % $NUMBER_OF_ACTIONS; // новое кол-во действий
		} else {
			$count_number_of_action_add = $count_number_of_action; // новое кол-во действий
		}

		//$count_action_add=$arr_users_bonus['PROPERTY_BASE_COUNT_ACTION']+$quanity_add_curr;// новое кол-во полученных купонов
		$val_bonus_niam = $bonus['PROPERTY_BASE_COUNT_NIYAM'];
		if($typeEvent == 'ORDER') {
			if($bonus['PROPERTY_ORDER_COUNT_NIAM'] == 'Y') {
				if($order_id > 0) {
					$aCartBonuses = CNiyamaCart::GetCartListByOrderIdWithoutBonusBluds($order_id);
					$count_niam = $aCartBonuses['TOTAL_PRICE'];
					$val_bonus_niam = $count_niam;
				} else {
					$val_bonus_niam = 0;
				}
			}
		}

		$name_IB = self::GetIbByType('user');
		$addBonus = array();
		if($quanity_add_curr > 0) {
			if($bonus['PROPERTY_BASE_TYPE_ACTION'] == 'ALL') {
				$ORDER = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$AUTHSOC = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$SURVEY = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$RECOMENDATIONSOC = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$MEDAL = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$TIMELINE = $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE'] - ($NUMBER_OF_ACTIONS * $quanity_add_curr);
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'] = $ORDER > 0 ? $ORDER : 0;
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'] = $AUTHSOC > 0 ? $AUTHSOC : 0;
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'] = $SURVEY > 0 ? $SURVEY : 0;
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'] = $RECOMENDATIONSOC > 0 ? $RECOMENDATIONSOC : 0;
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL'] = $MEDAL > 0 ? $MEDAL : 0;
				$arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE'] = $TIMELINE > 0 ? $TIMELINE : 0;
			}
		}
		$defBonus = array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB),
			'ACTIVE' => 'Y',
			'NAME' => $bonus['NAME'],
		);

		$defBonusProp = array(
			'BONUS_ID' => $bonus['ID'],
			'USER_ID' => $user_id,
			'ORDER_ID' => $order_id > 0 ? $order_id : 0,

			'BASE_QUANITY'	=> $quanity_add,
			'BASE_NUMBER_OF_ACTIONS_ORDER' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER'],
			'BASE_NUMBER_OF_ACTIONS_AUTHSOC' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC'],
			'BASE_NUMBER_OF_ACTIONS_SURVEY' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY'],
			'BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'],
			'BASE_NUMBER_OF_ACTIONS_MEDAL' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL'],
			'BASE_NUMBER_OF_ACTIONS_TIMELINE' => $arr_users_bonus['PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE'],
		);
		$addBonus = $defBonus;
		$addBonus['PROPERTY_VALUES'] = $defBonusProp;

		$addBonus['PROPERTY_VALUES']['BASE_NUMBER_OF_ACTIONS_'.$typeEvent] = !empty($count_number_of_action_add) ? $count_number_of_action_add : 0;

		$name_IB = CNiyamaCoupons::GetIbByType(CNiyamaCoupons::$arTypeIB[0]);
		$addCoupon = array();
		$user_fields = array();
		$type_bonus = CNiyamaCoupons::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE', $name_IB);
		$type_img = CNiyamaCoupons::GetArrEnumXmlIdByProp('BASE_TYPE_IMG', $name_IB);
		$type_money = CNiyamaCoupons::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE', $name_IB);
		$type_visual = CNiyamaCoupons::GetArrEnumXmlIdByProp('BASE_BONUS_TYPE_BLUDO_VISIUAL', $name_IB);
		$sum_discount = CNiyamaCoupons::GetArrEnumXmlIdByProp('BASE_BONUS_NO_SUM', $name_IB);

		$defCoupon = array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($name_IB),
			'ACTIVE' => 'Y',
			'NAME' => $bonus['NAME'],
		);
		if($quanity_add_curr > 0) {
			if($bonus['PROPERTY_BASE_TYPE_ACTION'] == 'ALL' || $bonus['PROPERTY_BASE_TYPE_ACTION'] == 'ORDER') {
				$defCoupon['ACTIVE'] = 'N';
			}
		}
		$defProp = array(
			'COUPON_ID'=> $bonus['ID'],
			'USER_ID' => $user_id,
			'ORDER_ID' => $order_id > 0 ? $order_id : 0,
			'BASE_TYPE_IMG' => array('VALUE' => $type_img['DEFAULT']),
			'BASE_BONUS_TYPE' => array('VALUE' => $type_bonus['DISCOUNT_MONEY']),
			'BASE_BONUS_TYPE_DISCOUNT_MONEY_TYPE' => array('VALUE' => $type_money['NIYAM']),
			'BASE_BONUS_TYPE_DISCOUNT_MONEY' => $val_bonus_niam,

			'BASE_QUANITY' => $quanity_add,
		);
		$addCoupon = $defCoupon;
		$addCoupon['PROPERTY_VALUES'] = $defProp;

		if($quanity_add_curr > 0) {
			if($bonus['PROPERTY_BASE_TYPE_ACTION'] != 'ALL' && $bonus['PROPERTY_BASE_TYPE_ACTION'] != 'ORDER') {
				$val_coupon = CNiyamaCoupons::GetCouponValByType(CNiyamaCoupons::$BONUS_NIYAM, $user_id);
				$user_fields['UF_'.CNiyamaCoupons::$BONUS_NIYAM] = $val_coupon + intval($val_bonus_niam) * $quanity_add_curr;
			}
		}
		$bonus_users = 0;
		$id_bonus_users = 0;
		if(!empty($addBonus)) {
			$obIBlockElement = new CIBlockElement();
			if($arr_users_bonus['ID'] > 0) {
				$result = $obIBlockElement->Update($arr_users_bonus['ID'], $addBonus);
				$id_bonus_users = $arr_users_bonus['ID'];
			} else {
				$result = $obIBlockElement->Add($addBonus);
				$id_bonus_users = $result;
			}
		}

		if($quanity_add_curr > 0) {
			if(!empty($addCoupon)) {
				$arr_users_coupon = array();
				$arr_users_coupon['ID'] = false;
				if($id_bonus_users > 0) {
					$arr_users_coupon = CNiyamaCoupons::GetArUserCoupon($id_bonus_users, $user_id);
				}
				$obIBlockElement = new CIBlockElement();
				if($arr_users_coupon['ID'] !== false && $arr_users_coupon['ID'] > 0) {
					$result = $obIBlockElement->Update($arr_users_coupon['ID'], $addCoupon);
				} else {
					$result = $obIBlockElement->Add($addCoupon);
				}
			}
			if(!empty($user_fields)) {
				$obUser = new CUser();
				$obUser->Update($user_id, $user_fields);
			}
		}
	}

	public static function GetArUserBonus($id_bonus, $user_id) {
		$result = array(
			'ID' => false,
			'user_id' => $user_id,
			'PROPERTY_BASE_QUANITY' => 0, // кол-во полученных бонусов
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY' => 0,
			'PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => 0,
		);

		if(!CModule::IncludeModule('iblock')) {
			return $result;
		}

		$arFilter = array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('IB_NIYAMA_BONUS_NIYAM_USERS', 'NIYAMA_DISCOUNTS'),
			'=PROPERTY_USER_ID' => $user_id,
			'PROPERTY_BONUS_ID' => $id_bonus,
		);
		$res = CIBlockElement::GetList(
			array(
				'active_from' => 'DESC'
			),
			$arFilter,
			false,
			false,
			array(
				'ID',
				'PROPERTY_BASE_QUANITY', 'PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER', 'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC',
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL', 'PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE', 'PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY',
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC'
			)
		);
		if($ar_fields = $res->GetNext()) {
			$result = array(
				'ID' => $ar_fields['ID'],
				'user_id' => $user_id,
				'PROPERTY_BASE_QUANITY' => $ar_fields['PROPERTY_BASE_QUANITY_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_ORDER_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_AUTHSOC_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_MEDAL_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_TIMELINE_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_SURVEY_VALUE'],
				'PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC' => $ar_fields['PROPERTY_BASE_NUMBER_OF_ACTIONS_RECOMENDATIONSOC_VALUE'],
			);
		}
		return $result;
	}

}

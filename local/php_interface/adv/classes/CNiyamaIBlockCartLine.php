<?

class CNiyamaIBlockCartLine {

	public static function GetIBlockId() {
		return CProjectUtils::GetIBlockIdByCode('cartline', 'customer_loyalty');
	}

	public static function GetCurUserPermissions() {
		CModule::IncludeModule('iblock');
		return CIBlock::GetPermission(self::GetIBlockId());
	}

    /**
     * GetList
     * @param bool $bRefreshCache
     * @return array
     */
    public static function GetList($bRefreshCache = false)
    {
        $arReturn = array();
        // дополнительный параметр для ID кэша
        $sCacheAddParam = 'v3';
        // идентификатор кэша (обязательный и уникальный параметр)
        $sCacheId = __CLASS__.'||'.__FUNCTION__;
        // массив тегов для тегированного кэша (если пустой, то тегированный кэш не будет использован)
        $arAddCacheTags = array(__METHOD__);
        // путь для сохранения кэша
        $sCachePath = '/'.__CLASS__.'/'.__FUNCTION__.'/';
        // сохранять ли значения дополнительно в виртуальном кэше
        $bUseStaticCache = true;
        // максимальное количество записей виртуального кэша
        $iStaticCacheEntityMaxSize = METHODS_DEFAULT_CACHE_ENTITY_SIZE;
        // соберем в массив идентификационные параметры кэша
        $arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam);

        $obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize);
        if(!$bRefreshCache && $obExtCache->InitCache()) {
			$arReturn = $obExtCache->GetVars();
        } else {
        	$iIBlockId = self::GetIBlockId();

			// открываем кэшируемый участок
			$obExtCache->StartDataCache();
			CModule::IncludeModule('iblock');

			$rsItems = CIBlockElement::GetList(
				array(
					'SORT' => 'ASC'
				),
				array(
					'IBLOCK_ID' => $iIBlockId,
				), 
				false, 
				false, 
				array(
					'ID', 'NAME', 'CODE', 'SORT', 'ACTIVE', 'XML_ID',
					'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DETAIL_TEXT',
					'PROPERTY_CUPONBG_VERTICAL', 'PROPERTY_CUPONBG_HORIZONTAL'
				)
			);
			while($arItem = $rsItems->Fetch()) {
				$arReturn[] = $arItem;
			}

			// закрываем кэшируемый участок
			$obExtCache->EndDataCache($arReturn);
        }
        return $arReturn;
    }

	public static function GetLineByPriceVal($dOrderPrice) {
		$dOrderPrice = doubleval($dOrderPrice);
		$arReturn = array();
		if($dOrderPrice > 0) {
			$arLine = self::GetList();
			foreach($arLine as $arItem) {
				if(!empty($arItem['CODE'])) {
					if(doubleval($arItem['CODE']) <= $dOrderPrice){
						$arReturn[$arItem['ID']]= $arItem;
					}
				}
			}
		}
		return $arReturn;
	}

	public static function GetLineByOrderId($iOrderId) {
		$iOrderId = intval($iOrderId);
		$dOrderPrice = 0;
		if($iOrderId > 0 && CModule::IncludeModule('sale')) {
			$arOrder = CSaleOrder::GetById($iOrderId);
			$dOrderPrice = $arOrder['PRICE'] ? doubleval($arOrder['PRICE']) : 0;
		}
		return self::GetLineByPriceVal($dOrderPrice);
	}

	public static function calcLine() {
		$arCart = CNiyamaCart::getTotalPrice();
		$dTotalPrice = doubleval($arCart['TOTAL_PRICE']);
		return self::GetLineByPriceVal($dTotalPrice);
	}


}
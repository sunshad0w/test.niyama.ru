<?
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 15.05.2015
 */

class CNiyamaOldSiteAuth {

	private $arConnectParams = array();
	private $sLastError = '';

	/**
	 * @param array $arConnectParams
	 */
	public function __construct($arConnectParams = array()) {
		$this->InitConnectParams($arConnectParams);
	}

	/**
	 * InitConnectParams
	 * @param $arParams
	 */
	private function InitConnectParams($arParams) {
		$this->arConnectParams = array();
		$arParams = is_array($arParams) ? $arParams : array();

		$mDefVal = defined('NIYAMA_AUTH_SERVICE_DOMAIN') ? NIYAMA_AUTH_SERVICE_DOMAIN : '';
		$this->arConnectParams['DOMAIN'] = isset($arParams['DOMAIN']) ? $arParams['DOMAIN'] : $mDefVal;
	}

	/**
	 * GetParam
	 * @param $sParamCode
	 * @return string
	 */
	public function GetParam($sParamCode) {
		return isset($this->arConnectParams[$sParamCode]) ? $this->arConnectParams[$sParamCode] : '';
	}

	/**
	 * Проверка на Json
	 * isJson
	 * @param $string
	 * @return bool
	 */
	private function IsJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * CurlConnect
	 * @param $sDomain
	 * @param $arSendParam
	 * @return bool|mixed|object
	 */
	private function CurlConnect($sDomain, $arSendParam) {
		$mReturn = false;
		$this->FlushLastError();
		if(function_exists('curl_version')) {
			if(!empty($arSendParam)) {
				$sJsonEncodingData = json_encode($arSendParam);
			}

			$rsCurl = curl_init();
			curl_setopt($rsCurl, CURLOPT_URL, $sDomain);
			curl_setopt($rsCurl, CURLOPT_POST, 1);
			curl_setopt($rsCurl, CURLOPT_POSTFIELDS, $sJsonEncodingData);
			curl_setopt($rsCurl, CURLOPT_RETURNTRANSFER, true);
			$mServerReturn = curl_exec($rsCurl);
			if($mServerReturn === false) {
				$this->SetLastError('Ошибка запроса к удаленному серверу: '.curl_error($rsCurl));
			}
			curl_close($rsCurl);

			if($mServerReturn !== false) {
				$obServerReturn = (object) array();
				if($this->IsJson($mServerReturn)) {
					$obServerReturn = json_decode($mServerReturn);
					$mReturn = $obServerReturn;
				} else {
					$this->SetLastError('Получен неизвестный ответ сервера');
				}
			}
		} else {
			$this->SetLastError('Не установлены необходимые модули для удаленной авторизации');
		}
		return $mReturn;
	}

	/**
	 * Запрос авторизации пользователя на старом сайте
	 *
	 * @param $sEmail
	 * @param $sPassword
	 * @return bool
	 */
	public function AuthUserRequest($sEmail, $sPassword) {
		# Подключаемся к web-сервису
		$obReturn = $this->CurlConnect(
			$this->GetParam('DOMAIN'),
			array(
			   'email' => $sEmail,
			   'password' => $sPassword
			)
		);

		return $obReturn && isset($obReturn->result) ? $obReturn->result : false;
	}

	/**
	 * Авторизация юзера на старом сайте
	 * 
	 * @param $sEmail
	 * @param $sPassword
	 * @param $bTransferUserAuth
	 * @return bool
	 */
	public function AuthUser($sEmail, $sPassword, $bTransferUserAuth = true) {
		$sReturnCode = 'EMPTY_PARAMS';
		if(strlen($sEmail) && strlen($sPassword)) {
			$sReturnCode = 'OLD_SITE_AUTH_CLOSED';

			// Проверям юзера на старом сайте только если локально он деактивирован и задан соответствующий внешний код
			$arUser = CUser::GetByLogin($sEmail)->Fetch();
			if($arUser && $arUser['ACTIVE'] == 'N' && $arUser['XML_ID'] == 'check_old_site_auth') {
				// юзер может авторизоваться удаленно
				$sReturnCode = 'OLD_SITE_AUTH_FAIL';
				if($this->AuthUserRequest($sEmail, $sPassword)) {
					// авторизация удаленно прошла успешно
					// пробуем авторизоваться локально
					$sReturnCode = 'LOCAL_SITE_AUTH_FAIL';
					$bDoAuth = true;
					if($bTransferUserAuth) {
						// переносим дальнейшую авторизацию юзера на локальный сайт
						$bTransferResult = $this->TransferUserAuth($arUser['ID'], $sPassword);
						if(!$bTransferResult) {
							$bDoAuth = false;
							$sReturnCode = 'AUTH_TRANSFER_FAIL';
						}
					}
					if($bDoAuth) {
						if($GLOBALS['USER']->Authorize($arUser['ID'])) {
							$sReturnCode = 'LOCAL_SITE_AUTH_SUCCESS';
						}
					}
				}
			}
		}
		return $sReturnCode;
	}

	protected function TransferUserAuth($iUserId, $sPassword) {
		$this->FlushLastError();
		$obUser = new CUser();
		$bReturn = $obUser->Update(
			$iUserId,
			array(
				'ACTIVE' => 'Y',
				'PASSWORD' => $sPassword,
				'CONFIRM_PASSWORD' => $sPassword,
				// сбрасываем код, чтобы при возможной деактивации юзера в будущем, он больше не стучался на сатрый сайт
				'XML_ID' => '',
				// отключаем отражение в таймлайне и синхронизацию данных с FO
				'_NIYAMA_IGNORE_TIMELINE_' => 'Y',
				'_SKIP_FO_SYNC_' => 'Y',
			)
		);
		if(!$bReturn) {
			$this->SetLastError($obUser->LAST_ERROR);
		}
		return $bReturn;
	}

	protected function FlushLastError() {
		$this->sLastError = '';
	}

	protected function SetLastError($sErrMsg) {
		$this->sLastError = $sErrMsg;
	}

	public function GetLastError() {
		return $this->sLastError;
	}
}

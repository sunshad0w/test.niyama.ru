<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Константы (статические константы объявлять только здесь)
 * (файл подключается в php_interface/init.php)
 */

// Максимальное количество записей виртуального кэша
@define('METHODS_DEFAULT_CACHE_ENTITY_SIZE', 100);

// Параметры подключения к web-сервису системы FastOperator Нияма
//@define('NIYAMA_FO_SERVICE_WSDL', 'http://192.168.100.234:1234/FastOperator.asmx?WSDL'); // niyama
//@define('NIYAMA_FO_SERVICE_WSDL', 'http://192.168.100.234:1238/FastOperator.asmx?WSDL'); // тестовый
//@define('NIYAMA_FO_SERVICE_WSDL', 'http://192.168.100.234:1300/FastOperator.asmx?WSDL'); // pizzapi
/*тестов*/
@define('NIYAMA_FO_SERVICE_WSDL', 'http://192.168.100.234:1300/FastOperator.asmx?WSDL');
@define('NIYAMA_FO_SERVICE_LOGIN', ''); // пользователь подключения к веб-сервису
@define('NIYAMA_FO_SERVICE_PASSWORD', ''); // пароль подключения к веб-сервису
@define('NIYAMA_FO_SERVICE_ENABLE_WSDL_CACHE', true); // кэшировать ли ответ WSDL
@define('NIYAMA_FO_SERVICE_WSDL_RESERVE', 'http://93.186.53.234:1234/FastOperator.asmx?WSDL');
/*тестовое*/
//@define('NIYAMA_FO_SERVICE_WSDL', 'http://192.168.100.234:1238/FastOperator.asmx?WSDL');
@define('NIYAMA_FO_SERVICE_CONNECTION_TIMEOUT', 5);
//@define('NIYAMA_FO_IMPORT_IMAGES_DIR', 'http://194.54.177.66:1234/images/');
@define('NIYAMA_FO_IMPORT_IMAGES_DIR', 'http://192.168.100.234:1234/images/');


// Параметры подключения к web-сервису системы ПДС Нияма
//@define('NIYAMA_PDS_SERVICE_DOMAIN', '194.54.177.66');
@define('NIYAMA_PDS_SERVICE_DOMAIN', '185.42.108.34');
@define('NIYAMA_PDS_SERVICE_PORT', '3400');

// Параметры подключения к web-сервису аторизации старого сайта
@define('NIYAMA_AUTH_SERVICE_DOMAIN', 'http://146.185.161.34/api/auth/');


// логирование операций с AcquiroPay
@define('_ACQUIROPAY_DEBUG_', true);
// логирование операции экспорта заказов в FO
//@define('_NIYAMA_LOG_FO_ADD_ORDER_', true);

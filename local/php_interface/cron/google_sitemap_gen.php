<?
//
// Cron-скрипт генерации карты сайта
//

@ini_set('mbstring.func_overload', 2);
@ini_set('mbstring.internal_encoding', 'UTF-8');

if(!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = realpath(dirname(__FILE__).'/../../..');
}

/*
ini_set('display_errors', '0');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('log_errors', '1');
ini_set('error_log', $_SERVER['DOCUMENT_ROOT'].'/_log/_php_error_.log');
*/

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true); 
define('BX_NO_ACCELERATOR_RESET', true);

define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

@set_time_limit(0);
@ignore_user_abort(true);

if(!isset($GLOBALS['USER']) || !is_object($GLOBALS['USER'])) {
	$GLOBALS['USER'] = new CUser();
}

CModule::IncludeModule('search');

// генерация google sitemap для сайта s1
$sSiteId = 's1';
$arOptions = array(
	'FORUM_TOPICS_ONLY' => 'Y',
	'BLOG_NO_COMMENTS' => 'Y',
	'USE_HTTPS' => 'N',
);
$obSiteMap = new CSiteMap();
$bContinue = true;
$arPrevStep = array();
while($bContinue) {
	$bContinue = false;
	$mSiteMapResult = $obSiteMap->Create($sSiteId, array(3600, 5000), $arPrevStep, $arOptions);
	if(is_array($mSiteMapResult) && $mSiteMapResult['ID']) {
		$arPrevStep = $mSiteMapResult;
		$bContinue = true;
	} if(!$mSiteMapResult) {
		// ошибка
		echo $obSiteMap->m_error;
	}
} 


require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

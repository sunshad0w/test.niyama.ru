<?
//
// Для корректной работы отправки почты и отработки ВСЕХ (периодических и непериодических) агентов через cron необходимо:
// 1) в dbconn_local.php добавить:
// блокируем на хитах отправку почты и запуск агентов
// if(!defined('BX_CRONTAB_SUPPORT')) {
//	define('BX_CRONTAB_SUPPORT', true);
// }
// if(!defined('DisableEventsCheck')) {
//	define('DisableEventsCheck', true);
// }
// if(!defined('NO_AGENT_CHECK')) {
//	define('NO_AGENT_CHECK', true);
// }
//
// 2) выполнить ОДИН раз:
// включаем запуск агентов в прологе, чтобы не нарушать окружение, само же отключение агентов на хитах будет управляться через NO_AGENT_CHECK:
// COption::SetOptionString('main', 'check_agents', 'Y');
// чтобы выполнялись и периодические, и непериодические агенты:
// COption::SetOptionString('main', 'agents_use_crontab', 'N');
// отключаем отправку почты в эпилоге:
// COption::SetOptionString('main', 'check_events', 'N');
//
// проверить результат можно так:
// echo COption::GetOptionString('main', 'check_agents', ''); // Y
// echo COption::GetOptionString('main', 'agents_use_crontab', ''); // N
// echo COption::GetOptionString('main', 'check_events', ''); // N

//
// P.S. Чтобы перевести обратно агенты и отправку почты на хиты посетителей:
// define('BX_CRONTAB_SUPPORT', false);
// COption::SetOptionString('main', 'check_agents', 'Y');
// COption::SetOptionString('main', 'agents_use_crontab', 'N');
// COption::SetOptionString('main', 'check_events', 'Y');
//

// !!! Внимание !!! 
// При разработке обработчиков агентов необходимо учитывать особенность их запуска методом CAgents::ExecuteAgents(). 
// На отработку ВСЕЙ очереди дается 10 минут, т.о. если, например, обработчик запускается последним в очереди 
// и с момента старта CAgents::ExecuteAgents() прошло 9 минут, а плановая работа агента больше минуты, то есть высокая вероятность старта параллельного процесса работы агента.
// Если же плановая работа больше 10 минут, то такие агенты будут практически постоянно работать в несколько параллельных процессов.


@ini_set('mbstring.func_overload', 2);
@ini_set('mbstring.internal_encoding', 'UTF-8');

if(!$_SERVER['DOCUMENT_ROOT']) {
	$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = realpath(dirname(__FILE__).'/../../..');
}

/*
ini_set('display_errors', '0');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('log_errors', '1');
ini_set('error_log', $_SERVER['DOCUMENT_ROOT'].'/_log/_php_error_.log');
*/

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true); 
define('BX_NO_ACCELERATOR_RESET', true);

#
# Условия для отправки почты:
# $check_events == Y и DisableEventsCheck == false и (BX_CRONTAB_SUPPORT == false или BX_CRONTAB == true)
#
# Условия запуска всех типов агентов:
# $check_agents == 'Y' и NO_AGENT_CHECK == false [и BX_CLUSTER_GROUP == 1] и $agents_use_crontab == 'N' и BX_CRONTAB_SUPPORT == false
#
# Условия запуска непериодических агентов:
# $check_agents == 'Y' и NO_AGENT_CHECK == false [и BX_CLUSTER_GROUP == 1] и ($agents_use_crontab == 'Y' или BX_CRONTAB_SUPPORT == true) и BX_CRONTAB = true
#
# Условия запуска периодических агентов:
# $check_agents == 'Y' и NO_AGENT_CHECK == false [и BX_CLUSTER_GROUP == 1] и ($agents_use_crontab == 'Y' или BX_CRONTAB_SUPPORT == true) и BX_CRONTAB = false
#

// забъем константу, чтобы ее нигде не установили в true и не заблокировали запуск агентов
define('NO_AGENT_CHECK', false);
// если опция agents_use_crontab = Y или BX_CRONTAB_SUPPORT = true, то проверяется BX_CRONTAB
// при BX_CRONTAB == false будут выполняться только периодические агенты, true - только непериодические
define('BX_CRONTAB', false); 
// BX_CRONTAB_SUPPORT установим в false, чтобы выполнялись все агенты (при условии, что agents_use_crontab = N)
define('BX_CRONTAB_SUPPORT', false);
// забъем константу, чтобы ее нигде не установили больше в true и не заблокировали отправку почты
define('DisableEventsCheck', false);		

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

@set_time_limit(0);
@ignore_user_abort(true);

$sTmpVal = COption::GetOptionString('main', 'check_agents', '');
if($sTmpVal != 'Y') {
	// агенты не отрабатывают в прологе
	CAgent::CheckAgents();
}

$sTmpVal = COption::GetOptionString('main', 'check_events', '');
if($sTmpVal != 'Y') {
	CEvent::CheckEvents();
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

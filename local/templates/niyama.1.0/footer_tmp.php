<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//IncludeTemplateLangFile(__FILE__);
					?></div><? 
					if($GLOBALS['APPLICATION']->GetCurPage(true) == SITE_DIR.'index.php') {
						?><div id="home-page-footer-description" class="description_main __footer"><? 
							 $GLOBALS['APPLICATION']->IncludeFile(
								SITE_DIR.'include_areas/home_index_seo.php',
								array(),
								array(
									'MODE' => 'html',
								)
							);
						?></div><?
					}
				?></div><?
				?><div class="page__sidebar <?$GLOBALS['APPLICATION']->ShowProperty('NIYAMA_PAGE_SIDEBAR_CLASS', 'default-sidebar')?>"><?
					// левое меню
					$GLOBALS['APPLICATION']->IncludeComponent(
						'bitrix:menu',
						'niyama.1.0.left',
						array(
							'ROOT_MENU_TYPE' => 'top',
							'CHILD_MENU_TYPE' => 'left',
							'MENU_CACHE_TYPE' => 'A',
							'MENU_CACHE_TIME' => '43200',
							'MENU_CACHE_USE_GROUPS' => 'N',
							'MENU_CACHE_GET_VARS' => array(),
							'MAX_LEVEL' => '3',
							'USE_EXT' => 'N',
							'ALLOW_MULTI_SELECT' => 'N'
						),
						null,
						array(
							'HIDE_ICONS' => 'N'
						)
					);

					$GLOBALS['APPLICATION']->IncludeComponent(
						'bitrix:main.include',
						'niyama.1.0.page-left',
						array(
							'AREA_FILE_SHOW' => 'sect',
							'AREA_FILE_SUFFIX' => 'left_inc',
							'AREA_FILE_RECURSIVE' => 'N',
							'EDIT_TEMPLATE' => ''
						),
						null,
						array(
							'HIDE_ICONS' => 'Y'
						)
					);
				?></div><?

				$GLOBALS['APPLICATION']->IncludeComponent(
					'bitrix:main.include',
					'niyama.1.0.page-right',
					array(
						'AREA_FILE_SHOW' => 'sect',
						'AREA_FILE_SUFFIX' => 'right_inc',
						'AREA_FILE_RECURSIVE' => 'N',
						'EDIT_TEMPLATE' => ''
					),
					null,
					array(
						'HIDE_ICONS' => 'Y'
					)
				);

			?></div>
		</main><?
		if (!empty($_SESSION['NIYAMA']['VIRTUAL_TUR_FILE'])) {
			$GLOBALS['APPLICATION']->IncludeComponent(
				'bitrix:main.include',
				'',
				array(
					'AREA_FILE_SHOW' => 'file',
					'PATH' => $_SESSION['NIYAMA']['VIRTUAL_TUR_FILE'],
					'EDIT_TEMPLATE' => ''
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
			unset($_SESSION['NIYAMA']['VIRTUAL_TUR_FILE']);
		} else {
			$GLOBALS['APPLICATION']->IncludeComponent(
				'bitrix:main.include',
				'',
				array(
					'AREA_FILE_SHOW' => 'sect',
					'AREA_FILE_SUFFIX' => 'after_main',
					'AREA_FILE_RECURSIVE' => 'N',
					'EDIT_TEMPLATE' => ''
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}
		?><footer class="page__footer">
			<div class="page__container clearfix">
				<div class="page__footer-left">
					<div class="social"><?
						// ссылки на соцсети
						$GLOBALS['APPLICATION']->IncludeFile(
							SITE_DIR.'include_areas/footer-social-links.php',
							array(),
							array(
								'MODE' => 'html',
								'SHOW_BORDER' => false
							)
						);
					?></div>
					<div class="footer-nav clearfix"><?
						// нижнее меню
						$GLOBALS['APPLICATION']->IncludeComponent(
							'bitrix:menu',
							'niyama.1.0.bottom',
							array(
								'ROOT_MENU_TYPE' => 'bottom',
								'CHILD_MENU_TYPE' => '',
								'MENU_CACHE_TYPE' => 'A',
								'MENU_CACHE_TIME' => '43200',
								'MENU_CACHE_USE_GROUPS' => 'N',
								'MENU_CACHE_GET_VARS' => array(),
								'MAX_LEVEL' => '1',
								'USE_EXT' => 'N',
								'ALLOW_MULTI_SELECT' => 'N'
							),
							null,
							array(
								'HIDE_ICONS' => 'Y'
							)
						);
					?></div>
					<div class="copyright">© <?=COption::GetOptionString('main', 'site_name')?>, <?=date('Y')?> - <a href="http://www.niyama.ru/">Доставка суши и роллов на дом</a></div>
					<a href="http://www.adv.ru" class="adv-logo">Сделано в ADV</a>
				</div>
				<div class="page__footer-right"><?
					// группы в соцсетях
					$GLOBALS['APPLICATION']->IncludeFile(
						SITE_DIR.'include_areas/footer-social-groups.php',
						array(),
						array(
							'MODE' => 'html',
							'SHOW_BORDER' => false
						)
					);
				?></div>
			</div>
		</footer><?

		// Форма добавления гостя
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.general-cart-guests-popup',
			array(),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		// Проверка на Модальное окно
		CNiyamaModal::initModal();

		// js веб-аналитики (GA + YA, в т.ч. добавляется код для <head>)
		$GLOBALS['APPLICATION']->IncludeFile(
			SITE_DIR.'include_areas/analytics-js.php',
			array(),
			array(
				'MODE' => 'html',
				'SHOW_BORDER' => false
			)
		);

	?>
<?
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.codes',
    array(
        'MODE' => ''
    )
);
?>

</body><?

?></html><?

if($GLOBALS['APPLICATION']->GetCurPage(false) == SITE_DIR) {
	$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_PAGE_CONTENT_CLASS', 'home-page');
}

// костыль: сохраняем адрес страницы (для кнопки назад в оформлении заказа)
$_SESSION['NIYAMA']['LAST_PAGE_URL'] = $GLOBALS['APPLICATION']->GetCurPage(false);

//
// необходимо на всех страницах, где присутствует постраничная навигация, после основного title страницы добавлять слова по следующему принципу: страница [номер текущей страницы]
//
$iPagenavPage = $GLOBALS['APPLICATION']->GetProperty('NIYAMA_PAGENAV_CUR_PAGE', 0);
if($iPagenavPage > 1) {
	$sCurTitle = trim($GLOBALS['APPLICATION']->GetTitle());
	$GLOBALS['APPLICATION']->SetTitle(trim($sCurTitle, '-').' - страница '.$iPagenavPage);
}
ob_start();
?><script type="text/javascript">
	var PagenavChangeTitle = function(iCurPageNum) {
		var sCurTitle = document.title;
		sCurTitle = sCurTitle.replace(/ - страница \d+/, '');
		if(iCurPageNum > 1) {
			sCurTitle += ' - страница ' + iCurPageNum;
		}
		document.title = sCurTitle;
	}
</script><?
$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());

if(defined('ERROR_404')) {
	CHTTP::SetStatus('404 Not Found');
}

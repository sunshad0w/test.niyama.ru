<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//IncludeTemplateLangFile(__FILE__);

// !!! включим перенос всех js-скриптов в футер
if(!defined('JS_MOVE_FOOTER_FLAG')) {
	define('JS_MOVE_FOOTER_FLAG', true);
}
define('SITE_DEFAULT_TEMPLATE_PATH', '/local/templates/.default');


?><!DOCTYPE html>
<!--[if IE 8]><html lang="ru" class="ie8 lt-ie10"><![endif]-->
<!--[if IE 9]><html lang="ru" class="ie9 lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--><html lang="ru"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width">

		<title><?
			$GLOBALS['APPLICATION']->ShowTitle();
		?></title><?

		// важен такой порядок
		$GLOBALS['APPLICATION']->ShowMeta('robots');
		$GLOBALS['APPLICATION']->ShowMeta('keywords');
		$GLOBALS['APPLICATION']->ShowMeta('description');
		$GLOBALS['APPLICATION']->ShowCSS();
		$GLOBALS['APPLICATION']->ShowHeadScripts();
		$GLOBALS['APPLICATION']->ShowHeadStrings();

		/* css */
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="stylesheet" href="'.CUtil::GetAdditionalFileURL(SITE_DEFAULT_TEMPLATE_PATH.'/static/dest/css/main.css').'">');

		/* js */
		$GLOBALS['APPLICATION']->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH.'/static/dest/js/main.js');
		$GLOBALS['APPLICATION']->AddHeadString('<!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->');

		/* vk widget */
		$GLOBALS['APPLICATION']->AddHeadString('<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>');

		// Favicon
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="icon" href="/favicon.ico" type="image/x-icon" />');
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />');?>

		<!-- Convead Widget -->
		<script>
			window.ConveadSettings = {
				<?php if($GLOBALS['USER']->IsAuthorized()) {
				?>
				 visitor_uid: '<?=trim($GLOBALS['USER']->GetID());?>',
				 visitor_info: {
				 first_name: '<?=trim($GLOBALS['USER']->GetFirstName());?>',
				 last_name: '<?=trim($GLOBALS['USER']->GetLastName());?>',
				 email: '<?=trim($GLOBALS['USER']->GetEmail());?>',
				 },
					<?php } ?>
				app_key: "8573c0e8c97f3038c6b82517e6fbc504"
				/* For more information on widget configuration please see:
				 http://convead.ru/help/kak-nastroit-sobytiya-vizitov-prosmotrov-tovarov-napolneniya-korzin-i-pokupok-dlya-vashego-sayta
				 */
			};
			(function(w,d,c){w[c]=w[c]||function(){(w[c].q=w[c].q||[]).push(arguments)};var ts = (+new Date()/86400000|0)*86400;var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.src = '//tracker.convead.io/widgets/'+ts+'/widget-8573c0e8c97f3038c6b82517e6fbc504.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})(window,document,'convead');
		</script>
		<!-- /Convead Widget -->
                <!--js-баннер-->
                <script>
                     var moscowTime = new Date(Date.now() + (new Date()).getTimezoneOffset()*60*1000 + 3*60*60*1000);
                     if (window.location.pathname == '/' && moscowTime.getHours() >= 6 && moscowTime.getHours() < 9){
                         convead('widget', 'show', {id: 6571});
                         //console.log('pixel');
                     }
                </script>
                <!--/js-баннер-->

	</head><?

	?><body class="page"><?
		?><div id="panel"><?$GLOBALS['APPLICATION']->ShowPanel()?></div><?

		?><div class="page__user-panel">
			<div class="page__container clearfix"><?

				// Мои достижения
				$GLOBALS['APPLICATION']->IncludeFile(
					SITE_DIR.'include_areas/discount-timeline.php',
					array(),
					array(
						'MODE' => 'php',
						'SHOW_BORDER' => false
					)
				);

				// Кнопки авторизации/регистрации
				?><div class="user-controls pull-right"><?
					if(!$GLOBALS['USER']->IsAuthorized()) {
						?><a href="/auth/?register=yes" class="user-controls__item">Регистрация</a><span class="user-controls__divider">|</span><a href="/auth/?backurl=<?=CProjectUtils::GetCurPageParam('', array(), true, false)?>" class="user-controls__item">Войти</a><?
					} else {
						$sTmpName = trim($GLOBALS['USER']->GetFirstName());
						$sTmpName = strlen($sTmpName) ? $sTmpName : trim($GLOBALS['USER']->GetEmail());
						?><a class="user-controls__item" href="/profile/"><?=$sTmpName?></a><span class="user-controls__divider">|</span><a class="user-controls__item" href="/?logout=yes">Выход</a><?
					}
				?></div><?

			?></div>
		</div><?

		// шапка
		?><header class="page__header">
			<div class="page__container clearfix"><?
				// логотип
				?><div class="pull-left"><?
					if($GLOBALS['APPLICATION']->GetCurPage(true) == SITE_DIR.'index.php') {
						$GLOBALS['APPLICATION']->IncludeFile(
							SITE_DIR.'include_areas/logo-home.php',
							array(),
							array(
								'MODE' => 'html',
								'SHOW_BORDER' => true
							)
						);
					} else {
						$GLOBALS['APPLICATION']->IncludeFile(
							SITE_DIR.'include_areas/logo-inner.php',
							array(),
							array(
								'MODE' => 'html',
								'SHOW_BORDER' => true
							)
						);
					}
				?></div><?

				// телефон вверху
				?><div class="pull-right"><?
					$GLOBALS['APPLICATION']->IncludeFile(
						SITE_DIR.'include_areas/tel-top.php',
						array(),
						array(
							'MODE' => 'html',
							'SHOW_BORDER' => true
						)
					);
				?></div><?

				// верхнее меню
				$GLOBALS['APPLICATION']->IncludeComponent(
					'bitrix:menu',
					'niyama.1.0.top',
					array(
						'ROOT_MENU_TYPE' => 'top',
						'CHILD_MENU_TYPE' => '',
						'MENU_CACHE_TYPE' => 'A',
						'MENU_CACHE_TIME' => '43200',
						'MENU_CACHE_USE_GROUPS' => 'N',
						'MENU_CACHE_GET_VARS' => array(),
						'MAX_LEVEL' => '1',
						'USE_EXT' => 'N',
						'ALLOW_MULTI_SELECT' => 'N'
					),
					null,
					array(
						'HIDE_ICONS' => 'N'
					)
				);
			?></div>
		 </header><?

		/* Слайдер */
		$GLOBALS['APPLICATION']->IncludeComponent(
			'bitrix:main.include',
			'niyama.1.0.slider-top',
			array(
				'AREA_FILE_SHOW' => 'page',
				'AREA_FILE_SUFFIX' => 'slider_top_inc',
				'AREA_FILE_RECURSIVE' => 'N',
				'EDIT_TEMPLATE' => ''
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		// Корзина
		$GLOBALS['APPLICATION']->IncludeComponent(
			'bitrix:main.include',
			'niyama.1.0.cart-top',
			array(
				'AREA_FILE_SHOW' => 'sect',
				'AREA_FILE_SUFFIX' => 'cart_top_inc',
				'AREA_FILE_RECURSIVE' => 'Y',
				'EDIT_TEMPLATE' => ''
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		?><main class="page__main <?$GLOBALS['APPLICATION']->ShowProperty('NIYAMA_PAGE_MAIN_ELEMENT_CLASS', 'default')?>"><?

			/* Промо-блок */
			$GLOBALS['APPLICATION']->IncludeComponent(
				'bitrix:main.include',
				'niyama.1.0.promo-block',
				array(
					'AREA_FILE_SHOW' => 'page',
					'AREA_FILE_SUFFIX' => 'promo_block_inc',
					'AREA_FILE_RECURSIVE' => 'N',
					'EDIT_TEMPLATE' => ''
				),
				null,
				array(
					'HIDE_ICONS' => 'N'
				)
			);

			?><div class="page__container clearfix"><?
				$GLOBALS['APPLICATION']->IncludeComponent(
					'bitrix:main.include',
					'niyama.1.0.page-top',
					array(
						'AREA_FILE_SHOW' => 'sect',
						'AREA_FILE_SUFFIX' => 'top_inc',
						'AREA_FILE_RECURSIVE' => 'N',
						'EDIT_TEMPLATE' => ''
					),
					null,
					array(
						'HIDE_ICONS' => 'Y'
					)
				);
				?><div class="page__content-wrap"><?
					?><div class="page__content <?$GLOBALS['APPLICATION']->ShowProperty('NIYAMA_PAGE_CONTENT_CLASS', '_inner')?>"><?
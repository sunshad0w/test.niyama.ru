<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

//echo $ITEM['PROPS']['PRODUCT_NAME'] . "<br>";

# Фото
$arAvaData = CFile::GetFileArray($ITEM['PROPS']['PREVIEW_PICTURE']);

?><div class="goods _table">
    <div class="goods__wrap">
        <div class="goods__image"><img width="188" src="<?=$arAvaData['SRC']?>"></div>
        <div class="goods__desc">
            <p class="goods__name"><?=$ITEM['PROPS']['PRODUCT_NAME']?></p>
            <div class="goods__composition"><?=$ITEM['PROPS']['ALL_INGREDIENT'] ?></div>
            <div class="select__wrap _mini js-widget pull-right" onclick="return {'select': {}}">
                <select class="select" data-id="<?=$ITEM['ID']?>">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
            </div>
            <div class="goods__cost"><?=$ITEM['PRICE']?> <span class="rouble">Р </span></div>
        </div>
    </div>
    <div class="goods__change"> </div>
    <div class="goods__remove" data-id="<?=$ITEM['ID']?>"> </div>
</div><?
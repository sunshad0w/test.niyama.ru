<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$GLOBALS['APPLICATION']->SetTitle('Востановление пароля');
$GLOBALS['APPLICATION']->AddChainItem("Востановление пароля");


$strError = '';
$strOther = '';
if($arParams['~AUTH_RESULT']['TYPE'] == 'ERROR') {
	$strError = $arParams['~AUTH_RESULT']['MESSAGE'];
} else {
	$strOther = $arParams['~AUTH_RESULT']['MESSAGE'];
}

if($strError && strpos($strError, $GLOBALS['MESS']['DATA_NOT_FOUND']) !== false) {
	$strError = 'Пользователь с таким электронным адресом не зарегистрирован';
}

if(!$GLOBALS['USER']->IsAuthorized()) {
	?><h1 class="auth__title align-center">Восстановление пароля</h1>
	<div class="auth"><?
		?><div class="auth__center">
			<form data-parsley-validate class="b-autorization-form__tag"  name="system_auth_form<?=$arResult['RND']?>" method="post" target="_top" action="<?=$arResult['AUTH_URL']?>"><?
				if(strlen($arResult['BACKURL'])) {
					?><input type="hidden" name="backurl" value="<?=$arResult['BACKURL']?>"><?
				}
				?><input type="hidden" name="AUTH_FORM" value="Y">
				<input type="hidden" name="TYPE" value="SEND_PWD">
				<input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult['USER_LOGIN']?>" placeholder="">
				<label class="label auth__label">E-mail</label>
				<input required data-parsley-type="email" type="email" class="input auth__input" value="" name="USER_EMAIL"><?
				if($strOther) {
					?><div class="auth__message"><?=$strOther?></div><?
				}
				if($strError) {
					?><div class="auth__message"><?=$strError?></div><?
				}
				?><input type="submit" value="Отправить" class="btn _style_2 auth__submit">
			</form>
		</div>
	</div><?
}

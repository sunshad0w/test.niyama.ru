<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Форма авторизации в оформлении заказа
//

//
// Авторизация через старый сайт
//
if($arResult['ERROR_MESSAGE']['TYPE'] && $arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR' && $arResult['ERROR_MESSAGE']['ERROR_TYPE'] && $arResult['ERROR_MESSAGE']['ERROR_TYPE'] == 'LOGIN') {
	// Если не прошла авторизация в текущей БД
	if(isset($_POST['USER_LOGIN']) && isset($_POST['USER_PASSWORD'])) {
		$obOldSiteAuthService = new CNiyamaOldSiteAuth();
		$sOldSiteAuthResultCode = $obOldSiteAuthService->AuthUser($_POST['USER_LOGIN'], $_POST['USER_PASSWORD'], true);
		switch($sOldSiteAuthResultCode) {
			case 'EMPTY_PARAMS':
				// не задан логин или пароль
				// оставляем штатное сообщение об ошибке
			break;

			case 'OLD_SITE_AUTH_CLOSED':
				// юзер не может авторизоваться удаленно
				// оставляем штатное сообщение об ошибке
			break;

			case 'OLD_SITE_AUTH_FAIL':
				// юзер может авторизоваться удаленно, но авторизация не удалась
				$sTmpErr = $obOldSiteAuthService->GetLastError();
				if($sTmpErr) {
					$arResult['ERROR_MESSAGE']['MESSAGE'] = 'Не удалось авторизоваться на удаленном сервисе. '.$sTmpErr.'<br>Пожалуйста, попробуйте позже или обратитесь к администрации сайта';
				} else {
					// оставляем штатное сообщение об ошибке
				}
			break;

			case 'AUTH_TRANSFER_FAIL':
				// юзер авторизовался удаленно, но не удалось перенести пароль на локальный сайт
				$sTmpErr = $obOldSiteAuthService->GetLastError();
				$arResult['ERROR_MESSAGE']['MESSAGE'] = 'Не удалось выполнить перенос аккаунта. '.$sTmpErr.' Пожалуйста, обратитесь к администрации сайта';
			break;

			case 'LOCAL_SITE_AUTH_FAIL':
				// юзер авторизовался удаленно, но не удалось авторизоваться локально
				$arResult['ERROR_MESSAGE']['MESSAGE'] = 'Во время авторизации произошел технический сбой. Пожалуйста, попробуйте авторизоваться еще раз, либо обратитесь к администрации сайта';
			break;

			case 'LOCAL_SITE_AUTH_SUCCESS':
				// юзер авторизовался удаленно и локально
				$sRedirect = isset($_REQUEST['backurl']) && strlen($_REQUEST['backurl']) ? trim($_REQUEST['backurl']) : '';
				$sRedirect = strlen($sRedirect) ? $sRedirect : '/order/confirm/';
				LocalRedirect($sRedirect);
				return;
			break;
		}
	}
}

$arRegistrationTypeItems = array();
$arTmp = CNiyamaDiscountLevels::FindRegistrationTypeItems();
foreach($arTmp as $iElementId) {
	$arRegistrationTypeItems[$iElementId] = CNiyamaDiscountLevels::GetDiscountLevelById($iElementId);
	if(!$arRegistrationTypeItems[$iElementId]) {
		unset($arRegistrationTypeItems[$iElementId]);
	}
}

?><h1 class="auth__title">Оформление заказа</h1><?
?><div class="auth _order">
	<div class="auth__left"><?

		ShowMessage($arParams['~AUTH_RESULT']);
		//ShowMessage($arResult['ERROR_MESSAGE']);

		?><label class="label"><?
			echo 'Войти через социальную сеть';
			$APPLICATION->IncludeComponent(
				'adv:social.auth', 
				'niyama.1.0', 
				array(
					'BACK_URL' => '/order/confirm/'
				), 
				null, 
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		?></label><?

		$sAddClass = '';
		if($arResult['ERROR_MESSAGE']) {
			$sAddClass = ' parsley-error';
		}

		?><form data-parsley-validate method="post" action="">
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<input type="hidden" name="step" value="auth" />

			<label class="label auth__label">Имя учётной записи</label>
			<input required class="input auth__input<?=$sAddClass?>" name="USER_LOGIN" maxlength="255" value="<?=$arResult['LAST_LOGIN']?>"><?
			if($arResult['ERROR_MESSAGE']['MESSAGE']) {
				?><div class="auth__error"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></div><?
			}
			?><a href="/auth/?forgot_password=yes" class="auth__fogot pull-right">Забыли пароль?</a>
			<label class="label auth__label">Пароль</label>
			<input required class="input auth__input<?=$sAddClass?>" type="password" name="USER_PASSWORD" maxlength="255" value="">
			<input type="submit" value="Войти" class="btn _style_2 auth__submit">
		</form><?
	?></div>
	<div class="auth__right">
		<div class="btn auth__continue" onclick="jQuery('#order-form-skip-auth').submit()">Продолжить без регистрации</div><?
		?><form id="order-form-skip-auth" method="post" action="">
			<input type="hidden" name="step" value="skip_auth" />
		</form><?

		$arTmp = $arRegistrationTypeItems ? reset($arRegistrationTypeItems) : array();
		if($arTmp) {
			?><div class="auth__bonus"><?
				$arProduct = $arTmp['BONUS_DISH'] ? CNiyamaCatalog::GetProductDetail($arTmp['BONUS_DISH']) : array();
				if($arProduct) {
					?><p>За регистрацию вы получите бонусное блюдо</p><?
					?><div class="auth__bonus-nyam"><?=$arProduct['INFO']['NAME']?></div><?
				} elseif($arTmp['NIYAM_CNT']) {
					?><p>За регистрацию мы начислим вам</p><?
					?><div class="auth__bonus-nyam"> <span class="text-brown"><?=CProjectUtils::FormatNum($arTmp['NIYAM_CNT'])?></span><img src="<?=$templateFolder.'/images/nyam.png'?>" alt=""><span class="text-orange">ниям<a href="#" class="auth__bonus-question"> <span>Бонусные баллы, которыми вы можете оплатить часть заказа</span></a></span></div><?
				} elseif($arTmp['DISCOUNT_VAL_PERC']) {
					?><p>За регистрацию вы получите скидку</p><?
					?><div class="auth__bonus-nyam"> <span class="text-brown"><?=$arTmp['DISCOUNT_VAL_PERC']?></span> <span class="text-orange">%</span></div><?
				} elseif($arTmp['DISCOUNT_VAL_RUB']) {
					?><p>За регистрацию вы получите скидку</p><?
					?><div class="auth__bonus-nyam"> <span class="text-brown"><?=CProjectUtils::FormatNum($arTmp['DISCOUNT_VAL_RUB'])?></span> <span class="text-orange">руб.</span></div><?
				}
				?><p><a href="/auth/?register=yes">Регистрация</a></p><?
			?></div><?
		}
	?></div>
</div><?

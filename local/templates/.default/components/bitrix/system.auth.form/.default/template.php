<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if(!$USER->IsAuthorized()):?>
    <div class="b-autorization-form  js-widget" onclick="return {widget: 'autorizationForm'}">
        <form class="b-autorization-form__tag" name="system_auth_form<?= $arResult["RND"] ?>" method="post"
              target="_top"
              action="/ajax/handlers.php<?= ((isset($_REQUEST["backurl"])) && (!empty($_REQUEST["backurl"]))) ? '?burl=' . $_REQUEST["backurl"] : '' ?>">
            <div class="b-autorization-form__heading">Вход на сайт</div>
            <div class="b-autorization-form__inputs-box">
                <input type="text" class="b-autorization-form__input _login" name="USER_LOGIN" maxlength="50"
                       value="<?= $arResult["USER_LOGIN"] ?>" placeholder="Логин"/>

                <div class="b-autorization-form__separator"></div>
                <input type="password" class="b-autorization-form__input _password" value="" name="USER_PASSWORD"
                       placeholder="Пароль"/>
            </div>
            <div class="b-msg _error"></div>
            <div class="b-button3 _width_full">Войти</div>
            <div class="b-autorization-form__links">
                <a class="b-autorization-form__link" href="/login?forgot_password=yes">Я не помню пароль</a>
                <a class="b-autorization-form__link" href="/login?register=yes">как стать партнером</a>
            </div>
            <input type="hidden" name="AUTH_FORM" value="Y"/>
            <input type="hidden" name="action" value="auth.form"/>
            <?= bitrix_sessid_post() ?>
            <input type="hidden" name="TYPE" value="AUTH"/>
            <? foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
            <? endforeach ?>
        </form>
    </div>
<?endif?>
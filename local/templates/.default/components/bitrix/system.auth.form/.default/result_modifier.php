<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION, $USER;
$cp = $this->__component;
$IS_AJAX_HTTP = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false;
if (is_object($cp)) {
    if (!$USER->IsAuthorized()) {
        if ((count($cp->arResult['POST']) > 0) &&
            ($cp->arResult['POST']['AUTH_FORM'] == "Y") &&
            ($cp->arResult['POST']['TYPE'] == "AUTH") &&
            ($IS_AJAX_HTTP)
        ) {
            if (count($cp->arResult['ERROR_MESSAGE']) > 0) {
                if (isset($cp->arResult['ERROR_MESSAGE']['MESSAGE'])) {
                    $APPLICATION->RestartBuffer();
                    if (!defined('PUBLIC_AJAX_MODE')) {
                        define('PUBLIC_AJAX_MODE', true);
                    }
                    header('Content-type: application/json');
                    echo json_encode(array('type' => 'error', 'msg' => strip_tags($cp->arResult['ERROR_MESSAGE']['MESSAGE'])));
                    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');
                    die();
                }
            }
        }
    } else {
        $APPLICATION->RestartBuffer();
        if (!defined('PUBLIC_AJAX_MODE')) {
            define('PUBLIC_AJAX_MODE', true);
        }
        header('Content-type: application/json');
        $redirect = false;
        if (isset($cp->arResult['GET']['burl']) && (!empty($cp->arResult['GET']['burl']))) {
            $redirect = $cp->arResult['GET']['burl'];
        }
        echo json_encode(array('type' => 'success', 'redirect' => $redirect));
        require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');
        die();
    }
}
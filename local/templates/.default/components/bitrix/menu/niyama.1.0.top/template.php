<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Верхнее меню сайта
//
//_log_array($arResult);

if(empty($arResult)) {
	return;
}

$arHiddenTabletItems = array();
?><nav onclick="return {'nav': {}}" class="nav centrifuge js-widget"><?
	$sCurUrl = $GLOBALS['APPLICATION']->GetCurPageParam('', array(), false);
	foreach($arResult as $arItem) {
		if($arItem['DEPTH_LEVEL'] == 1) {
			$sAttr = '';
			$sUrl = $arItem['PARAMS']['DIRECT_LINK'] ? $arItem['PARAMS']['DIRECT_LINK'] : $arItem['LINK'];
			if($arItem['SELECTED'] && $sUrl == $sCurUrl) {
				$sAttr .= ' onclick="window.location.href = \''.htmlspecialcharsbx($sUrl).'\'"';
				$sUrl = 'javascript:void(0)';
			}
			$sTmpId = $arItem['PARAMS']['HTML_ID'] ? ' id="'.$arItem['PARAMS']['HTML_ID'].'"' : '';
			$sTmpClass = $arItem['SELECTED'] && !strlen($sTmpId) ? ' _active' : '';
			$bIsHiddenTablet = $arItem['PARAMS']['IS_HIDDEN_TABLET'] && $arItem['PARAMS']['IS_HIDDEN_TABLET'] == 'Y';

			if($bIsHiddenTablet) {
				// класс _hidden-tablet не нужен
				$arHiddenTabletItems[] = '<a href="'.$sUrl.'"'.$sTmpId.' class="nav__item centrifuge__item'.$sTmpClass.'"'.$sAttr.'>'.$arItem['TEXT'].'</a>';
			}
			$sTmpClass .= $bIsHiddenTablet ? ' _hidden-tablet' : '';
			?><a href="<?=$sUrl?>"<?=$sTmpId?> class="nav__item centrifuge__item<?=$sTmpClass?>"<?=$sAttr?>><?=$arItem['TEXT']?></a><?
			// !!!
			echo ' ';
		}
	}
	if($arHiddenTabletItems) {
		?><span class="nav__item centrifuge__item pseudo-link _more">Еще
			<div class="nav__more"><?
				foreach($arHiddenTabletItems as $sItemHtml) {
					?><p><?=$sItemHtml?></p><?
				}
			?></div>
		</span><?
	}
?></nav><?

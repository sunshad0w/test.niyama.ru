<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();


if(empty($arResult)) {
	return;
}


$selAr=array();
$arMenu=$arResult;
foreach($arMenu as $key=>$arItem) {
	
	if ($arItem['SELECTED']==1){
		$selAr[]=$arItem;
		unset($arMenu[$key]);
		break;
	}
}

# Для ajax вызова попапа используем ссылку.
$sOrderLink = "ajax/";

?><div class="cabinet__title"><?
	foreach($selAr as $arItem) {
        ?><h2 class="h1 pull-left"><?=$arItem['TEXT']?></h2><?
    }
    foreach($arMenu as $arItem) {

        $sDopAttribute = 'class="cabinet__title-link pull-right"';
        if (substr_count($arItem['LINK'],$sOrderLink)){
            $sDopAttribute = 'class="fancybox cabinet__title-link pull-right" data-fancybox-type="ajax"';
        }

        ?><a <?= $sDopAttribute ?>  href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a><?

     }

?></div>

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Нижнее меню сайта
//
//_log_array($arResult);

if(empty($arResult)) {
	return;
}

// для авторазбивки на три колонки
$iCnt = count($arResult);
$iFirstLimit = ceil($iCnt / 3);
$iLimit = round(($iCnt - $iFirstLimit) / 2);
$iAdd = $iFirstLimit - $iLimit;
$iIdx = 0;

$sCurUrl = $GLOBALS['APPLICATION']->GetCurPageParam('', array(), false);
echo '<ul class="footer-nav__col">';
foreach($arResult as $arItem) {
	$bStartNextRow = false;
	// для авторазбивки на три колонки добавить один слэш комментария в следующей строке
	/*
	if($iIdx == ($iLimit + $iAdd)) {
		$iIdx = 0;
		$iAdd = 0;
		$bStartNextRow = true;
	}
	/*/
	if($arItem['PARAMS']['NEXT_ROW'] && $arItem['PARAMS']['NEXT_ROW'] == 'Y') {
		$bStartNextRow = true;
	}
	//*/

	if($bStartNextRow) {
		echo '</ul>';
		echo '<ul class="footer-nav__col">';
	}

	echo '<li class="footer-nav__item">';

	$sUrl = $arItem['PARAMS']['DIRECT_LINK'] ? $arItem['PARAMS']['DIRECT_LINK'] : $arItem['LINK'];
	if($arItem['SELECTED'] && $sUrl == $sCurUrl) {
		$sUrl = 'javascript:void(0)';
	}
	$sAttr = '';
	if($arItem['PARAMS']['OCTOLINE_FORM'] && $arItem['PARAMS']['OCTOLINE_FORM'] == 'Y') {
		$sAttr = ' '.CCustomProject::GetOctolineFormJsCall(' '.CCustomProject::GetAnalyticsByCode('link-back_call', ''));
		$sUrl = 'javascript:void(0)';
	} elseif($arItem['PARAMS']['SUPPORT_FORM'] && $arItem['PARAMS']['SUPPORT_FORM'] == 'Y') {
		$sAttr = ' data-fancybox-type="ajax" class="fancybox footer-nav__link" data-fancybox-href="/ajax/getSupportForm.php"';
		$sUrl = 'javascript:void(0)';
	}

	if($arItem['PARAMS']['ADD_ATTR']) {
		$sAttr .= ' '.$arItem['PARAMS']['ADD_ATTR'];
	}

	$sTmpClass = '';
	?><a href="<?=$sUrl?>"<?=$sAttr?> class="footer-nav__link<?=$sTmpClass?>"><?=$arItem['TEXT']?></a><?

	echo '</li>';
	++$iIdx;
}
echo '</ul>';

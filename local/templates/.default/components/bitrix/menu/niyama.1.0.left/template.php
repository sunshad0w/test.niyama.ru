<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Левое меню сайта
//
//_log_array($arResult);

if(empty($arResult)) {
	return;
}

$arTmp = $arResult;
$arResult = array();
$bSelect = false;
foreach($arTmp as $arItem) {
	if($arItem['DEPTH_LEVEL'] == 1) {
		$bSelect = $arItem['SELECTED'];
	}
	if($bSelect) {
		if($arItem['DEPTH_LEVEL'] > 1) {
			$arResult[] = $arItem;
		}
	}
}
if(empty($arResult)) {
	return;
}

?><ul class="nav-side"><?
	$iShowLevel = 0;
	$iPreviousLevel = 0;
	$sCurUrl = $GLOBALS['APPLICATION']->GetCurPageParam('', array(), false);
	foreach($arResult as $arItem) {
		if($arItem['DEPTH_LEVEL'] == 2) {
			$iShowLevel = 2;
		}

		if($arItem['DEPTH_LEVEL'] <= $iShowLevel) {
			if($iPreviousLevel && $arItem['DEPTH_LEVEL'] < $iPreviousLevel) {
				$iShowLevel = $arItem['DEPTH_LEVEL'];
				echo str_repeat('</ul></li>', ($iPreviousLevel - $arItem['DEPTH_LEVEL']));
			}
			$iPreviousLevel = $arItem['DEPTH_LEVEL'];

			echo '<li class="nav-side__item">';

			$sUrl = $arItem['PARAMS']['DIRECT_LINK'] ? $arItem['PARAMS']['DIRECT_LINK'] : $arItem['LINK'];
			if($arItem['SELECTED'] && $sUrl == $sCurUrl) {
				$sUrl = 'javascript:void(0)';
			}
			$sTmpId = $arItem['PARAMS']['HTML_ID'] ? ' id="'.$arItem['PARAMS']['HTML_ID'].'"' : '';
			$sTmpClass = $arItem['SELECTED'] ? ' _active' : '';
    	    ?><a href="<?=$sUrl?>"<?=$sTmpId?> class="nav-side__link<?=$sTmpClass?>"><span><?=$arItem['TEXT']?></span></a><?

			if($arItem['IS_PARENT'] && $arItem['SELECTED']) {
				$iShowLevel = $arItem['DEPTH_LEVEL'] + 1;
				echo '<ul>';
			} else {
				echo '</li>';
			}
		}
	}
	if($iPreviousLevel > 2) {
		echo str_repeat('</ul></li>', ($iPreviousLevel - 2));
	}
?></ul><?

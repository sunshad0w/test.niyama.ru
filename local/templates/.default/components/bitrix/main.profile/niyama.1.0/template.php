<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

if((!empty($arParams['ACTION_PARAM'])) && ($arParams['ACTION_PARAM'] == 'show_edit_profile')) {

	$arUserImg = array();
	if (strlen($arResult['arUser']['PERSONAL_PHOTO'])) {
		$arUserImg = CFile::ResizeImageGet($arResult['arUser']['PERSONAL_PHOTO'], array('width' => 45, 'height' => 45), BX_RESIZE_IMAGE_EXACT, false);
	}
	if (empty($arUserImg['src'])) {
		$arAvaData = CFile::GetFileArray(CUsersData::getFirstDefaultAvatar());
		$arUserImg['src'] = $arAvaData['SRC'];
	}

	?><div class="popup__user _wide js-widget" onclick="return {'cabinet': {}}"><?
		?><form autocomplete="off" method="post" name="form1" action="<?=(!empty($_REQUEST['FORM_TARGET'])) ? htmlspecialcharsbx($_REQUEST['FORM_TARGET']) : $arResult['FORM_TARGET']?>" enctype="multipart/form-data"><?
			echo $arResult['BX_SESSION_CHECK'];
			?><input type="hidden" name="lang" value="<?=LANG?>" /><?
			?><input type="hidden" name="ID" value=<?=$arResult['ID']?> /><?
			?><input type="hidden" name="LOGIN" maxlength="50" value="<?=$arResult['arUser']['LOGIN']?>" /><?
			?><input type="hidden" name="FORM_NAME" value="PROFILE_EDIT" /><?

			?><h1>Личные данные</h1><?
			?><div class="popup__user-body"><?
				?><div class="row"><?
					?><div class="popup__user-photo"><?
						?><label class="label">Фото или аватар</label>
						<div class="popup__user-pic">
							<img src="<?=$arUserImg['src']?>" alt="" />
						</div>
						<div class="input__file _photo _mini">Выбрать фото...<?
							?><input name="PERSONAL_PHOTO" type="file" class="input__file-input" id="useravatar" />
							<input name="no_resize" type="hidden"  value="Y" /><?
						?></div><?
					?></div><?
					?><div class="popup__avatars"><?
						$arFirstAvatar = CUsersData::getDefaultAvatarList();
						foreach ($arFirstAvatar as $row) {
							$pid = $row['PREVIEW_PICTURE'];
							$sChecked = $arResult['arUser']['PERSONAL_PHOTO'] == $pid ? ' checked="checked"' : '';
							$arAvaData = CFile::GetFileArray($pid);

							?><input type="radio" value="<?=$pid?>" id="cabinet_avatar<?=$pid?>" name="avatar" id="<?=$pid?>"<?=$sChecked?> class="radio" /><?
							?><label data-url="<?=$arAvaData['SRC']?>" for="cabinet_avatar<?=$pid?>" class="radio-avatar"><?
								?><img src="<?=$arAvaData['SRC']?>" class="_unactive" /><?
								?><img src="<?=$arAvaData['SRC']?>" class="_active" /><?
							?></label><?
						}
					?><input type="radio" value="" id="cabinet_avatar__user" name="avatar" class="radio" /><?
					?></div><?
				?></div><?
				?><div class="row"><?
					?><div class="col"><?
						?><label class="label">Имя</label><?
						?><input class="input" type="text" name="NAME" maxlength="50" value="<?=$arResult['arUser']['NAME']?>" /><?
						?><label class="label">Телефон (обязательно)</label><?
						?><input class="input _phone-mask" required type="text" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult['arUser']['PERSONAL_PHONE']?>" /><?
					?></div><?
					?><div class="col"><?
						?><label class="label">Фамилия</label><?
						?><input autocomplete="off" class="input" type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult['arUser']['LAST_NAME']?>" /><?
						?><label class="label">Электронная почта</label><?
						?><input type="email" class="input" name="EMAIL" maxlength="50" value="<?=$arResult['arUser']['EMAIL']?>" disabled="disabled" /><?
					?></div><?
					?><div class="col"><?
						?><label class="label">Дата рождения</label><?
						?><input autocomplete="off" class="input _birthday-mask js-widget" onclick="return {'inputDate': {}}" type="text" name="PERSONAL_BIRTHDAY" value="<?=$arResult['arUser']['PERSONAL_BIRTHDAY']?>" /><?

						?><div class="question"><?
							$GLOBALS['APPLICATION']->IncludeFile(
								SITE_DIR.'include_areas/change-email.php',
								array(),
								array(
									'MODE' => 'html',
									'SHOW_BORDER' => true
								)
							);
						?></div><?
					?></div><?

					?><div class="clearfix"></div><?
				?></div><?
				?><div class="clearfix"></div><?
				?><div class="popup__user-change-pass"> <span>Изменить пароль</span></div><?
				?><div class="row _passwords"><?
					?><div class="col"><?
						?><!--a href="/auth/?forgot_password=yes" class="auth__fogot pull-right">Забыли пароль?</a--><?
						?><label class="label">Новый пароль</label><?
						?><div onclick="return {'togglePass': {} }" class="input__wrap js-widget"><?
							?><div class="input__showpass"></div><?
							?><input class="input" type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="bx-auth-input" /><?
						?></div><?
					?></div><?
					?><div class="col"><?
						?><label class="label">Подтверждение пароля</label><?
						?><div onclick="return {'togglePass': {} }" class="input__wrap js-widget"><?
							?><div class="input__showpass"></div><?
							?><input autocomplete="off" class="input" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /><?
						?></div><?
					?></div><?
				?></div><?
				?><div class="row"><?
					?><input type="submit" name="save" value="<?=(($arResult['ID'] > 0) ? GetMessage('MAIN_SAVE') : GetMessage('MAIN_ADD'))?>" class="btn _style_4" /><?
				?></div><?
			?></div><?
		?></form><?

		?><div class="popup__crop"><?
			?><div class="fancybox-skin"><?
				?><div class="popup__crop-body"><?
					?><h1>Кадрирование фото</h1><?
					?><div class="popup__crop-notice">Для установки аватара выделите область на фотографии</div><?
					?><div class="popup__crop-img"><?
						?><img src="" alt="" /><?
					?></div><?
					?><div class="popup__crop-preview"><?
						?><h2>Предпросмотр</h2><?
						?><div><?
							?><img src=""/><?
							?><input type="hidden" value="" class="popup__crop-id"><?
							?><input type="hidden" value="" class="popup__crop-x1"><?
							?><input type="hidden" value="" class="popup__crop-y1"><?
							?><input type="hidden" value="" class="popup__crop-x2"><?
							?><input type="hidden" value="" class="popup__crop-y2"><?
						?></div><?
					?></div><?
					?><div class="clear"><?
						?><div class="row"><?
							?><input type="button" class="btn _style_4 popup__crop-submit" value="Обрезать"><?
							?><a href="javascript:;" class="popup__crop-cancel">Отменить</a><?
						?></div><?
					?></div><?
				?></div><?
				?><a href="javascript:;" class="fancybox-close popup__crop-close" title="Close"></a><?
			?></div><?
		?></div><?
	?></div><?

} else {

	if($arResult['strProfileError']) {
		ShowError($arResult['strProfileError']);
	}

	if($arResult['DATA_SAVED'] == 'Y') {
		ShowNote(GetMessage('PROFILE_DATA_SAVED'));

		if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['FORM_NAME'] && $_POST['FORM_NAME'] == 'PROFILE_EDIT') {
			localredirect(CProjectUtils::GetCurPageParam('', array(), true, false));
		}
	}

	if($arResult['ID'] > 0) {
		$arUserImg = array();
		if (strlen($arResult['arUser']['PERSONAL_PHOTO'])) {
			$arUserImg = CFile::ResizeImageGet($arResult['arUser']['PERSONAL_PHOTO'], array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT, false);
		}
		if (empty($arUserImg['src'])) {
			$arAvaData = CFile::GetFileArray(CUsersData::getFirstDefaultAvatar());
			$arUserImg['src'] = $arAvaData['SRC'];
		}

		$arOrderInfo = CNiyamaCart::getUserOrderInfo();

		$sTmpOrderValCnt = CProjectUtils::PrintCardinalNumberRus(intval($arOrderInfo['ORDER_COUNT']), 'заказов', 'заказ', 'заказа');
		$sTmpProductValCnt = CProjectUtils::PrintCardinalNumberRus(intval($arOrderInfo['PRODUCT_COUNT']), 'блюд', 'блюдо', 'блюда');

		$sTextOrderInfo = 'Всего '.$arOrderInfo['ORDER_COUNT'].' '.$sTmpOrderValCnt.' на '.$arOrderInfo['PRODUCT_COUNT'].' '.$sTmpProductValCnt;

		?><div class="cabinet__user">
			<div class="cabinet__user-card">
				<a href="/ajax/get_user_profile.php?FORM_TARGET=<?=$arResult['FORM_TARGET']?>" data-fancybox-type="ajax" class="cabinet__edit fancybox"></a>
				<div class="cabinet__user-img"><img src="<?=$arUserImg['src']?>" alt=""></div>
				<div class="cabinet__user-info">
					<div class="cabinet__user-name"><?=$arResult['arUser']['NAME']?></div>
					<div class="cabinet__user-tel"><?=$arResult['arUser']['PERSONAL_PHONE']?></div>
					<div class="cabinet__user-email"><?=$arResult['arUser']['EMAIL']?></div>
				</div>
			</div>
			<div class="cabinet__orders">
				<a class="fancybox" href="/ajax/getProfileOrderList.php" data-fancybox-type="ajax"><?=$sTextOrderInfo?></a>
			</div>
		</div><?
	}
}

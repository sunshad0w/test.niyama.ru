<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult['ITEMS']) > 0) :

    ?><div onclick="return {'promo': {}}" class="promo js-widget">
    <div class="page__container clearfix">
        <div class="promo__slider"><?

            foreach ($arResult['ITEMS'] as $item):

                ?><div><?
                echo ( !empty($item['CODE']) ) ? '<a target="_blank" href="'.$item['CODE'].'">' :"";
                ?><img src="<?= $item['PREVIEW_PICTURE']['SRC']; ?>"><?
                echo ( !empty($item['CODE']) ) ? '</a>' :"";
                ?></div><?

            endforeach;


        ?></div><?
    ?></div>
    </div><?

endif;
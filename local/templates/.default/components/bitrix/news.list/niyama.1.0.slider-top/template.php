<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult['ITEMS']) > 0) :

?><div class="page__slider">
    <div class="page__container">
        <div onclick="return {mainSlider: {}}" class="main-slider js-widget">
            <div class="main-slider__slider"><?

                foreach ($arResult['ITEMS'] as $item):

                     ?><div class="main-slider__slider-item"><?
                        echo ( !empty($item['CODE']) ) ? '<a target="_blank" href="'.$item['CODE'].'">' :"";
                        ?><img src="<?= $item['PREVIEW_PICTURE']['SRC']; ?>"><?
                        echo ( !empty($item['CODE']) ) ? '</a>' :"";
                    ?></div><?

                endforeach;

            ?></div>
        </div>
    </div>
</div><?

endif;

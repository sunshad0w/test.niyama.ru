<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

// Перенаправляет на авторизацию если мы не на странице авторизации
if(strpos($GLOBALS['APPLICATION']->GetCurPage(false), '/auth/') !== 0) {
	LocalRedirect('/auth/?backurl='.$GLOBALS['APPLICATION']->GetCurPage(false));
}

//
// Авторизация через старый сайт
//
if($arParams['AUTH_RESULT']['TYPE'] && $arParams['AUTH_RESULT']['TYPE'] == 'ERROR' && $arParams['AUTH_RESULT']['ERROR_TYPE'] && $arParams['AUTH_RESULT']['ERROR_TYPE'] == 'LOGIN') {
	// Если не прошла авторизация в текущей БД
	if(isset($_POST['USER_LOGIN']) && isset($_POST['USER_PASSWORD'])) {
		$obOldSiteAuthService = new CNiyamaOldSiteAuth();
		$sOldSiteAuthResultCode = $obOldSiteAuthService->AuthUser($_POST['USER_LOGIN'], $_POST['USER_PASSWORD'], true);
		switch($sOldSiteAuthResultCode) {
			case 'EMPTY_PARAMS':
				// не задан логин или пароль
				// оставляем штатное сообщение об ошибке
			break;

			case 'OLD_SITE_AUTH_CLOSED':
				// юзер не может авторизоваться удаленно
				// оставляем штатное сообщение об ошибке
			break;

			case 'OLD_SITE_AUTH_FAIL':
				// юзер может авторизоваться удаленно, но авторизация не удалась
				$sTmpErr = $obOldSiteAuthService->GetLastError();
				if($sTmpErr) {
					$arParams['~AUTH_RESULT']['MESSAGE'] = 'Не удалось авторизоваться на удаленном сервисе. '.$sTmpErr.'<br>Пожалуйста, попробуйте позже или обратитесь к администрации сайта';
				} else {
					// оставляем штатное сообщение об ошибке
				}
			break;

			case 'AUTH_TRANSFER_FAIL':
				// юзер авторизовался удаленно, но не удалось перенести пароль на локальный сайт
				$sTmpErr = $obOldSiteAuthService->GetLastError();
				$arParams['~AUTH_RESULT']['MESSAGE'] = 'Не удалось выполнить перенос аккаунта. '.$sTmpErr.' Пожалуйста, обратитесь к администрации сайта';
			break;

			case 'LOCAL_SITE_AUTH_FAIL':
				// юзер авторизовался удаленно, но не удалось авторизоваться локально
				$arParams['~AUTH_RESULT']['MESSAGE'] = 'Во время авторизации произошел технический сбой. Пожалуйста, попробуйте авторизоваться еще раз, либо обратитесь к администрации сайта';
			break;

			case 'LOCAL_SITE_AUTH_SUCCESS':
				// юзер авторизовался удаленно и локально
				$sRedirect = isset($_REQUEST['backurl']) && strlen($_REQUEST['backurl']) ? trim($_REQUEST['backurl']) : '';
				$sRedirect = strlen($sRedirect) ? $sRedirect : SITE_DIR;
				LocalRedirect($sRedirect);
				return;
			break;
		}
	}
}


$arResult['LAST_LOGIN'] = isset($_POST['USER_LOGIN']) ? htmlspecialcharsbx($_POST['USER_LOGIN']) : $arResult['LAST_LOGIN'];

?><h1 class="auth__title align-center">Вход в личный кабинет</h1>
<div class="auth">
	<div class="auth__left">
		<form data-parsley-validate name="form_auth" method="post" target="_top" action="<?=$arResult['AUTH_URL']?>">
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" /><?
			if(isset($_REQUEST['backurl'])) {
				?><input type="hidden" name="backurl" value="<?= $_REQUEST['backurl'] ?>" /><?
			}
			foreach ($arResult['POST'] as $key => $value) {
				?><input type="hidden" name="<?=$key?>" value="<?=$value?>" /><?
			}

			?><label class="label auth__label">E-mail</label><?
			$sClass = '';
			if(!empty($arResult['ERROR_MESSAGE'])) {
				$sClass = ' parsley-error';
			}
			if($err_txt = ADV_get_flashdata('auth_error')) {
				$sClass = ' parsley-error';
			}
			if($err_txtAUTH_MESSAGE = ADV_get_flashdata('AUTH_MESSAGE')) {
				$sClass = ' parsley-error';
			}
			if(!empty($arParams['~AUTH_RESULT']['MESSAGE'])) {
				$sClass = ' parsley-error';
			}

			?><input tabindex="1" required class="input auth__input<?=$sClass?>" name="USER_LOGIN" maxlength="255" value="<?=$arResult['LAST_LOGIN']?>" /><?

			if(!empty($arResult['ERROR_MESSAGE'])) {
				?><div class="auth__error"><?
					echo implode('<br>', $arResult['ERROR_MESSAGE']);
				?></div><?
			}
			if($err_txt = ADV_get_flashdata('auth_error')) {
				?><div class="auth__error"><?
					echo $err_txt;
				?></div><?
			}
			if($err_txtAUTH_MESSAGE = ADV_get_flashdata('AUTH_MESSAGE')) {
				?><div class="auth__error"><?
					echo $err_txtAUTH_MESSAGE;
				?></div><?
			}
			if(!empty($arParams['~AUTH_RESULT']['MESSAGE'])) {
				?><div class="auth__error"><?
					echo $arParams['~AUTH_RESULT']['MESSAGE'];
				?></div><?
			}

			?><a href="/auth/?forgot_password=yes" class="auth__fogot pull-right">Забыли пароль?</a>
			<label class="label auth__label">Пароль</label>
			<input tabindex="2" required  class="input auth__input" type="password" name="USER_PASSWORD" maxlength="255" />
			<input type="submit" value="Войти" class="btn _style_2 auth__submit" />
		</form>
	</div>
	<div class="auth__right">
		<label class="label">Войти через социальную сеть</label><?
		$APPLICATION->IncludeComponent(
			'adv:social.auth', 
			'niyama.1.0', 
			array(
			),
			null, 
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		?>У вас еще нет учетной записи? &mdash; <a href="/auth/?register=yes">Зарегистрируйтесь</a>
	</div>
</div><?

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

// delayed function must return a string
if(empty($arResult)) {
    return '';
}

global $APPLICATION;

$sReturn = '<div class="breadcrumbs"><a class="breadcrumbs__back" href="javascript:history.back()" onMouseOver="return true">Назад</a>';


$sReturn .= '<ul class="breadcrumbs__list">';
foreach ($arResult as $i => $arItem) {

    $sTag = !empty($arItem['LINK']) ? '<a href="'.$arItem['LINK'].'">' : '';
    $eTag = !empty($arItem['LINK']) ? '</a>' : '';
    $lClass = ($i < count($arResult))? '' : '_last';
    $sReturn .= ' <li class="breadcrumbs__list-item '. $lClass .' ">'.$sTag.$arItem['TITLE'].$eTag.'</li>';
}

$sReturn .= '</ul>';

$sReturn .= '</div>';

return $sReturn;
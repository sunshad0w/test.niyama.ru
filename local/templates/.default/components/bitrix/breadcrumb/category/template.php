<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
array_pop($arResult);
// delayed function must return a string
if(empty($arResult)) {
    return '';
}

global $APPLICATION;

$sReturn = '<div  class="breadcrumbs pull-left _category"><a class="breadcrumbs__back" href="javascript:history.back()" onMouseOver="return true">Назад</a>';


/*if(empty($arResult)) {*/
    $sReturn .= '<ul class="breadcrumbs__list">';
    foreach ($arResult as $i => $arItem) {
    
        $sTag = !empty($arItem['LINK']) ? '<a href="'.$arItem['LINK'].'">' : '';
        $eTag = !empty($arItem['LINK']) ? '</a>' : '';
        $lClass = ($i < count($arResult))? '' : '_last';
        $sReturn .= ' <li class="breadcrumbs__list-item '. $lClass .' ">'.$sTag.$arItem['TITLE'].$eTag.'</li>';
    }
    $sReturn .= '</ul>';
/*}*/
/*
if (isset($_REQUEST['ings'])&&(!empty($_REQUEST['ings']))){
	$sReturn .= '<div class="catalog__filters-act">';
	foreach ( $_REQUEST['ings'] as $ing ){
		$sReturn .= '<div data-filter="'.$ing.'">'.$ing.'<a href="#" class="catalog__filter-del">x</a></div>';
	}
	$sReturn .= '&nbsp;</div>';
}
*/
$sReturn .= '</div>';

return $sReturn;
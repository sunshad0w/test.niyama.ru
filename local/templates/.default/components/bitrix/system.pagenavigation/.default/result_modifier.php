<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$arSefParams = CProjectUtils::GetSefParams();
$arExcept = array_merge($arSefParams, array('PHPSESSID', 'logout', 'login', 'logout_butt', 'back_url_admin', 'print', 'bitrix_include_areas', 'show_page_exec_time', 'show_include_exec_time', 'show_sql_stat', 'bitrix_show_mode', 'clear_cache'));

$arExcept[] = 'PAGEN_'.$arResult['NavNum'];
$arExcept[] = 'SIZEN_'.$arResult['NavNum'];
$arExcept[] = 'SHOWALL_'.$arResult['NavNum'];
$arResult['NavQueryString'] = htmlspecialchars(DeleteParam($arExcept));

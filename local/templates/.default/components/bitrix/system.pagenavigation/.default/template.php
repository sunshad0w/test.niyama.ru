<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$bReturn = false;
if(!$arResult['NavShowAlways'] || $arResult['NavShowAlways'] == 'N') {
	if($arResult['NavRecordCount'] == 0 || ($arResult['NavPageCount'] == 1 && $arResult['NavShowAll'] == false)) {
		$bReturn = true;
	}
}

if($bReturn) {
	return;
}

$sNavQueryString = !empty($arResult['NavQueryString']) ? '?'.$arResult['NavQueryString'].'&amp;PAGEN_'.$arResult['NavNum'] : '?PAGEN_'.$arResult['NavNum'];
$sNavQueryStringFull = !empty($arResult['NavQueryString']) ? '?'.$arResult['NavQueryString'] : '';
$sNavQueryStringShowAll = !empty($arResult['NavQueryString']) ? '?'.$arResult['NavQueryString'].'&amp;SHOWALL_'.$arResult['NavNum'] : '?SHOWALL_'.$arResult['NavNum'];
$sQSign = '=';

$arResult['PAGES'] = array();

if($arResult['bDescPageNumbering'] === true) {
	//
} else {

	// первая страница
	$sActive = 'N';
	$iTmpPageNum = 1;
	$sUrl = $arResult['bSavePage'] ? $arResult['sUrlPath'].$sNavQueryString.$sQSign.$iTmpPageNum : $arResult['sUrlPath'].$sNavQueryStringFull;
	if($arResult['NavPageNomer'] <= $iTmpPageNum) {
		$sActive = 'Y';
	}
	$arResult['PAGES'][] = array(
		'LINK' => $sUrl,
		'ACTIVE' => $sActive,
		'NUM' => $iTmpPageNum
	);

	if($arResult['NavPageCount'] > 1) {
		// берем влево 3 страницы
		$iTmpPageNum = $arResult['NavPageNomer'] - 1;
		$iLimitPageNum = $arResult['NavPageNomer'] - 3;
		$arTmp = array();
		while($iTmpPageNum > 1 && $iTmpPageNum >= $iLimitPageNum && $iTmpPageNum < $arResult['NavPageCount']) {
			$sActive = 'N';
			$sUrl = $arResult['sUrlPath'].$sNavQueryString.$sQSign.$iTmpPageNum;
			$arTmp[] = array(
				'LINK' => $sUrl,
				'ACTIVE' => $sActive,
				'NUM' => $iTmpPageNum
			);
			--$iTmpPageNum;
		}
		if($arTmp) {
			$arTmp = array_reverse($arTmp);
			$arResult['PAGES'] = array_merge($arResult['PAGES'], $arTmp);
		}

		// текущая страница
		if($arResult['NavPageNomer'] > 1 && $arResult['NavPageNomer'] < $arResult['NavPageCount']) {
			$sActive = 'Y';
			$iTmpPageNum = $arResult['NavPageNomer'];
			$sUrl = $arResult['sUrlPath'].$sNavQueryString.$sQSign.$iTmpPageNum;
			$arResult['PAGES'][] = array(
				'LINK' => $sUrl,
				'ACTIVE' => $sActive,
				'NUM' => $iTmpPageNum
			);
		}

		// берем вправо 3 страницы
		$iTmpPageNum = $arResult['NavPageNomer'] + 1;
		$iLimitPageNum = $arResult['NavPageNomer'] + 3;
		while($iTmpPageNum < $arResult['NavPageCount'] && $iTmpPageNum <= $iLimitPageNum) {
			$sActive = 'N';
			$sUrl = $arResult['sUrlPath'].$sNavQueryString.$sQSign.$iTmpPageNum;
			$arResult['PAGES'][] = array(
				'LINK' => $sUrl,
				'ACTIVE' => $sActive,
				'NUM' => $iTmpPageNum
			);
			++$iTmpPageNum;
		}

		// последняя страница
		$sActive = 'N';
		$iTmpPageNum = $arResult['NavPageCount'];
		$sUrl = $arResult['sUrlPath'].$sNavQueryString.$sQSign.$iTmpPageNum;
		if($arResult['NavPageNomer'] >= $iTmpPageNum) {
			$sActive = 'Y';
		}
		$arResult['PAGES'][] = array(
			'LINK' => $sUrl,
			'ACTIVE' => $sActive,
			'NUM' => $iTmpPageNum
		);
	}
}

$sPrevUrl = '';
$sNextUrl = '';
$bBreak = false;
foreach($arResult['PAGES'] as $arItem) {
	if($bBreak) {
		$sNextUrl = $arItem['LINK'];
		break;
	}
	if($arItem['ACTIVE'] == 'Y') {
		$bBreak = true;
	} else {
		$sPrevUrl = $arItem['LINK'];
	}
}

?><div class="pagination"><?
	?><div class="pagination__arrows"><?
		if($sPrevUrl) {
			?><span class="pagination__prev"><a href="<?=$sPrevUrl?>">предыдущая</a></span><?
		} else {
			?><span class="pagination__prev">предыдущая</span><?
		}
		?><span class="pagination__separator">|</span><?
		if($sNextUrl) {
			?><span class="pagination__next"><a href="<?=$sNextUrl?>">следующая</a></span><?
		} else {
			?><span class="pagination__next">следующая</span><?
		}
	?></div><?

	$iLastIdx = count($arResult['PAGES']) - 1; 
	foreach($arResult['PAGES'] as $iIdx => $arItem) {
		$bShow = true;
		if($iIdx == 1) {
			if(($arResult['PAGES'][0]['NUM'] + 1) != $arItem['NUM']) {
				$bShow = false;
			}
		} elseif($iIdx == ($iLastIdx - 1)) {
			if(($arResult['PAGES'][$iLastIdx]['NUM'] - 1) != $arItem['NUM']) {
				$bShow = false;
			}
		}
		if(!$bShow) {
			?><span class="pagination__dots">...</span><?
		} else {
			if($arItem['ACTIVE'] == 'Y') {
				?><a href="javascript:void(0)" class="pagination__item _active"><?
					echo $arItem['NUM'];
				?></a><?
			} else {
				?><a href="<?=$arItem['LINK']?>" class="pagination__item"><?
					echo $arItem['NUM'];
				?></a><?
			}
		}
	}
?></div><?

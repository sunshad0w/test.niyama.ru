<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Веб-форма "Контроль качества"
//

echo $arResult['arForm']['DESCRIPTION'];

?><div class="control-form js-widget" onclick="return {'controlForm': {}}"><?
	?><div class="control-form__side"><?
		$GLOBALS['APPLICATION']->IncludeFile(
			SITE_DIR.'include_areas/qa-form-right.php',
			array(),
			array(
				'MODE' => 'html',
				'SHOW_BORDER' => true
			)
		);
	?></div><?
	?><div class="control-form__main"><?
		?><h2>Заявка в электронном виде</h2><?

		if($arResult['isFormErrors'] == 'Y') {
//			echo $arResult['FORM_ERRORS_TEXT'];
		}

		if($arResult['FORM_ERRORS']) {
			foreach($arResult['FORM_ERRORS'] as $sTmpCode => $sErrMsg) {
				if(strpos($arResult['FORM_ERRORS'][$sTmpCode], 'Не заполнены следующие обязательные поля:') === 0) {
					$arResult['FORM_ERRORS'][$sTmpCode] = 'Поле обязательно для заполнения';
				}
			}
		}

		if($arResult['isFormNote'] == 'Y') {
			// уведомление об успешном отправлении
			$GLOBALS['APPLICATION']->IncludeFile(
				SITE_DIR.'include_areas/qa-form-success.php',
				array(),
				array(
					'MODE' => 'html',
					'SHOW_BORDER' => true
				)
			);

			if($_REQUEST['RESULT_ID'] && intval($_REQUEST['RESULT_ID']) > 0) {
				$_REQUEST['RESULT_ID'] = intval($_REQUEST['RESULT_ID']);
				if($_REQUEST['RESULT_ID'] > 0 && !$_SESSION['_NIYAMA_FORMS_RESULT_'][$_REQUEST['RESULT_ID']] && strlen($arResult['FORM_NOTE'])) {
					$_SESSION['_NIYAMA_FORMS_RESULT_'][$_REQUEST['RESULT_ID']] = $_REQUEST['RESULT_ID'];
					?><script type="text/javascript">
						jQuery(document).ready(
							function() {
								<?=CCustomProject::GetAnalyticsByCode('control-forma_otpravlena', '')?>;
							}
						);
					</script><?
				}
			}

		} else {

			$arResult['FORM_HEADER'] = str_replace('<form ', '<form data-parsley-validate ', $arResult['FORM_HEADER']);
			echo $arResult['FORM_HEADER'];

			$sTmpCode = 'QA_NAME';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?

				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="input"', 'class="input parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			$sTmpCode = 'QA_MESSAGE';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<textarea ', '<textarea required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?

				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="textarea"', 'class="textarea parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			$sTmpCode = 'QA_NOCENT';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<select ', '<select required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
				?><div onclick="return {'select': {}}" class="select__wrap js-widget"><?

					if($arResult['FORM_ERRORS'][$sTmpCode]) {
						$sTmp = str_replace('class="inputselect"', 'class="select parsley-error" data-placeholder="Укажите службу доставки или ресторан"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					} else {
						$sTmp = str_replace('class="inputselect"', 'class="select" data-placeholder="Укажите службу доставки или ресторан"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					}

					// добавим пустую опцию, чтобы появилось "Укажите службу доставки или ресторан"
					$iPos = strpos($sTmp, '>');
					if($iPos) {
						$sTmp = substr($sTmp, 0, ($iPos + 1)).'<option value=""></option>'.substr($sTmp, ($iPos + 1));
					}

					echo $sTmp;
				?></div><?
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			$sTmpCode = 'QA_DATE';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?

				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="input"', 'class="input parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			$sTmpCode = 'QA_TEL';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="input"', 'class="input parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			$sTmpCode = 'QA_EMAIL';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}
				$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace(' type="text" ', ' type="email" ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);

				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="input"', 'class="input parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				}
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				if($arResult['FORM_ERRORS'][$sTmpCode]) {
					?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
					unset($arResult['FORM_ERRORS'][$sTmpCode]);
				}
			}

			if($arResult['isUseCaptcha'] == 'Y') {
				?><div style="margin-top: 42px;"><?
					?><label class="label _lite">Введите символы на картинке</label><?
					?><div><?
						?><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" /><?
						?><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" width="180" height="40" /><?
						?><input required type="text" name="captcha_word" size="30" maxlength="50" value="" class="input" /><?
					?></div><?
				?></div><?
			}

			?><input <?=CCustomProject::GetAnalyticsByCode('control-knopka_otpravit')?> name="web_form_submit" type="submit" value="Отправить" class="btn _style_1"><label class="label _lite _smaller">* - поля, обязательные для заполнения</label><?

			echo $arResult['FORM_FOOTER'];
		}
	?></div><?
	?><div style="clear: both;"></div><?
?></div><?

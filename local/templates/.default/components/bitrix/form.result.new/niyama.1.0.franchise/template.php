<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Веб-форма отправки франшизы
//
//_log_array($arResult);

?><div class="vacancies-form js-widget" onclick="return {'vacanciesForm': {}}"><?

	if($arResult['isFormErrors'] == 'Y') {
		echo $arResult['FORM_ERRORS_TEXT'];
	}

	if($arResult['isFormNote'] == 'Y') {
		?><p>Ваша заявка успешно отправлена, мы свяжемся с вами</p><?
	} else {
		$sTmp = str_replace('<form', '<form data-parsley-validate class="form"', $arResult['FORM_HEADER']);
		echo $sTmp;
		echo $arResult['arForm']['DESCRIPTION'];


		$sTmpCode = 'FRANCHISE_NAME';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?><label class="label vacancies-form__label _top">Представьтесь</label><?

			?><div class="vacancies-form__fields"><?
			?> <div class="vacancies-form__col"><?
				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?				
				$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				echo $sTmp;
			?></div><?
		}

		$sTmpCode = 'FRANCHISE_TEL';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?> <div class="vacancies-form__col"><?
				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
			?></div><?
		}

		$sTmpCode = 'FRANCHISE_EMAIL';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?> <div class="vacancies-form__col"><?
				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
				//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				echo $sTmp;
			?></div><?
		}

		$sTmpCode = 'FRANCHISE_CITY';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?> <div class="vacancies-form__col"><?
				?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
			?></div><?
		}

		if($arResult['isUseCaptcha'] == 'Y') {
			?><div style="margin-top: 42px;"><?
				?> <div class="vacancies-form__col"><?
					?><label class="label">Введите символы на картинке</label><?
					?><div><?
						?><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" /><?
						?><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" width="180" height="40" /><?
						?><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="input _mb_1" /><?
					?></div><?
				?></div><?
			?></div><?
		}

		?><div class="vacancies-form__col"><?
			?><input name="web_form_submit" type="submit" value="Отправить" class="btn _style_1 vacancies-form__submit"><?
		?></div><?
		?></div><?

		echo $arResult['FORM_FOOTER'];
	}
?></div><?

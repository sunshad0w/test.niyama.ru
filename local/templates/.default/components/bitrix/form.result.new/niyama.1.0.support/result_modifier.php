<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$iCurUserId = 0;
$arResult['arUserData'] = array();
if($GLOBALS['USER']->IsAuthorized()) {
	$iCurUserId = $GLOBALS['USER']->GetId();
	$arResult['arUserData'] = CUsersData::GetDataUserForLK($iCurUserId);
}


$arResult['SUPPORT_ADMIN_MSG_VALUE'] = '';

//
// обращения по идентификации дисконтных карт через тех.поддержку
//
if($arResult['arUserData']) {
	if(isset($_REQUEST['cn']) && strlen(trim($_REQUEST['cn']))) {
		$arFormErrors = array();
		$sCardNum = trim($_REQUEST['cn']);
		// проверим, чтобы у юзера не было активных карт
		$arCurUserCard = CNiyamaIBlockCardData::GetCardData($iCurUserId, true, true);
		if($arCurUserCard) {
			if($arCurUserCard['ACTIVE'] == 'Y') {
				$arFormErrors['CARD_ALREADY_ACTIVE'] = 'Уже активирована карта под другим номером';
			} elseif($arCurUserCard['PROPERTY_CARDNUM_VALUE'] != $sCardNum) {
				$arFormErrors['WRONG_CARD'] = 'Номера зарегистрированной карты и активируемой не совпадают';
			}
		}

		if(!$arFormErrors) {
			// проверяем, закреплен ли уже за кем-нибудь введенный номер карты
			$arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sCardNum);
			if(!$arPdsCardInfo || $arPdsCardInfo['_ERRORS_'] || !$arPdsCardInfo['Account']) {
				$arFormErrors['UNKNOWN_ERROR'] = 'Карта не зарегистрирована, либо произошла неизвестная ошибка';
			}
		}

		if(!$arFormErrors) {
			$arTmp = isset($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) && is_array($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) ? $_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'] : array();
			$arTmp[] = $sCardNum;
			$arTmp = array_unique($arTmp);
			$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Обращение с форм активации карт.'."\n";
			$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Cписок номеров карт, которые пытались активировать: '.implode(', ', $arTmp);

			if($arResult['isFormNote'] == 'Y') {
				// создаем запись очереди идентификации карты (в ручном режиме)
				CNiyamaDiscountCard::SetCardIdentifyData($iCurUserId, $sCardNum, true);
			}
		}
	} elseif(isset($_REQUEST['ci']) && strlen(trim($_REQUEST['ci']))) {
		$sFormType = trim($_REQUEST['ci']);
		$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Обращение с форм активации карт.'."\n";
		switch($sFormType) {
			case 'check_card_num':
				$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Перенаправлено с формы проверки карты'."\n";
			break;

			case 'new_card_info':
				$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Перенаправлено с формы регистрации новой карты.'."\n";
			break;

			case 'card_confirm':
				$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Перенаправлено с формы идентификации владельца карты.'."\n";
			break;

			case 'edit_card_info':
				$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Перенаправлено с формы редактирования карты.'."\n";
			break;
		}
		$arTmp = isset($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) && is_array($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) ? $_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'] : array();
		if(isset($_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM']) && strlen($_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'])) {
			$arTmp[] = $_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'];
		}
		$arTmp = array_unique($arTmp);
		$arResult['SUPPORT_ADMIN_MSG_VALUE'] .= 'Cписок номеров карт, которые пытались активировать: '.implode(', ', $arTmp);
	}
}

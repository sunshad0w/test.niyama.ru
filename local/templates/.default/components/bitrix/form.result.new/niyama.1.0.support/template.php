<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Веб-форма отправки обращения в тех.поддержку
//
//_log_array($arResult);


?><div onclick="return {'cardCheck': {}}" class="js-widget ordering-form popup__card"><?
	?><h1>Обращение в поддержку сайта</h1><?
	echo '<div class="popup__user-body">';

	if($arResult['isFormErrors'] == 'Y') {
		echo $arResult['FORM_ERRORS_TEXT'];
	}

	if($arResult['isFormNote'] == 'Y') {
		?><p>Ваше обращение будет рассмотрено и обработано в течение двух дней</p><?
	} else {
		$sTmp = str_replace('<form', '<form data-parsley-validate class="form"', $arResult['FORM_HEADER']);
		echo $sTmp;
		echo $arResult['arForm']['DESCRIPTION'];

		$arDefaultValues = array(
			'SUPPORT_NAME' => '',
			'SUPPORT_LAST_NAME' => '',
			'SUPPORT_TEL' => '',
			'SUPPORT_EMAIL' => '',
			'SUPPORT_MESSAGE' => '',
		);
		if($arResult['arUserData']) {
			$arDefaultValues['SUPPORT_NAME'] = $arResult['arUserData']['NAME'];
			$arDefaultValues['SUPPORT_LAST_NAME'] = $arResult['arUserData']['LAST_NAME'];
			$arDefaultValues['SUPPORT_TEL'] = $arResult['arUserData']['PHONE'];
			$arDefaultValues['SUPPORT_EMAIL'] = $arResult['arUserData']['EMAIL'];
		}
		
		// скрытое поле для служебной информации
		$sTmpCode = 'SUPPORT_ADMIN_MSG';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
			?><input type="hidden" name="<?='form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID']?>" value="<?=htmlspecialcharsbx($arResult['SUPPORT_ADMIN_MSG_VALUE'])?>" /><?
		}

		?><div class="ordering-form__row"><?
			?><div class="input__wrap"><?
				$sTmpCode = 'SUPPORT_NAME';
				if($arResult['QUESTIONS'][$sTmpCode]) {
					?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
					//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
					//$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					//echo $sTmp;
					$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
					$sTmpFieldName = 'form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID'];
					$sVal = isset($arResult['arrVALUES'][$sTmpFieldName]) ? $arResult['arrVALUES'][$sTmpFieldName] : $arDefaultValues[$sTmpCode];
					?><input required class="input" type="text" name="<?=$sTmpFieldName?>" value="<?=htmlspecialcharsbx($sVal)?>" /><?
				}
			?></div><?
			echo ' '; // !!!
			?><div class="input__wrap"><?
				$sTmpCode = 'SUPPORT_LAST_NAME';
				if($arResult['QUESTIONS'][$sTmpCode]) {
					?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
					//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
					//$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					//echo $sTmp;
					$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
					$sTmpFieldName = 'form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID'];
					$sVal = isset($arResult['arrVALUES'][$sTmpFieldName]) ? $arResult['arrVALUES'][$sTmpFieldName] : $arDefaultValues[$sTmpCode];
					?><input required class="input" type="text" name="<?=$sTmpFieldName?>" value="<?=htmlspecialcharsbx($sVal)?>" /><?
				}
			?></div><?
		?></div><?

		?><div class="ordering-form__row"><?
			?><div class="input__wrap"><?
				$sTmpCode = 'SUPPORT_TEL';
				if($arResult['QUESTIONS'][$sTmpCode]) {
					?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
					//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
					//$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					//echo $sTmp;
					$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
					$sTmpFieldName = 'form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID'];
					$sVal = isset($arResult['arrVALUES'][$sTmpFieldName]) ? $arResult['arrVALUES'][$sTmpFieldName] : $arDefaultValues[$sTmpCode];
					?><input required class="input" type="text" name="<?=$sTmpFieldName?>" value="<?=htmlspecialcharsbx($sVal)?>" /><?
				}
			?></div><?
			echo ' '; // !!!
			?><div class="input__wrap"><?
				$sTmpCode = 'SUPPORT_EMAIL';
				if($arResult['QUESTIONS'][$sTmpCode]) {
					?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
					//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
					//$sTmp = str_replace('<input', '<input required', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					//echo $sTmp;
					$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
					$sTmpFieldName = 'form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID'];
					$sVal = isset($arResult['arrVALUES'][$sTmpFieldName]) ? $arResult['arrVALUES'][$sTmpFieldName] : $arDefaultValues[$sTmpCode];
					?><input required class="input" type="text" name="<?=$sTmpFieldName?>" value="<?=htmlspecialcharsbx($sVal)?>" /><?
				}
			?></div><?
		?></div><?

		?><div class="ordering-form__row"><?
			$sTmpCode = 'SUPPORT_MESSAGE';
			if($arResult['QUESTIONS'][$sTmpCode]) {
				?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?

				//if($arResult['FORM_ERRORS'][$sTmpCode]) {
				//	$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('class="textarea"', 'class="textarea parsley-error"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
				//}
				//echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
				//if($arResult['FORM_ERRORS'][$sTmpCode]) {
				//	?><div class="auth__error"><?=$arResult['FORM_ERRORS'][$sTmpCode]?></div><?
				//	unset($arResult['FORM_ERRORS'][$sTmpCode]);
				//}

				$arTmp = $arResult['QUESTIONS'][$sTmpCode]['STRUCTURE'][0];
				$sTmpFieldName = 'form_'.$arTmp['FIELD_TYPE'].'_'.$arTmp['ID'];
				$sVal = isset($arResult['arrVALUES'][$sTmpFieldName]) ? $arResult['arrVALUES'][$sTmpFieldName] : $arDefaultValues[$sTmpCode];
				?><textarea class="textarea" name="<?=$sTmpFieldName?>"><?=htmlspecialcharsbx($sVal)?></textarea><?
			}
		?></div><?

		if($arResult['isUseCaptcha'] == 'Y') {
			?><div class="ordering-form__row"><?
				?><div class="input__wrap"><?
					?><label class="label">Введите символы на картинке</label><?
					?><div><?
						?><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" /><?
						?><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" width="180" height="40" /><?
						?><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="input" /><?
					?></div><?
				?></div><?
			?></div><?
		}

		?><div class="ordering-form__row">
			<div class="input__wrap">
				<input type="hidden" name="web_form_submit" value="Отправить" />
				<input value="Отправить" type="submit" class="btn _style_4 _full-width" name="web_form_submit" />
			</div>
		</div><?

		echo $arResult['FORM_FOOTER'];
	}

	echo '</div>';

?></div><?

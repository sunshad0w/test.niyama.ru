<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Веб-форма отправки резюме
//
//_log_array($arResult);

?><div class="vacancies-form js-widget" onclick="return {'vacanciesForm': {}}"><?

	if($arResult['isFormErrors'] == 'Y') {
		echo $arResult['FORM_ERRORS_TEXT'];
	}

	if($arResult['isFormNote'] == 'Y') {
		?><p>Ваше резюме успешно отправлено, мы свяжемся с вами</p><?
	} else {
		$sTmp = str_replace('<form ', '<form data-parsley-validate class="form" ', $arResult['FORM_HEADER']);
		echo $sTmp;
		echo $arResult['arForm']['DESCRIPTION'];

		$sTmpCode = 'VACANCY_NAME';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
				$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
			}

			?><label class="label vacancies-form__label _top">Представьтесь</label><?
			?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
			echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
		}

		$sTmpCode = 'VACANCY_TEL';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
				$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
			}

			?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
			echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
		}

		$sTmpCode = 'VACANCY_EMAIL';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			if($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') {
				$arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'] = str_replace('<input ', '<input required ', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
			}

			?><label class="label _lite"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION'] . (($arResult['QUESTIONS'][$sTmpCode]['REQUIRED'] == 'Y') ? ' *' : '')?></label><?
			echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
		}

		$sTmpCode = 'VACANCY_POS';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?><div class="vacancies-form__select"><?
				?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
				?><div onclick="return {'select': {}}" class="select__wrap js-widget"><?
					$sTmp = str_replace('class="inputselect"', 'class="select" data-placeholder="Выберите позицию"', $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE']);
					// добавим пустую опцию, чтобы появилось "Выберите позицию"
					$iPos = strpos($sTmp, '>');
					if($iPos) {
						$sTmp = substr($sTmp, 0, ($iPos + 1)).'<option value=""></option>'.substr($sTmp, ($iPos + 1));
					}
					echo $sTmp;
				?></div><?
			?></div><?
		}

		$sTmpCode = 'VACANCY_FILE';
		if($arResult['QUESTIONS'][$sTmpCode]) {
			?><label class="label"><?=$arResult['QUESTIONS'][$sTmpCode]['CAPTION']?></label><?
			?><div class="input__file _mb_1"><span class="input__file-name">Выбрать файл...</span><?
				echo $arResult['QUESTIONS'][$sTmpCode]['HTML_CODE'];
			?></div><?
		}

		if($arResult['isUseCaptcha'] == 'Y') {
			?><div style="margin-top: 42px;"><?
				?><label class="label">Введите символы на картинке</label><?
				?><div><?
					?><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" /><?
					?><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" width="180" height="40" /><?
					?><input type="text" required name="captcha_word" size="30" maxlength="50" value="" class="input _mb_1" /><?
				?></div><?
			?></div><?
		}

		?><input name="web_form_submit" type="submit" value="Отправить" class="btn _style_1 vacancies-form__submit"><label class="label _lite _smaller">* - поля, обязательные для заполнения</label><?
		echo $arResult['FORM_FOOTER'];
	}
?></div><?

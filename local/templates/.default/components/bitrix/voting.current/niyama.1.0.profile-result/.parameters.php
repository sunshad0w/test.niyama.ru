<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TEXT_OK" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('TEXT_OK'),
		"TYPE"		=> "STRING",		
		"MULTIPLE" => "N",	
		"DEFAULT" => "Спасибо за участие в опросе, нам важно ваше мнение"	
	),
	"SHOW_RESULT" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('SHOW_RESULT'),
		"TYPE"		=> "CHECKBOX",		
		"MULTIPLE" => "N",	
		"DEFAULT" => "Y"	
	)
	
);

?>
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$sAuthMessage = ADV_get_flashdata('AUTH_MESSAGE');
$aAuthError = ADV_get_flashdata('auth_error');
$isShowMessage = $sAuthMessage || $aAuthError || $arResult['ERRORS'];

// Показываем Ошибки при регистрации

?><h1 class="auth__title align-center">Регистрация</h1><?

?><div class="auth"><?
	$bField = false;
	$bPassField = false;
	$sText = $sAuthMessage;
	if($err_txt = $aAuthError) {
		$sText = $err_txt;
	}

	if($arResult['ERRORS']) {
		$arTmp = $arResult['ERRORS'];
		$arResult['ERRORS'] = array();
		foreach($arTmp as $mKey => $sError) {
			if(!is_numeric($mKey)) {
				$arResult['ERRORS'][$mKey] = str_replace('#FIELD_NAME#', '&quot;'.GetMessage('REGISTER_FIELD_'.$mKey).'&quot;', $sError);
			} else {
				$arTmpCur = explode('<br>', $sError);
				foreach($arTmpCur as $sError) {
					if(stripos($sError, 'пароль') !== false) {
						$arResult['ERRORS']['PASSWORD'] = $sError;
					} elseif(stripos($sError, 'E-Mail') !== false) {
						$arResult['ERRORS']['EMAIL'] = $sError;
					} else {
						$arResult['ERRORS'][] = $sError;
					}
				}
			}
		}
	}

	if($sAuthMessage || $aAuthError) {
		?><div class="auth__bonus _wide">
			<p><?=$sText?></p>
		</div><?
	} else {
		# НИЯМЫ todo необходимо воводить реальные данные
		$arRegLevel = CNiyamaDiscountLevels::FindRegistrationTypeItems();
		$arLevel = CNiyamaDiscountLevels::GetDiscountLevelById($arRegLevel[0]);
		if (!empty($arLevel)) {
			?><div class="auth__bonus _wide">
				<p>За регистрацию мы начислим вам </p>
				<div class="auth__bonus-nyam"> <span class="text-brown"><?=$arLevel['NIYAM_CNT']?></span><img src="/images/nyam_big.png" alt=""><span class="text-orange">ниям<a href="javascript:void(0)" class="auth__bonus-question"> <span>Бонусные баллы, которыми вы можете оплатить часть заказа</span></a></span></div>
			</div><?
		}
	}

	?><form data-parsley-validate method="post" target="_top" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data"><?
		?><div class="auth__left"><?
			if($arResult['BACKURL']) {
				?><input type="hidden" name="backurl" value="<?=$arResult['BACKURL']?>" /><?
			}
			foreach($arResult['SHOW_FIELDS'] as $sFieldName) {
				?><label class="label auth__label"><?=GetMessage('REGISTER_FIELD_'.$sFieldName)?>:<?=($arResult['REQUIRED_FIELDS_FLAGS'][$sFieldName] == 'Y') ? '<span>*</span>' : ''?></label><?
				$sClass = '';
				$sErrText = '';
				if($arResult['ERRORS'][$sFieldName]) {
					$sClass = ' parsley-error';
					$sErrText = rtrim($arResult['ERRORS'][$sFieldName], '.');
					unset($arResult['ERRORS'][$sFieldName]);
				}

				switch($sFieldName) {
					case 'PASSWORD':
						?><div onclick="return {'togglePass': {}}" class="input__wrap js-widget">
							<div class="input__showpass"></div><?
							?><input required type="password" id="PassWD" data-parsley-minlength="6" class="input auth__input<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						?></div><?
						if(strlen($sErrText)) {
							?><div rel="PassWD" class="auth__error"><?=$sErrText?></div><?
						}
						if($arResult['SECURE_AUTH']) {
							?><span class="bx-auth-secure" id="bx_auth_secure" title="<?=GetMessage('AUTH_SECURE_NOTE')?>" style="display:none">
								<div class="bx-auth-secure-icon"></div>
							</span>
							<noscript>
								<span class="bx-auth-secure" title="<?=GetMessage('AUTH_NONSECURE_NOTE')?>">
									<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
								</span>
							</noscript>
							<script type="text/javascript">
								document.getElementById('bx_auth_secure').style.display = 'inline-block';
							</script><?
						}
					break;

					case 'CONFIRM_PASSWORD':
						?><div onclick="return {'togglePass': {}}" class="input__wrap js-widget">
							<div class="input__showpass"></div><?
							?><input id="<?='REGISTER_'.$sFieldName?>" required data-parsley-equalto="#PassWD" type="password" class="input auth__input<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						?></div><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;

					case 'PERSONAL_PHONE':
						// Инпут для телефона
						?><input required type="tel" id="<?='REGISTER_'.$sFieldName?>" class="input auth__input _phone-mask<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;

					case 'EMAIL':
						?><input required data-parsley-type="email" id="<?='REGISTER_'.$sFieldName?>" type="email" class="input auth__input<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;

					case 'PERSONAL_PHOTO':
					case 'WORK_LOGO':
						?><input id="<?='REGISTER_'.$sFieldName?>" size="30" type="file" name="REGISTER_FILES_<?=$sFieldName?>" /><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;

					case 'NAME':
						?><input id="<?='REGISTER_'.$sFieldName?>" required type="text" class="input auth__input<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;

					default:
						?><input id="<?='REGISTER_'.$sFieldName?>" type="text" class="input auth__input<?=$sClass?>" name="REGISTER[<?=$sFieldName?>]" value="<?=$arResult['VALUES'][$sFieldName]?>" maxlength="255" /><?
						if(strlen($sErrText)) {
							?><div rel="<?='REGISTER_'.$sFieldName?>" class="auth__error"><?=$sErrText?></div><?
						}
					break;
				}
			}
			// CAPTCHA
			if($arResult['USE_CAPTCHA'] == 'Y') {
				?><label class="label auth__label"><?=GetMessage('REGISTER_CAPTCHA_TITLE')?></label>
				<input type="hidden" name="captcha_sid" value="<?=$arResult['CAPTCHA_CODE']?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult['CAPTCHA_CODE']?>" width="180" height="40" alt="CAPTCHA" />
				<label class="label auth__label"><?=GetMessage('REGISTER_CAPTCHA_PROMT')?>:<span class="starrequired">*</span></label>
				<input type="text" class="input auth__input" name="captcha_word" maxlength="50" value="" /><?
			}

			if($arResult['ERRORS']) {
				?><div class="auth__error"><?=implode('<br />', $arResult['ERRORS'])?></div><?
			}

			?><input type="submit" name="register_submit_button" value="<?=GetMessage('AUTH_REGISTER')?>" class="btn _style_2 auth__submit" onclick="yaCounter24799106.reachGoal('register_ok'); ga('send','pageview','/register_ok'); return true;">
		</div>
		<div class="auth__right">
			<label class="label">Войти через социальную сеть</label><?
			$APPLICATION->IncludeComponent(
				'adv:social.auth',
				'niyama.1.0',
				array(
					'BACK_URL' => '/auth/?register=yes',
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
			/*
			?><a href="/auth/?login=yes">Авторизация</a><?
			*/
			if($arResult['USER_PROPERTIES']['SHOW'] == 'Y') {
				foreach($arResult['USER_PROPERTIES']['DATA'] as $FIELD_NAME => $arUserField) {
					?><p>
						<input type="checkbox" name="UF_SUBSCRIPTION" id="rememberPass" class="checkbox _big"<?=($_REQUEST['UF_SUBSCRIPTION'] == 'on' ? ' checked="checked"' : '')?> />
						<label for="rememberPass" class="checkbox-label">Получать рассылку акций и новостей</label>
					</p><?
				}
			}
			?><p>Уже зарегистрированы на сайте? - <a href="/auth/">Войти</a></p>
		</div>
	</form>
</div><?

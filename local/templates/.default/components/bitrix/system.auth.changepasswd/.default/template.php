<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$APPLICATION->setTitle('Смена пароля');
$APPLICATION->AddChainItem("Смена пароля");

$strError = false;
$strOther = false;
if ($arParams["~AUTH_RESULT"]['TYPE'] == "ERROR"){
    $strError = $arParams["~AUTH_RESULT"]['MESSAGE'];
} else {
    $strOther = $arParams["~AUTH_RESULT"]['MESSAGE'];
}


//if (!empty($strOther)): ?>
<!--    <div class="b-inline-msg _lamp">-->
<!--        --><?//= $strOther; ?>
<!--    </div>-->
<?// endif;


?><h1 class="auth__title align-center">Смена пароля</h1>
<div class="auth"><?

    if (!empty( $strError)){
    ?><div class="auth__bonus _wide">
        <p><?= $strError; ?></p>
    </div><?
    }

    if (!empty($strOther)){
        ?><div class="auth__bonus _wide">
        <p><?= $strOther; ?></p>
        </div><?
    }
    ?><div class="auth__center">
        <form data-parsley-validate method="post" class="b-form js-widget" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
            <?if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <? endif ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="CHANGE_PWD">

            <label  class="label auth__label">Имя учётной записи</label>
            <input required class="input auth__input" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" required>
            <label   class="label auth__label">Контрольная строка</label>
            <input required class="input auth__input" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" required>
            <label class="label auth__label">Новый пароль</label>
            <div onclick="return {'togglePass': {} }" class="input__wrap js-widget">
                <div class="input__showpass"></div>
                <input required type="password" id="PassWD" type="password" class="input auth__input"  name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" required>
            </div>
            <label class="label auth__label"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></label>
            <div onclick="return {'togglePass': {} }" class="input__wrap js-widget">
                <div class="input__showpass"></div>
                <input required data-parsley-equalto="#PassWD" type="password" class="input auth__input" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required >
            </div>
            <input type="submit" value="Изменить пароль" class="btn _style_2 auth__submit">
        </form>
    </div>
</div><?


?>

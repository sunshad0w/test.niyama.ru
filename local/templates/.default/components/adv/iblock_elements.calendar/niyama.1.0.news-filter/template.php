<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Фильтр новостей (шаблон не должен кэшироваться)
//

if(!$arResult['ITEMS']) {
	return;
}

$arFirst = reset($arResult['ITEMS']);
$iCurYearTmp = isset($_REQUEST['year']) ? intval($_REQUEST['year']) : $arFirst['GROUP_DATE_FIELD'];
$iCurYear = $iCurYearTmp;
// проверка корректности переданного года
/*
$iCurYear = '';
foreach($arResult['ITEMS'] as $arItem) {
	if($arItem['GROUP_DATE_FIELD'] == $iCurYearTmp) {
		$iCurYear = $iCurYearTmp;
		break;
	}
}
*/
$iCurYear = intval($iCurYear) > 0 ? $iCurYear : $arFirst['GROUP_DATE_FIELD'];

?><div class="news__years"><?
	?><div class="years"><?
		foreach($arResult['ITEMS'] as $arItem) {
			$sUrl = $arItem['GROUP_DATE_FIELD'] == $arFirst['GROUP_DATE_FIELD'] ? $arParams['PAGE_BASE_URL'] : $arParams['PAGE_BASE_URL'].'?year='.$arItem['GROUP_DATE_FIELD'];
			$sTmpClass = $arItem['GROUP_DATE_FIELD'] == $iCurYear ? ' _active' : '';
			?><a class="years__item<?=$sTmpClass?>" href="<?=$sUrl?>"><span><?=$arItem['GROUP_DATE_FIELD']?></span></a><?
		}
	?></div><?
?></div><?

$GLOBALS['arNewsCityFilterEx'] = array(
	'>=DATE_ACTIVE_FROM' => ConvertTimeStamp(mktime(0, 0, 0, 1, 1, $iCurYear), 'FULL'),
	'<=DATE_ACTIVE_FROM' => ConvertTimeStamp(mktime(23, 59, 59, 12, 31, $iCurYear), 'FULL'),
);

$GLOBALS['APPLICATION']->SetTitle('Новости за '.$iCurYear.' год');

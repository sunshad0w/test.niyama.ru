<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// МЕНЮ
//
$iDefaultCount = intval(CNiyamaCustomSettings::getStringValue("default_categories_count", 10));
$bisAjaxMode = isset($arParams['MODE']) && $arParams['MODE'] == 'AJAX';

if ( CModule::IncludeModule('iblock')) :
    $arMenuItems = CNiyamaCatalog::getMenu();
    if (!empty($arMenuItems)) :


        if ($bisAjaxMode) :

        //$iCountCheck = ceil(count($arMenuItems) / 2);
        $iCount = count($arMenuItems);
        $iCountCheck = ($iDefaultCount && $iDefaultCount < $iCount) ? ceil($iDefaultCount / 2) : ceil($iCount / 2);
    
        ?>
        <nav class="filter__group clearfix" data-category="menu[]"><?
     
			
			//
			// Элементы по-умолчанию
			//
			?><div class="filter__col"><?
				$i = 1;
				foreach ($arMenuItems as $item) {

                    $sDisabled = "";
                    $bSet = (Isset($_REQUEST['ing']) && !isset($_REQUEST['menu']));
                    if(isset($arParams['ISSET']) && !empty($arParams['ISSET']) && $bSet ){
                        if (!in_array($item['ID'],$arParams['ISSET'])){
                            $sDisabled = "_disabled";
                        }
                    }

					?><div data-filter="<?= $item['ID'] ?>" class="filter__item <?= (isset($_REQUEST['menu'])) && (in_array($item['ID'],$_REQUEST['menu']))? "_selected" : ""; ?><?=((isset($_REQUEST['ex_menu'])) && (in_array($item['ID'], $_REQUEST['ex_menu'])) ? ' _excluded' : '')?>">
                        <div class="filter__link__ing filter__link <?=$sDisabled?>">
                            <span class="pseudo-link"><?=$item['NAME']?></span>
                        </div>
					</div><?
					echo ($i == $iCountCheck)? '</div><div class="filter__col">' : '';
					$i++;
					if ($iDefaultCount > 0 && $i > $iDefaultCount) {
						break;
					}
					
				}
			?></div><?
	
			?><div class="clearfix"></div><?
			
			//
			// Скрытые элементы
			//
			if ($iDefaultCount && $iDefaultCount < $iCount) {
				?><div class="filter__hidden"><?
					?><div class="filter__col"><?
	
						$iCountCheck = ceil(($iCount - $iDefaultCount) / 2);
	
						$i = 1;
						foreach ($arMenuItems as $item) {

                            $sDisabled = "";
                            $bSet = (Isset($_REQUEST['ing']) && !isset($_REQUEST['menu']));
                            if(isset($arParams['ISSET']) && !empty($arParams['ISSET']) && $bSet){
                                if (!in_array($item['ID'],$arParams['ISSET'])){
                                    $sDisabled = "_disabled";
                                }
                            }

							if ($i > $iDefaultCount) {
								?><div data-filter="<?= $item['ID'] ?>" class="filter__item <?= (isset($_REQUEST['menu'])) && (in_array($item['ID'],$_REQUEST['menu']))? "_selected" : ""; ?>">
								<div class="filter__link <?= $sDisabled ?>">
                                    <span class="pseudo-link"><?=$item['NAME']?></span>
                                </div>
								</div><?
								echo ($i - $iDefaultCount == $iCountCheck)? '</div><div class="filter__col">' : '';
							}
							$i++;
						}

					?></div><?
                    ?><div class="clearfix"></div><?
				?></div><?
				?><div class="filter__item"><?
					?><a class="filter__link-more" href="#"><?
						?><span class="pseudo-link">Все категории</span><?
						?><span class="pseudo-link hidden-bl">Скрыть</span><?
					?></a><?
				?></div><?
			}
		?></nav><?
			
			
		else:

            /**
             * Классическая навигация
             */
            $arMenu = false;
            if (isset($arParams['CAT_INFO'])){
                $arMenu = $arParams['CAT_INFO'];
            }

            $iCountCheck = ceil(count($arMenuItems) / 2);
            $iCount = count($arMenuItems);
            //$iCountCheck = ($iDefaultCount && $iDefaultCount < $iCount) ? ceil($iDefaultCount / 2) : ceil($iCount / 2);

            ?><nav class="filter__group clearfix" data-category="menu[]"><?


            //
            // Элементы по-умолчанию
            //
            ?><div class="filter__col"><?
            $i = 1;
            foreach ($arMenuItems as $item) {
                ?><div data-filter="<?= $item['ID'] ?>" class="filter__item <?= ((isset($arMenu['ID'])) && ($item['ID']==$arMenu['ID']))? "_selected" : ""; ?>">
                <a href="<?= $item['SECTION_PAGE_URL'] ?>" class="filter__link _menu-item"><span class="pseudo-link"><?= $item['NAME'] ?></span></a>
                </div><?
                echo ($i == $iCountCheck)? '</div><div class="filter__col">' : '';
                $i++;
//                if ($iDefaultCount > 0 && $i > $iDefaultCount) {
//                    break;
//                }
            }
            ?></div><?

            ?><div class="clearfix"></div><?


            ?></nav><?


        endif;
			

    endif;
endif;

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$arReturn = CNiyamaIBlockCartLine::GetList();
$arCalc = CNiyamaIBlockCartLine::calcLine();

$iproc = 0;
$iSelectedIdx = 0;
switch(count($arCalc)) {
	case 1:
		$iproc = 10;
		$iSelectedIdx = 1;
	break;

	case 2:
		$iproc = 50;
		$iSelectedIdx = 2;
	break;

	case 3:
		$iproc = 98;
		$iSelectedIdx = 3;
	break;
}
//PR($arReturn);
if (!empty($arReturn)) {
	?><div class="cart-progress"><?
		?><div class="cart-progress__bar" style="width: <?=$iproc?>%;"></div><?
		?><div class="cart-progress__overlay"></div><?

		$i = 1;
		foreach ($arReturn as $row) {
			//_two _three
			switch ($i) {
				case 1:
					$class = ' _one';
				break;

				case 2:
					$class = ' _two';
				break;

				case 3:
					$class = ' _three';
				break;
			}

			if($i <= $iSelectedIdx) {
				$class .= ' _active';
			}
			?><div class="cart-progress__point<?=$class?>">
				<div class="cart-progress__desc"><a data-title="<?=$row['DETAIL_TEXT']?>" class="cart-progress__link"><span><?=$row['NAME']?></span></a></div>
			</div><?
			$i++;
		}
	?></div><?
}

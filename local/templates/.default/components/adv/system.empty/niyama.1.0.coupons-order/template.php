<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$arCouponsList=$arParams['arCouponsList'];
//PR($arCouponsList);
if($arCouponsList) {
	?><div class="order__group _coupons">
		<div class="order__group-title">
			<div class="order__group-name">Купоны</div>
		</div>
		<div class="order__item"><?
			foreach($arCouponsList as $arBasketItem) {
				$arItemData = $arBasketItem['_PRODUCT_']['_MISC_DATA_'];
				if($arItemData['TYPE_IMG'] == 'DOWNLOAD') {
					// тип карточки "Загрузка изображения"
					$sTmpImg = '';
					if($arItemData['IMG1']) {
						$arTmpImg = CFile::ResizeImageGet(
							$arItemData['IMG1'],
							array(
								'width' => 454, 
								'height' => 143
							), 
							BX_RESIZE_IMAGE_PROPORTIONAL,
							true
						);
						if($arTmpImg['src']) {
							$sTmpImg .= '<img src="'.$arTmpImg['src'].'" width="'.$arTmpImg['width'].'" height="'.$arTmpImg['height'].'" alt="">';
						}
					}

					if($arItemData['IMG2']) {
						$arTmpImg = CFile::ResizeImageGet(
							$arItemData['IMG2'],
							array(
								'width' => 217, 
								'height' => 293
							), 
							BX_RESIZE_IMAGE_PROPORTIONAL,
							true
						);
						if($arTmpImg['src']) {
							$sTmpImg .= '<img src="'.$arTmpImg['src'].'" width="'.$arTmpImg['width'].'" height="'.$arTmpImg['height'].'" alt="">';
						}
					}

					?><div data-id="<?=$arBasketItem['ID']?>" class="coupon _image"><?=$sTmpImg?></div><?
				} else {
					// типы карточек "По умолчанию" и "Изображение блюд/блюда"
					switch($arItemData['BONUS_TYPE']) {
						case 'BLUDO':
							$sTmpStyle = '_style_2';
							$sTmpImg = '';
							switch(count($arItemData['PICS_BLUDS'])) {
								case 1:
									$sTmpStyle = '_style_3';
									if($arItemData['PICS_BLUDS'][0]) {
										$arTmpImg = CFile::ResizeImageGet(
											$arItemData['PICS_BLUDS'][0], 
											array(
												'width' => 96, 
												'height' => 70
											), 
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										if($arTmpImg['src']) {
											$sTmpImg .= '<img class="coupon__pic-img" src="'.$arTmpImg['src'].'" width="'.$arTmpImg['width'].'" height="'.$arTmpImg['height'].'" alt="">';
										}
									}
								break;

								case 2:
									$sTmpStyle = '_style_2';
									if($arItemData['PICS_BLUDS'][0]) {
										$arTmpImg = CFile::ResizeImageGet(
											$arItemData['PICS_BLUDS'][0], 
											array(
												'width' => 96, 
												'height' => 70
											), 
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										if($arTmpImg['src']) {
											$sTmpImg .= '<img class="coupon__pic-img pull-left" src="'.$arTmpImg['src'].'" width="'.$arTmpImg['width'].'" height="'.$arTmpImg['height'].'" alt="">';
										}
									}
									if($arItemData['PICS_BLUDS'][1]) {
										$arTmpImg = CFile::ResizeImageGet(
											$arItemData['PICS_BLUDS'][1], 
											array(
												'width' => 96, 
												'height' => 70
											), 
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										if($arTmpImg['src']) {
											$sTmpImg .= '<img class="coupon__pic-img pull-right" src="'.$arTmpImg['src'].'" width="'.$arTmpImg['width'].'" height="'.$arTmpImg['height'].'" alt="">';
										}
									}
								break;
							}

							?><div data-id="<?=$arBasketItem['ID']?>" class="coupon <?=$sTmpStyle?>">
								<div class="coupon__pic"><?
									echo $sTmpImg;
									?><div class="coupon__pic-price">
										<span class="coupon__pic-price-actual"><?=CProjectUtils::FormatNum($arItemData['SUM_NEW_PRICE'])?><span class="rouble">a</span></span>
										<span class="coupon__pic-price-old"><?=CProjectUtils::FormatNum($arItemData['SUM_OLD_PRICE'])?><span class="rouble">a</span></span>
									</div>
								</div>
								<div class="coupon__info">
									<div class="coupon__title"><?=$arItemData['NAME']?></div>
									<p class="coupon__desc"><?=$arItemData['TEXT']?></p>
								</div>
							</div><?
							// !!!
							echo ' ';
						break;

						case 'CONST_DISCOUNT':
							?><div data-id="<?=$arBasketItem['ID']?>" class="coupon _style_4">
								<div class="coupon__title"> 
									<div class="coupon__title-left">
										<div class="coupon__title-left-top">постоянная</div>
										<div class="coupon__title-left-bottom">скидка</div>
									</div>
									<div class="coupon__title-right"><img src="<?=$templateFolder.'/img/coupon_card.png'?>"></div>
									<div class="coupon__title-center">
										<div class="coupon__badge _percent"><span><?=$arBasketItem['PROPS']['val_coupon_used']?><sup>%</sup></span></div>
									</div>
								</div>
							</div><?
							// !!!
							echo ' ';
						break;

						case 'DISCOUNT_PERSENT':
							?><div data-id="<?=$arBasketItem['ID']?>" class="coupon _style_1">
								<div class="coupon__title"> 
									<div class="coupon__title-left">разовая</div>
									<div class="coupon__title-right">скидка</div>
									<div class="coupon__title-center">
										<div class="coupon__badge _percent"><span><?=$arItemData['DISCOUNT_PER']?><sup>%</sup></span></div>
									</div>
								</div>
								<p class="coupon__desc">Воспользуйтесь при оплате любого заказа</p>
							</div><?
							// !!!
							echo ' ';
						break;

						case 'DISCOUNT_MONEY':
							$sTmpText = 'НИЯМ';
							$sTmpBadge = '_nyam';
							$sTmpDesc = CNiyamaCustomSettings::GetStringValue('text_coupon_by_niam', '');
							if($arItemData['MONEY_TYPE'] == 'MONEY') {
								$sTmpText = CProjectUtils::PrintCardinalNumberRus($arBasketItem['PROPS']['val_coupon'], 'рублей', 'рубль', 'рубля');
								$sTmpBadge = '_gift';
								$sTmpDesc = CNiyamaCustomSettings::GetStringValue('text_coupon_by_rub', '');
							}
							?><div data-id="<?=$arBasketItem['ID']?>" class="coupon _style_1">
								<div class="coupon__title"><?
									/*
									?><div class="coupon__title-left"><?=CProjectUtils::FormatNum($arItemData['MONEY'])?></div><?
									*/
									?><div class="coupon__title-left"><?=CProjectUtils::FormatNum($arBasketItem['PROPS']['val_coupon_used'])?></div><?
									?><div class="coupon__title-right"><?=$sTmpText?></div>
									<div class="coupon__title-center">
										<div class="coupon__badge <?=$sTmpBadge?>"></div>
									</div>
								</div>
								<p class="coupon__desc"><?=$sTmpDesc?></p>
							</div><?
							// !!!
							echo ' ';
						break;
					}
				}
				if ($arParams['AfterEachCouponBR']){
					echo '<br>';
				}
			}
		?></div>
	</div><?
}
  ?>      
 



                      
                      
                     
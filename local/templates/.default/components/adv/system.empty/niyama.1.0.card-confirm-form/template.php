<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Форма подтверждения регистрации карты
//
    $iCurUserId = isset($_REQUEST['uid']) ? intval($_REQUEST['uid']) : 0;
    $sCardCheckword = isset($_REQUEST['code']) ? trim($_REQUEST['code']) : '';

    if($iCurUserId <= 0) {
        ShowError('Не задан идентификатор');
        return;
    }

    if(!strlen($sCardCheckword)) {
        ShowError('Не задан проверочный код');
        return;
    }

    $bSuccess = false;
    $arFormErrors = array();

    $arIdentify = CNiyamaDiscountCard::GetCardIdentifyData($iCurUserId);
    if(!$arIdentify) {
        $arFormErrors['IDENTIFY_ITEM_NOT_FOUND'] = 'Ссылка не найдена, либо устарела';
    }

    if(!$arFormErrors) {
        if($arIdentify['UF_CHECKWORD'] == $sCardCheckword) {

            // проверим, чтобы у юзера не было активных карт
            $arCurUserCard = CNiyamaIBlockCardData::GetCardData($iCurUserId, true, true);
            if($arCurUserCard) {
                if($arCurUserCard['ACTIVE'] == 'Y') {
                    $arFormErrors['CARD_ALREADY_ACTIVE'] = 'У вас уже активирована карта с другим номером';
                }
                /*
                elseif($arCurUserCard['PROPERTY_CARDNUM_VALUE'] != $arIdentify['UF_CARDNUM']) {
                    $arFormErrors['WRONG_CARD'] = 'Номера зарегистрированной карты и активируемой не совпадают';
                }
                */
            }

            if(!$arFormErrors) {
                $sTmpCheckCard = trim($arIdentify['UF_CARDNUM']);
                $arPdsCardInfo = array();
                if(strlen($sTmpCheckCard)) {
                    $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sTmpCheckCard);
                } else {
                    $arFormErrors['CARD_NUM_EMPTY'] = 'Информация о номере карты не найдена';
                }
                if($arPdsCardInfo) {
                    if($arPdsCardInfo['_ERRORS_']) {
                        $arFormErrors = array_merge($arFormErrors, $arPdsCardInfo['_ERRORS_']);
                    }
                    if(!$arFormErrors && $arPdsCardInfo['Account']) {
                        // все ок, добавляем карту юзеру
                        $iCardDataElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arPdsCardInfo, $iCurUserId, array('ACTIVE' => 'Y'), true);
                        if($iCardDataElementId) {
                            if($arPdsCardInfo['Account']['Discount']) {
                                CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iCurUserId, CNiyamaIBlockCardData::GetTrueDiscount($arPdsCardInfo['Account']['Discount']));
                            }

                            CNiyamaDiscountCard::DeleteCardIdentifyData($iCurUserId);
                            $bSuccess = true;
                        } else {
                            $arFormErrors['UNKNOWN_ERROR'] = 'Не удалось привязать карту к пользователю. Попробуйте повторить операцию позже, либо обратитесь в тех.поддержку сайта';
                        }
                    }
                }
            }
        } else {
            $arFormErrors['WRONG_CHECKWORD'] = 'Неверный проверочный код';
        }

        if(!$bSuccess && !$arFormErrors) {
            $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка. Попробуйте повторить операцию позже, либо обратитесь в тех.поддержку сайта';
        }
    }

    ?><h1 class="auth__title" style="color: white">Подтверждение регистрации карты</h1><?
    if($arFormErrors) {
        echo '<div style="color: red; padding: 10px 0;">'.implode('<br />', $arFormErrors).'</div>';
    } elseif($bSuccess) {
        echo '<div style="color: green; padding: 10px 0;">Карта успешно зарегистрирована</div>';
        LocalRedirect('/personal/');
    }




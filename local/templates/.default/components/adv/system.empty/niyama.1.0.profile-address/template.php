<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="cabinet__addresses"><?

    ?><div class="cabinet__controls"><?

        $APPLICATION->IncludeComponent(
            'adv:system.empty',
            'niyama.1.0.profile-address-popup',
            array(),
            null,
            array(
                'HIDE_ICONS' => 'Y'
            )
        );

        ?><h2>Адреса доставки</h2>
    </div><?

    # Получаем Адреса доставки
	$CityOtherId=intval(CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other', "27"));
    $arAddress = CUsersData::getAddressList();
	$arTmp = CNiyamaOrders::GetDeliveryRegions();
	$arDELIVERY_REGIONS = array();
	foreach($arTmp as $arItem) {
		if($arItem['ACTIVE'] == 'Y') {
			$arDELIVERY_REGIONS[$arItem['ID']] = $arItem;
		}
	}
    if (!empty($arAddress)) {
        foreach ($arAddress as $aItem) {
			if ($aItem['PROPERTY_REGION_VALUE']==$CityOtherId){
				$sRegion = $aItem['PROPERTY_CITY_DOP_VALUE'];
			}else{
				$sRegion = $arDELIVERY_REGIONS[$aItem['PROPERTY_REGION_VALUE']]['NAME'];
			}
            $sSubway = (!empty($aItem['PROPERTY_SUBWAY_VALUE'])) ? " (м. ".$aItem['PROPERTY_SUBWAY_VALUE'].")" : "";

          ?><div class="cabinet__address <?= (intval($aItem['PROPERTY_IS_DEFAULT_VALUE'])>0)? "_active" :"" ?> ">
                <span><?= $sRegion.  $sSubway .", ".$aItem['PROPERTY_ADDRESS_VALUE'].", ".$aItem['PROPERTY_HOUSE_VALUE'].", ".$aItem['PROPERTY_HOME_VALUE'] ?>
                    <a href="/ajax/getProfileEditAddressForm.php?aid=<?= $aItem['ID'] ?>" data-fancybox-type="ajax" class="fancybox">
                    <span class="pseudo-link">Редактировать</span></a>
                </span>
            </div><?

        }
    }
    ?><br>
</div>



          



<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle('Спасибо');

?><div class="page__container clearfix">
    <div class="page__content-wrap">
        <h1 class="auth__title">Спасибо за регистрацию на сайте Нияма!</h1><?
        ?><div class="ordering-form _thanks"><?
        	if (isset($arParams['AUTH_MESSAGE']) && strlen($arParams['AUTH_MESSAGE'])) {
            	?><p><?=$arParams['AUTH_MESSAGE']?></p><?
			}
            $sBackUrl = SITE_DIR;
            $sTitle = 'на главную страницу';
            if ($sTmpBackUrl = ADV_get_flashdata('BACKURL')) {
                $sBackUrl = $sTmpBackUrl;
                $sTitle = 'к заказу';
            }

            ?><p><a href="<?=$sBackUrl?>">Вернуться <?=$sTitle?></a></p><?
		?></div><?

        ?><div class="ordering-form__social">Присоединяйтесь к нам в социальных сетях<br>
            <div class="social _auth"><?
                // ссылки на соцсети
                $GLOBALS['APPLICATION']->IncludeFile(
                    SITE_DIR.'include_areas/footer-social-links.php',
                    array(),
                    array(
                        'MODE' => 'html',
                        'SHOW_BORDER' => false
                    )
                );
            ?></div>
        </div>
	</div>
</div><?

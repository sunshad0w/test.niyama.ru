<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Акция (детально)
//
$_REQUEST['ELEMENT_ID'] = isset($_REQUEST['ELEMENT_ID']) ? intval($_REQUEST['ELEMENT_ID']) : 0;
if($_REQUEST['ELEMENT_ID'] < 0) {
	ShowError('По вашему запросу ничего не найдено');
	@define('ERROR_404', 'Y');
	return;
}

$GLOBALS['arActionsDetailFilterEx'] = array(
	'ID' => intval($_REQUEST['ELEMENT_ID'])
);
$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.iblock_data_list',
	'niyama.1.0.promos-detail',
	array(
		'CACHE_TYPE' => 'A',
		'CACHE_TIME' => '43200',
		'ELEMENT_CNT' => 0,
		'PAGER_SHOW' => 'N',
		'ELEMENT_FILTER_NAME' => 'arActionsDetailFilterEx',
		'SORT_BY1' => 'ID',
		'SORT_ORDER1' => 'ASC',
		'SORT_BY2' => '',
		'SORT_ORDER2' => '',
		'IBLOCKS' => array(CProjectUtils::GetIBlockIdByCode('actions', 'misc')),
		'FIELD_CODE' => array(
			'ID', 'NAME', 'CODE', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'DATE_ACTIVE_FROM', 'DETAIL_PAGE_URL',
			'LIST_PAGE_URL',
			'PROPERTY_*',
		),
		'KEY_FIELD' => '',
		'INCLUDE_TEMPLATE' => 'Y',
		'CACHE_TEMPLATE' => 'Y',
		'GET_NEXT_ELEMENT_MODE' => 'Y',
		'CACHE_EMPTY_RESULT' => 'N',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

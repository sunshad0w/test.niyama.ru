<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$iDefaultCountExIng = intval(CNiyamaCustomSettings::getStringValue('default_ex_ingredient_count', 10));

if (isset($_REQUEST['alerg'])) {
	$arExIngredientsMenu = $_REQUEST['alerg'];
} 


$arSort = array(
	'SORT' => 'ASC',
	'NAME' => 'ASC',
);
$arFilter = null;
$arSelect = null;
$arMenuExItems = CNiyamaCatalog::getExIngredients($arSelect, $arFilter, $arSort);

if (!empty($arMenuExItems)) {
	/*
	$i = 0;
	foreach ($arMenuItems as $item) {
		if ($iDefaultCountExIng > 0 && ($i + 1) >= $iDefaultCountExIng) {
			break;
		}
		if ($item['PROPERTY_IS_POPULAR_VALUE'] == 0) {
			$iDefaultCount = $i;
			break;
		}
		$i++;
	}
	*/

	$iCount = count($arMenuExItems);

	$iCountCheck = ($iDefaultCountExIng && $iDefaultCountExIng < $iCount) ? ceil($iDefaultCountExIng / 2) : ceil($iCount / 2);
	/*
	if (!empty($arParams['ISSET'])) {
		$activeMenuItems = array();
		$disabledMenuItems = array();
		foreach ($arMenuExItems as $item) {
			if (in_array($item['ID'], $arParams['ISSET'])) {
				$activeMenuItems[] = $item;
			} else {
				$disabledMenuItems[] = $item;
			}
		}
		$arMenuExItems = array_merge($activeMenuItems, $disabledMenuItems);
	}
	*/

	?><div class="filter__group clearfix" data-category="alerg[]"><?
		?><div class="filter__title"><?=$arParams['~MENU_TITLE']?></div><?

		//
		// Элементы по умолчанию
		//
		?><div class="filter__col"><?
			$i = 1;
			foreach ($arMenuExItems as $item) {

				$sDisabled = '';
				$bSet = ((isset($_REQUEST['menu'])) || (isset($_REQUEST['ing'])) || (isset($_REQUEST['ings'])));
				if(isset($arParams['ISSET']) && $bSet) {
					if (!empty($arParams['ISSET'])) {
						if (!in_array($item['ID'], $arParams['ISSET'])) {
							$sDisabled = ' _disabled';
						}
					} else {
						$sDisabled = ' _disabled';
					}
				}

				?><div data-filter="<?=$item['ID']?>" class="filter__item<?=((isset($arExIngredientsMenu)) && (in_array($item['ID'], $arExIngredientsMenu)) ? ' _excluded _selected' : '')?>">
					<div class="filter__link__ing filter__link<?=$sDisabled?>">
						<span class="pseudo-link"><?=$item['NAME']?></span>
					</div>
				</div><?
				echo ($i == $iCountCheck)? '</div><div class="filter__col">' : '';
				$i++;
				if ($iDefaultCountExIng > 0 && $i > $iDefaultCountExIng) {
					break;
				}
			}
		?></div><?

		?><div class="clearfix"></div><?

		//
		// Скрытые элементы
		//
		if ($iDefaultCountExIng && $iDefaultCountExIng < $iCount) {
			$is_selected = false;
			ob_start();
			?><div class="filter__col"><?

				$iCountCheck = ceil(($iCount - $iDefaultCountExIng) / 2);

				$i = 1;
				foreach ($arMenuExItems as $item) {
					$sDisabled = '';
					$bSet = (isset($_REQUEST['menu']));
					if(isset($arParams['ISSET']) && $bSet) {
						if (!empty($arParams['ISSET'])) {
							if (!in_array($item['ID'], $arParams['ISSET'])) {
								$sDisabled = ' _disabled';
							}
						} else {
							$sDisabled = ' _disabled';
						}
					}
					if ($i > $iDefaultCountExIng) {
						$selected = '';
						if (isset($arExIngredientsMenu) && (in_array($item['ID'], $arExIngredientsMenu))) {
							$selected = '_selected';
							$is_selected = true;
						}
						?><div data-filter="<?=$item['ID']?>" class="filter__item<?=((isset($arExIngredientsMenu)) && (in_array($item['ID'], $arExIngredientsMenu)) ? ' _excluded _selected' : '')?>">
							<div class="filter__link__ing filter__link<?=$sDisabled?>">
								<span class="pseudo-link"><?=$item['NAME']?></span>
							</div>
						</div><?
						echo ($i - $iDefaultCountExIng == $iCountCheck)? '</div><div class="filter__col">' : '';
					}
					$i++;
				}
			?></div><?
			$sExIng = ob_get_clean();

			?><div class="filter__hidden"<?=($is_selected) ? ' style="display: block;"' : ''?>><?
				echo $sExIng;
				?><div class="clearfix"></div><?
			?></div><?

			?><div class="filter__item"><?
				?><a class="filter__link-more" href="#"><?
					?><span class="pseudo-link"<?=($is_selected) ? ' style="display: none;"' : ''?>>Все исключения</span><?
					?><span class="pseudo-link hidden-bl"<?=($is_selected) ? ' style="display: inline;"' : ''?>>Скрыть</span><?
				?></a><?
			?></div><?
		}
	?></div><?
}

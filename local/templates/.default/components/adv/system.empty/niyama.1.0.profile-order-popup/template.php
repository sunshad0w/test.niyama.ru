<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('sale');

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';
$iGuestID = (isset($arParams['GID']) && intval($arParams['GID']) > 0) ? intval($arParams['GID']) : false;

# Получаем список гостей пользователя
$arGuests = CNiyamaCart::GetCurUserGuestsList();

?><div onclick="return {'orderHistory': {}}" data-url="/ajax/getProfileOrderList.php" class="order-history js-widget">
	<div class="order-history__select"><?
		if(!empty($arGuests)) {
			?><label class="label">Заказ с гостем</label><?
			?><div onclick="return {'select': {}}" class="select__wrap js-widget"><?
				?><select class="select"><?
					?><option data-img-src="/images/profile/guests_all.png" value="0">Все гости</option><?

					foreach($arGuests as $arItem) {
						# Получаем аватар
						$arAvaData = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);
						$sTmpSelected = $iGuestID == $arItem['ID'] ? ' selected="selected"' : '';
						?><option<?=$sTmpSelected?> data-img-src="<?=$arAvaData['src']?>" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
					}
				?></select><?
			?></div><?
		}
	?></div><?

	# Получаем Список заказов
	$dbOrder = CSaleOrder::GetList(
		array(
			'UF_FO_DATE_CREATE' => 'DESC',
			'DATE_INSERT' => 'DESC',
			'ID' => 'DESC'
		),
		array(
			'USER_ID' => intval($GLOBALS['USER']->GetID()),
			'@STATUS_ID' => $arParams['SELECT_STATUSES'],
			'LID' => 's1'
		),
		false,
		false,
		array(
			'ID', 'STATUS_ID', 'PRICE', 'DATE_INSERT', 'CANCELED',
			'UF_*'
		)
	);

	$arReturnTmp = array();
	while($arOrder = $dbOrder->Fetch()) {
		$arOrderCart = CNiyamaCart::GetCartListByOrderId($arOrder['ID']);
		$arCurrentItemsCount = count($arOrderCart['current'][0]['ITEMS']);

		$iGuestItemsCount = 0;
		$iGuestCount = 0;
		$arOrderCart['guest'] = $arOrderCart['guest'] && is_array($arOrderCart['guest']) ? $arOrderCart['guest'] : array();
		foreach($arOrderCart['guest'] as $arTmpItem) {
			if($arTmpItem['ITEMS']) {
				$iGuestItemsCount += count($arTmpItem['ITEMS']);
			}
			$iGuestCount++;
		}
		if((!empty($arOrderCart['GUEST_CNT']['QUANTITY'])) && ($arOrderCart['GUEST_CNT']['QUANTITY'] > 0)) {
			$iGuestCount += intval($arOrderCart['GUEST_CNT']['QUANTITY']);
		}

		$iAllProductCount = (intval($arCurrentItemsCount) + intval($iGuestItemsCount));

		$sStatusName = 'Выполняется';
		if($arOrder['STATUS_ID'] == 'F') {
			$sStatusName = 'Выполнен';
		} else {
			if($arOrder['CANCELED'] == 'Y') {
				 $sStatusName = 'Отменен';
			}
		}
		$arOrder['STATUS_NAME'] = $sStatusName;
		$arOrder['DATE_INSERT_FORMAT'] = date('d.m.Y', MakeTimeStamp($arOrder['DATE_INSERT'], 'DD.MM.YYYY HH:MI:SS'));
		$arOrder['UF_FO_DATE_CREATE'] = $arOrder['UF_FO_DATE_CREATE'] ? date('d.m.Y', MakeTimeStamp($arOrder['UF_FO_DATE_CREATE'], 'DD.MM.YYYY HH:MI:SS')) : '';
		$arOrder['ORDER_CART'] = $arOrderCart;
		$arOrder['PRODUCTS_COUNT'] = $iAllProductCount;
		$arOrder['GUEST_COUNT'] = $iGuestCount;
		$arReturnTmp[] = $arOrder;
	}

	# Если установлен гость то фильтруем заказы по гостю
	$arReturn = array();
	if($iGuestID) {
		if(!empty($arReturnTmp)) {
			foreach($arReturnTmp as $oRow) {
				if(!empty($oRow['ORDER_CART']['guest'])) {
					if(isset($oRow['ORDER_CART']['guest'][$iGuestID])) {
						$arReturn[] = $oRow;
					}
				}
			}
		}
	} else {
		$arReturn = $arReturnTmp;
	}

	if(!empty($arReturn)) {
		?><div class="popup__user-body">
			<table class="order-history__table">
				<thead>
					<tr>
						<th><span>Дата</span></th>
						<th>№ заказа</th>
						<th class="align-right"><span>Сумма </span><span class="rouble">a</span>
						</th>
						<th><span>Статус</span></th>
						<th>Гости и блюда</th>
						<th><span>Ниямы</span></th>
					</tr>
					<tr>
						<th colspan="6" class="_top"></th>
					</tr>
				</thead>
				<tbody><?
					foreach($arReturn as $row) {
						$sFoOrderId = (!empty($row['UF_FO_ORDER_CODE'])) ? $row['UF_FO_ORDER_CODE'] : 'н/д';
						?><tr data-href="<?='/profile/order/'.$row['ID'].'.php'?>">
							<td><?=($row['UF_FO_DATE_CREATE'] ? $row['UF_FO_DATE_CREATE'] : $row['DATE_INSERT_FORMAT'])?></td>
							<td><?=$sFoOrderId?></td>
							<td class="align-right"><?=$row['PRICE']?></td>
							<td><?=$row['STATUS_NAME']?></td>
							<td><?=$row['GUEST_COUNT']?>/<?=$row['PRODUCTS_COUNT']?></td><?
							// TODO Уточнить по начислению ниямов
							?><td>--</td>
						</tr><?
					}
				?></tbody>
			</table>
		</div><?
	} else {
		?><div class="order-history__table">Ничего не найдено</div><?
	}
?></div><?

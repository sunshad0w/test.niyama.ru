<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();


/**
 * Add
 */
if(($arParams['MODE'] && $arParams['MODE'] == 'ADD_GUEST') || ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['addGuest'] == 'Y' && check_bitrix_sessid())) {
	$mGuestId = '';
	$sErrMsg = '';

	$sGuestName = isset($_REQUEST['NAME']) ? trim($_REQUEST['NAME']) : '';
	if(!strlen($sGuestName)) {
		$sGuestName = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : $sGuestName;
	}

	if(strlen($sGuestName)) {
		$sGuestNameUpper = ToUpper($sGuestName);
		$arGuestsList = CNiyamaCart::GetCurUserGuestsList();
		foreach($arGuestsList as $arItem) {
			if(ToUpper($arItem['NAME']) == $sGuestNameUpper) {
				$sErrMsg = 'Гость с таким именем уже существует, пожалуйста, укажите другое имя';
				break;
			}
		}

		if(!strlen($sErrMsg)) {
			$iAvaID = $_REQUEST['avatar_id'] ? intval($_REQUEST['avatar_id']) : 0;
			if($iAvaID <= 0) {
				$iAvaID = $_REQUEST['avatar'] ? intval($_REQUEST['avatar']) : $iAvaID;
			}
			if($iAvaID <= 0) {
				$iAvaID = CUsersData::GetFirstDefaultAvatar();
			}

			$mPhoto = false;
			if (isset($_FILES['PHOTO']) && (!empty($_FILES['PHOTO']['name']))) {
				$mPhoto = $_FILES['PHOTO'];
			}

			$mGuestId = CNiyamaCart::AddGuest($sGuestName, $mPhoto, $iAvaID);
		}
	} else {
		$sErrMsg = 'Имя гостя введено некорректно, пожалуйста, введите другое имя';
	}

	if($arParams['JSON_RESULT'] && $arParams['JSON_RESULT'] == 'Y') {
		$arJson = array(
			'gid' => $mGuestId
		);
		if(strlen($sErrMsg)) {
			$arJson['error'] = $sErrMsg;
		} else {
			if($_REQUEST['dir'] && $_REQUEST['dir'] == '/profile/') {
				$arJson['redirect_url'] = '/profile/';
			}
		}

		$GLOBALS['APPLICATION']->RestartBuffer();
		echo json_encode($arJson);
		return;
	}
	
	if(strlen($sErrMsg)) {
		ADV_set_flashdata('profile_guest_popup_error', $sErrMsg);
	}
	LocalRedirect('/profile/');
	return;
}

/**
 * Update
 */
if($_SERVER['REQUEST_METHOD'] == 'POST' && ($_REQUEST['editFuest'] == 'Y') && check_bitrix_sessid()) {

	$mGuestId = '';
	$sErrMsg = '';

	$sGuestName = isset($_REQUEST['NAME']) ? trim($_REQUEST['NAME']) : '';
	if(!strlen($sGuestName)) {
		$sGuestName = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : $sGuestName;
	}

	if(strlen($sGuestName)) {
		$iGuestId = isset($_REQUEST['ID']) ? intval($_REQUEST['ID']) : 0;
		if($iGuestId > 0) {
			$sGuestNameUpper = ToUpper($sGuestName);
			$arGuestsList = CNiyamaCart::GetCurUserGuestsList();
			if($arGuestsList[$iGuestId]) {
				unset($arGuestsList[$iGuestId]);
			} else {
				$sErrMsg = 'Невозможно отредактировать данные выбранного гостя';
			}
		} else {
			$sErrMsg = 'Неверный запрос';
		}

		if(!strlen($sErrMsg)) {
			foreach($arGuestsList as $arItem) {
				if(ToUpper($arItem['NAME']) == $sGuestNameUpper) {
					$sErrMsg = 'Гость с таким именем уже существует, пожалуйста, укажите другое имя';
					break;
				}
			}
		}

		if(!strlen($sErrMsg)) {
			$iAvaID = $_REQUEST['avatar_id'] ? intval($_REQUEST['avatar_id']) : 0;
			if($iAvaID <= 0) {
				$iAvaID = $_REQUEST['avatar'] ? intval($_REQUEST['avatar']) : $iAvaID;
			}
			//if($iAvaID <= 0) {
			//	$iAvaID = CUsersData::GetFirstDefaultAvatar();
			//}

			$mPhoto = false;
			if(isset($_FILES['PHOTO']) && (!empty($_FILES['PHOTO']['name']))) {
				$mPhoto = $_FILES['PHOTO'];
			}

			$mGuestId = CNiyamaCart::UpdateGuest($iGuestId, $sGuestName, $mPhoto, $iAvaID);
		}
	} else {
		$sErrMsg = 'Имя гостя введено некорректно, пожалуйста, введите другое имя';
	}

	if(strlen($sErrMsg)) {
		ADV_set_flashdata('profile_guest_popup_error', $sErrMsg);
	}
	LocalRedirect('/profile/');
	return;
}

/**
 * Delete
 */
if (isset($_REQUEST['delete_guest']) && $_REQUEST['delete_guest'] && (isset($_REQUEST['guest_id'])) && check_bitrix_sessid()) {

   $iGuestID = $_REQUEST['guest_id'];
   $arGuest = CNiyamaCart::getGuestById($iGuestID);
   if($USER->GetID() == intval($arGuest['PROPERTY_USER_ID_VALUE'])) {
	   CNiyamaCart::deleteGuest($iGuestID);
	   LocalRedirect('/profile/');
   }
}

/**
 * Выводим форму
 */
if (isset($arParams['SHOW_MODAL']) && ($arParams['SHOW_MODAL'] == 'Y')) {
	$isEdit = false;
	$sName  = '';
	$iAva = '';

	$iGuestID = (isset($arParams['GID'])) ? $arParams['GID'] : false;
	if($iGuestID) {
		$isEdit = true;
		$arGuest = CNiyamaCart::getGuestById($iGuestID);
		$sName = $arGuest['NAME'];
		$iAva = $arGuest['PREVIEW_PICTURE'];
		$arAvaData = CFile::ResizeImageGet(
			$arGuest['PREVIEW_PICTURE'], 
			array(
				'width' => 44, 
				'height' => 44
			), 
			BX_RESIZE_IMAGE_EXACT, 
			false
		);
	}

	?><div class="popup__guest">
		<form method="post" name="form1" action="/profile/" enctype="multipart/form-data"><?
			?><input type="hidden" name="<?=(!$isEdit) ? 'addGuest' : 'editFuest'?>" value="Y" >
			<input type="hidden" id="avatar_id" name="avatar_id" value="" ><?
			if($isEdit) {
				?><input type="hidden" name="ID" value="<?=$iGuestID?>"><?
			}
			echo bitrix_sessid_post();

			?><p class="h1 popup__title"><?=(!$isEdit) ? 'Новый гость' : 'Редактирование данных гостей'?></p><?
			?><label class="label">Как зовут гостя?</label>
			<input type="text" name="NAME" class="input _mb_2" value="<?=$sName?>"><br>
			<label class="label">Выберите фото или аватар</label>
			<div class="input__file _photo">Выбрать фото...
				<div id="files" data-id="<?=$iAva?>" class="input__file-pic">
					<img src="<?=(!empty($arAvaData['src']) ? $arAvaData['src'] : '/images/blank.png')?>" alt="">
					<div id="progress" class="progress">
						<div class="progress-bar progress-bar-success" style="width: 0"></div>
					</div>
				</div>
				<input id="fileupload" type="file" name="files[]" class="input__file-input">
			</div>
			<div class="clearfix"></div>
			<p class="avatars"><?
				$arFirstAvatar = CUsersData::getDefaultAvatarList();
				foreach($arFirstAvatar as $row) {
					$arAvaData = CFile::GetFileArray($row['PREVIEW_PICTURE']);
					?><input type="radio" id="ava<?=$row['PREVIEW_PICTURE']?>" name="avatar" value="<?=$row['PREVIEW_PICTURE']?>" id="<?=$row['PREVIEW_PICTURE']?>" class="radio1"<?=($row['PREVIEW_PICTURE'] == $iAva) ? ' checked="checked"' : ''?> />
					<label for="ava<?=$row['PREVIEW_PICTURE']?>" class="radio-avatar1" style="background: url(<?=$arAvaData['SRC']?>) #fff no-repeat center center"></label><?
				}
			?></p>
			<input value="<?=(!$isEdit) ? 'Добавить' : 'Изменить'?>" type="submit" class="btn _style_4" /><?
//			if (!CNiyamaOrders::CheckGuest($iGuestID)) {
//				if ($isEdit) {
					?>   <a class="btn _style_4" href="/profile/?delete_guest=Y&guest_id=<?=$iGuestID?>&sessid=<?=bitrix_sessid()?>">Удалить</a><?
//				}
//			}
		?></form>
	</div><?
	exit();
}

# Выводим ссылку на добавление гостя
?><a class="cabinet__controls-add fancybox" data-fancybox-type="ajax" href="/ajax/getProfileAddGuestForm.php"><span>Добавить гостя</span></a><?

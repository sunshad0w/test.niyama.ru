<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();


$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';

#
# Меняем режим просмотра только при переключении через ajax
#
if ($bAjaxRequest) {
    CCustomProject::ChangeMode();
}

# Проверка на режимы просмотра
if (CCustomProject::isMode(0)) {

    #
    # Фасетный фильтр ( по умолчанию)
    #

    if ($bAjaxRequest) {
        ob_start();
    }

    /* Меню */
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-menu',
        array(
            'MODE' => 'AJAX'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );
    /* Ингредиенты */
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-ingredients-menu',
        array(
            'MENU_TITLE' => 'Ингредиенты'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );
    /* Исключающие ингредиенты */
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-ex-ingredients-menu',
        array(
            'MENU_TITLE' => 'Исключения'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );
	// Специальное меню
	/*
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-special-menu',
        array(
            'MENU_TITLE' => 'Специальное меню'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );
    */

    if ($bAjaxRequest) {
        $sFilters = ob_get_clean();
    }

    if ($bAjaxRequest) {
        echo json_encode(
        	array(
	            'title' => 'ФИЛЬТР',
    	        'linkTitle' => 'К меню',
        	    'filters' => $sFilters,
            	'type' => 'filter'
	        )
		);
    }

} else {

    #
    # Классическая навигация
    #

    if ($bAjaxRequest) {
        ob_start();
    }
    /* Меню */
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.general-menu',
        array(
            'MODE' => 'REQUEST'
        ),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );

    if ($bAjaxRequest) {
        $sFilters = ob_get_clean();
    }


    if ($bAjaxRequest) {
        echo json_encode(
        	array(
	            'title' => 'Меню',
    	        'linkTitle' => 'Фильтр по меню',
        	    'filters' => $sFilters,
            	'type' => 'menu'
	        )
		);
    }

}



                      
                      
                     
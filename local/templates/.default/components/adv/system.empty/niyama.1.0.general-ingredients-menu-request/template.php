<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

//
// ИНГРЕДИЕНТЫ
//

// Из настроек получаем количество по-умолчанию
$iDefaultCount = intval(CNiyamaCustomSettings::getStringValue('default_ingredient_count', 10));


$arIngredientsMenu = $_REQUEST['ings'];
$ar2IngredientsMenu = $_REQUEST['alerg'];

if (isset($sQueryWord)) {
	$allingMenu = CNiyamaCatalog::getIngredients(
		null,
		array(
			'NAME' => trim($sQueryWord)
		)
	);
	if (!empty($alling)) {
		$arIngredientsMenu[] = $allingMenu[0]['ID'];
	}
}

$arSort = array(
	'PROPERTY_IS_POPULAR' => 'DESC',
	'NAME' => 'ASC',
);
$arFilter = array(
	array(
		'LOGIC' => 'OR',
		array(
			'PROPERTY_IS_HIDDEN' => 1,
			'PROPERTY_IS_POPULAR' => 1,
		),
		array(
			'!PROPERTY_IS_HIDDEN' => 1,		
		),	
	)
);
$arSelect = array(
	'PROPERTY_IS_POPULAR',
	'PROPERTY_IS_HIDDEN'
);
$arMenuItems = CNiyamaCatalog::getIngredients($arSelect, $arFilter, $arSort);

if (!empty($arMenuItems)) {
	
	if (!empty($arParams['ISSET'])) {
		$activeMenuItems = array();
		$disabledMenuItems = array();
		foreach ($arMenuItems as $item) {
			if (in_array($item['ID'], $arParams['ISSET'])) {
				$activeMenuItems[] = $item;
			} else {
				$disabledMenuItems[] = $item;
			}
		}	
		$arMenuItems = array_merge($activeMenuItems, $disabledMenuItems);
	}
	
	$i = 0;
	foreach ($arMenuItems as $item) {
		if ($iDefaultCount > 0 && $i + 1 >= $iDefaultCount) {
			break;
		}
		if ($item['PROPERTY_IS_POPULAR_VALUE'] == 0) {
			$iDefaultCount = $i;
			break;
		}
		$i++;
	}

	$arMenuItemsTmp = array();
	foreach ($arMenuItems as $items) {
		if (isset($arParams['ISSET'])) {
			if (in_array($items['ID'], $arParams['ISSET'])) {
				$arMenuItemsTmp[] = $items;
			}
		}
	}

	$iCount = count($arMenuItemsTmp);
	$iCountCheck = ceil($iCount / 2);
	//$iCountCheck = ceil(($iCount - $iDefaultCount) / 2);

	$arDeleteParam = array('MENU_CODE', 'page');

	?><div class="filter__group clearfix" data-category="ing[]"><?
		if($arMenuItemsTmp) {
			?><div class="filter__title"><?=$arParams['MENU_TITLE']?></div><?
		}

		//
		// Элементы по-умолчанию
		//
		?><div class="filter__col"><?
			$i = 1;
			foreach($arMenuItemsTmp as $item) {
				$TmpUrl = array();
				$TmpUrl[$item['ID']] = 'ings[]=' .$item['ID'];

				if (isset($arIngredientsMenu) && (!empty($arIngredientsMenu))) {					
					foreach ($arIngredientsMenu as $q) {
						if ($item['ID'] == $q) {
							unset($TmpUrl[$q]);
						} else {
							$TmpUrl[$q] = 'ings[]='.$q;
						}
					}
				}
				if (isset($ar2IngredientsMenu) && (!empty($ar2IngredientsMenu))) {
					foreach ($ar2IngredientsMenu as $q) {
						if ($item['ID'] == $q) {
							unset($TmpUrl[$q]);
						} else {
							$TmpUrl[$q] = 'alerg[]='.$q;
						}
					}
				}
				if (!empty($TmpUrl)) {
					$url = 'Search/?'.implode('&', $TmpUrl);
				} else {
					$url = '';
				}  
				$cur_page = $APPLICATION->GetCurPage(false);	 
				$pos = strpos($cur_page, 'Search'); 
				if ($pos !== false) {
					$cur_page = substr($cur_page, 0, $pos);
				}	   
				
				?><div data-filter="<?=$item['ID']?>" class="filter__item<?=(isset($arIngredientsMenu)) && (in_array($item['ID'], $arIngredientsMenu)) ? ' _selected' : ''?>">
					<a href="<?=$cur_page.$url?>" class="filter__link _menu-item"><span class="pseudo-link"><?=$item['NAME']?></span></a>
				</div><?
				echo ($i == $iCountCheck) ? '</div><div class="filter__col">' : '';
				$i++;
			}
		?></div><?
		?><div class="clearfix"></div><?
	?></div><?
}

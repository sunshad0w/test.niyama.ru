<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Рестораны
//
$GLOBALS['APPLICATION']->AddHeadString('<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');

$sElementCode = isset($_REQUEST['ELEMENT_CODE']) ? trim($_REQUEST['ELEMENT_CODE']) : '';
$bDetailPage = strlen($sElementCode) ? true : false;

// SEO
if($bDetailPage) {
	// получаем id по символьному коду
	$iRestsListIBlockId = CProjectUtils::GetIBlockIdByCode('restaurants-list', 'restaurants');
	CModule::IncludeModule('iblock');
	$arItem = CIBlockElement::GetList(
		array(),
		array(
			'IBLOCK_ID' => $iRestsListIBlockId,
			'=CODE' => $sElementCode,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y'
		), 
		false,
		false,
		array('ID')
	)->Fetch();
	if($arItem) {
		$obPropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($iRestsListIBlockId, $arItem['ID']);
		$IPROPERTY = $obPropValues->getValues();
		if (!empty($IPROPERTY)) {
			$sDesc = $IPROPERTY['ELEMENT_META_DESCRIPTION'];
			$sTitle = $IPROPERTY['ELEMENT_META_TITLE'];
			$sHeader = $IPROPERTY['ELEMENT_META_TITLE'];
			$sWord = $IPROPERTY['ELEMENT_META_KEYWORDS'];

			if ($sDesc) {
				$GLOBALS['APPLICATION']->SetPageProperty('description', $sDesc);
			}
			if ($sWord) {
				$GLOBALS['APPLICATION']->SetPageProperty('keywords', $sWord);
			}
			$GLOBALS['APPLICATION']->SetTitle($sTitle);
		}
	}
}

// текстовый блок
if(!$bDetailPage) {
	?><div onclick="return {'restsMap': {}}" data-rests-url="/ajax/restaurants.php" data-one-rest-url="/ajax/restaurants.php" class="map js-widget">
		<div id="map"></div>
	</div><?
	?><div onclick="return {'restsInfo': {}}" class="rests-info js-widget"><?
		// компонент также подгружается через аякс
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.restaurants-index-info',
			array(),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	?></div><?
} else {
	?><div onclick="return {'restsMap': {}}" data-rests-url="/ajax/restaurants.php" data-one-rest-url="/ajax/restaurants.php" class="map _one js-widget">
		<div id="map"></div>
	</div><?

	// задается в шаблоне компонента
	$GLOBALS['APPLICATION']->ShowProperty('NIYAMA_RESTAURANTS_DETAIL', '');
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа - страница "Заказ оплачен" (при оплате онлайн)
//
if(!CModule::IncludeModule('sale')) {
	return;
}

// даем побольше времени на обработку заказа FO
sleep(3);

$iOrderId = intval($arParams['ORDER_ID']);
$arOrderData = $iOrderId > 0 ? CNiyamaOrders::GetOrderById($iOrderId, true) : array();

if(!$arOrderData || $arOrderData['CAN_ACCESS'] != 'Y') {
	ShowError('Доступ запрещен');
	return;
}

$arPayments = CNiyamaOrders::GetPayments();
?><h1 class="auth__title _thanks">Спасибо за заказ!</h1>
<div class="ordering-form _thanks"><?
	if($arOrderData['ORDER']['PAY_SYSTEM_ID'] && $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['IS_CASH'] != 'Y') {
		if($arOrderData['ORDER']['PAYED'] == 'Y') {
			?><p><b>Заказ оплачен</b></p><?
			if(!strlen($arOrderData['ORDER']['UF_FO_ORDER_CODE'])) {
				?><p>Наш оператор свяжется с Вами в ближайшее время и уточнит детали заказа.</p><?
			} else {
				?><p>Наш оператор свяжется с Вами в ближайшее время и уточнит детали заказа. ID заказа: <?=$arOrderData['ORDER']['UF_FO_ORDER_CODE']?></p><?
			}

			$arTmp = $arOrderData['ORDER_PROPS']['CUSTOMER_EMAIL'] ? reset($arOrderData['ORDER_PROPS']['CUSTOMER_EMAIL']) : array();
			$sEmail = $arTmp['VALUE'] ? trim($arTmp['VALUE']) : '';
			if(strlen($sEmail)) {
				?><p>Дополнительно мы отправили подтверждение заказа Вам на почту: <?=$sEmail?>.</p><?
			}
		} else {
			?><p><b style="color: red;">Получение оплаты не подтверждено</b></p><?
			?><p>Если вы полностью оплатили заказ и видите данное сообщение, пожалуйста, свяжитесь с администрацией сайта и сообщите о случившейся ошибке. <br />ID заказа на сайте: <?=$iOrderId?></p><?

			// опасно так писать, могут забить весь диск, но как временное решение пусть будет
			if(class_exists('CCustomLog')) {
				$arLogInfo = array(
					'_REQUEST' => $_REQUEST,
					'ORDER' => $arOrderData['ORDER'],
					'_SERVER' => $_SERVER,
				);
				$obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/online_pay_error/'.date('Y-m-d').'_'.$iOrderId.'.log');
				$obCustomLog->Log($arLogInfo);
			}
		}
	} else {
		?><p>Вероятно, произошла какая-то ошибка и вы попали на данную страницу случайно.</p><?
	}

	//
	// Бонусы
	//
	if($GLOBALS['USER']->IsAuthorized()) {
		$arNewCoupons = CNiyamaCoupons::GetListInOrderByUserId($arOrderData['ORDER']['USER_ID'], $arOrderData['ORDER']['ID']);
		if($arNewCoupons) {
			?><div class="ordering-form__coupons">
				<h2 class="ordering-form__coupons-title">После выполнения заказа вы получите бонусные купоны! <span>Вы сможете воспользоваться ими для оплаты следующего заказа!</span></h2>
				<div><?
					foreach($arNewCoupons as $iCouponId) {
						$GLOBALS['APPLICATION']->IncludeComponent(
							'adv:system.empty',
							'niyama.1.0.coupon',
							array(
								'CUPONS_IDS' => $iCouponId,
							),
							null,
							array(
								'HIDE_ICONS' => 'Y'
							)
						);
					}
				?></div>
			</div><?
		}
	}

?></div><?

?><div class="ordering-form__social">Присоединяйтесь к нам в социальных сетях<br>
	<div class="social _auth"><?
		// ссылки на соцсети
		$GLOBALS['APPLICATION']->IncludeFile(
			SITE_DIR.'include_areas/footer-social-links.php',
			array(),
			array(
				'MODE' => 'html',
				'SHOW_BORDER' => false
			)
		);
	?></div>
</div>



<?php 
$stols = array('current', 'guest', 'common');
$products = array();
foreach($stols as $stol) {
	if(!empty($arOrderData['ORDER_CART'][$stol])) {
		foreach($arOrderData['ORDER_CART'][$stol] as $key => $person) {
			if(!empty($person['ITEMS'])) {
				foreach($person['ITEMS'] as $item) {
					if(($item['PROPS']['type'] == 'product') || (empty($item['PROPS']))) {
						if(!empty($products[$item['PRODUCT_ID'].'_'.$item['PRICE']])) {
							$products[$item['PRODUCT_ID'].'_'.$item['PRICE']]['quantity'] += intval($item['QUANTITY']);
						} else {
							$prod_detail = CNiyamaCatalog::GetProductDetail($item['PRODUCT_ID']);
							$products[$item['PRODUCT_ID'].'_'.$item['PRICE']] = array(
								'id' => $item['PRODUCT_ID'],
								'name' => $prod_detail['INFO']['NAME'],
								'category' => $prod_detail['INFO']['SECTION']['NAME'],
								'price' => $item['PRICE'],
								'quantity' => intval($item['QUANTITY']),
							);
						}
					}
				}
			}
		}
	}
}

?>
<script>
convead('event', 'purchase', {
	order_id: <?=$arOrderData['ORDER']['ID']?>,
	revenue: <?=$arOrderData['ORDER_CART']['TOTAL_PRICE']?>,
	items: [
                <?php
	if(!empty($products)) {
		foreach ($products as $product) {
			?>
			{product_id: '<?=$product['id']?>', qnt: <?=$product['quantity']?>, price: <?=$product['price']?>},
                <?php
        }
    }
    ?>
]},{
    first_name: '<?=$GLOBALS['USER']->GetFirstName();?>',
    last_name: '<?=$GLOBALS['USER']->GetLastName();?>',
    email: '<?=$GLOBALS['USER']->GetEmail();?>',
    phone: ''
  });
</script>

<?

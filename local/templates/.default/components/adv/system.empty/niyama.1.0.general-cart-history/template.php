<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

// $USER->IsAuthorized()
if($USER->IsAuthorized()) {

	CModule::IncludeModule('sale');

	$arCurrentOrderData = CNiyamaCart::getCurrentOrderData();

	$arHistoryOrders = array();
	$dbOrder = CSaleOrder::GetList(
		 array(
		 	'ID' => 'DESC'
		 ),
		 array(
			'USER_ID' => intval($USER->GetID()),
			'CANCELED' => 'N', 
			'@STATUS_ID' => array('E', 'F', 'N', 'P', 'R', 'S', 'T'), 
			'LID' => 's1'
		)
	);
	while($arOrder = $dbOrder->Fetch()) {
		if($arCurrentOrderData['ID'] != $arOrder['ID']) {
			$arHistoryOrders[] = $arOrder;
		}
	}

	if($arHistoryOrders) {
		echo '<div onclick="return {cartHistory: {}}" class="cart-history pull-left js-widget">';
	} else {
		echo '<div class="cart-history cart-history__empty pull-left">';
	}

	if (!$arCurrentOrderData) {
		$cDate = ADV_textDate(time(), false, 'timestamp');
		$cTitle = 'КОРЗИНА';
	} else {
		$cDate = $arCurrentOrderData['DATE'];
		$cTitle = $arCurrentOrderData['NAME'];
	}

	if($arHistoryOrders) {
		?><a class="cart-history__title"> <span><?=$cTitle?> </span></a><?
	} else {
		?><a class="cart-history__title">
			<span><?=$cTitle?></span>
		</a><?
	}
	?><div class="cart-history__date"><?=$cDate?></div><?

	?><div class="cart-history__list-wrap">
		<div class="cart-history__list-scroll">
			<ul class="cart-history__list"><?
				if ($arCurrentOrderData) {
					?><li class="cart-history__list-item">
						<a href="#" class="cart-history__list-link" data-id="cart">
							<div class="cart-history__list-date">КОРЗИНА</div>
							<div class="cart-history__list-sum"><?= ADV_textDate(time(), false, 'timestamp') ?></div>
						</a>
					</li><?
				}

				foreach($arHistoryOrders as $arOrder) {
					?><li class="cart-history__list-item">
						<a href="#" class="cart-history__list-link" data-id="<?=$arOrder['ID']?>">
							<div class="cart-history__list-date"><?= ADV_textDate($arOrder['DATE_INSERT'], false) ?></div>
							<div class="cart-history__list-sum"><?=CProjectUtils::FormatNum($arOrder['PRICE'])?><span class="rouble">a</span></div>
						</a>
					</li><?
				 }
			?></ul>
		</div>
	</div><?
	echo '</div>';
} else {
	?><div class="cart-history cart-history__empty pull-left">
		<a class="cart-history__title">
			<span>КОРЗИНА</span>
			<div class="cart-history__list-sum"><?= ADV_textDate(time(), false, 'timestamp') ?></div>
		</a>
	</div><?
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

/**
 * Повторяем заказ
 */
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'replace_order' && isset($_REQUEST['order_id']) && check_bitrix_sessid()) {

	CNiyamaCart::setCurrentOrderData(false);

	$iOrderId = (intval($_REQUEST['order_id']) > 0) ? intval($_REQUEST['order_id']) : 0;
	if($iOrderId) {
		$bCanAccess = false;
		$arOrder = CNiyamaOrders::GetOrderById($iOrderId, true);

		if ($arOrder) {
			if ($arOrder['CAN_ACCESS'] == 'Y') {
				$bCanAccess = true;
			}
		}

		if (!$bCanAccess) {
			LocalRedirect('/');
		}

		$arCartItems = $arOrder['ORDER_CART'];
		// Добавляем к текущим
		if (!empty($arCartItems['current'][0]['ITEMS'])) {
			foreach ($arCartItems['current'][0]['ITEMS'] as $cRow) {
				if ($cRow['PROPS']['type'] == 'product') {
					$iGuestID = false;
					if ($iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false)) {
						//
					}
				}
			}
		}

		// Добавляем гостей
		if (!empty($arCartItems['guest'])) {
			foreach ($arCartItems['guest'] as $id => $gRow) {
				foreach ($gRow['ITEMS'] as $cRow) {
					if ($cRow['PROPS']['type'] == 'product') {
						$iGuestID = $id;
						if ($iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false)) {
							//
						}
					}
				}
			}
		}
		# Общий стол
		if (!empty($arCartItems['common'][0]['ITEMS'])) {
			foreach ($arCartItems['common'][0]['ITEMS'] as $cRow) {
				if ($cRow['PROPS']['type'] == 'product') {
					$iGuestID = CNiyamaCart::$iForAllGuestID;
					if($iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false)) {
						//
					}
				}
			}
		}
	}
	LocalRedirect('/');
}

# Общая функция вывода элемента корзины
if(!function_exists('PrintNiyamaCartItem')) {
	function PrintNiyamaCartItem($arBasketItem) {
		if (isset($arBasketItem['PROPS']['type'])) {
			switch ($arBasketItem['PROPS']['type']) {

				# Купон
				case CNiyamaCart::$arCartItemType[1]:
				   	PrintNiyamaCuponsCartItem($arBasketItem);
				break;

				# Доп компонент
				case CNiyamaCart::$arCartItemType[2]:
					NiyamaPrintDopCompItem($arBasketItem);
				break;

				# Блюдо
				default:
					PrintNiyamaProductCartItem($arBasketItem);
				break;
			}
		}
	}
}
if(!function_exists('NiyamaPrintDopCompItem')) {
	function NiyamaPrintDopCompItem($arBasketItem) {

		$arDopCompDetail = CNiyamaCatalog::GetDopCompDetailByImpElementId($arBasketItem['PRODUCT_ID']);
		if(!empty($arDopCompDetail)) {
			$arImg = CFile::GetFileArray($arDopCompDetail['PREVIEW_PICTURE']);
			?><div onclick="return {'goods': {}}" class="goods _table s-widget">
				<div class="goods__wrap">
					<div class="goods__item">
						<div class="goods__image"><img width="188" src="<?=$arImg['SRC']?>" alt="<?$arItem["NAME"]?>"></div>
						<div class="goods__desc">
							<p class="goods__name"><?=$arDopCompDetail['NAME']?></p>
							<div class="goods__composition"></div>
							<div class="goods__cost"><?=$arBasketItem['PRICE']?> <span class="rouble">a </span></div>
						</div>
					</div>
				</div>
			<div class="goods__static-count"><?= $arBasketItem['QUANTITY'] ?> шт.</div><?
		?></div><?

		}

	}
}
if(!function_exists('NiyamaCartSort')) {
	function NiyamaCartSort(&$prods) {
		$arCoupon = array();
		foreach($prods as $key => $prod) {
			if (isset($prod['PROPS']['type'])) {
				switch ($prod['PROPS']['type']) {

					# Купон
					case CNiyamaCart::$arCartItemType[1]:
						$arCoupon[] = $prod;
						unset($prods[$key]);
					break;

					# Блюдо
					default:
						//
					break;
				}
			}
		}
		if (!empty($arCoupon)) {
			$prods = array_merge($prods, $arCoupon);
		}
	}
}

# Функция вывода Блюд в корзине
if(!function_exists('PrintNiyamaProductCartItem')) {
	function PrintNiyamaProductCartItem($arBasketItem) {

		$by_coupon = ($arBasketItem['PROPS']['BY_COUPON'] == 'Y');
		if (!empty($arBasketItem['PROPS']['SHOW_IN_CART'])) {
			$show_in_cart = ($arBasketItem['PROPS']['SHOW_IN_CART'] == 'Y');
			if (!$show_in_cart) {
				return false;
			}
		}
		static $iMaxQuantity = 0;
		if(!$iMaxQuantity) {
			# Получаем максимальное кол-во блюд в заказе.
			$iMaxQuantity = intval(CNiyamaCustomSettings::GetStringValue('order_dish_max_quantity', 12));
		}
		# Фото
		$arProduct = CNiyamaCatalog::GetProductDetail($arBasketItem['PRODUCT_ID'], true);
		$bDisabled = $arProduct['INFO']['ACTIVE'] != 'Y';
		if($arProduct['INFO']) {
			$arImg = CFile::GetFileArray($arProduct['INFO']['PREVIEW_PICTURE']);
			# Получаем Похожие товары.
			$sTmpClass = $bDisabled ? ' _disabled' : '';
			?><div data-id="<?=$arBasketItem['ID']?>" onclick="return {'goods': {}}" class="goods _table <?=($by_coupon) ? '_by-coupon' : 'js-widget'?>">
				<div class="goods__wrap">
					<div class="goods__item<?=$sTmpClass?>" data-id="<?=$arProduct['INFO']['ID']?>">
						<div class="goods__image"><img width="188" src="<?=$arImg['SRC']?>" alt=""></div>
						<div class="goods__desc">
							<div class="goods__composition"><p class="goods__name"><?=$arProduct['INFO']['NAME']?></p><?
								/*
								if(!empty($arProduct['EXCLUDE_INGREDIENT'])) {
									$ex_ings = array();
									foreach($arProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
										if (!in_array($elem, $arProduct['ALL_INGREDIENT'])) {
											$ex_ings[$key] = $elem;
										}
									}
									if (!empty($ex_ings)) {
										$arProduct['ALL_INGREDIENT'] = array_merge($arProduct['ALL_INGREDIENT'], $ex_ings);
									}
								}
								*/
								echo implode(', ', $arProduct['ALL_INGREDIENT']);
							?></div>
							<div class="goods__cost"><?=$arBasketItem['PRICE']?> <span class="rouble">a </span></div>
						</div>
					</div>
				</div>
				<div class="goods__static-count">x<?= ceil($arBasketItem['QUANTITY']); ?></div><?
				if($bDisabled) {
					?><div class="goods__not-in-menu" style="display: block">Отсутствует в меню</div><?
				}
			?></div><?
		}
	}
}


# Функция вывода купонов в корзине
if(!function_exists('PrintNiyamaCuponsCartItem')) {
	function PrintNiyamaCuponsCartItem($arBasketItem)
	{
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.coupon',
			array(
				'CUPONS_IDS' => $arBasketItem['PRODUCT_ID'],
				'SHOW_USED' => 'N',
				'SHOW_IN_CART' => 'Y',
				'ID_IN_CART' => $arBasketItem['ID'],
				'VAL_COUPON' => $arBasketItem['PROPS']['val_coupon'],
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	}
}

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';
$iOrderId = (isset($arParams['ORDER_ID']) && intval($arParams['ORDER_ID']) > 0) ? intval($arParams['ORDER_ID']) : 0;
$arOrder = array();
$bCanAccess = false;

if ($iOrderId) {
	$arOrder = CNiyamaOrders::GetOrderById($iOrderId, true);

	$arStarus = CNiyamaOrders::GetStatusById($arOrder['ORDER']['STATUS_ID']);
	$arOrder['ORDER']['STATUS_NAME'] = $arStarus['NAME'];

	if ($arOrder) {
		if ($arOrder['CAN_ACCESS'] == "Y") {
			$bCanAccess = true;
		}
	}
}

if (!$bCanAccess) {
	ShowError("Заказ не найден");
	return;
}

# Получаем ID Гостя
$iGuestID = (isset($_REQUEST['gid'])) ? $_REQUEST['gid'] : false;

if(!$iGuestID) {
	$iGuestIDTmp = CNiyamaCart::getOrderGuestID();
	$iGuestID = ($iGuestIDTmp) ? $iGuestIDTmp : false;
} else {
	$iGuestIDTmp = CNiyamaCart::getOrderGuestID();
	if((!$iGuestIDTmp) || ($iGuestID != $iGuestIDTmp)) {
		$iGuestID = ($iGuestID != 'current') ? $iGuestID : false;
		CNiyamaCart::setOrderGuestID($iGuestID);
	}
}

$arCartItems = $arOrder['ORDER_CART'];

$bIsCurrent  = (isset($arCartItems['current']) && (!empty($arCartItems['current'])) ) ? true : false;
$bIsGuest	= (isset($arCartItems['guest']) && (!empty($arCartItems['guest'])) ) ? true : false;
$bIsCommon   = (isset($arCartItems['common']) && (!empty($arCartItems['common'])) ) ? true : false;

# Список Истории заказов
$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.general-cart-history',
	array(),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

$isSeeCurrent = ($iGuestID == "current") || (!$iGuestID);
$isSeeCommon = ($iGuestID == "common");
$isSeeGuest = ($iGuestID != "current" && $iGuestID != "common");

# Выбираем купоны для всех столов
$ar_coupons_each_guest = array();

if($arCartItems['common'][0]['ITEMS']) {
	foreach($arCartItems['common'][0]['ITEMS'] as $key => $prod) {
		if($prod['PROPS']['type'] == CNiyamaCart::$arCartItemType[1]) {
			if(CNiyamaCoupons::GetTypeForAll($prod['PRODUCT_ID'])) {
				$ar_coupons_each_guest[$key] = $prod;
				unset($arCartItems['common'][0]['ITEMS'][$key]);
			}
		}
	}
}


# Далее выводим список уже добавленных ранее гостей
if(!$bAjaxRequest) {

	?><div onclick="return{'guests': {}}" class="guests pull-left js-widget _order">
		<div class="guests__list"><?
			# Если смотрим себя то показываем одно!
			if ($isSeeCurrent) {

				$iSubPrice = ($bIsCurrent) ? $arCartItems['current'][0]['PRICE'] : "0";
				# Получаем аватар
				$arAvaData = CFile::ResizeImageGet(CUsersData::getPersonalPhoto(), array('width'=>44, 'height'=>44), BX_RESIZE_IMAGE_EXACT, false);
				$sScrAvatar = $arAvaData['src'];
				$sNameTab = CUsersData::getUserName();

			} else if ($isSeeGuest) {
				# Если смотри гостя

				# Если вкладка « на всех » то другой аватар
				if ($iGuestID == CNiyamaCart::$iForAllGuestID) {

					$iSubPrice  = (isset($arCartItems['common'][0]['PRICE'])) ? $arCartItems['common'][0]['PRICE'] : "0";
					$sScrAvatar = CNiyamaCart::$sForAllAvatar;
					$sNameTab   = CNiyamaCart::$sForAllName;

				} else {

					$iSubPrice = ($bIsGuest && (isset($arCartItems['guest'][$iGuestID]['PRICE']))) ? $arCartItems['guest'][$iGuestID]['PRICE'] : "0";
					# Получаем аватар
					$arGidData = CNiyamaCart::getGuestById($iGuestID);
					$arAvaData = CFile::ResizeImageGet($arGidData['PREVIEW_PICTURE'], array('width'=>44, 'height'=>44), BX_RESIZE_IMAGE_EXACT, false);
					$sScrAvatar = $arAvaData['src'];
					$sNameTab = $arGidData['NAME'];
				}
			} else if ($isSeeCommon) {

				$iSubPrice = ($bIsCurrent) ? $arCartItems['common'][0]['PRICE'] : "0";
				# Получаем аватар
				$sScrAvatar = CNiyamaCart::$sForAllAvatar;
				$sNameTab =  CNiyamaCart::$sForAllName;

			}

			?><div class="guests__card _current"><?
				?><div class="guests__card-img"><img src="<?= $sScrAvatar ?>"></div>
				<div class="guests__card-sum"> <i id="guests__card-sum"><?= $iSubPrice ?></i> <span class="rouble">a</span></div>
				<div class="guests__card-name"> <?= $sNameTab ?> </div>
			</div><?
			?><a href="#" class="guests__list-choose"></a><?

			?><div class="guests__list-wrap">
				<div class="guests__list-list"><?

					# Если не смотрим себя то добавляем в список себя.
					if (!$isSeeCurrent) {
						$iSubPriceUser = ($bIsCurrent) ? $arCartItems['current'][0]['PRICE'] : "0";
						# Получаем аватар
						$arAvaData = CFile::ResizeImageGet(CUsersData::getPersonalPhoto(), array('width'=>44, 'height'=>44), BX_RESIZE_IMAGE_EXACT, false);
						$sScrAvatar = $arAvaData['src'];
						$sNameTab = CUsersData::getUserName();

						?><a href="#" class="guests__card _big" data-gid="current">
							<div class="guests__card-img"><img src="<?=$sScrAvatar?>" alt=""></div>
							<div class="guests__card-sum"><?=$iSubPriceUser?> </div>
							<div class="guests__card-name"><?=$sNameTab?></div>
						</a><?
					}

					# На всех
					if (!empty($arCartItems['common'])) {
						if (!$isSeeCommon) {

							$igProce = (isset($arCartItems['common'][0]['PRICE'])) ? $arCartItems['common'][0]['PRICE'] : "0";

							?><a href="#" class="guests__card _big" data-gid="common">
							<div class="guests__card-img"><img src="<?= CNiyamaCart::$sForAllAvatar ?>"></div>
							<div class="guests__card-sum"><?= $igProce ?>  <span class="rouble">a</span></div>
							<div class="guests__card-name"><?= CNiyamaCart::$sForAllName ?></div>
							</a><?
						}
					}

					if (!empty($arCartItems['guest'])) {
						foreach ($arCartItems['guest'] as $id => $grow) {

							$igProce = ((isset($arCartItems['guest'][$id]['PRICE']))) ? $arCartItems['guest'][$id]['PRICE'] : "0";
							$arGidData = CNiyamaCart::getGuestById($id);

							if ($iGuestID != $id) {
								# Получаем аватар
								$dGuestAva = CFile::ResizeImageGet($arGidData['PREVIEW_PICTURE'], array('width'=>44, 'height'=>44), BX_RESIZE_IMAGE_EXACT, false);
								?><a href="#" class="guests__card _big" data-gid="<?=$id?>">
									<div class="guests__card-img"><img src="<?=$dGuestAva['src'] ?>"></div>
									<div class="guests__card-sum"><?=$igProce?>  <span class="rouble">a</span></div>
									<div class="guests__card-name"><?=$arGidData['NAME']?></div>
								</a><?
							}
						}
					}
				?></div>
			</div>
		</div><?

		$sStatusName = 'Выполняется';
		if ($arOrder['ORDER']['STATUS_ID'] == 'F') {
			$sStatusName = 'Выполнен';
		} else {
			if ($arOrder['ORDER']['CANCELED'] == 'Y') {
				$sStatusName = 'Отменен';
			}
		}

		$sNum = strlen($arOrder['ORDER']['UF_FO_ORDER_CODE']) ? $arOrder['ORDER']['UF_FO_ORDER_CODE'] : ' н/д';
		?><div class="guests__add pull-right">
			<a href="#" class="guests__add-sign _disabled">Заказ №<?=$sNum?><br><?=$sStatusName?></a>
			<div class="guests__add-wrap">
			</div>
		</div>
	</div><?

	?><div class="cart-buy pull-right"><?
		?><span class="cart-buy__sum"><i><?=intval($arOrder['ORDER']['PRICE'])?></i> <span class="rouble">a</span></span><?
		?><a href="/?action=replace_order&order_id=<?= $iOrderId ?>&sessid=<?= bitrix_sessid() ?>" class="cart-buy__button btn _style_1 pull-right">Повторить</a><?
	?></div>
	<div class="clearfix"></div><?
}

//
// Карточки товаров в корзине
//
if(!$bAjaxRequest) {
	echo '<div class="cart__table">';
}


# Если смотрим текущего пользователя
if(!$iGuestID) {
	# Если у него есть товары
	if($bIsCurrent) {
		if($arCartItems['current'][0]['ITEMS']) {
			NiyamaCartSort($arCartItems['current'][0]['ITEMS']);
			foreach($arCartItems['current'][0]['ITEMS'] as $prod) {
				PrintNiyamaCartItem($prod);
			}
		}
	}

} else {

	if ($isSeeCommon) {
		# Если общая вкладка
		if(!empty($arCartItems['common'])) {
			if($arCartItems['common'][0]['ITEMS']) {
				NiyamaCartSort($arCartItems['common'][0]['ITEMS']);
				foreach($arCartItems['common'][0]['ITEMS'] as $prod) {
					PrintNiyamaCartItem($prod);
				}
			}

		}
	} else {
		if($isSeeGuest) {
			$bIsNeedleGuest = (isset($arCartItems['guest'][$iGuestID])) ? true : false;
			# Если есть тот кто нам нужен
			if($bIsNeedleGuest) {
				if($arCartItems['guest'][$iGuestID]['ITEMS']) {
					NiyamaCartSort($arCartItems['guest'][$iGuestID]['ITEMS']);
					foreach($arCartItems['guest'][$iGuestID]['ITEMS'] as $prod) {
						PrintNiyamaCartItem($prod);
					}
				}
			}
		}
	}
}


foreach ($ar_coupons_each_guest as $arCoupon) {
	$GLOBALS['APPLICATION']->IncludeComponent(
		'adv:system.empty',
		'niyama.1.0.coupon',
		array(
			'CUPONS_IDS' => (!empty($arCoupon['PRODUCT_ID'])) ? $arCoupon['PRODUCT_ID'] : '',
			'SHOW_USED' => 'N',
			'SHOW_IN_CART' => 'Y',
			'ID_IN_CART' => (!empty($arCoupon['ID'])) ? $arCoupon['ID'] : '',
			'RES' => $arCoupon,
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
}

if(!$bAjaxRequest) {
	echo '</div>';
}


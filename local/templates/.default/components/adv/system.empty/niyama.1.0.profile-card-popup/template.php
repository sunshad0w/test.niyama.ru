<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
//
// Попапы работы с дисконтными картами в личном кабинете пользователя
//
$iCurUserId = $GLOBALS['USER']->GetId();
$sCurUserLogin = $GLOBALS['USER']->GetLogin();
$sCardConfirmUrlTemplate = '/confirm_card/?uid=#USER_ID#&code=#CONFIRM_CODE#';
if ($iCurUserId <= 0) {
    ShowError('Пожалуйста, авторизуйтесь');
    return;
}

// максимальное количество попыток проверки номера карты
$iCheckNumMaxAttempts = 5;
// максимальное количество попыток идентификации карты
$iConfirmationMaxAttempts = 5;
// максимальное количество попыток смены даты рождения
$iBirthdayMaxAttempts = 1;

$arCardData = CNiyamaIBlockCardData::GetCardData($iCurUserId, true);

$bShowCheckNumForm = false;
$bShowEditForm = false;
$bShowEditSuccess = false;
$bShowSupportLink = false;
$bShowNewCardInfoForm = false;
$bShowConfirmForm = false;
$bShowWaitForm = false;
$bShowConfirmSuccess = false;
$bShowNewCardSuccessForm = false;
$arFormErrors = array();
$sCardNum = $arCardData && isset($arCardData['PROPERTY_CARDNUM_VALUE']) ? $arCardData['PROPERTY_CARDNUM_VALUE'] : '';
$sCardType = $sCardNum ? CNiyamaDiscountCard::GetCardTypeByCardNum($sCardNum) : '';

// контроль по юзеру, если залогинился под другим логином, то сбрасываем старые данные сессии
if ($_SESSION['_NIYAMA_CARDS_']['USER_ID'] && $_SESSION['_NIYAMA_CARDS_']['USER_ID'] != $iCurUserId) {
    unset($_SESSION['_NIYAMA_CARDS_']);
}
if (!isset($_SESSION['_NIYAMA_CARDS_']['USER_ID'])) {
    $_SESSION['_NIYAMA_CARDS_']['USER_ID'] = $iCurUserId;
}

// контроль попыток ввода данных
if (!isset($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) || !is_array($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'])) {
    $_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'] = array();
}
if (!isset($_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM']) || !is_array($_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM'])) {
    $_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM'] = array();
}

if (!$arCardData) {
    if (count($_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM']) >= $iCheckNumMaxAttempts) {
        $bShowCheckNumForm = true;
        $arFormErrors['ERR_ATTEMPTS'] = 'Вы использовали максимально допустимое количество попыток активации карты';
    } elseif (count($_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM']) >= $iConfirmationMaxAttempts) {
        $bShowConfirmForm = true;
        $arFormErrors['ERR_ATTEMPTS'] = 'Вы использовали максимально допустимое количество попыток идентификации карты';
    }
}
elseif ($arCardData['ACTIVE'] == 'Y') {
    // обновим карточку по полям в ПДС (если не POST-запрос)
    if ($_SERVER['REQUEST_METHOD'] != 'POST' && strlen($arCardData['PROPERTY_CARDNUM_VALUE'])) {
        $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($arCardData['PROPERTY_CARDNUM_VALUE']);
        if ($arPdsCardInfo['Account'] && $arPdsCardInfo['User']) {
            $iCardDataElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arPdsCardInfo, $iCurUserId, array(), true, true);
            if ($iCardDataElementId) {
                if ($arPdsCardInfo['Account']['Discount']) {
                    CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iCurUserId, CNiyamaIBlockCardData::GetTrueDiscount($arPdsCardInfo['Account']['Discount']));
                }

                $arCardData = CNiyamaIBlockCardData::GetCardData($iCurUserId, true, true);
            }
        }
    }

    $arCardData['_CAN_EDIT_BIRTHDAY_'] = 'N';
    // получим историю изменения карты по полю дата рождения, чтобы нельзя было менять д.р. больше допустимого числа раз
    if ($arCardData['PROPERTY_USER_ID_VALUE'] && $arCardData['PROPERTY_CARDNUM_VALUE']) {
        $sTmpHLEntityClass = CNiyamaIBlockCardData::GetCardsHistoryEntityClass();
        if ($sTmpHLEntityClass) {
            $dbItems = $sTmpHLEntityClass::GetList(
                array(
                    'order' => array(
                        'ID' => 'ASC'
                    ),
                    'filter' => array(
                        '=UF_ELEMENT_ID' => $arCardData['ID'],
                        '=UF_USER_ID' => $arCardData['PROPERTY_USER_ID_VALUE'],
                        '=UF_CARDNUM' => $arCardData['PROPERTY_CARDNUM_VALUE'],
                        array(
                            array(
                                '!UF_BIRTHDAY' => $arCardData['PROPERTY_BIRTHDAY_VALUE'],
                            ),
                            array(
                                '!=UF_BIRTHDAY' => false,
                            ),
                        ),
                    ),
                    'select' => array(
                        'ID',
                    )
                )
            );
            if ($dbItems->GetSelectedRowsCount() < $iBirthdayMaxAttempts) {
                $arCardData['_CAN_EDIT_BIRTHDAY_'] = 'Y';
            }
        }
    }
}

//
// Обработка постинга форм
//
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $sFormType = isset($_POST['FORM_TYPE']) ? trim($_POST['FORM_TYPE']) : '';
    if (!check_bitrix_sessid()) {
        $arFormErrors['SESS'] = 'Ваша сессия истекла, пожалуйста, повторите попытку';
    }
    if ($arCardData) {
        switch ($sFormType) {
            case 'edit_card_info':
                $sTmpCheckCard = $sCardNum;
                $bShowEditForm = true;

                if (!strlen($sTmpCheckCard)) {
                    $arFormErrors['CARD_NUM_EMPTY'] = 'Нет информации о номере карты';
                    $bShowSupportLink = true;
                } elseif ($arCardData['ACTIVE'] != 'Y') {
                    $bShowWaitForm = true;
                    $bShowEditForm = false;
                    $arFormErrors['CARD_NOT_ACTIVE'] = 'Карта находится на стадии активации';
                }

                if (!$arFormErrors) {
                    $_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'] = $sTmpCheckCard;
                    $sTmpCardType = CNiyamaDiscountCard::GetCardTypeByCardNum($sTmpCheckCard);
                    if (!$sTmpCardType) {
                        $arFormErrors['INCORRECT_TYPE'] = 'Карта с таким номером не разрешена в системе, проверьте номер карты и повторите ввод';
                    } else {
                        // проверяем карту в ПДС
                        $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sTmpCheckCard);
                        if ($arPdsCardInfo && $arPdsCardInfo['Number'] == $sTmpCheckCard) {
                            if (!$arPdsCardInfo['Account']) {
                                $arFormErrors['CARD_ERR'] = 'Указанная карта не зарегистрирована в центральной системе';
                                $bShowSupportLink = true;
                            } else {
                                // карта есть, проверяем наличие данных: ФИО, тел, e-mail, адрес, дата рождения
                                $arFields = array(
                                    'holder' => '',
                                    'email' => '', //*
                                    'tel1' => '', // *
                                    'birthday' => '',
                                    'address' => '',
                                );
                                // ---
                                $arTmpName = array();
                                if (strlen(trim($_POST['LAST_NAME']))) {
                                    $arTmpName[] = trim($_POST['LAST_NAME']);
                                } else {
                                    $arFormErrors['ERR_FIELD_LAST_NAME'] = 'Не указана фамилия владельца карты';
                                }
                                if (strlen(trim($_POST['NAME']))) {
                                    $arTmpName[] = trim($_POST['NAME']);
                                } else {
                                    $arFormErrors['ERR_FIELD_NAME'] = 'Не указано имя владельца карты';
                                }
                                if (strlen(trim($_POST['SECOND_NAME']))) {
                                    $arTmpName[] = trim($_POST['SECOND_NAME']);
                                }
                                // если заданы только имя и фамилия, то порядок будет: Иван Иванов, иначе: Иванов Иван Иванович
                                $arTmpName = count($arTmpName) == 2 ? array_reverse($arTmpName) : $arTmpName;
                                $arFields['holder'] = implode(' ', $arTmpName);

                                // ---
                                if (strlen(trim($_POST['EMAIL']))) {
                                    if (check_email($_POST['EMAIL'])) {
                                        $arFields['email'] = trim($_POST['EMAIL']);
                                    } else {
                                        $arFormErrors['ERR_FIELD_EMAIL'] = 'E-mail владельца карты указан некорректно';
                                    }
                                } else {
                                    $arFormErrors['ERR_FIELD_EMAIL'] = 'Не указан e-mail владельца карты';
                                }

                                // ---
                                if (strlen(trim($_POST['PHONE']))) {
                                    $arFields['tel1'] = trim($_POST['PHONE']);
                                } else {
                                    $arFormErrors['ERR_FIELD_PHONE'] = 'Не указан телефон владельца карты';
                                }

                                // ---
                                if ($arCardData['_CAN_EDIT_BIRTHDAY_'] == 'Y') {
                                    if (strlen(trim($_POST['BIRTHDAY']))) {
                                        $sTmpVal = trim($_POST['BIRTHDAY']);
                                        $obTmpDate = date_create($sTmpVal);
                                        if ($obTmpDate) {
                                            $arFields['birthday'] = $obTmpDate->Format('d.m.Y');
                                        } else {
                                            $arFormErrors['ERR_FIELD_BIRTHDAY'] = 'Дата рождения указана некорректно';
                                        }
                                    }
                                }

                                // ---
                                $arTmpVal = array();
                                if (strlen(trim($_POST['CITY']))) {
                                    $arTmpVal[] = 'Город: ' . trim($_POST['CITY']);
                                }
                                if (strlen(trim($_POST['STREET']))) {
                                    $arTmpVal[] = 'Улица: ' . trim($_POST['STREET']);
                                }
                                if (strlen(trim($_POST['HOME']))) {
                                    $arTmpVal[] = 'Дом: ' . trim($_POST['HOME']);
                                }
                                if (strlen(trim($_POST['HOUSING']))) {
                                    $arTmpVal[] = 'Корпус: ' . trim($_POST['HOUSING']);
                                }
                                if (strlen(trim($_POST['BUILDING']))) {
                                    $arTmpVal[] = 'Строение: ' . trim($_POST['BUILDING']);
                                }
                                if (strlen(trim($_POST['APARTMENT']))) {
                                    $arTmpVal[] = 'Квартира: ' . trim($_POST['APARTMENT']);
                                }
                                $arFields['address'] = implode("\n", $arTmpVal);

                                if (!$arFormErrors) {
                                    // обновляем карту
                                    $arResponse = CNiyamaDiscountCard::EditPdsCard($sTmpCheckCard, $arFields);
                                    if ($arResponse['GetErrorText']) {
                                        $arFormErrors['REGISTER_ERROR'] = 'Ошибка редактирования карты';
                                        $bShowSupportLink = true;
                                    } elseif ($arResponse['NoXml']) {
                                        $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка';
                                        $bShowSupportLink = true;
                                    } elseif (!$arPdsCardInfo['Account'] || !$arPdsCardInfo['User']) {
                                        $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка';
                                        $bShowSupportLink = true;
                                    } else {
                                        // карта отредактирована, сохраним данные на сайте
                                        // надо учитывать $arResponse
                                        $iCardDataElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arResponse, $iCurUserId, array('ACTIVE' => 'Y', 'CODE' => ''), true, false);
                                        if ($iCardDataElementId) {
                                            if ($arResponse['Account']['Discount']) {
                                                CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iCurUserId, CNiyamaIBlockCardData::GetTrueDiscount($arResponse['Account']['Discount']));
                                            }

                                            CNiyamaDiscountCard::DeleteCardIdentifyData($iCurUserId);
                                            $bShowEditSuccess = true;
                                        } else {
                                            $arFormErrors['UNKNOWN_ERROR'] = 'Не удалось привязать карту к пользователю. Попробуйте повторить операцию позже, либо обратитесь в тех.поддержку сайта';
                                            $bShowSupportLink = true;
                                        }
                                    }
                                }
                            }
                        } else {
                            $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка. Попробуйте повторить операцию';
                            $bShowSupportLink = true;
                        }
                    }
                }

                if ($bShowNewCardInfoForm) {
                    $bShowCheckNumForm = false;
                }
                break;
        }

    } else {
        switch ($sFormType) {
            case 'check_card_num':
                // Обработка постинга формы "Проверка карты"
                $bShowCheckNumForm = true;

                if (!$arFormErrors) {
                    // проверка номера карты
                    $sTmpCheckCard = isset($_POST['CHECK_CARD_NUM']) ? trim($_POST['CHECK_CARD_NUM']) : '';
                    if (strlen($sTmpCheckCard)) {
                        $sTmpCardType = CNiyamaDiscountCard::GetCardTypeByCardNum($sTmpCheckCard);
                        if (!$sTmpCardType) {
                            $_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'][] = $sTmpCheckCard;
                            $arFormErrors['INCORRECT_TYPE'] = 'Карта с таким номером не разрешена в системе, проверьте номер карты и повторите ввод';
                        } else {
                            // проверяем, закреплен ли уже за кем-нибудь введенный номер карты
                            $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sTmpCheckCard);
                            if ($arPdsCardInfo && $arPdsCardInfo['Number'] == $sTmpCheckCard) {
                                if ($arPdsCardInfo['Account']) {
                                    // карта существует, смотрим результат проверки по активности
                                    if ($arPdsCardInfo['_ERRORS_']) {
                                        $arFormErrors = array_merge($arFormErrors, $arPdsCardInfo['_ERRORS_']);
                                    }

                                    if (!$arFormErrors) {
                                        // с картой нет проблем, можем запрашивать подтверждение ее принадлежности посетителю
                                        $bShowCheckNumForm = false;
                                        $bShowConfirmForm = true;
                                        $sCardNum = $sTmpCheckCard;
                                    } else {
                                        $bShowSupportLink = true;
                                    }
                                } else {
                                    // такой карты еще нет,	проверяем наличие данных: ФИО, тел, e-mail, адрес, дата рождения
                                    // если чего-то нет, то запрашиваем информацию для регистрации новой карты
                                    // только простые карты можем создавать
                                    if (CNiyamaDiscountCard::CanSiteRegisterNum($sTmpCheckCard)) {
                                        $bShowCheckNumForm = false;
                                        $bShowNewCardInfoForm = true;
                                        $sCardNum = $sTmpCheckCard;
                                    } else {
                                        $arFormErrors['WRONG_NEW_CARD_TYPE'] = 'Невозможно зарегистрировать новую карту с указанным номером';
                                    }
                                }

                                if ($sCardNum) {
                                    $_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'] = $sCardNum;
                                    // сбросим счетчики неудачных попыток
                                    //$_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'] = array();
                                    $_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM'] = array();
                                }
                                if ($arFormErrors) {
                                    $_SESSION['_NIYAMA_CARDS_']['ERR_CARDS_NUM'][] = $sTmpCheckCard;
                                }
                            } else {
                                $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка. Попробуйте повторить операцию';
                                $bShowSupportLink = true;
                            }
                        }
                    } else {
                        $arFormErrors['CARD_NUM_EMPTY'] = 'Пожалуйста, укажите номер карты';
                    }
                }
                break;

            case 'new_card_info':
                $sTmpCheckCard = '';
                if ($_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM']) {
                    $sTmpCheckCard = $_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'];
                }
                $sCardNum = $sTmpCheckCard;

                $bShowCheckNumForm = true;
                if ($arFormErrors) {
                    $bShowNewCardInfoForm = true;
                } else {
                    if (strlen($sTmpCheckCard)) {
                        $sTmpCardType = CNiyamaDiscountCard::GetCardTypeByCardNum($sTmpCheckCard);
                        if (!$sTmpCardType) {
                            $arFormErrors['INCORRECT_TYPE'] = 'Карта с таким номером не разрешена в системе, проверьте номер карты и повторите ввод';
                        } else {
                            // проверяем, закреплен ли уже за кем-нибудь введенный номер карты
                            $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sTmpCheckCard);
                            if ($arPdsCardInfo && $arPdsCardInfo['Number'] == $sTmpCheckCard) {
                                if ($arPdsCardInfo['Account']) {
                                    // карта уже зарегистрирована, отправляем юзера назад
                                    $arFormErrors['CARD_REGISTERED'] = 'Указанная карта уже зарегистрирована за другим посетителем';
                                    $bShowSupportLink = true;
                                } else {
                                    // такой карты еще нет,	проверяем наличие данных: ФИО, тел, e-mail, адрес, дата рождения
                                    // если чего-то нет, то запрашиваем информацию для регистрации новой карты
                                    // только простые карты можем создавать
                                    if (CNiyamaDiscountCard::CanSiteRegisterNum($sTmpCheckCard)) {
                                        $arFields = array(
                                            'tel1' => '', // *
                                            'email' => '', //*
                                            'discount' => '5%', //*
                                            'locked' => '0',
                                            'address' => '',
                                            'birthday' => '',
                                            'holder' => '',
                                            'expired' => '01.01.2030',

                                            'bonus' => '',
                                            'scheme' => '',
                                            'paylimit' => '',
                                            'folder' => '',
                                            'discounttypelimit' => '',
                                            'discountlimit' => '',
                                            'discountlevelname' => '',
                                            'balance' => '',
                                            'seize' => '',
                                        );
                                        // ---
                                        $arTmpName = array();
                                        if (strlen(trim($_POST['LAST_NAME']))) {
                                            $arTmpName[] = trim($_POST['LAST_NAME']);
                                        } else {
                                            $arFormErrors['ERR_FIELD_LAST_NAME'] = 'Не указана фамилия владельца карты';
                                        }
                                        if (strlen(trim($_POST['NAME']))) {
                                            $arTmpName[] = trim($_POST['NAME']);
                                        } else {
                                            $arFormErrors['ERR_FIELD_NAME'] = 'Не указано имя владельца карты';
                                        }
                                        if (strlen(trim($_POST['SECOND_NAME']))) {
                                            $arTmpName[] = trim($_POST['SECOND_NAME']);
                                        }
                                        // если заданы только имя и фамилия, то порядок будет: Иван Иванов, иначе: Иванов Иван Иванович
                                        $arTmpName = count($arTmpName) == 2 ? array_reverse($arTmpName) : $arTmpName;
                                        $arFields['holder'] = implode(' ', $arTmpName);

                                        // ---
                                        if (strlen(trim($_POST['EMAIL']))) {
                                            if (check_email($_POST['EMAIL'])) {
                                                $arFields['email'] = trim($_POST['EMAIL']);
                                            } else {
                                                $arFormErrors['ERR_FIELD_EMAIL'] = 'E-mail владельца карты указан некорректно';
                                            }
                                        } else {
                                            $arFormErrors['ERR_FIELD_EMAIL'] = 'Не указан e-mail владельца карты';
                                        }

                                        // ---
                                        if (strlen(trim($_POST['PHONE']))) {
                                            $arFields['tel1'] = trim($_POST['PHONE']);
                                        } else {
                                            $arFormErrors['ERR_FIELD_PHONE'] = 'Не указан телефон владельца карты';
                                        }

                                        // ---
                                        if (strlen(trim($_POST['BIRTHDAY']))) {
                                            $sTmpVal = trim($_POST['BIRTHDAY']);
                                            $obTmpDate = date_create($sTmpVal);
                                            if ($obTmpDate) {
                                                $arFields['birthday'] = $obTmpDate->Format('d.m.Y');
                                            } else {
                                                $arFormErrors['ERR_FIELD_BIRTHDAY'] = 'Дата рождения указана некорректно';
                                            }
                                        } else {
                                            $arFormErrors['ERR_FIELD_BIRTHDAY'] = 'Не указана дата рождения';
                                        }

                                        // ---
                                        $arTmpVal = array();
                                        if (strlen(trim($_POST['CITY']))) {
                                            $arTmpVal[] = 'Город: ' . trim($_POST['CITY']);
                                        }
                                        if (strlen(trim($_POST['STREET']))) {
                                            $arTmpVal[] = 'Улица: ' . trim($_POST['STREET']);
                                        }
                                        if (strlen(trim($_POST['HOME']))) {
                                            $arTmpVal[] = 'Дом: ' . trim($_POST['HOME']);
                                        }
                                        if (strlen(trim($_POST['HOUSING']))) {
                                            $arTmpVal[] = 'Корпус: ' . trim($_POST['HOUSING']);
                                        }
                                        if (strlen(trim($_POST['BUILDING']))) {
                                            $arTmpVal[] = 'Строение: ' . trim($_POST['BUILDING']);
                                        }
                                        if (strlen(trim($_POST['APARTMENT']))) {
                                            $arTmpVal[] = 'Квартира: ' . trim($_POST['APARTMENT']);
                                        }
                                        $arFields['address'] = implode("\n", $arTmpVal);

                                        if (!$arFormErrors) {
                                            $arResponse = CNiyamaDiscountCard::RegisterPdsCard($sTmpCheckCard, $arFields);
                                            $bShowNewCardInfoForm = true;
                                            // карты в ПДС создаются с задержкой в минуту, поэтому в ответ всегда будет приходить GetErrorText "Not found"
                                            // игнорируем эту ошибку и привязываем деактивированную карту к юзеру, затем агент проверит ее и либо удалит, либо активирует запись
                                            if ($arResponse['GetErrorText'] == 'Not found') {
                                                $arResponse['GetErrorText'] = '';
                                            }
                                            if ($arResponse['GetErrorText']) {
                                                $arFormErrors['REGISTER_ERROR'] = 'Ошибка регистрации карты';
                                                $bShowSupportLink = true;
                                            } elseif ($arResponse['NoXml']) {
                                                $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка';
                                                $bShowSupportLink = true;
                                            } else {
                                                // создаем деактивированную запись привязки карты к юзеру
                                                $arTmpPdsCardInfo = array(
                                                    'Number' => $sTmpCheckCard,
                                                    'User' => array(
                                                        'Holder' => $arFields['holder'],
                                                        'Birthday' => $arFields['birthday'],
                                                        'Tel1' => $arFields['tel1'],
                                                        'Email' => $arFields['email'],
                                                        'Address' => $arFields['address'],
                                                    ),
                                                    'Account' => array(
                                                        'Card' => $sTmpCheckCard,
                                                        'Discount' => CNiyamaIBlockCardData::GetDiscountCode(5),
                                                        'Offered' => '',
                                                    ),
                                                );
                                                $bTmpUpdateIfExists = true;
                                                $bTmpCompareUpdateFields = false;
                                                $iCardDataElementId = CNiyamaDiscountCard::AddCardDataElementByPdsInfo($arTmpPdsCardInfo, $iCurUserId, array('ACTIVE' => 'N', 'CODE' => 'check_queue'), $bTmpUpdateIfExists, $bTmpCompareUpdateFields);
                                                if ($iCardDataElementId) {
                                                    if ($arTmpPdsCardInfo['Account']['Discount']) {
                                                        CNiyamaDiscountLevels::UpdateUserOrderLevelByDiscount($iCurUserId, CNiyamaIBlockCardData::GetTrueDiscount($arTmpPdsCardInfo['Account']['Discount']));
                                                    }

                                                    $bShowNewCardSuccessForm = true;
                                                } else {
                                                    $arFormErrors['WRONG_NEW_CARD_TYPE'] = 'Не удалось создать запись карты. Пожалуйста, попробуйте зарегистрировать карту позже, либо обратитесь в службу тех.поддержки сайта';
                                                    $bShowSupportLink = true;
                                                }
                                            }
                                        } else {
                                            $bShowNewCardInfoForm = true;
                                        }
                                    } else {
                                        $arFormErrors['WRONG_NEW_CARD_TYPE'] = 'Невозможно зарегистрировать новую карту с указанным номером';
                                        $bShowSupportLink = true;
                                    }
                                }
                            } else {
                                $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка. Попробуйте повторить операцию';
                                $bShowSupportLink = true;
                            }
                        }
                    } else {
                        $arFormErrors['CARD_NUM_EMPTY'] = 'Пожалуйста, укажите номер карты';
                    }
                }

                if ($bShowNewCardInfoForm) {
                    $bShowCheckNumForm = false;
                }
                if ($bShowNewCardSuccessForm) {
                    $bShowNewCardInfoForm = false;
                    $bShowCheckNumForm = false;
                }
                break;

            case 'card_confirm':
                $sTmpCheckCard = '';
                if ($_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM']) {
                    $sTmpCheckCard = $_SESSION['_NIYAMA_CARDS_']['CUR_CARD_NUM'];
                }

                $sCardNum = $sTmpCheckCard;

                $bShowConfirmForm = true;

                if (!$arFormErrors) {
                    if (strlen($sTmpCheckCard)) {
                        $sTmpCardType = CNiyamaDiscountCard::GetCardTypeByCardNum($sTmpCheckCard);
                        if (!$sTmpCardType) {
                            $arFormErrors['INCORRECT_TYPE'] = 'Карта с таким номером не разрешена в системе, проверьте номер карты и повторите ввод';
                            $bShowSupportLink = true;
                        } elseif (in_array($sTmpCardType, array('staff', 'special', 'vip'))) {
                            $arFormErrors['INCORRECT_IDENTIFICATION'] = 'Неверный способ идентификации карты';
                            $bShowSupportLink = true;
                        } else {
                            // проверяем, закреплен ли уже за кем-нибудь введенный номер карты
                            $arPdsCardInfo = CNiyamaDiscountCard::GetPdsCardInfo($sTmpCheckCard);
                            if ($arPdsCardInfo && $arPdsCardInfo['Number'] == $sTmpCheckCard) {
                                if ($arPdsCardInfo['User']) {
                                    // карта зарегистрирована, проверяем юзера
                                    $sTmpEmail = '';
                                    if (strlen(trim($_POST['EMAIL']))) {
                                        if (check_email($_POST['EMAIL'])) {
                                            $sTmpEmail = trim($_POST['EMAIL']);
                                        } else {
                                            $arFormErrors['ERR_FIELD_EMAIL'] = 'E-mail владельца карты указан некорректно';
                                        }
                                    } else {
                                        $arFormErrors['ERR_FIELD_EMAIL'] = 'Не указан e-mail владельца карты';
                                    }

                                    $sTmpPhone = '';
                                    if (strlen(trim($_POST['PHONE']))) {
                                        $sTmpPhone = CCustomProject::GetNumsOnly($_POST['PHONE']);
                                    }
                                    if (!strlen($sTmpPhone)) {
                                        $arFormErrors['ERR_FIELD_PHONE'] = 'Не указан телефон владельца карты';
                                    }

                                    if (strlen($sTmpPhone) && strlen($sTmpEmail)) {
                                        $sTmpPhoneCard = CCustomProject::GetNumsOnly($arPdsCardInfo['User']['Tel1']);

                                        if ($sTmpPhoneCard == $sTmpPhone && $sTmpEmail == $arPdsCardInfo['User']['Email']) {
                                            // идентификация прошла, создаем запись в базе и отправляем уведомление на ящик
                                            // сброс счетчика
                                            $_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM'] = array();
                                            $arIdentify = CNiyamaDiscountCard::SetCardIdentifyData($iCurUserId, $sTmpCheckCard, false);
                                            if ($arIdentify) {
                                                // генерируем код для подтверждения
                                                $sCardConfirmUrl = str_replace(
                                                    array('#USER_ID#', '#CONFIRM_CODE#', '#CARD_NUM#'),
                                                    array($iCurUserId, $arIdentify['UF_CHECKWORD'], $sTmpCheckCard),
                                                    $sCardConfirmUrlTemplate
                                                );
                                                CEvent::SendImmediate(
                                                    'NIYAMA_CARD_CONFIRMATION',
                                                    's1',
                                                    array(
                                                        'CONFIRM_URL' => $sCardConfirmUrl,
                                                        'CARD_NUM' => $sTmpCheckCard,
                                                        'CARD_TYPE' => $sTmpCardType,
                                                        'CARD_EMAIL' => $arPdsCardInfo['User']['Email'],
                                                        'CARD_TEL' => $arPdsCardInfo['User']['Tel1'],
                                                        'CHECKWORD' => $arIdetify['UF_CHECKWORD'],
                                                        'AUTHOR_USER_LOGIN' => $sCurUserLogin,
                                                        'AUTHOR_USER_ID' => $iCurUserId,
                                                        'DATE' => date('d.m.Y H:i:s'),
                                                    )
                                                );
                                                $bShowConfirmSuccess = true;
                                            } else {
                                                $arFormErrors['UNKNOWN_ERROR'] = 'Не удалось сохранить данные. Попробуйте повторить операцию';
                                                $bShowSupportLink = true;
                                            }
                                        } else {
                                            $_SESSION['_NIYAMA_CARDS_']['ERR_CONFIRM'][] = array(
                                                'EMAIL' => $sTmpEmail,
                                                'PHONE' => $sTmpPhone
                                            );
                                            $arFormErrors['NOT_EQUAL'] = 'Введенные данные не совпадают с данными карты';
                                        }
                                    }

                                } else {
                                    $arFormErrors['CARD_NOT_REGISTERED'] = 'Невозможно идентифицировать указанную карту';
                                }
                            } else {
                                $arFormErrors['UNKNOWN_ERROR'] = 'Произошла неизвестная ошибка. Попробуйте повторить операцию';
                                $bShowSupportLink = true;
                            }
                        }
                    } else {
                        $arFormErrors['CARD_NUM_EMPTY'] = 'Пожалуйста, укажите номер карты';
                        $bShowConfirmForm = false;
                        $bShowCheckNumForm = true;
                    }
                }
                break;
        }
    }
} else {
    if ($arCardData) {
        if ($arCardData['ACTIVE'] == 'Y') {
            $bShowEditForm = true;
        } else {
            $bShowWaitForm = true;
        }
    } elseif (!$bShowConfirmForm) {
        $bShowCheckNumForm = true;
    }
}
?>
<div class="container">
    <div class="profile-popup">
        <?
        //
        // Форма проверки карты (шаг 1)
        //
        if ($bShowCheckNumForm) {
            $sCheckCardNum = isset($_POST['CHECK_CARD_NUM']) ? htmlspecialcharsbx(trim($_POST['CHECK_CARD_NUM'])) : $sCardNum;
            $bAttemptsErr = isset($arFormErrors['ERR_ATTEMPTS']);
            $sTmpDisabled = $bAttemptsErr ? ' disabled="disabled"' : '';
            ?>
            <div onclick="return {'cardCheck': {}}" class="card-check js-widget"><?
            ?>
            <form action="/ajax/getProfileAddCardForm.php" method="post" class="card_popup_confirm"><?
            echo bitrix_sessid_post();
            ?><input type="hidden" name="FORM_TYPE" value="check_card_num"/><?

            ?><p class="h1 popup__title">Проверка карты</p>
            <label class="profile-popup__label">Номер карты</label>
            <input type="text" class="input _mb_2" name="CHECK_CARD_NUM"
                   value="<?= $sCheckCardNum ?>"<?= $sTmpDisabled ?> />
            <div class="card-check__error"><?
            if ($bAttemptsErr) {
                echo '<div>Вы использовали максимально допустимое количество попыток активации карты.</div>';
                $bShowSupportLink = true;
            } elseif ($arFormErrors) {
                echo implode('<br />', $arFormErrors);
            }
            if ($bShowSupportLink) {
                echo '<div><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=check_card_num" href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Обратиться в тех.поддержку сайта</a></em></div>';
            }
            ?></div><?
            if (!$bAttemptsErr) {
                ?><input value="Проверить" type="submit" class="btn _style_4 _full-width"/><?
            }
            ?></form><?
            ?></div><?
        }

        if ($bShowWaitForm) {
            ?>
            <div class="card-check js-widget">
                <p class="h1 popup__title">Регистрация карты</p>
                <p>Карта находится на стадии активации. Обычно эта процедура занимает около 5 минут, если с момента
                    регистрации прошло больше времени, пожалуйста, обратитесь в тех.поддержку сайта.</p>
                <p><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=new_card_info" href="javascript:void(0)"
                          data-fancybox-type="ajax" class="fancybox">Обратиться в тех.поддержку сайта</a></em></p>
            </div><?
        }

        if ($bShowNewCardSuccessForm) {
            ?>
            <div class="card-check js-widget">
                <p class="h1 popup__title">Регистрация карты</p>
                <p>Спасибо! Карта передана на регистрацию, обычно эта процедура занимает около 5 минут.</p>
            </div><?
        }

        //
        // Форма запроса данных для регистарции новой карты (шаг 2а)
        // и форма редактирования карты
        //
        if ($bShowNewCardInfoForm || $bShowEditForm) {
            $sName = '';
            $sLast_Name = '';
            $sBirthday = '';
            $sPhone = '';
            $sEmail = '';
            $sCity = '';
            $sStreet = '';
            $sHome = '';
            $sHousing = '';
            $sBuilding = '';
            $sApartment = '';

            if ($_POST['FORM_TYPE'] && ($_POST['FORM_TYPE'] == 'new_card_info' || $_POST['FORM_TYPE'] == 'edit_card_info')) {
                $sName = isset($_POST['NAME']) ? htmlspecialcharsbx($_POST['NAME']) : '';
                $sLastName = isset($_POST['LAST_NAME']) ? htmlspecialcharsbx($_POST['LAST_NAME']) : '';
                $sSecondName = isset($_POST['SECOND_NAME']) ? htmlspecialcharsbx($_POST['SECOND_NAME']) : '';
                $sBirthday = isset($_POST['BIRTHDAY']) ? htmlspecialcharsbx($_POST['BIRTHDAY']) : '';
                $sPhone = isset($_POST['PHONE']) ? htmlspecialcharsbx($_POST['PHONE']) : '';
                $sEmail = isset($_POST['EMAIL']) ? htmlspecialcharsbx($_POST['EMAIL']) : '';
                $sCity = isset($_POST['CITY']) ? htmlspecialcharsbx($_POST['CITY']) : '';
                $sStreet = isset($_POST['STREET']) ? htmlspecialcharsbx($_POST['STREET']) : '';
                $sHome = isset($_POST['HOME']) ? htmlspecialcharsbx($_POST['HOME']) : '';
                $sHousing = isset($_POST['HOUSING']) ? htmlspecialcharsbx($_POST['HOUSING']) : '';
                $sBuilding = isset($_POST['BUILDING']) ? htmlspecialcharsbx($_POST['BUILDING']) : '';
                $sApartment = isset($_POST['APARTMENT']) ? htmlspecialcharsbx($_POST['APARTMENT']) : '';

                if ($arCardData && $arCardData['_CAN_EDIT_BIRTHDAY_'] != 'Y') {
                    $sBirthday = htmlspecialcharsbx($arCardData['PROPERTY_BIRTHDAY_VALUE']);
                }

            } else {
                if ($bShowNewCardInfoForm) {
                    $arCurUserInfo = CUser::GetById($iCurUserId)->Fetch();
                    if ($arCurUserInfo) {
                        $sName = htmlspecialcharsbx($arCurUserInfo['NAME']);
                        $sLastName = htmlspecialcharsbx($arCurUserInfo['LAST_NAME']);
                        $sSecondName = htmlspecialcharsbx($arCurUserInfo['SECOND_NAME']);
                        $sBirthday = htmlspecialcharsbx($arCurUserInfo['PERSONAL_BIRTHDAY']);
                        $sPhone = htmlspecialcharsbx($arCurUserInfo['PERSONAL_PHONE']);
                        $sEmail = htmlspecialcharsbx($arCurUserInfo['EMAIL']);
                        $sCity = htmlspecialcharsbx($arCurUserInfo['PERSONAL_CITY']);
                        $sStreet = htmlspecialcharsbx($arCurUserInfo['PERSONAL_STREET']);
                        $sHome = '';
                        $sHousing = '';
                        $sBuilding = '';
                        $sApartment = '';
                    }
                }
                elseif ($bShowEditForm) {
                    if ($arCardData && $arCardData['ACTIVE'] == 'Y') {
                        $sName = htmlspecialcharsbx($arCardData['PROPERTY_NAME_VALUE']);
                        $sLastName = htmlspecialcharsbx($arCardData['PROPERTY_LAST_NAME_VALUE']);
                        $sSecondName = htmlspecialcharsbx($arCardData['PROPERTY_SECOND_NAME_VALUE']);
                        $sBirthday = htmlspecialcharsbx($arCardData['PROPERTY_BIRTHDAY_VALUE']);
                        $sPhone = htmlspecialcharsbx($arCardData['PROPERTY_PHONE_VALUE']);
                        $sEmail = htmlspecialcharsbx($arCardData['PROPERTY_EMAIL_VALUE']);
                        $sCity = htmlspecialcharsbx($arCardData['PROPERTY_CITY_VALUE']);
                        $sStreet = htmlspecialcharsbx($arCardData['PROPERTY_STREET_VALUE']);
                        $sHome = htmlspecialcharsbx($arCardData['PROPERTY_HOME_VALUE']);
                        $sHousing = htmlspecialcharsbx($arCardData['PROPERTY_HOUSING_VALUE']);
                        $sBuilding = htmlspecialcharsbx($arCardData['PROPERTY_BUILDING_VALUE']);
                        $sApartment = htmlspecialcharsbx($arCardData['PROPERTY_APARTMENT_VALUE']);
                    }
                }
            }
            $bShowAllForm = true;

            ?>

            <div onclick="return {'cardCheck': {}}" class="ordering-form popup__card js-widget"><?
                if ($bShowNewCardInfoForm) {
                    ?><h1>Регистрация карты</h1><?
                } else {
                    ?><h1>Редактирование карты</h1><?
                }

                ?>
                <div class="popup__user-body"><?
                    if ($bShowEditSuccess) {
                        $bShowAllForm = false;
                        ?><p class="text-brown">Изменения успешно сохранены и будут актуализированы в течение 5
                            минут</p><?
                    }

                    if ($bShowAllForm) {
                        ?>
                        <form method="post" name="profileCardFom" action="/ajax/getProfileAddCardForm.php" class="card_popup_confirm"><?
                        echo bitrix_sessid_post();
                        $sTmpFormType = 'edit_card_info';
                        if ($bShowNewCardInfoForm) {
                            $sTmpFormType = 'new_card_info';
                        }

                        ?><input type="hidden" name="FORM_TYPE" value="<?= $sTmpFormType ?>" /><?

                        ?>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="profile-popup__input">
                                <label class="profile-popup__label">Номер карты</label>
                                <label class="input"><?= $sCardNum ?></label>
                            </div>
                        </div>
                        </div><?
                        ?>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Имя</label>
                                    <input class="input" name="NAME" value="<?= $sName ?>" required=""/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Фамилия</label>
                                    <input class="input" name="LAST_NAME" value="<?= $sLastName ?>" required=""/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Отчество</label>
                                    <input class="input" name="SECOND_NAME" value="<?= $sSecondName ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Дата рождения</label><?
                                    if ($bShowNewCardInfoForm) {
                                        ?><input class="input _birthday-mask" name="BIRTHDAY" value="<?= $sBirthday ?>"
                                                 required=""/><?
                                    } else {
                                        if ($arCardData && $arCardData['_CAN_EDIT_BIRTHDAY_'] == 'Y') {
                                            ?><input class="input" name="BIRTHDAY" value="<?= $sBirthday ?>"
                                                     required=""/><?
                                        } else {
                                            echo $sBirthday;
                                        }
                                    }
                                    ?></div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Телефон</label><?
                                    /*
                                    ?><input class="input _phone-mask" name="PHONE" value="<?=$sPhone?>" /><?
                                    */
                                    ?><input class="input" name="PHONE" value="<?= $sPhone ?>" required=""/><?
                                    ?></div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Электронная почта</label><?
                                    ?><input class="input" name="EMAIL" value="<?= $sEmail ?>" required=""/><?
                                    ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Город</label>
                                    <input class="input" name="CITY" value="<?= $sCity ?>"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Улица</label>
                                    <input class="input" name="STREET" value="<?= $sStreet ?>"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="profile-popup__input">
                                    <label class="profile-popup__label">Дом</label>
                                    <input class="input" name="HOME" value="<?= $sHome ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        <div class="col-md-3">
                            <div class="profile-popup__input">
                                <label class="profile-popup__label">Корпус</label>
                                <input class="input" name="HOUSING" value="<?= $sHousing ?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="profile-popup__input">
                                <label class="profile-popup__label">Строение</label>
                                <input class="input" name="BUILDING" value="<?= $sBuilding ?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="profile-popup__input">
                                <label class="profile-popup__label">Квартира</label>
                                <input class="input" name="APARTMENT" value="<?= $sApartment ?>"/>
                            </div>
                        </div>
                        </div><?
                        ?>
                        <div class="card-check__error"><?
                        if ($arFormErrors) {
                            echo '<div style="margin: 0 0 10px 0;">' . implode('<br />', $arFormErrors) . '</div>';
                        }
                        if ($bShowSupportLink) {
                            echo '<div style="margin: 0 0 10px 0;"><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=' . $sTmpFormType . '" href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Обратиться в тех.поддержку сайта</a></em></div>';
                        }
                        ?></div><?
                        ?>
                        <div class="row">
                        <div class="profile-popup__save-btn">
                            <input value="<?= ($bShowEditForm ? 'Сохранить' : 'Зарегистрировать') ?>" type="submit"/>
                        </div>
                        </div>
                        </form><?
                    }
                    ?></div><?
                ?></div>

            <?
        }

        //
        // Форма запроса данных для проверки принадлежности карты посетителю (шаг 2б)
        //
        if ($bShowConfirmForm) {
            $sEmail = '';
            $sPhone = '';

            if ($_POST['FORM_TYPE'] && $_POST['FORM_TYPE'] == 'card_confirm') {
                $sEmail = isset($_POST['EMAIL']) ? htmlspecialcharsbx($_POST['EMAIL']) : '';
                $sPhone = isset($_POST['PHONE']) ? htmlspecialcharsbx($_POST['PHONE']) : '';
            }

            $bAttemptsErr = isset($arFormErrors['ERR_ATTEMPTS']);
            $sTmpDisabled = $bAttemptsErr ? ' disabled="disabled"' : '';

            $sCardType = $sCardNum ? CNiyamaDiscountCard::GetCardTypeByCardNum($sCardNum) : '';
            $bShowAllForm = false;

            ?>
            <div onclick="return {'cardCheck': {}}" class="ordering-form popup__card js-widget">
                <h1>Карта уже зарегистрирована</h1>
                <div class="popup__user-body"><?
                    if ($bShowConfirmSuccess) {
                        $bShowAllForm = false;
                        ?><p class="text-brown">На указанный e-mail отправлено письмо для подтверждения регистрации
                            карты</p><?
                    } else {
                        switch ($sCardType) {
                            case 'staff':
                                // сотрудники
                                ?><p class="text-brown">Вы ввели номер из категории карт сотрудников компании «Нияма»,
                                проверьте номер и повторите ввод или введите данные для Вашей <a
                                        href="/ajax/getSupportForm.php?cn=<?= $sCardNum ?>" data-fancybox-type="ajax"
                                        class="fancybox">идентификации</a></p><?
                                break;

                            case 'special':
                                // особые клиенты
                                ?><p class="text-brown">Вы ввели номер из категории «Special», проверьте номер и
                                повторите ввод или введите данные для Вашей <a
                                        href="/ajax/getSupportForm.php?cn=<?= $sCardNum ?>" data-fancybox-type="ajax"
                                        class="fancybox">идентификации</a></p><?
                                break;

                            case 'vip':
                                // учредители и топ-менеджеры
                                ?><p class="text-brown">Вы ввели номер из категории «VIP», проверьте номер и повторите
                                ввод или введите данные для Вашей <a href="/ajax/getSupportForm.php?cn=<?= $sCardNum ?>"
                                                                     data-fancybox-type="ajax" class="fancybox">идентификации</a>
                                </p><?
                                break;

                            default:
                                ?><p class="text-brown">Если вы являетесь владельцем карты, введите e-mail и телефон,
                                который вы указывали при регистрации карты</p><?
                                $bShowAllForm = true;
                                break;
                        }
                    }
                    if ($bAttemptsErr) {
                        ?>
                        <div class="card-check__error" style="margin: 0;"><?
                        if ($arFormErrors) {
                            echo '<div style="margin: 10px 0 0 0;">' . implode('<br />', $arFormErrors) . '</div>';
                        }
                        if ($bShowSupportLink) {
                            echo '<div style="margin: 10px 0 0 0;"><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=card_confirm" href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Обратиться в тех.поддержку сайта</a></em></div>';
                        }
                        ?></div><?
                        ?>
                        <div><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=card_confirm"
                                    href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Я не помню
                                    e-mail или телефон</a></em></div><?
                    } elseif ($bShowAllForm) {
                        ?>
                        <form method="post" action="/ajax/getProfileAddCardForm.php" class="card_popup_confirm"><?
                        echo bitrix_sessid_post();
                        ?><input type="hidden" name="FORM_TYPE" value="card_confirm"/><?

                        ?>
                        <div class="ordering-form__row">
                        <div class="input__wrap">
                            <label class="profile-popup__label">Электронная почта</label>
                            <input class="input _mb_2" name="EMAIL" value="<?= $sEmail ?>"<?= $sTmpDisabled ?> required=""/>
                        </div>
                        <div class="input__wrap">
                            <label class="profile-popup__label">Телефон</label>
                            <input class="input _mb_2" name="PHONE" value="<?= $sPhone ?>"<?= $sTmpDisabled ?> required=""/>
                        </div>
                        <div><br><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=card_confirm"
                                        href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Я не помню
                                    e-mail или телефон</a></em></div>
                        </div>
                        <div class="card-check__error" style="margin: 0;"><?
                            if ($arFormErrors) {
                                echo '<div style="margin: 10px 0 0 0;">' . implode('<br />', $arFormErrors) . '</div>';
                            }
                            if ($bShowSupportLink) {
                                echo '<div style="margin: 10px 0 0 0;"><em><a data-fancybox-href="/ajax/getSupportForm.php?ci=card_confirm" href="javascript:void(0)" data-fancybox-type="ajax" class="fancybox">Обратиться в тех.поддержку сайта</a></em></div>';
                            }
                            ?></div>
                        <div class="ordering-form__row">
                            <div class="input__wrap">
                                <input value="Подтвердить" type="submit" class="btn _style_4 _full-width"/>
                            </div>
                        </div>
                        </form><?
                    }
                    ?></div>
            </div>
            <?
        }

        ?>

    </div>
</div>

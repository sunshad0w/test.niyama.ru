/**
 * Created by user on 2017-05-29.
 */
$(function() {
    $(document).on('submit', 'form[name="profileCardFom"]', function(e) {
        e.preventDefault();
        $action = $(this).attr('action');
        $.post($action, $(this).serialize(), function(data){
            if(data){
                $container = $(data).find(".container").html();
                $(this).find('.container').html($container);
            }

        });
    });
});
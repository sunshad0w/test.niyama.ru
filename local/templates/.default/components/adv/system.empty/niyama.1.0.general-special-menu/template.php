<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// ИНГРЕДИЕНТЫ
//

if ( CModule::IncludeModule('iblock')) :
    $arMenuItems = CNiyamaCatalog::getSpecMenu();
    if (!empty($arMenuItems)) :

    $iCountCheck = ceil(count($arMenuItems) / 2);

    ?><div class="filter__group clearfix" data-category="specmenu[]"><?
        ?><div class="filter__title"><?= $arParams['MENU_TITLE'] ?></div><?
        ?><div class="filter__col"><?

        $i = 1;
        foreach ($arMenuItems as $item ):
            ?><div data-filter="<?= $item['ID'] ?>" class="filter__item <?= (isset($_REQUEST['specmenu'])) && (in_array($item['ID'],$_REQUEST['specmenu']))? "_selected" : ""; ?>">
                <a href="#" class="filter__link"><span class="pseudo-link"><?= $item['NAME'] ?></span></a>
            </div><?
            echo ($i == $iCountCheck)? '</div><div class="filter__col">' : '';
        $i++;
        endforeach;

        ?></div><?

        ?><div class="filter__col"></div><?
    ?></div><?

    endif;
endif;

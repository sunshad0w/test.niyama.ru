<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');

?><script type="text/javascript">
	JNiyamaShare = {
		vkontakte: function(purl, ptitle, pimg, text) {
			url  = 'http://vkontakte.ru/share.php?';
			url += 'url='		  + encodeURIComponent(purl);
			url += '&title='	   + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&image='	   + encodeURIComponent(pimg);
			url += '&noparse=true';
			JNiyamaShare.popup(url);
		},
		odnoklassniki: function(purl, ptitle, pimg, text) {
			url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
			url += '&st.comments=' + encodeURIComponent(text);
			url += '&st._surl='	+ encodeURIComponent(purl);
			JNiyamaShare.popup(url);
		},
		facebook: function(purl, ptitle, pimg, text) {
			url  = 'http://www.facebook.com/sharer.php?s=100';
			url += '&p[title]='	 + encodeURIComponent(ptitle);
			url += '&p[summary]='   + encodeURIComponent(text);
			url += '&p[url]='	   + encodeURIComponent(purl);
			url += '&p[images][0]=' + encodeURIComponent(pimg);
			JNiyamaShare.popup(url);
		},
		twitter: function(purl, ptitle, pimg, text) {
			url  = 'http://twitter.com/share?';
			url += 'text='	  + encodeURIComponent(ptitle);
			url += '&url='	  + encodeURIComponent(purl);
			url += '&counturl=' + encodeURIComponent(purl);
			JNiyamaShare.popup(url);
		},
		mailru: function(purl, ptitle, pimg, text) {
			url  = 'http://connect.mail.ru/share?';
			url += 'url='		  + encodeURIComponent(purl);
			url += '&title='	   + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&imageurl='	+ encodeURIComponent(pimg);
			JNiyamaShare.popup(url)
		},

		popup: function(url) {
			window.open(url,'','toolbar=0,status=0,width=626,height=436');
			$.post('/ajax/shareEvent.php', {social:url, type:"product_detail"}, function (data){});
		}
	};
</script><?

# Если есть параметр

if($arParams['CODE']) {

	$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';

	$dir = $GLOBALS['APPLICATION']->GetCurDir();

	$arProduct = CNiyamaCatalog::GetProductDetailByCode($arParams['CODE']);

	if($arProduct && ($arProduct['INFO']['DETAIL_PAGE_URL'] == $dir || $bAjaxRequest)) {
		$iMenuIBlockId = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');

		$arSeoProduct = $arProduct;
		$arCanonicalProduct = array();
		if($arProduct['INFO']['PROPERTY_CANONICAL_ITEM_VALUE']) {
			$arCanonicalProduct = CNiyamaCatalog::GetProductDetail($arProduct['INFO']['PROPERTY_CANONICAL_ITEM_VALUE']);
			if($arCanonicalProduct) {
				$arSeoProduct = $arCanonicalProduct;
				$GLOBALS['APPLICATION']->AddHeadString('<link rel="canonical" href="http://'.SITE_SERVER_NAME.$arCanonicalProduct['INFO']['DETAIL_PAGE_URL'].'" />');
			}
		}

		#
		# Получаем СЕО элемента
		#
		$pTitle = $arSeoProduct['INFO']['NAME'];
		$pDesc = '';
		$pkWord = '';
		$obSeoProps = new \Bitrix\Iblock\InheritedProperty\ElementValues($iMenuIBlockId, $arSeoProduct['INFO']['ID']);
		$arSeoProps = $obSeoProps->GetValues();
		if($arSeoProps) {
			$pTitle = $arSeoProps['ELEMENT_META_TITLE'] ? $arSeoProps['ELEMENT_META_TITLE'] : $pTitle;
			$pDesc = isset($arSeoProps['ELEMENT_META_DESCRIPTION']) ? $arSeoProps['ELEMENT_META_DESCRIPTION'] : $pDesc;
			$pkWord = isset($arSeoProps['ELEMENT_META_KEYWORDS']) ? $arSeoProps['ELEMENT_META_KEYWORDS'] : $pkWord;
		}
		if($pDesc) {
			$GLOBALS['APPLICATION']->SetPageProperty('description', $pDesc);
		}
		if($pkWord) {
			$GLOBALS['APPLICATION']->SetPageProperty('keywords', $pkWord);
		}
		$GLOBALS['APPLICATION']->SetTitle($pTitle);

		if(!$bAjaxRequest) {
			$arRes = CCustomProject::GetIBlockParentSection($iMenuIBlockId, $arProduct['INFO']['IBLOCK_SECTION_ID']);
			$arCategory = array();
			foreach(array_reverse($arRes) as $item) {
				$arCategory[] = array(
					'name' => $item['NAME'], 
					'id' => $item['ID'], 
					'code' => $item['CODE'],
					'url' => $item['SECTION_PAGE_URL']
				);
			}

			$GLOBALS['APPLICATION']->AddChainItem('Меню', '/menu/');
			foreach($arCategory as $catItem) {
				$GLOBALS['APPLICATION']->AddChainItem($catItem['name'], $catItem['url']);
			}
			$GLOBALS['APPLICATION']->AddChainItem($arProduct['INFO']['NAME']);
			// Хлебные крошки
			$GLOBALS['APPLICATION']->IncludeComponent(
				'bitrix:breadcrumb', 
				'catalog', 
				array(
					'START_FROM' => 1
				), 
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}
		static $iMaxQuantity = 0;
		if(!$iMaxQuantity) {
			$iMaxQuantity = intval(CNiyamaCustomSettings::GetStringValue('order_dish_max_quantity', 12));
		}

		// Увеличиваем счетчик
		CIBlockElement::CounterInc(intval($arProduct['INFO']['ID']));

		// Изображение блюда
		$arPreviewPicture = $arProduct['INFO']['PREVIEW_PICTURE'] ? CFile::GetFileArray($arProduct['INFO']['PREVIEW_PICTURE']) : array();
		$arDetailPicture = $arPreviewPicture;
		if($arProduct['INFO']['DETAIL_PICTURE']) {
			$arTmpImg = CFile::ResizeImageGet(
				$arProduct['INFO']['DETAIL_PICTURE'],
				array(
					'width' => 361,
					'height' => 361
				),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);
			if($arTmpImg) {
				$arPreviewPicture = array(
					'SRC' => $arTmpImg['src'],
					'WIDTH' => $arTmpImg['width'],
					'HEIGHT' => $arTmpImg['height'],
				); 
			}
			$arTmpImg = CFile::ResizeImageGet(
				$arProduct['INFO']['DETAIL_PICTURE'],
				array(
					'width' => 800,
					'height' => 800
				),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true
			);
			if($arTmpImg) {
				$arDetailPicture = array(
					'SRC' => $arTmpImg['src'],
					'WIDTH' => $arTmpImg['width'],
					'HEIGHT' => $arTmpImg['height'],
				); 
			}
		}

		$purl = 'http://'.SITE_SERVER_NAME.$arProduct['INFO']['DETAIL_PAGE_URL'];
		$ptitle = $arProduct['INFO']['NAME'];
		$image = $arPreviewPicture ? $arPreviewPicture['SRC'] : '';
		$pimg = $image ? 'http://'.SITE_SERVER_NAME.$image : '';
		$text = $arProduct['INFO']['PREVIEW_TEXT'];

		$APPLICATION->AddHeadString('<meta property="og:title" content="'.$ptitle.'"><meta property="og:url" content="'.$purl.'"><meta property="og:description" content="'.$text.'"><meta property="og:image" content="'.$pimg.'">');

		?><div class="goods-one js-widget" onclick="return {'goodsOne': {}}">
			<div class="goods-one__img"><?
				?><a class="fancybox-gallery" rel="item" href="<?=$arDetailPicture['SRC']?>"><?
					?><img alt="" src="<?=$arPreviewPicture['SRC']?>"><?
				?></a>
			</div>
			<div class="goods-one__desc"><?
				// Название
				?><h1 class="goods-one__title"><?=$arProduct['INFO']['NAME']?></h1><?
				// Штуки, калории, вес
				?><div class="goods-one__specs"><?
					$bNeedBullet = false;
					$iP = intval($arProduct['INFO']['PROPERTY_NUM_OF_PIECES']);
					$iC = intval($arProduct['INFO']['PROPERTY_NUM_OF_CALORIES']);
					$sW = trim($arProduct['INFO']['PROPERTY_WEIGHT_FULL_VALUE']);
					if ($iP) {
						?><span class="goods-one__specs-count"><?=$iP?> шт</span><?
						$bNeedBullet = true;
					}
					if ($iC) {
						if ($bNeedBullet) {
							?><span class="goods-one__specs-bullet"> • </span><?
						}
						?><span class="goods-one__specs-energy"><?=$iC?> ккал</span><?
						$bNeedBullet = true;
					}
					if(strlen($sW)) {
						if($bNeedBullet) {
							?><span class="goods-one__specs-bullet"> • </span><?
						}
						?><span class="goods-one__specs-weight"><?=$sW?> г</span><?
					}
				?></div><?
				// Ингредиенты
				/*
				if(!empty($arProduct['EXCLUDE_INGREDIENT'])) {
					$ex_ings = array();
					foreach($arProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
						if (!in_array($elem, $arProduct['ALL_INGREDIENT'])) {
							$ex_ings[$key] = $elem;
						}
					}
					if (!empty($ex_ings)) {
						$arProduct['ALL_INGREDIENT'] = array_merge($arProduct['ALL_INGREDIENT'], $ex_ings);
					}
				}
				*/
				?><div class="goods-one__composition"><?=implode(', ', $arProduct['ALL_INGREDIENT'])?></div>
				<p class="goods-one__about">
					<?=$arProduct['INFO']['PREVIEW_TEXT']?>
					<?=$arProduct['INFO']['DETAIL_TEXT']?>
				</p>
			</div>
			<div class="goods-one__side">
				<div class="goods-one__cart">
					<div class="goods-one__cost">
						<div class="goods-one__sum"><?=$arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'].' <span class="rouble">a</span>'?></div>
						<div class="goods-one__count">
							<div class="select__wrap _mini js-widget pull-right" onclick="return {'select': {}}">
								<select class="select"><?
									for($i = 1; $i <= $iMaxQuantity; $i++) {
										?><option value="<?=$i?>"><?=$i?></option><?
									}
								?></select>
							</div>
						</div>
					</div><?

					$sBackUrl = (!$bAjaxRequest) ? $GLOBALS['APPLICATION']->GetCurPageParam('', array('CODE', 'ID')) : '/';

					?><a data-id="<?=$arProduct['INFO']['ID']?>" data-price="<?=$arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>" class="cart-buy__button btn _style_1" href="#" onclick="yaCounter24799106.reachGoal('basket_click' ); ga ('send','pageview','/basket_click'); return true;">в корзину</a>
				</div>
				<div class="social _orange"><?
					$arShare = array(
						'facebook' => '_facebook',
						'vkontakte' => '_vk',
						'odnoklassniki' => '_ok',
						'twitter' => '_twitter'
					);
					foreach($arShare as $key => $class) {
						?><div class="social__item"><a class="<?=$class?> social__item-in" href="#" onclick="JNiyamaShare.<?=$key?>('<?=$purl?>', '<?=$ptitle?>', '<?=$pimg?>', '<?=$text?>'); return false"></a></div><?
					}
				?></div>
			</div>
			<div class="goods-one__bigpic">
				<img src="<?=$arDetailPicture['SRC']?>" alt="" />
				<a href="javascript:;" class="fancybox-close goods-one__bigpic-close" title="Close"></a>
			</div>

		</div>
        <?
        $APPLICATION->IncludeComponent(
            'adv:system.empty',
            'niyama.1.0.codes',
            array(
                'MODE' => 'product',
                'productId' => $arProduct['INFO']['ID'],
                'productPrice' => $arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
                'productUrl' => $arProduct['INFO']['DETAIL_PAGE_URL'],
                'productImage' => $arDetailPicture['SRC'],
                'productName' => $arProduct['INFO']['NAME'],
                'productCategory' => $arProduct['INFO']['IBLOCK_SECTION_ID'],
                'productCategoryName' => $arProduct['INFO']['SECTION']['name']
            )
        );
        ?>

        <?
		if (!$bAjaxRequest) {
			# Рекомендуем
			$GLOBALS['APPLICATION']->IncludeComponent(
				'adv:system.empty',
				'niyama.1.0.product-recommended',
				array(
					'BLOCK_TITLE' => 'Рекомендуем',
					'CODE' => $arParams['CODE'],
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}
	} else {
		if(!$bAjaxRequest) {
			ShowError('По вашему запросу ничего не найдено');
			@define('ERROR_404', 'Y');
			//LocalRedirect('/');
		}
	}
} else {
	if(!$bAjaxRequest) {
		ShowError('По вашему запросу ничего не найдено');
		@define('ERROR_404', 'Y');
		//LocalRedirect('/');
	}
}

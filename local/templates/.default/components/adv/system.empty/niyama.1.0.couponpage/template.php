<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$coupon_in_cart = CNiyamaCoupons::GetIDsCouponsByCart();

?><div onclick="return {'coupons': {}}" class="coupons js-widget"><?
	foreach(CNiyamaCoupons::GetListCurUser() as $coupon) {
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.coupon',
			array(
				'CUPONS_IDS' => $coupon,
				'SHOW_USED' => 'Y',
				'IN_CART' => isset($coupon_in_cart[$coupon]) ? 'Y' : 'N',
				'VAL_COUPON_USED' => $coupon_in_cart[$coupon]['PROPS']['val_coupon_used'],
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	}
?></div><?

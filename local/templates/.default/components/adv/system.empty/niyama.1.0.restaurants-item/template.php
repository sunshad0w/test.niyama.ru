<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Ресторан детально ($arResult приходит из параметров)
//
$arResult = $arParams['arResult'];
$sElementCode = $arParams['~sElementCode'];
$arCurRestaurant = array();
if(strlen($sElementCode)) {
	foreach($arResult['RESTAURANTS'] as $arItem) {
		if($arItem['CODE'] == $sElementCode) {
			$arCurRestaurant = $arItem;
			break;
		}
	}
}
if(!$arCurRestaurant) {
	if(strlen($sElementCode)) {
		ShowError('По вашему запросу ничего не найдено');
		@define('ERROR_404', 'Y');
	}
} else {
	?><div onclick="return {'restsInfo': {}}" class="rests-info js-widget"><?
		?><p class="rests-info__title"><?=$arCurRestaurant['NAME']?></p><?
		if($arCurRestaurant['PROPERTIES']['F_DOWNLOAD']['_VALUE_']) {
			$arTmpFile = reset($arCurRestaurant['PROPERTIES']['F_DOWNLOAD']['_VALUE_']);
			if($arTmpFile['SRC']) {
				$sTmpCaption = $arTmpFile['DESCRIPTION'] ? $arTmpFile['DESCRIPTION'] : 'Меню ресторана';
				$arPathInfo = pathinfo($arTmpFile['SRC']);
				$sTmpStyle = '';
				if($arCurRestaurant['PROPERTIES']['F_DOWNLOAD_BG']['_VALUE_']) {
					$arTmpBg = reset($arCurRestaurant['PROPERTIES']['F_DOWNLOAD_BG']['_VALUE_']);
					if($arTmpBg['SRC']) {
						$sTmpStyle = ' style="background-image: url(\''.$arTmpBg['SRC'].'\')"';
					}
				}
				?><div class="rests-info__side">
    		    	<div class="badge _menu"<?=$sTmpStyle?>>
						<div class="badge__text">
							<a href="<?=$arTmpFile['SRC']?>" class="badge__link"><?=$sTmpCaption?></a><br>
							<div class="badge__desc"><?=$arPathInfo['extension']?> (<?=CProjectUtils::GetStrFileSize($arTmpFile['FILE_SIZE'])?>)</div>
						</div>
					</div>
				</div><?
			}
		}
		?><div class="rests-info__desc"><?
			?><div class="rests-info__contacts">
				<div class="contacts"><?
					if($arCurRestaurant['PROPERTIES']['S_ADDRESS']['VALUE']) {
						?><div class="contacts__column _address">
							<div class="contacts__title">адрес</div>
							<div class="contacts__desc"><?=nl2br($arCurRestaurant['PROPERTIES']['S_ADDRESS']['VALUE'])?></div>
						</div><?
					}

					if($arCurRestaurant['PROPERTIES']['S_SHEDULE']['VALUE']) {
						?><div class="contacts__column _time">
							<div class="contacts__title">время работы</div>
							<div class="contacts__desc"><?=nl2br($arCurRestaurant['PROPERTIES']['S_SHEDULE']['VALUE'])?></div>
						</div><?
					}

					if($arCurRestaurant['PROPERTIES']['S_TEL']['VALUE']) {
						?><div class="contacts__column _phone">
							<div class="contacts__title">телефон</div>
							<div class="contacts__desc"><?=nl2br($arCurRestaurant['PROPERTIES']['S_TEL']['VALUE'])?></div>
						</div><?
					}
				?></div>
			</div><?

			if($arCurRestaurant['DETAIL_TEXT']) {
				echo $arCurRestaurant['DETAIL_TEXT'];
			}

			if($arCurRestaurant['PROPERTIES']['F_PHOTOS']['_VALUE_']) {
				?><div class="rests-info__carousel"><?
					foreach($arCurRestaurant['PROPERTIES']['F_PHOTOS']['_VALUE_'] as $arTmpFile) {
						$arTmpThumb = CFile::ResizeImageGet(
							$arTmpFile,
							array(
								'width' => 88,
								'height' => 86
							),
							BX_RESIZE_IMAGE_EXACT,
							true
						);
						$arTmpBig = CFile::ResizeImageGet(
							$arTmpFile,
							array(
								'width' => 800,
								'height' => 800
							),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							true
						);
						if($arTmpThumb && $arTmpBig) {
							?><div class="rests-info__carousel-item"><a href="<?=$arTmpBig['src']?>" rel="rests" class="fancybox-gallery"><img src="<?=$arTmpThumb['src']?>" alt="" width="<?=$arTmpThumb['width']?>" height="<?=$arTmpThumb['height']?>"></a></div><?
						}
					}
				?></div><?
			}

			if($arCurRestaurant['PROPERTIES']['S_WAY_SUBWAY']['VALUE'] || $arCurRestaurant['PROPERTIES']['S_WAY_CAR']['VALUE']) {
				?><div class="rests-info__howto"><?
					?><label class="label rests-info__howto-title">Как добраться</label><?

					if($arCurRestaurant['PROPERTIES']['S_WAY_SUBWAY']['VALUE']) {
						$iSubwayStationId = intval($arCurRestaurant['PROPERTIES']['R_SUBWAY']['VALUE']);
						$sSubwayLineXmlId = $iSubwayStationId > 0 && $arResult['SUBWAY'][$iSubwayStationId]['LINE'] ? $arResult['SUBWAY'][$iSubwayStationId]['LINE']['XML_ID'] : '';
						$sTmpLineMarker = '';
						if(strlen($sSubwayLineXmlId)) {
							$arColors = array(
								'M1' => '#cd0505',
								'M2' => '#0b7121',
								'M3' => '#082a8b',
								'M4' => '#2694d2',
								'M5' => '#800200',
								'M6' => '#ff8001',
								'M7' => '#93017d',
								'M8' => '#e0c201',
								'M9' => '#a3a6b5',
								'M10' => '#8cce3a',
								'M11' => '#29b2a6',
								'M12' => '#32c0da',
							);
							if($arColors[$sSubwayLineXmlId]) {
								$sTmpLineMarker = '<span style="background: '.$arColors[$sSubwayLineXmlId].'" class="rests-info__howto-metro"> </span>';
							}
						}
						?><div class="rests-info__howto-row">
							<div class="rests-info__howto-img"><img src="<?=$templateFolder.'/images/metro.png'?>" alt=""></div>
							<div class="rests-info__howto-desc">
								<div class="rests-info__howto-subtitle">На метро </div>
								<div class="rests-info__howto-body"><?=$sTmpLineMarker.$arCurRestaurant['PROPERTIES']['S_WAY_SUBWAY']['~VALUE']['TEXT']?></div><?
							?></div>
						</div><?
					}

					if($arCurRestaurant['PROPERTIES']['S_WAY_CAR']['VALUE']) {
						?><div class="rests-info__howto-row">
							<div class="rests-info__howto-img"><img src="<?=$templateFolder.'/images/auto.png'?>" alt=""></div>
							<div class="rests-info__howto-desc">
								<div class="rests-info__howto-subtitle">На автомобиле</div>
								<div class="rests-info__howto-body"><?=$arCurRestaurant['PROPERTIES']['S_WAY_CAR']['~VALUE']['TEXT']?></div><?
							?></div>
						</div><?
					}
				?></div><?
			}
		?></div>
	</div><?
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

?><div class="rests-info__side" style="margin-top:36px"> 
	<div class="badge _menu">
		<div class="badge__text"><?
			$GLOBALS['APPLICATION']->IncludeFile(
				SITE_DIR.'include_areas/restaurants-index-side.php',
				array(),
				array(
					'MODE' => 'html',
					'SHOW_BORDER' => true,
					'NAME' => '"Область меню ресторана"',
				)
			);
		?></div>
	</div>
</div>
<div class="rests-info__desc"><?
	$GLOBALS['APPLICATION']->IncludeFile(
		SITE_DIR.'include_areas/restaurants-index-desc.php',
		array(),
		array(
			'MODE' => 'html',
			'SHOW_BORDER' => true,
			'NAME' => '"Область с приветственным тектсом"',
		)
	);
?></div><?

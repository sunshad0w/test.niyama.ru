<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Вакансии
//
?><div class="vacancies">
	<h1><?=$arParams['~PAGE_TITLE']?></h1><?

	// фильтр по городам
	$APPLICATION->IncludeComponent(
		'niyama:vacancies.city-filter',
		'niyama.1.0',
		array(
			'IBLOCK_CODE' => 'vacancies',
			'SORT_BY1' => 'SORT',
			'SORT_ORDER1' => 'ASC',
			'SORT_BY2' => 'NAME',
			'SORT_ORDER2' => 'ASC',

			'CACHE_TYPE' => 'A',
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);

	// текстовый блок
	$GLOBALS['APPLICATION']->IncludeFile(
		SITE_DIR.'include_areas/vacancies-top.php',
		array(),
		array(
			'MODE' => 'html',
			'SHOW_BORDER' => true
		)
	);

	// вакансии города
	$arCmpResult = $APPLICATION->IncludeComponent(
		'niyama:vacancies.list',
		'niyama.1.0',
		array(
			'IBLOCK_CODE' => 'vacancies',
			'SORT_BY1' => 'SORT',
			'SORT_ORDER1' => 'ASC',
			'SORT_BY2' => 'NAME',
			'SORT_ORDER2' => 'ASC',
			'CITY_LEVEL_SECTION_ID' => $GLOBALS['NIYAMA_CITY_LEVEL_SECTION_ID'],
			//'RESTAURANT_LEVEL_SECTION_ID' => isset($_REQUEST['RESTAURANT_ID']) ? intval($_REQUEST['RESTAURANT_ID']) : 0,
			'RESTAURANT_LEVEL_SECTION_ID' => 0,

			'CACHE_TYPE' => 'Y',
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
	if($GLOBALS['NIYAMA_CITY_LEVEL_SECTION_ID'] && !$arCmpResult['CITY']) {
		ShowError('Неверно указан город');
		@define('ERROR_404', 'Y');
	}

?></div><?

<?php

#
# Получение активного голосования
#
session_start();


if (!empty($_REQUEST['VOTE_ID'])){
	$_SESSION['iProfileVoteId']=$_REQUEST['VOTE_ID'];
	$_SESSION['VoteOk']=true;
	global $APPLICATION;
	LocalRedirect($APPLICATION->GetCurUri());
}
if (!empty($_SESSION['iProfileVoteId'])&&(int)$_SESSION['iProfileVoteId']>0) {
	$iVoteId=$_SESSION['iProfileVoteId'];
	unset($_SESSION['iProfileVoteId']);
}else{
	$iVoteId = CNiyamaVotes::GetActiveBySID('VOTE_NIYAMA');
}

$APPLICATION->IncludeComponent(
	"bitrix:voting.current", 
	"niyama.1.0.profile-result", 
	array(
		"CHANNEL_SID" => "VOTE_NIYAMA",
		"VOTE_ID" => $iVoteId,
		"VOTE_ALL_RESULTS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "Y",
		"TEXT_OK" => "Спасибо за участие в опросе, нам важно ваше мнение",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SHOW_RESULT" => "Y"
	),
	false
);

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';
$bNofilter = isset($arParams['NOFILTER']) && $arParams['NOFILTER'] == 'Y';

$iCurrentPage = isset($_REQUEST['page']) && intval($_REQUEST['page']) > 1 ? intval($_REQUEST['page']) : 1;

$iCatalogIBlockId = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');
	
#
# Получаем СЕО Рраздела
#
$sSeoText = '';
$sPageHeadTitle = '';
if(!empty($arParams['REQUEST_MENU'])) {
	$sMetaTitle = $arParams['REQUEST_MENU']['NAME'];
	$sPageHeadTitle = $arParams['REQUEST_MENU']['NAME'];
	$obSeoPropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($iCatalogIBlockId, $arParams['REQUEST_MENU']['ID']);
	$arSeoPropValues = $obSeoPropValues->GetValues();
	if($arSeoPropValues) {
		$sMetaTitle = $arSeoPropValues['SECTION_META_TITLE'] ? $arSeoPropValues['SECTION_META_TITLE'] : $sMetaTitle;
		$sPageHeadTitle = $arSeoPropValues['SECTION_PAGE_TITLE'] ? $arSeoPropValues['SECTION_PAGE_TITLE'] : $sPageHeadTitle;
		if($arSeoPropValues['SECTION_META_DESCRIPTION']) {
			$GLOBALS['APPLICATION']->SetPageProperty('description', $arSeoPropValues['SECTION_META_DESCRIPTION']);
		}
		if($arSeoPropValues['SECTION_META_KEYWORDS']) {
			$GLOBALS['APPLICATION']->SetPageProperty('keywords', $arSeoPropValues['SECTION_META_KEYWORDS']);
		}
	}
	$GLOBALS['APPLICATION']->SetTitle($sMetaTitle);

	if(!empty($arParams['REQUEST_MENU']['ID'])) {
		$arCategory = array();
		$arRes = array_reverse(CCustomProject::GetIBlockParentSection($iCatalogIBlockId, $arParams['REQUEST_MENU']['ID']));
		foreach($arRes as $arItem) {
			$arCategory[] = array(
				'name' => $arItem['NAME'],
				'id' => $arItem['ID'],
				'code' => $arItem['CODE'],
				'url' => $arItem['SECTION_PAGE_URL'],
				'description' => $arItem['DESCRIPTION'],
			);
			$sSeoText = $arItem['DESCRIPTION'];
		}
	}
}

//
// SEO-инфоблок
//
$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.catalog-seo',
	array(
		'SET_TITLE' => 'Y',
		'SET_KEYWORDS' => 'Y',
		'SET_DESCRIPTION' => 'Y',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);
$sSeoDescription = $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_TEXT', '');
$sSeoH1 = $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_H1', '');
$sSeoText = $sSeoDescription ? $sSeoDescription : $sSeoText;
$sSeoText = $iCurrentPage > 1 ? '' : $sSeoText;
$sPageHeadTitle = $sSeoH1 ? $sSeoH1 : $sPageHeadTitle;
$sPageHeadTitle = $iCurrentPage > 1 ? '' : $sPageHeadTitle;


$GLOBALS['APPLICATION']->AddChainItem('Меню', '/menu/');
if(!empty($arCategory)) {
	foreach($arCategory as $arCatItem) {
		$GLOBALS['APPLICATION']->AddChainItem($arCatItem['name'], $arCatItem['url']);
	}
} else {
	$GLOBALS['APPLICATION']->AddChainItem($arParams['REQUEST_MENU']['NAME'], '/cat/'.$_REQUEST['MENU_CODE'].'/');
}

$rRuestMenu = array($arParams['REQUEST_MENU']['ID']);

$arSectionFilter = array();

$_SESSION['NIYAMA']['FASET'] = array(
	'menu' => (!empty($arParams['REQUEST_MENU']['ID'])) ? array($arParams['REQUEST_MENU']['ID']) : array(),
	'ex_menu' => (!empty($_REQUEST['ex_menu']) && (is_array($_REQUEST['ex_menu']))) ? $_REQUEST['ex_menu'] : array(),
	'ing' => (!empty($_REQUEST['ing']) && (is_array($_REQUEST['ing']))) ? $_REQUEST['ing'] : array(),
	'ex_ing' => (!empty($_REQUEST['ex_ing']) && (is_array($_REQUEST['ex_ing']))) ? $_REQUEST['ex_ing'] : array(),
);
# Фильтр
$arPFilter = array();
$arPFilterNoIng = array();
$sQueryWord = '';
if(!empty($arParams['REQUEST_MENU'])) {
	$arPFilterMenu = false;
	if (!empty($arParams['REQUEST_MENU']['ID'])) {
		$arPFilterMenu = true;
		$arPFilter['SECTION_ID'] = $arParams['REQUEST_MENU']['ID'];
		$arPFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		//$arSectionFilter[] = $rRuestMenu;
	}

	$arPFilterSMenu = array();
	if(!empty($_REQUEST['specmenu']) && (is_array($_REQUEST['specmenu']))) {
		if($arPFilterMenu) {
			$arPFilter['LOGIC'] = 'OR';
			$arPFilter['PROPERTY_SPEC_MENU.ID'] = $_REQUEST['specmenu'];
		} else {
			$arPFilter['PROPERTY_SPEC_MENU.ID'] = $_REQUEST['specmenu'];
		}
		$arSectionFilter[] = $_REQUEST['specmenu'];
	}

	$sQueryWord = $_REQUEST['q'];
	$arIngredients = $_REQUEST['ings'];

	if(strlen($sQueryWord)) {
		$alling = CNiyamaCatalog::getIngredients(
			null,
			array(
				'NAME' => trim($sQueryWord)
			)
		);
		if (!empty($alling)) {
			$arIngredients[] = $alling[0]['ID'];
			$sQueryWord = '';
		}
	}

	$arPFilterIng = array();
	$arPFilterNoIng = $arPFilter;
	if(!empty($arIngredients) && (is_array($arIngredients))) {
		$arSectionFilter[] = $arIngredients;
		if($arPFilterMenu) {
			$arPFilter['LOGIC'] = 'AND';
			$arPFilter[] = array(
				'LOGIC' => 'OR',
				array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
				array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
			);
		} else {
			//$arPFilter['LOGIC'] = 'OR';
			$arPFilter[] = array(
				'LOGIC' => 'OR',
				array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
				array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
			);
		}
	}
	$arExIngredients = $_REQUEST['ex_ing'];
	if(!empty($arExIngredients) && (is_array($arExIngredients))) {
		$arPFilter['LOGIC'] = 'AND';
		$arPFilter[] = array(
			'LOGIC' => 'AND',
			array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $iCatalogIBlockId, '?PROPERTY_GENERAL_INGREDIENT' => $arExIngredients))),
			array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $iCatalogIBlockId, '?PROPERTY_MORE_INGREDIENT' => $arExIngredients))),
		);
	}
	$arExIngredients = $_REQUEST['alerg'];
	if(!empty($arExIngredients) && (is_array($arExIngredients))) {
		$arPFilter['LOGIC'] = 'AND';
		$arPFilter[] = array(
			'LOGIC' => 'AND',
			array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $iCatalogIBlockId, '?PROPERTY_EXCLUDE_INGREDIENT' => $arExIngredients))),
		);
	}
}


$isFirstPage = false;
$arNavArray = array('nPageSize' => 24);
if(!empty($_REQUEST['page'])) {
	$arNavArray['iNumPage'] = intval($_REQUEST['page']);
	$isFirstPage = intval($_REQUEST['page']) == 1;
} else {
	$arNavArray['iNumPage'] = 1;
	$isFirstPage = true;
}
if(strlen($sQueryWord)) {
	$temp_ar= array(
		'LOGIC' => 'OR',
		//array('NAME' => '%'.trim($sQueryWord).'%'),
		array('PROPERTY_IMPORT_ELEMENT.NAME' => '%'.$sQueryWord.'%'),
		array('PROPERTY_GENERAL_INGREDIENT.NAME' => '%'.$sQueryWord.'%'),
		array('PROPERTY_MORE_INGREDIENT.NAME' => '%'.$sQueryWord.'%')
	);
	$arPFilter[] = $temp_ar;
	$arPFilterNoIng[] = $temp_ar;
}

$arAllElements =  CNiyamaCatalog::getAllMenuElements(1);

$tmpSections = array();
foreach ($arSectionFilter as $item) {
	foreach ($item as $roe) {
		$tmpSections[] = $roe;
	}
}

$iPerPage = 24;
$arNavArray = null;
$arMenuItems = CNiyamaCatalog::GetList(null, $arPFilter, null, $arNavArray);
$arMenuItemsNoIng = CNiyamaCatalog::GetList(null, $arPFilterNoIng, null, $arNavArray);

#
# Пересортировка с частотой заказов
#
$arDisplayProducts = $arMenuItems['ITEMS'] && is_array($arMenuItems['ITEMS']) ? $arMenuItems['ITEMS'] : array();
if(!empty($arMenuItems['ITEMS'])) {
	$iUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetId() : false;
	$arOrderProductsIds = CNiyamaCatalog::GetOrderProductsIds($iUserId);
	if($arOrderProductsIds) {
		$arTmpIsOrders = array();
		foreach($arOrderProductsIds as $iProductId => $iOrdersCnt) {
			if(isset($arMenuItems['ITEMS'][$iProductId])) {
				$arTmpIsOrders[$iProductId] = $arMenuItems['ITEMS'][$iProductId];
				unset($arMenuItems['ITEMS'][$iProductId]);
			}
		}
		$arDisplayProducts = array_merge($arTmpIsOrders, $arMenuItems['ITEMS']);
	}
}


#
# Проверка на Меню и ингредиенты
#
if($arPFilterMenu && !empty($_REQUEST['ings'])) {
	$arTmpIsOrdersIng = array();
	if(!empty($arDisplayProducts)) {
		foreach ($arDisplayProducts as $key => $val) {
			$iSectionId = (isset($val['INFO']['IBLOCK_SECTION_ID']) && !empty($val['INFO']['IBLOCK_SECTION_ID'])) ? $val['INFO']['IBLOCK_SECTION_ID'] : false;
			if($iSectionId) {
				if(in_array($iSectionId, $rRuestMenu)) {
					# Получаем ингредиетны
					$iIngProduct = array();
					if(!empty($val['ING']['ALL_INGREDIENT'])) {
						$iIngProduct = array_keys($val['ING']['ALL_INGREDIENT']);
					}

					$arTmpCount = array();
					foreach($_REQUEST['ings'] as $uIng) {
						if(in_array($uIng, $iIngProduct)) {
							$arTmpCount[] = $uIng;
						}
					}
					if(count($arTmpCount) == count($_REQUEST['ings'])) {
						$arTmpIsOrdersIng[] = $val;
						unset($arDisplayProducts[$key]);
					}
				}
			}
		}

		/* 
		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = (array)$arTmpIsOrdersIng;
		}
		*/
		if(!empty($arTmpIsOrdersIng)) {
			$tmparDub = array();
			foreach($arTmpIsOrdersIng as $key => $srow) {
				$iSubIrow = array_keys($srow['ING']['ALL_INGREDIENT']);
				if(count($iSubIrow) == count($_REQUEST['ing'])) {
					$tmparDub[] = $srow;
					unset($arTmpIsOrdersIng[$key]);
				}
			}
			if(!empty($tmparDub)) {
				$arTmpIsOrdersIng = array_merge((array)$tmparDub, (array)$arTmpIsOrdersIng);
			}
		}

		if(!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge((array)$arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}


#
# Получаем общий массив ингредиентов
#
$GLOBALS['ALL_REQUEST_ING'] = array();
if($arPFilterMenu) {
	if(!empty($arDisplayProducts)) {
		$arGing = array();
		foreach($arDisplayProducts as $key => $val) {
			# Получаем ингредиетны
			$iIngProduct = array();
			if(!empty($val['ING']['ALL_INGREDIENT'])) {
				$arGing = array_merge((array)$arGing, array_keys($val['ING']['ALL_INGREDIENT']));
			}
		}
	}
}

// для получения всех видимых
if(!empty($arMenuItemsNoIng['ITEMS'])) {
	foreach($arMenuItemsNoIng['ITEMS'] as $key => $val) {
		# Получаем ингредиетны
		if(!empty($val['ING']['ALL_INGREDIENT'])) {
			$arGing = array_merge((array)$arGing, array_keys($val['ING']['ALL_INGREDIENT']));
		}
	}
}
if(!empty($arGing)) {
	$GLOBALS['ALL_REQUEST_ING'] = array_unique($arGing);
}

#
# Получаем общий массив исключающих ингредиентов
#
$GLOBALS['ALL_REQUEST_EX_ING'] = array();
$arGingEx = array();
if($arPFilterMenu) {
	if(!empty($arDisplayProducts)) {
		$arGingEx = array();
		foreach($arDisplayProducts as $key => $val) {
			# Получаем ингредиетны
			$iIngProduct = array();
			if(!empty($val['EX_ING']['EXCLUDE_INGREDIENT'])) {
				$arGingEx = array_merge((array)$arGingEx, array_keys($val['EX_ING']['EXCLUDE_INGREDIENT']));
			}
		}
	}
}

// для получения всех видимых
if(!empty($arMenuItemsNoIng['ITEMS'])) {
	foreach($arMenuItemsNoIng['ITEMS'] as $key => $val) {
		# Получаем ингредиетны
		if(!empty($val['EX_ING']['EXCLUDE_INGREDIENT'])) {
			$arGingEx = array_merge((array)$arGingEx, array_keys($val['EX_ING']['EXCLUDE_INGREDIENT']));
		}
	}
}
if(!empty($arGingEx)) {
	$GLOBALS['ALL_REQUEST_EX_ING'] = array_unique($arGingEx);
}


#
# Обработка перед выводом
#
$iAllCount = count($arDisplayProducts);
$arDisplayProducts = ADV_array_pagination($arDisplayProducts, $iCurrentPage, $iPerPage - 1);
//if (!empty($arDisplayProducts)) {
	if ((isset($_REQUEST['ings']) && (!empty($_REQUEST['ings']))) || (isset($_REQUEST['alerg']) && (!empty($_REQUEST['alerg'])))) {
		?><div class="catalog__filters-act _inner"><?
			$SelIngs = array();
			if (!empty($_REQUEST['ings'])) {
				foreach($_REQUEST['ings'] as $row) {
					$SelIngs[$row]='ings[]=' .$row;
				}
			}
			if (!empty($_REQUEST['alerg'])) {
				foreach($_REQUEST['alerg'] as $row) {
					$SelIngs[$row] = 'alerg[]=' .$row;
				}
			}
			if (!empty($_REQUEST['ings'])) {
				foreach($_REQUEST['ings'] as $row) {
					if (isset($arAllElements[$row])) {
						$TmpUrl = $SelIngs;
						unset($TmpUrl[$row]);

						if (!empty($TmpUrl)) {
							$url = 'Search/?'.implode('&', $TmpUrl);
						} else {
							$url = '';
						}
						$cur_page = $GLOBALS['APPLICATION']->GetCurPage(false);
						$pos = strpos($cur_page, 'Search');
						if ($pos !== false) {
							$cur_page = substr($cur_page, 0, $pos);
						}
						?><div data-filter="<?=$row?>"><?=$arAllElements[$row]?><a href="<?=$cur_page.$url?>" class="catalog__filter-del">x</a></div><?
					}
				}
			}
			if (!empty($_REQUEST['alerg'])) {
				foreach($_REQUEST['alerg'] as $row) {
					if (isset($arAllElements[$row])) {
						$TmpUrl=$SelIngs;
						unset($TmpUrl[$row]);

						if (!empty($TmpUrl)) {
							$url = 'Search/?'.implode('&', $TmpUrl);
						} else {
							$url = '';
						}
						$cur_page = $GLOBALS['APPLICATION']->GetCurPage(false);
						$pos = strpos($cur_page, 'Search');
						if ($pos !== false) {
							$cur_page = substr($cur_page, 0, $pos);
						}
						?><div data-filter="<?=$row?>"><span style="text-decoration: line-through;"><?=$arAllElements[$row]?></span><a href="<?=$cur_page.$url?>" class="catalog__filter-del">x</a></div><?
					}
				}
			}
		?>&nbsp;</div><?
	}
//}

if(!empty($arParams['REQUEST_MENU'])) {
	?><h2 class="h2"><?=$sPageHeadTitle?></h2><?
}


if($bAjaxRequest) {
	ob_start();
}

$bShowProductsList = true;
if($bShowProductsList) {
	?><ul onclick="return {'catalog': {}}" id="iso" class="catalog js-widget"><?
		foreach($arDisplayProducts as $item) {
			# Фото
			$arAvaData = CFile::GetFileArray($item['INFO']['PREVIEW_PICTURE']);
			/*
			if(!empty($item['EX_ING']['EXCLUDE_INGREDIENT'])) {
				$ex_ings = array();
				foreach($item['EX_ING']['EXCLUDE_INGREDIENT'] as $key => $elem) {
					if (!in_array($elem, $item['ING']['ALL_INGREDIENT'])) {
						$ex_ings[$key] = $elem;
					}
				}
				if(!empty($ex_ings)) {
					$item['ING']['ALL_INGREDIENT'] = array_merge($item['ING']['ALL_INGREDIENT'], $ex_ings);
				}
			}
			*/

			$sIngValue = '';
			if(isset($item['ING']['ALL_INGREDIENT']) && !empty($item['ING']['ALL_INGREDIENT'])) {
				$sIngValue = implode(', ', $item['ING']['ALL_INGREDIENT']);
			}

			?><li data-category="one" class="catalog__item">
				<div class="goods">
					<div class="goods__wrap">
						<div class="goods__image fancybox" data-fancybox-group="goods" data-fancybox-type="ajax" href="/ajax/getGoodsCard.php?CODE=<?=$item['INFO']['CODE']?>"><img width="188" src="<?=$arAvaData['SRC']?>" alt=""></div>
						<div class="goods__desc">
							<a href="<?=$item['INFO']['DETAIL_PAGE_URL']?>">
								<h2 class="goods__name"><?=$item['INFO']['NAME']?></h2>
							</a>
							<div class="goods__composition"><?=$sIngValue?></div>
							<div class="goods__cost"><?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?> <span class="rouble">a </span></div><?
							/*
							?><div class="goods__add-btn"><a href="/ajax/add2cart.php?pid=<?=$item['INFO']['ID']?>" class="btn" data-id="<?=$item['INFO']['ID']?>" data-price="<?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>" onclick="yaCounter24799106.reachGoal('basket_click' ); ga ('send','pageview','/basket_click'); ga ('send', 'pageview',' /basket_click'); return true;"> В корзину</a></div><?
							*/
							?><div class="goods__add-btn"><a href="javascript:void(0)" class="btn" data-id="<?=$item['INFO']['ID']?>" data-price="<?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>" onclick="yaCounter24799106.reachGoal('basket_click'); ga('send', 'pageview', '/basket_click'); ga('send', 'pageview',' /basket_click'); return true;">В корзину</a></div><?
						?></div><?
						if (!empty($item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE'])) {
							?><div class="goods__num"><?=$item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE'].' шт.'?></div><?
						}
					?></div>
				</div>
			</li><?
		}
	?></ul><?

	/**
	 * Постраничная навигация
	 */
	/*
	$arTmpQueryParams = array();
	if(isset($_REQUEST['q']) && strlen(trim($_REQUEST['q']))) {
		$arTmpQueryParams['q'] = htmlspecialcharsbx(trim($_REQUEST['q']));
	}
	if($_SESSION['NIYAMA']['FASET'] && is_array($_SESSION['NIYAMA']['FASET'])) {
		foreach($_SESSION['NIYAMA']['FASET'] as $sTmpParamName => $arTmpParamValue) {
			if($arTmpParamValue) {
				$arTmpQueryParams[$sTmpParamName] = $arTmpParamValue;
			}
		}
	}
	*/
	$APPLICATION->IncludeComponent(
		'adv:system.empty',
		'niyama.1.0.catalog-pagenav',
		array(
			'CURRENT_PAGE' => $iCurrentPage,
			'ITEMS_ALL_CNT' => $iAllCount,
			'PER_PAGE' => $iPerPage,
			'AJAX_REQUEST_TYPE' => 'N',
			//'ADD_QUERY_PARAMS' => $arTmpQueryParams ? http_build_query($arTmpQueryParams) : '',
			'ADD_QUERY_PARAMS' => CProjectUtils::DeleteGetParams(CProjectUtils::GetSefParams(array('page'))),
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
}

if(empty($arDisplayProducts)) {
	?><p style="color: red; margin: 25px 0;">По заданному фильтру блюда не найдены</p><?
	@define('ERROR_404', 'Y');
}

if(strlen($sSeoText)) {
	?><div class="description_main"><?=$sSeoText?></div><?
}

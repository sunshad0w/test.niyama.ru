<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

global $issetCode;

$utm_source = $_COOKIE['utm_source'];
$utm_medium = $_COOKIE['utm_medium'];
$utm_campaign = $_COOKIE['utm_campaign'];

if (isset($_GET["utm_source"])){
    setcookie("utm_source",$_GET["utm_source"],time() + 3600 * 24 * 30, '/');
    $utm_source = $_GET['utm_source'];
}
if (isset($_GET["utm_medium"])){
    setcookie("utm_medium",$_GET["utm_medium"],time() + 3600 * 24 * 30, '/');
    $utm_source = $_GET['utm_medium'];
}
if (isset($_GET["utm_campaign"])){
    setcookie("utm_campaign",$_GET["utm_campaign"],time() + 3600 * 24 * 30, '/');
    $utm_source = $_GET['utm_campaign'];
}
if (isset($_GET["actionpay"])){
    setcookie("actionpay",$_GET["actionpay"],time() + 3600 * 24 * 30, '/');
}

if (isset($_GET["admitad_uid"])){
    setcookie("admitad_uid",$_GET["admitad_uid"],time() + 3600 * 24 * 30, '/');
}

// apypxl
switch($arParams['MODE']){
    case 'main':
        $issetCode = 1;
        ?>

        <script async="true" type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=main&mid=75484"></script>

        <?/*?>
        <script type="text/javascript">
            window._ad = window._ad || [];
            window._ad.push({code: "6a4a7498a3", level: 0}); (function () {
                var s=document.createElement("script");
                s.async=true;
                s.src=(document.location.protocol == "https:" ? "https:" : "http:") + "//сdn.admitad.com/static/js/retag.js";
                var a=document.getElementsByTagName("script")[0]
                a.parentNode.insertBefore(s,a);
            })()
        </script>
        <?*/?>

        <script type="text/javascript">
            window._retag = window._retag || [];
            window._retag.push({code: "9ce88869fe", level: 0});
            (function () {
                var id = "admitad-retag";
                if (document.getElementById(id)) {return;}
                var s = document.createElement("script");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
                var a = document.getElementsByTagName("script")[0]
                a.parentNode.insertBefore(s, a);
            })()
        </script>


        <?
        // АкшенПей
        ?>
        <script>
            window.APRT_DATA = {
                pageType: 1
            };
        </script>
        <?
        break;

    case 'category':
        $issetCode = 1;
        foreach($arParams['products'] as $item) {
            $item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE'] = round($item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE']);
            $goodsStr[] = "{$item['INFO']['ID']}:{$item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE']}";
        }
?>

<script async=”true” type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=list&codes=<?=implode(',',$goodsStr)?>&cat_id=<?=$arParams['categoryId']?>&mid=75484"></script>

<script type="text/javascript">
    window.ad_category = "<?=$arParams['categoryId']?>";
    // required
    window._retag = window._retag || [];
    window._retag.push({code: "9ce88869f1", level: 1});
    (function () {
        var id = "admitad-retag";
        if (document.getElementById(id)) {return;}
        var s=document.createElement("script");
        s.async = true; s.id = id;
        var r = (new Date).getDate();
        s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
        var a = document.getElementsByTagName("script")[0]
        a.parentNode.insertBefore(s, a);
    })()
</script>

<script>
window.APRT_DATA = {
    pageType: 3,
    currentCategory: {
        id: <?=$arParams['categoryId']?>,
        name: <?=$arParams['categoryName']?>
    }
};
</script>

    <?
        break;

    case 'product':
        $issetCode = 1;
        ?>
        <script type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=card&codes=<?=$arParams['productId']?>:<?=$arParams['productPrice']?>&mid=75484"></script>

        <script type="text/javascript">
            window.ad_product = {
                "id": "<?=$arParams['productId']?>",
                // required
                "vendor": "niyama",
                "price": "<?=$arParams['productPrice']?>",
                "url": "http://niyama.ru<?=$arParams['productUrl']?>",
                "picture": "<?=$arParams['productImage']?>",
                "name": "<?=$arParams['productName']?>",
                "category": "<?=$arParams['productCategory']?>"
            };
            window._retag = window._retag || [];
            window._retag.push({code: "9ce88869f0", level: 2});
            (function () {
                var id = "admitad-retag";
                if (document.getElementById(id)) {return;}
                var s = document.createElement("script");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
                var a = document.getElementsByTagName("script")[0]
                a.parentNode.insertBefore(s, a);
            })()
        </script>

        <?
        // АкшенПей
        ?>
        <script>
            window.APRT_DATA = {
                pageType: 2,
                currentCategory: {
                    id: '<?=$arParams['productCategory']?>',
                    name: '<?=$arParams['productCategoryName']?>'
                },
                currentProduct: {
                    id: '<?=$arParams['productId']?>',
                    name: '<?=$arParams['productName']?>',
                    price: '<?=$arParams['productPrice']?>'
                }
            };
        </script>
         <?
        break;

    case 'basket':
        $issetCode = 1;
        $prodArr = $prodArrGdeSlon = $prodActionPay = array();
        foreach($arParams['products'] as $arItem)
            $prodArr[] = "{id: '{$arItem['ID']}', number: ".(int)$arItem['QUANTITY']."}";
            for ($i = 1; $i <= $arItem['QUANTITY']; $i++) {
                $prodArrGdeSlon[] = "{$arItem['ID']}:".(int)$arItem['PRICE']."";
            }
            $prodActionPay[] = "{id: '{$arItem['ID']}', name: '{$arItem['_PRODUCT_']['NAME']}', quantity: {$arItem['QUANTITY']}}";
        ?>

        <script type="text/javascript">
            window.ad_products = [<?=implode(',',$prodArr)?>];
            window._retag = window._retag || [];
            window._retag.push({code: "9ce88869f3", level: 3});
            (function () {
                var id = "admitad-retag";
                if (document.getElementById(id)) {return;}
                var s = document.createElement("script");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
                var a = document.getElementsByTagName("script")[0]
                a.parentNode.insertBefore(s, a);
            })()
        </script>


        <script type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=basket&codes=<?=implode(',',$prodArrGdeSlon)?>&mid=75484"></script>

        <script>
            window.APRT_DATA = {
                pageType: 4,
                basketProducts: [<?=implode(',',$prodActionPay)?>]
            };
        </script>
<?

        break;
    case 'thanks':
        $issetCode = 1;
        if(!empty($arParams['products'])) {
            $goodsStr = '';
            $goodsStrArr = $prodArr = array();
            $amount = 0;
            foreach ($arParams['products'] as $product) {
                $product['price'] = round($product['price']);
                for ($i = 1; $i <= $product['quantity']; $i++) {
                    $goodsStrArr[] = "{$product['id']}:" . (int)$product['price'] . "";
                }
                $prodArr[] = "{id: '{$product['id']}', number: ".(int)$product['quantity']."}";
                $prodActionPay[] = "{id: '{$product['id']}', name: '{$product['name']}',price: '{$product['price']}', quantity: ".(int)$product['quantity']."}";
                $amount = $amount + $product['price']*$product['quantity'];
            }
            $goodsStr = implode(',',$goodsStrArr);
        }

        if ($utm_source == 'mament' && $utm_medium == 'CPA' && $utm_campaign == 'sale') {
            echo '<img src="http://r.mament.ru/pixel?conversion_type=3&order_id='.$arParams['orderId'].'&amount='.$arParams['orderTotal'].'" width="1px" height="1px"/>';
            echo '<script type="text/javascript" src="http://r.mament.ru/pixel?conversion_type=3&order_id='.$arParams['orderId'].'&amount='.$arParams['orderTotal'].'&tid="></script>';
        }

        // АкшенПей

        //if ($utm_source == 'actionpay' && $utm_medium == 'cpa' && $utm_campaign == 'agency' && isset($_COOKIE["actionpay"])) {
        if(isset($_COOKIE["actionpay"]) && $_COOKIE["actionpay"] != '') {
            echo '<img src="http://apypxl.com/ok/9398.png?actionpay=' . $_COOKIE["actionpay"] . '&apid=' . $arParams['orderId'] . '&price=' . $arParams['orderTotal'] . '" width="1px" height="1px"/>';
        }
        ?>
            <script>
                window.APRT_DATA = {
                    pageType: 6,
                    purchasedProducts: [<?=implode(',',$prodActionPay)?>],
                    orderInfo: {
                        id: '<?=$arParams['orderId']?>',
                        totalPrice: '<?=$arParams['orderTotal']?>'
                    }
                };
            </script>
            <?
        //}

        echo '<script type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=thanks&codes='.$goodsStr.'&order_id='.$arParams['orderId'].'&mid=75484"></script>';
        echo '<script type="text/javascript" src="https://www.gdeslon.ru/thanks.js?merchant_id=75484&codes=001:'.$arParams['orderTotal'].'&order_id='.$arParams['orderId'].'"></script>';

        ?>

        <script type="text/javascript">
            window.ad_order = "<?=$arParams['orderId']?>";
            // required
            window.ad_amount = "<?=$arParams['orderTotal']?>";
            window.ad_products = [<?=implode(',',$prodArr)?>];
            window._retag = window._retag || [];
            window._retag.push({code: "9ce88869f2", level: 4});
            (function () {
                var id = "admitad-retag";
                if (document.getElementById(id)) {return;}
                var s = document.createElement("script");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static/js/retag.min.js?r="+r;
                var a = document.getElementsByTagName("script")[0]
                a.parentNode.insertBefore(s, a);
            })()
        </script>

        <?
        if(isset($_COOKIE["admitad_uid"]) && $_COOKIE["admitad_uid"] != '') {
            ?>
            <script type="text/javascript">
                (function (d, w) {
                    w._admitadPixel = {
                        response_type: 'img',
                        action_code: '1',
                        campaign_code: '6a4a7498a3'
                    };
                    w._admitadPositions = w._admitadPositions || [];
                    <?
                    $noscriptStr = '';
                    $i = 0;
                    $position_count = count($arParams['products']);
                    foreach($arParams['products'] as $product){
                    $i++;
                    ?>
                    w._admitadPositions.push({
                        uid: '<?=$_COOKIE["admitad_uid"]?>',
                        order_id: '<?=$arParams['orderId']?>',
                        position_id: '<?=$i?>',
                        client_id: '',
                        tariff_code: '1',
                        currency_code: '',
                        position_count: <?=$position_count?>,
                        price: '<?=$product['price']?>',
                        quantity: '<?=$product['quantity']?>',
                        product_id: '<?=$product['id']?>',
                        screen: '',
                        tracking: '',
                        old_customer: '',
                        coupon: '',
                        promocode: '',
                        payment_type: 'sale'
                    });
                    <?
                    $noscriptStr .= "&order_id={$arParams['orderId']}&position_id={$i}&tariff_code=1&currency_code=&position_count={$position_count}&price={$product['price']}&quantity={$product['quantity']}&product_id={$product['id']}&coupon=&promocode=&payment_type=sale";
                    }?>
                    var id = '_admitad-pixel';
                    if (d.getElementById(id)) {
                        return;
                    }
                    var s = d.createElement('script');
                    s.id = id;
                    var r = (new Date).getTime();
                    var protocol = (d.location.protocol === 'https:' ? 'https:' : 'http:');
                    s.src = protocol + '//cdn.asbmit.com/static/js/pixel.min.js?r=' + r;
                    d.head.appendChild(s);
                })(document, window)
            </script>
            <noscript>
                <img src="//ad.admitad.com/r?campaign_code=6a4a7498a3&action_code=1&response_type=img&uid=<?=$_COOKIE["admitad_uid"]?><?=$noscriptStr?>"
                     width="1" height="1" alt="">
            </noscript>


        <?
        }
        break;
    default:
        // значение по умолчанию, вызывается в футере, если ранее не вызывались коды, то покажем код по умолчанию
        if(!$issetCode) {
            ?>
            <script>
                window.APRT_DATA = {
                    pageType: 0
                };
            </script>
            <script type="text/javascript" src="https://www.gdeslon.ru/landing.js?mode=other&mid=75484"></script>

        <?
        }
        break;
}

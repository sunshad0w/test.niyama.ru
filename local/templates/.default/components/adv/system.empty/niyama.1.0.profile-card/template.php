<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($sErrorMessage = ADV_get_flashdata('ERROR')) {
    ShowError($sErrorMessage);
}


$arCardData = CNiyamaIBlockCardData::GetCardData();
$sCardNum = '';

if ($arCardData && strlen($arCardData['PROPERTY_CARDNUM_VALUE'])) {
    $sCardNum = trim($arCardData['PROPERTY_CARDNUM_VALUE']);
}

$sCardType = '';
if ($sCardNum) {
    $sCardType = CNiyamaDiscountCard::GetCardTypeByCardNum($sCardNum);
}

$iDiscount = CNiyamaIBlockCardData::GetUserDiscountDB();
//$iDiscount = $arCardData['TRUE_DISCOUNT'] ? $arCardData['TRUE_DISCOUNT'] : 0;
$bCard = $iDiscount > 0 || $arCardData;
?>
<?if (!empty($arCardData)): ?>
    <span class = "cabinet__cart-number" >№ <?= $sCardNum; ?></span>
    <div class="cabinet__card">

        <!--<a href="/ajax/getProfileAddCardForm.php" data-fancybox-type="ajax" class="fancybox cabinet__card-submit btn _style_4"> -->

        <div class="profile-card" data-url="/ajax/getProfileAddCardForm.php">
            <div class="cabinet__card-img-wrap">
                <div class="cabinet__card-img">
                    <?
                    $sCardSrc = '/images/profile/pizza_pi_card.png';
                    switch ($sCardType) {
                        case 'vip':
                        case 'special':
                        case 'staff':
                        case 'sushied':
                        case 'pizza_pi':
                        case 'test':
                            $sCardSrc = '/images/profile/' . $sCardType . '_card.png';
                            break;
                    }
                    ?>
                    <img src="<?= $sCardSrc?>" />
                </div>
            </div>
        </div>
        <div class="cabinet__cart-discount-wrap">
            <?if(!empty($iDiscount) && $iDiscount !=0 ):?>
                <div class="cabinet__cart-discount-restaurant"><span>Скидка в ресторане</span><span class="cabinet__cart-discount-circle"><?= empty($iDiscount)? 0 : $iDiscount?>%</span></div>
                <?if(intval($iDiscount) > 15):?>
                    <div class="cabinet__cart-discount-delivery"><span>Максимальный размер скидки на доставку</span><span class="cabinet__cart-discount-circle">15%</span></div>
                <?endif;?>
            <?endif;?>
        </div>
    </div>
<? else: ?>
    <div class="cabinet__card">

        <!--<a href="/ajax/getProfileAddCardForm.php" data-fancybox-type="ajax" class="fancybox cabinet__card-submit btn _style_4"> -->

        <div class="profile-card profile-no__card" data-url="/ajax/getProfileAddCardForm.php">
            <div class="cabinet__card-img-wrap">
                <div class="cabinet__card-img">
                    <?
                    $sCardSrc = '/images/profile/pizza_pi_card.png';
                    switch ($sCardType) {
                        case 'vip':
                        case 'special':
                        case 'staff':
                        case 'sushied':
                        case 'pizza_pi':
                        case 'test':
                            $sCardSrc = '/images/profile/' . $sCardType . '_card.png';
                            break;
                    }
                    ?>

                </div>
            </div>
        </div>
        <div class="cabinet__cart-discount-wrap">
            <?if(!empty($iDiscount) && $iDiscount !=0 ):?>
                <div class="cabinet__cart-discount-restaurant"><span>Скидка в ресторане</span><span class="cabinet__cart-discount-circle"><?= empty($iDiscount)? 0 : $iDiscount?>%</span></div>
                <?if(intval($iDiscount) > 15):?>
                    <div class="cabinet__cart-discount-delivery"><span>Максимальный размер скидки на доставку</span>&nbsp;<span class="cabinet__cart-discount-circle">15%</span></div>
                <?endif;?>
            <?endif;?>
        </div>
    </div>
<? endif;?>

<script>


    BX.ready(function () {
        var oPopupConfirm = new BX.PopupWindow('DiscountCard', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'black', opacity: '80'
            },
            events: {
                onPopupShow: function () {
                    $('body').on('submit', '.card_popup_confirm', function (event) {
                        event.preventDefault();
                        var formData = new FormData($(this)[0]);
                        console.log(formData);

                        $.ajax({
                            type: "POST",
                            url: '/ajax/getProfileAddCardForm.php',
                            data: formData,
                            processData: false,
                            contentType: false,
                            headers: {
                                "Upgrade-Insecure-Requests": 1
                            },
                            success: function (data) {
                                oPopupConfirm.setContent(data);
                                console.log('ololol');
                                console.log(data);
                            }
                        });
                    });
                }
            }
        });


//console.log(url);
        $('.profile-card').click(function () {
            var url = $('.profile-card').data('url');
            window.sendAjaxDiscountCard(url);
        });


        window.sendAjaxDiscountCard = function (url) {
            $.get(url,
                function (response) {
                    if (response) {
                        oPopupConfirm.setContent(response);
                        oPopupConfirm.show();
                    } else {
                        console.log('ошибка')
                    }
                });
        };


    });


</script>
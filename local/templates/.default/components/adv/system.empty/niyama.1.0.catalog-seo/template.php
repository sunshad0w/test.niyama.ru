<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// SEO-инфоблок
//

$arParams['SET_TITLE'] = $arParams['SET_TITLE'] && $arParams['SET_TITLE'] == 'N' ? 'N' : 'Y';
$arParams['SET_KEYWORDS'] = $arParams['SET_KEYWORDS'] && $arParams['SET_KEYWORDS'] == 'N' ? 'N' : 'Y';
$arParams['SET_DESCRIPTION'] = $arParams['SET_DESCRIPTION'] && $arParams['SET_DESCRIPTION'] == 'N' ? 'N' : 'Y';
$arParams['SEO_TEXT_PAGE_PROP_NAME'] = $arParams['SEO_TEXT_PAGE_PROP_NAME'] && strlen($arParams['SEO_TEXT_PAGE_PROP_NAME']) ? $arParams['SEO_TEXT_PAGE_PROP_NAME'] : 'NIYAMA_SEO_TEXT';
$arParams['SEO_H1_PAGE_PROP_NAME'] = $arParams['SEO_H1_PAGE_PROP_NAME'] && strlen($arParams['SEO_H1_PAGE_PROP_NAME']) ? $arParams['SEO_H1_PAGE_PROP_NAME'] : 'NIYAMA_SEO_H1';

CModule::IncludeModule('iblock');

$sSeoDescription = '';
$sSeoH1 = '';
$sSeoTitle = '';
$sCurPage = $GLOBALS['APPLICATION']->GetCurPage(false);
$sCurPageParams = $GLOBALS['APPLICATION']->GetCurPageParam('', array(), false);
$arTmp = parse_url($sCurPageParams);
$arCurPageParams = array();
if($arTmp['query']) {
	parse_str($arTmp['query'], $arCurPageParams);
}

$iSEOIBlockId = CProjectUtils::GetIBlockIdByCode('catalog_seo', 'catalog');
$arFilter = array(
	'IBLOCK_ID' => intval($iSEOIBlockId),
	'ACTIVE' => 'Y', 
	'PROPERTY_URL' => $sCurPage.'%'
);
$dbItems = CIBlockElement::GetList(
	array(
		'SORT' => 'ASC'
	), 
	$arFilter, 
	false, 
	false, 
	array(
		'ID', 'DETAIL_TEXT', 
		'PROPERTY_URL', 'PROPERTY_TITLE', 'PROPERTY_KEYWORDS', 'PROPERTY_DESCRIPTION', 'PROPERTY_SEO_H1'
	)
);
while($arSeoItem = $dbItems->Fetch()) {
	if(!$arSeoItem['PROPERTY_URL_VALUE']) {
		continue;
	}

	$arTmp = parse_url($arSeoItem['PROPERTY_URL_VALUE']);
	if($arTmp['path'] != $sCurPage) {
		continue;
	}
	$arTmpPageParams = array();
	if($arTmp['query']) {
		parse_str($arTmp['query'], $arTmpPageParams);
	}

	if($arTmpPageParams) {
		if(!$arCurPageParams) {
			continue;
		}
		$bEqual = true;
		foreach($arTmpPageParams as $sParamName => $mParamVal) {
			if($arCurPageParams[$sParamName]) {
				if((is_array($arCurPageParams[$sParamName]) && is_array($mParamVal)) || (!is_array($arCurPageParams[$sParamName]) && !is_array($mParamVal))) {
					if(is_array($mParamVal)) {
						$arTmpDiff = array_diff($mParamVal, $arCurPageParams[$sParamName]);
						if($arTmpDiff || count($mParamVal) != count($arCurPageParams[$sParamName])) {
							$bEqual = false;
						}
					} else {
						if($arCurPageParams[$sParamName] != $mParamVal) {
							$bEqual = false;
						}
					}
				} else {
					$bEqual = false;
				}
			} else {
				$bEqual = false;
			}
			if(!$bEqual) {
				break;
			}
		}
		if(!$bEqual) {
			continue;
		}
	}

	$sSeoDescription = $arSeoItem['DETAIL_TEXT'];
	$sSeoH1 = $arSeoItem['PROPERTY_SEO_H1_VALUE'];

	$sSeoTitle = $arSeoItem['PROPERTY_TITLE_VALUE'];
	if($arParams['SET_KEYWORDS'] == 'Y' && strlen($arSeoItem['PROPERTY_KEYWORDS_VALUE'])) {
		$GLOBALS['APPLICATION']->SetPageProperty('keywords', $arSeoItem['PROPERTY_KEYWORDS_VALUE']);
	}
	if($arParams['SET_DESCRIPTION'] == 'Y' && strlen($arSeoItem['PROPERTY_DESCRIPTION_VALUE'])) {
		$GLOBALS['APPLICATION']->SetPageProperty('description', $arSeoItem['PROPERTY_DESCRIPTION_VALUE']);
	}
}

if($arParams['SET_TITLE'] == 'Y' && strlen($sSeoTitle)) {
	$GLOBALS['APPLICATION']->SetTitle($sSeoTitle);
}

$GLOBALS['APPLICATION']->SetPageProperty($arParams['SEO_TEXT_PAGE_PROP_NAME'], $sSeoDescription);
$GLOBALS['APPLICATION']->SetPageProperty($arParams['SEO_H1_PAGE_PROP_NAME'], $sSeoH1);

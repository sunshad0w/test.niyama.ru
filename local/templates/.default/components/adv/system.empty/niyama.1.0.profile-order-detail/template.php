<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$iOrderId = (isset($_REQUEST['ORDER_ID']) && intval($_REQUEST['ORDER_ID']) > 0) ? intval($_REQUEST['ORDER_ID']) : false;
$arOrder = array();
$bAccess = false;

if($iOrderId) {
	$arOrder = CNiyamaOrders::GetOrderById($iOrderId, true);
	if ($arOrder) {
		if ($arOrder['CAN_ACCESS'] == 'Y') {
			$bAccess = true;
		}
	}
}


$sFoOrderId = strlen($arOrder['ORDER']['UF_FO_ORDER_CODE']) ? $arOrder['ORDER']['UF_FO_ORDER_CODE'] : ' н/д';

$GLOBALS['APPLICATION']->SetTitle('Заказ №'.$sFoOrderId);

$arCurrentItemsCount = $arOrder['ORDER_CART']['current'][0]['ITEMS'] ? count($arOrder['ORDER_CART']['current'][0]['ITEMS']) : array();

$iGuestItemsCount = 0;
$iGuestCount = 0;
if($arOrder['ORDER_CART']['guest'] && is_array($arOrder['ORDER_CART']['guest'])) {
	foreach($arOrder['ORDER_CART']['guest'] as $arItem) {
		if(isset($arItem['ITEMS'])) {
			$iGuestItemsCount += count($arItem['ITEMS']);
		}
		$iGuestCount++;
	}
}
if((!empty($arOrder['ORDER_CART']['GUEST_CNT']['QUANTITY'])) && ($arOrder['ORDER_CART']['GUEST_CNT']['QUANTITY'] > 0)) {
	$iGuestCount += (int)$arOrder['ORDER_CART']['GUEST_CNT']['QUANTITY'];
}

$iAllProductCount = (intval($arCurrentItemsCount) + intval($iGuestItemsCount));
$sAllTmpValCnt = CProjectUtils::PrintCardinalNumberRus($iAllProductCount, 'блюд', 'блюдо', 'блюда');
$sGuestTmpValCnt = CProjectUtils::PrintCardinalNumberRus($iGuestCount, 'гостей', 'гость', 'гостя');

# Выделяем купоны
//$arCupons = array();
//foreach ($arOrder['ORDER_CART']['common'] as $oRow) {
//	foreach ($oRow['ITEMS'] as $cId => $sRow) {
//		if ($sRow['PROPS']['type'] == CNiyamaCart::$arCartItemType[1] ) {
//			$arCupons[] = $sRow;
//		}
//	}
//}

# Выделяем дополнительные компоненты
//$arDopComp = array();
//foreach ($arOrder['ORDER_CART']['common'] as $oRow) {
//	foreach ($oRow['ITEMS'] as $cId => $sRow) {
//		if ($sRow['PROPS']['type'] == CNiyamaCart::$arCartItemType[2] ) {
//			$arDopComp[] = $sRow;
//		}
//	}
//}


function PrintOrderItem($arItem) {
	$bReturn = false;
	switch ($arItem['PROPS']['type']) {

		# Купон
		case 'cupon':
			$bReturn = true;
			?><div class="order__item"><?
				$GLOBALS['APPLICATION']->IncludeComponent(
					'adv:system.empty',
					'niyama.1.0.coupon',
					array(
						'CUPONS_IDS' => $arItem['PRODUCT_ID'],
						'SHOW_USED' => 'N',
						'SHOW_IN_CART' => 'Y'
					),
					null,
					array(
						'HIDE_ICONS' => 'Y'
					)
				);
			?></div><?
		break;

		# Дополнительный компонент
		case 'dop_component':
			$arDopCompDetail = CNiyamaCatalog::GetDopCompDetailByImpElementId($arItem['PRODUCT_ID'], true);
			if(!empty($arDopCompDetail)) {
				$bReturn = true;
				$arImg = CFile::GetFileArray($arDopCompDetail['PREVIEW_PICTURE']);

				?><div class="order__item">
					<div class="order__item-count">
						<span class="order__item-cost"><?=$arItem['PRICE']?> <span class="rouble">a </span></span>
						<div class="order__item-select"><?=$arItem['QUANTITY']?></div>
					</div>
					<div class="order__item-img"><?
						if($arImg['SRC']) {
							?><img src="<?=$arImg['SRC']?>" alt=""><?
						}
					?></div>
					<div class="order__item-desc">
						<div class="order__item-title"><?=$arDopCompDetail['NAME']?></div>
					</div>
				</div><?
			}
		break;

		# Блюдо
		default:
			$arProduct = CNiyamaCatalog::GetProductDetail($arItem['PRODUCT_ID'], true);

			if($arProduct['INFO'] && $arProduct['INFO']['FO_XML_ID'] != '-') {
				$bReturn = true;
				$arImg = CFile::GetFileArray($arProduct['INFO']['PREVIEW_PICTURE']);
				?><div class="order__item"><? 
					if (($arProduct['INFO']['ACTIVE']=='Y')&&($arProduct['INFO']['_ACTIVE']=='Y')) {
						?><div class="order__item-count">
							<span class="order__item-cost"><?=$arItem['PRICE']?> <span class="rouble">a </span></span>
							<div class="order__item-select"><?=$arItem['QUANTITY']?></div>
						</div><? 
					} else {
						?><div class="order__item-not-in-menu">Отсутствует в меню</div><?
					}
					?><div class="order__item-img"><?
						if($arImg['SRC']) {
							?><img src="<?=$arImg['SRC']?>" alt=""><?
						}
					?></div>
					<div class="order__item-desc">
						<div class="order__item-title"><?=$arProduct['INFO']['NAME']?></div><?
						/*
						if (!empty($arProduct['EXCLUDE_INGREDIENT'])) {
							$ex_ings = array();
							foreach($arProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
								if (!in_array($elem, $arProduct['ALL_INGREDIENT'])){
									$ex_ings[$key] = $elem;
								}
							}
							if (!empty($ex_ings)){
								$arProduct['ALL_INGREDIENT'] = array_merge($arProduct['ALL_INGREDIENT'], $ex_ings);
							}
						}
						*/
						?><div class="order__item-ingredients"><?=implode(', ', $arProduct['ALL_INGREDIENT'])?></div>
					</div>
				</div><?
			}
		break;
	}
	return $bReturn;
}


?><div class="cabinet__main">
	<div class="cabinet__title">
		<h2 class="h1 pull-left">Заказ №<?=$sFoOrderId?></h2>
		<a href="/profile/hero_room/" class="cabinet__title-link pull-right">Комната героя</a>
		<a href="/ajax/getProfileOrderList.php" data-fancybox-type="ajax" class="fancybox cabinet__title-link pull-right">История заказов</a>
	</div>
	<div class="cabinet__info cabinet__transparent"><?

		if ($bAccess) {
			$arGuestsList = CNiyamaCart::GetGuestsList($arUser['ID'], false, false, false);
			$bWasDishesPrinted = false;
			?><div class="order">
				<div class="order__main"><?

					# выводим текущего
					if (!empty($arOrder['ORDER_CART']['current'])) {

						$iSubPriceUser = (isset($arOrder['ORDER_CART']['current'][0]['PRICE'])) ? $arOrder['ORDER_CART']['current'][0]['PRICE'] : '0';
						# Получаем аватар
						$arAvaData = CFile::ResizeImageGet(
							CUsersData::getPersonalPhoto(),
							array(
								'width' => 44,
								'height' => 44
							), 
							BX_RESIZE_IMAGE_EXACT,
							false
						);
						$sScrAvatar = $arAvaData['src'];
						$sNameTab = CUsersData::getUserName();

						$iCount = count($arOrder['ORDER_CART']['current'][0]['ITEMS']);
						$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($iCount, 'блюд', 'блюдо', 'блюда');
						// купоны
						$arCouponsList = array();
						$arTableTypes = array('current', 'guest', 'common');

						foreach($arTableTypes as $sTableType) {
							$arTmpCart = $arOrder['ORDER_CART'][$sTableType] ? $arOrder['ORDER_CART'][$sTableType] : array();

							foreach($arTmpCart as $iPersonId => $arTable) {
								if($arTable['ITEMS']) {
									foreach($arTable['ITEMS'] as $iBasketItemId => $arItem) {
										$arTmp = CNiyamaOrders::GetProductDataByBasketItem($arItem, true, $arOrder['ORDER']['USER_ID']);
										$arItem['_PRODUCT_'] = $arTmp;
										if($arItem['_PRODUCT_']['TYPE'] == 'coupon') {
											$arCouponsList[] = $arItem;
											unset($arOrder['ORDER_CART'][$sTableType][$iPersonId]['ITEMS'][$iBasketItemId]);
										}
									}
								}
							}
						}

						if($arCouponsList) {
							$GLOBALS['APPLICATION']->IncludeComponent(
								'adv:system.empty',
								'niyama.1.0.coupons-order',
								array(
									'arCouponsList' => $arCouponsList,
									'AfterEachCouponBR' => true
								),
								null,
								array(
									'HIDE_ICONS' => 'Y'
								)
							);
						}
						?><div class="order__group">
							<div class="order__group-title">
								<div class="order__group-guest"><img src="<?=$sScrAvatar?>" alt=""></div>
								<div class="order__group-name"><?=$sNameTab?>
									<div class="order__group-info"><?
										echo $iCount.' '.$sTmpValCnt.', <b>'.$iSubPriceUser.' </b><span class="rouble">a</span>';
									?></div>
								</div>
							</div><?
							if (isset($arOrder['ORDER_CART']['current'][0]['ITEMS'])) {
								foreach ($arOrder['ORDER_CART']['current'][0]['ITEMS'] as $arItem) {
									$bTmpRes = PrintOrderItem($arItem);
									$bWasDishesPrinted = $bWasDishesPrinted || $bTmpRes;
								}
							}
						?></div><?
					}

					# Выводим гостей
					if(!empty($arOrder['ORDER_CART']['guest'])) {
						foreach($arOrder['ORDER_CART']['guest'] as $iGuestID => $row) {
							$sScrAvatar = '';
							if($iGuestID == CNiyamaCart::$iForAllGuestID) {
								$sScrAvatar = CNiyamaCart::$sForAllAvatar;
								$sNameTab = CNiyamaCart::$sForAllName;
							} else {
								$arGidData = isset($arGuestsList[$iGuestID]) ? $arGuestsList[$iGuestID] : array();
								if($arGidData['PREVIEW_PICTURE']) {
									$arAvaData = CFile::ResizeImageGet(
										$arGidData['PREVIEW_PICTURE'],
										array(
											'width' => 44,
											'height' => 44
										),
										BX_RESIZE_IMAGE_EXACT,
										false
									);
									$sScrAvatar = $arAvaData['src'];
								}
								$sNameTab = $arGidData ? $arGidData['NAME'] : '-';
							}

							$iPerc = ((isset($arOrder['ORDER_CART']['guest'][$iGuestID]['PRICE']))) ? $arOrder['ORDER_CART']['guest'][$iGuestID]['PRICE'] : '0';
							$iCount = count($arOrder['ORDER_CART']['guest'][$iGuestID]['ITEMS']);

							$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($iCount, 'блюд', 'блюдо', 'блюда');

							?><div class="order__group">
								<div class="order__group-title">
									<div class="order__group-guest"><?
										if($sScrAvatar) {
											?><img src="<?=$sScrAvatar?>" alt=""><?
										}
									?></div>
									<div class="order__group-name"><?
										echo $sNameTab;
										?><div class="order__group-info"><?
											echo $iCount.' '.$sTmpValCnt.', <b>'.$iPerc.' </b><span class="rouble">a</span>';
										?></div>
									</div>
								</div><?

								foreach ($arOrder['ORDER_CART']['guest'][$iGuestID]['ITEMS'] as $arItem) {
									$bTmpRes = PrintOrderItem($arItem);
									$bWasDishesPrinted = $bWasDishesPrinted || $bTmpRes;
								}
							?></div><?
						}
					}

					# Выводим стол "на всех"
					if (!empty($arOrder['ORDER_CART']['common'][0]['ITEMS'])) {
						$iCount = count($arOrder['ORDER_CART']['common'][0]['ITEMS']);
						$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($iCount, 'блюд', 'блюдо', 'блюда');

						?><div class="order__group">
							<div class="order__group-title">
								<div class="order__group-guest"><img src="<?=CNiyamaCart::$sForAllAvatar?>" alt=""></div>
								<div class="order__group-name">Дополнительно на всех</div>
							</div><?

							foreach ($arOrder['ORDER_CART']['common'][0]['ITEMS'] as $arItem) {
								$bTmpRes = PrintOrderItem($arItem);
								$bWasDishesPrinted = $bWasDishesPrinted || $bTmpRes;
							}
						?></div><?
					}

					if(!$bWasDishesPrinted) {
						ShowError('Информация о блюдах не найдена');
					}

					?><div class="order__group">
						<div class="order__amount"><span class="h1">итого </span><span class="h1 order__amount-sum"><?=$arOrder['ORDER']['PRICE']?> </span><span class="rouble">a</span></div>
						<div class="order__bonus">
							<div class="order__bonus-img"><img src="/local/templates/.default/components/niyama/order.make/niyama.1.0/images/icon_1.png" alt=""></div><span class="order__bonus-text"> <span><?=$iGuestCount?> </span><span><?=$sGuestTmpValCnt?>, </span><span><?=$iAllProductCount?> </span><span><?=$sAllTmpValCnt?></span></span>
						</div><?
						/*
						<div class="order__bonus">
							<div class="order__bonus-img"><img src="/local/templates/.default/static/images/delivery.png" alt=""></div><span class="order__bonus-text">Бесплатная доставка</span>
						</div><?
						*/
					?></div>
					<div class="order__group">
						<a href="/order/?action=replace_order&order_id=<?=$arOrder['ORDER']['ID']?>&sessid=<?=bitrix_sessid()?>&backurl=<?=$GLOBALS['APPLICATION']->GetCurPage(false)?>" class="btn _style_4 pull-right">Повторить заказ</a>
					</div>
				</div><?
			} else {
				ShowError('Заказ не найден');
			}
		?></div>
	</div>
</div><?


?><div class="cabinet__side"><?
	# Карта
	$GLOBALS['APPLICATION']->IncludeComponent(
		'adv:system.empty',
		'niyama.1.0.profile-card',
		array(),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);

	# Голосование
	$GLOBALS['APPLICATION']->IncludeComponent(
		'bitrix:voting.form',
		'profile',
		array(
			'VOTE_ID' => '1',
			'VOTE_RESULT_TEMPLATE' => '',
			'CACHE_TYPE' => 'A',
			'CACHE_TIME' => '3600'
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
?></div><?

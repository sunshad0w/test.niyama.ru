<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

#
# TODO Стилизовать - Свёрстано, пример разметки в _popup_congrats.html
#


$arModal = $arParams['DATA'];
$arImg['SRC'] = 'https://cdn0.iconfinder.com/data/icons/customicondesign-office7-shadow-png/128/Metal-gold-blue.png';
if(!empty($nuRow['PROPERTY_MEDAL_ID_PROPERTY_BASE_TYPE_IMG_DOWNLOAD_VALUE'])) {
	$arImg = CFile::GetFileArray($nuRow['PROPERTY_MEDAL_ID_PROPERTY_BASE_TYPE_IMG_DOWNLOAD_VALUE']);
}

$url_soc = 'http://'.$_SERVER['HTTP_HOST'];
$text_soc = '';
$img_soc = '';

$bShowPopup = true;

ob_start();
?><div style="display: none;">
	<div id="inline1"><?
		switch($arModal['sTYPE']) {
			case 'TYPE_1':
				// за уровень скидки
				if($arModal['ELEM_ID']) {
					// id текущей записи в HLB UserModalShow
					$iModalShowItemId = $arModal['iModalShowItemId'] ? $arModal['iModalShowItemId'] : 0;
					// получим список всех уровней скидок
					$arAllDiscountLevels = CNiyamaDiscountLevels::GetDiscountLevels();
					if($arAllDiscountLevels[$arModal['ELEM_ID']]) {
						$arDiscountLevel = $arAllDiscountLevels[$arModal['ELEM_ID']];
						$arDiscountLevelPrn = $arDiscountLevel;
						if($arDiscountLevel['DISCOUNT_VAL_PERC'] > 0) {
							if($arDiscountLevel['B_ONE_TIME'] != 'Y') {
								// это процентная скидка
								$arDiscountLevelPrn = array();

								// получим информацию о текущем проценте скидки юзера
								$arCardData = CNiyamaIBlockCardData::GetCardData($GLOBALS['USER']->GetId());
								$iCurDiscountPerc = $arCardData['TRUE_DISCOUNT'] ? $arCardData['TRUE_DISCOUNT'] : 0;
								// произведем поиск выадашек для аналогичного типа и удалим их из очереди,
								// поскольку за раз может быть сделан прыжок сразу на несколько уровней вверх (незачем поздравлять юзера с 6% скидки, когда на сайте показывается, что у него 10%), 
								// и поздравление выведем о фактическом уровне скидки
								$arAllLevelsCardsDisc = array();
								foreach($arAllDiscountLevels as $arTmpItem) {
									if($arTmpItem['DISCOUNT_VAL_PERC'] > 0 && $arTmpItem['B_ONE_TIME'] != 'Y') {
										$arAllLevelsCardsDisc[$arTmpItem['ID']] = $arTmpItem;
										if($arTmpItem['DISCOUNT_VAL_PERC'] == $iCurDiscountPerc) {
											$arDiscountLevelPrn = $arTmpItem;
										}
									}
								}

								// очистим очередь выпадашек по величине скидки дисконтной карты
								$arUserCurModalsList = CNiyamaModal::GetModalShow($GLOBALS['USER']->GetId());
								if($arUserCurModalsList) {
									foreach($arUserCurModalsList as $arTmpItem) {
										if($arTmpItem['UF_TYPE_ID'] == 'TYPE_1' && $arTmpItem['UF_ELEM_ID'] && $arAllLevelsCardsDisc[$arTmpItem['UF_ELEM_ID']]) {
											// удаляем запись очереди выпадашек по интересующему типу скидок
											// текущую запись HLB не трогаем, она сама удалится
											if($arTmpItem['ID'] != $iModalShowItemId) {
												UserModalShowTable::Delete($arTmpItem['ID']);
											}
										}
									}
								}
							}
						}

						if($arDiscountLevelPrn) {
							?><div class="popup__gratz">
								<div class="popup__gratz-side">
									<h2 class="popup__gratz-title"><?=$arModal['NAME']?></h2>
									<p class="popup__gratz-desc"><?=$arDiscountLevelPrn['DETAIL_TEXT']?></p>
								</div>
								<div class="popup__gratz-medal">
									<div class="medal _big">
									</div>
								</div>
								<div class="popup__gratz-share">
									<div class="label"><?=$arModal['DETAIL_TEXT']?></div>
									<div class="social _auth">
										<div class="_facebook social__item"><a href="#" class="_facebook social__item-in" onclick="JNiyamaShare.facebook('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
										<div class="_vk social__item"><a href="#" class="_vk social__item-in" onclick="JNiyamaShare.vkontakte('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
										<div class="_ok social__item"><a href="#" class="_ok social__item-in" onclick="JNiyamaShare.odnoklassniki('<?=$url_soc?>','<?=$text_soc?>')"></a></div>
										<div class="_google social__item"><a href="#" class="_google social__item-in" onclick="JNiyamaShare.plus('<?=$url_soc?>')"></a></div>
										<div class="_mail social__item"><a href="#" class="_mail social__item-in" onclick="JNiyamaShare.mailru('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>')"></a></div><? 
										/*
										?><div class="_ya social__item"><a href="#" class="_ya social__item-in"></a></div><?
										*/
									?></div>
								</div>
							</div><?
						} else {
							$bShowPopup = false;
						}
					}
				}
			break;

			case 'TYPE_2':
				$arModal['PROPERTY_VIDEO_VALUE'] = '';
				$arImg = array();
				$arImg['SRC'] = 'https://cdn0.iconfinder.com/data/icons/customicondesign-office7-shadow-png/128/Metal-gold-blue.png';
				$img_soc = 'https://cdn0.iconfinder.com/data/icons/customicondesign-office7-shadow-png/128/Metal-gold-blue.png';
				if(!empty($arModal['ELEM_ARR']['PROPERTY_BASE_TYPE_IMG']) && $arModal['ELEM_ARR']['PROPERTY_BASE_TYPE_IMG'] == 'DOWNLOAD') {
					if(!empty($arModal['ELEM_ARR']['PROPERTY_BASE_TYPE_IMG_DOWNLOAD'])) {
						$arImg = CFile::GetFileArray($arModal['ELEM_ARR']['PROPERTY_BASE_TYPE_IMG_DOWNLOAD']);
						$img_soc = 'http://'.$_SERVER['HTTP_HOST'].$arImg['SRC'];
					}
				}
				$medal_name = !empty($arModal['ELEM_ARR']['NAME']) ? ' '.strtolower($arModal['ELEM_ARR']['NAME']) : '';
				$title_soc = 'Я получил медаль'.$medal_name.'!';
				$text_soc = $arModal['ELEM_ARR']['DETAIL_TEXT'];
				?><div class="popup__gratz">
					<div class="popup__gratz-side">
						<h2 class="popup__gratz-title"><?=$arModal['NAME']?></h2>
						<h3 class="popup__gratz-subtitle"><?=$arModal['PREVIEW_TEXT']?><?=(!empty($arModal['ELEM_ARR']['NAME']) ? ' '.strtolower($arModal['ELEM_ARR']['NAME']) : '')?>!</h3>
						<p class="popup__gratz-desc"><?=$arModal['ELEM_ARR']['DETAIL_TEXT']?></p>
					</div>
					<div class="popup__gratz-medal">
						<div class="medal _big">
							<div class="medal__body"><img src="<?=$arImg['SRC']?>" alt="" /></div>
							<div class="medal__ribbon">
								<div class="medal__ribbon-text"><?=$arModal['ELEM_ARR']['NAME']?></div>
							</div>
						</div>
					</div>
					<div class="popup__gratz-share">
						<div class="label"><?=$arModal['DETAIL_TEXT']?></div>
						<div class="social _auth">
							<div class="_facebook social__item"><a href="#" class="_facebook social__item-in" onclick="JNiyamaShare.facebook('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
							<div class="_vk social__item"><a href="#" class="_vk social__item-in" onclick="JNiyamaShare.vkontakte('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
							<div class="_ok social__item"><a href="#" class="_ok social__item-in" onclick="JNiyamaShare.odnoklassniki('<?=$url_soc?>','<?=$text_soc?>')"></a></div>
							<div class="_google social__item"><a href="#" class="_google social__item-in" onclick="JNiyamaShare.plus('<?=$url_soc?>')"></a></div>
							<div class="_mail social__item"><a href="#" class="_mail social__item-in" onclick="JNiyamaShare.mailru('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>')"></a></div><? 
							/*
							?><div class="_ya social__item"><a href="#" class="_ya social__item-in"></a></div><?
							*/
						?></div>
					</div>
				</div><?
			break;

			case 'TYPE_3':
				$title_soc = 'Я зарегистрировался на сайте '.$_SERVER['HTTP_HOST'].'!';
				$text_soc = $arModal['PREVIEW_TEXT'];
				?><div class="popup__gratz">
					<div class="popup__gratz-side">
						<h2 class="popup__gratz-title"><?=$arModal['NAME']?></h2><? 
						/*
						?><h3 class="popup__gratz-subtitle"><?=$arModal['PREVIEW_TEXT']?><?=(!empty($arModal['ELEM_ARR']['NAME']))?' '.strtolower($arModal['ELEM_ARR']['NAME']):''?>!</h3><?
						*/
						?><p class="popup__gratz-desc"><?=$arModal['PREVIEW_TEXT']?></p>
					</div>
					<div class="popup__gratz-medal">
						<div class="medal _big"><? 
							if(!empty($arModal['PROPERTY_VIDEO_VALUE'])) {
								$APPLICATION->IncludeComponent(
									'bitrix:player',
									'',
									array(
										'PLAYER_TYPE' => 'auto',
										'USE_PLAYLIST' => 'N',
										'PATH' => $arModal['PROPERTY_VIDEO_VALUE']['path'],
										'PLAYLIST_DIALOG' => '',
										'PROVIDER' => 'video',
										'STREAMER' => '',
										'WIDTH' => '251',
										'HEIGHT' => '258',
										/*
										'WIDTH' => $arModal['PROPERTY_VIDEO_VALUE']['width'],
										'HEIGHT' => $arModal['PROPERTY_VIDEO_VALUE']['height'],
										*/
										'PREVIEW' => '',
										'FILE_TITLE' => $arModal['PROPERTY_VIDEO_VALUE']['title'],
										'FILE_DURATION' => $arModal['PROPERTY_VIDEO_VALUE']['duration'],
										'FILE_AUTHOR' => $arModal['PROPERTY_VIDEO_VALUE']['author'],
										'FILE_DATE' => $arModal['PROPERTY_VIDEO_VALUE']['date'],
										'FILE_DESCRIPTION' => $arModal['PROPERTY_VIDEO_DESCRIPTION'],
										'SKIN_PATH' => '/bitrix/components/bitrix/player/mediaplayer/skins',
										'SKIN' => 'bitrix.swf',
										'CONTROLBAR' => 'bottom',
										'WMODE' => 'transparent',
										'PLAYLIST' => 'right',
										'PLAYLIST_SIZE' => '180',
										'LOGO' => '/images/logo.png',
										'LOGO_LINK' => 'http://niyama.ru/',
										'LOGO_POSITION' => 'bottom-left',
										'PLUGINS' => array('tweetit-1', 'fbit-1'),
										'PLUGINS_TWEETIT-1' => 'tweetit.link=',
										'PLUGINS_FBIT-1' => 'fbit.link=',
										'ADDITIONAL_FLASHVARS' => '',
										'WMODE_WMV' => 'window',
										'SHOW_CONTROLS' => 'Y',
										'PLAYLIST_TYPE' => 'xspf',
										'PLAYLIST_PREVIEW_WIDTH' => '64',
										'PLAYLIST_PREVIEW_HEIGHT' => '48',
										'SHOW_DIGITS' => 'Y',
										'CONTROLS_BGCOLOR' => 'FFFFFF',
										'CONTROLS_COLOR' => '000000',
										'CONTROLS_OVER_COLOR' => '000000',
										'SCREEN_COLOR' => '000000',
										'AUTOSTART' => 'Y',
										'REPEAT' => 'list',
										'VOLUME' => '90',
										'MUTE' => 'N',
										'HIGH_QUALITY' => 'Y',
										'SHUFFLE' => 'N',
										'START_ITEM' => '1',
										'ADVANCED_MODE_SETTINGS' => 'Y',
										'PLAYER_ID' => '',
										'BUFFER_LENGTH' => '10',
										'DOWNLOAD_LINK' => $arModal['PROPERTY_VIDEO_VALUE']['path'],
										'DOWNLOAD_LINK_TARGET' => '_self',
										'ADDITIONAL_WMVVARS' => '',
										'ALLOW_SWF' => 'Y',
									),
									null,
									array(
										'HIDE_ICONS' => 'Y'
									)
								);
								$img_soc = 'http://'.$_SERVER['HTTP_HOST'].$arModal['PROPERTY_VIDEO_VALUE']['path'];
							} else {
								$arDataImg = CFile::ResizeImageGet($arModal['PREVIEW_PICTURE'], array('width' => 251, 'height' => 258), BX_RESIZE_IMAGE_EXACT, false);
								?><img src="<?=$arDataImg['src']?>" alt=""><?
								$img_soc = 'http://'.$_SERVER['HTTP_HOST'].$arDataImg['src'];
							}
						?></div>
					</div>
					<div class="popup__gratz-share">
						<div class="label"><?=$arModal['DETAIL_TEXT']?></div>
						<div class="social _auth">
							<div class="_facebook social__item"><a href="#" class="_facebook social__item-in" onclick="JNiyamaShare.facebook('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
							<div class="_vk social__item"><a href="#" class="_vk social__item-in" onclick="JNiyamaShare.vkontakte('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>'); return false"></a></div>
							<div class="_ok social__item"><a href="#" class="_ok social__item-in" onclick="JNiyamaShare.odnoklassniki('<?=$url_soc?>','<?=$text_soc?>')"></a></div>
							<div class="_google social__item"><a href="#" class="_google social__item-in" onclick="JNiyamaShare.plus('<?=$url_soc?>')"></a></div>
							<div class="_mail social__item"><a href="#" class="_mail social__item-in" onclick="JNiyamaShare.mailru('<?=$url_soc?>','<?=$title_soc?>','<?=$img_soc?>','<?=$text_soc?>')"></a></div><?
							/*
							?><div class="_ya social__item"><a href="#" class="_ya social__item-in"></a></div><?
							*/
						?></div>
					</div>
				</div><?
			break;
		}
	?></div>
</div><?
$sPopupHtml = ob_get_clean();

if(!$bShowPopup) {
	return;
}

echo $sPopupHtml;

?><script type="text/javascript">
	$(document).ready(function() {
		$.fancybox({
			'href': '#inline1',
			wrapCSS: "fancybox-gratz"
		});
		<?
		if(empty($arModal['PROPERTY_VIDEO_VALUE'])) {
			?>setTimeout("$.fancybox.close()", 10000);<? 
		}
		?>
	});

	JNiyamaShare = {
		vkontakte: function(purl, ptitle, pimg, text) {
			url = 'http://vkontakte.ru/share.php?';
			url += 'url=' + encodeURIComponent(purl);
			url += '&title=' + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&image=' + encodeURIComponent(pimg);
			url += '&noparse=true';
			JNiyamaShare.popup(url);
		},
		odnoklassniki: function(purl, text) {
			url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
			url += '&st.comments=' + encodeURIComponent(text);
			url += '&st._surl=' + encodeURIComponent(purl);
			JNiyamaShare.popup(url);
		},
		facebook: function(purl, ptitle, pimg, text) {
			url = 'http://www.facebook.com/sharer.php?s=100';
			url += '&p[title]='  + encodeURIComponent(ptitle);
			url += '&p[summary]=' + encodeURIComponent(text);
			url += '&p[url]=' + encodeURIComponent(purl);
			url += '&p[images][0]=' + encodeURIComponent(pimg);
			JNiyamaShare.popup(url);
		},
		twitter: function(purl, ptitle) {
			url = 'http://twitter.com/share?';
			url += 'text=' + encodeURIComponent(ptitle);
			url += '&url=' + encodeURIComponent(purl);
			url += '&counturl=' + encodeURIComponent(purl);
			JNiyamaShare.popup(url);
		},
		mailru: function(purl, ptitle, pimg, text) {
			url = 'http://connect.mail.ru/share?';
			url += 'url=' + encodeURIComponent(purl);
			url += '&title=' + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&imageurl=' + encodeURIComponent(pimg);
			JNiyamaShare.popup(url)
		},
		plus: function(purl) {
			url = 'https://plus.google.com/share?';
			url += 'url=' + encodeURIComponent(purl);
			JNiyamaShare.popup(url)
		},
		popup: function(url) {
			window.open(url,'','toolbar=0,status=0,width=626,height=436');
			$.post('/ajax/shareEvent.php', {social:url, type:"modal_share"}, function (data){});
		}
	};
</script><?

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа - страница "Ошибка оплаты заказа" (при оплате онлайн)
//

echo '<h1 class="auth__title">Ошибка оплаты заказа!</h1>';
echo '<div class="ordering-form _thanks">';

$iOrderId = isset($_REQUEST['cf']) ? intval($_REQUEST['cf']) : '';
if($iOrderId > 0) {

	$arOrderData = CNiyamaOrders::GetOrderById($iOrderId, true);
	$arPayments = CNiyamaOrders::GetPayments();
		
	$bCorrectToken = false;
	if($arOrderData && $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['IS_CASH'] != 'Y' && $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['ACTIVE'] == 'Y') {
		$dbItems = CSalePaySystemAction::GetList(
			array(),
			array(
				'PAY_SYSTEM_ID' => $arOrderData['ORDER']['PAY_SYSTEM_ID'],
				'PERSON_TYPE_ID' => $arOrderData['ORDER']['PERSON_TYPE_ID'],
			),
			false,
			false,
			array('ID', 'PAY_SYSTEM_ID', 'PERSON_TYPE_ID', 'NAME', 'ACTION_FILE', 'RESULT_FILE', 'NEW_WINDOW', 'PARAMS', 'ENCODING', 'LOGOTIP')
		);
		if($arPaySysAction = $dbItems->Fetch()) {
			CSalePaySystemAction::InitParamarrays($arOrderData['ORDER'], $arOrderData['ORDER']['ID'], $arPaySysAction['PARAMS']);
			$dOrderPrice = number_format(CSalePaySystemAction::GetParamValue('SHOULD_PAY'), 2, '.', '');
			$sCheckToken = md5(CSalePaySystemAction::GetParamValue('MERCHANT_ID').CSalePaySystemAction::GetParamValue('PRODUCT_ID').$dOrderPrice.CSalePaySystemAction::GetParamValue('ORDER_ID').CSalePaySystemAction::GetParamValue('SW'));

			$sCallbackToken = trim($_REQUEST['token']);
			if($sCheckToken == $sCallbackToken) {
				$bCorrectToken = true;
			}
		}
	}

	if(!$bCorrectToken) {
		ShowError('Доступ запрещен');
	} else {
		?><p>Оплата заказа завершилась неудачей. ID заказа: <?=$arOrderData['ORDER']['UF_FO_ORDER_CODE']?></p><?
	}

} else {
	echo '<p>';
	echo 'Оплата заказа завершилась неудачей.';
	if(isset($_REQUEST['error'])) {
		echo '<br>'.htmlspecialcharsbx($_REQUEST['error']);
	}
	echo '</p>';
}

echo '</div>';

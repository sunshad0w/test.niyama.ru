<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


/**
 * Add
 */
if($_SERVER["REQUEST_METHOD"]=="POST" && ($_REQUEST["addAddress"] == 'Y') && check_bitrix_sessid())
{
    $iIsDefault = (isset($_REQUEST['IS_DEFAULT']) && $_REQUEST['IS_DEFAULT'] == "on")? 1 : 0;
    $sSubWay = (!empty($_REQUEST['SUBWAY'])) ? $_REQUEST['SUBWAY'] : '';
    $addId = CUsersData::addAddress($_REQUEST['REGION'],$_REQUEST['CITY_DOP'],$sSubWay,$_REQUEST['ADDRESS'],$_REQUEST['HOUSE'],$_REQUEST['HOME'],$_REQUEST['PORCH'],$_REQUEST['INTERCOM'],$_REQUEST['FLOOR'],$_REQUEST['WITHIN_MKAD'],$iIsDefault);
    LocalRedirect("/profile/");
}

/**
 * Update
 */
if($_SERVER["REQUEST_METHOD"]=="POST" && ($_REQUEST["editAddress"] == 'Y') && check_bitrix_sessid())
{
    $iIsDefault = (isset($_REQUEST['IS_DEFAULT']) && $_REQUEST['IS_DEFAULT'] == "on")? 1 : 0;
    $iIsDelete = (isset($_REQUEST['IS_DELETE']) && $_REQUEST['IS_DELETE'] == "on")? true : false;
    $sSubWay = (!empty($_REQUEST['SUBWAY'])) ? $_REQUEST['SUBWAY'] : '';
    if (!$iIsDelete) {
        CUsersData::updateAddress($_REQUEST['ID'],$_REQUEST['REGION'],$_REQUEST['CITY_DOP'],$sSubWay,$_REQUEST['ADDRESS'],$_REQUEST['HOUSE'],$_REQUEST['HOME'],$_REQUEST['PORCH'],$_REQUEST['INTERCOM'],$_REQUEST['FLOOR'],$_REQUEST['WITHIN_MKAD'],$iIsDefault);
    } else {
        CUsersData::deleteAddress(intval($_REQUEST['ID']));
    }
    LocalRedirect("/profile/");
}

/**
 * Delete
 */
if (isset($_REQUEST['delete_guest']) && $_REQUEST['delete_guest'] && (isset($_REQUEST['gid'])) && check_bitrix_sessid()){
    

}

/**
 * Выводим форму
 */
if (isset($arParams['SHOW_MODAL'])&&($arParams['SHOW_MODAL'] == "Y")) {

    $isEdit = false;
    $sTitle  = "Добавление адреса доставки";
    $sAddress   = "";
	$sHouse   = "";
    $sSubWay = "";
    $iRegion = "";
	$sCityDop='';
    $iPorch  = "";
    $sHome = "";
    $iIs_default = "";

    $iAddress = (isset($arParams['AID'])) ? $arParams['AID'] : false;
    if ($iAddress){
        $arAdress = CUsersData::GetAddressByID($iAddress);
        $isEdit  = true;
        $sTitle  = "Редактирование адреса доставки";
        $sAddress = $arAdress['PROPERTY_ADDRESS_VALUE'];
		$sHouse = $arAdress['PROPERTY_HOUSE_VALUE'];
        $sSubWay = $arAdress['PROPERTY_SUBWAY_VALUE'];
       // $iRegion = $arAdress['PROPERTY_REGION_ENUM_ID'];
		$iRegion = $arAdress['PROPERTY_REGION_VALUE'];
		$sCityDop = $arAdress['PROPERTY_CITY_DOP_VALUE'];
        $iPorch  = $arAdress['PROPERTY_PORCH_VALUE'];
        $sHome = $arAdress['PROPERTY_HOME_VALUE'];
		$sIntercom = $arAdress['PROPERTY_INTERCOM_VALUE'];
		$sFloor = $arAdress['PROPERTY_FLOOR_VALUE'];
		$sWithInMkad = $arAdress['PROPERTY_WITHIN_MKAD_VALUE'];
        $iIs_default = $arAdress['PROPERTY_IS_DEFAULT_VALUE'];
		
    }
	
	$CityOtherId=intval(CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other', "27"));

?><div class="ordering-form">
        <form method="post" name="form1" action="/profile/" enctype="multipart/form-data"><?
            ?><input type="hidden" name="<?= (!$iAddress) ? "addAddress" : "editAddress"  ?>" value="Y" ><?

            if ($isEdit){
                ?><input type="hidden" name="ID" value="<?= $arAdress['ID'] ?>" ><?
            }

            echo bitrix_sessid_post();

            ?><h1><?= $sTitle ?></h1>
            <div class="popup__user-body">
                <div onclick="return {'addressAutocomplete': {}}" class="ordering-form__body block2 js-widget">
                    <div class="ordering-form__row">
                        <div onclick="return {'select': {}}" class="select__wrap js-widget">
                            <label class="label">Город</label><?
    
                            /*$IBLOCK_ID = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
                            $arPropRegionEnum = CCustomProject::GetEnumPropValues($IBLOCK_ID, 'REGION');*/
                            
                            $arTmp = CNiyamaOrders::GetDeliveryRegions();
                            $arDELIVERY_REGIONS = array();
                            foreach($arTmp as $arItem) {
                                if($arItem['ACTIVE'] == 'Y') {
                                    $arDELIVERY_REGIONS[$arItem['ID']] = $arItem;
                                }
                            }
    
                            ?><select required class="_city select" name="REGION"><?
    
                                $isMsk = ($isEdit) ?  false : true;
                                /*foreach ($arPropRegionEnum as $iRegItem){
                                    if ( ($iRegItem['XML_ID'] == "REGION_1") && ($iRegion == $iRegItem['ID'])){
                                        $isMsk = true;
                                    }
                                    ?><option data-city="<?= $iRegItem['XML_ID'] ?>" value="<?= $iRegItem['ID'] ?>" <?= ( $iRegion == $iRegItem['ID'])? "selected" : "" ?> ><?= $iRegItem['VALUE'] ?></option><?
                                }*/
                                $sRegion='';
                                foreach($arDELIVERY_REGIONS as $arItem) {
                                    $sTmpSelected = '';
                                    if ($arItem['ID'] == $iRegion)	{
                                        $sTmpSelected = ' selected="selected"';
                                        $sRegion=$arItem['NAME'];
                                    }																					
                                    $sTmpDataCity = $arItem['NAME'] == 'Москва' ? 'REGION_1' : 'REGION_X';
									if ($CityOtherId==$arItem['ID']){
										$sTmpDataCity='REGION_OTHER';	
									}
                                    ?><option<?=$sTmpSelected?> data-city="<?=$sTmpDataCity?>" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
                                }
                                if ($sRegion == 'Москва'){
                                    $isMsk = true;
                                }
    
                            ?></select>                        
                            
                        </div>
                      <? /*  <div onclick="return {'select': {}}" class="select__wrap js-widget <?= (!$isMsk)? "_hidden" :"" ?> ">
                            <label class="label">Станция метро</label><?
    
                            ?><select class="_metro select" name="SUBWAY"><?
    
                                $arSubWays = CNiyamaIBlockSubway::GetAllStationsList();
                                ?><option value="no">(Выберете станцию)</option><?
                                foreach ($arSubWays as $sItem) {
                                 ?><option value="<?= $sItem['ID'] ?>" <?= ($iSubWay == $sItem['ID'])? "selected" : ""; ?> ><?= $sItem['NAME'] ?></option><?
                                }
    
                            ?></select>
                        </div> */?>
                        
                        <div class="input__wrap<?=(!$isMsk) ? ' _hidden' : ''?>">
                            <label class="label">Станция метро</label>
                            <input name="SUBWAY" value="<?=htmlspecialcharsbx($sSubWay)?>" data-parsley-group="block2" class="input _metro">
                        </div>
						
                        
                        <div class="input__wrap <?=($iRegion==$CityOtherId)?'':'_hidden'?>">
							<label class="label">Название населённого пункта</label>
							<input name="CITY_DOP" value="<?=$sCityDop?>" data-parsley-group="block2" class="input _other">
						</div>
                    </div>
                    
                    <? /*<div class="ordering-form__row _mkad <?= (!$isMsk)? "_hidden" :"" ?>" <?= (!$isMsk)? 'style="display: none;"':'' ?>><?
                        $sChecked = $sWithInMkad ? ' checked="checked"' : '';
                        ?><input type="checkbox" name="WITHIN_MKAD" value="1"<?=$sChecked?> id="mkad" class="checkbox _big _mkad">
                        <label for="mkad" class="checkbox-label">В пределах МКАД</label>
                    </div>*/?>
                    <div class="ordering-form__row _other-txt" <?=($iRegion==$CityOtherId)?'':'style="display: none;"'?>>
                        	<?=CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other_text', "Не нашли свой город? Позвоните нам <span>+7 495 781-781-9</span> или <a href='/delivery/' target='_blank'>посмотрите карту доставки</a>")?>
					</div>
                    <div class="ordering-form__row">
                    	<div class="input__wrap">
                            <label class="label">Улица (обязательно)</label>
                            <input  class="input _address" required value="<?= $sAddress ?>" name="ADDRESS" />
                        </div>
                        <div class="input__wrap">
                            <label class="label">Дом (обязательно)</label>
                            <input  class="input _house" required value="<?= $sHouse ?>" name="HOUSE" />
                        </div>
                    </div>
                    <div class="ordering-form__row _quarter">
                        <div class="input__wrap">
                            <label class="label">Квартира</label>
                            <input name="HOME" value="<?=$sHome?>" class="input _apart"> <? // input _address?>
                        </div>
                        <div class="input__wrap">
                            <label class="label">Подъезд</label>
                            <input name="PORCH" value="<?=$iPorch?>" class="input _porch">
                        </div>
                        <div class="input__wrap">
                            <label class="label">Домофон</label>
                            <input name="INTERCOM" value="<?=$sIntercom?>" class="input _intercom">
                        </div>
                        <div class="input__wrap">
                            <label class="label">Этаж</label>
                            <input name="FLOOR" value="<?=$sFloor?>" class="input _floor">
                        </div>
                    </div>
                    <?php /*?><div class="ordering-form__row">
                            <label class="label">Корпус, строение, квартира</label>
                            <input class="input _address" value="<?= $sHome ?>" name="HOME" />
                    </div>
                    <div class="ordering-form__row">
                            <label class="label">Подъезд, домофон, этаж</label>
                            <input class="input _address" value="<?= $iPorch ?>" name="PORCH" />
                    </div><?php */?>
    
                    <div class="ordering-form__row align-left">
                        <input value="on" type="checkbox" name="IS_DEFAULT" id="address" class="checkbox _big address" <?= ( $iIs_default )? "checked" : ""; ?>/>
                        <label for="address" data-name="IS_DEFAULT" class="checkbox-label __inline">Мой основной адрес</label><?
                        if ($isEdit){ ?>
                            <input value="on" type="checkbox" name="IS_DELETE" id="delete" class="checkbox _big address"/>
                            <label for="delete" data-name="IS_DELETE" class="checkbox-label __inline">Удалить</label>
                        <?}?>
                    </div><?
    
    
                    ?><div class="ordering-form__row">
                        <input type="submit" value="<?= ($isEdit)? "Сохранить" : "Добавить" ?>" class="btn _style_3"/>
                    </div>
                </div>
        	</div>
        </form>
    </div><?

exit();
}


# Выводим ссылку на добавление гостя
?><a class="cabinet__controls-add fancybox" data-fancybox-type="ajax" href="/ajax/getProfileAddAddressForm.php"><span>Добавить адрес</span></a><?

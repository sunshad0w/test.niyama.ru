<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа
//
if((isset($_REQUEST['action'])) && ($_REQUEST['action'] == 'replace_order') && (isset($_REQUEST['order_id'])) && (check_bitrix_sessid())) {

	//CNiyamaCart::setCurrentOrderData(false);

	$iOrderId = (intval($_REQUEST['order_id']) > 0) ? intval($_REQUEST['order_id']) : 0;
	if($iOrderId) {
		$bCanAccess = false;
		$arOrder = CNiyamaOrders::GetOrderById($iOrderId, true);

		if($arOrder) {
			if($arOrder['CAN_ACCESS'] == 'Y') {
				$bCanAccess = true;
			}
		}

		if(!$bCanAccess) {
			if((isset($_REQUEST['backurl']))&&(!empty($_REQUEST['backurl']))) {
				LocalRedirect($_REQUEST['backurl']);
			}else{
				LocalRedirect('/');
			}
		}
		CNiyamaCart::clearCart();
		$arCartItems = $arOrder['ORDER_CART'];
		// Добавляем к текущим
		if(!empty($arCartItems['current'][0]['ITEMS'])) {
			foreach($arCartItems['current'][0]['ITEMS'] as $cRow) {
				if($cRow['PROPS']['type'] == 'product') {
					$iGuestID = false;
					$iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false);
				}
			}
		}

		// Добавляем гостей
		if(!empty($arCartItems['guest'])) {
			foreach($arCartItems['guest'] as $id => $gRow) {
				foreach($gRow['ITEMS'] as $cRow) {
					if($cRow['PROPS']['type'] == 'product') {
						$iGuestID = $id;
						$iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false);
					}
				}
			}
		}
		# Общий стол
		if(!empty($arCartItems['common'][0]['ITEMS'])) {
			foreach($arCartItems['common'][0]['ITEMS'] as $cRow) {
				if($cRow['PROPS']['type'] == 'product') {
					$iGuestID = CNiyamaCart::$iForAllGuestID;
					$iItemId = CNiyamaCart::AddToCart($cRow['PRODUCT_ID'], $cRow['QUANTITY'], $iGuestID, $cRow['PROPS']['type'], false);
				}
			}
		}
	}

	if((isset($_REQUEST['nexturl'])) && (!empty($_REQUEST['nexturl']))) {
		LocalRedirect($_REQUEST['nexturl']);
	} else {
		LocalRedirect('/order/');
	}
}



if(defined('NIYAMA_ORDER_AJAX_REQUEST')) {
	ob_start();
}
$GLOBALS['APPLICATION']->IncludeComponent(
	'niyama:order.make',
	'niyama.1.0',
	array(
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

if(defined('NIYAMA_ORDER_AJAX_REQUEST')) {
	$sHtml = ob_get_clean();
	$GLOBALS['APPLICATION']->RestartBuffer();
	echo json_encode(array('html' => $sHtml));
}

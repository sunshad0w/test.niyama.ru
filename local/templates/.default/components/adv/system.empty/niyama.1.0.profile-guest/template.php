<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die(); 

?><div class="cabinet__guests"><?

    ?><div class="cabinet__controls"><?

		$sErrMsg = ADV_get_flashdata('profile_guest_popup_error');
		if(strlen($sErrMsg)) {
			echo '<p style="color: red;">'.$sErrMsg.'</p>';
		}

        $APPLICATION->IncludeComponent(
            'adv:system.empty',
            'niyama.1.0.profile-guest-popup',
            array(),
            null,
            array(
                'HIDE_ICONS' => 'Y'
            )
        );

        ?><h2>Ваши гости</h2>
    </div><?

    # Получаем Гостей
    $arGuests = CNiyamaCart::GetCurUserGuestsList();
    if (!empty($arGuests)) {
        foreach ($arGuests as $gItem) {

            # Информация о зазказх
            $arOrderInfo = CNiyamaCart::getGuestOrderInfo($gItem['ID']);
            $sTmpOrderValCnt = "раз";
            if ($arOrderInfo['ORDER_COUNT']){
                $sTmpOrderValCnt = CProjectUtils::PrintCardinalNumberRus($arOrderInfo['ORDER_COUNT'], 'раз', 'раз', 'раза');
            }
            $sTmpProductValCnt = "блюд";
            if ($arOrderInfo['PRODUCT_COUNT']){
                $sTmpProductValCnt = CProjectUtils::PrintCardinalNumberRus($arOrderInfo['PRODUCT_COUNT'], 'блюд', 'блюдо', 'блюда');
            }
            $sTextOrderInfo = "".$arOrderInfo['ORDER_COUNT']." ".$sTmpOrderValCnt." и съел(а) ".$arOrderInfo['PRODUCT_COUNT']." ".$sTmpProductValCnt;


            # Получаем аватар
            $arAvaData = CFile::ResizeImageGet($gItem['PREVIEW_PICTURE'], array('width'=>44, 'height'=>44), BX_RESIZE_IMAGE_EXACT, false);
            ?><div class="guests _cabinet">
                <div class="guests__list">
                    <div class="guests__card _current">
                        <a class="cabinet__edit fancybox" data-fancybox-type="ajax" href="/ajax/getProfileEditGuestForm.php?gid=<?= $gItem['ID'] ?>"></a>

                        <div class="guests__card-img"><img src="<?= $arAvaData['src'] ?>"></div>
                        <div class="guests__card-name"><?= $gItem['NAME'] ?></div>
                    </div>
                    <div class="guests__list-wrap"></div>
                </div>
                <div class="guests__add pull-right">
                    <div class="guests__orders">
                        Трапезничал(a) с вами <a class="fancybox" href="/ajax/getProfileOrderList.php?gid=<?= $gItem['ID'] ?>" data-fancybox-type="ajax"><?= $sTextOrderInfo ?></a>
                    </div>
                </div>
            </div><?

        }
    }
    ?><br>
</div><?

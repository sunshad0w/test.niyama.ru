<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';

/* Форма добавления гостя */

if(!$bAjaxRequest) {
	echo '<div onclick="return {\'popup\': {}}" class="popup js-widget">';
	echo '<div class="popup__overlay"></div>';
	echo '<div class="popup__window">';
} else {
	echo '<form>';
}

?><div class="popup__close"></div>
<p class="h1 popup__title">Новый гость</p>
<label class="label">Как зовут гостя?</label>
<div class="popup__window__row">
	<input type="text" class="input _mb_2">
	<div class="parsley-errors-list"></div>
</div>
<input type="hidden" id="guest_popup_dir" name="dir" value="<?=$GLOBALS['APPLICATION']->GetCurDir()?>">
<label class="label">Выберите фото или аватар</label>
<div class="input__file _photo">Выбрать фото...
	<div id="files" data-id="" class="input__file-pic">
		<img src="/images/blank.png" alt="">
		<div id="progress" class="progress">
			<div class="progress-bar progress-bar-success" style="width: 0"></div>
		</div>
	</div>
	<input id="fileupload" type="file" name="files[]" class="input__file-input">
</div>
<div class="clearfix"></div>
<p class="avatars"><?
	$arFirstAvatar = CUsersData::getDefaultAvatarList();
	$i = 1;
	foreach($arFirstAvatar as $row) {
		$arAvaData = CFile::GetFileArray($row['PREVIEW_PICTURE']);
		?><input type="radio" name="avatar" id="<?=$row['PREVIEW_PICTURE']?>" class="radio"<?=($i == 1) ? ' checked="checked"' : ''?> />
		<label for="<?=$row['PREVIEW_PICTURE']?>" class="radio-avatar"><img src="<?=$arAvaData['SRC']?>" alt="" /></label><?
		$i++;
	}
?></p>
<input value="Добавить" type="submit" class="btn _style_2" /><?

if(!$bAjaxRequest) {
	echo '</div>';
	echo '</div>';
} else {
	echo '</form>';
}

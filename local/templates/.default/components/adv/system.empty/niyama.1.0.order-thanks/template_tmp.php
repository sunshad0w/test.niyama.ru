<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа - страница "Спасибо за заказ" (при оплате наличными)
//

$iOrderId = intval($_REQUEST['id']);
if($iOrderId <= 0) {
	localredirect(SITE_DIR);
	return;
}

$arOrderData = CNiyamaOrders::GetOrderById($iOrderId, true);

if(!$arOrderData || $arOrderData['CAN_ACCESS'] != 'Y') {
	localredirect(SITE_DIR);
	return;
}

unset($_SESSION['NIYAMA']['ORDER']['CUSTOMER_DELIVERY_TYPE']);
$arPayments = CNiyamaOrders::GetPayments();
?><h1 class="auth__title _thanks">Спасибо за заказ!</h1>
<div class="ordering-form _thanks"><?
	//['ORDER_CART']['TOTAL_PRICE']
	if($arOrderData['ORDER']['STATUS_ID'] == 'E' && $arOrderData['ORDER']['UF_FO_WAS_SEND']) {
		?><p><b>Возникли проблемы при отправке заказа</b></p><?
	} else {
		?><p>Наш оператор свяжется с Вами в ближайшее время и уточнит детали заказа.</p>
		<p>ID заказа: <?=$arOrderData['ORDER']['UF_FO_ORDER_CODE']?></p><?
	}
	if (!empty($arOrderData['ORDER_CART']['TOTAL_PRICE']) && $arOrderData['ORDER_CART']['TOTAL_PRICE'] > 0) {
		?><p>Стоимость заказа: <?=$arOrderData['ORDER_CART']['TOTAL_PRICE']?> рублей</p><?
	}
	if($arOrderData['ORDER']['PAY_SYSTEM_ID']) {
		if ($arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['IS_CASH'] == 'Y') {
			echo '<p>Оплата наличными';
			$arTmp = $arOrderData['ORDER_PROPS']['DELIVERY_CHANGE_AMOUNT'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_CHANGE_AMOUNT']) : array();
			$sChangeAmount = $arTmp['VALUE'] ? trim($arTmp['VALUE']) : '';
			if(strlen($sChangeAmount)) {
				echo '<br>Сдача с '.$sChangeAmount;
			}
			echo '</p>';
		} else {
			if (!empty($arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['PSA_NAME'])) {
				echo '<p>Оплата '.$arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['PSA_NAME'];
			}
		}
	}
	$arTmp = $arOrderData['ORDER_PROPS']['DELIVERY_TYPE'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_TYPE']) : array();
	if($arTmp['VALUE'] == self) {
		$city = '';
		$rest = '';
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_RESTAURANT'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_RESTAURANT']) : array();
		$arDeliverRest=CNiyamaOrders::GetDeliveryRestaurantsList();
		if(!empty($arDeliverRest) && !empty($arTmp1['VALUE'])) {
			foreach($arDeliverRest as $key => $el) {
				if($el['DEPARTMENT_ID'] == $arTmp1['VALUE']) {
					$city = $el['CITY_NAME'];
					$rest = $el['NAME'];
				}
			}
		}
		$str_rest = (!empty($city)) ? 'г. '.$city.',' : '';
		$str_rest .= (!empty($rest)) ? ' "'.$rest.'"' : '';
		echo '<p>Ресторан самовывоза'.(!empty($str_rest) ? ': '.$str_rest :'');
	} else {
		$sRegion = '';
		$sSubway = '';
		$sAadress = '';
		$sHouse = '';
		$sApartament = '';
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_REGION'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_REGION']) : array();
		$arTmp2 = $arOrderData['ORDER_PROPS']['DELIVERY_CITY_DOP'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_CITY_DOP']) : array();
		$arDeliverRegions = CNiyamaOrders::GetDeliveryRegions();

		$CityOtherId = intval(CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other', '27'));
		if ($arTmp1['VALUE'] == $CityOtherId) {
			$sRegion = $arTmp2['VALUE'];
		} else {
			$sRegion = $arDeliverRegions[$arTmp1['VALUE']]['NAME'];			
		}
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_SUBWAY'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_SUBWAY']) : array();
		$sSubway = $arTmp1['VALUE'];
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_ADDRESS'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_ADDRESS']) : array();
		$sAadress = $arTmp1['VALUE'];
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_HOUSE'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_HOUSE']) : array();
		$sHouse = $arTmp1['VALUE'];
		$sHouse = (!empty($sHouse)) ? 'дом '.$sHouse : '';
		$sAadress .= (!empty($sAadress)) ? ', '.$sHouse : $sHouse;
		$arTmp1 = $arOrderData['ORDER_PROPS']['DELIVERY_APART'] ? reset($arOrderData['ORDER_PROPS']['DELIVERY_APART']) : array();
		$sApartament = $arTmp1['VALUE'];

		$str_adr = (!empty($sRegion)) ? $sRegion : '';
		if ($sRegion == 'Москва') {
			$str_adr .= (!empty($sSubway)) ? ' (м. '.$sSubway.')' : '';
		}
		$str_adr .= (!empty($sAadress)) ? ', '.$sAadress : '';
		$str_adr .= (!empty($sApartament)) ? ', кв. '.$sApartament : '';
		echo '<p>Доставка'.(!empty($str_adr) ? ': '.$str_adr : '');
	}
	$arTmp = $arOrderData['ORDER_PROPS']['CUSTOMER_EMAIL'] ? reset($arOrderData['ORDER_PROPS']['CUSTOMER_EMAIL']) : array();
	$sEmail = $arTmp['VALUE'] ? trim($arTmp['VALUE']) : '';
	if(strlen($sEmail)) {
		?><p>Дополнительно мы отправили подтверждение заказа Вам на почту: <?=$sEmail?>.</p><?
	}

	//
	// Бонусы
	//
	if($GLOBALS['USER']->IsAuthorized()) {
		$arNewCoupons = CNiyamaCoupons::GetListInOrderByUserId($arOrderData['ORDER']['USER_ID'], $arOrderData['ORDER']['ID']);
		if($arNewCoupons) {
			?><div class="ordering-form__coupons">
				<h2 class="ordering-form__coupons-title">После выполнения заказа вы получите бонусные купоны! <span>Вы сможете воспользоваться ими для оплаты следующего заказа!</span></h2>
				<div><?
					foreach($arNewCoupons as $iCouponId) {
						$GLOBALS['APPLICATION']->IncludeComponent(
							'adv:system.empty',
							'niyama.1.0.coupon',
							array(
								'CUPONS_IDS' => $iCouponId,
							),
							null,
							array(
								'HIDE_ICONS' => 'Y'
							)
						);
					}
				?></div>
			</div><?
		}
	}
?></div><?

$stols = array('current', 'guest', 'common');
$products = array();
foreach($stols as $stol) {
	if(!empty($arOrderData['ORDER_CART'][$stol])) {
		foreach($arOrderData['ORDER_CART'][$stol] as $key => $person) {
			if(!empty($person['ITEMS'])) {
				foreach($person['ITEMS'] as $item) {
					if(($item['PROPS']['type'] == 'product') || (empty($item['PROPS']))) {
						if(!empty($products[$item['PRODUCT_ID'].'_'.$item['PRICE']])) {
							$products[$item['PRODUCT_ID'].'_'.$item['PRICE']]['quantity'] += intval($item['QUANTITY']);
						} else {
							$prod_detail = CNiyamaCatalog::GetProductDetail($item['PRODUCT_ID']);
							$products[$item['PRODUCT_ID'].'_'.$item['PRICE']] = array(
								'id' => $item['PRODUCT_ID'],
								'name' => $prod_detail['INFO']['NAME'],
								'category' => $prod_detail['INFO']['SECTION']['NAME'],
								'price' => $item['PRICE'],
								'quantity' => intval($item['QUANTITY']),
							);
						}
					}
				}
			}
		}
	}
}

?><script type="text/javascript">
	ga('require', 'ecommerce', 'ecommerce.js')
	ga('ecommerce:addTransaction', {
		'id': '<?=$arOrderData['ORDER']['ID']?>', // Transaction ID. Required
		'affiliation': 'НИЯАМА', // Affiliation or store name
		'revenue': '<?=$arOrderData['ORDER_CART']['TOTAL_PRICE']?>' // Grand Total
	});
	// addItem should be called for every item in the shopping cart.
	<?
	if(!empty($products)) {
		foreach ($products as $product) {
			?>
			ga('ecommerce:addItem', {
				'id': '<?=$arOrderData['ORDER']['ID']?>', // Transaction ID. Required
				'name': '<?=$product['name']?>', // Product name. Required
				'sku': '<?=$product['id']?>', // SKU/code
				'category': '<?=$product['category']?>', // Category or variation
				'price': '<?=$product['price']?>', // Unit price
				'quantity': '<?=$product['quantity']?>' // Quantity
			});
			<?
		}
	}
	?>
	ga('ecommerce:send');
</script>
<script>
convead('event', 'purchase', {
	order_id: <?=$arOrderData['ORDER']['ID']?>,
	revenue: <?=$arOrderData['ORDER_CART']['TOTAL_PRICE']?>,
	items: [
                <?php
	if(!empty($products)) {
		foreach ($products as $product) {
			?>
			{product_id: '<?=$product['id']?>', qnt: <?=$product['quantity']?>, price: <?=$product['price']?>},
                <?php
        }
    }
    ?>
]},{
    first_name: '<?=$GLOBALS['USER']->GetFirstName();?>',
    last_name: '<?=$GLOBALS['USER']->GetLastName();?>',
    email: '<?=$GLOBALS['USER']->GetEmail();?>',
    phone: ''
  });
</script>


<?
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.codes',
    array(
        'MODE' => 'thanks',
        'products' => $products,
        'orderTotal' => $arOrderData['ORDER_CART']['TOTAL_PRICE'],
        'orderId' => $arOrderData['ORDER']['UF_FO_ORDER_CODE']
    )
);
?>


<?

//CNiyamaCatalog::GetProductDetail

?><div class="ordering-form__social">Присоединяйтесь к нам в социальных сетях<br>
	<div class="social _auth"><?
		// ссылки на соцсети
		$GLOBALS['APPLICATION']->IncludeFile(
			SITE_DIR.'include_areas/footer-social-links.php',
			array(),
			array(
				'MODE' => 'html',
				'SHOW_BORDER' => false
			)
		);
	?></div>
</div><?
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Постраничная навигация для каталога
 */

$iCur = intval($arParams['CURRENT_PAGE']);
$iAllCount = intval($arParams['ITEMS_ALL_CNT']);
$iPerPage = intval($arParams['PER_PAGE']);
$iEnd = ceil($iAllCount / $iPerPage);
$bPageUrlAjaxStyle = $arParams['AJAX_REQUEST_TYPE'] && $arParams['AJAX_REQUEST_TYPE'] == 'Y';
$sAddParams = isset($arParams['~ADD_QUERY_PARAMS']) ? $arParams['~ADD_QUERY_PARAMS'] : '';
parse_str($sAddParams, $arRequestParams);

if($iEnd > 1) {
	$iStart = $iCur - 2;
	$iFinish = $iCur + 2;
	if($iStart < 1) {
		// Это если нужно сдвигать конец
		//$iFinish -= $iStart - 1;
		$iStart = 1;
	}
	if($iFinish > $iEnd) {
		// Это если нужно сдвигать начало
		// $iStart -= $iFinish - $iEnd;
		if($iStart < 1) {
			$iStart = 1;
		}
		$iFinish = $iEnd;
	}

	$sTmpCss = '';
	$sTmpAttr = '';
	if($bPageUrlAjaxStyle) {
		$sTmpCss = ' js-widget';
		$sTmpAttr = ' onclick="return {\'pagination\': {}}"';
	}
	$sUrlAttr_ = '';
	if($bPageUrlAjaxStyle) {
		$sUrlAttr_ .= ' rel="nofollow"';
	}

	?><div class="pagination<?=$sTmpCss?>"<?=$sTmpAttr?>><?
		// Предыдущая, следующая
		?><div class="pagination__arrows"><?
			if($iCur > 1) {
				$sUrlAttr = $sUrlAttr_;
				$arRequestParams['page'] = ($iCur - 1);
				if($bPageUrlAjaxStyle) {
					$sUrlAttr .= ' data-params="'.htmlspecialcharsbx(json_encode($arRequestParams)).'"';
				}
				$sUrl = '?'.http_build_query($arRequestParams);

				?><a href="<?=$sUrl?>"<?=$sUrlAttr?> class="pagination__prev">предыдущая</a><?
			} else {
				/*
				?><span class="pagination__prev _disabled">предыдущая</span><?
				*/
			}

			if($iCur < $iEnd) {
				if($iCur > 1) {
					?><span class="pagination__separator">|</span><?
				}

				$sUrlAttr = $sUrlAttr_;
				$arRequestParams['page'] = ($iCur + 1);
				if($bPageUrlAjaxStyle) {
					$sUrlAttr .= ' data-params="'.htmlspecialcharsbx(json_encode($arRequestParams)).'"';
				}
				$sUrl = '?'.http_build_query($arRequestParams);

				?><a href="<?=$sUrl?>"<?=$sUrlAttr?> class="pagination__next">следующая</a><?
			} else {
				/*
				?><span class="pagination__next _disabled">следующая</span><?
				*/
			}
		?></div><?

		// Странички
		?><div class="pagination__items"><?
			if($iStart > 1) {
				$sUrlAttr = $sUrlAttr_;
				$arRequestParams['page'] = 1;
				if($bPageUrlAjaxStyle) {
					$sUrlAttr .= ' data-params="'.htmlspecialcharsbx(json_encode($arRequestParams)).'"';
				}
				$sUrl = '?'.http_build_query($arRequestParams);

				?><a href="<?=$sUrl?>"<?=$sUrlAttr?> class="pagination__item">1</a><?

				if($iStart > 2) {
					?><span class="pagination__dots">...</span><?
				}
			}
			for($i = $iStart; $i <= $iFinish; $i++) {
				$sCurrent = $i == $iCur ? ' _active' : '';

				$sUrlAttr = $sUrlAttr_;
				$arRequestParams['page'] = $i;
				if($bPageUrlAjaxStyle) {
					$sUrlAttr .= ' data-params="'.htmlspecialcharsbx(json_encode($arRequestParams)).'"';
				}
				$sUrl = '?'.http_build_query($arRequestParams);

				?><a href="<?=$sUrl?>"<?=$sUrlAttr?> class="pagination__item<?=$sCurrent?>"><?=$i?></a><?
			}
			if($iFinish < $iEnd) {
				if($iFinish < $iEnd - 1) {
					?><span class="pagination__dots">...</span><?
				}

				$sUrlAttr = $sUrlAttr_;
				$arRequestParams['page'] = $iEnd;
				if($bPageUrlAjaxStyle) {
					$sUrlAttr .= ' data-params="'.htmlspecialcharsbx(json_encode($arRequestParams)).'"';
				}
				$sUrl = '?'.http_build_query($arRequestParams);

				?><a href="<?=$sUrl?>"<?=$sUrlAttr?> class="pagination__item"><?=$iEnd?></a><?
			}
		?></div><?
	?></div><?

	//
	// необходимо на всех страницах, где присутствует постраничная навигация, после основного title страницы добавлять слова по следующему принципу: страница [номер текущей страницы]
	//
	$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_PAGENAV_CUR_PAGE', $iCur);
	?><script type="text/javascript">
		jQuery(document).ready(
			function() {
				try {
					window.PagenavChangeTitle(<?=$iCur?>);
				} catch(e) {
					//
				}
			}
		);
	</script><?
}

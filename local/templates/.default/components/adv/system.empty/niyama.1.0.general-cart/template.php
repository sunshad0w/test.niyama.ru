<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

//
// Общая функция вывода элемента корзины
//
if(!function_exists('PrintNiyamaCartItem')) {
	function PrintNiyamaCartItem($arBasketItem) {
		global $iCountProdsCurStol;
		global $arProductsCurUser;
		if (isset($arBasketItem['PROPS']['type'])) {
			switch ($arBasketItem['PROPS']['type']) {

				# Купон
				case CNiyamaCart::$arCartItemType[1]:
					PrintNiyamaCuponsCartItem($arBasketItem);
				break;

				# Доп компонент
				case CNiyamaCart::$arCartItemType[2]:
					//echo "1";
				break;

				# Блюдо
				default:
					$iCountProdsCurStol++;
					$arProductsCurUser[] = $arBasketItem['PRODUCT_ID'];
					PrintNiyamaProductCartItem($arBasketItem);
				break;
			}
		}
	}
}

if(!function_exists('NiyamaCartSort')) {
	function NiyamaCartSort(&$prods) {
		$arCoupon = array();
		foreach($prods as $key => $prod) {
			if (isset($prod['PROPS']['type'])) {
				switch ($prod['PROPS']['type']) {

					# Купон
					case CNiyamaCart::$arCartItemType[1]:
						$arCoupon[] = $prod;
						unset($prods[$key]);
					break;

					# Блюдо
					default:
						//
					break;
				}
			}
		}
		/*
		if (!empty($arCoupon)) {
			$prods=array_merge($prods, $arCoupon);
		}
		*/
		return $arCoupon;
	}
}

//
// Функция вывода Блюд в корзине
//
if(!function_exists('PrintNiyamaProductCartItem')) {
	function PrintNiyamaProductCartItem($arBasketItem) {

		if(!empty($arBasketItem['PROPS']['SHOW_IN_CART'])) {
			if($arBasketItem['PROPS']['SHOW_IN_CART'] != 'Y') {
				return false;
			}
		}

		$by_coupon = $arBasketItem['PROPS']['BY_COUPON'] == 'Y';

		static $iMaxQuantity = 0;
		if(!$iMaxQuantity) {
			# Получаем максимальное кол-во блюд в заказе
			$iMaxQuantity = intval(CNiyamaCustomSettings::GetStringValue('order_dish_max_quantity', 12));
		}
		# карточка товара (выбираем также из числа деактивированных)
		$arProduct = CNiyamaCatalog::GetProductDetail($arBasketItem['PRODUCT_ID'], true);
		$bDisabled = $arProduct['INFO']['ACTIVE'] != 'Y';

		if($arProduct['INFO']) {
			$arImg = CFile::GetFileArray($arProduct['INFO']['PREVIEW_PICTURE']);

			# Получаем Похожие товары
			if(!$by_coupon) {
				$arGetRelatedProducts = CNiyamaCatalog::GetRelatedProductsById($arBasketItem['PRODUCT_ID']);
			} else {
				$arGetRelatedProducts = array();
				if ((!empty($arBasketItem['PROPS']['ID_COUPON_BLUDO'])) && ($arBasketItem['PROPS']['ID_COUPON_BLUDO'] > 0)) {
					$arProduct['INFO']['ID'] = $arBasketItem['PROPS']['ID_COUPON_BLUDO'];
				}
			}

			?>

<div data-id="<?=$arBasketItem['ID']?>" onclick="return {'goods': {}}" class="goods _table <?=($by_coupon) ? '_by-coupon' : 'js-widget'?>">
				<div class="goods__wrap"><?
					$sTmpClass = $bDisabled ? ' _disabled' : '';
					?><div class="goods__item<?=$sTmpClass?>" data-id="<?=$arProduct['INFO']['ID']?>">
						<div class="goods__image"><img width="188" src="<?=$arImg['SRC']?>" alt=""></div>
						<div class="goods__desc"><?
							/*
							if(!empty($arProduct['EXCLUDE_INGREDIENT'])) {
								$ex_ings = array();
								foreach($arProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
									if (!in_array($elem, $arProduct['ALL_INGREDIENT'])) {
										$ex_ings[$key] = $elem;
									}
								}
								if(!empty($ex_ings)) {
									$arProduct['ALL_INGREDIENT'] = array_merge($arProduct['ALL_INGREDIENT'], $ex_ings);
								}
							}
							*/
							?><div class="goods__composition"><p class="goods__name"><?=$arProduct['INFO']['NAME']?></p><?=implode(', ', $arProduct['ALL_INGREDIENT'])?></div><?
							if(!$bDisabled) {
								?><div class="goods__cost"><?=round($arBasketItem['PRICE'])?> <span class="rouble">a </span></div><?
							}
						?></div>
					</div><?

					if (!empty($arGetRelatedProducts)) {
						foreach ($arGetRelatedProducts as $arRelProduct) {
							$arImg = CFile::GetFileArray($arRelProduct['INFO']['PREVIEW_PICTURE']);
							?><div class="goods__item" data-id="<?= $arRelProduct['INFO']['ID'] ?>">
								<div href="/ajax/getGoodsCard.php?CODE=<?=$arRelProduct['INFO']['CODE']?>" data-fancybox-type="ajax" class="goods__image fancybox"><img width="188" src="<?=$arImg['SRC']?>" alt=""></div>
								<div class="goods__desc"><?
									/*
									if(!empty($arRelProduct['EXCLUDE_INGREDIENT'])) {
										$ex_ings = array();
										foreach($arRelProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
											if (!in_array($elem, $arRelProduct['ALL_INGREDIENT'])) {
												$ex_ings[$key] = $elem;
											}
										}
										if (!empty($ex_ings)) {
											$arRelProduct['ALL_INGREDIENT'] = array_merge($arRelProduct['ALL_INGREDIENT'], $ex_ings);
										}
									}
									*/
									?><div class="goods__composition"><p class="goods__name"><?=$arRelProduct['INFO']['NAME']?></p><?=implode(', ', $arRelProduct['ALL_INGREDIENT'])?></div>
									<div class="goods__cost"><?=$arRelProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?> <span class="rouble">a </span></div>
									<div data-id="<?=$arRelProduct['INFO']['ID']?>" class="goods__add-btn btn">Заменить</div>
								</div>
							</div><?
						}
					}
				?></div><?
				if($bDisabled) {
					?><div class="goods__not-in-menu">Отсутствует в меню</div><?
				}
				$arBasketItem['QUANTITY'] = intval($arBasketItem['QUANTITY']);
				if(!$by_coupon) {
					?><div onclick="return {'select': {}}" class="select__wrap _mini js-widget">
						<select class="select" data-id="<?=$arBasketItem['ID']?>"><?
							for($i = 1; $i <= $iMaxQuantity; $i++) {
								?><option value="<?=$i?>"<?=($arBasketItem['QUANTITY'] == $i) ? ' selected="selected"' : ''?>><?=$i?></option><?
							}
							if($arBasketItem['QUANTITY'] > $iMaxQuantity) {
								?><option value="<?=$arBasketItem['QUANTITY']?>" selected="selected"><?=$arBasketItem['QUANTITY']?></option><?
							}
						?></select>
					</div><?
				}
				if(!empty($arGetRelatedProducts)) {
					?><div class="goods__change">
						<div class="goods__change-text">Похожие блюда</div>
						<div class="goods__change-next"></div>
						<div class="goods__change-prev"></div>
					</div><?
				}
				?><a class="goods__remove" href="#" data-id="<?=$arBasketItem['ID']?>"> </a><?
			?></div><?
		}
	}
}

//
// Функция вывода купонов в корзине
//
if(!function_exists('PrintNiyamaCuponsCartItem')) {
	function PrintNiyamaCuponsCartItem($arBasketItem) {
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.coupon',
			array(
				'CUPONS_IDS' => $arBasketItem['PRODUCT_ID'],
				'SHOW_USED' => 'N',
				'SHOW_IN_CART' => 'Y',
				'ID_IN_CART' => $arBasketItem['ID'],
				'VAL_COUPON' => $arBasketItem['PROPS']['val_coupon'],
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	}
}

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';
$bLightResult = isset($arParams['LIGHT_RESULT']) && $arParams['LIGHT_RESULT'] == 'Y';
# Если не установлен флаг вывода все для всех
$bGetAllCart = isset($_REQUEST['all']) ? true : false;
# Получаем текущую корзину пользователя
$arCartItems = CNiyamaCart::GetCartList();

// Сохраняем скисок айдишников товаров в корзине (для использования в методе поиска похожих товаров)
$GLOBALS['CART_PRODUCT_IDS'] = $arCartItems['PRODUCT_IDS'];


# Выбираем купоны для всех столов
$ar_coupons_each_guest = array();

if($arCartItems['common'][0]['ITEMS']) {
	foreach($arCartItems['common'][0]['ITEMS'] as $key => $prod) {
		if ($prod['PROPS']['type'] == 'cupon') {
			if (CNiyamaCoupons::GetTypeForAll($prod['PRODUCT_ID'])) {
				$ar_coupons_each_guest[$key] = $prod;
				unset($arCartItems['common'][0]['ITEMS'][$key]);
			}
		}
	}
}

// купоны для уровней скидок текущего заказа
/*
$arCalc = CNiyamaIBlockCartLine::calcLine();
foreach ($arCalc as $el) {
	$el['TYPE_IMG']='DOWNLOAD';
	$el['IMG1']=$el['PROPERTY_CUPONBG_HORIZONTAL_VALUE'];
	$el['IMG2']=$el['PROPERTY_CUPONBG_VERTICAL_VALUE'];
	$el['USED']='Y';
	$el['ACTIVE']='Y';
	$el['NO_DEL']='Y';
	$ar_coupons_each_guest[]=$el;
}
*/

//
// Получаем список гостей пользователя
//
$arGuests = CNiyamaCart::GetCurUserGuestsList();
$arPrintTablesList = $_SESSION['NIYAMA']['GUESTS'] && is_array($_SESSION['NIYAMA']['GUESTS']) ? $_SESSION['NIYAMA']['GUESTS'] : array();

// объединяем список сессии со списком из корзины
if($arCartItems['guest'] && is_array($arCartItems['guest'])) {
	foreach($arCartItems['guest'] as $key => $elem) {
		$arPrintTablesList[$key] = $key;
	}
}
// добавляем в этот список "общий стол", если для него в корзине уже есть товары
if($arCartItems['common'][0]['ITEMS']) {
	$arPrintTablesList[CNiyamaCart::$iForAllGuestID] = CNiyamaCart::$iForAllGuestID;
}

// Если запрос добавления/смены стола 
$mGuestID = false;
if(isset($_REQUEST['gid'])) {
	$mGuestID = htmlspecialcharsbx($_REQUEST['gid']);
}
if($mGuestID) {
	/*
	// Проверка на кол-во гостей
	if(!isset($arCartItems['guest'][$mGuestID])) {
		$iGuestCount = (count($_SESSION['NIYAMA']['GUESTS']));
		//$iOptionsMaxGuest = intval(CNiyamaCustomSettings::getStringValue('max_num_guests', '8') - 1);
		$iOptionsMaxGuest = intval(CNiyamaCustomSettings::getStringValue('max_num_guests', '8'));
		if($iGuestCount > $iOptionsMaxGuest) {
			$arJson = array(
				'ERROR' => 'Количество гостей не должно превышать '.intval(CNiyamaCustomSettings::GetStringValue('max_num_guests', '8'))
			);
			echo json_encode($arJson);
			die();
		}
	}
	*/
	// сохраним в сессии новый стол (кроме своего, т.к. свой стол есть всегда)
	if($mGuestID != 'current') {
		$arPrintTablesList[$mGuestID] = $mGuestID;
	}
}

// сверим получившийся список со списком гостей юзера в базе, отсутствующие столы убираем из списка для вывода
if($arPrintTablesList) {
	foreach($arPrintTablesList as $mTmpGuestId) {
		// если стол "для всех", то оставляем его, остальные сверяем с базой
		if($mTmpGuestId != CNiyamaCart::$iForAllGuestID) {
			if(!$arGuests[$mTmpGuestId]) {
				unset($arPrintTablesList[$mTmpGuestId]);
			}
		}
	}
}

$_SESSION['NIYAMA']['GUESTS'] = $arPrintTablesList;

// добавим в массив корзины отсутствующие столы (это еще пустые столы)
if($arPrintTablesList) {
	foreach($arPrintTablesList as $id) {
		if(is_numeric($id) && $id != CNiyamaCart::$iForAllGuestID && !isset($arCartItems['guest'][$id])) {
			$arCartItems['guest'][$id] = array();
		}
	}
}

if(!$mGuestID) {
	$mGuestIDTmp = CNiyamaCart::getGuestID();
	$mGuestID = ($mGuestIDTmp) ? $mGuestIDTmp : false;
} else {
	$mGuestIDTmp = CNiyamaCart::getGuestID();
	if((!$mGuestIDTmp) || ($mGuestID != $mGuestIDTmp)) {
		$mGuestID = ($mGuestID != 'current') ? $mGuestID : false;
		CNiyamaCart::setGuestID($mGuestID);
	}
}

$bIsCurrent = isset($arCartItems['current']) && !empty($arCartItems['current']) ? true : false;
$bIsGuest = isset($arCartItems['guest']) && !empty($arCartItems['guest']) ? true : false;
$iTotalPrice = isset($arCartItems['TOTAL_PRICE']) ? $arCartItems['TOTAL_PRICE'] : '0';

# Проверяем на наличие текущего гостя в списке гостей
if($mGuestID) {
	# Если у него нет товаров и нет в списке добавленых гостей - удаляем/ставим текущего
	if(!isset($arCartItems['guest'][$mGuestID]) && !isset($arGuests[$mGuestID])) {
		# Если это не владка "На всех"
		if(($mGuestID != CNiyamaCart::$iForAllGuestID)) {
			$mGuestID = false;
			CNiyamaCart::setGuestID($mGuestID);
		}
	}
}

# Список Истории заказов
$APPLICATION->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.general-cart-history',
	array(),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

# Далее выводим список добавленных ранее гостей
if(!$bLightResult) {
	$isSeeCurrent = !$mGuestID ? true : false;
	$isSeeGuest = $mGuestID ? true : false;

	?><div onclick="return{'guests': {}}" class="guests pull-left js-widget">
		<div class="guests__list cart-selected-guest-list"><?
			$remCard = true;
			# активен "свой стол"
			if ($isSeeCurrent) {
				$remCard = false;

				$iSubPrice = (isset($arCartItems['current'][0]['PRICE'])) ? $arCartItems['current'][0]['PRICE'] : '0';
				$iProductsCount = ((isset($arCartItems['current'][0]['ITEMS']))) ? count($arCartItems['current'][0]['ITEMS']) : '0';
				$cId = 'current';

				# Получаем аватар
				if ($GLOBALS['USER']->IsAuthorized()) {
					$arAvaData = CFile::ResizeImageGet(
						CUsersData::getPersonalPhoto(),
						array(
							'width' => 44,
							'height' => 44
						),
						BX_RESIZE_IMAGE_EXACT,
						false
					);
					$sSrcAvatar = $arAvaData['src'];
					$sNameTab = CUsersData::getUserName();
				} else {
					# Получаем первый из списка
					$arAvaData = CFile::ResizeImageGet(
						CUsersData::getFirstDefaultAvatar(), 
						array(
							'width' => 44, 
							'height' => 44
						), 
						BX_RESIZE_IMAGE_EXACT, 
						false
					);
					$sSrcAvatar = $arAvaData['src'];
					$sNameTab = CNiyamaCart::$sCurrentTabName;
				}
			} else {
				# активен стол гостя или стол "на всех"
				$cId = $mGuestID;
				# у стола "на всех" свой аватар
				if ($mGuestID == CNiyamaCart::$iForAllGuestID) {
					$iSubPrice = isset($arCartItems['common'][0]['PRICE']) ? $arCartItems['common'][0]['PRICE'] : '0';
					$sSrcAvatar = CNiyamaCart::$sForAllAvatar;
					$sNameTab = CNiyamaCart::$sForAllName;

					$iProductsCount = isset($arCartItems['common'][0]['ITEMS']) ? count($arCartItems['common'][0]['ITEMS']) : '0';
				} else {
					$iSubPrice = $bIsGuest && isset($arCartItems['guest'][$mGuestID]['PRICE']) ? $arCartItems['guest'][$mGuestID]['PRICE'] : '0';
					# Получаем аватар
					$arGidData = CNiyamaCart::getGuestById($mGuestID);
					$arAvaData = CFile::ResizeImageGet($arGidData['PREVIEW_PICTURE'], array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);

					$sSrcAvatar = $arAvaData['src'];
					$sNameTab = $arGidData['NAME'];

					$iProductsCount = isset($arCartItems['guest'][$mGuestID]['ITEMS']) ? count($arCartItems['guest'][$mGuestID]['ITEMS']) : '0';
				}
			}

			?><div class="guests__card _current" data-count="<?=$iProductsCount?>" data-price="<?=$iSubPrice?>" data-gid="<?=$cId?>"><?
				if ($remCard) {
					?><a href="#" class="guests__card-remove"></a><?
				}
				?><div class="guests__card-img"><img src="<?=$sSrcAvatar?>" alt=""></div>
				<div class="guests__card-sum"> <i id="guests__card-sum"><?=$iSubPrice?></i> <span class="rouble">a</span></div>
				<div class="guests__card-name"> <?=$sNameTab?> </div>
			</div><?
			?><a href="#" class="guests__list-choose"></a><?

			//
			// Выпадающий список столов заказа
			//
			?><div class="guests__list-wrap">
				<div class="guests__list-list"><?

					// Если в данный момент выбран не "свой стол", то добавляем его в список - он должен быть всегда
					if (!$isSeeCurrent) {
						$iSubPriceUser = (isset($arCartItems['current'][0]['PRICE'])) ? $arCartItems['current'][0]['PRICE'] : '0';
						// Получаем аватар
						if ($GLOBALS['USER']->IsAuthorized()) {
							$arAvaData = CFile::ResizeImageGet(CUsersData::getPersonalPhoto(), array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);
							$sSrcAvatar = $arAvaData['src'];
							$sNameTab = CUsersData::getUserName();
						} else {
							// Получаем первый из списка
							$arAvaData = CFile::ResizeImageGet(CUsersData::getFirstDefaultAvatar(), array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);
							$sSrcAvatar = $arAvaData['src'];
							$sNameTab = CNiyamaCart::$sCurrentTabName;
						}

						?><a href="#" class="guests__card _big" data-gid="current">
							<div class="guests__card-img"><img src="<?=$sSrcAvatar?>" alt=""></div>
							<div class="guests__card-sum"><?=$iSubPriceUser?> <span class="rouble">a</span></div>
							<div class="guests__card-name"><?=$sNameTab?></div>
						</a><?
					}

					// Если в списке есть стол "на всех" и он не выбран в данный момент
					if(isset($arPrintTablesList[CNiyamaCart::$iForAllGuestID]) && $mGuestID != CNiyamaCart::$iForAllGuestID) {
						$igProce = isset($arCartItems['common'][0]['PRICE']) ? $arCartItems['common'][0]['PRICE'] : '0';

						$iProductsCount = isset($arCartItems['common'][0]['ITEMS']) ? count($arCartItems['common'][0]['ITEMS']) : '0';

						?><a href="#" class="guests__card _big" data-count="<?=$iProductsCount?>" data-price="<?=$igProce?>" data-gid="<?=CNiyamaCart::$iForAllGuestID?>">
							<button class="guests__card-remove"></button>
							<div class="guests__card-img"><img src="<?=CNiyamaCart::$sForAllAvatar?>" alt=""></div>
							<div class="guests__card-sum"><?=$igProce?>  <span class="rouble">a</span></div>
							<div class="guests__card-name"><?=CNiyamaCart::$sForAllName?></div>
						</a><?
					}

					// Столы гостей
					if(!empty($arCartItems['guest'])) {
						foreach($arCartItems['guest'] as $id => $grow) {
							if ($mGuestID != $id) {
								if (isset($arGuests[$id])) {
									$igProce = isset($arCartItems['guest'][$id]['PRICE']) ? $arCartItems['guest'][$id]['PRICE'] : '0';
									$iProductsCount = isset($arCartItems['guest'][$id]['ITEMS']) ? count($arCartItems['guest'][$id]['ITEMS']) : '0';

									# Получаем аватар
									$arGuestAva = CFile::ResizeImageGet($arGuests[$id]['PREVIEW_PICTURE'], array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);

									?><a href="#" class="guests__card _big" data-count="<?=$iProductsCount?>" data-price="<?=$igProce?>" data-gid="<?=$arGuests[$id]['ID']?>">
										<button class="guests__card-remove"></button>
										<div class="guests__card-img"><img src="<?=$arGuestAva['src']?>" alt=""></div>
										<div class="guests__card-sum"><?=$igProce?> <span class="rouble">a</span></div>
										<div class="guests__card-name"><?=$arGuests[$id]['NAME']?></div>
									</a><?
								}
							}
						}
					}
				?></div>
			</div>
		</div><?
		
		//
		// Список всех гостей юзера
		//
		?><div class="guests__add pull-right">
			<a href="#" class="guests__add-sign">
				<span>Добавьте гостя, чтобы разделить заказ</span>
				<span class="_hidden-desktop">Добавьте гостя и разделите заказ</span>
			</a>
			<a href="#" class="guests__add-button"></a>
			<div class="guests__add-wrap">
				<div class="guests__add-list">
					<a href="#" class="guests__card _new _small">
						<div class="guests__card-img"></div>
						<div class="guests__card-name">
							Новый<br>гость
						</div>
					</a><?

					$sClassCommonDisabled = $arPrintTablesList[CNiyamaCart::$iForAllGuestID] ? ' _disabled' : '';
					# Выводим стол "На всех"
					?><a href="#" class="guests__card _all _small<?= $sClassCommonDisabled ?>" data-gid="<?=CNiyamaCart::$iForAllGuestID?>">
						<div class="guests__card-img"><img src="<?=CNiyamaCart::$sForAllAvatar?>"></div>
						<div class="guests__card-name"><?=CNiyamaCart::$sForAllName?> </div>
					</a><?

					if (!empty($arGuests)) {
						foreach ($arGuests as $gItem) {
							$sClassGuestDisabled = isset($arCartItems['guest'][$gItem['ID']]) ? ' _disabled' : '';
							# Получаем аватар
							$arAvaData = CFile::ResizeImageGet($gItem['PREVIEW_PICTURE'], array('width' => 44, 'height' => 44), BX_RESIZE_IMAGE_EXACT, false);
							?><a href="#" class="guests__card _all _small<?=$sClassGuestDisabled?>" data-gid="<?=$gItem['ID']?>">
								<div class="guests__card-img"><img src="<?=$arAvaData['src']?>"></div>
								<div class="guests__card-name"><?=$gItem['NAME']?></div>
							</a><?
						}
					}
				?></div>
			</div>
		</div>
	</div><?

	?><div class="cart-buy pull-right"><?
		?><span class="cart-buy__sum"><i><?=$iTotalPrice?></i> <span class="rouble">a</span></span><?
		 # Проверка корзины на минимальную стоимость
		 $iOptionsMinOrder = intval(CNiyamaCustomSettings::getStringValue('min_sum_by_order', '0'));
		 $iOptionsMinSelfDeliveryOrder = intval(CNiyamaCustomSettings::getStringValue('min_self_delivery_sum_by_order', '0'));
		 $sDisabled = ' _disabled';
		 $sDisabledHref = 'javascript:void(0);';
		 if($iTotalPrice > 0 && $iTotalPrice >= $iOptionsMinOrder) {
			 $sDisabled = '';
			 $sDisabledHref = '/order/';
		 }
		?><a href="<?=$sDisabledHref?>" class="cart-buy__button btn _style_1 pull-right<?=$sDisabled?>">Заказать</a><?
		if($iOptionsMinOrder || $iOptionsMinSelfDeliveryOrder) {
			?><div class="cart-buy__min-sum"><?
				if($iOptionsMinOrder) {
					echo 'Минимальная сумма заказа - '.$iOptionsMinOrder.'<span class="rouble">a</span>';
				}
				if($iOptionsMinSelfDeliveryOrder) {
					echo '<br />Самовывоз от '.$iOptionsMinSelfDeliveryOrder.'<span class="rouble">a</span> при заказе по телефону';
				}
			?></div><?
		}
	?></div>
	<div class="clearfix"></div><?
}

//
// Карточки товаров в корзине
//
if(!$bLightResult) {
	$sClassMod = $iTotalPrice > 0 || $arCartItems ? ' _min-height _filled' : ' _empty';
	
	$sTitle = ($iTotalPrice > 0) ? '' : ' title="Чтобы сделать заказ, перетащите сюда блюдо или кликните по кнопке «В корзину»"';
	echo '<div class="cart__table'.$sClassMod.'"'.$sTitle.'>';
}
$arCoupon = array();
global $iCountProdsCurStol;
$iCountProdsCurStol = 0;
global $arProductsCurUser;
$arProductsCurUser = array();
//ob_start(); // буферизируем для вывода купонов в начале раньше товаров
# Если мы не хотим получать все для всех
if(!$bGetAllCart) {

	# Если смотрим текущего пользователя
	if(!$mGuestID) {
		# Если у него есть товары
		if($bIsCurrent) {
			if($arCartItems['current'][0]['ITEMS']) {
				$arCoupon = NiyamaCartSort($arCartItems['current'][0]['ITEMS']);
				foreach($arCartItems['current'][0]['ITEMS'] as $prod) {
					PrintNiyamaCartItem($prod);
				}
			}
		}
	} else {
		if ($mGuestID == CNiyamaCart::$iForAllGuestID) {
			# Если общая вкладка
			if(!empty($arCartItems['common'])) {
				if($arCartItems['common'][0]['ITEMS']) {
					$arCoupon = NiyamaCartSort($arCartItems['common'][0]['ITEMS']);
					foreach($arCartItems['common'][0]['ITEMS'] as $prod) {
						PrintNiyamaCartItem($prod);
					}
				}
			}
		} else {
			if($bIsGuest) {
				$bIsNeedleGuest = (isset($arCartItems['guest'][$mGuestID])) ? true : false;
				# Если есть тот кто нам нужен
				if($bIsNeedleGuest) {
					if($arCartItems['guest'][$mGuestID]['ITEMS']) {
						$arCoupon = NiyamaCartSort($arCartItems['guest'][$mGuestID]['ITEMS']);
						foreach($arCartItems['guest'][$mGuestID]['ITEMS'] as $prod) {
							PrintNiyamaCartItem($prod);
						}
					}
				}
			}
		}
	}
} else {
	# Если установили флаг all
	$arReturnList = array();
	if($bIsCurrent) {
		if($arCartItems['current'][0]['ITEMS']) {
			foreach($arCartItems['current'][0]['ITEMS'] as $prod) {
				$arReturnList[] = $prod;
			}
		}
	}
	if($bIsGuest) {
		if($arCartItems['guest']) {
			foreach($arCartItems['guest'] as $arGuest) {
				if($arGuest['ITEMS']) {
					foreach($arGuest['ITEMS'] as $prod) {
						$arReturnList[] = $prod;
					}
				}
			}
		}
	}

	# Выводим
	if($arReturnList) {
		$arCoupon = NiyamaCartSort($arReturnList);
		foreach($arReturnList as $prod) {
			PrintNiyamaCartItem($prod);
		}
	}
}


#
# Выводим апсейл
#

if ($iCountProdsCurStol >= 3) {
	$arApSailProduct = CNiyamaCart::getApSailProduct($arProductsCurUser);
	if (!empty($arApSailProduct)) {
		if ($arApSailProduct['INFO']) {

			$arImg = CFile::GetFileArray($arApSailProduct['INFO']['PREVIEW_PICTURE']);
			/*
			if(!empty($arApSailProduct['EX_ING']['EXCLUDE_INGREDIENT'])) {
				$ex_ings = array();
				foreach($arApSailProduct['EX_ING']['EXCLUDE_INGREDIENT'] as $key => $elem) {
					if (!in_array($elem, $arApSailProduct['ING']['ALL_INGREDIENT'])) {
						$ex_ings[$key] = $elem;
					}
				}
				if (!empty($ex_ings)) {
					$arApSailProduct['ING']['ALL_INGREDIENT'] = array_merge($arApSailProduct['ING']['ALL_INGREDIENT'], $ex_ings);
				}
			}
			*/
			$sIngValue = '';
			if (isset($arApSailProduct['ING']['ALL_INGREDIENT']) && !empty($arApSailProduct['ING']['ALL_INGREDIENT'])) {
				$sIngValue = implode(', ', $arApSailProduct['ING']['ALL_INGREDIENT']);
			}

			?><div onclick="return {'goods': {}}" class="goods _table _auction js-widget">
				<div class="goods__wrap">
					<div class="goods__item">
						<div href="/ajax/getGoodsCard.php?CODE=<?=$arApSailProduct['INFO']['CODE']?>" data-fancybox-type="ajax" class="goods__image fancybox"><img src="<?=$arImg['SRC']?>" alt=""></div>
						<div class="goods__desc">
							<a href="<?=$arApSailProduct['INFO']['DETAIL_PAGE_URL']?>">
								<p class="goods__name"><?=$arApSailProduct['INFO']['NAME']?></p>
							</a>
							<div class="goods__composition"><?=$sIngValue?></div>
							<div class="goods__cost"><?=$arApSailProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?> <span class="rouble">a </span></div>
							<div data-id="<?=$arApSailProduct['INFO']['ID']?>" class="goods__add-btn btn" onclick="yaCounter24799106.reachGoal('basket_click' ); ga('send','pageview','/basket_click'); convead('event', 'update_cart', {items: [{product_id: <?=$arApSailProduct['INFO']['ID']?>, qnt: '1', price: <?=$arApSailProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>},]});return true;">В корзину</div>
						</div>
					</div>
				</div>
			</div><?
		}
	}
}

#
# Выводим аукционные блюда
#
/*
$arAuctionProducts = CNiyamaCatalog::getAuctionProducts();
if (!empty($arAuctionProducts) && ($iTotalPrice > 0)) {

	if ($arAuctionProducts['PRODUCT_DETAIL']['INFO']) {

		$arImg = CFile::GetFileArray($arAuctionProducts['PRODUCT_DETAIL']['INFO']['PREVIEW_PICTURE']);

		if (isset($arAuctionProducts['PRODUCT_DETAIL']['ALL_INGREDIENT']) && !empty($arAuctionProducts['PRODUCT_DETAIL']['ALL_INGREDIENT'])) {
			$sIngValue = implode(", ",$arAuctionProducts['PRODUCT_DETAIL']['ALL_INGREDIENT']);
		} else {
			$sIngValue = "";
		}

		?><div onclick="return {'goods': {}}" class="goods _table _auction js-widget">
			<div class="goods__wrap">
				<div class="goods__item">
					<div href="/ajax/getGoodsCard.php?CODE=<?= $arAuctionProducts['PRODUCT_DETAIL']['INFO']['CODE'] ?>" data-fancybox-type="ajax" class="goods__image fancybox"><img src="<?= $arImg['SRC'] ?>"></div>
					<div class="goods__desc">
						<a href="<?= $arAuctionProducts['PRODUCT_DETAIL']['INFO']['DETAIL_PAGE_URL'] ?>">
							<p class="goods__name"><?= $arAuctionProducts['PRODUCT_DETAIL']['INFO']['NAME'] ?></p>
						</a>
						<div class="goods__composition"><?= $sIngValue ?></div>
						<div class="goods__cost"><?= $arAuctionProducts['PRODUCT_DETAIL']['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'] ?> <span class="rouble">a </span></div>
						<div data-id="<?= $arAuctionProducts['PRODUCT_DETAIL']['INFO']['ID'] ?>" class="goods__add-btn btn">В корзину</div>
					</div>
				</div>
			</div>
		</div><?
	}
}
*/
//$products = ob_get_contents();
//ob_end_clean();

if ((!empty($ar_coupons_each_guest)) || (!empty($arCoupon))) {
	//ob_start(); 

	if (!empty($arCoupon)) {
		foreach($arCoupon as $prod) {
			PrintNiyamaCartItem($prod);
		}
	}

	if (!empty($ar_coupons_each_guest)) {
		foreach ($ar_coupons_each_guest as $arCoupon) {
			$GLOBALS['APPLICATION']->IncludeComponent(
				'adv:system.empty',
				'niyama.1.0.coupon',
				array(
					'CUPONS_IDS' => (!empty($arCoupon['PRODUCT_ID'])) ? $arCoupon['PRODUCT_ID'] : '',
					'SHOW_USED' => 'N',
					'SHOW_IN_CART' => 'Y',
					'ID_IN_CART' => (!empty($arCoupon['ID'])) ? $arCoupon['ID'] : '',
					'RES' => $arCoupon,
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}
	}
	//$coupons_cart = trim(ob_get_contents());
	//ob_end_clean();

	/*
	if (!empty($coupons_cart)) {
		echo '<div class="cart__table-coupons">';
		echo $coupons_cart;
		echo '</div>';
	}
	*/
}

if(!$bLightResult) {
	echo '</div>';
}

// таймлайн скидок текущего заказа
$APPLICATION->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.general-cart-timeline',
	array(),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);
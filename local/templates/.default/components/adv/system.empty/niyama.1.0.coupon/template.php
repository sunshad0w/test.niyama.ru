<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$arResult = array();
if (!empty($arParams['CUPONS_IDS'])) {
	$arResult = CNiyamaCoupons::GetCouponById($arParams['CUPONS_IDS']);
} else {
	$arResult = $arParams['RES'];
}

if (empty($arResult)) {
	return false;
}

$show_used = $arParams['SHOW_USED'] == 'Y';
$show_in_cart = $arParams['SHOW_IN_CART'] == 'Y';
$no_del = $arResult['NO_DEL'] == 'Y';
$arResult['ID_COUPON'] = $arResult['ID'];
if($show_in_cart && !empty($arParams['ID_IN_CART'])) {
	$arResult['ID'] = $arParams['ID_IN_CART'];
	$arResult['val_coupon_used'] = CNiyamaCart::GetValCouponUsed($arResult['ID']);
}

if($show_in_cart && $arResult['BONUS_TYPE'] == 'BLUDO') {
	if($arResult['VISUAL'] != 'COMPLECT') {
		return false;
	}
}

if($arParams['IN_CART'] == 'Y') {
	$show_used = true;
	$arResult['ACTIVE'] = 'N';
	$arResult['USED'] = 'Y';
}

$sBlockCss = '';
if($show_in_cart) {
	$sBlockCss .= ' _table';
}
if($show_used && $arResult['USED'] == 'Y') {
	$sBlockCss .= ' _used';
}
if($arResult['ACTIVE'] != 'Y') {
	$sBlockCss .= ' _disabled';
}
$sCouponRemoveHtml = $show_in_cart && $no_del === false ? '<div class="coupon__remove" data-id="'.$arResult['ID'].'"></div>' : '';
$sToCartButtonHtml = !$show_in_cart ? '<div class="coupon__add">В КОРЗИНУ</div>' : '';
			
switch ($arResult['TYPE_IMG']) {
	case 'DEFAULT':
		switch ($arResult['BONUS_TYPE']) {
			case 'DISCOUNT_MONEY':
				if (isset($arParams['VAL_COUPON'])) {
					$arResult['MONEY'] = $arParams['VAL_COUPON'];
				}
				if (isset($arParams['VAL_COUPON_USED'])) {
					$arResult['MONEY'] = $arResult['MONEY'] - $arParams['VAL_COUPON_USED'];
					if ($arResult['MONEY'] > 0) {
						$arResult['ACTIVE'] = 'Y';
					}
				}
				if ($show_in_cart) {
					$arResult['MONEY'] = $arResult['val_coupon_used'];
				}
				$badge = '';
				switch ($arResult['MONEY_TYPE']) {
					case 'MONEY':
						$title_right = CProjectUtils::PrintCardinalNumberRus($arResult['MONEY'], 'рублей', 'рубль', 'рубля');
						$badge = ' _gift';
					break;

					case 'NIYAM':
						$title_right = CProjectUtils::PrintCardinalNumberRus($arResult['MONEY'], 'НИЯМ', 'НИЯМА', 'НИЯМЫ');
						$badge = ' _nyam';
					break;
				}

				?><div class="coupon fancybox _style_1 ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>" data-fancybox-href="/ajax/getCouponPopup.php?pid=<?=$arResult['ID_COUPON']?>" data-fancybox-type="ajax"><?
					echo $sCouponRemoveHtml;
					echo $sToCartButtonHtml;
					?><div class="coupon__title">
						<div class="coupon__title-left"><?=number_format($arResult['MONEY'], 0, '.', ' ')?></div>
						<div class="coupon__title-right"><?=$title_right?></div>
						<div class="coupon__title-center">
							<div class="coupon__badge<?=$badge?>"></div>
						</div>
					</div>
					<p class="coupon__desc"><?=$arResult['TEXT']?></p>
				</div><?
			break;

			case 'CONST_DISCOUNT':
				$max_discount = intval(CNiyamaCustomSettings::GetStringValue('max_discount_by_order', 15));
				if ($arResult['DISCOUNT_PER'] > $max_discount) {
					$arResult['DISCOUNT_PER'] = $max_discount;
				}
				?><div class="coupon _style_4 ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>"><?
					if(!$show_in_cart) {
						echo $sCouponRemoveHtml;
					} elseif($sCouponRemoveHtml) {
						// если купон отображается в корзине и заказ пустой, то дадим возможность его удалить, иначе блокируем возможность удаления постоянной скидки
						$arTmpOrderPrice = CNiyamaCart::GetTotalPrice();
						if(!$arTmpOrderPrice['TOTAL_PRICE']) {
							echo $sCouponRemoveHtml;
						}
					}
					echo $sToCartButtonHtml;
					?><div class="coupon__title">
						<div class="coupon__title-left">
							<div class="coupon__title-left-top">постоянная </div>
							<div class="coupon__title-left-bottom">скидка</div>
						</div>
						<div class="coupon__title-right"><img src="<?=$templateFolder.'/img/coupon_card.png'?>" alt=""></div>
						<div class="coupon__title-center">
							<div class="coupon__badge _percent"> <span><?=(!empty($arResult['val_coupon_used'])) ? $arResult['val_coupon_used'] : $arResult['DISCOUNT_PER']?><sup>%</sup></span></div>
						</div>
					</div>
				</div><?
			break;

			case 'DISCOUNT_PERSENT':
				?><div class="coupon _style_1 ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>"><?
					echo $sCouponRemoveHtml;
					echo $sToCartButtonHtml;
					?><div class="coupon__title"> 
						<div class="coupon__title-left">разовая</div>
						<div class="coupon__title-right">скидка</div>
						<div class="coupon__title-center">
							<div class="coupon__badge _percent"><span><?=$arResult['DISCOUNT_PER']?><sup>%</sup></span></div>
						</div>
					</div>
					<p class="coupon__desc"><?=$arResult['TEXT']?></p>
				</div><?
			break;

			case 'BLUDO':
				$style = false;
				$img = '';
				switch (sizeof($arResult['PICS_BLUDS'])) {
					case 1:
						$style = 3;
						$file1 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][0], array('width' => 96, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL); 
						$img = '<img class="coupon__pic-img" src="'.$file1['src'].'" alt="">';
					break;

					case 2:
						$style = 2;
						$file1 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][0], array('width' => 96, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL);
						$file2 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][1], array('width' => 96, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL);
						$img = '<img class="coupon__pic-img pull-left" src="'.$file1['src'].'" alt=""><img class="coupon__pic-img pull-right" src="'.$file2['src'].'" alt="">';
					break;
				}
				if ($style !== false) {
					?><div class="coupon _style_<?=$style?> ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>"><?
						echo $sCouponRemoveHtml;
						echo $sToCartButtonHtml;
						?><div class="coupon__pic">
							<?=$img?>
							<div class="coupon__pic-price">
								<span class="coupon__pic-price-actual">
									<?=$arResult['SUM_NEW_PRICE']?><span class="rouble">a</span>
								</span>
								<span class="coupon__pic-price-old">
									<?=$arResult['SUM_OLD_PRICE']?><span class="rouble">a</span>
								</span>
							</div>
						</div>
						<div class="coupon__info">
						  <div class="coupon__title"><?=$arResult['NAME']?></div>
						  <p class="coupon__desc"><?=$arResult['TEXT']?></p>
						</div>
					</div><?
				}
			break;
		}
	break;

	case 'BLUDA':
		switch ($arResult['BONUS_TYPE']) {
			case 'BLUDO':
				$style = false;
				switch (sizeof($arResult['PICS_BLUDS'])) {
					case 1:
						$style = 3;
						$file1 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][0], array('width' => 140, 'height' => 120), BX_RESIZE_IMAGE_PROPORTIONAL); 
						$img = '<img class="coupon__pic-img" src="'.$file1['src'].'" alt="">';
					break;

					case 2:
						$style = 2;
						$file1 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][0], array('width' => 96, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL);
						$file2 = CFile::ResizeImageGet($arResult['PICS_BLUDS'][1], array('width' => 96, 'height' => 70), BX_RESIZE_IMAGE_PROPORTIONAL);
						$img = '<img class="coupon__pic-img pull-left" src="'.$file1['src'].'" alt=""><img class="coupon__pic-img pull-right" src="'.$file2['src'].'" alt="">';
					break;
				}
				if ($style !== false) {
					?><div class="coupon _style_<?=$style?> ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>"><?
						echo $sCouponRemoveHtml;
						echo $sToCartButtonHtml;
						?><div class="coupon__pic">
							<?=$img?>
							<div class="coupon__pic-price">
								<span class="coupon__pic-price-actual">
									<?=$arResult['SUM_NEW_PRICE']?><span class="rouble">a</span>
								</span>
								<span class="coupon__pic-price-old">
									<?=$arResult['SUM_OLD_PRICE']?><span class="rouble">a</span>
								</span>
							</div>
						</div>
						<div class="coupon__info">
						  <div class="coupon__title"><?=$arResult['NAME']?></div>
						  <p class="coupon__desc"><?=$arResult['TEXT']?></p>
						</div>
					</div><?
				}
			break;
		}
	break;

	case 'DOWNLOAD':
		$img1 = '';
		$img2 = '';

		$rsFile = CFile::GetByID($arResult['IMG1']);
		$arFile = $rsFile->Fetch();
		if (!empty($arFile)) {
			$img = CFile::ResizeImageGet($arResult['IMG1'], array('width' => 454, 'height' => 143), BX_RESIZE_IMAGE_PROPORTIONAL);
			$img1 = $img['src'];
		} else {
			$img1 = $arResult['IMG1'];
		}
		$rsFile = CFile::GetByID($arResult['IMG2']);
		$arFile = $rsFile->Fetch();
		if (!empty($arFile)) {
			$img = CFile::ResizeImageGet($arResult['IMG2'], array('width' => 217, 'height' => 293), BX_RESIZE_IMAGE_PROPORTIONAL);
			$img2 = $img['src'];
		} else {
			$img2 = $arResult['IMG2'];
		}

		?><div class="coupon _image ui-draggable<?=$sBlockCss?>" data-id="<?=$arResult['ID']?>"><?
			echo $sCouponRemoveHtml;
			echo $sToCartButtonHtml;
			?><img src="<?=$img1?>" alt="">
			<img src="<?=$img2?>" alt="">
		</div><?
	break;
}

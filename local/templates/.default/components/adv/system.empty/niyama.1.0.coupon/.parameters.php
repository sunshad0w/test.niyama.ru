<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"CUPONS_IDS" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('CUPONS_IDS'),
		"TYPE"		=> "STRING",		
		"MULTIPLE" => "N",		
	),	
	"SHOW_USED" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('SHOW_USED'),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N"
	),
	"SHOW_IN_CART" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('SHOW_IN_CART'),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N"
	),
	"IN_CART" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('IN_CART'),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N"
	),
	"ID_IN_CART" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('ID_IN_CART'),
		"TYPE" => "STRING",
		"DEFAULT" => "N"
	),
	"RES" => array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('RES'),
		"TYPE" => "STRING",
		"DEFAULT" => "N"
	)
	
);

?>
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Скидки и Акции
//

?><h1 class="page__title"><?=$arParams['~PAGE_TITLE']?></h1><?
?><div class="news"><?
		// лента
		CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.iblock_data_list',
			'niyama.1.0.promos-list',
			array(
				'CACHE_TYPE' => 'A',
				'CACHE_TIME' => '43200',
				'ELEMENT_CNT' => 20,
				'PAGER_SHOW' => 'Y',
				'ELEMENT_FILTER_NAME' => '',
				'SORT_BY1' => 'DATE_ACTIVE_FROM',
				'SORT_ORDER1' => 'DESC',
				'SORT_BY2' => 'SORT',
				'SORT_ORDER2' => 'ASC',
				'IBLOCKS' => array(CProjectUtils::GetIBlockIdByCode('actions', 'misc')),
				'FIELD_CODE' => array(
					'ID', 'NAME', 'CODE', 'PREVIEW_TEXT', 'DATE_ACTIVE_FROM', 'DETAIL_PAGE_URL',
					'PROPERTY_*',
				),
				'KEY_FIELD' => '',
				'INCLUDE_TEMPLATE' => 'Y',
				'CACHE_TEMPLATE' => 'Y',
				'GET_NEXT_ELEMENT_MODE' => 'Y'
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);


?></div><?

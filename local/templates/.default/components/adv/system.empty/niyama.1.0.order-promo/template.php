<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Промо-блок оформления заказа (вызывается из шаблона компонента niyama:order.make)
//
/*
Промо-блок с N карточками блюд. Карточки блюд выводятся в виде слайдера со стрелками прокрутки. 
По умолчанию N равно двум, в административной части должна быть возможность настраивать вывод количества с ограничением в 4 карточки. 
В блоке могут выводиться (в порядке приоритета):

o	Условие следующего недостигнутого уровня (если таковой есть) из списка скидок к текущему заказу (см. «Уровни скидок текущего заказа»). 
Для условия подбираются и выводятся  карточки блюд, добавление которых в заказ позволит выполнить условие и получить бонус (бесплатную доставку). По умолчанию выводится 2 карточки блюда.
Если кликнуть по цене на карточке или по кнопке «Добавить», доступной при наведении, то карточка попадает в список персоны, если в заказе только одна персона, 
или в список «Дополнительно на всех», если в заказе несколько персон.
Также карточку можно перетащить в список любой персоны.
Когда блюдо добавлено к заказу, вид карточки в блоке меняется: отображается количество блюд, добавленных к заказу.
Список блюд, выводимых в блоке, подбирается автоматически по следующему алгоритму:
	Блюдо не должно содержаться в заказе;
	Стоимость блюда должна дополнять сумму заказа до суммы, указанной в условии с минимальными отклонениями от указанной суммы (50-100 рублей);
	Предпочтение отдается блюдам, которые чаще всего заказываются другими пользователями, если текущий пользователь не авторизован, или текущим пользователем;

o	Если все условия уровней скидок к заказу выполнены, то выводятся карточки  с установленным признаком «Маржинальный» в соответствии с установленным весом (см. раздел «Настройка сведений о блюде»). 
Карточки блюд выводятся при условии, если блюд нет в заказе;

// этот пункт пропускаем, т.к. изменились условия для промо-блока на слое меню
o	Если нет блюд, соответствующих выше перечисленным условиям, то выводятся карточки блюд, установленных для вывода в промо-блок слоя меню (см. раздел «Промо-блок»), при условии, что блюда еще нет в заказе. 
Предпочтение отдается блюдам, которые чаще заказываются текущим пользователем, если доступна статистика, или всеми пользователями;

o	Если нет карточек, соответствующих выше перечисленным условиям, то для вывода в блок выбираются карточки из множества «Отличное сочетание» для всех блюд, 
добавленных к заказу (см. раздел «Настройка блока «Отличное сочетание»). Блюда выбираются при условии (в порядке приоритета выбора), если:
	Блюда еще нет в заказе;
	Блюдо чаще заказывается текущим пользователем, если есть такая статистика, или всеми пользователями.
*/

CModule::IncludeModule('iblock');

$arCartLine = CNiyamaIBlockCartLine::GetList();
$iItemsCnt = intval(CNiyamaCustomSettings::GetStringValue('order_info_promo_items_cnt', 4));
$arResult = $arParams['arResult'];
$arNextLevel = array();
foreach($arCartLine as $arItem) {
	if($arItem['CODE'] > $arResult['ORDER']['PRICE']) {
		$arNextLevel = $arItem;
		break;
	}
}

// полный каталог блюд
$arCatalog = CNiyamaCatalog::GetProductsDetailsList(false, false);
foreach($arCatalog as $mKey => $arItem) {
	if(($arItem['INFO']['ACTIVE'] != 'Y') || ($arItem['INFO']['_ACTIVE'] != 'Y')) {
		unset($arCatalog[$mKey]);
	}
}

// блюда в корзине
$arCartProducts = array();
foreach($arResult['CART'] as $sTableType => $arTables) {
	foreach($arTables as $arTableItems) {
		if($arTableItems['ITEMS']) {
			foreach($arTableItems['ITEMS'] as $arItem) {
				if($arItem['PROPS']['type'] == 'product') {
					$arCartProducts[$arItem['PRODUCT_ID']] = $arCatalog[$arItem['PRODUCT_ID']];
					// удалим из каталога все блюда, которые есть в заказе
					unset($arCatalog[$arItem['PRODUCT_ID']]);
				}
			}
		}
	}
}

// получим список блюд, наиболее часто заказываемых
$arUserOrders = array();
$arFilter = array(
	'>ORDER_ID' => 0,
	'NOTES' => 'product',
	'@ORDER_STATUS' => array('N', 'F'),
	// за полгода выбираем данные
	'>=DATE_UPDATE' => date($GLOBALS['DB']->DateFormatToPHP(CSite::GetDateFormat('FULL')), (time() - (180 * 86400))),
);
if($GLOBALS['USER']->GetId()) {
	$arFilter['USER_ID'] = $GLOBALS['USER']->GetId();
}
$dbItems = CSaleBasket::GetList(
	array(
		// не работает сортировка по CNT
		//'CNT' => 'DESC'
	),
	$arFilter,
	array(
		'PRODUCT_ID'
	),
	array(
		'nTopCount' => 500
	)
);
while($arItem = $dbItems->Fetch()) {
	$arUserOrders[$arItem['PRODUCT_ID']] = $arItem;
}
if($arUserOrders) {
	CProjectUtils::DoSort($arUserOrders, 'CNT', 'DESC');
}

$arCarouselItems = array();
$sPromoBlockHtml = '';
if($arNextLevel) {
	//
	// условие недостигнутого уровня
	//
	$dLeftSum = $arNextLevel['CODE'] - $arResult['ORDER']['PRICE'];
	if($arNextLevel['PREVIEW_TEXT']) {
		$sPromoBlockHtml = str_replace('#SUM#', CProjectUtils::FormatNum($dLeftSum).'&nbsp;<span class="rouble">a</span>', $arNextLevel['PREVIEW_TEXT']);
	}
	$dLeftSum1 = $dLeftSum - 100;
	$dLeftSum2 = $dLeftSum + 100;
	foreach($arCatalog as $arItem) {
		if($arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'] >= $dLeftSum1 && $arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'] <= $dLeftSum2) {
			$arCarouselItems[$arItem['INFO']['ID']] = array(
				// отклонение от требуемой суммы
				'_PRICE_DIFF_' => abs($dLeftSum - $arItem['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']),
				// популярность блюда
				'_ORDERS_STAT_' => $arUserOrders[$arItem['INFO']['ID']] ? $arUserOrders[$arItem['INFO']['ID']]['CNT'] : 0,
				'NAME' => $arItem['INFO']['NAME'],
				'PRICE' => $arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
				'IMG_ID' => $arItem['INFO']['PREVIEW_PICTURE'],
			);
		}
	}
	if($arCarouselItems) {
		// поднимем вверх наиболее популярные блюда
		CProjectUtils::DoSort($arCarouselItems, '_ORDERS_STAT_', 'DESC');
		// поднимем вверх блюда, наиболее близкие по цене
		CProjectUtils::DoSort($arCarouselItems, '_PRICE_DIFF_', 'ASC');
	}
} 

if(!$arCarouselItems) {
	//
	// маржинальные блюда
	//
	foreach($arCatalog as $arItem) {
		if($arItem['INFO']['PROPERTY_MARGINALITY_VALUE']) {
			$arCarouselItems[$arItem['INFO']['ID']] = array(
				'_MARGINALITY_WEIGHT_' => $arItem['INFO']['PROPERTY_MARGINALITY_WEIGHT_VALUE'],
				'NAME' => $arItem['INFO']['NAME'],
				'PRICE' => $arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
				'IMG_ID' => $arItem['INFO']['PREVIEW_PICTURE'],
			);
		}
	}
	if($arCarouselItems) {
		// поднимем вверх наиболее популярные блюда
		CProjectUtils::DoSort($arCarouselItems, '_MARGINALITY_WEIGHT_', 'DESC');
	}
}

if(!$arCarouselItems && $arCartProducts) {
	//
	// отличное сочетание (рекомендуемые)
	//
	$arRecommended = array();
	$dbItems = CIBlockElement::GetList(
		array(),
		array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('recommended', 'catalog'),
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'N',
			'PROPERTY_PRODUCT_ID' => array_keys($arCartProducts)
		),
		false,
		false,
		array(
			'ID', 'PROPERTY_PRODUCTS'
		)
	);
	while($arItem = $dbItems->Fetch()) {
		if($arItem['PROPERTY_PRODUCTS_VALUE'] && is_array($arItem['PROPERTY_PRODUCTS_VALUE'])) {
			foreach($arItem['PROPERTY_PRODUCTS_VALUE'] as $iKey => $iRecommendedProductId) {
				if($arItem['PROPERTY_PRODUCTS_DESCRIPTION'][$iKey] == 'on') {
					// от шеф-повара
					$arRecommended[$iRecommendedProductId] = $iRecommendedProductId;
				}
			}
		}
	}

	if($arRecommended) {
		foreach($arRecommended as $iRecommendedProductId) {
			if($arCatalog[$iRecommendedProductId]) {
				$arItem = $arCatalog[$iRecommendedProductId];
				$arCarouselItems[$arItem['INFO']['ID']] = array(
					// популярность блюда
					'_ORDERS_STAT_' => $arUserOrders[$arItem['INFO']['ID']] ? $arUserOrders[$arItem['INFO']['ID']]['CNT'] : 0,
					'NAME' => $arItem['INFO']['NAME'],
					'PRICE' => $arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
					'IMG_ID' => $arItem['INFO']['PREVIEW_PICTURE'],
				);
			}
		}
	} else {
		foreach($arUserOrders as $iProductId => $arItem) {
			if($arCatalog[$iProductId]) {
				$arItem = $arCatalog[$iProductId];
				$arCarouselItems[$arItem['INFO']['ID']] = array(
					// популярность блюда
					'_ORDERS_STAT_' => $arItem['CNT'],
					'NAME' => $arItem['INFO']['NAME'],
					'PRICE' => $arItem['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'],
					'IMG_ID' => $arItem['INFO']['PREVIEW_PICTURE'],
				);
				if(count($arCarouselItems) >= $iItemsCnt) {
					break;
				}
			}
		}
	}

	if($arCarouselItems) {
		// поднимем вверх наиболее популярные блюда
		CProjectUtils::DoSort($arCarouselItems, '_ORDERS_STAT_', 'DESC');
	}
}

if($arCarouselItems) {
	$arCarouselItems = array_slice($arCarouselItems, 0, $iItemsCnt, true);

	if(!strlen($sPromoBlockHtml)) {
		$sPromoBlockHtml = '<div class="label">Рекомендуем</div><div class="order__add-desc"></div>';
	}

	?><div class="order__add"><?
		echo $sPromoBlockHtml;
		?><div class="order__add-carousel"><?
			foreach($arCarouselItems as $iProductId => $arItem) {
				?><div class="order__add-carousel-item">
					<div class="goods _order">
						<div class="goods__wrap"><?
							?><div class="goods__image"><?
								if($arItem['IMG_ID']) {
									$arTmpImg = CFile::ResizeImageGet(
										$arItem['IMG_ID'],
										array(
											'width' => 110, 
											'height' => 110
										), 
										BX_RESIZE_IMAGE_EXACT,
										true
									);
									if($arTmpImg['src']) {
										?><img src="<?=$arTmpImg['src']?>" alt=""><?
									}
								}
							?></div>
							<div class="goods__desc">
								<h2 class="goods__name"><?=$arItem['NAME']?></h2>
								<div class="goods__cost"><?=CProjectUtils::FormatNum($arItem['PRICE'])?><span class="rouble">a</span></div>
								<div data-id="<?=$iProductId?>" class="goods__add-btn btn">Добавить</div>
							</div>
						</div>
					</div>
				</div><?
			}
		?></div>
	</div><?
}

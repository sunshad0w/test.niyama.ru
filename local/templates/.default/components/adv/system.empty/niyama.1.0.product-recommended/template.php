<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');


// Получаем рекомендованные товары.
$arRelatedProducts = CNiyamaCatalog::GetRecProducts($arParams['CODE']);
if(empty($arRelatedProducts )) {
	return;
}

?><label class="label"><?= $arParams['BLOCK_TITLE'] ?></label><?

?><div class="goods-slider js-widget" onclick="return {goodsSlider: {}}">
    <div class="goods-slider__slider"><?

                foreach ($arRelatedProducts as $arProduct) {

                    if (isset($arProduct['INFO']) && !empty($arProduct['INFO'])) {

                    $arImg = CFile::GetFileArray($arProduct['INFO']['PREVIEW_PICTURE']);
                    ?><div class="goods-slider__slider-item">
                            <div class="goods">
                                <div class="goods__wrap">
                                    <div class="goods__image"><img src="<?=  $arImg['SRC'] ?>"></div>
                                    <div class="goods__desc">
                                        <a href="<?=$arProduct['INFO']['DETAIL_PAGE_URL']?>">
                                        <h2 class="goods__name"><?= $arProduct['INFO']['NAME'] ?></h2>
                                        </a>
                                        <?
                                        /*
										if (!empty($arProduct['EXCLUDE_INGREDIENT'])) {
											$ex_ings = array();
											foreach($arProduct['EXCLUDE_INGREDIENT'] as $key => $elem) {
												if (!in_array($elem, $arProduct['ALL_INGREDIENT'])) {
													$ex_ings[$key] = $elem;
												}
											}
											if (!empty($ex_ings)) {
												$arProduct['ALL_INGREDIENT'] = array_merge($arProduct['ALL_INGREDIENT'], $ex_ings);
											}
										}
										*/
										?>
                                        <div class="goods__composition"><?=implode(', ', $arProduct['ALL_INGREDIENT'])?></div>
                                        <div class="goods__cost"><?= $arProduct['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE'] ?>  <span class="rouble">a </span></div>
                                        <div class="goods__add-btn btn" data-id="<?=$arProduct['INFO']['ID']?>" onclick="yaCounter24799106.reachGoal('basket_click' ); ga ('send','pageview','/basket_click'); return true;">В корзину</div>
                                    </div><?
                                    if (!empty($arProduct['INFO']['PROPERTY_NUM_OF_PIECES'])) {
                                        ?><div class="goods__num"><?= $arProduct['INFO']['PROPERTY_NUM_OF_PIECES'] ?> шт.</div><?
                                    }
									if ($arProduct['CHEF'] == 'on') {
										?><div class="goods__sheff"></div><?
									}
                                ?></div>
                            </div>
                        </div><?

                    }

                }

            ?></div></div>
</div><?

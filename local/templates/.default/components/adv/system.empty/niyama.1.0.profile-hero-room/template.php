<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->SetTitle("Комната героя");

# Получаем медаль текущего пользователя
$arISReturn = CNiyamaMedals::GetUserMedals();
# Получаем общий список медалей
$arReturn = CNiyamaMedals::GetMedalsList();

# Выводим медали которые уже есть у пользовтеля первыми.
$newArray = array();
foreach ($arReturn as $key => $row){
    if (isset($arISReturn[$row['ID']])) {
        $newArray[] = $row;
        unset($arReturn[$key]);
    }
}

$arISNUReturn = CNiyamaMedals::GetUserMedals(false, "N");

?><div class="cabinet__main">
    <div class="cabinet__title">
        <h2 class="h1 pull-left">Комната героя</h2><a href="/ajax/getProfileOrderList.php" data-fancybox-type="ajax" class="fancybox cabinet__title-link pull-right">История заказов</a><a href="/profile/" class="cabinet__title-link pull-right">Личный кабинет</a>
    </div>
    <div class="cabinet__info cabinet__transparent _medals"><?

        foreach ($arISNUReturn as $nuRow) {

            $arImg['SRC'] = 'https://cdn0.iconfinder.com/data/icons/customicondesign-office7-shadow-png/128/Metal-gold-blue.png';
            if (!empty($nuRow['PROPERTY_MEDAL_ID_PROPERTY_BASE_TYPE_IMG_DOWNLOAD_VALUE'])) {
                $arImg = CFile::GetFileArray($nuRow['PROPERTY_MEDAL_ID_PROPERTY_BASE_TYPE_IMG_DOWNLOAD_VALUE']);
            }

            ?><div class="medal">
            <div class="medal__body"><img src="<?= $arImg['SRC'] ?>"></div>
            <div class="medal__ribbon">
                <div class="medal__ribbon-text"><?= $nuRow['PROPERTY_MEDAL_ID_NAME'] ?></div>
            </div>
            </div><?
        }

        foreach ($arReturn as $row) {
                ?><div class="medal">
                    <div class="medal__body _empty">
                        <div class="medal__desc"><?= $row['PREVIEW_TEXT'] ?></div>
                    </div>
                    <div class="medal__ribbon">
                        <div class="medal__ribbon-text"><?= $row['NAME'] ?></div>
                    </div>
                </div><?
            }

    ?></div>
</div><?




?><div class="cabinet__side"><?

    # Карта
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.profile-card',
        array(),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );

    # Голосование
    $APPLICATION->IncludeComponent(
        'adv:system.empty',
        'niyama.1.0.profile-vote',
        array(),
        null,
        array(
            'HIDE_ICONS' => 'Y'
        )
    );

?></div>


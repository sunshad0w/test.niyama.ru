<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

?><div class="cabinet__main"><?
	$APPLICATION->IncludeComponent(
		'bitrix:menu',
		'niyama.1.0.profile',
		array(
			'ROOT_MENU_TYPE' => 'profile',
			'MENU_CACHE_TYPE' => 'A',
			'MENU_CACHE_TIME' => '3600',
			'MENU_CACHE_USE_GROUPS' => 'Y',
			'MENU_CACHE_GET_VARS' => '',
			'MAX_LEVEL' => '1',
			'CHILD_MENU_TYPE' => 'left',
			'USE_EXT' => 'N',
			'DELAY' => 'N',
			'ALLOW_MULTI_SELECT' => 'N'
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
	?><div class="cabinet__info cabinet__transparent"><?
		// профиль
		$APPLICATION->IncludeComponent(
			'bitrix:main.profile', 
			'niyama.1.0', 
			array(
				'AJAX_MODE' => 'N',
				'AJAX_OPTION_JUMP' => 'N',
				'AJAX_OPTION_STYLE' => 'N',
				'AJAX_OPTION_HISTORY' => 'N',
				'SET_TITLE' => 'N',
				'USER_PROPERTY' => array(),
				'SEND_INFO' => 'N',
				'CHECK_RIGHTS' => 'N',
				'ACTION_PARAM' => '',
				'AJAX_OPTION_ADDITIONAL' => ''
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		# Список гостей
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.profile-guest',
			array(),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		# Адреса доставки
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.profile-address',
			array(),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	?></div>
</div>
<div class="cabinet__side"><?
	# Карта
	$APPLICATION->IncludeComponent(
		'adv:system.empty',
		'niyama.1.0.profile-card',
		array(),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
	
	# Голосование
	$APPLICATION->IncludeComponent(
		'adv:system.empty',
		'niyama.1.0.profile-vote',
		array(),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);

?></div><?

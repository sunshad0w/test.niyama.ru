<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('iblock');

$bAjaxRequest = isset($arParams['AJAX_REQUEST']) && $arParams['AJAX_REQUEST'] == 'Y';
$bNofilter = isset($arParams['NOFILTER']) && $arParams['NOFILTER'] == 'Y';

$iCurrentPage = 1;
if($bAjaxRequest || isset($_REQUEST['page'])) {
	$iCurrentPage = isset($_REQUEST['page']) && intval($_REQUEST['page']) > 1 ? intval($_REQUEST['page']) : 1;
}

//
// SEO-инфоблок
//
$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.catalog-seo',
	array(
		'SET_TITLE' => 'Y',
		'SET_KEYWORDS' => 'Y',
		'SET_DESCRIPTION' => 'Y',
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);
$sSeoDescription = $iCurrentPage <= 1 ? $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_TEXT', '') : '';
$sSeoH1 = $iCurrentPage <= 1 ? $GLOBALS['APPLICATION']->GetPageProperty('NIYAMA_SEO_H1', '') : '';


$arSectionFilter = array();
$arExIngFilter = array();
# Фильтр
$arPFilter = array();
$arPFilterNoIng = array();
$_SESSION['NIYAMA']['FASET'] = array(
	'menu' => (!empty($_REQUEST['menu']) && (is_array($_REQUEST['menu']))) ? $_REQUEST['menu'] : array(),
	'ex_menu' => (!empty($_REQUEST['ex_menu']) && (is_array($_REQUEST['ex_menu']))) ? $_REQUEST['ex_menu'] : array(),
	'ing' => (!empty($_REQUEST['ing']) && (is_array($_REQUEST['ing']))) ? $_REQUEST['ing'] : array(),
	'ex_ing' => (!empty($_REQUEST['ex_ing']) && (is_array($_REQUEST['ex_ing']))) ? $_REQUEST['ex_ing'] : array(),
	'alerg' => (!empty($_REQUEST['alerg']) && (is_array($_REQUEST['alerg']))) ? $_REQUEST['alerg'] : array(),
);

$sQueryWord = '';
if (!empty($_REQUEST)) {
	$arPFilterMenu = false;
	if (!empty($_REQUEST['menu']) && (is_array($_REQUEST['menu']))) {
		$arPFilterMenu = true;
		$arPFilter['SECTION_ID'] = $_REQUEST['menu'];
		$arPFilter['INCLUDE_SUBSECTIONS'] = 'Y';
		$arSectionFilter[] = $_REQUEST['menu'];
	}
	if (!empty($_REQUEST['ex_menu']) && (is_array($_REQUEST['ex_menu']))) {
		
		$arPFilter['LOGIC'] = 'AND';
		$arPFilter['!SECTION_ID'] = $_REQUEST['ex_menu'];
	}

	$arPFilterSMenu = array();
	if (!empty($_REQUEST['specmenu']) && (is_array($_REQUEST['specmenu']))) {
		if ($arPFilterMenu) {
			$arPFilter['LOGIC'] = 'OR';
			$arPFilter['PROPERTY_SPEC_MENU.ID'] = $_REQUEST['specmenu'];
		} else {
			$arPFilter['PROPERTY_SPEC_MENU.ID'] = $_REQUEST['specmenu'];
		}

		$arSectionFilter[] = $_REQUEST['specmenu'];
	}

	$sQueryWord = isset($_REQUEST['q']) ? trim($_REQUEST['q']) : '';
	$arIngredients = $_REQUEST['ing'];

	if(strlen($sQueryWord)) {
		$alling = CNiyamaCatalog::getIngredients(
			null,
			array(
				'NAME' => $sQueryWord
			)
		);
		if (!empty($alling)) {
			$arIngredients[] = $alling[0]['ID'];
			$sQueryWord = '';
		}
	}

	$arPFilterIng = array();
	$arPFilterNoIng = $arPFilter;
	if (!empty($arIngredients) && (is_array($arIngredients))) {
		$arSectionFilter[] = $arIngredients;
		if ($arPFilterMenu) {
			$arPFilter['LOGIC'] = 'AND';
			$arPFilter[] = array(
				'LOGIC' => 'OR',
				array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
				array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
			);
		} else {
			// $arPFilter['LOGIC'] = 'OR';
			$arPFilter[] = array(
				'LOGIC' => 'OR',
				array('?PROPERTY_GENERAL_INGREDIENT' => $arIngredients), // implode(' || ',$arIngredients)),
				array('?PROPERTY_MORE_INGREDIENT' => $arIngredients) //implode(' || ',$arIngredients))
			);
		}
	}
	$arExIngredients = $_REQUEST['ex_ing'];
	if (!empty($arExIngredients) && (is_array($arExIngredients))) {
			$IBLOCK_ID_genaral_menu = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');
			$arPFilter['LOGIC'] = 'AND';
			$arPFilter[] = array(
				'LOGIC' => 'AND',
				array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_GENERAL_INGREDIENT' => $arExIngredients))),
				array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_MORE_INGREDIENT' => $arExIngredients))),
				
			);
	}
	$arExIngredients = $_REQUEST['alerg'];
	if (!empty($arExIngredients) && (is_array($arExIngredients))) {
		$arExIngFilter = $arExIngredients;
		$IBLOCK_ID_genaral_menu = CProjectUtils::GetIBlockIdByCode('general-site-menu', 'catalog');
		$arPFilter['LOGIC'] = 'AND';
		$arPFilter[] = array(
			'LOGIC' => 'AND',
			array('!ID' => CIBlockElement::SubQuery('ID', array('IBLOCK_ID' => $IBLOCK_ID_genaral_menu, '?PROPERTY_EXCLUDE_INGREDIENT' => $arExIngredients))),
		);
	}
}

$isFirstPage = false;
$arNavArray = array('nPageSize' => 24);
if($arParams['AJAX_REQUEST'] == 'Y') {
	if (!empty($_REQUEST['page'])) {
		$arNavArray['iNumPage'] = intval($_REQUEST['page']);
		$isFirstPage = (intval($_REQUEST['page']) == 1) ? true : false;
	} else {
		$arNavArray['iNumPage'] = 1;
		$isFirstPage = true;
	}
} else {
	$arNavArray['iNumPage'] = 1;
	$isFirstPage = true;
}

if(strlen($sQueryWord)) {
	$temp_ar = array(
		'LOGIC' => 'OR',
		//array('NAME' => '%'.$sQueryWord.'%'),
		array('PROPERTY_IMPORT_ELEMENT.NAME' => '%'.$sQueryWord.'%'),
		array('PROPERTY_GENERAL_INGREDIENT.NAME' => '%'.$sQueryWord.'%'),
		array('PROPERTY_MORE_INGREDIENT.NAME' => '%'.$sQueryWord.'%')
	);
	$arPFilter[] = $temp_ar;
	$arPFilterNoIng[] = $temp_ar;
}

$arAllElements = CNiyamaCatalog::getAllMenuElements();

/**
 * Строим заголовок
 */
$sFilterHeader = '';
if (isset($_REQUEST['menu']) && (!empty($_REQUEST['menu']))) {
	$arMenusNames = CNiyamaCatalog::getMenuNames();
	$arSelectMenuNames = array();
	foreach ($_REQUEST['menu'] as $mId) {
		$arSelectMenuNames[] = $arMenusNames[$mId];
	}

	if (count($arSelectMenuNames) > 2) {
		$sLst = $arSelectMenuNames[count($arSelectMenuNames) - 1];
		unset($arSelectMenuNames[count($arSelectMenuNames) - 1]);

		$sFilterHeader .= implode(', ', $arSelectMenuNames);
		$sFilterHeader .= ' или '.$sLst;

	} else {
		$sFilterHeader .= implode(' или ', $arSelectMenuNames);
	}

	if (isset($_REQUEST['ing']) && (!empty($_REQUEST['ing']))) {
		$sFilterHeader .= ' c ';
		$arTpNamesOfIngredients = CNiyamaCatalog::getTpNamesOfIngredients();
		$arSelectIngNames = array();
		foreach ($_REQUEST['ing'] as $iIngRow) {
			if (!empty($arTpNamesOfIngredients[$iIngRow])) {
				$arSelectIngNames[] = $arTpNamesOfIngredients[$iIngRow] ;
			}
		}

		if (count($arSelectIngNames) > 2) {

			$sLst = $arSelectIngNames[(count($arSelectIngNames) -1)];
			unset($arSelectIngNames[(count($arSelectIngNames) -1)]);

			$sFilterHeader .= implode(', ', $arSelectIngNames);
			$sFilterHeader .= ' или '.$sLst;

		} else {
			$sFilterHeader .= implode(' или ', $arSelectIngNames);
		}
	}
} else {
	if (isset($_REQUEST['ing']) && (!empty($_REQUEST['ing']))) {
		$sFilterHeader .= 'Блюда с ';
		$arTpNamesOfIngredients = CNiyamaCatalog::getTpNamesOfIngredients();
		$arSelectIngNames = array();
		foreach ($_REQUEST['ing'] as $iIngRow) {
			if (!empty($arTpNamesOfIngredients[$iIngRow])) {
				$arSelectIngNames[] = $arTpNamesOfIngredients[$iIngRow] ;
			}
		}
		if (count($arSelectIngNames) > 2) {

			$sLst = $arSelectIngNames[count($arSelectIngNames) -1];
			unset($arSelectIngNames[count($arSelectIngNames) -1]);

			$sFilterHeader .= implode(', ', $arSelectIngNames);
			$sFilterHeader .= ' или '.$sLst;

		} else {
			$sFilterHeader .= implode(' или ', $arSelectIngNames);
		}
	}
}


$tmpSections = array();

foreach ($arSectionFilter as $item) {
	foreach ($item as $roe) {
		$tmpSections[] = $roe;
	}
}



if (!empty($tmpSections)) {
	CNiyamaStatistic::FasStatisticAdd($tmpSections);
}

$iPerPage = 24;

$arNavArray = null;
$arMenuItems = CNiyamaCatalog::GetList(null, $arPFilter, null, $arNavArray);
$arMenuItemsNoIng = CNiyamaCatalog::GetList(null, $arPFilterNoIng, null, $arNavArray);

#
# Пересортировка с частотой заказов
#
$arDisplayProducts = $arMenuItems['ITEMS'] && is_array($arMenuItems['ITEMS']) ? $arMenuItems['ITEMS'] : array();
if(!empty($arMenuItems['ITEMS'])) {
	$iUserId = $GLOBALS['USER']->IsAuthorized() ? $GLOBALS['USER']->GetId() : false;
	$arOrderProductsIds = CNiyamaCatalog::GetOrderProductsIds($iUserId);
	if($arOrderProductsIds) {
		$arTmpIsOrders = array();
		foreach($arOrderProductsIds as $iProductId => $iOrdersCnt) {
			if(isset($arMenuItems['ITEMS'][$iProductId])) {
				$arTmpIsOrders[$iProductId] = $arMenuItems['ITEMS'][$iProductId];
				unset($arMenuItems['ITEMS'][$iProductId]);
			}
		}
		$arDisplayProducts = array_merge($arTmpIsOrders, $arMenuItems['ITEMS']);
	}
}


#
# Если указаны два и более ингредиентов
#
if (isset($_REQUEST['ing']) && !empty($_REQUEST['ing']) && (count($_REQUEST['ing']) > 1) ) {
	$arTmpIsOrdersIng = array();

	if (!empty($arDisplayProducts)) {
		foreach ($arDisplayProducts as $key => $val) {
			$arIngProduct = array();
			if (!empty($val['ING']['ALL_INGREDIENT'])) {
				$arIngProduct = array_keys($val['ING']['ALL_INGREDIENT']);
			}

			$arTmpCount = array();
			foreach ($_REQUEST['ing'] as $uIng) {
				if (in_array($uIng, $arIngProduct)) {
					$arTmpCount[] = $uIng;
				}
			}
			if (count($arTmpCount) == count($_REQUEST['ing'])) {
				$arTmpIsOrdersIng[] = $val;
				unset($arDisplayProducts[$key]);
			}
		}

		if (!empty($arTmpIsOrdersIng)) {
			$tmparDub = array();
			foreach($arTmpIsOrdersIng as $key => $srow) {
				$arSubIrow = array_keys($srow['ING']['ALL_INGREDIENT']);
				if (count($arSubIrow) == count($_REQUEST['ing'])) {
					$tmparDub[] = $srow;
					unset($arTmpIsOrdersIng[$key]);
				}
			}
			if (!empty($tmparDub)) {
				$arTmpIsOrdersIng = array_merge((array)$tmparDub, (array)$arTmpIsOrdersIng);
			}
		}

		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge((array)$arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}

#
# Проверка на Меню
#
if ($arPFilterMenu) {
	$arTmpIsOrdersIng = array();
	if (!empty($arDisplayProducts)) {
		foreach ($arDisplayProducts as $key => $val) {
			$iSectionId = (isset($val['INFO']['IBLOCK_SECTION_ID']) && !empty($val['INFO']['IBLOCK_SECTION_ID'])) ? $val['INFO']['IBLOCK_SECTION_ID'] : false;
			if ($iSectionId) {
				if(in_array($iSectionId, $_REQUEST['menu'])) {
					$arTmpIsOrdersIng[] = $val;
					unset($arDisplayProducts[$key]);
				}
			}
		}
		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge($arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}

#
# Проверка на сортировку вывода по Ингредиенту
#
if (!empty($_REQUEST['ing'])) {
	$arTmpIsOrdersIng = array();
	if (!empty($arDisplayProducts)) {
		$nIngReverseArray = $_REQUEST['ing'];
		foreach ($nIngReverseArray as $uIng) {
			foreach ($arDisplayProducts as $key => $val) {
				# Получаем ингредиетны
				$arIngProduct = array();
				if (!empty($val['ING']['ALL_INGREDIENT'])) {
					$arIngProduct = array_keys($val['ING']['ALL_INGREDIENT']);
				}

				if (in_array($uIng,$arIngProduct)) {
					$arTmpIsOrdersIng[] = $val;
					unset($arDisplayProducts[$key]);
				}
			}
		}
	}
	if (!empty($arTmpIsOrdersIng)) {
		$arDisplayProducts = array_merge($arTmpIsOrdersIng, (array)$arDisplayProducts);
	}
}

#
# Проверка на кол-во совпадений
#
if (!empty($_REQUEST['ing'])) {
	$arSeparator = array();
	$arTmpIsOrdersIng = array(); 
	$ALTmpCountArray = array(); 
	$arIngSeparator = array();
	if (!empty($arDisplayProducts)) {
		foreach ($arDisplayProducts as $key => $val) {

			# Получаем ингредиетны
			$arIngProduct = array();
			if (!empty($val['ING']['ALL_INGREDIENT'])) {
				$arIngProduct = array_keys($val['ING']['ALL_INGREDIENT']);
			}
			$arTmpCount = array();
			foreach($_REQUEST['ing'] as $uIng) {
				if(in_array($uIng, $arIngProduct)) {
					$arTmpCount[] = $uIng;
				}
			}
			if (!empty($arTmpCount)) {
				$ALTmpCountArray[count($arTmpCount)][] = $val;
				$arIngSeparator[count($arTmpCount)] = $arTmpCount;
				unset($arDisplayProducts[$key]);
			}
		}
		if (!empty($ALTmpCountArray)) {
			krsort($ALTmpCountArray);
			foreach ($ALTmpCountArray as $count => $mRow) {
				foreach ($mRow as $SubRow) {
					$arTmpIsOrdersIng[] = $SubRow;
					$arSeparator[$SubRow['INFO']['ID']] = array('ing' => $arIngSeparator[$count]);
				}
			}
		}
		unset($ALTmpCountArray);
		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge($arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}

#
#  Проверка по Меню (типу)
#
if ($arPFilterMenu) {
	$arTmpIsOrdersIng = array(); 
	$arTyStup = array();
	if (!empty($arDisplayProducts)) {
		foreach ($_REQUEST['menu'] as $dMenu) {
			foreach ($arDisplayProducts as $key => $val) {
				$iSectionId = (isset($val['INFO']['IBLOCK_SECTION_ID']) && !empty($val['INFO']['IBLOCK_SECTION_ID'])) ? $val['INFO']['IBLOCK_SECTION_ID'] : false;
				if ($iSectionId) {
					if($dMenu == $iSectionId) {
						$arTyStup[$iSectionId][] = $val;
						unset($arDisplayProducts[$key]);
					}
				}
			}
		}

		if (!empty($arTyStup)) {
			$nView = array();
			foreach ($arTyStup as $subs) {
				foreach ($subs as $riot) {
					$arTmpIsOrdersIng[] = $riot;
				}
			}
		}
		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge((array)$arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}

#
# Проверка на Меню и ингредиенты
#
if ($arPFilterMenu && !empty($_REQUEST['ing'])) {
	$arTmpIsOrdersIng = array();
	if (!empty($arDisplayProducts)) {
		foreach ($arDisplayProducts as $key => $val) {

			$iSectionId = (isset($val['INFO']['IBLOCK_SECTION_ID']) && !empty($val['INFO']['IBLOCK_SECTION_ID'])) ? $val['INFO']['IBLOCK_SECTION_ID'] : false;
			if ($iSectionId) {
				if(in_array($iSectionId, $_REQUEST['menu'])) {
					# Получаем ингредиетны
					$arIngProduct = array();
					if (!empty($val['ING']['ALL_INGREDIENT'])) {
						$arIngProduct = array_keys($val['ING']['ALL_INGREDIENT']);
					}
					$arTmpCount = array();
					foreach ($_REQUEST['ing'] as $uIng) {
						if (in_array($uIng, $arIngProduct)) {
							$arTmpCount[] = $uIng;
						}
					}
					if (count($arTmpCount) == count($_REQUEST['ing'])) {
						$arTmpIsOrdersIng[] = $val;
						unset($arDisplayProducts[$key]);
					}
				}
			}
		}
		if (!empty($arTmpIsOrdersIng)) {
			$arDisplayProducts = array_merge($arTmpIsOrdersIng, (array)$arDisplayProducts);
		}
	}
}

#
# Получаем общий массив ингредиентов
#
$GLOBALS['ALL_AJAX_ING'] = array();
$arGing = array();
if (!empty($arDisplayProducts)) {
	foreach ($arDisplayProducts as $key => $val) {
		# Получаем ингредиенты
		$arIngProduct = array();
		if (!empty($val['ING']['ALL_INGREDIENT'])) {
			$arGing = array_merge($arGing, array_keys($val['ING']['ALL_INGREDIENT']));
		}
	}
}


// для получения всех видимых
if (!empty($arMenuItemsNoIng['ITEMS'])) {
	foreach ($arMenuItemsNoIng['ITEMS'] as $key => $val) {
		# Получаем ингредиетны
		if (!empty($val['ING']['ALL_INGREDIENT'])) {
			$arGing = array_merge($arGing, (array) array_keys($val['ING']['ALL_INGREDIENT']));
		}
	}
}

if (!empty($arGing)) {
	$GLOBALS['ALL_AJAX_ING'] = array_unique($arGing);
}


#
# Получаем общий массив исключающих ингредиентов
#
$GLOBALS['ALL_AJAX_EX_ING'] = array();
$arGingEx = array();
if (!empty($arDisplayProducts)) {
	foreach ($arDisplayProducts as $key => $val) {
		# Получаем ингредиетны		
		if (!empty($val['EX_ING']['EXCLUDE_INGREDIENT'])) {
			$arGingEx = array_merge($arGingEx, array_keys($val['EX_ING']['EXCLUDE_INGREDIENT']));
		}
	}
}
if (!empty($arGingEx)) {
	$GLOBALS['ALL_AJAX_EX_ING'] = array_unique($arGingEx);
}


/**
 * Получаем массив ID Основного меню
 */
$GLOBALS['ALL_AJAX_MENU'] = array();
if(!empty($arDisplayProducts)) {
	$arGingF = array();
	foreach($arDisplayProducts as $key => $val) {
		# Получаем ингредиетны
		$arIngProduct = array();
		if(!empty($val['INFO']['IBLOCK_SECTION_ID'])) {
			$arTmp = is_array($val['INFO']['IBLOCK_SECTION_ID']) ? $val['INFO']['IBLOCK_SECTION_ID'] : array($val['INFO']['IBLOCK_SECTION_ID']);
			$arGingF = array_merge($arGingF, $arTmp);
		}
	}
}

if (!empty($arGingF)) {
	$GLOBALS['ALL_AJAX_MENU'] = array_unique($arGingF);
}


#
# Обработка перед выводом, расчет данных для постраничной навигации
#
$iAllCount = count($arDisplayProducts);
$arDisplayProducts = ADV_array_pagination($arDisplayProducts, $iCurrentPage, ($iPerPage - 1));

#
# Установка разделителя
#
if (!empty($_REQUEST['ing'])) {
	// ???
}

# Если используем ajax
if ($bAjaxRequest) {
	ob_start();
}

if($sSeoH1) {
	$sFilterHeader = $sSeoH1;
}

?><noindex><div onclick="return {'catalog': {}}" class="js-widget"><?
	?><div class="catalog__filters-act"><?
		foreach ($tmpSections as $row) {
			if (isset($arAllElements[$row])) {
				?><div data-filter="<?=$row?>"><?=$arAllElements[$row]?><a href="javascript:void(0)" class="catalog__filter-del">x</a></div><?
			}
		}
		foreach ($arExIngFilter as $row) {
			if (isset($arAllElements[$row])) {
				?><div data-filter="<?=$row?>"><span style="text-decoration: line-through;"><?=$arAllElements[$row]?></span><a href="javascript:void(0)" class="catalog__filter-del">x</a></div><?
			}
		}
	?> </div><?

	#
	# Устанавливаем заголовок
	#
	if($sFilterHeader) {
		?><p><?=$sFilterHeader?></p><?
	}

	$curSeparator = false;
	$bShowProductsList = true;
	if($bShowProductsList) {
		?><ul id="iso" class="catalog"><?
			foreach($arDisplayProducts as $item) {
				#
				# Проверка на разделитель для ингредиентов
				#
				/*
				if ( (!empty($_REQUEST['ing']) || !empty($_REQUEST['menu'])) ) {
					$arIngProduct = array();
					if (!empty($item['ING']['ALL_INGREDIENT'])) {
					 $arIngProduct = array_keys($item['ING']['ALL_INGREDIENT']);
					}

					$arTmpCount = array();
					if (isset($_REQUEST['ing']) && !empty($_REQUEST['ing'])) {
						foreach ( $_REQUEST['ing'] as $uIng) {
							if (in_array($uIng, $arIngProduct)) {
								$arTmpCount[] = $uIng;
							}
						}
					}
					$iSectionId = (isset($item['INFO']['IBLOCK_SECTION_ID']) && !empty($item['INFO']['IBLOCK_SECTION_ID']) )? $item['INFO']['IBLOCK_SECTION_ID'] : false;
					if ($iSectionId && !empty($_REQUEST['menu']) && in_array($iSectionId,$_REQUEST['menu'])) {
						$arTmpCount[] = $iSectionId;
					}

					$hashCurSeparator = md5(serialize($arTmpCount));
					if (!$curSeparator || $curSeparator != $hashCurSeparator) {
						$curSeparator = $hashCurSeparator;

						if (!empty($_REQUEST['menu'])) {
							$TmpSubCount = array();
							foreach ($arTmpCount as $key => $dEl) {
								if (in_array($dEl,$_REQUEST['menu'])) {
									$TmpSubCount[] = $dEl;
									unset($arTmpCount[$key]);
								}
							}
							$arTmpCount = array_merge((array)  $TmpSubCount,(array) $arTmpCount);
						}

						if (!empty($arTmpCount) && !empty($tmpSections )) {
							foreach ($arTmpCount as $key => $dEl) {
								if (!in_array($dEl,$tmpSections)) {
									unset($arTmpCount[$key]);
								}
							}
						}

						?><div class="catalog__separator"><?
						foreach($arTmpCount as $row) {
							?><div><?//=$arAllElements[$row]?></div><?
						}
						?></div><?
					}
				}
				*/

				# Фото
				$arAvaData = CFile::GetFileArray($item['INFO']['PREVIEW_PICTURE']);
				/*
				if(!empty($item['EX_ING']['EXCLUDE_INGREDIENT'])) {
					$ex_ings = array();
					foreach($item['EX_ING']['EXCLUDE_INGREDIENT'] as $key => $elem) {
						if (!in_array($elem,$item['ING']['ALL_INGREDIENT'])) {
							$ex_ings[$key] = $elem;
						}
					}
					if (!empty($ex_ings)) {
						$item['ING']['ALL_INGREDIENT'] = array_merge($item['ING']['ALL_INGREDIENT'], $ex_ings);
					}
				}
				*/
				$sIngValue = '';
				if(isset($item['ING']['ALL_INGREDIENT']) && !empty($item['ING']['ALL_INGREDIENT'])) {
					$sIngValue = implode(', ', $item['ING']['ALL_INGREDIENT']);
				}

				?><li data-category="one" class="catalog__item">
					<div class="goods">
						<div class="goods__wrap">
							<div class="goods__image fancybox" data-fancybox-group="goods" data-fancybox-type="ajax" href="/ajax/getGoodsCard.php?CODE=<?=$item['INFO']['CODE']?>"><img width="188" src="<?=$arAvaData['SRC']?>" alt=""></div>
							<div class="goods__desc">
								<div class="goods__composition">
									<a href="<?=$item['INFO']['DETAIL_PAGE_URL']?>">
										<p class="goods__name"><?=$item['INFO']['NAME']?></p>
									</a><?
									echo $sIngValue;
								?></div>
								<div class="goods__cost"><?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?> <span class="rouble">a </span></div><?
								/*
								?><div class="goods__add-btn"><a href="/ajax/add2cart.php?pid=<?=$item['INFO']['ID']?>" class="btn" data-id="<?=$item['INFO']['ID']?>" data-price="<?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>" onclick="yaCounter24799106.reachGoal('basket_click' ); ga ('send','pageview','/basket_click'); return true;">В корзину</a></div><?
								*/
								?><div class="goods__add-btn"><a href="javascript:void(0)" class="btn" data-id="<?=$item['INFO']['ID']?>" data-price="<?=$item['INFO']['PROPERTY_IMPORT_ELEMENT_PROPERTY_PRICE_VALUE']?>" onclick="yaCounter24799106.reachGoal('basket_click' ); ga ('send','pageview','/basket_click'); return true;">В корзину</a></div><?
							?></div><?
							if(!empty($item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE'])) {
								?><div class="goods__num"><?=$item['INFO']['PROPERTY_NUM_OF_PIECES_VALUE'].' шт.'?></div><?
							}
						?></div>
					</div>
				</li><?
			}
		?></ul><?

		/**
		 * Постраничная навигация
		 */
		$arTmpQueryParams = array();
		$arTmpQueryParams['nofilter'] = 'yes';
		if(isset($_REQUEST['q']) && strlen(trim($_REQUEST['q']))) {
			$arTmpQueryParams['q'] = htmlspecialcharsbx(trim($_REQUEST['q']));
		}
		if($_SESSION['NIYAMA']['FASET'] && is_array($_SESSION['NIYAMA']['FASET'])) {
			foreach($_SESSION['NIYAMA']['FASET'] as $sTmpParamName => $arTmpParamValue) {
				if($arTmpParamValue) {
					$arTmpQueryParams[$sTmpParamName] = $arTmpParamValue;
				}
			}
		}
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.catalog-pagenav',
			array(
				'CURRENT_PAGE' => $iCurrentPage,
				'ITEMS_ALL_CNT' => $iAllCount,
				'PER_PAGE' => $iPerPage,
				'AJAX_REQUEST_TYPE' => 'Y',
				'ADD_QUERY_PARAMS' => $arTmpQueryParams ? http_build_query($arTmpQueryParams) : '',
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	}

	if(empty($arDisplayProducts)) {
		?><h2 style="margin: 50px 0;">По вашему запросу блюд не найдено</h2><?
		?><script type="text/javascript">jQuery('#home-page-footer-description').hide();</script><?
	} else {
		?><script type="text/javascript">jQuery('#home-page-footer-description').show();</script><?
	}

?></div></noindex><?

if($sSeoDescription) {
	?><div class="description_main"><?=$sSeoDescription?></div><?
}


if($bAjaxRequest) {
	$sProducts = ob_get_clean();
}

if($bAjaxRequest) {
	$arJson = array();
	if($bNofilter) {
		$arJson = array(
			'products' => $sProducts,
		);
	} else {

		ob_start();

		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.general-menu',
			array(
				'MODE' => 'AJAX',
				'ISSET' => $GLOBALS['ALL_AJAX_MENU']
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

		// Ингредиенты
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.general-ingredients-menu',
			array(
				'MENU_TITLE' => 'Ингредиенты',
				'ISSET' => $GLOBALS['ALL_AJAX_ING']
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		// Исключения
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.general-ex-ingredients-menu',
			array(
				'MENU_TITLE' => 'Исключения',
				'ISSET' => $GLOBALS['ALL_AJAX_EX_ING']
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		$sFilters = ob_get_clean();

		$arJson = array(
			'products' => $sProducts,
			'filters' => $sFilters,
		);
	}

	$GLOBALS['APPLICATION']->RestartBuffer();
	echo json_encode($arJson);
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Новости (список)
//

?><h1 class="page__title"><?=$arParams['~PAGE_TITLE']?></h1><?
?><div class="news"><?
	// фильтр по годам
	$GLOBALS['APPLICATION']->IncludeComponent(
		'adv:iblock_elements.calendar',
		'niyama.1.0.news-filter',
		array(
			'PAGE_BASE_URL' => '/about/news/',
			'DATE_FILTER_FROM' => '',
			'DATE_FILTER_TO' => '',
			'CACHE_TYPE' => 'A',
			'CACHE_TIME' => '43200',
			'GROUP_TYPE' => 'YEAR',
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('news', 'misc'),
			'INCLUDE_TEMPLATE' => 'Y',
			'CACHE_TEMPLATE' => 'N'
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);

	if(!empty($GLOBALS['arNewsCityFilterEx'])) {
		// фильтр по городам/ресторанам
		$GLOBALS['APPLICATION']->IncludeComponent(
			'niyama:news.city-filter',
			'niyama.1.0',
			array(
				'NEWS_IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('news', 'misc'),
				'ELEMENT_FILTER_NAME' => 'arNewsCityFilterEx',
				'CACHE_TYPE' => 'A',
				'CACHE_TIME' => '43200',
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	}

	if(!empty($GLOBALS['arNewsListFilterEx'])) {
		// лента
		CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.iblock_data_list',
			'niyama.1.0.news-list',
			array(
				'CACHE_TYPE' => 'A',
				'CACHE_TIME' => '43200',
				'ELEMENT_CNT' => 20,
				'PAGER_SHOW' => 'Y',
				'ELEMENT_FILTER_NAME' => 'arNewsListFilterEx',
				'SORT_BY1' => 'DATE_ACTIVE_FROM',
				'SORT_ORDER1' => 'DESC',
				'SORT_BY2' => 'SORT',
				'SORT_ORDER2' => 'ASC',
				'IBLOCKS' => array(CProjectUtils::GetIBlockIdByCode('news', 'misc')),
				'FIELD_CODE' => array(
					'ID', 'NAME', 'CODE', 'PREVIEW_TEXT', 'DATE_ACTIVE_FROM', 'DETAIL_PAGE_URL',
					'PROPERTY_*',
				),
				'KEY_FIELD' => '',
				'INCLUDE_TEMPLATE' => 'Y',
				'CACHE_TEMPLATE' => 'Y',
				'GET_NEXT_ELEMENT_MODE' => 'Y'
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
	} else {
		ShowError('По запросу ничего не найдено');
	}

?></div><?

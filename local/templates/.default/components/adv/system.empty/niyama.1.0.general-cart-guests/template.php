<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();


# Если не установлен флаг вывода все для всех
$bGetAllCart = (isset($_REQUEST['all']))? true : false;

# Получаем ID Гостя.
$iGuestID = (isset($_REQUEST['gid'])) ? $_REQUEST['gid'] : false;
if (!$iGuestID){
    $iGuestIDTmp = CNiyamaCart::getGuestID();
    $iGuestID = ($iGuestIDTmp) ? $iGuestIDTmp : false;
} else {
    $iGuestIDTmp = CNiyamaCart::getGuestID();
    if ( (!$iGuestIDTmp) || ($iGuestID != $iGuestIDTmp) ) {
        $iGuestID = ($iGuestID!="current")? $iGuestID : false;
        CNiyamaCart::setGuestID($iGuestID);
    }
}



# Если мы не хотим получать все для всех
if (!$bGetAllCart) {

    # Если смотрим текущего пользователя
    if (!$iGuestID) {
        # Если у него есть товары
        if ($bIsCurrent){

            if (count($arCartItems['current']['ITEMS']) > 0):
                foreach ($arCartItems['current']['ITEMS'] as $prod):

                    $GLOBALS['APPLICATION']->IncludeFile(
                        $APPLICATION->GetTemplatePath("includes/cart/item.php"),
                        array(
                            "ITEM" => $prod
                        ),
                        array(
                            'MODE' => 'html',
                            'SHOW_BORDER' => false
                        )
                    );

                endforeach;
            endif;
        }
        # Если смотрим гостя
    } else {
        # Если есть товары
        if ($bIsGuest){
            $bIsNeedleGuest    = (isset($arCartItems['guest'][$iGuestID]))? true : false;

            # Если есть тот кто нам нужен
            if ($bIsNeedleGuest){
                if (count($arCartItems['guest'][$iGuestID]['ITEMS']) > 0):
                    foreach ($arCartItems['guest'][$iGuestID]['ITEMS'] as $prod):

                        $GLOBALS['APPLICATION']->IncludeFile(
                            $APPLICATION->GetTemplatePath("includes/cart/item.php"),
                            array(
                                "ITEM" => $prod
                            ),
                            array(
                                'MODE' => 'html',
                                'SHOW_BORDER' => false
                            )
                        );

                    endforeach;
                endif;
            }

        }

    }

# Если установили флаг all
} else {

    $arReturnList = array();
    if ($bIsCurrent){
        if (count($arCartItems['current']['ITEMS']) > 0){
            foreach ($arCartItems['current']['ITEMS'] as $prod){
                $arReturnList[] = $prod;
            }
        }
    }
    if ($bIsGuest){
        if (count($arCartItems['guest']) > 0){
            foreach ($arCartItems['guest'] as $arGuest){
                if (count($arGuest['ITEMS'])>0){
                    foreach ($arGuest['ITEMS'] as $prod){
                        $arReturnList[] = $prod;
                    }
                }
            }
        }
    }

    # Выводим
    if (count($arReturnList) > 0):
        foreach ($arReturnList as $prod):

            $GLOBALS['APPLICATION']->IncludeFile(
                $APPLICATION->GetTemplatePath("includes/cart/item.php"),
                array(
                    "ITEM" => $prod
                ),
                array(
                    'MODE' => 'html',
                    'SHOW_BORDER' => false
                )
            );

        endforeach;
    endif;
}

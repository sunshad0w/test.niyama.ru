<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

//
// ИНГРЕДИЕНТЫ
//

// Из настроек получаем количество по-умолчанию
$iDefaultCount = intval(CNiyamaCustomSettings::getStringValue('default_ingredient_count', 10));

if (isset($_REQUEST['ings'])) {
	$arIngredientsMenu = $_REQUEST['ings'];
} elseif(isset($_REQUEST['ing'])) {
	$arIngredientsMenu = $_REQUEST['ing'];
}
if (isset($_REQUEST['ex_ing'])) {
	$arExIngredientsMenu = $_REQUEST['ex_ing'];
}
$allingMenu = CNiyamaCatalog::getIngredients(
	null,
	array(
		'NAME' => trim($sQueryWord)
	)
);
if (!empty($alling)) {
	$arIngredientsMenu[] = $allingMenu[0]['ID'];
}

$arSort = array(
	'PROPERTY_IS_POPULAR' => 'DESC',
	'NAME' => 'ASC',
);
$arFilter = array(
	array(
		'LOGIC' => 'OR',
		array(
			'PROPERTY_IS_HIDDEN' => 1,
			'PROPERTY_IS_POPULAR' => 1,
		),
		array(
			'!PROPERTY_IS_HIDDEN' => 1,
		),
	)
);
$arSelect = array(
	'PROPERTY_IS_POPULAR',
	'PROPERTY_IS_HIDDEN'
);
$arMenuItems = CNiyamaCatalog::getIngredients($arSelect, $arFilter, $arSort);

if (!empty($arMenuItems)) {
	$i = 0;
	foreach ($arMenuItems as $item) {
		if ($iDefaultCount > 0 && ($i + 1) >= $iDefaultCount) {
			break;
		}
		if ($item['PROPERTY_IS_POPULAR_VALUE'] == 0) {
			$iDefaultCount = $i;
			break;
		}
		$i++;
	}

	$iCount = count($arMenuItems);
	$iCountCheck = ($iDefaultCount && $iDefaultCount < $iCount) ? ceil($iDefaultCount / 2) : ceil($iCount / 2);
	if (!empty($arParams['ISSET'])) {
		$activeMenuItems = array();
		$disabledMenuItems = array();
		foreach ($arMenuItems as $item) {
			if (in_array($item['ID'], $arParams['ISSET'])) {
				$activeMenuItems[] = $item;
			} else {
				$disabledMenuItems[] = $item;
			}
		}
		$arMenuItems = array_merge($activeMenuItems, $disabledMenuItems);
	}

	?><div class="filter__group clearfix" data-category="ing[]"><?
		?><div class="filter__title"><?=$arParams['~MENU_TITLE']?></div><?

		//
		// Элементы по умолчанию
		//
		?><div class="filter__col"><?
			$i = 1;
			foreach ($arMenuItems as $item) {

				$sDisabled = '';
				$bSet = (isset($_REQUEST['menu']));
				if(isset($arParams['ISSET']) && $bSet) {
					if (!empty($arParams['ISSET'])) {
						if (!in_array($item['ID'], $arParams['ISSET'])) {
							$sDisabled = ' _disabled';
						}
					} else {
						$sDisabled = ' _disabled';
					}
				}

				?><div data-filter="<?=$item['ID']?>" class="filter__item<?=((isset($arIngredientsMenu)) && (in_array($item['ID'], $arIngredientsMenu)) ? ' _selected' : '')?><?=((isset($arExIngredientsMenu)) && (in_array($item['ID'], $arExIngredientsMenu)) ? ' _excluded' : '')?>">
					<div class="filter__link__ing filter__link<?=$sDisabled?>">
						<span class="pseudo-link"><?=$item['NAME']?></span>
					</div>
				</div><?
				echo ($i == $iCountCheck)? '</div><div class="filter__col">' : '';
				$i++;
				if ($iDefaultCount > 0 && $i > $iDefaultCount) {
					break;
				}
			}
		?></div><?

		?><div class="clearfix"></div><?

		//
		// Скрытые элементы
		//
		if ($iDefaultCount && $iDefaultCount < $iCount) {
			$is_selected = false;
			ob_start();
			?><div class="filter__col"><?

				$iCountCheck = ceil(($iCount - $iDefaultCount) / 2);

				$i = 1;
				foreach ($arMenuItems as $item) {
					$sDisabled = '';
					$bSet = (isset($_REQUEST['menu']));
					if(isset($arParams['ISSET']) && $bSet) {
						if (!empty($arParams['ISSET'])) {
							if (!in_array($item['ID'], $arParams['ISSET'])) {
								$sDisabled = ' _disabled';
							}
						} else {
							$sDisabled = ' _disabled';
						}
					}

					if ($i > $iDefaultCount) {
						$selected = '';
						if ((isset($arIngredientsMenu) && (in_array($item['ID'],$arIngredientsMenu)))||(isset($arExIngredientsMenu) && (in_array($item['ID'],$arExIngredientsMenu)))) {$
							$selected = '_selected';
							$is_selected = true;
						}
						/*
						?><div data-filter="<?= $item['ID'] ?>" class="filter__item<?=((isset($arIngredientsMenu)) && (in_array($item['ID'],$arIngredientsMenu))? ' _selected' : '')?>">
							<div class="filter__link <?= $sDisabled ?>">
								<span class="pseudo-link"><?=$item['NAME']?></span>
								<a href="#" class="exclude__link"><span class="__exclude">Исключить</span><span class="__include">Не исключать</span></a>
							</div>
						</div><?
						*/
						?><div data-filter="<?=$item['ID']?>" class="filter__item<?=((isset($arIngredientsMenu)) && (in_array($item['ID'], $arIngredientsMenu)) ? ' _selected' : '')?><?=((isset($arExIngredientsMenu)) && (in_array($item['ID'], $arExIngredientsMenu)) ? ' _excluded' : '')?>">
							<div class="filter__link__ing filter__link<?=$sDisabled?>">
								<span class="pseudo-link"><?=$item['NAME']?></span>
							</div>
						</div><?
						echo ($i - $iDefaultCount == $iCountCheck)? '</div><div class="filter__col">' : '';
					}
					$i++;
				}
			?></div><?
			$sIng = ob_get_clean();

			?><div class="filter__hidden"<?=($is_selected) ? ' style="display: block;"' : ''?>><?
				echo $sIng;
				?><div class="clearfix"></div><?
			?></div><?

			?><div class="filter__item"><?
				?><a class="filter__link-more" href="#"><?
					?><span class="pseudo-link"<?=($is_selected) ? ' style="display: none;"' : ''?>>Все ингредиенты</span><?
					?><span class="pseudo-link hidden-bl"<?=($is_selected) ? ' style="display: inline;"' : ''?>>Скрыть</span><?
				?></a><?
			?></div><?
		}
	?></div><?
}

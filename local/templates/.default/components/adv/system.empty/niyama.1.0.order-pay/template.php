<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа - страница оплаты заказа (при оплате онлайн)
//

$iOrderId = intval($_REQUEST['id']);
if($iOrderId <= 0) {
	localredirect(SITE_DIR);
	return;
}

$arOrderData = CNiyamaOrders::GetOrderById($iOrderId, true);
if(!$arOrderData || $arOrderData['CAN_ACCESS'] != 'Y') {
	ShowError('Доступ запрещен');
	return;
}

$arPayments = CNiyamaOrders::GetPayments();
		
if(!$arOrderData['ORDER']['PAY_SYSTEM_ID'] || $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['IS_CASH'] == 'Y') {
	localredirect(SITE_DIR);
	return;
}

$bActionFile = false;
if($arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['ACTIVE'] == 'Y') {
	$dbItems = CSalePaySystemAction::GetList(
		array(),
		array(
			'PAY_SYSTEM_ID' => $arOrderData['ORDER']['PAY_SYSTEM_ID'],
			'PERSON_TYPE_ID' => $arOrderData['ORDER']['PERSON_TYPE_ID'],
		),
		false,
		false,
		array('ID', 'PAY_SYSTEM_ID', 'PERSON_TYPE_ID', 'NAME', 'ACTION_FILE', 'RESULT_FILE', 'NEW_WINDOW', 'PARAMS', 'ENCODING', 'LOGOTIP')
	);
	if($arPaySysAction = $dbItems->Fetch()) {
		CSalePaySystemAction::InitParamarrays($arOrderData['ORDER'], $arOrderData['ORDER']['ID'], $arPaySysAction['PARAMS']);
		$sActionPath = $_SERVER['DOCUMENT_ROOT'].$arPaySysAction['ACTION_FILE'].'/payment.php';
		if(file_exists($sActionPath)) {
			$bActionFile = true;
			include($sActionPath);
		}
	}
}

if(!$bActionFile) {
	ShowError('Оплата заказа выбранной платежной системой невозможна');
}

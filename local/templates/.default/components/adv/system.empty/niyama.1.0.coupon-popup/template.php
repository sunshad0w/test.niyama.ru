<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$text_money1 = '';
$text_money2 = '';
$str_sign = '';
$str_sign_err = '';
// этот значение передается фронтенд обработчику аякс-запроса для апдейта карточки купона в списке (блок div.coupon__title)
$arResult['sCouponTitleBlockHtml'] = '';
if (!empty($arParams['arResult'])) {
	$arResult = $arParams['arResult'];
	$koefficient_niam_by_rub = intval(CNiyamaCustomSettings::getStringValue('koefficient_niam_by_rub', 100));
	$dWithdrawVal = isset($arResult['BONUS_WITHDRAW_VAL']) ? doubleval($arResult['BONUS_WITHDRAW_VAL']) : 0;
	$iNewVal = $arResult['BONUS_VAL'] - $dWithdrawVal;
	if ($arResult['MONEY_TYPE'] == 'NIYAM') {
		$text_money1 = 'НИЯМ';
		$text_money2 = 'ниям';
		$max_discount_cfg = CNiyamaCustomSettings::getStringValue('max_discount_order_by_niam', 10);
		$str_sign = 'Не более '.$max_discount_cfg.'% от суммы заказа — '.CProjectUtils::FormatNum($arResult['MAX_USED']).' '.CProjectUtils::PrintCardinalNumberRus($arResult['MAX_USED'], 'ниям', 'нияма', 'ниямы').'<br />Значение должно быть кратно '.$koefficient_niam_by_rub;

		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title">';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-left">'.CProjectUtils::FormatNum($iNewVal).'</div>';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-right">'.CProjectUtils::PrintCardinalNumberRus($iNewVal, 'ниям', 'нияма', 'ниямы').'</div>';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-center"><div class="coupon__badge _nyam"></div></div>';
		$arResult['sCouponTitleBlockHtml'] .= '</div>';
	} elseif($arResult['MONEY_TYPE'] == 'MONEY') {
		$text_money1 = 'РУБЛЕЙ';
		$text_money2 = 'рублей';
		$max_discount_cfg = intval(CNiyamaCustomSettings::getStringValue('max_discount_by_order', 15));
		$str_sign = 'Не более '.$max_discount_cfg.'% от суммы заказа — '.CProjectUtils::FormatNum($arResult['MAX_USED']).' '.CProjectUtils::PrintCardinalNumberRus($arResult['MAX_USED'], 'рублей', 'рубль', 'рубля');

		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title">';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-left">'.CProjectUtils::FormatNum($iNewVal).'</div>';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-right">'.CProjectUtils::PrintCardinalNumberRus($iNewVal, 'рублей', 'рубль', 'рубля').'</div>';
		$arResult['sCouponTitleBlockHtml'] .= '<div class="coupon__title-center"><div class="coupon__badge _nyam"></div></div>';
		$arResult['sCouponTitleBlockHtml'] .= '</div>';
	}
	if($arResult['error']) {
		$str_sign_err = 'Произошла неизвестная ошибка, невозможно использовать бонус';
		if($arResult['coupon__count'] && $arResult['coupon__count'] > $arResult['MAX_USED']) {
			$str_sign_err = $str_sign;
		} elseif($arResult['error_msg']) {
			$str_sign_err = $arResult['error_msg'];
		}
	}
}



?><div onclick="return {'couponPopup': {}}" class="coupon-popup js-widget">
	<form action="/ajax/getCouponPopup.php">
		<input type="hidden" value="<?=$arResult['ID']?>" name="pid" />
		<p class="h1 popup__title">Бонусы купона</p>
		<div class="coupon _popup _style_1">
			<div class="coupon__title">
				<div class="coupon__title-left"><?=CProjectUtils::FormatNum($arResult['MONEY'])?></div>
				<div class="coupon__title-right"><?=$text_money1?></div>
				<div class="coupon__title-center">
					<div class="coupon__badge _nyam"></div>
				</div>
			</div>
		</div>
		<label class="label">Сколько <?=$text_money2?> применить к корзине?</label><?
		if(isset($arResult['BONUS_WITHDRAW_VAL'])) {
			$sPrnVal = htmlspecialcharsbx($arResult['BONUS_WITHDRAW_VAL']);
		} else {
			$sPrnVal = $arResult['MAX_USED'] > $arResult['BONUS_VAL'] ? $arResult['BONUS_VAL'] : $arResult['MAX_USED'];
		}

		if($arResult['error']) {
			?><input type="text" name="coupon__count" value="<?=$sPrnVal?>" class="input _error _mb_3" />
			<div class="coupon-popup__sign _error"><?=$str_sign_err?></div><?
		} else {
			?><input type="text" name="coupon__count" value="<?=$sPrnVal?>" class="input _mb_3" /><?
			?><div class="coupon-popup__sign"><?=$str_sign?></div><?
		}

		?><input value="Применить" type="submit" class="btn _style_2 _full-width" />
	</form>
</div><?


<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$GLOBALS['APPLICATION']->RestartBuffer();

?><html>
<head></head>
<body>
<script type="text/javascript">
    var Callback = {
        redirect: function(localtion) {
            window.opener.location.replace(localtion);
            window.close();
        }
    };
    Callback.<?=$arResult['method']?>("<?=join('","', $arResult['params'])?>");
</script>
</body>
</html><?

die;
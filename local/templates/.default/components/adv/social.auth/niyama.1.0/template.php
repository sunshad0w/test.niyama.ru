<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

?><div class="social _auth">
    <div class="_facebook social__item" data-site="facebook"></div>
    <div class="_vk social__item" data-site="vkontakte"></div>
    <div class="_google social__item" data-site="googleplus"></div>
    <div class="_mail social__item" data-site="mailru"></div>
    <div class="_ya social__item" data-site="yandex"></div>
</div><?


?><script type="text/javascript">
	<?
	if($arResult['SOCIAL_PARAMS'] && (!$arParams['ADD_HEAD_SCRIPT_DEFAULT'] || $arParams['ADD_HEAD_SCRIPT_DEFAULT'] != 'Y')) {
		?>
		var SOCIAL_SITES_PARAMS = <?=json_encode($arResult['SOCIAL_PARAMS'])?>;
		<?
	}
	?>
	function openWindow(url, winWidth, winHeight) {
		var winTop = (screen.height / 2) - (winHeight / 2);
		var winLeft = (screen.width / 2) - (winWidth / 2);
		window.open(url, '', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	}

	// ����������� � ����������� �� ��� ����
	jQuery('.social._auth').on(
		'click', 
		'.social__item', 
		function() {
			var that = this;
			var site = jQuery(this).data('site').trim();
			objSite = SOCIAL_SITES_PARAMS[site];
			openWindow(objSite.DIALOG_URL, objSite.WIDTH, objSite.HEIGHT);
		}
	);
</script><?

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// ошибка запроса авторизации
//

// запишем в лог ошибку
if($arResult['EXCEPTION_OBJECT']) {
	$obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/soc_auth_err.log');
	$obCustomLog->Log($arResult['EXCEPTION_MESSAGE'] ? array('MESSAGE' => $arResult['EXCEPTION_MESSAGE'], 'REQUEST_URI' => $_SERVER['REQUEST_URI']) : array('MESSAGE' => 'Неизвестная ошибка', 'REQUEST_URI' => $_SERVER['REQUEST_URI']));
}

ADV_set_flashdata('auth_error', 'Во время авторизации произошла неизвестная ошибка, пожалуйста, попробуйте позже или обратитесь к администрации сайта');

$GLOBALS['APPLICATION']->RestartBuffer();

?><html>
	<head></head>
	<body>
		<script type="text/javascript">
			window.opener.location.replace('/auth/?register=yes');
			window.close();
		</script>
	</body>
</html><?

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
if(empty($arResult['ITEMS'])) {
	return;
}

$arItem = reset($arResult['ITEMS']);
$GLOBALS['APPLICATION']->SetTitle($arItem['NAME']);
$GLOBALS['APPLICATION']->SetPageProperty('description', $arItem['PREVIEW_TEXT']);
$GLOBALS['APPLICATION']->AddChainItem($arItem['NAME'], $arItem['DETAIL_PAGE_URL']);

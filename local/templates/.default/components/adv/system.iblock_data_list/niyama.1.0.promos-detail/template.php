<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Акция детально
//
//_show_array($arResult);

if(empty($arResult['ITEMS'])) {
	ShowError('По вашему запросу ничего не найдено');
	@define('ERROR_404', 'Y');
	return;
}

$iCurYear = date('Y');
$arItem = reset($arResult['ITEMS']);
if($arItem) {
	?><h1 class="post__title _one"><?=$arItem['NAME']?></h1><?

	$iTimestamp = $arItem['DATE_ACTIVE_FROM'] ? MakeTimeStamp($arItem['DATE_ACTIVE_FROM']) : 0;
	$sTmpClass = '';
	$sTmpJs = '';
	if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
		$sTmpJs .= ' onclick="return {\'post\': {}}"';
		$sTmpClass .= ' js-widget';
	}
	if($arItem['PROPERTIES']['B_MARK']['VALUE']) {
		$sTmpClass .= ' _birthday';
	}

	?><div<?=$sTmpJs?> class="post<?=$sTmpClass?>"><?
		if($iTimestamp) {
			$sFormat = 'j F';
			$iTmpYear = date('Y', $iTimestamp);
			if($iCurYear != $iTmpYear) {
				$sFormat = 'j F Y';
			}
			?><p class="post__date _one"><?=ToLower(FormatDate($sFormat, $iTimestamp))?></p><?
		}
		?><div class="post__desc"><?
			echo $arItem['PREVIEW_TEXT'];
		?></div><?
		if($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) {
			$sTmpClass = '';
			if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
				$sTmpClass .= ' _carousel';
			}
			?><div class="post__img<?=$sTmpClass?>"><?
				foreach($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] as $iTmpImgId) {
					$arTmpImg = CFile::ResizeImageGet(
						$iTmpImgId,
						array(
							'width' => 646,
							'height' => 318
						),
						BX_RESIZE_IMAGE_EXACT,
						true
					);
					if($arTmpImg['src']) {
						?><img src="<?=$arTmpImg['src']?>" width="<?=$arTmpImg['width']?>" height="<?=$arTmpImg['height']?>" alt="" /><?
					}
				}
			?></div><?
		}
		?><div class="post__desc"><?
			echo $arItem['DETAIL_TEXT'];
		?></div><?

		?><div class="post__share"><?
			$GLOBALS['APPLICATION']->IncludeComponent(
				'bitrix:main.share',
				'',
				array(
					'PAGE_URL' => htmlspecialcharsback($arItem['DETAIL_PAGE_URL']),
					'PAGE_TITLE' => htmlspecialcharsback($arItem['NAME']),
				),
				$component,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		?></div><?

		?><div class="post__back"><a href="<?=$arItem['LIST_PAGE_URL']?>" class="post__back-link">Все акции</a></div><?
	?></div><?
}

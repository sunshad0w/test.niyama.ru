<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Новость детально
//
//_log_array($arResult);
if(empty($arResult['ITEMS'])) {
	ShowError('По вашему запросу ничего не найдено');
	@define('ERROR_404', 'Y');
	return;
}

$iCurYear = date('Y');
$arItem = reset($arResult['ITEMS']);
if($arItem) {
	?><h1 class="post__title _one"><?=$arItem['NAME']?></h1><?

	$iTimestamp = $arItem['DATE_ACTIVE_FROM'] ? MakeTimeStamp($arItem['DATE_ACTIVE_FROM']) : 0;
	$sTmpClass = '';
	$sTmpJs = '';
	if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
		$sTmpJs .= ' onclick="return {\'post\': {}}"';
		$sTmpClass .= ' js-widget';
	}
	if($arItem['PROPERTIES']['B_MARK']['VALUE']) {
		$sTmpClass .= ' _birthday';
	}

	?><div<?=$sTmpJs?> class="post<?=$sTmpClass?>"><?
		if($iTimestamp) {
			$sFormat = 'j F';
			$iTmpYear = date('Y', $iTimestamp);
			if($iCurYear != $iTmpYear) {
				$sFormat = 'j F Y';
			}
			$sAddr = '';
			if($arItem['PROPERTIES']['R_RESTAURANT']['VALUE'] && count($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']) == 1) {
				$iTmpId = reset($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']);
				if($arResult['RESTAURANTS'][$iTmpId]) {
					$sAddr = rtrim(', '.$arResult['RESTAURANTS'][$iTmpId]['NAME'].', '.$arResult['RESTAURANTS'][$iTmpId]['PROPERTY_S_ADDRESS_VALUE'], ', ');
				}
			}
			?><p class="post__date _one"><?=ToLower(FormatDate($sFormat, $iTimestamp)).$sAddr?></p><?
		}
		?><div class="post__desc"><?
			echo $arItem['PREVIEW_TEXT'];
		?></div><?
		$sSocImgSrc = '';
		if($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) {
			$sTmpClass = '';
			if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
				$sTmpClass .= ' _carousel';
			}
			?><div class="post__img<?=$sTmpClass?>"><?
				foreach($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] as $iTmpImgId) {
					$arTmpImg = CFile::ResizeImageGet(
						$iTmpImgId,
						array(
							'width' => 646,
							'height' => 318
						),
						BX_RESIZE_IMAGE_EXACT,
						true
					);
					if($arTmpImg['src']) {
						if(!$sSocImgSrc) {
							$sSocImgSrc = $arTmpImg['src'];
						}
						?><img src="<?=$arTmpImg['src']?>" width="<?=$arTmpImg['width']?>" height="<?=$arTmpImg['height']?>" alt="" /><?
					}
				}
			?></div><?
		}
		?><div class="post__desc"><?
			echo $arItem['DETAIL_TEXT'];
		?></div><?

		?><div class="post__share"><?
			$APPLICATION->IncludeComponent(
				'bitrix:asd.share.buttons',
				'niyama.1.0',
				array(
					'ASD_ID' => '',
					'ASD_TITLE' => $arItem['NAME'],
					'ASD_URL' => 'http://'.SITE_SERVER_NAME.$arItem['DETAIL_PAGE_URL'],
					'ASD_PICTURE' => strlen($sSocImgSrc) ? 'http://'.SITE_SERVER_NAME.$sSocImgSrc : '',
					'ASD_TEXT' => '',
					'ASD_LINK_TITLE' => 'Поделиться в #SERVICE#',
					'ASD_SITE_NAME' => 'Нияма',
					'ASD_INCLUDE_SCRIPTS' => array('FB_LIKE', 'VK_LIKE', 'TWITTER'),
					'LIKE_TYPE' => 'LIKE',
					'VK_API_ID' => CNiyamaCustomSettings::GetStringValue('vk_api_id', ''),
					'VK_LIKE_VIEW' => 'full',
					'TW_DATA_VIA' => '',
					'SCRIPT_IN_HEAD' => 'N'
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		?></div><?

		?><div class="post__back"><a href="<?=$arItem['LIST_PAGE_URL']?>" class="post__back-link">Все новости</a></div><?
	?></div><?
}

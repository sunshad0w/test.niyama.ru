<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Список новостей
//
//_show_array($arResult);

if(empty($arResult['ITEMS'])) {
	ShowError('По вашему запросу ничего не найдено');
	return;
}

$iCurYear = date('Y');

?><div class="news__posts"><?
	foreach($arResult['ITEMS'] as $arItem) {
		$iTimestamp = $arItem['DATE_ACTIVE_FROM'] ? MakeTimeStamp($arItem['DATE_ACTIVE_FROM']) : 0;
		$sTmpClass = '';
		$sTmpJs = '';
		if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
			$sTmpJs .= ' onclick="return {\'post\': {}}"';
			$sTmpClass .= ' js-widget';
		}
		if($arItem['PROPERTIES']['B_MARK']['VALUE']) {
			$sTmpClass .= ' _birthday';
		}
		?><div<?=$sTmpJs?> class="post<?=$sTmpClass?>"><?
			?><h2 class="post__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="post__title-link"><?=$arItem['NAME']?></a></h2><?
			if($iTimestamp) {
				$sFormat = 'j F';
				$iTmpYear = date('Y', $iTimestamp);
				if($iCurYear != $iTmpYear) {
					$sFormat = 'j F Y';
				}
				$sAddr = '';
				if($arItem['PROPERTIES']['R_RESTAURANT']['VALUE'] && count($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']) == 1) {
					$iTmpId = reset($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']);
					if($arResult['RESTAURANTS'][$iTmpId]) {
						$sAddr = rtrim(', '.$arResult['RESTAURANTS'][$iTmpId]['NAME'].', '.$arResult['RESTAURANTS'][$iTmpId]['PROPERTY_S_ADDRESS_VALUE'], ', ');
					}
				}
				?><p class="post__date"><?=ToLower(FormatDate($sFormat, $iTimestamp)).$sAddr?></p><?
			}
			?><div class="post__desc"><?
				echo $arItem['PREVIEW_TEXT'];
			?></div><?
			if($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) {
				$sTmpClass = '';
				if($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] && count($arItem['PROPERTIES']['F_PHOTOS']['VALUE']) > 1) {
					$sTmpClass .= ' _carousel';
				}
				?><div class="post__img<?=$sTmpClass?>"><?
					foreach($arItem['PROPERTIES']['F_PHOTOS']['VALUE'] as $iTmpImgId) {
						$arTmpImg = CFile::ResizeImageGet(
							$iTmpImgId,
							array(
								'width' => 646,
								'height' => 318
							),
							BX_RESIZE_IMAGE_EXACT,
							true
						);
						if($arTmpImg['src']) {
							?><img src="<?=$arTmpImg['src']?>" width="<?=$arTmpImg['width']?>" height="<?=$arTmpImg['height']?>" alt="" /><?
						}
					}
				?></div><?
			}
		?></div><?
	}

	if($arResult['NAV_STRING']) {
		echo $arResult['NAV_STRING'];
	}
?></div><?

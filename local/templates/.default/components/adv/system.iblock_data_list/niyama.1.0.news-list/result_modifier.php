<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Список новостей
//

if(empty($arResult['ITEMS'])) {
	return;
}

$arResult['RESTAURANTS'] = array();
$arTmpItems = array();
foreach($arResult['ITEMS'] as $arItem) {
	if($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']) {
		$arTmpItems = array_merge($arTmpItems, $arItem['PROPERTIES']['R_RESTAURANT']['VALUE']);
	}
}

if($arTmpItems) {
	$dbItems = CIBlockElement::GetList(
		array(),
		array(
			'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode('restaurants-list', 'restaurants'),
			'ID' => array_unique($arTmpItems)
		),
		false,
		false,
		array(
			'ID', 'NAME', 'PROPERTY_S_ADDRESS'
		)
	);
	while($arItem = $dbItems->Fetch()) {
		$arResult['RESTAURANTS'][$arItem['ID']] = $arItem;
	}
}

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Панель фильтра для раздела "Рестораны" + левый навигационный блок + ресторан детально
//

if(!$arResult['RESTAURANTS']) {
	return;
}

$iCurCity = $_REQUEST['city'] ? intval($_REQUEST['city']) : $arParams['CITY'];
$iCurSubway = $_REQUEST['subway'] ? intval($_REQUEST['subway']) : $arParams['SUBWAY'];
$sElementCode = isset($_REQUEST['ELEMENT_CODE']) ? trim($_REQUEST['ELEMENT_CODE']) : '';

// раскомментировать, если требуется менять выбранные селекты городов и метро при выбранном ресторане
/*
$arCurRestaurant = array();
if(strlen($sElementCode)) {
	foreach($arResult['RESTAURANTS'] as $arItem) {
		if($arItem['CODE'] == $sElementCode) {
			$arCurRestaurant = $arItem;
			break;
		}
	}
}
if($arCurRestaurant) {
	$iCurCity = $iCurCity ? $iCurCity : $arCurRestaurant['PROPERTIES']['R_CITY']['VALUE'];
	$iCurSubway = $iCurSubway ? $iCurSubway : $arCurRestaurant['PROPERTIES']['R_SUBWAY']['VALUE'];
}
*/

?><div onclick="return {'restsControls': {}}" class="rests-controls js-widget"><?
	if($arResult['CITIES']) {
		$arCities = array();
		$iMoscowId = 0;
		foreach($arResult['CITIES'] as $arItem) {
			if($arItem['_USED_'] == 'Y') {
				$arCities[$arItem['ID']] = $arItem;
				if($arItem['NAME'] == 'Москва') {
					$iMoscowId = $arItem['ID'];
				}
			}
		}
		$iCurCity = $iCurCity > 0 && $arCities[$iCurCity] ? $iCurCity : $iMoscowId;

		if($arCities) {
			?><div class="rests-controls__city">
				<div onclick="return {'select': {}}" class="select__wrap js-widget">
					<select class="select _city" name="city"><?
						if($iMoscowId) {
							$sTmpSelected = $iCurCity == $iMoscowId ? ' selected="selected"' : '';
							?><option<?=$sTmpSelected?> data-city="REGION_1" value="<?=$iMoscowId?>">Москва</option><?
							unset($arCities[$iMoscowId]);
						}
						if($arCities) {
							foreach($arCities as $arItem) {
								$sTmpSelected = $iCurCity == $arItem['ID'] ? ' selected="selected"' : '';
								?><option<?=$sTmpSelected?> data-city="REGION_X" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
							}
						}
					?></select>
				</div>
			</div><?
		}
	}

	if($iCurCity && $arResult['SUBWAY']) {
		$arSubway = array();
		foreach($arResult['SUBWAY'] as $arItem) {
			if($arItem['_USED_'] == 'Y') {
				$arSubway[$arItem['ID']] = $arItem;
			}
		}
		$iCurSubway = $iCurSubway > 0 && $arSubway[$iCurSubway] ? $iCurSubway : 0;

		if($arSubway) {
			?><div class="rests-controls__station">
				<div onclick="return {'select': {}}" class="select__wrap js-widget<?=!isset($arResult['SUBWAY2CITY'][$iCurCity])?' _hidden':''?>">
					<select class="select _metro" name="subway"><?
						?><option value="0">Все станции метро</option><?
						foreach($arSubway as $arItem) {
							$sTmpSelected = $iCurSubway == $arItem['ID'] ? ' selected="selected"' : '';
							?><option<?=$sTmpSelected?> value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
						}
					?></select>
				</div>
			</div><?
		}
	}

	if($arResult['SERVICES']) {
		?><div class="rests-controls__filter"><?
			foreach($arResult['SERVICES'] as $arItem) {
				$sDisabled = '';
				if($arItem['_USED_'] != 'Y' || !$arResult['SERVICES2CITY'][$iCurCity][$arItem['ID']]) {
					$sDisabled = ' disabled="disabled"';
				}
				?><div id="<?=$arItem['CODE'].'Wrap'?>" class="rests-controls__filter-wrap">
					<input<?=$sDisabled?> type="checkbox" name="<?=$arItem['CODE']?>" value="<?=$arItem['ID']?>" id="<?=$arItem['CODE']?>" class="checkbox">
					<label for="<?=$arItem['CODE']?>" class="checkbox-label"><?
						echo $arItem['NAME'];
						if($arItem['PREVIEW_PICTURE']['SRC']) {
							?><div class="rests-controls__filter-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" width="<?=$arItem['PREVIEW_PICTURE']['WIDTH']?>" height="<?=$arItem['PREVIEW_PICTURE']['HEIGHT']?>" alt=""></div><?
						}
					?></label>
				</div><?
			}
		?></div><?
	}

?></div><?

//
// блок меню слева
//
$arRestaurants = array();
foreach($arResult['RESTAURANTS'] as $arItem) {
	$arRestaurants[$arItem['IBLOCK_SECTION_ID']][$arItem['ID']] = $arItem;
}
ob_start();
$sTmpUl = '';
$sTmpUlClose = '';
foreach($arResult['GROUPS'] as $arItem) {
	if($arItem['DEPTH_LEVEL'] == '1') {
		$sTmpUl = '';
		if(strlen($sElementCode)) {
			$sTmpUl .= $sTmpUlClose.'<label class="label"><a href="/restaurants/">'.$arItem['NAME'].'</a></label>';
		} else {
			$sTmpUl .= $sTmpUlClose.'<label class="label">'.$arItem['NAME'].'</label>';
		}
		$sTmpUl .= '<ul class="nav-side">';
		$sTmpUlClose = '</ul>';
	}
	if($arRestaurants[$arItem['ID']]) {
		echo $sTmpUl;
		$sTmpUl = '';
		foreach($arRestaurants[$arItem['ID']] as $arRestaurantItem) {
			$sTmpClass = $sElementCode && $sElementCode == $arRestaurantItem['CODE'] ? ' _active' : '';
			?><li class="nav-side__item"><a data-rid="<?=$arRestaurantItem['ID']?>" href="<?=$arRestaurantItem['DETAIL_PAGE_URL']?>" class="nav-side__link<?=$sTmpClass?>"><span><?=$arRestaurantItem['NAME']?></span></a></li><?
		}
	}
}
if(!strlen($sTmpUl)) {
	echo $sTmpUlClose;
}
$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_RESTAURANTS_NAV', ob_get_clean());


//
// карточка ресторана
//
ob_start();
$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.restaurants-item',
	array(
		'arResult' => $arResult,
		'sElementCode' => $sElementCode
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);
$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_RESTAURANTS_DETAIL', ob_get_clean());

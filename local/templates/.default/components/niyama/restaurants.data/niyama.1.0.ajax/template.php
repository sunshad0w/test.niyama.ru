<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Ajax-скрипт для раздела "Рестораны"
//

$iRestaurantId = isset($_REQUEST['rid']) ? intval($_REQUEST['rid']) : 0;
//$iRestaurantId = 0;

$arJson = array();
if($arResult['RESTAURANTS']) {
	$arDepartments = CNiyamaIBlockDepartments::GetAllDepartments();
	if($iRestaurantId) {
		// карточка ресторана
		$arJson['rest'] = array();
		$arItem = isset($arResult['RESTAURANTS'][$iRestaurantId]) ? $arResult['RESTAURANTS'][$iRestaurantId] : array();
		$sElementCode = isset($arItem['CODE']) ? $arItem['CODE'] : '';
		if($arItem) {
			$iDepId = intval($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']);
			$arCurDep = isset($arDepartments[$iDepId]) && $arDepartments[$iDepId]['ACTIVE'] == 'Y' ? $arDepartments[$iDepId] : array();
			$iLat = '0';
			$iLong = '0';
			if($arCurDep) {
				if($arCurDep['PROPERTY_LAT']) {
					$iLat = $arCurDep['PROPERTY_LAT'];
					$iLong = $arCurDep['PROPERTY_LONG'];
				} elseif($arCurDep['PROPERTY_LAT_ALT']) {
					$iLat = $arCurDep['PROPERTY_LAT_ALT'];
					$iLong = $arCurDep['PROPERTY_LONG_ALT'];
				}
			}
			$arJson['rest'] = array(
				'lat' => $iLat,
				'long' => $iLong,
				'title' => $arItem['NAME'],
				'city_id' => intval($arItem['PROPERTIES']['R_CITY']['VALUE']),
				'subway_id' => intval($arItem['PROPERTIES']['R_SUBWAY']['VALUE']),
				'services' => $arItem['PROPERTIES']['R_SERVICES']['VALUE'] && is_array($arItem['PROPERTIES']['R_SERVICES']['VALUE']) ? $arItem['PROPERTIES']['R_SERVICES']['VALUE'] : array(),
			);
		}
		ob_start();
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.restaurants-item',
			array(
				'arResult' => $arResult,
				'sElementCode' => $sElementCode
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		$arJson['html'] = ob_get_clean();
	} else {
		// главная страница раздела
		$arJson['rests'] = array();
		foreach($arResult['RESTAURANTS'] as $arItem) {
			$iDepId = intval($arItem['PROPERTIES']['R_RESTAURANT']['VALUE']);
			$arCurDep = isset($arDepartments[$iDepId]) && $arDepartments[$iDepId]['ACTIVE'] == 'Y' ? $arDepartments[$iDepId] : array();
			$iLat = '0';
			$iLong = '0';
			if($arCurDep) {
				if($arCurDep['PROPERTY_LAT']) {
					$iLat = $arCurDep['PROPERTY_LAT'];
					$iLong = $arCurDep['PROPERTY_LONG'];
				} elseif($arCurDep['PROPERTY_LAT_ALT']) {
					$iLat = $arCurDep['PROPERTY_LAT_ALT'];
					$iLong = $arCurDep['PROPERTY_LONG_ALT'];
				}
			}
			$arJson['rests'][$arItem['ID']] = array(
				'lat' => $iLat,
				'long' => $iLong,
				'title' => $arItem['NAME'],
				'city_id' => intval($arItem['PROPERTIES']['R_CITY']['VALUE']),
				'subway_id' => intval($arItem['PROPERTIES']['R_SUBWAY']['VALUE']),
				'services' => $arItem['PROPERTIES']['R_SERVICES']['VALUE'] && is_array($arItem['PROPERTIES']['R_SERVICES']['VALUE']) ? $arItem['PROPERTIES']['R_SERVICES']['VALUE'] : array(),
			);
		}

		//
		// !!! костыль, чтобы отображались корректно иконки редактирования при загрузке через аякс !!!
		//
		if($GLOBALS['USER']->IsAuthorized() && $GLOBALS['APPLICATION']->GetShowIncludeAreas()) {
			if(!$GLOBALS['APPLICATION']->editArea) {
				$GLOBALS['APPLICATION']->editArea = new CEditArea();
				$GLOBALS['APPLICATION']->editArea->includeAreaIndex[0] = 999;
			} elseif(is_a($GLOBALS['APPLICATION']->editArea, 'CEditArea')) {
				$GLOBALS['APPLICATION']->editArea->includeAreaIndex[0] = 999;
			}
		}

		// компонент также подгружается в adv:system.empty - niyama.1.0.restaurants
		ob_start();
		$GLOBALS['APPLICATION']->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.restaurants-index-info',
			array(),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);
		$arJson['html'] = ob_get_clean();
	}
}

$GLOBALS['APPLICATION']->RestartBuffer();
header('Content-type: application/json');

echo json_encode($arJson);

// это вместо эпилога
CProjectUtils::AjaxEpilog();
die();

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Вакансии: фильтр по городам
//
//_show_array($arResult);

if(empty($arResult['ITEMS'])) {
	return;
}

$arFirst = reset($arResult['ITEMS']);
$iActiveId = 0;
if(isset($_REQUEST['CITY_ID'])) {
	$iActiveId = intval($_REQUEST['CITY_ID']) > 0 ? intval($_REQUEST['CITY_ID']) : -1;
}
//if(!$iActiveId || !$arResult['ITEMS'][$iActiveId] || !$arResult['ITEMS'][$iActiveId]['ELEMENT_CNT']) {
if(!$iActiveId || ($arResult['ITEMS'][$iActiveId] && !$arResult['ITEMS'][$iActiveId]['ELEMENT_CNT'])) {
	$iActiveId = $arFirst['ID'];
}

?><div class="cities"><?
	$sTmp = 'Вакансии в ';
	foreach($arResult['ITEMS'] as $arItem) {
		$sActive = $arItem['ID'] == $iActiveId ? ' _active' : '';
		$sName = strlen($arItem['UF_PP']) ? $arItem['UF_PP'] : $arItem['NAME'];
		if($arItem['ELEMENT_CNT']) {
			$sPageUrl = $arItem['ID'] == $arFirst['ID'] ? $arItem['LIST_PAGE_URL'] : $arItem['SECTION_PAGE_URL'];
			?><a href="<?=$sPageUrl?>" class="cities__item<?=$sActive?>"><span><?=$sTmp.$sName?></span></a><?
		} else {
			?><span class="cities__item<?=$sActive?>"><span><?=$sTmp.$sName?></span></span><?
		}
		$sTmp = '';
	}
?></div><?

$GLOBALS['NIYAMA_CITY_LEVEL_SECTION_ID'] = $iActiveId;

if($arResult['ITEMS'][$iActiveId]) {
	$sName = strlen($arResult['ITEMS'][$iActiveId]['UF_PP']) ? $arResult['ITEMS'][$iActiveId]['UF_PP'] : $arResult['ITEMS'][$iActiveId]['NAME'];
	$GLOBALS['APPLICATION']->SetTitle('Вакансии в '.$sName);
}

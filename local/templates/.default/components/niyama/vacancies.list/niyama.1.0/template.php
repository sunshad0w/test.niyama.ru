<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Вакансии: список вакансий
//

if(empty($arResult['CITY'])) {
	return;
}

if($arParams['RESTAURANT_LEVEL_SECTION_ID'] && !$arResult['RESTAURANTS'][$arParams['RESTAURANT_LEVEL_SECTION_ID']]) {
	ShowError('Неверно указан ресторан');
	@define('ERROR_404', 'Y');
	return;
}


if(empty($arResult['ITEMS'])) {
	if($arParams['RESTAURANT_LEVEL_SECTION_ID']) {
		ShowError('Сейчас нет открытых вакансий для ресторана');
	} else {
		$sName = $arResult['CITY']['UF_PP'] ? $arResult['CITY']['UF_PP'] : $arResult['CITY']['NAME'];
		ShowError('Сейчас нет открытых вакансий в '.$sName);
	}
	return;
}


$arUsedRestaurants = array();
foreach($arResult['ITEMS_SECTIONS'] as $arTmp) {
	$arUsedRestaurants = array_merge($arTmp, $arUsedRestaurants);
}

if(count($arResult['RESTAURANTS']) > 1) {
	?><div onclick="return {'vacancies': {}}" class="vacancies__select js-widget">
		<div onclick="return {'select': {}}" class="select__wrap js-widget">
			<select class="select"><?
				$iSelectedRestaurantId = $arParams['RESTAURANT_LEVEL_SECTION_ID'] ? intval($arParams['RESTAURANT_LEVEL_SECTION_ID']) : $arResult['CITY']['ID'];
				$sName = $arResult['CITY']['UF_RP'] ? $arResult['CITY']['UF_RP'] : $arResult['CITY']['NAME'];
				?><option value="0">Все рестораны <?=$sName?></option><?
				foreach($arResult['RESTAURANTS'] as $arItem) {
					if(in_array($arItem['ID'], $arUsedRestaurants)) {
						$sTmpSelected = $arItem['ID'] == $iSelectedRestaurantId ? ' selected="selected"' : '';
						?><option<?=$sTmpSelected?> value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
					}
				}
			?></select>
		</div>
	</div><?
}

foreach($arResult['ITEMS'] as $arGroupedItems) {
	// общие поля для сгруппированых элементов: название + картинки
	$arCommon = reset($arGroupedItems);

	$arItems2Rest = array();
	foreach($arGroupedItems as $arItem) {
		if(!$arItem['IBLOCK_SECTION_ID']) {
			// в рестораны всех городов
			$arItems2Rest[0][] = $arItem['ID'];
		} elseif($arResult['ITEMS_SECTIONS'][$arItem['ID']]) {
			foreach($arResult['ITEMS_SECTIONS'][$arItem['ID']] as $iTmpSectionId) {
				if($arResult['RESTAURANTS'][$iTmpSectionId]) {
					// в указанные рестораны города
					$arItems2Rest[$iTmpSectionId][] = $arItem['ID'];
				} elseif($arResult['CITY']['ID'] == $iTmpSectionId) {
					// во все рестораны города
					$arItems2Rest[$iTmpSectionId][] = $arItem['ID'];
				}
			}
		}
	}
	$bAll = $arItems2Rest[0] || $arItems2Rest[$arResult['CITY']['ID']];
	if($bAll) {
		$sTmpText = 'Во все рестораны города:';
	} else {
		$iTmpCount = count($arItems2Rest);
		$sTmpText = $iTmpCount > 1 ? 'В '.$iTmpCount.' '.CProjectUtils::PrintCardinalNumberRus($iTmpCount, 'ресторанов', 'ресторан', 'ресторана').':' : 'В ресторан:';
	}

	$arTmp = !$bAll ? array_keys($arItems2Rest) : $arUsedRestaurants;
	$arTmp[] = 0;
	?><div onclick="return {'vacancy': {}}" data-rests="[<?=implode(',', $arTmp)?>]" class="vacancy js-widget"><?
		?><div class="vacancy__img"><?
			$sImgSrc = $templateFolder.'/images/job_'.$arCommon['_IMG_TYPE_'].'.png';
			if($arItem['DETAIL_PICTURE']) {
				$sImgSrc = $arItem['DETAIL_PICTURE'];
			}
			?><img src="<?=$sImgSrc?>" alt=""><?
		?></div><?
		?><div class="vacancy__body"><?
			?><h2 class="h2 vacancy__title"><?=$arCommon['NAME']?></h2><?
			?><div class="vacancy__desc"><?
				?><div class="vacancy__desc-to"><?=$sTmpText?></div><?
				if($arItems2Rest) {
					?><div class="vacancy__rests"><?
						// в рестораны всех городов 
						if($arItems2Rest[0]) {
							foreach($arItems2Rest[0] as $iItemId) {
								?><a href="javascript:void(0)" data-id="<?='vacancy-'.$iItemId?>" class="vacancy__rest"><span>Все рестораны</span></a><?
							}
							unset($arItems2Rest[0]);
						}
						// во все рестораны города
						if($arItems2Rest[$arResult['CITY']['ID']]) {
							foreach($arItems2Rest[$arResult['CITY']['ID']] as $iItemId) {
								?><a href="javascript:void(0)" data-id="<?='vacancy-'.$iItemId?>" class="vacancy__rest"><span>Все рестораны</span></a><?
							}
							unset($arItems2Rest[$arResult['CITY']['ID']]);
						}
						// в указанные рестораны города
						if($arItems2Rest) {
							foreach($arResult['RESTAURANTS'] as $iTmpSectionId => $arSectionItem) {
								if($arItems2Rest[$iTmpSectionId]) {
									foreach($arItems2Rest[$iTmpSectionId] as $iItemId) {
										?><a href="javascript:void(0)" data-id="<?='vacancy-'.$iItemId?>" class="vacancy__rest"><span><?=$arSectionItem['NAME']?></span></a><?
									}
								}
							}
						}
					?></div><?
				}
				foreach($arGroupedItems as $arItem) {
					?><div id="<?='vacancy-'.$arItem['ID']?>" class="vacancy__about"><?
						echo $arItem['DETAIL_TEXT'];
					?></div><?
				}
			?></div>
		</div>
	</div><?
}

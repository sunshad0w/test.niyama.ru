<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа (шаг 1 - корзина)
//

//_log_array($arResult, '$arResult');

?><div onclick="return {'order': {}}" class="order js-widget"><?
	//
	// Правая колонка
	//
	?><div class="order__side">
		<div class="order__amount">
			<span class="h1">итого </span><span class="h1 order__amount-sum"><?=CProjectUtils::FormatNum($arResult['ORDER']['PRICE'])?> <span class="rouble">a</span></span>
		</div><?
		?><div class="order__bonus">
			<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/icon_1.png'?>" alt=""></div>
			<span class="order__bonus-text"><?
				if($arResult['ORDER']['PERSONS_CNT']) {
					?><span><?=$arResult['ORDER']['PERSONS_CNT']?></span> <span><?=CProjectUtils::PrintCardinalNumberRus($arResult['ORDER']['PERSONS_CNT'], 'персон', 'персона', 'персоны')?>, </span><?
				}
				?><span><?=$arResult['ORDER']['DISHES_CNT']?></span> <span><?=CProjectUtils::PrintCardinalNumberRus($arResult['ORDER']['DISHES_CNT'], 'блюд', 'блюдо', 'блюда')?></span>
			</span>
		</div><?

		//
		// таймлайн скидок текущего заказа
		//
		$sTmpKey = $arResult['ORDER_TIMELINE_BONUSES']['free_delivery'] ? 'free_delivery' : 'free_delivery_mkad';
		if($arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]) {
			?><div class="order__bonus">
				<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/delivery.png'?>" alt=""></div><span class="order__bonus-text"><?=$arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]['NAME']?></span>
			</div><?
		}
		$sTmpKey = 'card';
		if($arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]) {
			?><div class="order__bonus">
				<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/card.png'?>" alt=""></div><span class="order__bonus-text"><?=$arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]['NAME']?></span>
			</div><?
		}

		if($arResult['HAS_NOT_ACTIVE_PRODUCTS'] == 'Y') {
			echo '<div style="color: red; margin: 35px 0;">В корзине содержатся блюда, которых уже нет в нашем меню. Оформление заказа невозможно.</div>';
		} elseif($arResult['ORDER']['PRICE'] >= $arResult['ORDER_MIN_PRICE']) {
			?><div class="order__checkout"><?
				?><div class="btn _style_3" onclick="jQuery('#order-form-confirm-cart').submit()">Оформить заказ</div>
				<form id="order-form-confirm-cart" method="post" action="/order/confirm/">
					<input type="hidden" name="step" value="confirm_cart">
				</form><?

				/*
				if($arResult['ORDER_ALT_TEL']) {
					?><p>или по телефону <b><?=$arResult['ORDER_ALT_TEL']?></b></p><?
					?><p>сообщить № заказа <b><?=$arResult['DRAFT_ORDER']['ACCOUNT_NUMBER']?></b></p><?
				}
				*/
			?></div><?
		} else {

			echo '<div class="order-buy__min-sum">';
			echo 'Минимальная сумма заказа - '.CProjectUtils::FormatNum($arResult['ORDER_MIN_PRICE']).'<span class="rouble">a</span>';
			 $iOptionsMinSelfDeliveryOrder = intval(CNiyamaCustomSettings::getStringValue('min_self_delivery_sum_by_order', '0'));
			if($iOptionsMinSelfDeliveryOrder) {
				echo '<br />Самовывоз от '.$iOptionsMinSelfDeliveryOrder.'<span class="rouble">a</span> при заказе по телефону';
			}
			echo '</div>';

			?><div class="order__checkout"><?
				?><div class="btn _style_3 _disabled">Оформить заказ</div><?
				/*
				?><form id="order-form-confirm-cart" method="post" action="">
					<input type="hidden" name="step" value="confirm_cart">
				</form><?
				*/
				/*
				if($arResult['ORDER_ALT_TEL']) {
					?><p>или по телефону <b><?=$arResult['ORDER_ALT_TEL']?></b></p><?
					?><p>сообщить № заказа <b><?=$arResult['DRAFT_ORDER']['ACCOUNT_NUMBER']?></b></p><?
				}
				*/
			?></div><?

		}
		//
		// промо-блок
		//
		$APPLICATION->IncludeComponent(
			'adv:system.empty',
			'niyama.1.0.order-promo',
			array(
				'arResult' => $arResult
			),
			null,
			array(
				'HIDE_ICONS' => 'Y'
			)
		);

	?></div><?

	//
	// Состав корзины
	//
	?><div class="order__main"><?

		// купоны
		$arCouponsList = array();
		$arTableTypes = array('current', 'guest', 'common');
		foreach($arTableTypes as $sTableType) {
			$arTmpCart = $arResult['CART'][$sTableType] ? $arResult['CART'][$sTableType] : array();
			foreach($arTmpCart as $iPersonId => $arTable) {
				if($arTable['ITEMS']) {
					foreach($arTable['ITEMS'] as $iBasketItemId => $arItem) {
						if($arItem['_PRODUCT_']['TYPE'] == 'coupon') {
							$arCouponsList[] = $arItem;
							unset($arResult['CART'][$sTableType][$iPersonId]['ITEMS'][$iBasketItemId]);
						}
					}
				}
			}
		}

//
// to do: адаптировать system.empty:niyama.1.0.coupon под проверку заказа и перевести купоны на него
//

		if($arCouponsList) {
			$GLOBALS['APPLICATION']->IncludeComponent(
				'adv:system.empty',
				'niyama.1.0.coupons-order',
				array(
					'arCouponsList'=>$arCouponsList
				),
				null,
				array(
					'HIDE_ICONS' => 'Y'
				)
			);
		}

		// состав заказа
		$arTableTypes = array('current', 'guest');
		foreach($arTableTypes as $sTableType) {
			$arTmpCart = $arResult['CART'][$sTableType] ? $arResult['CART'][$sTableType] : array();
			foreach($arTmpCart as $iPersonId => $arTable) {
				if($arTable['ITEMS']) {
					?><div data-datbleid="<?=$sTableType?>" data-guestid="<?=$iPersonId?>" class="order__group"><?
						?><div class="order__group-title">
							<div class="order__group-guest"><?
								$arTmpImg = array();
								if($arTable['_PERSON_']['IMG_ID']) {
									$arTmpImg = CFile::ResizeImageGet(
										$arTable['_PERSON_']['IMG_ID'],
										array(
											'width' => 30,
											'height' => 35
										),
										BX_RESIZE_IMAGE_EXACT,
										true
									);
								}
								if(!$arTmpImg['src']) {
									$iAvatarId = CUsersData::getFirstDefaultAvatar();
									if($iAvatarId) {
										$arTmpImg = CFile::ResizeImageGet(
											$iAvatarId,
											array(
												'width' => 30,
												'height' => 35
											),
											BX_RESIZE_IMAGE_EXACT,
											true
										);
									}
								}
								if($arTmpImg['src']) {
									?><img src="<?=$arTmpImg['src']?>" alt=""><?
								}
							?></div>
							<div class="order__group-name"><?
								echo $arTable['_PERSON_']['NAME'];
								$iTmpCnt = count($arTable['ITEMS']);
								echo '<div class="order__group-info">'.$iTmpCnt.' '.CProjectUtils::PrintCardinalNumberRus($iTmpCnt, 'блюд', 'блюдо', 'блюда').', <b>'.CProjectUtils::FormatNum($arTable['SUM_PRICE']).'</b> <span class="rouble">a</span></div>';
							?></div>
						</div><?
						foreach($arTable['ITEMS'] as $arItem) {
							?><div class="order__item">
								<button class="order__item-remove" data-id="<?=$arItem['ID']?>"></button>
								<div class="order__item-count">
									<span class="order__item-cost"><?=CNiyamaOrders::FormatPrice($arItem['PRICE'])?></span><?
									if($arItem['PROPS']['BY_COUPON'] && $arItem['PROPS']['BY_COUPON'] == 'Y' || ($arItem['_PRODUCT_'] && $arItem['_PRODUCT_']['ACTIVE'] != 'Y')) {
										?><div class="order__item-select"><?
											echo intval($arItem['QUANTITY']);
										?></div><?
									} else {
										?><div class="order__item-select">
											<div onclick="return {'select': {}}" class="select__wrap _mini js-widget">
												<select data-id="<?=$arItem['ID']?>" name="<?=('ITEM['.$arItem['ID'].'][QUANTITY]')?>" class="select"><?
													for($i = 1; $i <= $arResult['ORDER_DISH_MAX_QUANTITY']; $i++) {
														$sTmpSelected = $i == $arItem['QUANTITY'] ? ' selected="selected"' : '';
														?><option value="<?=$i?>"<?=$sTmpSelected?>><?=$i?></option><?
													}
													if($arItem['QUANTITY'] > $arResult['ORDER_DISH_MAX_QUANTITY']) {
														?><option value="<?=intval($arItem['QUANTITY'])?>" selected="selected"><?=intval($arItem['QUANTITY'])?></option><?
													}
												?></select>
											</div>
										</div><?
									}
								?></div>
								<div class="order__item-img"><?
									if($arItem['_PRODUCT_']['IMG_ID']) {
										$arTmpImg = CFile::ResizeImageGet(
											$arItem['_PRODUCT_']['IMG_ID'],
											array(
												'width' => 119,
												'height' => 86
											),
											BX_RESIZE_IMAGE_EXACT,
											true
										);
										if($arTmpImg['src']) {
											?><img src="<?=$arTmpImg['src']?>" alt=""><?
										}
									}
								?></div>
								<div class="order__item-desc">
									<div class="order__item-title"><?=$arItem['_PRODUCT_']['NAME']?></div>
									<div class="order__item-ingredients"><?
										if($arItem['_PRODUCT_'] && $arItem['_PRODUCT_']['ACTIVE'] != 'Y') {
											echo '<span style="color: red">Данного блюда уже нет в нашем меню</span>';
										} elseif($arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT']) {
											/*
											if(!empty($arItem['_PRODUCT_']['_MISC_DATA_']['EXCLUDE_INGREDIENT'])) {
												$ex_ings = array();
												foreach($arItem['_PRODUCT_']['_MISC_DATA_']['EXCLUDE_INGREDIENT'] as $key => $elem) {
													if (!in_array($elem,$arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT'])) {
														$ex_ings[$key] = $elem;
													}
												}
												if (!empty($ex_ings)) {
													$arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT'] = array_merge($arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT'], $ex_ings);
												}
											}
											*/
											echo implode(', ', $arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT']);
										}
									?></div>
								</div>
							</div><?
						}
					?></div><?
				}
			}
		}

		//
		// Дополнительно на всех
		//
		if($arResult['ADD_COMP'] || $arResult['CART']['common'][0]['ITEMS']) {
			?><div data-datbleid="common" data-guestid="1" class="order__group _all">
				<div class="order__group-title">
					<div class="label"><?
						if($arResult['ORDER']['PERSONS_CNT']) {
							echo 'Дополнительно на всех';
						} else {
							echo 'На всех';
						}
						if($arResult['CART']['common'][0]['SUM_PRICE']) {
							echo '<div class="order__group-info"><b>'.CProjectUtils::FormatNum($arResult['CART']['common'][0]['SUM_PRICE']).'</b> <span class="rouble">a</span></div>';
						}
					?></div>
				</div><?

				$arCommonCartTableItems = isset($arResult['CART']['common'][0]['ITEMS']) ? $arResult['CART']['common'][0]['ITEMS'] : array();
				$arTableItems = array();
				foreach($arResult['ADD_COMP'] as $arItem) {
					$arTmpFreeBasketItem = array();
					$arTmpPaidBasketItem = array();
					foreach($arCommonCartTableItems as $iTmpId => $arCommonTableItem) {
						if($arCommonTableItem['PROPS']['type'] == 'dop_component') {
							if(!$arTmpFreeBasketItem && $arCommonTableItem['PRODUCT_ID'] == $arItem['FREE']['PRODUCT_ID']) {
								$arTmpFreeBasketItem = $arCommonTableItem;
								unset($arCommonCartTableItems[$iTmpId]);
							} elseif(!$arTmpPaidBasketItem && $arCommonTableItem['PRODUCT_ID'] == $arItem['PAID']['PRODUCT_ID']) {
								$arTmpPaidBasketItem = $arCommonTableItem;
								unset($arCommonCartTableItems[$iTmpId]);
							}
						}
					}

					$arTableItems[] = array(
						'_PRODUCT_' => array(
							'IMG_ID' => $arItem['INFO']['IMG_ID'],
							'NAME' => $arItem['INFO']['NAME'],
							'INGREDIENTS' => '',
							'ACTIVE' => 'Y'
						),
						'FREE' => array(
							'QUANTITY' => $arItem['FREE_COUNT'],
							'PRICE' => 0,
							'BASKET_ITEM_ID' => $arTmpFreeBasketItem ? $arTmpFreeBasketItem['ID'] : 0,
							'PRODUCT_ID' => $arItem['FREE']['PRODUCT_ID']
						),
						'PAID' => array(
							'QUANTITY' => $arTmpPaidBasketItem ? $arTmpPaidBasketItem['QUANTITY'] : 0,
							'PRICE' => $arItem['PAID']['PRICE'],
							'BASKET_ITEM_ID' => $arTmpPaidBasketItem ? $arTmpPaidBasketItem['ID'] : 0,
							'PRODUCT_ID' => $arItem['PAID']['PRODUCT_ID']
						),
						'IS_ADD_COMP' => 'Y',
						'CAN_BUY_MORE' => $arItem['CAN_BUY_MORE'],
						'UNIT_TYPE' => $arItem['INFO']['PROPERTY_UNIT_TYPE'],
						'PROPS' => array()
					);
				}

				if($arCommonCartTableItems) {
					foreach($arCommonCartTableItems as $arItem) {
						$arTableItems[] = array(
							'_PRODUCT_' => array(
								'IMG_ID' => $arItem['_PRODUCT_']['IMG_ID'],
								'NAME' => $arItem['_PRODUCT_']['NAME'],
								'INGREDIENTS' => $arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT'] ? implode(', ', $arItem['_PRODUCT_']['_MISC_DATA_']['ALL_INGREDIENT']) : '',
								'ACTIVE' => $arItem['_PRODUCT_']['ACTIVE']
							),
							'FREE' => array(
								'QUANTITY' => 0,
								'PRICE' => 0,
								'BASKET_ITEM_ID' => 0,
								'PRODUCT_ID' => 0
							),
							'PAID' => array(
								'QUANTITY' => $arItem['QUANTITY'],
								'PRICE' => $arItem['PRICE'],
								'BASKET_ITEM_ID' => $arItem['ID'],
								'PRODUCT_ID' => $arItem['PRODUCT_ID']
							),
							'IS_ADD_COMP' => 'N',
							'CAN_BUY_MORE' => 'Y',
							'UNIT_TYPE' => '',
							'PROPS' => $arItem['PROPS']
						);
					}
				}

				foreach($arTableItems as $arItem) {
					?><div class="order__item"><?
						if($arItem['IS_ADD_COMP'] != 'Y') {
							?><div class="order__item-remove" data-id="<?=$arItem['PAID']['BASKET_ITEM_ID']?>"></div><?
						}
						?><div class="order__item-count"><?
							if($arItem['CAN_BUY_MORE'] == 'Y' && $arItem['PAID']['PRICE']) {
								?><span class="order__item-cost"><?=CProjectUtils::FormatNum($arItem['PAID']['PRICE']).' '?><span class="rouble">a</span></span><?
								if($arItem['PROPS']['BY_COUPON'] && $arItem['PROPS']['BY_COUPON'] == 'Y' || ($arItem['_PRODUCT_'] && $arItem['_PRODUCT_']['ACTIVE'] != 'Y')) {
									?><div class="order__item-select"><?
										echo intval($arItem['PAID']['QUANTITY']);
									?></div><?
								} else {
									?><div class="order__item-select">
										<div onclick="return {'select': {}}" class="select__wrap _mini js-widget"><?
											if($arItem['IS_ADD_COMP'] == 'Y') {
												echo '<select data-id="add-'.$arItem['PAID']['PRODUCT_ID'].'-'.$arItem['PAID']['BASKET_ITEM_ID'].'" class="select">';
											} else {
												echo '<select data-id="'.$arItem['PAID']['BASKET_ITEM_ID'].'" class="select">';
											}
											$i = $arItem['IS_ADD_COMP'] == 'Y' ? 0 : 1;
											for($i; $i <= $arResult['ORDER_DISH_MAX_QUANTITY']; $i++) {
												$sTmpSelected = $i == $arItem['PAID']['QUANTITY'] ? ' selected="selected"' : '';
												?><option value="<?=$i?>"<?=$sTmpSelected?>><?=$i?></option><?
											}
											if($arItem['PAID']['QUANTITY'] > $arResult['ORDER_DISH_MAX_QUANTITY']) {
												?><option value="<?=intval($arItem['PAID']['QUANTITY'])?>" selected="selected"><?=intval($arItem['PAID']['QUANTITY'])?></option><?
											}
											echo '</select>';
										?></div>
									</div><?
								}
							}
						?></div>
						<div class="order__item-img"><?
							if($arItem['_PRODUCT_']['IMG_ID']) {
								$arTmpImg = CFile::ResizeImageGet(
									$arItem['_PRODUCT_']['IMG_ID'],
									array(
										'width' => 119,
										'height' => 86
									),
									BX_RESIZE_IMAGE_EXACT,
									true
								);
								if($arTmpImg['src']) {
									?><img src="<?=$arTmpImg['src']?>" alt=""><?
								}
							}
						?></div>
						<div class="order__item-desc">
							<div class="order__item-title"><?=$arItem['_PRODUCT_']['NAME']?></div>
							<div class="order__item-ingredients"><?
								if($arItem['_PRODUCT_'] && $arItem['_PRODUCT_']['ACTIVE'] != 'Y') {
									echo '<span style="color: red">Данного блюда уже нет в нашем меню</span>';
								} elseif($arItem['_PRODUCT_']['INGREDIENTS']) {
									echo $arItem['_PRODUCT_']['INGREDIENTS'];
								} else {
									if($arItem['FREE']['QUANTITY']) {
										$sTmpValCnt = '';
										$sTmpVal = ToUpper($arItem['UNIT_TYPE']);
										if($sTmpVal == 'ПОРЦ.') {
											$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($arItem['FREE']['QUANTITY'], 'порций', 'порция', 'порции');
										} elseif($sTmpVal == 'КОМПЛ.') {
											$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($arItem['FREE']['QUANTITY'], 'комплектов', 'комплект', 'комплекта');
										} else {
											$sTmpValCnt = CProjectUtils::PrintCardinalNumberRus($arItem['FREE']['QUANTITY'], 'штук', 'штука', 'штуки');
										}
										// Number2Word_Rus(
										echo $arItem['FREE']['QUANTITY'].' '.$sTmpValCnt.' бесплатно';
									}
								}
							?></div><?
								if($arItem['IS_ADD_COMP'] == 'Y') {
									echo '<div>дополнительно:</div> ';
								}
						?></div>
					</div><?
				}
			?></div><?
		}

		//
		// Гости
		//
		if (!empty($arResult['guest_count'])) {
			$guest_count = array(
				'QUANTITY' => $arResult['guest_count']['QUANTITY'],
				'TYPE' => 'guest_count',
				'BASKET_ITEM_ID' => $arResult['guest_count']['ID']
			);
		} else {
			$guest_count = array(
				'QUANTITY' => $arResult['ORDER']['PERSONS_CNT'],
				'TYPE' => 'guest_count',
				'BASKET_ITEM_ID' => 0
			);
		}
		//if($arResult['ADD_COMP'] || $arResult['CART']['common'][0]['ITEMS']) {
			?><div data-datbleid="common" data-guestid="1" class="order__group _all">
				<div class="order__group-title">
					<div class="label">Гости</div>
				</div>
				<div class="order__item">
					<div class="order__item-count">
						<div class="order__item-select no_cost">
							<div onclick="return {'select': {}}" class="select__wrap _mini js-widget">
								<select data-id="<?='add-'.$guest_count['TYPE'].'-'.$guest_count['BASKET_ITEM_ID']?>" class="select"><? 
									$i = 1;
									for($i; $i <= $arResult['ORDER_DISH_MAX_QUANTITY']; $i++) {
										$sTmpSelected = $i == $guest_count['QUANTITY'] ? ' selected="selected"' : '';
										?><option value="<?=$i?>"<?=$sTmpSelected?>><?=$i?></option><?
									}
									if($guest_count['QUANTITY'] > $arResult['ORDER_DISH_MAX_QUANTITY']) {
										?><option value="<?=intval($guest_count['QUANTITY'])?>" selected="selected"><?=intval($guest_count['QUANTITY'])?></option><?
									}
								?></select>
							</div>
						</div>
					</div>
					<div class="order__item-img">
						<img src="/images/niyama_count_guests.png" alt="">
					</div>
					<div class="order__item-desc">
						<div class="order__item-title">Сколько гостей?</div>
						<div class="order__item-ingredients">Мы доставим палочки, салфетки и приправы</div>
					</div>
				</div>
			</div><?
		//}

	?></div>
</div>

<script>
convead('event', 'update_cart', {
            items: [
                <?php foreach($arTable['ITEMS'] as $arItem) { ?>
                    {product_id: '<?=$arItem['ID'] ?>', qnt: <?=$arItem['QUANTITY'] ?>, price: <?=$arItem['PRICE'] ?>},
                <?php } ?>
]
});
</script>
<?
$APPLICATION->IncludeComponent(
    'adv:system.empty',
    'niyama.1.0.codes',
    array(
        'MODE' => 'basket',
        'products' => $arTable['ITEMS']
    )
);
?>
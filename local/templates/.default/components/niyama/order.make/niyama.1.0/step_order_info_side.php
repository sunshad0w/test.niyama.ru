<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа: подключаемый файл с правой колонкой для шага 3 (шаг 3 - контактная информация)
// Подключается также самостоятельно при аякс запросе на третьем шаге
//

?><div class="order"><?
	?><div class="order__amount">
		<span class="h1">итого </span><span class="h1 order__amount-sum"><?=CProjectUtils::FormatNum($arResult['ORDER']['PRICE'], ' ')?> <span class="rouble">a</span></span>
	</div><?

	?><div class="order__bonus">
		<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/icon_1.png'?>" alt=""></div><?
		?><span class="order__bonus-text"><?
			if($arResult['ORDER']['PERSONS_CNT']) {
				?><span><?=$arResult['ORDER']['PERSONS_CNT']?></span> <span><?=CProjectUtils::PrintCardinalNumberRus($arResult['ORDER']['PERSONS_CNT'], 'персон', 'персона', 'персоны')?>, </span><?
			}
			?><span><?=$arResult['ORDER']['DISHES_CNT']?></span> <span><?=CProjectUtils::PrintCardinalNumberRus($arResult['ORDER']['DISHES_CNT'], 'блюд', 'блюдо', 'блюда')?></span><?
		?></span>
	</div><?

	//
	// таймлайн скидок текущего заказа
	//
	$sTmpKey = $arResult['ORDER_TIMELINE_BONUSES']['free_delivery'] ? 'free_delivery' : 'free_delivery_mkad';
	if($arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]) {
		?><div class="order__bonus">
			<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/delivery.png'?>" alt=""></div><span class="order__bonus-text"><?=$arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]['NAME']?></span>
		</div><?
	}
	$sTmpKey = 'card';
	if($arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]) {
		?><div class="order__bonus">
			<div class="order__bonus-img"><img src="<?=$templateFolder.'/images/card.png'?>" alt=""></div><span class="order__bonus-text"><?=$arResult['ORDER_TIMELINE_BONUSES'][$sTmpKey]['NAME']?></span>
		</div><?
	}
?></div><? 

$GLOBALS['APPLICATION']->IncludeComponent(
	'adv:system.empty',
	'niyama.1.0.orderconfirm-promo',
	array(
		'arResult' => $arResult
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

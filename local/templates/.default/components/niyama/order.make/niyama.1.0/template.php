<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа
//

//_show_array($arResult);

if(empty($arResult['CART'])) {
	ShowError('Корзина пуста');
	return;
}

if($arParams['STEP_ORDER_INFO_SIDE_ONLY'] && $arParams['STEP_ORDER_INFO_SIDE_ONLY']) {
	// запрашивается только правая колонка третьего шага
	include('step_order_info_side.php');
	return;
}

switch($arResult['STEP']) {
	case 'cart':
		// первый шаг
		include('step_cart.php');
	break;

	case 'auth':
		// второй шаг
		include('step_auth.php');
	break;

	case 'order_info':
		// третий шаг
		include('step_order_info.php');
	break;
}

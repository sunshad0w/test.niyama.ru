<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа (шаг 3 - контактная информация)
//

//_log_array($arResult, '$arResult');

$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_PAGE_WRAP_ELEMENT_CLASS', '_light-2');

// Блок "мы можем вам перезвонить" (выводится на каждом табе)
ob_start();
$GLOBALS['APPLICATION']->IncludeFile(
	SITE_DIR.'include_areas/order-info.php',
	array(),
	array(
		'MODE' => 'php',
		'SHOW_BORDER' => false
	)
);
$sOctoline = ob_get_clean();

$iCityOtherId = intval(CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other', '27'));
?><h1 class="auth__title">Оформление заказа</h1><?

?><div onclick="return {'orderingForm': {} }" data-sidebar-url="/ajax/order_confirm_step2.php" class="ordering-form js-widget">
<p>Оформите заказ сейчас и получите <a target="_blank" href="http://www.niyama.ru/promos/115952.php">скидку 30%</a> на следующий заказ! Скидка действует в течение восьми дней с момента первого заказа. Ждем Вас снова и снова, дорогие друзья!
</p>
	<form data-parsley-validate action="" method="post"><?

		if($arResult['STEP_ERRORS']) {
			ShowError(implode('<br />', $arResult['STEP_ERRORS']));
		}

		?><input type="hidden" name="step" value="<?=$arResult['STEP']?>"><?

		// правая колонка
		?><div class="ordering-form__side"><?
			include('step_order_info_side.php');
		?></div><?

		//
		// Блок "Представьтесь"
		//
		?><div class="ordering-form__step">
			<div class="ordering-form__counter _orange">1</div>
			<h2 class="ordering-form__title">
				Представьтесь
				<div id="edit1" data-block="1" class="ordering-form__edit"><span>Редактировать</span></div>
			</h2>
			<div id="desc1" class="ordering-form__desc"></div>
			<div class="ordering-form__body block1">
				<div class="ordering-form__row">
					<div class="input__wrap">
						<label class="label">Имя</label>
						<input type="text" name="CUSTOMER_NAME" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['CUSTOMER_NAME'])?>" data-parsley-group="block1" required class="input _name">
					</div>
					<div class="input__wrap">
						<label class="label">Фамилия</label>
						<input type="text" name="CUSTOMER_LAST_NAME" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['CUSTOMER_LAST_NAME'])?>" data-parsley-group="block1" class="input _surname">
					</div>
				</div>
				<div class="ordering-form__row">
					<div class="input__wrap">
						<label class="label">Телефон</label>
						<input type="tel" name="CUSTOMER_PHONE" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['CUSTOMER_PHONE'])?>" data-parsley-group="block1" data-parsley-phonenumber required class="input _phone">
					</div>
					<div class="input__wrap">
						<label class="label">Электронная почта</label>
						<input type="email" name="CUSTOMER_EMAIL" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['CUSTOMER_EMAIL'])?>" data-parsley-group="block1" required data-parsley-type="email" class="input _email">
					</div>
				</div>
				<div class="ordering-form__row">
					<button type="button" data-current-block="1" data-next-block="2" class="btn _style_3" onclick="yaCounter24799106.reachGoal('yourself_ok'); ga('send','pageview','/yourself_ok');  return true;">Продолжить</button>
					<?=$sOctoline?>
				</div>
			</div>
		</div><?

		//
		// Блок "Доставка"
		//
		?><div class="ordering-form__step">
			<div class="ordering-form__counter">2</div>
			<h2 class="ordering-form__title">
				Доставка
				<div id="edit2" data-block="2" class="ordering-form__edit"><span>Редактировать</span></div>
			</h2>
			<div id="desc2" class="ordering-form__desc"></div>
			<div class="ordering-form__body block2"><?
				if($arResult['DELIVERY_TYPES']) {
					?><div id="deliverytype" class="ordering-form__row align-left"><?
						if(!$arResult['ORDER_INFO']['DELIVERY_TYPE']) {
							$arTmp = reset($arResult['DELIVERY_TYPES']);
							$arResult['ORDER_INFO']['DELIVERY_TYPE'] = $arTmp['VALUE'];
						}
						foreach($arResult['DELIVERY_TYPES'] as $arItem) {
							$sChecked = $arItem['VALUE'] == $arResult['ORDER_INFO']['DELIVERY_TYPE'] ? ' checked="checked"' : '';
							$sId = $arItem['VALUE'];
							switch($arItem['VALUE']) {
								case 'courier':
									$sId = 'delivery';
								break;

								case 'self':
									$sId = 'pickup';
								break;
							}
							?><div class="ordering-form__col">
								<input type="radio" name="CUSTOMER_DELIVERY_TYPE" value="<?=$arItem['VALUE']?>" id="<?=$sId?>"<?=$sChecked?> class="radio <?='_'.$sId?>">
								<label for="<?=$sId?>" class="radio-label checkbox-label"><?=$arItem['NAME']?></label>
							</div><?
						}
					?></div><?
				}
				?><div id="deliveryTypeGroup"><?
					//
					// Доставка курьером
					//
					?><div id="deliveryControls"><?
						if($GLOBALS['USER']->IsAuthorized()) {
							$arAddress = CUsersData::getAddressList();
							$arTmp = CNiyamaOrders::GetDeliveryRegions();
							$arDELIVERY_REGIONS = array();
							foreach($arTmp as $arItem) {
								if($arItem['ACTIVE'] == 'Y') {
									$arDELIVERY_REGIONS[$arItem['ID']] = $arItem;
								}
							}

							if(!empty($arAddress)) {
								?><div class="ordering-form__row">
									<div onclick="return {'select': {}}" class="select__wrap __addresses js-widget">
										<select name="CUSTOMER_DELIVERY_ADDRESSES" class="select _addresses"><?
											foreach($arAddress as $aItem) {
												if($aItem['PROPERTY_REGION_VALUE'] == $iCityOtherId) {
													$sRegion = $aItem['PROPERTY_CITY_DOP_VALUE'];
												} else {
													$sRegion = $arDELIVERY_REGIONS[$aItem['PROPERTY_REGION_VALUE']]['NAME'];
												}
												$sSubway = (!empty($aItem['SUBWAY_DATA'])) ? ' (м. '.$aItem['SUBWAY_DATA']['NAME'].')' : '';
												$sAddress = $aItem['PROPERTY_ADDRESS_VALUE'];
												$sHouse = (!empty($aItem['PROPERTY_HOUSE_VALUE']) ? ', '.$aItem['PROPERTY_HOUSE_VALUE'] : '');
												$sHome = (!empty($aItem['PROPERTY_HOME_VALUE']) ? ', кв.'.$aItem['PROPERTY_HOME_VALUE'] : '');
												?><option<?=(intval($aItem['PROPERTY_IS_DEFAULT_VALUE']) > 0) ? ' selected="selected"' : ''?> data-address="" value="<?=$aItem['ID']?>"><?=$sRegion.$sSubway.', '.$sAddress.$sHouse.$sHome?></option><?
											}
											?><option value="0" data-address="">Новый адрес</option>
										</select>
									</div>
								</div><?
							}
						}
						?><div class="ordering-form__row"><?
							if($arResult['DELIVERY_REGIONS']) {
								?><div onclick="return {'select': {}}" class="select__wrap js-widget">
									<label class="label">Город</label>
									<select name="CUSTOMER_DELIVERY_REGION" class="select _city"><?
										$sRegion = '';
										$bMsk = !empty($arResult['ORDER_INFO']['DELIVERY_REGION']) ? false : true;
										foreach($arResult['DELIVERY_REGIONS'] as $arItem) {
											$sTmpSelected = '';
											if($arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION']) {
												$sTmpSelected = ' selected="selected"';
												$sRegion = $arItem['NAME'];
											}
											//$sTmpSelected = $arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION'] ? ' selected="selected"' : '';
											$sTmpDataCity = $arItem['NAME'] == 'Москва' ? 'REGION_1' : 'REGION_X';
											if($iCityOtherId == $arItem['ID']) {
												$sTmpDataCity='REGION_OTHER';
											}
											?><option<?=$sTmpSelected?> data-city="<?=$sTmpDataCity?>" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
										}
										if($sRegion == 'Москва') {
											$bMsk = true;
										}
									?></select><?
								?></div><?
								// !!!
								echo ' ';
							}

							if($arResult['SUBWAY_STATIONS']) {
								?><div class="input__wrap<?=(!$bMsk) ? ' _hidden' : ''?>">
									<label class="label">Станция метро</label>
									<input name="CUSTOMER_DELIVERY_SUBWAY_STATION" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_SUBWAY'])?>" data-parsley-group="block2" class="input _metro">
								</div><?
							}
							?><div class="input__wrap<?=($arResult['ORDER_INFO']['DELIVERY_REGION'] == $iCityOtherId) ? '' : ' _hidden'?>">
								<label class="label">Название населённого пункта</label>
								<input name="CUSTOMER_DELIVERY_CITY_DOP" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_CITY_DOP'])?>" data-parsley-group="block2" class="input _other">
							</div>
						</div><?
						/*
						?><div class="ordering-form__row _mkad<?=(!$bMsk) ? ' _hidden' : ''?>"<?=(!$bMsk) ? ' style="display: none;"' : ''?>><?
							$sChecked = $arResult['ORDER_INFO']['DELIVERY_WITHIN_MKAD'] ? ' checked="checked"' : '';
							?><input type="checkbox" name="CUSTOMER_DELIVERY_WITHIN_MKAD" value="Y"<?=$sChecked?> id="mkad" class="checkbox _big _mkad">
							<label for="mkad" class="checkbox-label">В пределах МКАД</label>
						</div><?
						*/
						?><div class="ordering-form__row _other-txt"><?
							echo CNiyamaCustomSettings::GetStringValue('delevery_id_sity_other_text', 'Не нашли свой город? Позвоните нам <span>+7 495 781-781-9</span> или <a href="/delivery/" target="_blank">посмотрите карту доставки</a>');
						?></div><?

						?><div class="ordering-form__row ">
							<div class="input__wrap _street-wrap">
								<label class="label">Улица</label>
								<input name="CUSTOMER_DELIVERY_ADDRESS" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_ADDRESS'])?>" data-parsley-group="block2" required class="input _address">
							</div>
							<div class="input__wrap _house-wrap">
								<label class="label">Дом</label>
								<input name="CUSTOMER_DELIVERY_HOUSE" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_HOUSE'])?>" data-parsley-group="block2" required class="input _house">
							</div>
						</div>
						<div class="ordering-form__row _quarter">
							<div class="input__wrap">
								<label class="label">Квартира</label>
								<input name="CUSTOMER_DELIVERY_APART" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_APART'])?>" class="input _apart">
							</div>
							<div class="input__wrap">
								<label class="label">Подъезд</label>
								<input name="CUSTOMER_DELIVERY_PORCH" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_PORCH'])?>" class="input _porch">
							</div>
							<div class="input__wrap">
								<label class="label">Домофон</label>
								<input name="CUSTOMER_DELIVERY_INTERCOM" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_INTERCOM'])?>" class="input _intercom">
							</div>
							<div class="input__wrap">
								<label class="label">Этаж</label>
								<input name="CUSTOMER_DELIVERY_FLOOR" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_FLOOR'])?>" class="input _floor">
							</div>
						</div><?
						?><div id="deliverytime" class="ordering-form__row align-left"><?
							$bByDate = $arResult['ORDER_INFO']['DELIVERY_BY_TIME'] && $arResult['ORDER_INFO']['DELIVERY_BY_DATE'];
							?><div class="ordering-form__col"><?
								$sTmpChecked = !$bByDate ? ' checked="checked"' : '';
								?><input type="radio"<?=$sTmpChecked?> name="deliverytime" id="soon" checked class="radio _soon">
								<label for="soon" class="radio-label checkbox-label">Как можно скорее</label>
							</div>
							<div class="ordering-form__col"><?
								$sTmpChecked = $bByDate ? ' checked="checked"' : '';
								?><input type="radio"<?=$sTmpChecked?> name="deliverytime" id="certain" class="radio _certain">
								<label for="certain" class="radio-label checkbox-label">К определённой дате и времени</label>
								<div class="ordering-form__row _certain">
									<div name="time" onclick="return {'select': {}}" class="select__wrap js-widget">
										<select name="CUSTOMER_DELIVERY_BY_TIME" class="select"><?
											$iHours = 0;
											$iMinutes = 0;
											for($i = 0; $i < 48; $i++) {
												$iMinutes2 = $iMinutes == 0 ? 30 : 0;
												$iHours2 = $iMinutes == 0 ? $iHours : $iHours + 1;
												$iHours2 = $iHours2 > 23 ? 0 : $iHours2;
												$sValue = sprintf('%02d:%02d - %02d:%02d', $iHours, $iMinutes, $iHours2, $iMinutes2);
												$iMinutes = $iMinutes2;
												$iHours = $iHours2;
												$sSelected = $arResult['ORDER_INFO']['DELIVERY_BY_TIME'] == $sValue ? ' selected="selected"' : '';
												?><option value="<?=$sValue?>"<?=$sSelected?>><?=$sValue?></option><?
											}
										?></select><?
									?></div>
									<div class="input__wrap"><?
										$sTmpVal = $arResult['ORDER_INFO']['DELIVERY_BY_DATE'] ? $arResult['ORDER_INFO']['DELIVERY_BY_DATE'] : date('d.m.Y');
										?><input name="CUSTOMER_DELIVERY_BY_DATE" onclick="return {'inputDate': {}}" data-min-date="true" value="<?=htmlspecialcharsbx($sTmpVal)?>" class="input js-widget">
									</div>
								</div>
							</div>
						</div>
					</div><?
					//
					// Самовывоз
					//
					?><div id="pickupControls"><?
						if($arResult['DELIVERY_RESTARAUNTS']) {
							$arCities = array();
							$arRestaurants2City = array();
							$iSelectedCityId = 0;
							foreach($arResult['DELIVERY_RESTARAUNTS'] as $arItem) {
								$arCities[$arItem['CITY_ID']] = array(
									'ID' => $arItem['CITY_ID'],
									'NAME' => $arItem['CITY_NAME'],
									'SORT' => $arItem['CITY_NAME'] == 'Москва' ? 100 : 500
								);
								$arRestaurants2City[$arItem['CITY_ID']][$arItem['ID']] = array(
									'NAME' => $arItem['NAME'],
									'DEPARTMENT_ID' => $arItem['DEPARTMENT_ID'],
									'DEPARTMENT_XML_ID' => $arItem['DEPARTMENT_XML_ID'],
								);
								if($arItem['DEPARTMENT_XML_ID'] == $arResult['ORDER_INFO']['DELIVERY_RESTAURANT']) {
									$iSelectedCityId = $arItem['CITY_ID'];
								}
							}
							CProjectUtils::DoSort($arCities, 'NAME', 'ASC');
							CProjectUtils::DoSort($arCities, 'SORT', 'ASC');
							if(!$iSelectedCityId) {
								$arTmp = reset($arCities);
								$iSelectedCityId = $arTmp['CITY_ID'];
							}
							?><div class="ordering-form__row">
								<div onclick="return {'select': {}}" class="select__wrap js-widget">
									<label class="label">Город</label>
									<select name="_CUSTOMER_DELIVERY_RESTAURANT_CITY_" class="select _city"><?
										foreach($arCities as $iCityId => $arCityItem) {
											$sTmpSelected = $iCityId == $iSelectedCityId ? ' selected="selected"' : '';
											?><option value="<?=$iCityId?>"<?=$sTmpSelected?>><?=$arCityItem['NAME']?></option><?
										}
									?></select>
								</div>
							</div><?
							foreach($arRestaurants2City as $iCityId => $arCityRestaurants) {
								?><div id="<?='rest_'.$iCityId?>" class="ordering-form__row _rests">
									<div onclick="return {'select': {}}" class="select__wrap js-widget">
										<label class="label">Ресторан</label>
										<select name="_CUSTOMER_DELIVERY_RESTAURANT_[<?=$iCityId?>]" class="select _restaurant"><?
											foreach($arCityRestaurants as $arItem) {
												$sTmpSelected = $arItem['DEPARTMENT_XML_ID'] == $arResult['ORDER_INFO']['DELIVERY_RESTAURANT'] ? ' selected="selected"' : '';
												?><option value="<?=$arItem['DEPARTMENT_XML_ID']?>"<?=$sTmpSelected?>><?=$arItem['NAME']?></option><?
											}
										?></select>
									</div>
								</div><?
							}
						}
					?></div>
				</div>
				<div class="ordering-form__comment">
					<label class="label">Комментарий</label>
					<textarea name="USER_DESCRIPTION" class="textarea"><?=htmlspecialcharsbx($arResult['ORDER_INFO']['USER_DESCRIPTION'])?></textarea>
				</div>
				<div class="ordering-form__row align-left">
					<button type="button" data-current-block="2" data-next-block="3" class="btn _style_3" onclick="yaCounter24799106.reachGoal('shipping_ok'); ga ('send','pageview','/shipping_ok');  return true;">Продолжить</button>
					<?=$sOctoline?>
				</div>
			</div>
		</div><?

		?><div class="ordering-form__step">
			<div class="ordering-form__counter">3</div>
			<h2 class="ordering-form__title">Оплата</h2>
			<div class="ordering-form__body block3"><?
				$sTdStyle = '';
				if(count($arResult['PAYMENTS']) < 4) {
					$sTdStyle = ' style="width: '.floor(100 / count($arResult['PAYMENTS'])).'%;"';
				}
				if($arResult['PAYMENTS']) {
					?><table class="ordering-form__table"><?
						$iSelectedPaymentId = 0;
						if(!$arResult['PAYMENTS'][$iSelectedPaymentId]) {
							$arTmp = reset($arResult['PAYMENTS']);
							$iSelectedPaymentId = $arTmp['ID'];
						}
						$iCnt = 0;
						$sCloseTr = '';
						foreach($arResult['PAYMENTS'] as $arItem) {
							if($iCnt % 4 == 0) {
								echo $sCloseTr.'<tr>';
								$sCloseTr = '</tr>';
							}
							++$iCnt;
							echo '<td'.$sTdStyle.'>';
							$sTmpChecked = $arItem['ID'] == $iSelectedPaymentId ? ' checked="checked"' : '';
							$sTmpAddClass = $arItem['IS_CASH'] == 'Y' ? ' _cash' : '';
							?><input type="radio"<?=$sTmpChecked?> name="CUSTOMER_PAYMENT" value="<?=$arItem['ID']?>" id="<?='payment'.$arItem['ID']?>" class="radio ordering-form__radio<?=$sTmpAddClass?>"><?
							?><label for="<?='payment'.$arItem['ID']?>" class="radio-label checkbox-label checkbox-label"><?
								echo $arItem['PSA_NAME'];
								?><div class="ordering-form__payment-img"><?
									if($arItem['PSA_LOGOTIP']) {
										$arTmpImg = CFile::ResizeImageGet(
											$arItem['PSA_LOGOTIP'],
											array(
												'width' => 140,
												'height' => 83
											),
											BX_RESIZE_IMAGE_PROPORTIONAL,
											true
										);
										if($arTmpImg['src']) {
											?><img src="<?=$arTmpImg['src']?>" alt=""><?
										}
									}
								?></div><?
							?></label><?
							echo '</td>';
						}
						echo $sCloseTr;
					?></table><?

					?><div id="iscash" class="ordering-form__row">
						<div class="input__wrap">
							<label class="label">Потребуется ли сдача?</label>
							<input placeholder="С какой суммы?" name="CUSTOMER_DELIVERY_CHANGE_AMOUNT" value="<?=htmlspecialcharsbx($arResult['ORDER_INFO']['DELIVERY_CHANGE_AMOUNT'])?>" class="input">
						</div>
					</div><?
				}

				?><input type="submit" value="Заказать сейчас!" id="submitOrder" class="btn _style_3" onclick="yaCounter24799106.reachGoal('payment_ok'); ga('send','pageview','/payment_ok');  return true;"><?
				echo $sOctoline;
			?></div>
		</div>
	</form>
</div><?

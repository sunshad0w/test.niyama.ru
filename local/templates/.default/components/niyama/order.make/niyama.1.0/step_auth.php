<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Оформление заказа (шаг 2 - авторизация)
//

$GLOBALS['APPLICATION']->SetPageProperty('NIYAMA_PAGE_WRAP_ELEMENT_CLASS', '_light-2');

$APPLICATION->IncludeComponent(
	'bitrix:system.auth.form',
	'niyama.1.0.order',
	array(
	),
	null,
	array(
		'HIDE_ICONS' => 'Y'
	)
);

<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Таймлайн скидок юзера
//
//_log_array($arResult);

if(empty($arResult['LEVELS'])) {
	return;
}

$arRegistrationLevels = array();
$arOrderLevels = array();

foreach($arResult['LEVELS'] as $arItem) {
	if($arItem['PROPERTIES']['DISCOUNT_TYPE']['VALUE_XML_ID'] == 'registration') {
		$sKey1 = $arResult['USER_LEVELS'][$arItem['ID']] ? 'RICHED' : 'NOT_RICHED';
		$sKey2 = $arItem['ACTIVE'] == 'Y' ? 'ACTUAL' : 'OLD';
		$arRegistrationLevels[$sKey1][$sKey2][] = array(
			'NAME' => $arItem['NAME'],
			'RICHED' => $arResult['USER_LEVELS'][$arItem['ID']] ? 'Y' : 'N',
			'TEXT' => $arItem['PREVIEW_TEXT']
		);
	} elseif($arItem['PROPERTIES']['DISCOUNT_TYPE']['VALUE_XML_ID'] == 'order') {
		$iKey1 = doubleval($arItem['PROPERTIES']['DISCOUNT_VAL_PERC']['VALUE']);
		$sKey2 = $arResult['USER_LEVELS'][$arItem['ID']] ? 'RICHED' : 'NOT_RICHED';
		$sKey3 = $arItem['ACTIVE'] == 'Y' ? 'ACTUAL' : 'OLD';
		$arOrderLevels[$iKey1][$sKey2][$sKey3][] = array(
			'NAME' => $arItem['NAME'],
			'DISCOUNT_VAL_PERC' => doubleval($arItem['PROPERTIES']['DISCOUNT_VAL_PERC']['VALUE']),
			'RICHED' => $arResult['USER_LEVELS'][$arItem['ID']] ? 'Y' : 'N',
			'TEXT' => $arItem['PREVIEW_TEXT']
		);
	}
}

if($arOrderLevels) {
	ksort($arOrderLevels);
}

$arTmpLine = array();
$iLastRichedIdx = 0;
if($arRegistrationLevels) {
	$sKey1 = $arRegistrationLevels['RICHED'] ? 'RICHED' : 'NOT_RICHED';
	if($arRegistrationLevels[$sKey1]) {
		$sKey2 = $arRegistrationLevels[$sKey1]['ACTUAL'] ? 'ACTUAL' : 'OLD';
		foreach($arRegistrationLevels[$sKey1][$sKey2] as $arItem) {        	
			$arTmpLine[] = array(
				'NAME' => $arItem['NAME'],
				'RICHED' => $arItem['RICHED'],
				'TEXT' => $arItem['TEXT']
			);
			if($arItem['RICHED'] == 'Y') {
				$iLastRichedIdx = count($arTmpLine) - 1;
			}
		}
	}
}
if($arOrderLevels) {
	foreach($arOrderLevels as $arItems) {
		$sKey1 = $arItems['RICHED'] ? 'RICHED' : 'NOT_RICHED';
		if($arItems[$sKey1]) {
			$sKey2 = $arItems[$sKey1]['ACTUAL'] ? 'ACTUAL' : 'OLD';
			if($arItems[$sKey1][$sKey2]) {
				foreach($arItems[$sKey1][$sKey2] as $arItem) {
					$arTmpLine[] = array(
						'NAME' => $arItem['NAME'],
						'RICHED' => $arItem['RICHED'],
						'TEXT' => $arItem['TEXT'],
					);
					if($arItem['RICHED'] == 'Y') {
						$iLastRichedIdx = count($arTmpLine) - 1;
					}
				}
			}
		}
	}
}

$arCells = array('_one', '_two', '_three', '_four');
$arCells2Width = array(
	'_one' => '11%', 
	'_two' => '35%', 
	'_three' => '64%', 
	'_four' => '90%'
);
$sRichedLevelClass = '';
?><div class="achievements pull-left js-widget" onclick="return {'achievements': {}}">
	<div class="achievements__title">МОИ ДОСТИЖЕНИЯ</div>
	<div class="achievements__bar"><?

		$iCnt = count($arTmpLine);
		$iFirstIdx = 0;
		if($iCnt > 4) {
			if(($iCnt - $iLastRichedIdx) >= 3) {
				$iFirstIdx = $iLastRichedIdx - 1;
			} else {
				$iFirstIdx = $iCnt - 4;
			}
		}
		$iFirstIdx = $iFirstIdx < 0 ? 0 : $iFirstIdx;

		foreach($arTmpLine as $iIdx => $arItem) {
			if($iIdx >= $iFirstIdx && $arCells) {
				$sTmpClass = array_shift($arCells);

				?><a class="achievements__link <?=$sTmpClass?>"><?
					if($arItem['TEXT']) {
						?><span class="achievements__tooltip"><?=$arItem['TEXT']?></span><?
					}
					?><span><?=$arItem['NAME']?></span><?
				?></a><?
				if($arItem['RICHED'] == 'Y') {
					$sRichedLevelClass = $sTmpClass;
				}
			}
		}
		$sWidth = $sRichedLevelClass && $arCells2Width[$sRichedLevelClass] ? $arCells2Width[$sRichedLevelClass] : '0';
		?><div class="achievements__bar-progress" style="width:<?=$sWidth?>"></div><?
	?></div>
</div><?

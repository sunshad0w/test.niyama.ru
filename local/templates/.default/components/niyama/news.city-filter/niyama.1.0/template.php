<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//
// Новости: фильтр по городам/ресторанам
//
//_show_array($arResult);

if($arResult['NEWS_FOUND'] != 'Y') {
	return;
}

$sCurFilterTmp = isset($_REQUEST['restaurants']) ? trim($_REQUEST['restaurants']) : '';
$sCurFilter = '';
$sDefaultFilter = '';

if($arResult['R_CITIES'] || $arResult['R_RESTAURANTS']) {
	?><div class="news__rests"><?
		?><div onclick="return {'select': {}}" class="select__wrap js-widget"><?
			$sLabelVal = '';
			ob_start();
			?><select name="restaurants" class="select"><?
				$sRedirectUrl = CProjectUtils::GetCurPageParam('', array('restaurants', 'PAGEN_1', 'PAGEN_2', 'PAGEN_3', 'PAGEN_4', 'PAGEN_5'), true, false);
				$sRedirectUrl = strpos($sRedirectUrl, '?') === false ? $sRedirectUrl.'?restaurants=' : $sRedirectUrl.'&restaurants=';
				foreach($arResult['CITIES'] as $iCityId => $arCity) {
					$arFilterRestaurants = array();
					if($arResult['R_RESTAURANTS'] && $arResult['CITY-RESTAURANTS'][$iCityId]) {
						foreach($arResult['CITY-RESTAURANTS'][$iCityId] as $iRestaurantId) {
							if($arResult['RESTAURANTS'][$iRestaurantId] && $arResult['R_RESTAURANTS'][$iRestaurantId]) {
								$arFilterRestaurants[] = array(
									'NAME' => $arResult['RESTAURANTS'][$iRestaurantId]['NAME'],
									'ID' => $iRestaurantId,
								);
							}
						}
					}
					$bShowCity = false;
					if(count($arFilterRestaurants) > 1 || $arResult['R_CITIES'][$iCityId]) {
						$bShowCity = true;
					}

					if($bShowCity) {
						$sOptionText = 'Все рестораны '.$arCity['PROPERTY_S_NAME_RP_VALUE'];
						$sTmpOptionVal = 'c'.$iCityId;
						$sCurFilterTmp = strlen($sCurFilterTmp) ? $sCurFilterTmp : $sTmpOptionVal;
						$sTmpSelected = '';
						if($sCurFilterTmp == $sTmpOptionVal) {
							$sTmpSelected = ' selected="selected"';
							$sCurFilter = $sCurFilterTmp;
							$sLabelVal = $sOptionText;
						}
						?><option<?=$sTmpSelected?> value="<?=htmlspecialcharsbx($sRedirectUrl.$sTmpOptionVal)?>"><?=$sOptionText?></option><?
						if(!$sDefaultFilter) {
							$sDefaultFilter = $sTmpOptionVal;
						}
					}
					if($arFilterRestaurants) {
						foreach($arFilterRestaurants as $arRestaurant) {
							$sOptionText = $arRestaurant['NAME'];
							$sTmpOptionVal = 'r'.$arRestaurant['ID'];
							$sCurFilterTmp = strlen($sCurFilterTmp) ? $sCurFilterTmp : $sTmpOptionVal;
							$sTmpSelected = '';
							if($sCurFilterTmp == $sTmpOptionVal) {
								$sTmpSelected = ' selected="selected"';
								$sCurFilter = $sCurFilterTmp;
								$sLabelVal = $sOptionText;
							}
							?><option<?=$sTmpSelected?> value="<?=htmlspecialcharsbx($sRedirectUrl.$sTmpOptionVal)?>"><?=$sOptionText?></option><?
							if(!$sDefaultFilter) {
								$sDefaultFilter = $sTmpOptionVal;
							}
						}
					}
				}
			?></select><?
			$sTmpSelectHtml = ob_get_clean();

			?><label class="label"><?=$sLabelVal?></label><?
			echo $sTmpSelectHtml;

		?></div><?
	?></div><?
}

// генерируем внешний фильтр
if(!strlen($sCurFilter) && strlen($sDefaultFilter)) {
	$sCurFilter = $sDefaultFilter;
}

$arOrFilters = array();
if($sCurFilter) {
	if(strpos($sCurFilter, 'c') === 0) {
		// фильтр по всем ресторанам города	
		$iCityId = substr($sCurFilter, 1);
		$arOrFilters['LOGIC'] = 'OR';

		$arOrFilters[] = array(
			'=PROPERTY_R_CITY' => $iCityId
		);
		if($arResult['CITY-RESTAURANTS'][$iCityId]) {
			$arOrFilters[] = array(
				'=PROPERTY_R_RESTAURANT' => array_keys($arResult['CITY-RESTAURANTS'][$iCityId])			
			);
		}
		$arOrFilters[] = array(
			'=PROPERTY_R_CITY' => false,
			'=PROPERTY_R_RESTAURANT' => false
		);
	} else {
		// фильтр по ресторану
		$iRestaurantId = substr($sCurFilter, 1);
		$arOrFilters['LOGIC'] = 'OR';
		if($arResult['RESTAURANTS'][$iRestaurantId]['PROPERTY_R_CITY_VALUE']) {
			$arOrFilters[] = array(
				'=PROPERTY_R_CITY' => $arResult['RESTAURANTS'][$iRestaurantId]['PROPERTY_R_CITY_VALUE']
			);
		}
		$arOrFilters[] = array(
			'=PROPERTY_R_RESTAURANT' => $iRestaurantId			
		);
		$arOrFilters[] = array(
			'=PROPERTY_R_CITY' => false,
			'=PROPERTY_R_RESTAURANT' => false
		);
	}
}

if($arOrFilters) {
	$GLOBALS['arNewsListFilterEx'] = array(
		$arParams['_EXT_FILTER_'],
		array(
			$arOrFilters
		)
	);
} else {
	$GLOBALS['arNewsListFilterEx'] = array($arParams['_EXT_FILTER_']);
}

//_show_array($GLOBALS['arNewsListFilterEx']);

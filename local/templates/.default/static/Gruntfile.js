module.exports = function (grunt) {
	var path = require('path');
	var blocksFolder = 'src/blocks/composite'; // папка в которой лежат папки с блоками
	var tplFolder = 'templates';  // папка с шаблонами


/*  CONFIG
--------------------------------------------------------------------------------------------------------- */
	grunt.initConfig({
		compiledPage: '*', // страница которая будет компилироваться. По-умолчанию *, т.е. все страницы
		pkg: grunt.file.readJSON("package.json"),
		// Autoreload
		browser_sync: {
			dev: {
				bsFiles: {
					src : ['dest/css/*.css', 'dest/*.html', 'dest/js/*.js']
				},
				options: {
					server: {
						baseDir: "dest/" // Корневая папка для статичнго сервера.
					},
					open: false, // Не открывать страницу с сайтом при создании сервера.
					notify: false, // Не показывать уведомлений в окне браузера.
					watchTask: true // Отключить watch, так как для этого используется отдельный таск.
				}
			}
		},
		// Конкатенация файлов
		concat: {
			jade: {
				src:  'src/blocks/**/*.jade',
				dest: 'src/jade/_mixins.jade'
			},
			js: {
				options: {
					separator: ';',
					stripBanners: true
				},
				src: [
					"src/js/jquery-1.8.3.js",
					// "src/js/jquery-1.11.1.js",
					// "src/js/jquery-2.0.0.min.js",
					// "src/js/jquery-ui-1.10.4.custom.min.js",
					"src/js/jquery-ui-1.10.4.custom.js",
					"src/js/jquery.mousewheel.js",
					"src/js/plugins/*.js",
					"src/js/core.js",
					"src/blocks/base/**/*.js",
					"src/blocks/composite/**/*.js",
					"src/js/initial.js"
				],
				dest: 'dest/js/main.js'
			},
			less: {
				src: [
					"src/less/libs/normalize.less",
					"src/less/variables.less",
					"src/less/helpers.less",
					"src/less/base.less",
					"src/less/plugins/*.less",
					"src/blocks/base/**/*.less",
					"src/blocks/composite/**/*.less",
					"src/less/utils.less"
				],
				dest: 'src/less/_main.less'
			}
		},
		less: {
			compile: {
				src:  "src/less/_main.less",
				dest: "dest/css/main.css"
			}
		},
		jade: {
			compile: {
				options: {
					pretty: true
				},
				expand: true,
				flatten: true,
				src:  "src/<%= compiledPage %>.jade",
				dest: "dest/",
				ext: ".html"
			}
		},
		copy: {
			images: {
				files: [
					{
						expand:  true,
						cwd:    'src/images',
						src:     ['**/*.png', '**/*.jpg', '**/*.gif', '**/*.svg'],
						dest:   'dest/images'
					}, 
					{
						expand:  true,
						flatten: true,
						cwd:    'src/blocks',
						src:     ['**/*.png', '**/*.jpg', '**/*.gif', '**/*.svg'],
						dest:   'dest/images/'
					}
				]
			},
			fonts: {
				expand:  true,
				flatten: true,
				cwd:    'src/fonts',
				src:     ['**/*.woff', '**/*.ttf', '**/*.eot', '**/*.svg'],
				dest:   'dest/fonts/'
			}
			// ,dest: {
			// 	expand:  true,
			// 	flatten: false,
			// 	cwd:    'dest/',
			// 	src:     ['**'],
			// 	dest:   '/home/superadmin/dev/vagrant/niyama/www/local/templates/.default/static/dest/'
			// }
		},
		watch: {
			options: {
				spawn: false
			},
			less: {
				files: ['src/blocks/**/*.less', 'src/less/**/*.less'],
				tasks: ['concat:less', 'less', 'autoprefixer:dev']
			},
			jade: {
				files: ['src/<%= compiledPage %>.jade', 'src/blocks/**/*.jade', 'src/jade/**/*.jade'],
				tasks: ['concat:jade', 'jade']
			},
			js: {
				files: ['src/blocks/**/*.js', 'src/js/**/*.js'],
				tasks: ['concat:js']
			}
		},
		autoprefixer: {
			// в режиме разработки добавляем префиксы только для последних версий браузеров
			dev: {
				options: {
					browsers: ['last 1 version']
				},
				src: 'dest/css/main.css'
			},
			// добавляем префиксы всем необходимым браузерам
			prod: {
				options: {
					browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
				},
				src: 'dest/css/main.css'
			}
		},
		jshint: {
			options: {
				curly		: true,
				eqeqeq		: true,
				eqnull		: true,
				browser		: true,
				newcap		: true,
				quotmark	: true,
				globalstrict: true,
				strict		: true,
				onevar		: true,
				unused		: true,
				maxdepth	: 4,
				undef		: true,
				globals		: {
					jQuery		: true,
					ymaps		: true,
					$			: true,
					Modernizr	: true,
					console		: true
				}
			},
			files: ['src/blocks/base/**/*.js', 'src/blocks/composite/**/*.js']
		},
		cmq: {
			simple: {
				src: 'dest/css/main.css',
				dest: 'dest/css/main.css'
			}
		},
		uglify: {
			compile: {
				src:  "dest/js/main.js",
				dest: "dest/js/main.js"
			}
		}
		// 
		// compress: {
		// 	main: {
		// 		options: {
		// 			archive: 'files/backup/<%= pkg.name %> верстка (<%= grunt.template.today("dd.mm.yyyy") %>).zip',
		// 			mode: 'zip'
		// 		},
		// 		src: 'dest/**'
		// 	}
		// }
	});
/*------------------------------------------------------------------------------------------------------- */

/*  LOAD TASKS
--------------------------------------------------------------------------------------------------------- */
require('load-grunt-tasks')(grunt);
/*------------------------------------------------------------------------------------------------------- */


/*  TASKS
/*------------------------------------------------------------------------------------------------------- */
	/* ДЕФОЛТНЫЙ ТАСК ДЛЯ РАЗРАБОТКИ. */
	grunt.registerTask('default', function (page) {
		if (page && !grunt.file.exists('src/' + page + '.jade')) {
			grunt.log.error('Страница ' + page + ' не найдена.');
			return false;
		}
		// Что бы при изменении одной страницы не компилировались все имеющиеся страницы
		// мы может передать имя страницы для компиляции используя ключ --page
		grunt.config.set('compiledPage', page || '*');

		// Выполняем дефолтные таски
		grunt.task.run(['concat', 'jade', 'less', 'copy', 'autoprefixer:dev', 'browser_sync', 'watch']);
	});

	/* ТАСК ДЛЯ ПРОДАКШЕНА */
	// grunt.registerTask('prod', ['jshint', 'concat', 'uglify', 'jade', 'less', 'copy', 'autoprefixer:prod', 'cmq']);
	grunt.registerTask('prod', ['concat', 'jade', 'less', 'copy', 'autoprefixer:prod', 'cmq']);
	grunt.registerTask('pdev', ['concat', 'less', 'copy', 'autoprefixer:prod', 'cmq']);

		/* ТАСК ДЛЯ СОЗДАНИЯ ФАЙЛОВ БЛОКА */
	grunt.registerTask('create', function () {
		var name = grunt.option('name') ? grunt.option('name').split(',') : [],
			tech = grunt.option('tech') ? grunt.option('tech').split(',') : ['js', 'jade', 'less'],
			blockFolder, // папка для конкретного блока
			i;

		// создаваемый блок обязан иметь имя
		if (!name.length) {
			grunt.log.error("Введите имя блока");
			return;
		}
		
		for (i = 0; i < name.length; i++) {
			blockFolder = path.normalize(path.join(blocksFolder, name[i]));

			// Не создаем блок если он уже существует
			if (grunt.file.exists(blockFolder)) {
				grunt.log.error("Блок с именем «" + name[i] + "» уже существует");
				continue;
			}

			// Всегда создаем папку для картинок. Автоматически создатся и папка блока
			grunt.file.mkdir(path.normalize(path.join(blockFolder, "images")));
			// создаем файлы для каждой технологии
			createTechFile(name[i], blockFolder, tech);
			grunt.log.success("Блок «" + name[i] + "» успешно создан");
		}
	});

	grunt.registerTask('update', function () {
		var name = grunt.option('name'),
			tech = grunt.option('tech') ? grunt.option('tech').split(',') : undefined,
			blockFolder;

		if (name === undefined) {
			grunt.log.error('Введите имя блока');
			return;
		}
		if (tech === undefined) {
			grunt.log.error('Введите название технологии');
			return;
		}

		blockFolder = path.normalize(path.join(blocksFolder, name));

		if (!grunt.file.exists(blockFolder)) {
			grunt.log.error('Блока с именем «' + name + '» не существует');
			return;
		}

		// создаем файлы для каждой технологии
		createTechFile(name, blockFolder, tech);
		grunt.log.success("Блок «" + name + "» успешно обновлен");
	});

	grunt.registerTask('rename', function () {
		var fs             = require('fs'),
			path           = require('path'),
			name           = grunt.option('name'),
			newName        = grunt.option('new'),
			blocksFolder   = 'src/blocks',
			blockFolder    = path.normalize(path.join(blocksFolder, name)),
			newBlockFolder = path.normalize(path.join(blocksFolder, newName)),
			techs          = ['js', 'jade', 'less'];

		// errors handlers
		if (!name) {
			grunt.log.error("Введите имя блока, который вы хотите переименовать");
			return;
		}
		if (!newName) {
			grunt.log.error("Введите новое имя блока");
			return;
		}
		if (!grunt.file.exists(blockFolder)) {
			grunt.log.error("Блока «" + name + "» не существует");
			return;
		}
		if (grunt.file.exists(newBlockFolder)) {
			grunt.log.error("Блок c именем «" + newName + "» уже существует");
			return;
		}

		//rename files
		techs.forEach(function (item, index) {
			var filePath    = path.normalize(path.join(blockFolder, name + '.' + item)),
				newFilePath = path.normalize(path.join(blockFolder, newName + '.' + item));

			if (grunt.file.isFile(filePath)) {
				fs.renameSync(filePath, newFilePath);
			}
		});

		// rename folder
		fs.renameSync(blockFolder, newBlockFolder);
	});


	/* ТАСК ДЛЯ СОЗДАНИЯ ФАЙЛОВ БЛОКА */
	// grunt.registerTask('create', function (type, name, tech) {
	// 	name = name ? name.split(',') : [];
	// 	tech = tech ? tech.split(',') : ['js', 'jade', 'less'];
	// 	var blockTypeFolder = type === 'b' ? 'base' : 'composite';
	// 	var blockFolder; // папка для конкретного блока
	// 	var i;

	// 	// создаваемый блок обязан иметь имя
	// 	if (!name.length) {
	// 		grunt.log.error("Введите имя блока");
	// 		return false;
	// 	}
		
	// 	for (i = 0; i < name.length; i++) {
	// 		blockFolder = path.normalize(path.join(blocksFolder, blockTypeFolder, name[i]));

	// 		// Не создаем блок если он уже существует
	// 		if (grunt.file.exists(blockFolder)) {
	// 			grunt.log.error("Блок с именем «" + name[i] + "» уже существует");
	// 			return false;
	// 		}

	// 		// Всегда создаем папку для картинок. Автоматически создатся и папка блока
	// 		grunt.file.mkdir(path.normalize(path.join(blockFolder, "images")));
	// 		// создаем файлы для каждой технологии
	// 		createTechFile(name[i], blockFolder, tech);
	// 		grunt.log.success("Блок «" + name[i] + "» успешно создан");
	// 	}
	// });

	// grunt.registerTask('update', function (type, name, tech) {
	// 	name = name ? name.split(',') : [];
	// 	tech = tech ? tech.split(',') : [];
	// 	var blockTypeFolder = type === 'b' ? 'base' : 'composite';
	// 	var blockFolder;
	// 	var i;

	// 	if (!type.length) {
	// 		grunt.log.error('Введите тип блока');
	// 		return false;
	// 	}

	// 	if (!name.length) {
	// 		grunt.log.error('Введите имя блока');
	// 		return false;
	// 	}
	// 	if (!tech.length) {
	// 		grunt.log.error('Введите название технологии');
	// 		return false;
	// 	}

	// 	for (i = 0; i < name.length; i++) {
	// 		blockFolder = path.normalize(path.join(blocksFolder, blockTypeFolder, name[i]));

	// 		if (!grunt.file.exists(blockFolder)) {
	// 			grunt.log.error('Блока с именем «' + name[i] + '» не существует');
	// 			return false;
	// 		}

	// 		// создаем файлы для каждой технологии
	// 		createTechFile(name[i], blockFolder, tech);
	// 		grunt.log.success("Блок «" + name + "» успешно обновлен");
	// 	}
	// });

	// ---------------------------------------------------

	// grunt.registerTask('rename', function () {
	// 	var fs             = require('fs');
	// 	var path           = require('path');
	// 	var name           = grunt.option('name');
	// 	var newName        = grunt.option('new');
	// 	var blocksFolder   = 'src/blocks';
	// 	var blockFolder    = path.normalize(path.join(blocksFolder, name));
	// 	var newBlockFolder = path.normalize(path.join(blocksFolder, newName));
	// 	var techs          = ['js', 'jade', 'less'];

	// 	// errors handlers
	// 	if (!name) {
	// 		grunt.log.error("Введите имя блока, который вы хотите переименовать");
	// 		return;
	// 	}
	// 	if (!newName) {
	// 		grunt.log.error("Введите новое имя блока");
	// 		return;
	// 	}
	// 	if (!grunt.file.exists(blockFolder)) {
	// 		grunt.log.error("Блока «" + name + "» не существует");
	// 		return;
	// 	}
	// 	if (grunt.file.exists(newBlockFolder)) {
	// 		grunt.log.error("Блок c именем «" + newName + "» уже существует");
	// 		return;
	// 	}

	// 	//rename files
	// 	techs.forEach(function (item, index) {
	// 		var filePath    = path.normalize(path.join(blockFolder, name + '.' + item)),
	// 			newFilePath = path.normalize(path.join(blockFolder, newName + '.' + item));

	// 		if (grunt.file.isFile(filePath)) {
	// 			fs.renameSync(filePath, newFilePath);
	// 		}
	// 	});

	// 	// rename folder
	// 	fs.renameSync(blockFolder, newBlockFolder);
	// });

	function getTemplate (tech) {
		return grunt.file.read(path.join(tplFolder, tech + '.txt'));
	}
	function toCamelCase (str) {
		return str.split('-').reduce(function (result, item, index) {
			if (index === 0 ) return item; // первое слово оставляем как есть

			var str = item[0].toUpperCase() + item.slice(1, item.length);

			return result + str;
		}, '');
	}
	function createTechFile(blockName, folderPath, tech) {
		var techPath;
		for (j = 0; j < tech.length; j++) {
			techPath = path.normalize(path.join(folderPath + "/" + blockName + "." + tech[j]));

			if (grunt.file.exists(techPath)) {
				grunt.log.error('файл с технологией ' +  tech[j] + ' уже существует');
				continue;
			}

			switch (tech[j]) {
				case 'jade':
					tpl = getTemplate('jade');
					tpl = tpl.replace(/{{blockname}}/g, blockName);
					break;
				case 'less':
					tpl = getTemplate('less');
					tpl = tpl.replace(/{{blockname}}/g, blockName);
					break;
				case 'js':
					tpl = getTemplate('js', blockName);
					tpl = tpl.replace(/{{blockname}}/g, toCamelCase(blockName));
					tpl = tpl.replace(/{{projectname}}/g, grunt.config('pkg').name);
					break;
				default:
					tpl = '';
			}

			grunt.file.write(techPath, tpl);
		};
	}
};

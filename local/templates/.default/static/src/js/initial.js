$(document).ready(function() {
	var $popup = $('.popup'),
		$body = $('body');



	function setInputMasks (argument) {
		var $phoneMask = $('._phone-mask'),
			$birthdayMask = $('._birthday-mask');

		if ($phoneMask.length) {
			$phoneMask.mask("+7 (999) 999-99-99");
		}

		if ($birthdayMask.length) {
			$birthdayMask.mask("99.99.9999");
		}
	}
	setInputMasks();

	$('.fancybox').fancybox({
		// ajax: { cache: true },

		wrapCSS: 'fancybox-custom',

		beforeShow: function(){
			if ($('._cabinet').length) {
		 		$(".fancybox-skin").css("backgroundColor","#f0e8de");
			}
		},

		afterShow : function () {

			$.initWidgets();
			setInputMasks();

			// Для дропдауна в истории заказов
			var $inner = $('.fancybox-inner');
			if ( $inner.find('.order-history__table').length ) {
				$inner.css({
					'overflow': 'auto',
					'minHeight': '300px'
				});
				$.fancybox.update();
			}

			var $popupUserBody = $inner.find('.popup__user-body');

			if ( $popupUserBody.length ) {
				$inner.css({
					'overflow': 'hidden'
				});

				$popupUserBody.jScrollPane({
					autoReinitialise: true
				});

				$.fancybox.update();

				$('.fancybox-skin').css({
					"background": "#f0e8de"
				})
			}

			if ( $inner.find('form').length ) {
				$inner.find('form').parsley();
			}

			$popup.detach();

			$('#fileupload').fileupload({
			    url: '/ajax/uploadAvatar.php',
			    dataType: 'json',
			    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			    maxFileSize: 5000000, // 5 MB
			    disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
			    previewMaxWidth: 100,
			    previewMaxHeight: 100,
			    previewCrop: true,
			    done: function(e, data) {
			        $('#progress').fadeOut();
			        $('#files').data('id', data.result.id).find('img').prop('src', data.result.src);
                    $('#avatar_id').val(data.result.id);
			    }

			}).on('fileuploadprogressall', function (e, data) {

			    $('#progress').show();
			    var progress = parseInt(data.loaded / data.total * 100, 10);
			    $('#progress .progress-bar').css('width', progress + '%');

			});

			$('.popup__user-change-pass').on('click', function () {
				$('.row._passwords').slideToggle();
			});

			$inner.find('input[type="submit"]').on('click', function () {
				var $this = $(this);

				setTimeout(function () {
					if ( !$inner.find('.parsley-errors-list.filled').length ) {
						$this.off('click');

						$this.on('click', function () {
							return false;
						});
					} 
				}, 5);
			});

			$inner.find('.chosen-container .chosen-results').on('touchend', function(event) {
			    event.stopPropagation();
			    event.preventDefault();
			    return;
			});
		},

		afterClose: function () {
			$popup.appendTo($body);
		}


	});

	$('.fancybox-gallery').fancybox({
		wrapCSS: 'fancybox-gallery',
		padding: [8,8,8,8]
	});

	$('.auth__input').on('focus', function () {
		var $this = $(this);

		$this.next('.auth__error').html('&nbsp;');

		$('.auth__error').filter('[rel=' + $this.attr('id') + ']' ).html('&nbsp;');

		if ( !$this.siblings('.parsley-errors-list.filled').length ) {
			$this.removeClass('parsley-error');
		}
	});

	$('.chosen-container .chosen-results').on('touchend', function(event) {
	    event.stopPropagation();
	    event.preventDefault();
	    return;
	});

});


//Parsley localization for Russian language
//Evgeni Makarov
//github.com/emakarov


// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

// Define then the messages
window.ParsleyConfig.i18n.ru = $.extend(window.ParsleyConfig.i18n.ru || {}, {
	defaultMessage: "Некорректное значение.",
	type: {
		email:        "Введите корректный адрес почты.",
		url:          "Введите URL адрес.",
		number:       "Введите число.",
		integer:      "Введите целое число.",
		digits:       "Введите только цифры.",
		alphanum:     "Введите буквенно-цифровое значение."
	},
	notblank:       "Это поле должно быть заполнено.",
	required:       "Обязательное поле.",
	pattern:        "Это значение некорректно.",
	min:            "Это значение должно быть не менее чем %s.",
	max:            "Это значение должно быть не более чем %s.",
	range:          "Это значение должно быть в интервале от %s до %s.",
	minlength:      "Введите не менее %s символов.",
	maxlength:      "Введите не более %s символов.",
	length:         "Поле должно содержать от %s до %s символов.",
	mincheck:       "Выберите не менее %s значений.",
	maxcheck:       "Выберите не более %s значений.",
	check:          "Выберите от %s до %s значений.",
	equalto:        "Пароли должны совпадать"
});



// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator) {
	window.ParsleyValidator.addCatalog('ru', window.ParsleyConfig.i18n.ru, true)

	 window.ParsleyValidator
	  .addValidator('phonenumber', function (value, requirement) {
	  		// console.log(arguments);
	    	if ( /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value) ) {
	      		return true;
	      	} else {
	      		// console.log('nope');
	      		return false;
	      	}
	  }, 32)
	  .addMessage('ru', 'phonenumber', 'Неверный формат номера телефона');
}


$.tablesorter.addParser({
    // set a unique id
    id: 'time_ddmmyyyy',
    is: function(s) {
        // return false so this parser is not auto detected
        return false;
    },
    format: function(s) {
        // format your data for normalization
        var strDate = s;
        strDate = strDate.substring(0, 10); // takes only the date from "19.06.2012 10:40 Uhr"
        var dateParts = strDate.split(".");
        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
        // console.log(date);
        return date;
    },
    // set type, either numeric or text
    type: 'numeric'
}); 
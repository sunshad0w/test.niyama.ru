;(function ($) {
	'use strict';

	$.widget('niyama.promo', {
		_create: function () {
			this.$slider = this.element.find('.promo__slider');
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({

			});
		},
		_initPlugins: function () {
			var interval = this.element.data('interval') || 5000;
			var self = this;

			this.$slider.owlCarousel({
				items: 3,
				navigation : true,
				lazyLoad : true,
				itemsDesktop : [1200, 2], //5 items between 1000px and 901px
				itemsDesktopSmall : false, // betweem 900px and 601px
				itemsTablet: false, //2 items between 600 and 0
				itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
				singleItem: false,
				pagination: false,
				rewindNav: false,
				autoPlay: interval,
				stopOnHover : true,
				afterMove: function () {
					if ( this.currentItem === this.maximumItem ) {
						setTimeout(function () {
							self.$slider.data('owlCarousel').goTo(0);
						}, interval);
					};
				}
			});
		}
	});
})(jQuery);
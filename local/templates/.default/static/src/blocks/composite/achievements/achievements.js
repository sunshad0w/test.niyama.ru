;(function ($) {
	'use strict';

	$.widget('niyama.achievements', {
		_create: function () {

			this.$links = this.element.find('.achievements__link');

			this._initEvents();
		},
		_initEvents: function () {
			var self = this;

			this._on({
				'click .achievements__link': this._handleLinkClick
			});

			this._on(
			    window,
			    {
			        "click": function (e) {
			            var target = $(e.target);

			            if ( !target.closest(self.element).length && !target.hasClass('achievements__link') ) {
			                this.$links.removeClass('_active');
			            }

			        }
			    }
			);
		},
		_handleLinkClick: function (e) {
			var $target = $(e.currentTarget);

 			this.$links.not($target).removeClass('_active');

			$target.toggleClass('_active');
		}
	});
})(jQuery);
(function ($) {
	'use strict';

	$.widget('niyama.mainSlider', {
		_create: function () {
			this._initPlugins();
		},

		_initPlugins: function () {
			this.element.find('.main-slider__slider').owlCarousel({
				 
				      navigation : true, // Show next and prev buttons
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      singleItem:true,
				      lazyLoad : true,
				      mouseDrag: false,
				      autoHeight : true
				      // pagination: true
				      // "singleItem:true" is a shortcut for:
				      // items : 1, 
				      // itemsDesktop : false,
				      // itemsDesktopSmall : false,
				      // itemsTablet: false,
				      // itemsMobile : false
				  });
			// this.element.find('.main-slider__slider').carouFredSel({
			// 	prev: $('.main-slider__prev'),
			// 	next: $('.main-slider__next')
			// });
			
			
			// this.element.find('.main-slider__slider').slick({
			// 	dots: true,
			// 	customPaging: function(slider, i) {
			// 		return '<span type="button"></span>';
			// 	},
			// });
		}
	});
})(jQuery);
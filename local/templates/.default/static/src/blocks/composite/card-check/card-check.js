;(function ($) {
	'use strict';

	$.widget('niyama.cardCheck', {
		_create: function () {
			this.$form = this.element.find('form');
			this.url = this.$form.prop('action');

			this._initEvents();
		},

		_initEvents: function () {
			this._on({
				'submit form': this._handleFormSubmit
			});
		},

		_handleFormSubmit: function (e) {
			$.ajax({
				type: "POST",
				url: this.url,
				data: this.$form.serialize(),
				success: $.proxy(this._ajaxSuccess, this)
			});

			return false;
		},

		setInputMasks: function () {
			var $phoneMask = $('._phone-mask'),
				$birthdayMask = $('._birthday-mask');

			if ($phoneMask.length) {
				$phoneMask.mask("+7 (999) 999-99-99");
			}

			if ($birthdayMask.length) {
				$birthdayMask.mask("99.99.9999");
			}
		},

		_ajaxSuccess: function (data) {
			var $popup = $('.popup'),
			$body = $('body');

			this.element.replaceWith($(data));

			$.initWidgets();
			$.fancybox.update();

			this.setInputMasks();

			// Для дропдауна в истории заказов
			var $inner = $('.fancybox-inner');
			if ( $inner.find('.order-history__table').length ) {
				$inner.css({
					'overflow': 'auto',
					'minHeight': '300px'
				});
				$.fancybox.update();
			}

			var $popupUserBody = $inner.find('.popup__user-body');

			if ( $popupUserBody.length ) {
				$inner.css({
					'overflow': 'hidden'
				});

				$popupUserBody.jScrollPane({
					autoReinitialise: true
				});

				$.fancybox.update();

				$('.fancybox-skin').css({
					"background": "#f0e8de"
				})
			}

			if ( $inner.find('form').length ) {
				$inner.find('form').parsley();
			}

			$popup.detach();

			$('#fileupload').fileupload({
			    url: '/ajax/uploadAvatar.php',
			    dataType: 'json',
			    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			    maxFileSize: 5000000, // 5 MB
			    disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
			    previewMaxWidth: 100,
			    previewMaxHeight: 100,
			    previewCrop: true,
			    done: function(e, data) {
			        $('#progress').fadeOut();
			        $('#files').data('id', data.result.id).find('img').prop('src', data.result.src);
                    $('#avatar_id').val(data.result.id);
			    }

			}).on('fileuploadprogressall', function (e, data) {

			    $('#progress').show();
			    var progress = parseInt(data.loaded / data.total * 100, 10);
			    $('#progress .progress-bar').css('width', progress + '%');

			});

			$('.popup__user-change-pass').on('click', function () {
				$('.row._passwords').slideToggle();
			});

			$inner.find('input[type="submit"]').on('click', function () {
				var $this = $(this);

				setTimeout(function () {
					if ( !$inner.find('.parsley-errors-list.filled').length ) {
						$this.off('click');

						$this.on('click', function () {
							return false;
						});
					} 
				}, 5);
			});
		}
	});
})(jQuery);
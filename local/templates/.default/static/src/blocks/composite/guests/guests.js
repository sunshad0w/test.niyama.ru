;(function ($) {
	'use strict';

	$.widget('niyama.guests', {
		_create: function () {

			// список возможных гостей и враппер для него 
			this.$addList = this.element.find('.guests__add-list');
			this.$addWrap = this.element.find('.guests__add-wrap');

			// список приглашённых гостей и враппер для него 
			this.$inviteList = this.element.find('.guests__list-list');
			this.$inviteWrap = this.element.find('.guests__list-wrap');
			this.$inviteItem = this.element.find('.guests__card._big');
			this.$inviteMore = this.element.find('.guests__list-choose');
			this.$inviteRemove = this.element.find('.guests__card-remove');

			// Список добавленных гостей
			this.$guestsListWrap = this.element.find('.guests__list-wrap');

			this._initEvents();
			this._initPlugins();

			this._initPopup();
		},
		_initEvents: function () {
			var self = this; 

			this._on({
				'click .guests__add-sign': this._handleAddGuestClick,
				'click .guests__add-button': this._handleAddGuestClick,

				// Выпадашка для выбора корзины гостя
				'click .guests__list-choose': this._handleChooseGuestCart,
				// Удаление гостя
				'click .guests__card-remove': this._handleChooseGuestRemove,

				// Пункт "Новый гость" в списке
				'click .guests__card._current': this._handleOpenCartClick,
				// Пункт "Новый гость" в списке
				'click .guests__card._new': this._handleAddNewGuestClick,
				// Пункт "Добавить гостя" в списке
				'click .guests__card._all': this._handleChangeGuestAdd,
				// Пункт "Выбрать корзину гостя" в списке
				'click .guests__card._big': this._handleChangeGuestCart
			});

			this._on(
				window,
				{
					"click": function (e) {
						var target = $(e.target);

						if (!target.closest(self.element).length) {
							self._hideList();
						}
					}
				}
			);

			if (this.$inviteItem.length > 0) {
				this.$inviteMore.show();
				this.$inviteRemove.addClass('_show');
			}
		},

		_initPlugins: function () {
		},

		_initPopup: function () {
			this.$removePopup = $('<div class="goods-one _removed"><div class="goods-one__img"><img src=""/></div><div class="goods-one__desc"><p class="goods-one__about"></p></div><div class="goods-one__side"><a class="btn _full-width _style_1 goods-one__add">Вернуть</a></div></div>');
		},

		_handleAddGuestClick: function (e) {
			var self = this;
			this.element.toggleClass('_opened');

			this.$addWrap.slideToggle('fast', function () {
				self.$addList.jScrollPane();
			});


			return false;
		},

		_handleChooseGuestCart: function (e) {
			var self = this;

			this.$inviteWrap.slideToggle('fast', function () {
				self.$inviteList.jScrollPane();
			});

			return false;
		},

		_handleChooseGuestRemove: function (e) {
			var self = this,
				el = $(e.target),
				$parent = el.parent(),
				gid = $parent.data('gid'),
				count = $parent.data('count'),
				price = $parent.data('price'),
				name = $parent.find('.guests__card-name').text(),
				humanizeCount = this._getHumanizeNumber(count, ['блюдо', 'блюда', 'блюд']),
				humanizePrice = this._getHumanizeNumber(price, ['рубль', 'рубля', 'рублей']),
				totalGoodsText = humanizeCount + " на сумму " + humanizePrice,
				textForCancel = 
				"<p> " + name + ' был(а) удален из Ваших гостей. Если это произошло случайно - можно вернуть гостя обратно. </p><p>' + 
				totalGoodsText + '</p>',
				$imgForCancel = $parent.clone().removeClass('_big').addClass('_current _removed'),
				next = $('.guests__card._big:eq(0)'),
				next_gid = next.data('gid');

				if (gid === 1) {
					textForCancel = 
					'<p> Стол "На всех" был удален. Если это произошло случайно - можно вернуть его обратно. </p><p>' + 
					totalGoodsText + '</p>';
				}

			$.ajax({
				type: "GET",
				url: '/ajax/removeGuest.php',
				// dataType: 'json',
				data: {
					gid: gid
					// all: all
				},
				success: function (data) {
					if (gid == 'current') {
						el.parent().html(next.html());
						next.remove();
					}
					else {
						el.parent().remove();
					}

					self.$inviteList.jScrollPane();

					next_gid = (next_gid == gid) ? 'current' : next_gid;

					$.ajax({
						type: "GET",
						url: '/ajax/getCartList.php',
						// dataType: 'json',
						data: {
							gid: next_gid
							// all: all
						},
						success: function (data) {
							if ($('.cart__toggle-circle').hasClass('_opened')) {
								var opened = true;
							}

							$('.cart_wrap').html(data);
							self.$cartProgress = $('.cart-progress');
							
							if (opened) {
								self.$cartProgress.show();
								$('.cart__table').show();
								$('.cart__toggle').find('div').addClass('_opened');
							};

							var cart = $('.cart').parent();

							$.destroyWidgets(cart);
							$.initWidgets(cart);

							var $popup = self.$removePopup.clone(),
								$cancelButton = $popup.find('.goods-one__add');

							$popup.find('.goods-one__about').html(textForCancel);
							$popup.find('.goods-one__img').html($imgForCancel);


							function cancelRemoving () {
								$cancelButton.off('click');

								$.ajax({
									type: "GET",
									url: '/ajax/CartCancelDelete.php',
									dataType: 'json',
									data: {
										gid: gid
									},
									success: function (data) {
										// if (data.ERROR) {
										// 	var $error = $('<p />').text(data.ERROR);

										// 	$.fancybox({
										// 		content: $error,
										// 		padding: [50, 40, 0, 40]
										// 	});

										// 	return;
										// }

										// $('.cart-buy__sum i').text(data.TOTAL_PRICE);
										// $('#guests__card-sum').text(data.SUB_PRICE);
										// if ($('.cart__toggle-circle').hasClass('_opened')) {
										// 	var opened = true;
										// }

										$.ajax({
											type: "GET",
											url: '/ajax/getCartList.php',
											data: {
											},
											success: function (data) {
												$.fancybox.close();

												$('.cart_wrap').html(data);
												var $table = $('.cart__table');
												self.$cartProgress = $('.cart-progress');

												if (opened) {
													self.$cartProgress.show();

													$table.show();
													$('.cart__toggle').find('div').addClass('_opened');
												};

												var cart = $('.cart').parent();

												$.destroyWidgets(cart);
												$.initWidgets(cart);

												$table.addClass('_min-height').addClass('_filled');
											}
										});
									}
								});
							};

							function handleCtrlZ(e) {
								var code = e.keyCode || e.which;

								  if (e.keyCode == 26 && e.ctrlKey) {
										$(document).off('keypress');
										cancelRemoving();
									}
							}

							$(document).off('keypress'); 
							$(document).on('keypress', handleCtrlZ);


							$cancelButton.one('click', cancelRemoving);

							$.fancybox({
								content: $popup
							});
						}
					});
				}
			});

			return false;
		},

		_handleOpenCartClick: function () {

			$('.cart__toggle').trigger('click');

			return false;

		},

		_handleAddNewGuestClick: function () {

			$.publish('popup', 'newGuest');

			return false;

		},

		_handleChangeGuestAdd: function (e) {
			var self = this,
				el = $(e.target),
				$target = $(e.currentTarget),
				gid = $(e.currentTarget).data('gid');
				// all;

			if ($target.hasClass('_disabled')) {
				return false;
			}

			if ($('.cart-selected-guest-list .guests__card._big').add('.cart-selected-guest-list .guests__card._current').not('[data-gid="1"]').length >= 9) {

				$.fancybox({
					wrapCSS: 'fancybox-alert',
					content: $('<div class="popup__alert"><h2 class="h1">К сожалению, невозможно добавить более 8 гостей к одному столу.</h2><p>Вы можете добавить блюда на всех оставшихся гостей воспользовавшись одноименной опцией в корзине.</p></div>')
				});

				return false;
			}


			el.closest('.guests__add-wrap').addClass('_disabled');

			$.ajax({
				type: "GET",
				url: '/ajax/getCartList.php',
				// dataType: 'json',
				data: {
					gid: gid
					// all: all
				},
				success: function (data) {
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					$('.cart_wrap').html(data);
					self.$cartProgress = $('.cart-progress');

					if (opened) {
						self.$cartProgress.show();
						$('.cart__table').show();
						$('.cart__toggle').find('div').addClass('_opened');
					};

					var cart = $('.cart').parent();

					$.destroyWidgets(cart);
					$.initWidgets(cart);
				}
			});

			return false;

		},

		_getHumanizeNumber: function (number, titles) {
		    var cases = [2, 0, 1, 1, 1, 2];  
		    return number + " " + titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
		},

		_handleChangeGuestCart: function (e) {
			var self = this,
				el = $(e.target),
				$target = $(e.currentTarget),
				gid = $(e.currentTarget).data('gid');

			if ($target.hasClass('_disabled')) {
				return false;
			}

			el.closest('.guests__list-wrap').addClass('_disabled');

			$.ajax({
				type: "GET",
				url: '/ajax/getCartList.php',
				// dataType: 'json',
				data: {
					gid: gid
					// all: all
				},
				success: function (data) {
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					$('.cart_wrap').html(data);
					self.$cartProgress = $('.cart-progress');
					
					if (opened) {
						self.$cartProgress.show();
						$('.cart__table').show();
						$('.cart__toggle').find('div').addClass('_opened');
					};

					var cart = $('.cart').parent();

					$.destroyWidgets(cart);
					$.initWidgets(cart);
				}
			});

			return false;

		},

		_hideList: function () {
			var self = this; 
			
			this.$addWrap.slideUp('fast', function () {
				self.$addList.jScrollPane();
			});
			this.$inviteWrap.slideUp('fast', function () {
				self.$inviteList.jScrollPane();
			});

			this.element.removeClass('_opened');
		}
	});
})(jQuery);
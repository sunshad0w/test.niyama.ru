;(function ($) {
	'use strict';

	$.widget('niyama.nav', {
		_create: function () {

			this.$moreLink = this.element.find('._more');

			this.$moreBlock = this.$moreLink.find('.nav__more');
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({
				'click ._more': this._handleMoreClick
			});
		},
		_initPlugins: function () {

		},
		_handleMoreClick: function (e) {
			var $target = $(e.currentTarget);

			this.$moreBlock.toggle();

		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.orderHistory', {
		_create: function () {

			this.url = this.element.data('url');
			
			this._initEvents();
			this._initPlugins();
		},

		_initEvents: function () {
			this._on({
				'click td': this._handleCellClick,
				'change select': this._handleSelectChange
			});
		},

		_initPlugins: function () {
			this.$table = this.element.find('.order-history__table');

			this.$table.tablesorter({
				// widthFixed: true, 
				// widgets: ['zebra'],
				dateFormat: 'ddmmyyyy',
				headers: { 
					0: { 
						sorter:"time_ddmmyyyy"
					},
					1: { 
						sorter: false 
					}, 
					4: { 
						sorter: false 
					},
				} 
			}); 
		},

		_handleCellClick: function (e) {
			var $target = $(e.currentTarget),
				href = $target.parent().data('href');

			location.href = href;
		},

		_handleSelectChange: function (e) {
			var $target = $(e.currentTarget),
				gid = $target.val();

			this._sendAjax(gid);
		},

		_sendAjax: function (gid) {
			var self = this;

			// $.fancybox({
			// 	href : this.url, 
			// 	type: 'ajax',
			// 	afterShow : function () {
			// 		$.initWidgets();
			// 	}
			// });
			// $.initWidgets();

            self.$table.html('&nbsp;').addClass('__loader');

			$.ajax({
				type: 'GET',
				url: this.url,
				data: {
					gid: gid
				},
				success: function (data) {
					self.$table.closest('.order-history').replaceWith(data);
					$('.fancybox-inner').css({
						'overflow': 'visible'
					});

					$.fancybox.update();

					var history = $('.order-history').parent();
					$.destroyWidgets(history);
					$.initWidgets(history);

					$.proxy(self._initPlugins(), self);
				}
			});
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.cabinet', {
		_create: function () {
			
			this._initEvents();
		},

		_initEvents: function () {
			this._on({
				'click .cabinet__guests .cabinet__controls-add': this._handleAddNewGuestClick,
                'click .radio-avatar': this._handleChangeIcon,
                'click .popup__crop-submit': this._handleCropImg,
                'click .popup__crop-close, .popup__crop-cancel': this._handleCropClose
			});

            $('#useravatar').fileupload({
                url: '/ajax/uploadAvatar.php',
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                previewMaxWidth: 390,
                previewMaxHeight: 320,
                previewCrop: true,
                done: function(e, data) {
                    if (data.result.error) {
                        $('.popup__avatars').next().text(data.result.error).addClass('filled');
                        $('.popup__user-pic').addClass('error');
                        return false;
                    }
                    else {
                        $('.popup__avatars').next().removeClass('filled');
                        $('.popup__user-pic').removeClass('error');
                    }

                    $('.popup__crop').fadeIn().find('img').prop('src', data.result.src);

                    var id = data.result.id;
                    $('.popup__crop-id').val(id);

                    $('.popup__crop img').imgAreaSelect({
                        aspectRatio: '1:1',
                        handles: true,
                        onSelectEnd: function(img, selection) {
                            var x1 = selection.x1,
                                y1 = selection.y1,
                                x2 = selection.x2,
                                y2 = selection.y2;

                            var scaleX = 60 / (selection.width || 1),
                                scaleY = 60 / (selection.height || 1);
                          
                            $('.popup__crop-preview img').css({
                                width: Math.round(scaleX * data.result.width) + 'px',
                                height: Math.round(scaleY * data.result.height) + 'px',
                                marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
                                marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
                            });

                            //console.log(id, x1, y1, x2, y2);
                            $('.popup__crop-preview img').show();

                            $('.popup__crop-x1').val(x1);
                            $('.popup__crop-y1').val(y1);
                            $('.popup__crop-x2').val(x2);
                            $('.popup__crop-y2').val(y2);

                        }
                    });

                    $(document).trigger('resize');
                }

            });
		},

		_handleAddNewGuestClick: function () {
			$.publish('popup', ['newGuest', 'cabinet']);

			return false;
		},

        _handleChangeIcon: function (e) {
            var $target = $(e.currentTarget),
                ico = $target.data('url');

            $('.popup__user-pic').find('img').prop('src', ico);
        },

        _handleCropImg: function (e) {
            var width = $('.popup__crop-img img').width(),
                height = $('.popup__crop-img img').height(),
                x1 = $('.popup__crop-x1').val(),
                y1 = $('.popup__crop-y1').val(),
                x2 = $('.popup__crop-x2').val(),
                y2 = $('.popup__crop-y2').val(),
                id = $('.popup__crop-id').val();

                x1 = x1 ? $('.popup__crop-x1').val() : (width - height) / 2,
                y1 = y1 ? $('.popup__crop-y1').val() : 0,
                x2 = x2 ? $('.popup__crop-x2').val() : height,
                y2 = y2 ? $('.popup__crop-y2').val() : height,

            $.ajax({
                type: "GET",
                url: '/ajax/cropImage.php',
                dataType: 'json',
                data: {
                    file_id: id,
                    new_width: (x2 - x1),
                    new_height: (y2 - y1),
                    x1: x1,
                    y1: y1,
                    x2: x2,
                    y2: y2
                },
                success: function (data) {
                    //console.log(data);
                    $(e.currentTarget).closest('.popup__crop').fadeOut();
                    $('.popup__user-pic').find('img').prop('src', data.src);
                    $('#cabinet_avatar__user').val(data.id).prop('checked', true);
                }
            });
        },

        _handleCropClose: function (e) {

            $(e.currentTarget).closest('.popup__crop').fadeOut();

        }

	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.goodsSlider', {
		_create: function () {
			this.$goods = this.element.find('.goods');

			this._initEvents();
			this._initPlugins();
		},

		_initEvents: function () {
			var self = this;


		    this.$goods.find('.goods__add-btn').on('click', function () {
		    	self.handleAddToCartClick(this);
		    	return false;
		    });
		},

		_initPlugins: function () {
			this.element.find('.goods-slider__slider').owlCarousel({
				 
					  navigation : true, // Show next and prev buttons
					  slideSpeed : 400,
					  paginationSpeed : 400,
					  items:5,
					  itemsDesktop: [1279, 4],
					  itemsDesktopSmall: [1023, 3],
					  mouseDrag: false,
					  lazyLoad : true,
				      itemsTablet: false, //2 items between 600 and 0
				      itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
				      singleItem: false,
				      pagination: false
					  
					  // pagination: true
				 
					  // "singleItem:true" is a shortcut for:
					  // items : 1, 
					  // itemsDesktop : false,
					  // itemsDesktopSmall : false,
					  // itemsTablet: false,
					  // itemsMobile : false
				 
				  });
			// this.element.find('.main-slider__slider').carouFredSel({
			// 	prev: $('.main-slider__prev'),
			// 	next: $('.main-slider__next')
			// });
			
			
			// this.element.find('.main-slider__slider').slick({
			// 	dots: true,
			// 	customPaging: function(slider, i) {
			// 		return '<span type="button"></span>';
			// 	},
			// });
		},

		handleAddToCartClick: function (self) {

				var pid = $(self).data('id'),
					price = $(self).data('price'),

					el = $(self).parent().parent().parent(),
						// clone.find('.goods__add-btn').remove();
						// clone.find('.goods__num').remove();
						// clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
					clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
					clone2 = el.clone().css({position:'absolute',top:el.offset().top , left:el.offset().left, width: el.width() + 4, 'z-index': 100}).appendTo('body');
					clone2.animate({top:'300px', opacity:0},
						1000,
						function(){
							clone2.remove();
							var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
							// $.publish('addGoods', cloneHTML);
						});

	            $.ajax({
	                type: "GET",
	                url: '/ajax/add2cart.php',
	                dataType: 'json',
	                data: {
	                	pid: pid,
	                	quantity: 1,
	                },
	                success: function (data) {
						$('.cart-buy__sum i').text(data.TOTAL_PRICE);
						$('#guests__card-sum').text(data.SUB_PRICE);
						if ($('.cart__toggle-circle').hasClass('_opened')) {
							var opened = true;
						}

			            $.ajax({
			                type: "GET",
			                url: '/ajax/getCartList.php',
			                data: {
			                },
			                success: function (data) {
			                	$('.cart_wrap').html(data);
			                	if (opened) {
			                		$('.cart__table').show();
			                		$('.cart__toggle').find('div').addClass('_opened');
			                	};

			                	var cart = $('.cart').parent();

			                	$.destroyWidgets(cart);
			                	$.initWidgets(cart);
			            	}
			            });
	            	}
	            });

	            return false

			}
	});
})(jQuery);
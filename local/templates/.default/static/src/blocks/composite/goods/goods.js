;(function ($) {
	'use strict';

	$.widget('niyama.goods', {
		_create: function () {

			this.$notInMenu = this.element.find('.goods__not-in-menu');
			this.$selectWrap = this.element.find('.select__wrap');
			this.$buttons = this.element.find('.goods__add-btn');
			this.$remove = this.element.find('.goods__remove');
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			var self = this;

			this._on({
				'click .goods__change-next': this._handleNextClick,
				'click .goods__change-prev': this._handlePrevClick
			});

			if (this.element.hasClass('_auction')) {

				this.element.find('.goods__add-btn').one('click', $.proxy(this._handleAddAuctionToCart, this));

			} else if ( this.element.hasClass('_table') ) {

				if (this.$buttons.length) {
					this.$buttons.one('click', function () {
						var $this = $(this),
							pid = $this.closest('.goods__item').data('id'),
							old = self.element.data('id'),
							gid = $('.guests__card._current').data('gid');

						$.ajax({
							type: "GET",
							url: '/ajax/changeProduct.php',
							dataType: 'json',
							data: {
								pid: pid,
								old: old,
								gid: gid
							},
							success: function (data) {
								if (data.ERROR) {
									var $error = $('<p />').text(data.ERROR);

									$.fancybox({
										content: $error,
										padding: [50, 40, 0, 40]
									});

									return;
								}

								$('.cart-buy__sum i').text(data.TOTAL_PRICE);
								$('#guests__card-sum').text(data.SUB_PRICE);

								if ($('.cart__toggle-circle').hasClass('_opened')) {
									var opened = true;
								}

								$.ajax({
									type: "GET",
									url: '/ajax/getCartList.php',
									data: {
									},
									success: function (data) {
										$('.cart_wrap').html(data);
										var $table = $('.cart__table');
										self.$cartProgress = $('.cart-progress');

										if (opened) {
											self.$cartProgress.show();

											$table.show();
											$('.cart__toggle').find('div').addClass('_opened');
										};

										var cart = $('.cart').parent();

										$.destroyWidgets(cart);
										$.initWidgets(cart);

										$table.addClass('_min-height').addClass('_filled');
									}
								});

							}
						}); // конец аякса
						
					});
				}
			}

		},

		_initPlugins: function () {
			var $carousel = this.element.find('.goods__wrap'),
				self = this;

			if ( $carousel.find('.goods__item').length < 2 ) {
				return false;
			}

			$carousel.owlCarousel({
				singleItem: true,
				
				afterMove: function () {
					if (this.owl.currentItem === 0) {
						self.$selectWrap.show();
						self.$remove.css({zIndex: 10});
						self.$buttons.hide();
					} else {
						self.$selectWrap.hide();
						self.$remove.css({zIndex: -10});
						self.$buttons.show();
					}

					var $currentGoods = $(self.owl.$owlItems[self.owl.currentItem]).find('.goods__item');
					if ($currentGoods.hasClass('_disabled')) {
						self.$notInMenu.show();
						self.$buttons.hide();
					} else {
						self.$notInMenu.hide();
						self.$buttons.show();
					}
				},

				afterInit: function () {
					if (this.owl.currentItem === 0) {
						self.$selectWrap.show();
					} else {
						self.$selectWrap.hide();
					}

					if (self.owl && self.owl.$owlItems) {
						var $currentGoods = $(self.owl.$owlItems[self.owl.currentItem]).find('.goods__item');
						if ($currentGoods.hasClass('_disabled')) {
							self.$notInMenu.show();
						} else {
							self.$notInMenu.hide();
						}
					}
				}

			});

			this.owl = $carousel.data('owlCarousel');
		},


		_handleAddAuctionToCart: function (e) {
			var $target = $(e.currentTarget),
				pid = $target.data('id'),
				price = $target.data('price');

			$target.off('click');

			this._sendAddToCartAjax(pid);

			return false;
		},

		_sendAddToCartAjax: function (pid) {
			var self = this;

			$.ajax({
				type: "GET",
				url: '/ajax/add2cart.php',
				dataType: 'json',
				data: {
					pid: pid,
					quantity: 1,
					auction: true
				},
				success: function (data) {
					if (data.ERROR) {
						var $error = $('<p />').text(data.ERROR);

						$.fancybox({
							content: $error,
							padding: [50, 40, 0, 40]
						});

						return;
					}

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);

					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					$.ajax({
						type: "GET",
						url: '/ajax/getCartList.php',
						data: {
						},
						success: function (data) {
							$('.cart_wrap').html(data);
							var $table = $('.cart__table');
							self.$cartProgress = $('.cart-progress');

							if (opened) {
								self.$cartProgress.show();

								$table.show();
								$('.cart__toggle').find('div').addClass('_opened');
							};

							var cart = $('.cart').parent();

							$.destroyWidgets(cart);
							$.initWidgets(cart);

							$table.addClass('_min-height').addClass('_filled');
						}
					});
				}
			});
		},

		_handleNextClick: function () {
			console.log
			this.owl.next();
		},

		_handlePrevClick: function () {
			this.owl.prev();
		}
	});
})(jQuery);
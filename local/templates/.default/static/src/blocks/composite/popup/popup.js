;(function ($) {
	'use strict';

	$.widget('niyama.popup', {
        _create: function () {
            this._initEvents();
        },

		_initEvents: function () {
			$.subscribe('popup', $.proxy(this._handlePopup, this));

			this._on({
				'click .popup__overlay': this._handleCloseClick,
                'click .popup__close': this._handleCloseClick
				// 'click .btn': this._handleAddGuestClick
			});

            this._setAjaxEvent();

            $('#fileupload').fileupload({
                url: '/ajax/uploadAvatar.php',
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                previewMaxWidth: 100,
                previewMaxHeight: 100,
                previewCrop: true,
                done: function(e, data) {
                    $('#progress').fadeOut();
                    $('#files').data('id', data.result.id).find('img').prop('src', data.result.src);
                }

            }).on('fileuploadprogressall', function (e, data) {

                $('#progress').show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css('width', progress + '%');

            });
		},

        _setAjaxEvent: function () {
            this.element.find('.btn').on('click', $.proxy(this._handleAddGuestClick, this));
        },

        _removeAjaxEvent: function () {
             this.element.find('.btn').off('click');
        },

		_handleCloseClick: function () {
			this.element.hide();

            this.element.find('.parsley-error').removeClass('parsley-error');
            this.element.find('.parsley-errors-list').text('').removeClass('filled');
		},

		_handlePopup: function (e, value, isCabinet) {
			this.element.show();

            if (isCabinet) {
                this._removeAjaxEvent();
            } else {
                this._removeAjaxEvent();
                this._setAjaxEvent();
            }
		},

		_handleAddGuestClick: function (e) {

            var self = this,
                block = $(e.target).parent(),
                id = block.find('#files').data('id') ? block.find('#files').data('id') : block.find('.avatars').find(':checked').prop('id'),
                $name = block.find('._mb_2'),
                name = block.find('._mb_2').val(),
                dir = $('#guest_popup_dir').val();

            if (!$(e.target).prop('disabled') && name.replace(/\s+/g, '').length > 0) {

				if ($('.cart-selected-guest-list .guests__card._big').add('.cart-selected-guest-list .guests__card._current').not('[data-gid="1"]').length >= 9) {

                    $.fancybox({
                        wrapCSS: 'fancybox-alert',
                        content: $('<div class="popup__alert"><h2 class="h1">К сожалению, невозможно добавить более 8 гостей к одному столу.</h2><p>Вы можете добавить блюда на всех оставшихся гостей воспользовавшись одноименной опцией в корзине.</p></div>')
                    });

                    this.element.hide();

                    return false;
                }

                $name.removeClass('parsley-error');
                $(e.target).prop('disabled', true);

                $.ajax({
                    type: "GET",
                    url: '/ajax/addGuest.php',
    	            dataType: 'json',
                    data: {
                        name: name,
                        avatar_id: id,
                    	dir: dir
                    },
                    success: function (data) {
                        var gid = data.gid,
                            error = data.error,
                            redirect_url = data.redirect_url;

                        if (error) {
                            $name.addClass('parsley-error');
                            block.find('.parsley-errors-list').text(error).addClass('filled');
                            $(e.target).prop('disabled', false);
                            return false;
                        }
                        else {
                            block.removeClass('filled');
                            block.find('.parsley-errors-list').text(error).removeClass('filled');
                        }

		                if (!redirect_url) {
	                        block.find('._mb_2').val('');
                            // block.find('#files img').removeAttr('src');
    	                    block.find('#files img').attr('src', '/images/blank.png');
        	                block.find('#files .progress-bar').width(0);
            	            block.find('.avatars input:checked').prop('checked', false)
                	        $(e.target).prop('disabled', false);
	                        block.parent().hide();
                        }

                        $.ajax({
                            type: "GET",
                            url: '/ajax/getCartList.php',
                            // dataType: 'json',
                            data: {
                                gid: gid
                                // all: all
                            },
                            success: function (data) {
		                        if (redirect_url) {
        		                    window.location.replace(redirect_url);
        		                    return;
                        		}

                                if ($('.cart__toggle-circle').hasClass('_opened')) {
                                    var opened = true;
                                }

                                $('.cart_wrap').html(data);
                                
                                if (opened) {
                                    $('.cart__table').show();
                                    $('.cart__toggle').find('div').addClass('_opened');
                                };

                                var cart = $('.cart').parent();

                                $.destroyWidgets(cart);
                                $.initWidgets(cart);
                            }
                        });

		                if (redirect_url) {
                		    return false;
                        }
                	
                	}
                });
            }
            else if (name.replace(/\s+/g, '').length < 1) {
                $name.addClass('parsley-error');
                block.find('.parsley-errors-list').text('Введите имя гостя').addClass('filled');
            }

			return false;
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.cartHistory', {
		_create: function () {
			
			this.$link = this.element.find('.cart-history__title');
			this.$list = this.element.find('.cart-history__list-wrap');

			this.$scroll = this.element.find('.cart-history__list-scroll');

			this.$historyLink = this.element.find('.cart-history__list-link');
			this._initEvents();
		},
		_initEvents: function () {
			var self = this;

			this._on({
				'click .cart-history__title': this._handleTitleClick,
				'click .cart-history__list-link': this._handleLinkClick
			});

			this._on(
			    window,
			    {
			        "click": function (e) {
			            var target = $(e.target);
			            if (target.hasClass("cart-history__list-link")) {
			                return;
			            }
			            if (!target.closest(self.element).length) {
			                self._hideList();
			            }
			        }
			    }
			);

		},
		_handleTitleClick: function (e) {
			var self = this;

			this.$link.toggleClass('_opened');

			this.$list.slideToggle('fast', function () {
				self.$scroll.jScrollPane();
			});
		},

		_handleLinkClick: function (e) {
			var $target = $(e.currentTarget),
				orderID = $target.data('id'),
				opened = $('.cart__toggle-button').hasClass('_opened');
	        // var id = id,
	        //     date,
	        //     summ;

	        $.ajax({
	                type: "GET",
		            url: "/ajax/selectOrder.php",
		            data: {
		            	order_id: orderID
		            	// date: date,
		            	// summ: summ,
		            },
		            success: function (data) {
		            	$.ajax({
		            	    type: "GET",
		            	    url: '/ajax/getCartList.php',
		            	    data: {
		            	    },
		            	    success: function (data) {
		            	    	$('.cart_wrap').html(data);
		            	    	if (opened) {
		            	    		$('.cart__table').show();
		            	    		$('.cart__toggle').find('div').addClass('_opened');
		            	    	};

		            	    	var cart = $('.cart').parent();
		            	    	$.destroyWidgets(cart);
		            	    	$.initWidgets(cart);

		            			cart.find('.cart-progress').remove();
		            		}
		            	});

		            }
	            }
	        );

	        return false;
		},
		
		_hideList: function () {
			var self = this;
			this.$link.removeClass('_opened');

			this.$list.slideUp('fast', function () {
				self.$scroll.jScrollPane();
			});
		}
	});
})(jQuery);
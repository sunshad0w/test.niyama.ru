;(function ($) {
	'use strict';

	$.widget('niyama.pagination', {
		_create: function () {
			this.$pageTitle = $('h1');
			this.$htmlBody = $("html, body");

			this._initEvents();
		},
		_initEvents: function () {
			this._on({
				'click .pagination__item': this._handleClick,
				'click .pagination__prev': this._handleClick,
				'click .pagination__next': this._handleClick
			});
		},
		_handleClick: function (e) {
			var $target = $(e.currentTarget),
				// page = $target.attr('href'),
				params = $target.data('params'),
				self = this;

			if ( $target.hasClass('_disabled') ) {
				return;
			}

			if ( typeof(params) !== 'object' ) {
				params = $.parseJSON(params);
			}

			// var url = '';
			// $('.filter').find('.filter__item._selected').each(function() {
			// 	var el = $(this),
			// 		value = el.data('filter'),
			// 		category = el.closest('.filter__group').data('category');
					
			// 		url += category + '=' + value + '&';
			// 	}
			// );
			// url = '?' + url;
			// url = url.substring(0, url.length - 1);

			// if (url == '') {
			// 	page = '?' + page;
			// } else {
			// 	page = '&' + page;
			// }

			$.ajax({
				type: "POST",
				url: '/ajax/get_products.php' + url + page + '&nofilter=yes',
				//url: '/ajax/get_products.php',
				datatype: 'json',
				data: params,
				success: function (data) {
					var $data = $.parseJSON(data);

					$('.page__content').fadeOut(500, function() {
						self.$htmlBody.animate(
							{
								scrollTop: self.$pageTitle.offset().top - 130
							}, 
							'700', 'swing');

						$('.page__content').html($data.products).fadeIn(500);

						var catalog = $('.catalog').parent();

						$.destroyWidgets(catalog);
						$.initWidgets();


					});
				}
			});

			// if(page != window.location) {
			// 	window.history.pushState(null, null, page);
			// }

			return false;
		}
	});
})(jQuery);
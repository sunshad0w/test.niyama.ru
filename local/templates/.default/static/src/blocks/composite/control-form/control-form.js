;(function ($) {
	'use strict';

	$.widget('niyama.controlForm', {
		_create: function () {
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({

			});
		},
		_initPlugins: function () {

			var $phoneMask = $('._phone-mask');

			if ($phoneMask.length) {
				$phoneMask.mask("+7 (999) 999-99-99");
			}
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.post', {
		_create: function () {

			this.$carousel = this.element.find('.post__img._carousel');
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({

			});
		},
		_initPlugins: function () {
			if (this.$carousel.length) {
				this.$carousel.owlCarousel({
					  lazyLoad : true,
					  navigation : true, // Show next and prev buttons
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      mouseDrag: false,
				      singleItem:true,
				      autoHeight : true
				});
			}
		}
	});
})(jQuery);
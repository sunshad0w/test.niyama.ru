;(function ($) {
	'use strict';

	$.widget('niyama.orderingForm', {
		_create: function () {

			this.$form = this.element.find('form');

			this.currentBlock = 1;

			this.isDelivery = true;

			this.$orderTotalSum = this.element.find('.order__amount-sum');

			this.$mkadCheckbox = this.element.find('input._mkad');

			this.$bodies = this.element.find('.ordering-form__body');
			this.$isCash = $("#iscash");

			this.$sidebar = this.element.find('.ordering-form__side');

			// inputs for desc 

			// block1
			this.$name = this.element.find('input._name');
			this.$surname = this.element.find('input._surname');
			this.$phone = this.element.find('input._phone');
			this.$email = this.element.find('input._email');

			// block2
			this.$city = this.element.find('select._city').eq(0);
			this.$metro = this.element.find('input._metro');
			this.$other = this.element.find('input._other');
			this.$restaurant = this.element.find('select._restaurant');

			this.$address = this.element.find('input._address');
			this.$house = this.element.find('input._house');
			this.$apart = this.element.find('input._apart');
			this.$porch = this.element.find('input._porch');
			this.$intercom = this.element.find('input._intercom');
			this.$floor = this.element.find('input._floor');

			this.$soon = this.element.find('input._soon');
			this.$certain = this.element.find('input._certain');
			this.$deliveryRadios = this.$soon.add(this.$certain);
			this.$deliveryTimeOptions = this.element.find('.ordering-form__row._certain');

			this.$certainTime = this.element.find(".ordering-form__row._certain").find('select');
			this.$certainDate = this.element.find(".ordering-form__row._certain").find('input');

			// способы доставки
			this.$deliveryTypeGroup = $('#deliveryTypeGroup');
			this.$deliveryControls = $('#deliveryControls');
			this.$pickupControls = $('#pickupControls');

			this.$pickupControls.find('.ordering-form__row._rests').hide().eq(0).show();
			this.$pickupControls.detach();

			this._initEvents();
			this._initPlugins();

			// Первоначальный выбор типа доставки
			this._initDeliveryType();
		},
		_initEvents: function () {
			this._on({
				'click button': this._handleButtonClick,
				'submit form': this._handleFormSubmit,
				'click .ordering-form__edit': this._handleEditClick,
				// 'keyup .input._address': this._handleAddressKeyup,
				'keypress .input': this._handleEnterClick
			});

			this.$deliveryRadios.on('change', $.proxy(this._handleDeliveryRadioChange, this));

			this.$deliveryTimeOptions.hide();
			this.$deliveryTimeOptions.find('input').prop('disabled', true);

			// Обработчики на пересчёт доставки
			this.$certainTime.on('change', $.proxy(this._sendDeliveryAjax, this));
			this.$city.on('change', $.proxy(this._sendDeliveryAjax, this));
			this.$mkadCheckbox.on('change', $.proxy(this._sendDeliveryAjax, this));
			this.$soon.on('change', $.proxy(this._sendDeliveryAjax, this));
			this.$certain.on('change', $.proxy(this._sendDeliveryAjax, this));

			// обработчик на налик - потребуется ли сдача?
			this.element.find('.ordering-form__table').find('input').on('change', $.proxy(this._handlePaymentCheckboxesChange, this))

			// обработчик на изменение типа доставки
			$('#deliverytype').find('input').on('change', $.proxy(this._handleDeliveryTypeChange, this));

			this.$metro.on('change', function () {
				var $this = $(this);

				if ($this.val !== '') {
					$this.siblings('.chosen-container-error').removeClass('chosen-container-error');
				}
			});

			var self = this;
			this.$sidebar.find('.goods__add-btn').on('click', function () {
				self.handleAddToCartClick(this, self);
				return false;
			});
		},

		_initDeliveryType: function () {
			var $target = $('#deliverytype').find('input:checked');
			this._changeDeliveryType($target);
		},

		_initPlugins: function () {
			var self = this;

			this.$phone.mask("+7 (999) 999-99-99");

			 $(function() {
			    function log( message ) {
			      $( "<div>" ).text( message ).prependTo( "#log" );
			      $( "#log" ).scrollTop( 0 );
			    }

			    function getHousesData ( request, response, street ) {
			    	var city = self.$city.find('option:selected').text();

			      	var address = self.selectedStreet || request.term;

			      	var house = request.term;

			        $.ajax({
			          url: '/ajax/search_streets.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
						house: house
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			    }

			    function getAddressesData ( request, response, street ) {

			      	var city = self.$city.find('option:selected').text();

			      	//var address = self.selectedStreet || request.term;
			      	var address = self.$address.val();

			        $.ajax({
			          url: '/ajax/search_streets.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			      }

			    function getMetroData ( request, response, station ) {

			      	var city = self.$city.find('option:selected').text();

			      	//var address = self.selectedStreet || request.term;
			      	var address = self.$metro.val();

			        $.ajax({
			          url: '/ajax/search_metro.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			      }

			      function initHouseAutocomplete(street) {
			      	self.selectedStreet = street;

			      	self.$house.autocomplete({
			      		source: getHousesData,
			      		select: function( event, ui ) {
			      			self.$house.focus();
			      		  log( ui.item ?
			      		    "Selected: " + ui.item.label :
			      		    "Nothing selected, input was " + this.value);
			      		},

			      		open: function() {
			      			var $this = $(this);
			      		  $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			      		  var menu = $this.data("uiAutocomplete").menu.element;
			      		  menu.addClass("ordering-form__autocomplete _house");
			      		  menu.wrapInner('<div />');
			      		},

			      		close: function() {
			      			var $this = $(this);
			      		  $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			      		 	var menu = $this.data("uiAutocomplete").menu.element;
			      		},

			      		change: function() {
			      			var $this = $(this);

			      			var menu = $this.data("uiAutocomplete").menu.element;
			      			menu.addClass("ordering-form__autocomplete _house");
			      		},

			      		focus: function( event, ui ) {
			      				event.preventDefault(); // without this: keyboard movements reset the input to ''
			      				$(this).val(ui.item.label);
			      		}
			      	});
			      }


			    self.element.find(".input._address").autocomplete({
			      source: getAddressesData,
			      minLength: 3,
			      select: function( event, ui ) {
			      	initHouseAutocomplete(self.$address.val());
			      	self.$house.focus();
			        log( ui.item ?
			          "Selected: " + ui.item.label :
			          "Nothing selected, input was " + this.value);
			      },

			      open: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

			        var menu = $this.data("uiAutocomplete").menu.element;
			        menu.addClass("ordering-form__autocomplete");
			        menu.wrapInner('<div />');

			      },

			      close: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

			       	var menu = $this.data("uiAutocomplete").menu.element;

			      },

			      change: function() {
			      	var $this = $(this);

			      	var menu = $this.data("uiAutocomplete").menu.element;
			      	menu.addClass("ordering-form__autocomplete");

			      	// var pane = $(".ordering-form__autocomplete"),
			      	// 	api = pane.data('jsp');
			      	// 	api.reinitialise();;

			      },

			      focus: function( event, ui ) {
			      		event.preventDefault(); // without this: keyboard movements reset the input to ''

			      		delete self.selectedStreet;
			      		// console.log(event, ui);

			      		$(this).val(ui.item.label);
			      }
			    })

			    self.element.find(".input._metro").autocomplete({
			      source: getMetroData,
			      minLength: 3,
			      select: function( event, ui ) {
			        log( ui.item ?
			          "Selected: " + ui.item.label :
			          "Nothing selected, input was " + this.value);
			      },

			      open: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

			        var menu = $this.data("uiAutocomplete").menu.element;
			        menu.addClass("ordering-form__autocomplete");
			        menu.wrapInner('<div />');

			      },

			      close: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

			       	var menu = $this.data("uiAutocomplete").menu.element;

			      },

			      change: function() {
			      	var $this = $(this);

			      	var menu = $this.data("uiAutocomplete").menu.element;
			      	menu.addClass("ordering-form__autocomplete");

			      	// var pane = $(".ordering-form__autocomplete"),
			      	// 	api = pane.data('jsp');
			      	// 	api.reinitialise();;

			      },

			      focus: function( event, ui ) {
			      		event.preventDefault(); // without this: keyboard movements reset the input to ''

			      		delete self.selectedStreet;
			      		// console.log(event, ui);

			      		$(this).val(ui.item.label);
			      }
			    })

			  });

			this.element.find('.order__add-carousel').owlCarousel({
				singleItem: true,
				navigation: true,
				mouseDrag: true,
				touchDrag: true
			});
		},

		_handleButtonClick: function (e) {
			// var $target = $(e.currentTarget),
			// 	current = $target.data('currentBlock'),
			// 	next = $target.data('nextBlock');

			this._validateStep();
		},

		_handleAddressKeyup: function (e) {
			var city = this.$city.find('option:selected').text(),
				address = $(e.currentTarget).val();

			$.ajax({
				type: "GET",
				url: '/ajax/search_streets.php',
				dataType: 'json',
				data: {
					city: city,
					address: address
				},
				success: function (data) {
					$.each(data.streets, function (k, v) {
						// console.log(k, v);
					})
				}
			});
		},

		_handleFormSubmit: function () {
			this._validateStep();
		},

		_handleEnterClick: function (e) {
			if (e.keyCode == 13) {
				this._validateStep();

				return false;
			}
		},

		_handleEditClick: function (e) {
			var $target =  $(e.currentTarget),
				blockID = $target.data('block'),
				self = this;

			$('.block' + this.currentBlock).slideUp();
			$('.block' + blockID).slideDown('', function () {
				$(this).css({
					'overflow': 'visible'
				});
			});

			$('#desc' + blockID).slideUp();

			$target.removeClass("_visible");

			for (var i = blockID+1; i < 10; i++) {
				$('#edit' + i).removeClass("_visible");
				$('.block' + i ).siblings('.ordering-form__counter').removeClass('_orange');
			};

			self.currentBlock = blockID;

		},

		_handleDeliveryTypeChange: function (e) {
			var $target = $(e.currentTarget);

			this._changeDeliveryType($target);
		},

		_changeDeliveryType: function ($target) {
				var self = this,
					$title = $('#desc2').siblings('.ordering-form__title');

			if ( $target.hasClass('_delivery') ) {
				this.isDelivery = true;

				this.$pickupControls.slideUp(function () {
					self.$pickupControls.detach();
				});

				this.$deliveryTypeGroup.prepend(this.$deliveryControls);
				this.$deliveryControls.slideDown('', function () {
					$(this).css({
						'overflow': 'visible'
					});
				});

				var $edit = $('#edit2');
				$edit.detach();

				$title.text("Доставка").append($edit);

			} else {
				this.isDelivery = false;

				this.$deliveryControls.slideUp(function () {
					self.$deliveryControls.detach();
				});

				this.$deliveryTypeGroup.prepend(this.$pickupControls);
				this.$pickupControls.slideDown('', function () {
					$(this).css({
						'overflow': 'visible'
					});
				});

				var $edit = $('#edit2');
				$edit.detach();

				$title.text("Самовывоз").append($edit);
			}
		},

		_handleDeliveryRadioChange: function (e) {
			var $target = $(e.target);

			if ($target.hasClass('_certain')) {
				this.$deliveryTimeOptions.slideDown('', function () {
					$(this).css({
						"overflow": "visible"
					})
				});

				this.$deliveryTimeOptions.find('input').prop('disabled', false);
			} else {
				this.$deliveryTimeOptions.slideUp();

				this.$deliveryTimeOptions.find('input').prop('disabled', true);
			}
		},

		_handlePaymentCheckboxesChange: function (e) {
			var $target = $(e.currentTarget);

			if ($target.hasClass('_cash')) {
				this.$isCash.slideDown();
				$('#submitOrder').val('Оформить');
			} else {
				this.$isCash.slideUp();
				this.$isCash.find('input').val('');
				$('#submitOrder').val('Оплатить');
			}
		},

		_validateStep: function () {
			var current = this.currentBlock,
				next    = this.currentBlock + 1;

			// only validate going forward. If current group is invalid, do not go further
			// .parsley().validate() returns validation result AND show errors
			if (next > current) {
				if (this.isDelivery && this.currentBlock === 2) {
					var region = this.$city.find('option:selected').data("city"),
						metroStatus;

					// валидация поля метро, если город Москва
					if (this.$metro.val() === "" && region === "REGION_1") {
						this.$metro.parent().addClass('metro-error');
						metroStatus = false;
					} else {
						this.$metro.parent().removeClass('metro-error');
						metroStatus = true;
					}

					// валидация поля Другого города
					if (region !== "REGION_1") {
						if (this.$other.val() === "" && region === "REGION_OTHER") {
							this.$other.parent().addClass('metro-error');
							metroStatus = false;
						} else {
							this.$other.parent().removeClass('metro-error');
							metroStatus = true;
						}
					}

					if (false === this.$form.parsley().validate('block' + current) || !metroStatus)	{
							return;
					}
				}

				if (false === this.$form.parsley().validate('block' + current))	{
						return;
				}
			}

			if (current === 2) {
				var self = this;
				$.ajax({
					url: self.element.data('sidebarUrl'),
					type: 'POST',
					data: self.$form.serializeArray(),
					success: function (data) {
						self.$sidebar.html(data);

						$.initWidgets(self.$sidebar);

						self.$sidebar.find('.order__add-carousel').owlCarousel({
							singleItem: true,
							navigation: true,
							mouseDrag: true,
							touchDrag: true
						});

						self.$sidebar.find('.goods__add-btn').off('click').on('click', function () {
							self.handleAddToCartClick(this, self);
							return false;
						});
					}
				});
			}

			// validation was ok. We can go on next step.
			$('.block' + current)
			  .removeClass('show')
			  .addClass('hidden').slideUp();

			$('.block' + next)
			  .removeClass('hidden')
			  .addClass('show').slideDown('', function () {
			  	$(this).css({
			  		'overflow': 'visible'
			  	});
			  });

			$('.block' + (current + 1) ).siblings('.ordering-form__counter').addClass('_orange');

			$('#edit' + current).addClass("_visible");

			this._createDesc();

			this.currentBlock++;
		},

		_createDesc: function () {
			var $desc = $('<div />');
			
			if (this.currentBlock === 1) {

				var $name = $('<p />').text(this.$name.val() + " " + this.$surname.val()),
					$phone = $('<p />').text(this.$phone.val()),
					$email = $('<p />').text(this.$email.val());

				$desc.append($name, $phone, $email);

			} else if (this.currentBlock === 2) {
				if (!this.isDelivery) {
					var $restAddress = $('<p />').text(
							this.$city.find('option:selected').text() + ", " + $("#rest_" + $("#deliveryTypeGroup").find('select._city').val() ).find('option:selected').text()
						)

					$desc.append($restAddress);

				} else {
					var $city = $('<p />').text(
							this.$city.find('option:selected').text() + ", " + this.$metro.find('option:selected').text()
						),

						$address = this.$address,
						$apart = this.$apart,
						$porch = this.$porch,
						$intercom = this.$intercom,
						$floor = this.$floor;

					var $addressLine = $('<p />'),
						addressText = '',
						$delivery = $('<p />'),
						deliveryText;

					if ( $address.val() ) {
						addressText += $address.val();
					}

					if ( $apart.val() ) {
						addressText += ', кв. ' + $apart.val();
					}

					if ( $porch.val() ) {
						addressText += ', подъезд ' + $porch.val();
					}

					if ( $floor.val() ) {
						addressText += ', ' + $floor.val() + ' этаж';
					}

					if ( $intercom.val() ) {
						addressText += ', домофон: ' + $intercom.val();
					}
					$addressLine.text(addressText);


					var $deliveryCheckbox = $('[name="deliverytime"]:checked');
					if ($deliveryCheckbox.hasClass('_soon')) {
						deliveryText = "Время доставки: " + $deliveryCheckbox.siblings('label').text();
					} else {
						deliveryText = "Время доставки: " + $('.ordering-form__row._certain').find('.input').val() + ', ' + $('.ordering-form__row._certain').find('option:selected').text();
					}
					$delivery.append(deliveryText);


					$desc.append($city, $addressLine, $delivery);
				}

			}

			$('#desc' + this.currentBlock).html($desc).slideDown();
		},

		_sendDeliveryAjax: function () {

			var data = {},
				self = this;

			data.id = this.$city.val();

			var region = this.$city.find('option:selected').data("city");

			if ( region === "REGION_1" ) {
				data.inMKAD = this.$mkadCheckbox.prop("checked");
			} 

			if (this.$certain.prop("checked")) {
				data.time = this.$certainTime.val();
				data.date = this.$certainDate.val();
			}

			$.ajax({
				type: "GET",
				url: '/ajax/calcOrderDelivery.php',
				// url: '/testdeliveryJSON.json',
				dataType: 'json',
				data: data,
				success: function (result) {
					self.$orderTotalSum.html(result.totalSum + ' ');
				}
			});
		},

		handleAddToCartClick: function (goodsButton, self) {
			var $goodsButton = $(goodsButton);

			var pid = $goodsButton.data('id'),
				price = $goodsButton.data('price'),
				el = $goodsButton.parent().parent().parent(),
					// clone.find('.goods__add-btn').remove();
					// clone.find('.goods__num').remove();
					// clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
				clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
				clone2 = el.clone().css({position:'absolute',top:el.offset().top - 10, left:el.offset().left - 4, width: el.width(), 'z-index': 100}).appendTo('body');
				clone2.animate({top:'300px', opacity:0},
					1000,
					function(){
						clone2.remove();
						var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
						$.publish('addGoods', cloneHTML);
					});

            $.ajax({
                type: "GET",
                url: '/ajax/add2cart.php',
                dataType: 'json',
                data: {
                	pid: pid,
                	quantity: 1,
                	fromOrderPromo: true
                },
                success: function (data) {
                	window.ajaxData = data;

                	$.ajax({
                		url: self.element.data('sidebarUrl'),
                		type: 'POST',
                		data: self.$form.serializeArray(),
                		success: function (data) {
                			self.$sidebar.html(data);

                			$.initWidgets(self.$sidebar);

                			self.$sidebar.find('.order__add-carousel').owlCarousel({
                				singleItem: true,
                				navigation: true,
                				mouseDrag: true,
                				touchDrag: true
                			});

                			self.$sidebar.find('.goods__add-btn').off('click').on('click', function () {
                				self.handleAddToCartClick(this, self);
                				return false;
                			});
                		}
                	});

                	$('.cart-buy__sum i').text(data.TOTAL_PRICE);
                	$('#guests__card-sum').text(data.SUB_PRICE);
            	}
            });

            return false

		}

	});
})(jQuery);



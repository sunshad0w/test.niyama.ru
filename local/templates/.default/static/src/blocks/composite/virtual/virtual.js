;(function ($) {
	'use strict';

	$.widget('niyama.virtual', {
		_create: function () {

			this.virtual_box = $('.virtual');
			this.circle = document.getElementById("circle");
			this.desc = $(".desc_text");

			this.timerFunction = null;

			this._initEvents();

		},

		_initEvents: function () {
			this._on({
				'click .desc_circle': this._circleMove,
				'click .virtual_close': this._virtualClose,
			});

			$(document).keyup(function(e) {
				if (e.which == 27) {
					$('.virtual').fadeOut();
				}
			});
		},

		_circleMove: function (e) {

			var el = $(e.target),
				self = this,
				x = this.circle.getAttribute('cx'),
				y = this.circle.getAttribute('cy'),
				stopX = el.data('newx'),
				stopY = el.data('newy'),
				stepX = (stopX - x) / 10,
				stepY = (stopY - y) / 10,
				stepNum = el.data('step'),
				finish = null;

			if (stepNum == 3 || stepNum == 4) {
				$('.virtual').css({top:'-730px'})
			};
			if (stepNum == 2 || stepNum == 5) {
				$('.virtual').css({top:'0'})
			};

			this.timerFunction = setInterval(function(){
				var x = this.circle.getAttribute('cx'),
					y = this.circle.getAttribute('cy'),
					newX = Math.round(parseInt(x) + stepX),
					newY = Math.round(parseInt(y) + stepY);

				if (newX != stopX) {
					circle.setAttribute('cx', newX);
				}
				if (newY != stopY) {
					circle.setAttribute('cy', newY);
				}

				el.parent().fadeOut(function(){
					$('.desc.step' + stepNum).fadeIn();
				});

				if (newX == stopX) {
					circle.setAttribute('cx', newX);
					circle.setAttribute('cy', newY);
					self._circleStop();
				}
			}, 50);
		},

		_circleStop: function () {
			clearInterval(this.timerFunction);
			this.timerFunction = null;
		},

		_virtualClose: function () {
			$('.virtual').fadeOut();
		}
	});
})(jQuery);
(function ($) {
	'use strict';

	$.widget('niyama.catalog', {
		_create: function () {
			
			this.filters = [];

			this.$goods = this.element.find('.goods');

			this._initEvents();
			this._initPlugins();
		},

		_initEvents: function () {
			$.unsubscribe('filterby');
			$.unsubscribe('unfilterby');
			$.unsubscribe('addGoods');

			$.subscribe('filterby', $.proxy(this.handleFilterBy, this));
			$.subscribe('unfilterby', $.proxy(this.handleUnfilterBy, this));
			$.subscribe('addGoods', $.proxy(this.handleAddToCartDrug, this));

			if ($('html').hasClass('bx-no-touch')) {
				this.$goods.draggable({
					helper:'clone',
					appendTo: 'body',
					scroll: false,
					
					drag: function(e,ui) {
						// ui.position.top -= $('body').scrollTop();
					},
					stop: function(e,ui) {
						// self.handleAddToCartDrug(this);
					}

				});
			}

			var self = this;

			this.$goods.find('.goods__add-btn').on('click', function () {
				self.handleAddToCartClick(this);
				return false;
			});

			// this._on({
			// 	'click .catalog__filter-del': this.handleRemoveFilter
			// });

			this.element.find('.catalog__filter-del').off('click').on('click', $.proxy(this.handleRemoveFilter, this) ) ;

		},

		_initPlugins: function () {
			var self = this;
			$(window).load(function () {
				self.iso  = self.element.isotope();
			});
		},


		/* ОБРАБОТЧИКИ СОБЫТИЙ
		------------------------------------------------------------------- */
		handleFilterBy: function (e, value) {
			if (!value) return;

			this.filters.push('[data-category="' + value + '"]');
			this.filter();
		},

		handleUnfilterBy: function (e, value) {
			if (!value) return;

			var indexOf = this.filters.indexOf('[data-category="' + value + '"]');

			if (indexOf > -1) {
				this.filters.splice(indexOf, 1);
			}

			this.filter();
		},

		handleAddToCartClick: function (self) {

			$(self).addClass('__disabled');

			var pid = $(self).find('.btn').data('id'),
				price = $(self).find('.btn').data('price'),

				el = $(self).parent().parent().parent(),
					// clone.find('.goods__add-btn').remove();
					// clone.find('.goods__num').remove();
					// clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
				clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
				clone2 = el.clone().css({position:'absolute',top:el.offset().top , left:el.offset().left, width: el.width() + 4, 'z-index': 100}).appendTo('body');

				clone2.animate({top:'300px', opacity:0},
					1000,
					function(){
						clone2.remove();
						var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
						// $.publish('addGoods', cloneHTML);
					});

			this._sendAddToCartAjax(pid);

			return false;

		},

		handleAddToCartDrug: function (e, self) {

			var pid = $(self).find('.btn').data('id'),
				price = $(self).find('.btn').data('price');

			this._sendAddToCartAjax(pid);
		},

		_sendAddToCartAjax: function (pid) {
			var self = this;

			$.ajax({
				type: "GET",
				url: '/ajax/add2cart.php',
				dataType: 'json',
				data: {
					pid: pid,
					quantity: 1,
				},
				success: function (data) {
					if (data.ERROR) {
						var $error = $('<p />').text(data.ERROR);

						$.fancybox({
							content: $error,
							padding: [50, 40, 0, 40]
						});

						return;
					}

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					$.ajax({
						type: "GET",
						url: '/ajax/getCartList.php',
						data: {
						},
						success: function (data) {
							$('.cart_wrap').html(data);
							var $table = $('.cart__table');
							self.$cartProgress = $('.cart-progress');

							if (opened) {
								self.$cartProgress.show();

								$table.show();
								$('.cart__toggle').find('div').addClass('_opened');
							};

							var cart = $('.cart').parent();

							$.destroyWidgets(cart);
							$.initWidgets(cart);

							$table.addClass('_min-height').addClass('_filled');

							$('.goods__add-btn').removeClass('__disabled');
						}
					});
				}
			});
		},

		handleRemoveFilter: function (e) {
			e.preventDefault();
			
			var fid = $(e.target).parent().data('filter'),
				url = '',

			$el = $('.filter__item[data-filter="' + fid + '"]');
			$.publish('filter', [$el]);

		},


		/* МЕТОДЫ
		------------------------------------------------------------------- */
		filter: function () {
			this.iso.isotope({
				filter: this.filters.join() || '*'
			});
		}
	});
})(jQuery);
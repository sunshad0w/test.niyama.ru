;(function ($) {
	'use strict';

	$.widget('niyama.couponPopup', {
		_create: function () {
			this.$form = this.element.find('form');
			this.url = this.$form.prop('action');

			this._initEvents();
		},

		_initEvents: function () {
			this._on({
				'submit form': this._handleFormSubmit
			});
		},

		_handleFormSubmit: function (e) {
			var self = this;

			$.ajax({
				type: "POST",
				url: this.url,
				data: this.$form.serialize(),
				success: $.proxy(self._ajaxSuccess, self)
			});

			return false;
		},

		_ajaxSuccess: function (data) {
			this.element.replaceWith( $(data.html) );
			var _data = data;

			$.initWidgets();
			$.fancybox.update();

			if (data.success) {
				$.fancybox.close();

				$('.cart-buy__sum i').text(data.TOTAL_PRICE);
				$('#guests__card-sum').text(data.SUB_PRICE);
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					var need_update_coupon = data.need_update_coupon;

		            $.ajax({
		                type: "GET",
		                url: '/ajax/getCartList.php',
		                data: {
		                },
		                success: function (data) {
		                	$('.cart_wrap').html(data);
		                	if (opened) {
		                		$('.cart__table').show();
		                		$('.cart__toggle').find('div').addClass('_opened');
		                	};

		                	var cart = $('.cart').parent();

		                	$.destroyWidgets(cart);
		                	$.initWidgets(cart);

		                	var $couponTitle = $(_data.coupon__title);

		                	$('[data-id=' + _data.pid + ']').addClass('_disabled _used')
		                									.find('.coupon__title')
		                									.replaceWith($couponTitle);

		                	if (need_update_coupon) {
			                	$.ajax({
			                		type: "GET",
			                		url: '/ajax/getCouponByCartId.php',
			                		data: {
			                			pid: pid
			                		},
			                		success: function (data) {
			                			// console.log(data);
			                		}
			                	});
		                	}

		            	}
		            });
			}
		}
	});
})(jQuery);
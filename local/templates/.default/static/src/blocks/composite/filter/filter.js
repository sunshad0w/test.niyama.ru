(function ($) {
	'use strict';

	$.widget('niyama.sideFilter', {
		_create: function () {

			this._caching();
			this._initEvents();
        },

        _caching: function () {
            this.timer = '';
            this.Etimer = '';

            this.isMenu = this.element.hasClass('_menu');

            this.$ajaxOverlay = $('<div>', {
            	'class': 'ajax-progress'
            });

            var $ajaxDots = $('<div>', {
            	'class': 'ajax-progress__dots',
            	'title': 'Подбираем подходящие варианты'
            });

            this.$ajaxOverlay.append($ajaxDots);

        },

		_initEvents: function () {
			this._on({
				'click .filter__link': this.handleLinkClick,
                'click .filter__link-more': this.handleMoreLinks
                // 'click .exclude__link': this.handleExcludeItem,
                // 'mouseenter .filter__link': this.handleExcludeItemShow,
                // 'mouseleave .filter__link': this.handleExcludeItemHide
			});

			$.unsubscribe('selectCategory');
			$.unsubscribe('filter');
			$.unsubscribe('searchedFilterValue');
			$.unsubscribe('changeSide');

			$.subscribe('selectCategory', $.proxy(this._selectCategory, this));
			$.subscribe('filter', $.proxy(this.toogleItemRemove, this));
			$.subscribe('searchedFilterValue', $.proxy(this.setFilterBySearch, this));
			$.subscribe('changeSide', $.proxy(this.handleSideSwitch, this));
		},


		/* ОБРАБОТЧИКИ СОБЫТИЙ
		------------------------------------------------------------------- */
		handleLinkClick: function (e) {

			var $target = $(e.currentTarget),
                $item = $target.closest('.filter__item');

			if ($target.hasClass('_disabled')) {
                if (!$target.parent().hasClass('_selected') && !$target.parent().hasClass('_excluded')) {
                    return false;
                }
			}

			if (this.isMenu) {
				return true;
			}

            e.preventDefault();

            this.toogleItem(e, $item);
		},

		handleSideSwitch: function (e) {
			var self = this;

			if (this.element.hasClass('_hidden')) {
				this.element.fadeIn('', function () {
					self.element.removeClass('_hidden');
					$.publish('refreshSideSwitchClick', 1);
				});
			} else {
				this.element.hide();
				this.element.addClass('_hidden');
				$.publish('refreshSideSwitchClick', 1);
			}
		},


		/* МЕТОДЫ
		------------------------------------------------------------------- */
		toogleItem: function (e, $item) {

			var url = '',
				block = this.element,
				self = this;

			clearTimeout(this.timer);

			if ($item.hasClass('_selected')) {
                if ($item.hasClass('_excluded')) {
                    $item.removeClass('_selected _excluded');
                }
                else {
                    $item.removeClass('_selected');
                }
			} else {
                if ($item.closest('.filter__group').data('category') == 'alerg[]') {
                    $item.addClass('_selected _excluded');
                }
                else {
                    $item.addClass('_selected');
                }
			}
			
			this.timer = setTimeout(function(){

				self.$ajaxOverlay.appendTo('body');

				block.addClass('_disabled').find('.filter__item._selected').each(function(){
					var el = $(this),
						value = el.data('filter'),
						category = el.closest('.filter__group').data('category');
						
						url += category + '=' + value + '&';
					}
				);
				url = '?' + url;
				url = url.substring(0, url.length - 1);

				self._sendProductsAjax(block, url, self);

			}, 1000);

		},

		toogleItemRemove: function (e, $item) {

			var url = '',
				block = this.element;

			if ($item.hasClass('_selected')) {
				 $item.removeClass('_selected');
			} else {
				$item.addClass('_selected');
			}
			
			block.addClass('_disabled').find('.filter__item._selected').each(function(){
				var el = $(this),
					value = el.data('filter'),
					category = el.closest('.filter__group').data('category');
					
					url += category + '=' + value + '&';
				}
			);
			url = '?' + url;
			url = url.substring(0, url.length - 1);

			this._sendProductsAjax(block, url, this);
		},

		_sendProductsAjax: function (block, url, self) {

            $.ajax({
                type: "GET",
                url: '/ajax/get_products.php' + url,
                dataType: 'json',
                data: {
                }
            }).done(function (data) {
            	self.$ajaxOverlay.detach();

            	$('.page__content').fadeOut(500, function() {
            		$.publish('filterChange');

            		block.removeClass('_disabled');
            		$('.page__content').html(data.products).fadeIn(500);

            		self.element.html(data.filters).fadeIn(500);

            		var catalog = $('.catalog').parent().parent(),
            			sidebar = $('.page__sidebar');
            		
					$.destroyWidgets(catalog);
					$.initWidgets(catalog);
            	});
                
            });
			// body...
		},

		setFilterBySearch: function (e, value) {
			var $items = this.element.find('.filter__item'),
				$item = $items.filter('[data-filter=' + value + ']');

			$items.removeClass('_selected');
			$item.addClass('_selected');
		},

		_selectCategory: function (e, value) {
			var $item = this.element.find('.filter__item[data-filter=' + value + ']');

			$item.addClass('_selected');
			$.publish('filterby', value);
		},

        handleMoreLinks: function (e) {
            var $target = $(e.currentTarget),
                $item = $target.closest('.filter__group');

            $item.find('.filter__hidden').slideToggle(function(){
                $target.parent().find('span').toggle();
            });

            return false;
        },

        // handleExcludeItem: function (e) {

        //     var $target = $(e.currentTarget),
        //         $item = $(e.currentTarget).closest('.filter__item'),
        //         url = '',
        //         block = this.element;

        //     if ($item.hasClass('_excluded')) {
        //         $item.removeClass('_excluded');
        //     } else {
        //         $item.addClass('_excluded');
        //     }

        //     block.addClass('_disabled').find('.filter__item._selected').each(function(){
        //         var el = $(this),
        //             value = el.data('filter'),
        //             category = el.closest('.filter__group').data('category');
                    
        //             url += category + '=' + value + '&';
        //         }
        //     );
        //     block.find('.filter__item._excluded').each(function(){
        //         var el = $(this),
        //             value = el.data('filter'),
        //             category = 'ex_' + el.closest('.filter__group').data('category');
                    
        //             url += category + '=' + value + '&';
        //         }
        //     );
        //     url = '?' + url;
        //     url = url.substring(0, url.length - 1);

        //     this._sendProductsAjax(block, url, this);

        //     return false;
        // },

        // handleExcludeItemShow: function (e) {

        //     var $target = $(e.currentTarget),
        //         $item = $target.parent();

        //     clearTimeout(this.Etimer);

        //     if ($target.hasClass('_disabled') || $target.hasClass('_menu-item') || $target.parent().hasClass('_selected'))
        //     {
        //         return false;
        //     }
        //     else
        //     {
        //         this.Etimer = setTimeout(function (){
        //             $item.addClass('_excluding');
        //         }, 500);
        //     }

        // },

        // handleExcludeItemHide: function (e) {

        //     var $target = $(e.currentTarget);

        //     clearTimeout(this.Etimer);

        //     $target.parent().removeClass('_excluding');

        // }
	});
})(jQuery);
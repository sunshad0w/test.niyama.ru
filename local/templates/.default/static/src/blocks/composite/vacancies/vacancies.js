;(function ($) {
	'use strict';

	$.widget('niyama.vacancies', {
		_create: function () {
			
			this.$select = this.element.find('select');

			this._initEvents();
		},

		_initEvents: function () {
			this._on({
				'change select': this._handleSelectChange
			});
		},

		_handleSelectChange: function (e) {
			$.publish('changeVacanciesRest', this.$select.val());
		}

	});
})(jQuery);
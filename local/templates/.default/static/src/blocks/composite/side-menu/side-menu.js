;(function ($) {
	'use strict';

	$.widget('niyama.sideMenu', {
		_create: function () {
			
			this._initEvents();
		},
		_initEvents: function () {
			this._on({

			});

		$.subscribe('changeSide', $.proxy(this.handleSideSwitch, this));

		},

		handleSideSwitch: function (e) {
			var self = this;

			if (this.element.hasClass('_hidden')) {
				this.element.fadeIn('', function () {
					self.element.removeClass('_hidden');
				});
			} else {
				this.element.hide();
				this.element.addClass('_hidden');
			}
		}

	});
})(jQuery);
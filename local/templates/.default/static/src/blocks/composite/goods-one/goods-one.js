;(function ($) {
	'use strict';

	$.widget('niyama.goodsOne', {
		_create: function () {
			

			this._initEvents();
		},
		_initEvents: function () {
			this._on({
				'click .cart-buy__button': this._handleAddToCartClick,
				'click .fancybox-gallery': this._handleFancyBoxBigImg,
				'click .goods-one__bigpic-close': this._handleFancyBoxBigImgClose
			});
		},

		_handleAddToCartClick: function (e) {

			var self = this,
				pid = $(e.target).data('id'),
				price = $(e.target).data('price'),
				count = self.element.find('.select').val();

			$.ajax({
			    type: "GET",
			    url: '/ajax/add2cart.php',
			    dataType: 'json',
			    data: {
			    	pid: pid,
			    	quantity: count,
			    },
			    success: function (data) {
			    	$.fancybox.close();

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

		            $.ajax({
		                type: "GET",
		                url: '/ajax/getCartList.php',
		                data: {
		                },
		                success: function (data) {
		                	$('.cart_wrap').html(data);
		                	self.$cartProgress = $('.cart-progress');

		                	if (opened) {
		                		self.$cartProgress.show();

		                		$('.cart__table').show();
		                		$('.cart__toggle').find('div').addClass('_opened');
		                	};

		                	var cart = $('.cart').parent();

		                	$.destroyWidgets(cart);
		                	$.initWidgets(cart);
		            	}
		            });
            	}
			});

            return false;
		},

		_handleFancyBoxBigImg: function (e) {

			if ($(e.target).closest('.goods-one').parent().hasClass('fancybox-inner')) {

				$('.goods-one__bigpic').fadeToggle();

				return false;

			};

		},

		_handleFancyBoxBigImgClose: function (e) {

			$(e.target).parent().fadeOut();

		}

	});
})(jQuery);
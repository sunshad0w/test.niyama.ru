;(function ($) {
	'use strict';

	$.widget('niyama.search', {
		_create: function () {

			this.$searchField = $( "#search-field" );
			this.$clearButton = this.element.find('.search__clear-btn');

			this.$catalogParent = $('.home-page');

			// this.dataList =  [
			// 		{"type":"goods", "label" : "Салат греческий", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Салат цезарь", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Салат с морепродуктами", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Салат новый", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Салат старый", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Калифорния", "href": "/goods_one.html" },
			// 		{"type":"goods", "label" : "Филадельфия", "href": "/goods_one.html"  },
			// 		{"type":"category", "label" : "Тунец", "categoryID": "two", "href": "javascript:void(0)" },
			// 		{"type":"category", "label" : "Лосось", "categoryID": "three", "href": "javascript:void(0)" }
			// 	];

			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({
				'keyup .search__field': this._handleKeyUp,
				'click .search__clear-btn': this._handleClearClick,
				'click .search__submit-btn': this._handleSubmitClick
			});
		},
		_initPlugins: function () {
			var self = this;

			// $(function() {
			// 	self.$searchField.autocomplete(
			// 	{
			// 		source: self.dataList,

			// 		select: function( event, ui ) {
			// 			self.$searchField.val( ui.item.label );
						
			// 			switch (ui.item.type) {
			// 				case 'category': 
			// 					$.publish('selectCategory', ui.item.categoryID);
			// 					return false
			// 				break;

			// 				default:
			// 				break;
			// 			}

			// 			return false;
			// 		}

			// 	}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			// 		if (item) {
			// 			return $( "<li></li>" )
			// 				.data( "item.autocomplete", item )
			// 				.append( "<a href='" + item.href + "'>" + item.label + "</a>" )
			// 				.appendTo( ul );
			// 		} else {
			// 			return $("<li>Результатов не найдено</li>").data( "item.autocomplete", item ).appendTo( ul );;
			// 		}

			// 	};

			// });
		},
		_handleKeyUp: function (e) {
			var val = this.$searchField.val();

			if (val) {
				this.$clearButton.show();
			} else {
				this.$clearButton.hide();
			}

		},
		_handleClearClick: function () {
			this.$searchField.val("");
			this.$clearButton.hide();
			return false;
		},
		_handleSubmitClick: function () {
			var request = this.$searchField.val(),
				self = this;
			$.ajax({
				type: "POST",
				url: '/ajax/get_products.php',
				dataType: 'json',
				data: {
					"q": request
				}
			}).done(function (data) {
				/*var $data = $(data.products),
					$filter;

				self.$catalogParent.fadeOut(500, function() {
					// $('.catalog').html(data).fadeIn(500);
					// self.$catalogParent.replaceWith($data).fadeIn(500);
					self.$catalogParent.html($data).fadeIn(500);

					$filter = self.$catalogParent.find('.catalog__filters-act').children('[data-filter]');
					var filterValue = $filter.data('filter');

					$.publish('searchedFilterValue', filterValue);

					$.initWidgets();
				});*/

            	$('.page__content').fadeOut(500, function() {
            		$.publish('filterChange');

            		$('.page__content').html(data.products).fadeIn(500);

            		$('.filter').html(data.filters).fadeIn(500);

            		var catalog = $('.catalog').parent().parent(),
            			sidebar = $('.page__sidebar');
            		
					$.destroyWidgets(catalog);
					$.initWidgets(catalog);
            	});


				
			});
			return false;
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.cart', {
		_create: function () {
			
			this.$table = this.element.find('.cart__table');
			this.$droppable = $('.page__cart');

			this.$toggleButton = this.element.find('.cart__toggle-button');
			this.$toggleCircle = this.element.find('.cart__toggle-circle');

			this.$cartProgress = this.element.find('.cart-progress');

			this.$goodsCarousels = this.element.find('.goods__wrap');

			this.$pageSlider = $('.page__slider');

			this.$minSum = this.element.find( ".cart-buy__min-sum" );

			this.$ajaxOverlay = $('<div>', {
				'class': 'ajax-progress'
			});
			var $ajaxDots = $('<div>', {
				'class': 'ajax-progress__dots',
				'title': ''
			});

			this.$ajaxOverlay.append($ajaxDots);

			this.$tooltip = $("<div />");


			this.$cartProgressLinks = $('.cart-progress__link');


			this._initPopup();
			this._initEvents();
		},
		_initEvents: function () {
			// $.subscribe('addGoods', $.proxy(this._handleAdding, this));
			$.unsubscribe('filterChange', $.proxy(this._getCartList, this));
			$.subscribe('filterChange', $.proxy(this._getCartList, this));
			var self = this;

			this._on({
				'click .cart__toggle': this._handleButtonClick,
				'click .goods__remove': this._handleRemoveItem,
				'click .coupon__remove': this._handleRemoveItem,
				'click .coupon': this._handleCouponClick,
				'change .select': this._handleChangeCount,
				'click .cart-buy__button._disabled': this._handleDisableBuyButtonClick,
				'click .cart-progress__link': this._handleProgressLink
			});

			$(window).on('scroll', function () {
				if (self.$tooltip) {
					self.$cartProgressLinks.removeClass('_tipped');
					self._removeTooltip();
				}
			});

			this._on(
			    window,
			    {
			        "click": function (e) {
			            var target = $(e.target);

			            if ( !target.closest('.cart-progress').length ) {
			            	if (self.$tooltip) {
			            		self.$cartProgressLinks.removeClass('_tipped');
    							self._removeTooltip();
    						}
			            }

			        }
			    }
			);

			this.$droppable.droppable({
				hoverClass: 'dropHere',
				drop: function(event, ui) {
					// var clone = ui.draggable.clone();
		   //      	clone.find('.goods__add-btn').remove();
					// clone.find('.goods__num').remove();
		   //          // ui.draggable.find('input').remove;
		   //          // $(this).append($('<div class="goods _table">' + ui.draggable.html() + '</div>'));
		   //          var cloneClasses = '' + clone.prop('class') + '';

		   //          var cloneHTML = $('<div class="' + cloneClasses + '">' + clone.html() + '</div>').addClass('_table');
					// $.publish('addGoods', cloneHTML);


					if ( ui.draggable.hasClass('coupon') ) {
						$.publish('addCoupon', [ui.draggable] );
						return;
					}

					var currentGoods = ui.draggable.get(0);
					$.publish('addGoods', currentGoods);
				}
			});

			var pageCartOffset = $('.page__cart').offset().top;

			if (!window.isInitScrollEvent) {
				window.isInitScrollEvent = true;

				$(window).scroll(function(){
				var scroll = $(window).scrollTop(),
					$cartToggleCircle = self.element.find('.cart__toggle-circle'),
					$toggleButton = self.element.find('.cart__toggle-button'),
					$pageCart = $('.page__cart');

				if ( $cartToggleCircle.hasClass('_opened') ) {
					if (scroll >= pageCartOffset + $pageCart.height() ) {
						$('.cart__table').hide();
						$('.cart-progress').hide();
						$cartToggleCircle.removeClass('_opened');
						$toggleButton.removeClass('_opened');

						// $pageCart.addClass('_fixed');

						$(window).scrollTop( $('.page__main').offset().top - 125 );

					} else { 
						$pageCart.removeClass('_fixed');
					}
				} else {

					if (scroll >= pageCartOffset) {
						$pageCart.addClass('_fixed');
					} else { 
						$pageCart.removeClass('_fixed');
					}

				}

			});


				$('.page__cart').on('click', function(e){

					var $target = $(e.target),
						$this = $(this),
						animate = false;

					if ( ($target.is('.clearfix, .page__cart, .cart, .page__contaner') || $target.closest('.cart-buy__sum').length)  && !self.element.find('.cart__toggle-circle').hasClass('_opened')) {
						$('html, body').animate(
							{
								scrollTop: pageCartOffset - 5
							}, 
							'700', 'swing', function () {
								if (!animate) {
									($.proxy(self._openCart, self))();
									animate = true;
								}
							});
					}

					return true;
				});

			}


		},

		_handleCouponClick: function (e) {
			var $this = $(e.currentTarget);

			if ( !$(e.target).hasClass('coupon__remove') ) {
				if ( $this.hasClass('_disabled') || $this.hasClass('_table') ) {
					console.log('HAS!');
					e.preventDefault();
					e.stopPropagation();
					return false;
				}
			}
		},

		_initPopup: function () {
			this.$removePopup = $('<div class="goods-one _removed"><div class="goods-one__img"><img src=""/></div><div class="goods-one__desc"><p class="goods-one__about"></p></div><div class="goods-one__side"><a class="btn _full-width _style_1 goods-one__add">Вернуть</a></div></div>');
		},

		_handleDisableBuyButtonClick: function (e) {
			var $minSum = this.$minSum,
				$target = $(e.currentTarget);

				$minSum.effect("shake", {
					times: 2
				});
		},

		_handleProgressLink: function (e) {
			var $target = $(e.currentTarget),
				top = $target.offset().top,
				left = $target.offset().left,
				text = $target.data('title');

			if ($target.hasClass('_tipped')) {
				$target.removeClass('_tipped');
				this._removeTooltip();
			} else {
				this.$cartProgressLinks.removeClass('_tipped');

				if (this.$tooltip) {
					this._removeTooltip();
				}
				$target.addClass('_tipped');
				this._toggleTooltip(top, left, text);
			}

		},

		_toggleTooltip: function (top, left, text) {
			if (this.$tooltip) {
				this._removeTooltip();
			} else {
				var self = this;

				console.log(top, left);

				this.$tooltip = $("<div />", {
					"class": "tooltip"
				}).css({"top": top,"left": left}).text(text);

				this.$tooltip.appendTo('body');
			}
		},

		_removeTooltip: function () {
			this.$tooltip.detach();
			this.$tooltip = null;
		},

		_handleButtonClick: function (e) {
			var self = this;
			this.$table.removeClass('_min-height');

			if (this.$toggleButton.hasClass('_opened')) {
				self.$cartProgress.hide();
			}
			else {
				$('body,html').animate({'scrollTop': 0}, 500);
			}

			if (this.$pageSlider.length) {
				this.$pageSlider.slideUp('',function () {
					$(this).remove();
				});
			}
			
			this.$table.slideToggle(function () {
				self.$toggleButton.toggleClass('_opened');
				self.$toggleCircle.toggleClass('_opened');

				if (self.$toggleButton.hasClass('_opened')) {

					self.$table.css({
						'overflow': 'visible'
					});

					self.$cartProgress.fadeIn();
				}

				self.$goodsCarousels.each(function () {
					var owl = $(this).data('owlCarousel');

					if (owl) {
						owl.reinit();
					}

				})
			});
		},

		_openCart: function () {
			var self = this;
			this.$table.removeClass('_min-height');
			$('body,html').animate({'scrollTop': 0}, 500);

			if (this.$pageSlider.length) {
				this.$pageSlider.slideUp('',function () {
					$(this).remove();
				});
			}
			
			$('.cart__table').slideDown('', function () {
				self.$toggleButton.addClass('_opened');
				self.$toggleCircle.addClass('_opened');
				self.$table.css({
					'overflow': 'visible'
				});

				$('.cart-progress').fadeIn();

				self.$goodsCarousels.each(function () {
					var owl = $(this).data('owlCarousel');

					if (owl) {
						owl.reinit();
					}

				})
			});
		},

		_handleRemoveItem: function (e) {
			e.preventDefault();

			this.$ajaxOverlay.appendTo(this.$droppable.addClass('_relative'));

			var self = this,
				$target = $(e.currentTarget),
				$parent = $target.parent(),
				id = $target.data('id'),
				quantity = $parent.find('select.select').val(),
				name = $parent.find('.goods__name').eq(0).text(),
				idForCancel = $parent.find('.goods__item').data('id'),
				textForCancel = 'Блюдо "' + name + '" было удалено из Вашего заказа. Если это произошло случайно - можно вернуть блюдо обратно.',
				imageUrl = $parent.find('.goods__image').find('img').prop('src');

			$.ajax({
				type: "GET",
				url: '/ajax/removeFromCart.php',
				dataType: 'json',
				data: {
					ID: id,
				},
				success: function (data) {
					self.$ajaxOverlay.detach();
					self.$droppable.removeClass('_relative');

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);

					self._getCartList(false);

					$(e.target).parent().remove();
					if (!self.$table.children().length){
						self.$table.removeClass('_filled');
					}

					var $popup = self.$removePopup.clone(),
						$cancelButton = $popup.find('.goods-one__add'),
						currentUrl = $parent.hasClass('_by-coupon') ? '/ajax/add2cartcoupon.php' : '/ajax/add2cart.php';

					$popup.find('.goods-one__about').text(textForCancel);
					$popup.find('.goods-one__img').find('img').prop('src', imageUrl);


					function cancelRemoving (url, id) {
						$cancelButton.off('click');

						$.ajax({
							type: "GET",
							url: url,
							dataType: 'json',
							data: {
								pid: id,
								quantity: quantity,
							},
							success: function (data) {
								if (!data) {
									alert("Что-то пошло не так.");

									return;
								}

								if (data.ERROR) {
									var $error = $('<p />').text(data.ERROR);

									$.fancybox({
										content: $error,
										padding: [50, 40, 0, 40]
									});

									return;
								}

								self._getCartList(true);
							}
						});
					};

					function handleCtrlZ(e) {
						var code = e.keyCode || e.which;

						  if (e.keyCode == 26 && e.ctrlKey) {
								$(document).off('keypress');
								cancelRemoving(currentUrl, idForCancel);
							}
					}

					if ($parent.hasClass('goods')) {
						$(document).off('keypress'); 
						$(document).on('keypress', handleCtrlZ);

						$cancelButton.one('click', function () {
							cancelRemoving(currentUrl, idForCancel);
						});

						$.fancybox({
							content: $popup
						});
					}

					var $coupons = $('.coupons');
					if ( $coupons.length ) {
						$coupons.find('.coupon:data(id==' + id + ')').removeClass('_used _disabled');
					}

				}
			});
		},

		_handleChangeCount: function (e) {
			var self = this,
				id = $(e.target).data('id'),
				count = $(e.target).val();

			$.ajax({
				type: "GET",
				url: '/ajax/setCartQuantity.php',
				dataType: 'json',
				data: {
					ID: id,
					quantity: count
				},
				success: function (data) {
					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);

					// if (!self.$table.children.lendth){
					// 	self.$table.removeClass('_filled');
					// }
					self._getCartList(true);
				}
			});
		},

		_getCartList: function (closeFancybox) {
			var self = this;

			if ($('.cart__toggle-circle').hasClass('_opened')) {
				var opened = true;
			}

			$.ajax({
				type: "GET",
				url: '/ajax/getCartList.php',
				data: {
				},
				success: function (data) {
					if (closeFancybox) {
						$.fancybox.close();
					}

					$('.cart_wrap').html(data);
					var $table = $('.cart__table');
					self.$table = $table;
					self.$cartProgress = self.element.find('.cart-progress');

					if (opened) {
						self.$cartProgress.show();

						$table.show();
						$('.cart__toggle').find('div').addClass('_opened');
					};

					var cart = $('.cart').parent();

					$.destroyWidgets(cart);
					$.initWidgets(cart);

					$table.addClass('_filled');
				}
			});

			this._getCouponsList();
		},

		_getCouponsList: function () {
			var $coupons = $('.coupons');
			if ($coupons.length) {
				$.ajax({
					type: "GET",
					url: '/ajax/getCouponsList.php',
					data: {

					},
					success: function (data) {
						$('.coupons').replaceWith($(data));
						$.initWidgets();
					}
				});
			}
		},

		_handleAdding: function (e, cloneHTML) {
			// var $changeBlock = $('<div class="goods__change"> </div>'),
			// 	$remove = $('<div class="goods__remove"> </div>'),
			// 	self = this;

			// $remove.on('click', function () {

			// 	var $el = $(this).parent(),
			// 		pid = $remove.data('id');

	  //           $.ajax({
	  //               type: "GET",
	  //               url: '/ajax/removeFromCart.php',
		 //            dataType: 'json',
	  //               data: {
	  //               	ID: id,
	  //               },
	  //               success: function (data) {
			// 			$('.cart-buy__sum i').text(data.TOTAL_PRICE);
			// 			$('#guests__card-sum').text(data.SUB_PRICE);

			// 			$el.remove();
			// 			if (!self.$table.children.lendth){
			// 				self.$table.removeClass('_filled');
			// 			}
	  //           	}
	  //           });

			// })

			// var $el = $(cloneHTML);

			// if ($el.hasClass('goods')) {
			// 	$el.append($changeBlock).append($remove);
			// }

			// this.$table.append($el).addClass('_min-height').addClass('_filled');
			this.$table.addClass('_min-height').addClass('_filled');
		}


	});
})(jQuery);
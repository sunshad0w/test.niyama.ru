;(function ($) {
	'use strict';

	$.widget('niyama.vacanciesForm', {
		_create: function () {
			this.$fileInput = this.element.find('.input__file-input');
			this.$fileTitle = this.element.find('.input__file-name');

			// Проверка наличия file_api у браузера для стилизации input[type="file"]
			this.file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
			
			this._initEvents();
		},
		_initEvents: function () {
			this._on({
				'click .vacancies-form__submit': this._handleSubmit,
				'submit form': this._handleSubmit,
				'keyup input': this._handleKeyup,
				'change .input__file-input': this._handleFileChange
			});
		},
		_handleSubmit: function () {
			this.element.find('.errortext').parent().remove();
		},
		_handleKeyup: function (e) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				this.element.find('.errortext').parent().remove();
			}
		},
		_handleFileChange: function(e){
			var $currentTarget = $(e.currentTarget);
			this._changeFileTitle($currentTarget);
		},

		_changeFileTitle: function(inp){
			var file_name,
				lbl = this.$fileTitle,
				btn = inp.siblings('span.form__gray_file_button'),
				self = this;

			// Если есть поддержка file_api, имя берём оттуда, если нет, выдёргиваем из значения инпута
	        if( this.file_api && inp[ 0 ].files[ 0 ] ) {
	            file_name = inp[ 0 ].files[ 0 ].name;
	        } else {
	            file_name = inp.val().replace( "C:\\fakepath\\", '' );
	        }


	        if( file_name.length ) {
	            lbl.text( file_name );
	        } else {
	        	lbl.text('Выбрать файл...');
	        	this._clearFileTitle(inp, lbl);
	        }
		},
		_clearFileTitle: function(inp, lbl){
			inp.val('');
			lbl.text('Выбрать файл...');
		}
	});
})(jQuery);
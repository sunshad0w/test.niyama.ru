;(function ($) {
	'use strict';

	$.widget('niyama.togglePass', {
			_create: function () {
				this.$passField = this.element.find('input[type="password"]');
				this.$passButton = this.element.find('.input__showpass');

				this.hidden = true;
				this._initEvents();
			},

			_initEvents: function () {
				this._on({
					"click .input__showpass": this._togglePassVisibility
				});
			},

			_togglePassVisibility: function (e) {

				if (this.hidden) {
					this.$passField.prop('type', 'text');
				} else {
					this.$passField.prop('type', 'password');
				}

				this.$passButton.toggleClass('_opened');
				this.hidden = !this.hidden;

				return false;
			}
		});
})(jQuery);
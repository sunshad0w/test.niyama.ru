;(function ($) {
	'use strict';

	$.widget('niyama.restsInfo', {
		_create: function () {

			this.$carousel = this.element.find('.rests-info__carousel');
			
			this._initEvents();
			this._initPlugins();
		},

		_initEvents: function () {
			this._on({

			});

			$.subscribe("restsInfo", $.proxy(this._renderInfo, this) )
		},

		_initPlugins: function () {
			this.$carousel.owlCarousel({
				lazyLoad : true,
				navigation : true,
				mouseDrag: false,
				items: 6,
				itemsDesktop: [1279, 5],
				itemsDesktopSmall: [1023, 4],
				itemsTablet: false, //2 items between 600 and 0
				itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
				singleItem: false,
				pagination: false
			});
		},

		_renderInfo: function (e, data, isOne) {
			if (isOne) {
				this.element.replaceWith(data);
				$.initWidgets();
			} else {
				this.element.html(data);
			}
		}
	});
})(jQuery);
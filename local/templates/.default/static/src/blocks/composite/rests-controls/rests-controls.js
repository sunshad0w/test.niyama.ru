;(function ($) {
	'use strict';

	$.widget('niyama.restsControls', {
		_create: function () {
			this.filters = {};

			this.$services = this.element.find('.rests-controls__filter input');

			this.$cityWrap = this.element.find('.rests-controls__city');
			this.$stationWrap = this.element.find('.rests-controls__station');

			this.$citySelect = this.$cityWrap.find('select');
			this.$stationSelect = this.$stationWrap.find('select');

			this.services = this.$services.serializeArray();
			this.cityID = this.$cityWrap.find('.select').val();
			this.stationID = this.$stationWrap.find('.select').val();

			this.region = this.$cityWrap.find('option:selected').data('city');

			this._initEvents();
			this._initFilters();

		},

		_initEvents: function () {
			this._on({
				'change .checkbox': this._handleFilterChange,
				'change .rests-controls__city .select': this._handleCityChange,
				'change .rests-controls__station .select': this._handleStationChange
			});

			$.subscribe('restsCityChange', $.proxy(this._setCityByRest, this));

			// $.subscribe('oneRestFilters', $.proxy(this._setChecboxesByOneRest, this));

		},

		_initFilters: function (checkedStat) {
			var checkedStat = checkedStat ? checkedStat : false;

			this.filters['services'] = this.services;
			this.filters['city'] = parseInt(this.cityID);


			if (this.region === "REGION_1") {
				this.filters['station'] = parseInt(this.stationID);
			} else {
				delete this.filters['station'];
			}

			$.publish('allRestsFilters', [this.filters, checkedStat]);
		},

		_handleFilterChange: function (e) {
			var checkedStat = e ? $(e.currentTarget).prop('checked') : null;

			this.services = this.$services.serializeArray();

			this._initFilters(checkedStat);
		},

		_handleCityChange: function (e) {
			this.$services.prop('checked', false);

			var $target = $(e.currentTarget),
				value = $target.val();

			this.cityID = value;
			this.region = $target.find('option:selected').data('city');

			// this._initFilters();
			this._handleFilterChange();
		},

		_handleStationChange: function (e) {
			var $target = $(e.currentTarget),
				value = $target.val();

			this.stationID = value;

			this._initFilters();
		},

		_setChecboxesByOneRest: function (e, filters) {
			this.$services.prop('checked', false);

			for (var i = filters.length - 1; i >= 0; i--) {
				this.$services.filter('[value=' + filters[i] + ']').prop('checked', true);
			};
		},

		_setCityByRest: function (e, cityID) {
			this.$citySelect.val(cityID);
			this.$citySelect.trigger("chosen:updated");

			this.cityID = cityID;

			this.filters['services'] = this.services;
			this.filters['city'] = parseInt(this.cityID);

			var $selOption = this.$citySelect.find('option:selected');

			if ( $selOption.data('city') === "REGION_1" ) {
				this.$stationSelect.parent().removeClass("_hidden");
			} else {
				this.$stationSelect.parent().addClass("_hidden");
			}

			// this._initFilters();
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.sideSwitch', {
		_create: function () {
			this.$title = $('h1').eq(0);
			this.$filter = $('.filter');

			this.$ajaxOverlay = $('<div>', {
				'class': 'ajax-progress'
			});

			var $ajaxDots = $('<div>', {
				'class': 'ajax-progress__dots'
			});

			this.$ajaxOverlay.append($ajaxDots);

			this._initEvents();
		},

		_initEvents: function () {
			this._on({
				'click': this._handleSwitchClick
			});

		},

		_handleSwitchClick: function (e) {
			e.preventDefault();

			var self = this;

			self.$ajaxOverlay.appendTo('body');

			$.ajax({
			    type: "GET",
			    url: '/ajax/changeMode.php',
			    dataType: 'json',
			    data: {
			    },

			    success: function (data) {
			    	if (data.type === "filter") {
			    		self.$filter.removeClass("_menu");
			    	} else if (data.type === "menu") {
			    		self.$filter.addClass("_menu");

			    		$.ajax({
			    			type: "GET",
			    			url: '/ajax/get_products.php?page=1&nofilter=yes',
			    			datatype: 'json',
			    			success: function (data) {
			    				var $data = $.parseJSON(data);

			    				$('.page__content').fadeOut(500, function() {

			    					$('.page__content').html($data.products).fadeIn(500);

			    					var catalog = $('.catalog').parent();

			    					$.destroyWidgets(catalog);
			    					$.initWidgets();


			    				});
			    			}
			    		});

			    	}

			    	self.$ajaxOverlay.detach();

			    	self.$title.text(data.title);

			    	self.element.text(data.linkTitle);

			    	self.$filter.html( $(data.filters) );

			    	$.initWidgets();

			    	var sidebar = $('.page__sidebar');

					$.destroyWidgets(sidebar);
					$.initWidgets(sidebar);

			    }
			})
		}

	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.coupons', {
		_create: function () {
			
			this.$coupons = this.element.find('.coupon');

			this.$coupons.filter('._disabled').removeClass('fancybox');

			this._initEvents();
		},
		_initEvents: function () {
			var self = this;

			this._on({
				'dblclick .coupon': this._handleCouponAdding,
				'click .coupon': this._handleCouponClick,
				'click .coupon__add': this._handleCouponAdding
			});

			if ($('html').hasClass('bx-no-touch')) {
				this.$coupons.draggable({
					helper:'clone',
					appendTo: 'body',
					scroll: false,
					
					drag: function(e,ui) {
						return !$(this).hasClass('_disabled');
						// ui.position.top -= $('body').scrollTop();
					},

					accept: function() {
					},

					stop: function(e,ui) {
						return;
					}
				});
			}

			$.subscribe('addCoupon', $.proxy(this.handleAddToCartDrug, this) );

			// self.handleAddToCartDrug(this, $(this) );
		},

		_handleCouponClick: function (e) {
			var $this = $(e.currentTarget);

			if ( $this.hasClass('_disabled') || $this.hasClass('_table') ) {
				console.log('HAS!');
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		},

		_handleCouponAdding: function (e) {
			var el = this.getCouponElement(e);

			if ( !el.hasClass('fancybox') ) {
				this.handleAddToCartClick(this, el);
			}

		},

		getCouponElement: function (e) {
			var $target = $(e.target),
				el;

			if ( $target.hasClass('_disabled') || $target.parents('.coupon').hasClass('_disabled') ) {
				return false;
			} else {
				if ($target.parents('.coupon').length) {
					el = $target.parents('.coupon');
				} else {
					el = $target;
				}
			}

			return el;
		},

		handleAddToCartClick: function (self, el) {

			var pid = el.data('id');

				
					// clone.find('.goods__add-btn').remove();
					// clone.find('.goods__num').remove();
					// clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
				var clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
				clone2 = el.clone().css({position:'absolute',top:el.offset().top, left:el.offset().left, width: el.width(), 'z-index': 100}).appendTo('body');
				clone2.animate({top:'300px', opacity:0},
					1000,
					function(){
						clone2.remove();
						var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
						$.publish('addGoods', cloneHTML);
					});

            $.ajax({
                type: "GET",
                url: '/ajax/add2cartcoupon.php',
                dataType: 'json',
                data: {
                	pid: pid
                },
                success: function (data) {
					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

					var need_update_coupon = data.need_update_coupon;

		            $.ajax({
		                type: "GET",
		                url: '/ajax/getCartList.php',
		                data: {
		                },
		                success: function (data) {
		                	$('.cart_wrap').html(data);
		                	if (opened) {
		                		$('.cart__table').show();
		                		$('.cart__toggle').find('div').addClass('_opened');
		                	};

		                	var cart = $('.cart').parent();

		                	$.destroyWidgets(cart);
		                	$.initWidgets(cart);

		                	el.addClass('_disabled _used').off('click').removeClass('fancybox');

		                	if (need_update_coupon) {
			                	$.ajax({
			                		type: "GET",
			                		url: '/ajax/getCouponByCartId.php',
			                		data: {
			                			pid: pid
			                		},
			                		success: function (data) {
			                			// console.log(data);
			                		}
			                	});
		                	}

		            	}
		            });
            	}
            });

            return false

		},

		handleAddToCartDrug: function (event, el) {

			if ( el.hasClass('_disabled') ) { 
				return;
			}

			var pid = el.data('id');

            $.ajax({
                type: "GET",
                url: '/ajax/add2cartcoupon.php',
                dataType: 'json',
                data: {
                	pid: pid
                },
                success: function (data) {
					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);
					if ($('.cart__toggle-circle').hasClass('_opened')) {
						var opened = true;
					}

		            $.ajax({
		                type: "GET",
		                url: '/ajax/getCartList.php',
		                data: {
		                },
		                success: function (data) {
		                	$('.cart_wrap').html(data);
		                	if (opened) {
		                		$('.cart__table').show();
		                		$('.cart__toggle').find('div').addClass('_opened');
		                	};

		                	var cart = $('.cart').parent();

		                	$.destroyWidgets(cart);
		                	$.initWidgets(cart);

		                	el.addClass('_disabled _used').off('click').removeClass('fancybox');
		            	}
		            });


            		var $coupons = $('.coupons');
            		if ($coupons.length) {
			            $.ajax({
			            	type: "GET",
			            	url: '/ajax/getCouponsList.php',
			            	data: {

			            	},
			            	success: function (data) {
				            		$('.coupons').replaceWith($(data));
				            		$.initWidgets();
			            	}
			            });
            		}
            	}
            });
		}

	});
})(jQuery);
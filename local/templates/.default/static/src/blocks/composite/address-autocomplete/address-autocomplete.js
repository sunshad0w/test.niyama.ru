;(function ($) {
	'use strict';

	$.widget('niyama.addressAutocomplete', {
		_create: function () {

			// block2
			this.$city = this.element.find('select._city').eq(0);
			this.$metro = this.element.find('input._metro');
			this.$restaurant = this.element.find('select._restaurant');

			this.$address = this.element.find('input._address');
			this.$house = this.element.find('input._house');
			this.$apart = this.element.find('input._apart');
			this.$porch = this.element.find('input._porch');
			this.$intercom = this.element.find('input._intercom');
			this.$floor = this.element.find('input._floor');

			this.$soon = this.element.find('input._soon');
			this.$certain = this.element.find('input._certain');
			this.$deliveryRadios = this.$soon.add(this.$certain);
			this.$deliveryTimeOptions = this.element.find('.ordering-form__row._certain');

			this.$certainTime = this.element.find(".ordering-form__row._certain").find('select');
			this.$certainDate = this.element.find(".ordering-form__row._certain").find('input');

			this._initPlugins();

		},
		_initEvents: function () {

		},

		_initPlugins: function () {
			var self = this;

			 $(function() {
			    function log( message ) {
			      $( "<div>" ).text( message ).prependTo( "#log" );
			      $( "#log" ).scrollTop( 0 );
			    }

			    function getHousesData ( request, response, street ) {
			    	var city = self.$city.find('option:selected').text();

			      	var address = self.selectedStreet || request.term;

			      	var house = request.term;

			        $.ajax({
			          url: '/ajax/search_streets.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
						house: house
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			    }

			    function getAddressesData ( request, response, street ) {

			      	var city = self.$city.find('option:selected').text();

			      	//var address = self.selectedStreet || request.term;
			      	var address = self.$address.val();

			        $.ajax({
			          url: '/ajax/search_streets.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			      }

			    function getMetroData ( request, response, station ) {

			      	var city = self.$city.find('option:selected').text();

			      	//var address = self.selectedStreet || request.term;
			      	var address = self.$metro.val();

			        $.ajax({
			          url: '/ajax/search_metro.php',
			          dataType: "json",
			          data: {
						address: address,
						city: city,
			          },
			          success: function( data ) {
			          	var array;

			          	if ( data.buildings ) {
			          		array = $.map( data.buildings, function(value, index) {
			          		    return [value.full_name];
			          		});
			          	} else {
				          	array = $.map( data.streets, function(value, index) {
				          	    return [value.name];
				          	});
			          	}


			            response( array );
			          }
			        });
			      }

			      function initHouseAutocomplete(street) {
			      	self.selectedStreet = street;

			      	self.$house.autocomplete({
			      		source: getHousesData,
			      		select: function( event, ui ) {
			      			self.$house.focus();
			      		  log( ui.item ?
			      		    "Selected: " + ui.item.label :
			      		    "Nothing selected, input was " + this.value);
			      		},

			      		open: function() {
			      			var $this = $(this);
			      		  $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			      		  var menu = $this.data("uiAutocomplete").menu.element;
			      		  menu.addClass("ordering-form__autocomplete _house");
			      		  menu.wrapInner('<div />');
			      		},

			      		close: function() {
			      			var $this = $(this);
			      		  $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			      		 	var menu = $this.data("uiAutocomplete").menu.element;
			      		},

			      		change: function() {
			      			var $this = $(this);

			      			var menu = $this.data("uiAutocomplete").menu.element;
			      			menu.addClass("ordering-form__autocomplete _house");
			      		},

			      		focus: function( event, ui ) {
			      				event.preventDefault(); // without this: keyboard movements reset the input to ''
			      				$(this).val(ui.item.label);
			      		}
			      	});
			      }


			    self.element.find(".input._address").autocomplete({
			      source: getAddressesData,
			      minLength: 3,
			      select: function( event, ui ) {
			      	initHouseAutocomplete(self.$address.val());
			      	self.$house.focus();
			        log( ui.item ?
			          "Selected: " + ui.item.label :
			          "Nothing selected, input was " + this.value);
			      },

			      open: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

			        var menu = $this.data("uiAutocomplete").menu.element;
			        menu.addClass("ordering-form__autocomplete");
			        menu.wrapInner('<div />');

			      },

			      close: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

			       	var menu = $this.data("uiAutocomplete").menu.element;

			      },

			      change: function() {
			      	var $this = $(this);

			      	var menu = $this.data("uiAutocomplete").menu.element;
			      	menu.addClass("ordering-form__autocomplete");

			      	// var pane = $(".ordering-form__autocomplete"),
			      	// 	api = pane.data('jsp');
			      	// 	api.reinitialise();;

			      },

			      focus: function( event, ui ) {
			      		event.preventDefault(); // without this: keyboard movements reset the input to ''

			      		delete self.selectedStreet;
			      		// console.log(event, ui);

			      		$(this).val(ui.item.label);
			      }
			    })

			    self.element.find(".input._metro").autocomplete({
			      source: getMetroData,
			      minLength: 3,
			      select: function( event, ui ) {
			        log( ui.item ?
			          "Selected: " + ui.item.label :
			          "Nothing selected, input was " + this.value);
			      },

			      open: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

			        var menu = $this.data("uiAutocomplete").menu.element;
			        menu.addClass("ordering-form__autocomplete");
			        menu.wrapInner('<div />');

			      },

			      close: function() {
			      	var $this = $(this);

			        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

			       	var menu = $this.data("uiAutocomplete").menu.element;

			      },

			      change: function() {
			      	var $this = $(this);

			      	var menu = $this.data("uiAutocomplete").menu.element;
			      	menu.addClass("ordering-form__autocomplete");

			      	// var pane = $(".ordering-form__autocomplete"),
			      	// 	api = pane.data('jsp');
			      	// 	api.reinitialise();;

			      },

			      focus: function( event, ui ) {
			      		event.preventDefault(); // without this: keyboard movements reset the input to ''

			      		delete self.selectedStreet;
			      		// console.log(event, ui);

			      		$(this).val(ui.item.label);
			      }
			    })

			  });
		}

	});
})(jQuery);



;(function ($) {
	'use strict';

	$.widget('niyama.vacancy', {
		_create: function () {
			
			this.$aboutRests = this.element.find('.vacancy__about');
			this.$links = this.element.find('.vacancy__rest');

			this.restsArray = this.element.data('rests');

			this._initEvents();
		},
		_initEvents: function () {
			this._on({
				'click .vacancy__rest': this._handlePseudoLinkClick
			});

			$.subscribe('changeVacanciesRest', $.proxy(this._changeVacanciesRest, this));
		},
		_handlePseudoLinkClick: function (e) {
			var $current = $(e.currentTarget);

			if ($current.hasClass('_active')){
				this._hideAbout($current);
			} else {
				var id = $current.data('id');
				this._showAbout(id, $current);
			}


			return false;
		},
		_showAbout: function (id, $current) {
			this.$links.removeClass('_active');
			$current.addClass('_active');

			this.$aboutRests.slideUp();
			
			this.element.find('#'+id).slideDown('');
			
		},
		_hideAbout: function ($current) {
			$current.removeClass('_active');
			this.$aboutRests.slideUp();
		},

		_changeVacanciesRest: function (e, value) {
			var isInArray = this._checkNumberInArray(parseInt(value));

			if (isInArray) {
				this.element.show();
			} else {
				this.element.hide();
			}
		},

		_checkNumberInArray: function (value) {
			return this.restsArray.indexOf(value) > -1;
		}
	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.restsMap', {
		_create: function () {

			this.mapCanvas = null;
			this.contactsMap = null;
			this.ymCoords = [];
			this.placeMarks = [];

			this.timer = '';

			this.rests = {};
			this.restsHTML = '';

			this.filters = {};

			this.$navLinks = $('.nav-side__link');
			this.$sideBar = $('.page__sidebar');
			this.$pageContent = $('.page__content._rests');

			this.$allRestsLink = this.$sideBar.find('.label');

			this.restsURL = this.element.data("restsUrl");
			this.restsOneURL = this.element.data("oneRestUrl");

			// this.$restsInfo = this.element.find('.rests-info');
			// this.$restsInfo = $('.rests-info');

			this.placeMarkTemplate = {
					iconLayout: 'default#image',
					iconImageHref: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAvCAYAAACCLMghAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAHj0lEQVRYw8WYe1DU1xXHP/tgeblEQJ4CSgAFFBV5GFFQjBiN4yOJZirRUk3T1KaPzNhmOjZ9xMSq7Uw6TdL4GDSJRpPG2ARDqkTRVHlohIkgRKU8BBeBgKvg8mZ3+8f9/dbfwooLse2Z2Zm9557H9557zrn3/lSZEThLXsBiYD4wAwgE/KW5VqAJKAO+BI4D7c4YVTkBIA54CXgKcHcSbDdwBHgd+Ho4QfUwc/7Ae9Kq1o7AOZLsWqAUOAQEjTQC6ZJioJI5KWEOKcszmfLIArx8/fDQjwXAbB7AdKuNqyUF5Oz8Iw1XygfbawWygGPOAMgE3gF0MiM+fSnPbt2Dt3+wU8u/2XSdD3a8RHHuh0q2Gfgh8K6SqR2kuxzYD2gAXFzd+NH2faQsWyMMVxfhYTyLteYQ1s4b9HXdxmIBV50WlbsvusTNqINS8Q2KI+v3b2Ixmzl/7LBsWwNkAz2ADZkyAg8DFwE9gIf+IV77tISACZH0mG7TV7oDdfl2m3BJFRja4HorPDEHQsbdXYXbko/QRq7GarVSf/kiO37wGB3GVnm6C0gAroB9Eu6TnQNs2nOUgAmRNFecxPyPJDvnADnFsCgBVqaAWmUfxp5jT9NfsQuVSsXE2Hi25Zbh5mkz7SFtsUoJYAUwT5b43q+2E52Uxq2GSvQXNmBtrx6yz9/ehq4eCPWDYN+hedB7eiOde0UOP+QXyC/3HFVOPwKsAtDE+dhWHwoQkzyP57Zli6z54kkwVjhMtPI6uNMN4YHgosUx9Xei8gxEE5CEX8hEblRfxvDvSnk2FNirBiYBKTJ3/Za3ATCe2QrNhQ7tWqyQlQGX6oaG31Ekes/8HIC1L/9lcBQmq4GlMidiejLjI2NpqfoKXdnLQ4w1GWFfHhwthsY2WJoMbjqRiKfL4NBp6O51EIiyNzE1luPtH0xYzHTl1BI1kCyPZi1ZLf40Fwwxkn8Rdn8OixNF4s2KFj8QeZA+XVRC9nHHkdC2ngVgYeZGJXu2GoiVRzGz5gMwpvmwnXJeKXxZBptWOU44m7VYqL4BVQYH21a5E4Do5DQlO0YN2NqbT2CI+GOqt1POKYLVqaC/z2ngooHJIVD4zdA5q+k6AL6yD0HBWhS1r3WRuq+5z055VjScuwKTQsSef10DTTfBRw8aDcyMBI1U0LFhcKrMAQCzSA6tzlXJ1msBG8diHhB/1PZ1lZUBuedh24fgrYcAbxgwg1YL4QGglpxbEYlqvOMgPDIAF52Sq9MCRsAHQO/jB0DvnVZ0GgV6K0SHQmQw1DYJnrurcNbbJ/Y9Lhz+/i9RBb9YOdS/SicC3dNlUrKNaqBZHtVdKgGgy3+pvbIKgnzgq6sQEwaPJ0NCFBhaQauBgkqRpO2d8MwCiBrvAIBXuPDYZJehLVqgEqkSzh//mIenJTEQsgJufmaTajLC259BZjp4j4HefvDygI3LhNPFSaKxt9yCXZ/D3CnwaLw9gP5JLwBQee6Ukn1ZDRTJo9KTOQB4hM21SXR0wetHIHWqaDany0Sou3rh21siKn/9BF49KA6odQuh4hrsPQ59A3c9WfyFzRMH/qYEUKgGcqX8oan2KrXlF/CfOJmbUa8AIvl89JCRAE/NhYhg8HCF1w7Bnw5DXomI0GNSgwL42UpRsu98IeXQnJ14h8bSWP0NjdV2NfqpfB/IBxYATJm9gM0H8unt7MB8JJ7dH9RiaBMIW24JrajxkBgF0yNESy6vg2st0G4SjcrLA4wm0KrhxfWT8Vx3BavVyuZlM5TXtSJgjnwaNgLfB2g11BE4IYrwuCRMnolMcz3BNUMHxg5YOBOeXQwLZsDEQHDXgf9YUQGpU0W/8HQTp6P3GEhPGY9u4Xt4+E6gMOd98g/tUq7+BaBKeSPKRTqYdO4evHK4mLDoaRhritEVrIOOGkZCKn0Y3cnZjIvNwFBVweblMzEP9MvTZxDvC6vyRvQTpMdEX3cXr65Jo/7yRXwiZqNbdQF19AbJsub+zmf+jv6ME4yLzaClvpota9KUznuAH8t5J28BkvMq4GlA1d/Xy9lP9hMQFsHEqUm4RKygx2cu+iXvovIMwtyQB1aLDZRmfBqqmOfpnvoHxsZn4eblS2VRPlvXPUrXHbtH0k8RLyeh6uBavg34tZIxb9V6Vr24BZ/AEKwWCyr1cO8ZaGusJ/s3z3Gp4MTgqZ1SpO9GywEAFXAAeEbJdHF1I2nRE4THJRI/fyn+EyLQaMSZYbVYaGmo4dw/P6L8bB5VpYVYLZbBdk8BixDvg2EBgHiUHEMqzQdAl4BUHDxY7xXLPuBJSfG7kgF4nHu8lofbzHZJsZnRk2zDcC8B9X0MGICViNfMSMmpKN4PAMB54PlRANiESLxhyRkAAO8DB0fg/CDwljOCzgIAUb+1Tsg1MKjWHxSADsS3A8swMlZggyT7wAGAyId9w8x/jDjanaaRAgD4LWBywO9BfMwaEY0GQDOw2wF/P3DtfwEA4A1EnctkAf48GkOjBdAA5CjGuUD1aAyNFgCI3iDTSHqEHWlHq4i4VBgRX7+OjtbIdwHQB+RJNnr+HwBA9Pr7XxL/iwBOIn1uGy39B25CXjNMkIeqAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE0LTA2LTAyVDEzOjM3OjA1KzAwOjAwNWc92AAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNC0wNi0wMlQxMzozNzowNSswMDowMEQ6hWQAAAAASUVORK5CYII=',
					iconImageSize: [32, 47],
					iconImageOffset: [-16, -47]
				};

			this._initEvents();
			this._initPlugins();

		},

		_initEvents: function () {
			this._on({

			});

			// клики на ссылки в сайдбаре
			// this.$navLinks.on('click', $.proxy(this._handleNavSideLinkClick, this) );

			// Подписка на события изменений верхних фильтров
			$.subscribe('allRestsFilters', $.proxy(this._handleFiltersChange, this));

			// $.subscribe('restsFilters',$.proxy(this._filterRestsByServices, this) );
			// $.subscribe('restsFilters',$.proxy(this._handleFiltersChange, this) );
			// $.subscribe('restsCity', $.proxy(this._handleFiltersChange, this) );
			// $.subscribe('restsStation', $.proxy(this._handleFiltersChange, this) );

		},

		_initPlugins: function () {
			// Дождёмся загрузки API и готовности DOM.
			if ($('#map').length) {
				ymaps.ready (
					$.proxy(this._getRestsInfo, this)
				);
			};
		},

		_getRestsInfo: function () {
			var self = this,
				rests = {},
				html = '';

			$.ajax({
				type: "GET",
				url: self.restsURL,
				datatype: "json",

				success: function (data) {
					rests = data.rests,
					html = data.html;

					self.rests = rests;
					self.restsHTML = html;

					var $activeLink = self.$navLinks.filter('._active'),
						isOne = false;

					// console.log(self.$navLinks, '1111', $activeLink);

					if ($activeLink.length) {
						isOne = true;
						var rid = $activeLink.data('rid');

						rests = {};
						rests[rid] = self.rests[rid];
					}

					$.proxy(self._initMap, self, rests, isOne)();
				},

				error: function () {
					alert("Что-то пошло не так");
				}
			});

		},

		_getObjectLength: function (obj) {
			var size = 0, key;
			for (key in obj) {
				if (obj.hasOwnProperty(key)) size++;
			}
			return size;
		},

		_handleNavSideLinkClick: function (e) {
			e.preventDefault();

			var $target = $(e.currentTarget),
				rid = $target.data('rid'),
				self = this;

			if ($target.hasClass('_active')) {
				return false;
			}

			this.$navLinks.removeClass('_active');
			$target.addClass('_active');

			$.proxy(self._showOneRest, self, rid)();
		},

		_handleFiltersChange: function (e, filters, checkedStat) {
			this.filters = filters;

			if ($.isEmptyObject(this.rests)) {
				return;
			};

			this.$navLinks.removeClass('_active');

			var filteredRests = this._filterRests(this.rests);

			this._clearMap();

			var numberOfRests = this._getObjectLength(filteredRests);

			if (numberOfRests === 1 && checkedStat) {
				var self = this;
				$.each(filteredRests, function (k,v) {
					self._showOneRest(k);
				});
			} else {
				this._renderMapMarkers(filteredRests, false);
			}
		},

		_filterRests: function (rests) {
			var filteredRests = {};

			filteredRests = this._filterRestsByServices(this.filters.services, rests);
			filteredRests = this._filterRestsByCity(this.filters.city, filteredRests);

			if (this.filters.station && this.filters.station !== 0) {
				filteredRests = this._filterRestsByStation(this.filters.station, filteredRests);
			}

			return filteredRests;
		},

		_filterRestsByServices: function (services, rests) {
			if (services === []) {
				// this._renderMapMarkers(this.rests);
				return rests;
			}

			var filteredRests = {};

			$.each(this.rests, function (k, v) {
				var isSuit  = true,
					checked = true;

				for (var i = 0; i < services.length; i++) {
					checked = (v.services.indexOf(services[i].value) !== -1);

					isSuit *= checked;
				};

				if (isSuit) {
					filteredRests[k] = v;
				}
			});

			// this._renderMapMarkers(filteredRests);
			return filteredRests;
		},

		_filterRestsByCity: function (cityID, rests) {
			var filteredRests = {},
				cityID = parseInt(cityID);

			$.each(rests, function (k, v) {
				if (v.city_id === cityID) {
					filteredRests[k] = v;
				}
			});

			return filteredRests;
		},

		_filterRestsByStation: function (stationID, rests) {
			var filteredRests = {},
				stationID = parseInt(stationID);

			$.each(rests, function (k, v) {
				if (v.subway_id === stationID) {
					filteredRests[k] = v;
				}
			});

			return filteredRests;
		},

		_filterMapMarkers: function (filteredRests) {
			this.element.removeClass('_one');
		},

		_renderMapMarkers: function (rests, isOne) {
			if (!isOne) {
				$.publish("restsInfo", [$(this.restsHTML), false] );
			}

			var self = this;

			$.each(rests, function(k, v){
				var placeMark = new ymaps.Placemark([v.lat, v.long], {
					hintContent: v.title
				}, self.placeMarkTemplate);

				placeMark.restId = k;

				placeMark.events.add("click", function (e) {
					var rid = e.get('target').restId,
						url = self.$navLinks.filter('[data-rid=' + rid +  ']').prop('href');

					window.location = url;

					// $.proxy(self._showOneRest, self, rid)();

				})

				self.mapCanvas.geoObjects.add(placeMark);
			});

			this.mapCanvas.setBounds( this.mapCanvas.geoObjects.getBounds() );
			this.mapCanvas.setZoom( this.mapCanvas.getZoom() - 0.2 );

			if (!isOne) {
				this.element.removeClass('_one');
			}
		},

		_initMap: function(rests, isOne) {

			if (!isOne) {
				var filteredRests = this._filterRests(rests);
			} else {
				var filteredRests = rests;
			}

			if (this.mapCanvas) {
				var zoom = this.mapCanvas.getZoom();
				this.mapCanvas.destroy();
			}

			var center =  [55.76, 37.64];
			var zoom = 10;

			// Создание экземпляра карты и его привязка к контейнеру с
			// заданным id ("map").
			this.mapCanvas = new ymaps.Map('map', {
				// При инициализации карты обязательно нужно указать
				// её центр и коэффициент масштабирования.
				center: center, // Москва
				zoom: zoom,
				controls: ['zoomControl', 'rulerControl', 'fullscreenControl', 'typeSelector']
				},
				{
					maxZoom: 18,
					autoFitToViewport: 'always',
					avoidFractionalZoom: false
				}
			);

			var self = this;

			this._renderMapMarkers(filteredRests, isOne);

			if (isOne) {
				this.mapCanvas.setZoom(12);
			}

		},

		_clearMap: function () {
			this.mapCanvas.geoObjects.removeAll();
		}, 

		_destroyMap: function () {
			this.mapCanvas.destroy();
		},

		_showOneRest: function (rid) {
			var self = this;

			clearTimeout(this.timer);

			this.timer = setTimeout(function() {
				self.$sideBar.addClass('_disabled');
				self.$pageContent.addClass('_disabled');

				$.each(self.$allRestsLink, function (k, v) {
					var $a = $('<a>', {
						'href': '/restaurants'
					}).html($(v).text());

					$(v).html($a);
				});


				$.ajax({
					url: self.restsOneURL,
					datatype: "json",
					data: {
						rid: rid
					},
					success: function (data) {
						self.$sideBar.removeClass('_disabled');
						self.$pageContent.removeClass('_disabled');

						var $html = $(data.html),
							services = data.rest.services;

						$.publish("restsInfo", [$html, true]);

						$.publish('restsCityChange', self.rests[rid]['city_id']);

						// $.publish("oneRestFilters", [services]);

						var center = [parseFloat(data.rest.lat), parseFloat(data.rest.long)];

						var placeMark = new ymaps.Placemark(center, {
							hintContent: data.rest.title
						}, self.placeMarkTemplate);

						placeMark.restId = data.rest.id || rid;

						self.$navLinks.filter("[data-rid=" +  rid + "]").addClass('_active');

						// this._initMap(data.rest, true);

						self._clearMap();
						self.mapCanvas.geoObjects.add(placeMark);
						self.mapCanvas.panTo(center);

						self.mapCanvas.setBounds( self.mapCanvas.geoObjects.getBounds() );
						self.mapCanvas.setZoom( self.mapCanvas.getZoom() - 0.2 );

						self.element.addClass('_one');
					}
				});

			}, 700);
		}

	});
})(jQuery);
;(function ($) {
	'use strict';

	$.widget('niyama.aboutSlider', {
		_create: function () {
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({

			});


			var self = this;

			window.addEventListener("orientationchange", function() {
				self.owl.reinit();
			}, false);
		},
		_initPlugins: function () {
			this.element.find('.about-slider__slider').owlCarousel({
				navigation : false, // Show next and prev buttons
					slideSpeed : 300,
					paginationSpeed : 400,
					autoHeight : true,
					singleItem:true
			});

			this.owl = this.element.find('.about-slider__slider').data('owlCarousel');
		}
	});
})(jQuery);
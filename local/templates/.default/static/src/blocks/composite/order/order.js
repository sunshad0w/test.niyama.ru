;(function ($) {
	'use strict';

	$.widget('niyama.order', {
		_create: function () {

			this.$addItems = this.element.find('.order__add-carousel');

			this.$goods = this.element.find('.goods._order');

			this.$droppable = this.element.find('.order__group').not('._coupons');

			this.$minSum = this.element.find( ".order-buy__min-sum" );

			this.$items = this.element.find( ".order__main" );

			this._initEvents();
			this._initPlugins();
		},

		_initEvents: function () {
			var self = this;

			this._on({
				'click .order__item-remove': this._handleRemoveClick,
				'change .select': this._handleChangeCount,
				'click ._disabled': this._handleDisableClick
			});


			this.$goods.draggable({
			    helper:'clone',
			    appendTo: 'body',
			    scroll: false,
			    
				drag: function(e,ui) {
					// ui.position.top -= $('body').scrollTop();
				},
				stop: function(e,ui) {
					// self.handleAddToCartDrug(this);
				}
		    });

	        this.$droppable.droppable({
	            hoverClass: 'dropHere',
	            drop: function(event, ui) {
	                var currentGoods = ui.draggable.get(0),
	                	$this = $(this);

	                self.handleAddToCartDrug(null, currentGoods, $this.data('tableid'), $this.data('guestid'), self);
	            }
	        });


	        this.$goods.find('.goods__add-btn').click(function () {
	        	self.handleAddToCartClick(this);
	        	return false;
	        });

		},

		_initPlugins: function () {
			this.$addItems.owlCarousel({
				singleItem: true,
				navigation: true,
				mouseDrag: false,
				touchDrag: false
			});
		},

		_handleDisableClick: function (e) {
			this.$minSum.effect( "shake", {
				times: 2
			} );
			return false;
		},

		_handleRemoveClick: function (e) {
			// $(e.currentTarget).parent().remove();
			// $(e.currentTarget).parent().slideUp('', function () {
			// 	$(this).remove();
			// });
			this.$items.addClass('__disabled');

			this._handleRemoveItem(e);
		},

		_handleChangeCount: function (e) {
			var self = this,
				id = $(e.target).data('id'),
				count = $(e.target).val();

			$.ajax({
				type: "GET",
				url: '/ajax/setCartQuantity.php',
				dataType: 'json',
				data: {
					ID: id,
					quantity: count,
					fromOrder: true
				},
				success: function (data) {
					window.ajaxData = data;

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);

					self._renderOrder();
				}
			});
		},

		_handleRemoveItem: function (e) {
			var self = this,
				id = $(e.target).data('id');


			$.ajax({
				type: "GET",
				url: '/ajax/removeFromCart.php',
				dataType: 'json',
				data: {
					ID: id,
					fromOrder: true
				},
				success: function (data) {
					window.ajaxData = data;

					$('.cart-buy__sum i').text(data.TOTAL_PRICE);
					$('#guests__card-sum').text(data.SUB_PRICE);

					self._renderOrder();
					self.$items.removeClass('__disabled');
				}
			});
		},

		handleAddToCartDrug: function (e, goods, tableid, guestid, self) {

			var pid = $(goods).find('.btn').data('id'),
				price = $(goods).find('.btn').data('price');

            $.ajax({
                type: "GET",
                url: '/ajax/add2cart.php',
                dataType: 'json',
                data: {
                	pid: pid,
                	quantity: 1,
                	fromOrderPromo: true,
                	tableid: tableid,
                	guestid: guestid
                },
                success: function (data) {
                	window.ajaxData = data;

                	$('.cart-buy__sum i').text(data.TOTAL_PRICE);
                	$('#guests__card-sum').text(data.SUB_PRICE);

                	self._renderOrder();
            	}
            });
		},

		handleAddToCartClick: function (goodsButton) {
			var self = this;

			var pid = $(goodsButton).data('id'),
				price = $(goodsButton).data('price'),
				el = $(goodsButton).parent().parent().parent(),
					// clone.find('.goods__add-btn').remove();
					// clone.find('.goods__num').remove();
					// clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
				clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
				clone2 = el.clone().css({position:'absolute',top:el.offset().top - 10, left:el.offset().left - 4, width: el.width(), 'z-index': 100}).appendTo('body');
				clone2.animate({top:'300px', opacity:0},
					1000,
					function(){
						clone2.remove();
						var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
						$.publish('addGoods', cloneHTML);
					});

            $.ajax({
                type: "GET",
                url: '/ajax/add2cart.php',
                dataType: 'json',
                data: {
                	pid: pid,
                	quantity: 1,
                	fromOrderPromo: true
                },
                success: function (data) {
                	window.ajaxData = data;

                	$('.cart-buy__sum i').text(data.TOTAL_PRICE);
                	$('#guests__card-sum').text(data.SUB_PRICE);

                	self._renderOrder();
            	}
            });

            return false

		},

		_renderOrder: function () {
			var self = this;

			$.ajax({
				type: "POST",
				url: "/order/",
				dataType: "json",
				data: {
					step: "cart"
				},

				success: function (data) {
					var html = $(data.html);
					self.element.replaceWith(html);
					$.initWidgets();
				}
			});
		}

	});
})(jQuery);
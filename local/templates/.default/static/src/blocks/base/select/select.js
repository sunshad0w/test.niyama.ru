;(function ($) {
	'use strict';

	$.widget('niyama.select', {
		_create: function () {

			this.$select = this.element.find('select');
			
			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({
				'change': this._handleSelectChange
			});
		},
		_initPlugins: function () {
			this.$select.chosen({
				inherit_select_classes: true,
				disable_search: true,
				width: '100%'
			});
		},
		_handleSelectChange: function () {
			this.$select.trigger("chosen:updated");

			if (this.$select.hasClass('_city')) {
				var $selOption = this.$select.find('option:selected');

				this.$select.closest('.ordering-form__body').find('.input').val('');
            	$('select._addresses').val('0');
            	$('._addresses').trigger("chosen:updated");

				if ( $selOption.data('city') === "REGION_1" ) {
					$('.input._metro').parent().removeClass("_hidden");
					$('.select._metro').parent().removeClass("_hidden");
					$('.input._other').parent().addClass("_hidden");
					$('.ordering-form__row._mkad').slideDown();
					//$('.ordering-form__row._other-txt').hide();
				} else if ( $selOption.data('city') === "REGION_OTHER" ) {
					$('.input._metro').parent().addClass("_hidden");
					$('.select._metro').parent().addClass("_hidden");
					$('.input._other').parent().removeClass("_hidden");
					$('.ordering-form__row._mkad').slideUp().find('input').prop('checked', false);
					//$('.ordering-form__row._other-txt').show();
				} else {
					$('.input._metro').parent().addClass("_hidden");
					$('.select._metro').parent().addClass("_hidden");
					$('.input._other').parent().addClass("_hidden");
					$('.ordering-form__row._mkad').slideUp().find('input').prop('checked', false);
					//$('.ordering-form__row._other-txt').hide();
				}

				$('.ordering-form__row._rests').hide()
												.find('input')
												.prop('disabled', true);
				
				$('.ordering-form__row._rests').filter( '#rest_' + this.$select.val())
												.slideDown('', function () {
													$(this).css({
														'overflow': 'visible'
													});
												}).find('input')
												.prop('disabled', false);
			}

			if (this.$select.hasClass('_addresses')) {

				this.$select.closest('.ordering-form__body').find('.input').val('').addClass('ui-autocomplete-loading');


	            var address_id = this.$select.val();

	            $.ajax({
	                type: "GET",
	                url: '/ajax/get_user_adress.php',
	                data: {
	                	address_id: address_id
	                },
	                dataType: 'json',
	                success: function (data) {

		            	if (data.data.city == 1) {
		            		$('.input._metro').parent().removeClass("_hidden");
		            	}
			            	else {
			            		$('.input._metro').parent().addClass("_hidden");
			            	};

		            	$('select._city').val(data.data.city);
		            	$('._city').trigger("chosen:updated");
		            	$('._metro').val(data.data.metro);
		            	if (data.data.city_dop) {
			            	$('._other').removeClass("_hidden").val(data.data.city_dop);
		            	}
		            	else {
			            	$('._other').parent().addClass("_hidden");
		            	}

		            	if (data.data.mkad) {
		            		$('.ordering-form__row._mkad').slideDown().find('input').prop('checked', true);
		            	}
			            	else if (data.data.city == 1) {
			            		$('.ordering-form__row._mkad').slideDown().find('input').prop('checked', false);
			            	}
				            	else {
				            		$('.ordering-form__row._mkad').slideUp().find('input').prop('checked', false);
				            	};
		            	
		            	$('._address').val(data.data.street);
		            	$('._house').val(data.data.house);
		            	$('._apart').val(data.data.apart);
		            	$('._porch').val(data.data.porch);
		            	$('._intercom').val(data.data.intercom);
		            	$('._floor').val(data.data.floor);

						$('.ordering-form__body.block2').find('.input').removeClass('ui-autocomplete-loading');

	            	}
	            });
			}

		}
	});
})(jQuery);
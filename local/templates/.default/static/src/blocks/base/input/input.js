;(function ($) {
	'use strict';

	$.widget('niyama.inputDate', {
		_create: function () {

			this._initEvents();
			this._initPlugins();
		},
		_initEvents: function () {
			this._on({

			});
		},
		_initPlugins: function () {
			if ( this.element.data('mindate') ) {
				var mindate = 0;
			} else {
				var mindate = null;
			}

			if ( this.element.data('maxdate') ) {
				var maxdate = 0;
			} else {
				var maxdate = null;
			}

			this.element.datepicker({
				showOn: "button",
				buttonImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAlCAMAAAAUaRt1AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAQlBMVEX///+/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpm/rpn///9QOFhRAAAAFHRSTlMAACRx3dslrfEn7zzzcoX59Dso8jqVyNIAAAABYktHRACIBR1IAAAAmklEQVQ4y+2T3Q7CIAxG26l0bIi6fe//rA5kAcaPN5p44bko+coJaUhK5OHhNDDTziG6zhmXVMij6wBIhTz2BX7hO5EYiZWMaDCKYtITOkyaBF2Etvfn1gwzYCjc1Ifcjl3wPxMJMRHUVaVCiIlQ56OCNbYomXCDKUr+wt0W5T/kjw75TSEsTpWHW5y3q7f0l3ch1rK2rlfR/AS2VSV3d0p5WAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNC0wNi0wM1QwODoxNjoyOCswMDowMHWVRt4AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDYtMDNUMDg6MTY6MjgrMDA6MDAEyP5iAAAAAElFTkSuQmCC",
				buttonImageOnly: true,
				numberOfMonths: 2,
				maxDate: maxdate,
				minDate: mindate
			});

			/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
			/* Written by Andrew Stromnov (stromnov@gmail.com). */
			(function( factory ) {
				if ( typeof define === "function" && define.amd ) {

					// AMD. Register as an anonymous module.
					define([ "../datepicker" ], factory );
				} else {

					// Browser globals
					factory( jQuery.datepicker );
				}
			}(function( datepicker ) {

			datepicker.regional['ru'] = {
				closeText: 'Закрыть',
				prevText: '&#x3C;Пред',
				nextText: 'След&#x3E;',
				currentText: 'Сегодня',
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
				'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
				'Июл','Авг','Сен','Окт','Ноя','Дек'],
				dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
				dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
				dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
				weekHeader: 'Нед',
				dateFormat: 'dd.mm.yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''
			};

			datepicker.setDefaults(datepicker.regional['ru']);

			return datepicker.regional['ru'];

			}));


		}
	});
})(jQuery);
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//IncludeTemplateLangFile(__FILE__);

// !!! включим перенос всех js-скриптов в футер
if(!defined('JS_MOVE_FOOTER_FLAG')) {
	define('JS_MOVE_FOOTER_FLAG', true);
}
define('SITE_DEFAULT_TEMPLATE_PATH', '/local/templates/.default');


?><!DOCTYPE html>
<!--[if IE 8]><html lang="ru" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="ru" class="ie9 _light"><![endif]-->
<!--[if gt IE 9]><!--><html lang="ru" class="_light"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width">

		<title><?
			$GLOBALS['APPLICATION']->ShowTitle();
		?></title><?

		// важен такой порядок
		$GLOBALS['APPLICATION']->ShowMeta('robots');
		$GLOBALS['APPLICATION']->ShowMeta('keywords');
		$GLOBALS['APPLICATION']->ShowMeta('description');
		$GLOBALS['APPLICATION']->ShowCSS();
		$GLOBALS['APPLICATION']->ShowHeadScripts();
		$GLOBALS['APPLICATION']->ShowHeadStrings();

		/* css */
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="stylesheet" href="'.CUtil::GetAdditionalFileURL(SITE_DEFAULT_TEMPLATE_PATH.'/static/dest/css/main.css').'">');

		/* js */
		$GLOBALS['APPLICATION']->AddHeadScript(SITE_DEFAULT_TEMPLATE_PATH.'/static/dest/js/main.js');
		$GLOBALS['APPLICATION']->AddHeadString('<!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->');

		/* vk widget */
		$GLOBALS['APPLICATION']->AddHeadString('<script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>');

		// Favicon
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="icon" href="/favicon.ico" type="image/x-icon" />');
		$GLOBALS['APPLICATION']->AddHeadString('<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />');?>

<!-- Convead Widget -->
		<script>
			window.ConveadSettings = {
				<?php if($GLOBALS['USER']->IsAuthorized()) {
				?>
				 visitor_uid: '<?=trim($GLOBALS['USER']->GetID());?>',
				 visitor_info: {
				 first_name: '<?=trim($GLOBALS['USER']->GetFirstName());?>',
				 last_name: '<?=trim($GLOBALS['USER']->GetLastName());?>',
				 email: '<?=trim($GLOBALS['USER']->GetEmail());?>',
				 },
					<?php } ?>
				app_key: "8573c0e8c97f3038c6b82517e6fbc504"
				/* For more information on widget configuration please see:
				 http://convead.ru/help/kak-nastroit-sobytiya-vizitov-prosmotrov-tovarov-napolneniya-korzin-i-pokupok-dlya-vashego-sayta
				 */
			};
			(function(w,d,c){w[c]=w[c]||function(){(w[c].q=w[c].q||[]).push(arguments)};var ts = (+new Date()/86400000|0)*86400;var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.src = '//tracker.convead.io/widgets/'+ts+'/widget-8573c0e8c97f3038c6b82517e6fbc504.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})(window,document,'convead');
		</script>
		<!-- /Convead Widget -->

	</head><?

	?><body class="page _light"><?
		?><div id="panel"><?$GLOBALS['APPLICATION']->ShowPanel()?></div><?

		?><div class="page__wrap <?$GLOBALS['APPLICATION']->ShowProperty('NIYAMA_PAGE_WRAP_ELEMENT_CLASS', '_light')?>"><?
			// шапка
			?><header class="page__header">
				<div class="page__container clearfix"><?
					// логотип
					?><div class="pull-left"><?
						if( ($GLOBALS['APPLICATION']->GetCurPage(true) == SITE_DIR.'index.php') /*|| substr_count($GLOBALS['APPLICATION']->GetCurPage(false),'/order')*/ ) {
							$GLOBALS['APPLICATION']->IncludeFile(
								SITE_DIR.'include_areas/logo-home.php',
								array(),
								array(
									'MODE' => 'html',
									'SHOW_BORDER' => true
								)
							);
						} else {
							$GLOBALS['APPLICATION']->IncludeFile(
								SITE_DIR.'include_areas/logo-inner.php',
								array(),
								array(
									'MODE' => 'html',
									'SHOW_BORDER' => true
								)
							);
						}
					?></div><?

					// телефон вверху
					?><div class="pull-right"><?
						$GLOBALS['APPLICATION']->IncludeFile(
							SITE_DIR.'include_areas/tel-top-light.php',
							array(),
							array(
								'MODE' => 'html',
								'SHOW_BORDER' => true
							)
						);
					?></div><?
				?></div>
			</header><?
			?><main class="page__main _light">
				<div class="page__container clearfix">
					<div class="page__content-wrap"><?

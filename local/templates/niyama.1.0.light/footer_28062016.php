<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
//IncludeTemplateLangFile(__FILE__);
					?></div>
				</div>
			</main><?
		?></div>
		<footer class="page__footer _light">
			<div class="page__container clearfix">
				<div class="page__footer-left">
					<a href="http://www.adv.ru" class="adv-logo">Сделано в ADV</a>
					<div class="copyright">&copy; <?=COption::GetOptionString('main', 'site_name')?>, <?=date('Y')?></div>
				</div>
			</div>
		</footer><?

		// js веб-аналитики (GA + YA, в т.ч. добавляется код для <head>)
		$GLOBALS['APPLICATION']->IncludeFile(
			SITE_DIR.'include_areas/analytics-js.php',
			array(),
			array(
				'MODE' => 'html',
				'SHOW_BORDER' => false
			)
		);
	?></body><?
?></html><?

// костыль: сохраняем адрес страницы (для кнопки назад в оформлении заказа)
$_SESSION['NIYAMA']['LAST_PAGE_URL'] = $GLOBALS['APPLICATION']->GetCurPage(false);

//
// необходимо на всех страницах, где присутствует постраничная навигация, после основного title страницы добавлять слова по следующему принципу: страница [номер текущей страницы]
//
$iPagenavPage = $GLOBALS['APPLICATION']->GetProperty('NIYAMA_PAGENAV_CUR_PAGE', 0);
if($iPagenavPage > 1) {
	$sCurTitle = trim($GLOBALS['APPLICATION']->GetTitle());
	$GLOBALS['APPLICATION']->SetTitle(trim($sCurTitle, '-').' - страница '.$iPagenavPage);
}
ob_start();
?><script type="text/javascript">
	var PagenavChangeTitle = function(iCurPageNum) {
		var sCurTitle = document.title;
		sCurTitle = sCurTitle.replace(/ - страница \d+/, '');
		if(iCurPageNum > 1) {
			sCurTitle += ' - страница ' + iCurPageNum;
		}
		document.title = sCurTitle;
	}
</script><?
$GLOBALS['APPLICATION']->AddHeadString(ob_get_clean());

if(defined('ERROR_404')) {
	CHTTP::SetStatus('404 Not Found');
}

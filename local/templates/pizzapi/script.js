BX.ready(function () {
    var oLoginPopup = new BX.PopupWindow('Login', window.body, {
        autoHide: true,
        offsetTop: 1,
        offsetLeft: 0,
        lightShadow: true,
        closeIcon: true,
        closeByEsc: true,
        overlay: {
            backgroundColor: 'black', opacity: '80'
        },
        events: {
            onPopupShow: function () {
                $("#loginFormPopup").submit(function (event) {
                    event.preventDefault();
                    //grab all form data
                    var formData = new FormData($(this)[0]);
                    formData.append('Login', '1');
                    $.ajax({
                        type: "POST",
                        url: '/local/ajax/loginPopup.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        headers: {
                            "Upgrade-Insecure-Requests": 1
                        },
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data['errors']) {
                                $('#loginPopupErrors').html(data['errors']['MESSAGE']);
                            }
                            if (data['result']) {
                                location.reload();
                            }
                        }
                    });
                });
                $('.registerBtn').click(function (e) {
                    if (!e) {
                        e = window.event;
                    }
                    BX.PreventDefault(e);
                    oLoginPopup.close();
                    showRegPopup();
                });
                $('.forgotPassBtn').click(function (e) {
                    if (!e) {
                        e = window.event;
                    }
                    BX.PreventDefault(e);
                    oLoginPopup.close();
                    showForgotPassPopup();
                });
            }
        }
    });
    var oRegPopup = new BX.PopupWindow('Register', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'black', opacity: '80'
            },
            events: {
                onPopupShow: function () {
                    $('.loginBtn').click(function (e) {
                        if (!e) {
                            e = window.event;
                        }
                        BX.PreventDefault(e);
                        oRegPopup.close();
                        showLoginPopup();
                    });
                }
            }
        }),
        showLoginPopup = function () {
            $.get('/local/ajax/loginPopup.php', {'dataType': 'html'},
                function (response) {
                    if (response) {
                        try {
                            var json = JSON.parse(response);
                            if (json.result == 1) {
                                document.location.href = "/";
                            }
                        } catch (e) {
                            oLoginPopup.setContent(response);
                            oLoginPopup.show();
                        }
                    } else {
                        console.log('ошибка')
                    }
                });
        },
        showRegPopup = function () {
            $.get('/local/ajax/registerPopup.php', {'dataType': 'html'},
                function (response) {
                    if (response) {
                        oRegPopup.setContent(response);
                        oRegPopup.show();
                    } else {
                        console.log('ошибка')
                    }
                });
        };
    showForgotPassPopup = function () {
        $.get('/auth/?forgot_password=yes', {'dataType': 'html'},
            function (response) {
                if (response) {
                    var content = $(response).map(function () {
                        if (this.className == 'main-content') return this;
                    });
                    oRegPopup.setContent(content.html());
                    oRegPopup.show();
                } else {
                    console.log('ошибка')
                }
            });
    };
    $('#loginBtn').click(function (e) {
        if (!e) {
            e = window.event;
        }
        BX.PreventDefault(e);
        var clearLocationHref = window.location.href.replace(window.location.origin, "").replace(/\?.*/, '').replace(/\//g, '');
        if (clearLocationHref == 'auth') {
            window.location.href = window.location.href;
            return false;
        }
        showLoginPopup();
    });

    $('body').on('submit', '.ajax-reg-form', function (event) {
        event.preventDefault();
        //grab all form data  
        var $form = $(this),
            formData = new FormData($form[0]);
        formData.append('register_submit_button', '1');
        formData.append("REGISTER[LOGIN]", formData.get("REGISTER[EMAIL]"));
        $.ajax({
            type: "POST",
            url: '/local/ajax/register.php',
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                "Upgrade-Insecure-Requests": 1
            },
            success: function (data) {
                data = JSON.parse(data);
                console.log(data);
                if (data['errors']) {
                    var $errors = $form.find('.authform_errors');
                    $errors.html('');
                    for (i in data['errors']) {
                        $errors.append($('<p>').html(data['errors'][i]));
                    }
                }
                if (data['result']) {
                    // location.reload();
                    if (data['confirm']) {

                        oRegPopup.close();
                        Informer.success(data['confirm']);
                    }
                }
            }
        });
    });
    $('header').Stickyfill();

    if ($('.catalog__menu').length > 0) {
        var stickyOffset = $('.catalog__menu').offset().top;
        var stickyHeight = $('.catalog__menu').height();
        var navigationHeight = $('header').height();

        // костыль: ждем когда fotorama заинитится
        setTimeout(function () {
            stickyOffset = $('.catalog__menu').offset().top;
        }, 500);


        var stickCatalogMenu = function () {
            var stickyOffsetRecalc = stickyOffset;
            var sticky = $('.catalog__menu'),
                scroll = $(window).scrollTop();

            
            if (scroll + navigationHeight > stickyOffsetRecalc) {
                if (!$('.catalog__menu--wrapper').length) {
                    sticky.wrap("<div class='catalog__menu--wrapper' style='position:relative;  height:" + stickyHeight + "px;'></div>");
                }
                sticky.addClass('fixed');
                sticky.css('top', navigationHeight);
            } else {
                sticky.css('top', '');
                sticky.removeClass('fixed');
                if ($('.catalog__menu--wrapper').length) {
                    sticky.unwrap('.catalog__menu--wrapper');
                }
            }
        };

        $(window).scroll(function () {
            stickCatalogMenu();
        });

        $(window).resize(function () {
            // костыль: ждем когда fotorama заинитится
            setTimeout(function () {
                stickyOffset = $('.catalog__menu').offset().top;
            }, 500);
            stickCatalogMenu();
        })
    }

    $('form.footer__form').submit(function (e) {
        e.preventDefault();
        $.getJSON('/local/ajax/subscribe.php',
            $(this).serialize()
            , function (data) {
                if (data.status == 'ok') {
                    Informer.text("Спасибо, что подписались");
                } else {
                    Informer.error(data.msg);
                }
                $('form.footer__form')[0].reset();
            });
    });

});

var MIN_PRICE_ORDER =  781;

$(document).on('click', '.navigation__item--cart', function (e) {
    var price = $('#summary_price').data('price');
    var minPriceOrder = window.minPriceOrderInCart ? window.minPriceOrderInCart : MIN_PRICE_ORDER;
    if (price < minPriceOrder) {
        e.preventDefault();
        $('.catalog__cart__makeorder__button').addClass('.catalog__cart__makeorder--disabled');
        Informer.error('Минимальная сумма заказа ' + minPriceOrder + ' р.');
    }
});

$(document).on('click', '.toggle_requisits', function () {

    $('.requisits').toggle(200, '', function () {
        if ($('.requisits').is(':visible')) {
            $('.toggle_requisits').text('скрыть');
        } else {
            $('.toggle_requisits').text('показать');
        }
    });
});


$(document).on('click', '.catalog__cart__makeorder__button', function (e) {
    var minPriceOrder = window.minPriceOrderInCart ? window.minPriceOrderInCart : MIN_PRICE_ORDER;
    var price = $('#summary_price').data('price');
        if (price < minPriceOrder) {
        e.preventDefault();
        $('.catalog__cart__makeorder__button').addClass('.catalog__cart__makeorder--disabled');
        Informer.error('Минимальная сумма заказа ' + minPriceOrder + ' р.');
    }

});


var Informer = (function () {
    "use strict";

    var elem,
        hideHandler,
        that = {};

    that.init = function (options) {
        elem = $(options.selector);
    };

    var show = function (text) {
        clearTimeout(hideHandler);

        elem.find("span").html(text);
        elem.delay(200).fadeIn().delay(4000).fadeOut();
    };

    that.text = function (text) {
        elem.css('border', '1px solid #BCE8F5').css('color', 'white');
        show(text);
    };

    that.success = function (text) {
        elem.css('color', '#00FF77').css('border', '1px solid #BCF5E8');
        show(text);
    };

    that.error = function (text) {
        elem.css('border', '1px solid #F5BBBB').css('color', '#FF5555');
        show(text);
    };

    return that;
}());

$(document).ready(function () {
    Informer.init({
        "selector": ".alert-informer"
    });

});


var subscribe_me = function () {
    if ($('#email_subscribe_checkbox').prop('checked')) {
        $.ajax({
            type: "POST",
            url: '/local/ajax/subscribe.php',
            data: {email_subscribe: $('input[name="REGISTER[EMAIL]"]').val()}
        }).done(function () {
        });
    }
};
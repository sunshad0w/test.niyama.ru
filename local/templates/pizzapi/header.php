<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<!--[if IE 8]>
<html lang="ru" class="ie8 lt-ie10"><![endif]-->
<!--[if IE 9]>
<html lang="ru" class="ie9 lt-ie10"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="ru"><!--<![endif]-->

<html prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= LANG_CHARSET ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width">
    <meta name="viewport"
          content="target-densitydpi=device-dpi,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">

    <meta name="msapplication-square70x70logo" content="<?= SITE_TEMPLATE_PATH ?>/img/icons/windows-tile-70x70.png">
    <meta name="msapplication-square150x150logo" content="<?= SITE_TEMPLATE_PATH ?>/img/icons/windows-tile-150x150.png">
    <meta name="msapplication-square310x310logo" content="<?= SITE_TEMPLATE_PATH ?>/img/icons/windows-tile-310x310.png">
    <meta name="msapplication-TileImage" content="<?= SITE_TEMPLATE_PATH ?>/img/icons/windows-tile-144x144.png">
    <meta name="msapplication-TileColor" content="#65402F">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-152x152-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-76x76-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-60x60-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/apple-touch-icon.png">
    <link rel="icon" sizes="228x228" href="<?= SITE_TEMPLATE_PATH ?>/img/icons/coast-icon-228x228.png">

    
    <title><?$GLOBALS['APPLICATION']->ShowTitle();?></title>
    <link rel="stylesheet" href="/local/templates/pizzapi/plugins/bootstrap-4.0.0-alpha.6/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://use.fontawesome.com/d80851ca7b.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>


    <link rel="stylesheet" href="/local/templates/pizzapi/plugins/fancybox-master/dist/jquery.fancybox.css">
    <link rel="stylesheet" href="/local/templates/pizzapi/plugins/parsley.js-2.7.0/parsley.css">
    <link rel="stylesheet" href="/local/templates/pizzapi/plugins/fotorama/fotorama.css">
    <!--<link rel="stylesheet" href="/local/templates/pizzapi/plugins/slickslider/slick.css">
    <link rel="stylesheet" href="/local/templates/pizzapi/plugins/slickslider/slick-theme.css">-->

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">



    <?
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/bootstrap-4.0.0-alpha.6/tether.min.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/bootstrap-4.0.0-alpha.6/bootstrap.min.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/js/jquery.inputmask.bundle.min.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/js/textinput-filter.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/js/phone.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/js/phone-ru.js");
    //$APPLICATION->AddHeadScript("/local/templates/pizzapi/js/jquery.maskedinput.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/fancybox-master/dist/jquery.fancybox.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/js/stickyfill.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/parsley.js-2.7.0/parsley.min.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/parsley.js-2.7.0/ru.extra.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/parsley.js-2.7.0/ru.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/flexibility-master/flexibility.js");
    //$APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/bxslider/jquery.bxslider.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/fotorama/fotorama.js");
    //$APPLICATION->AddHeadScript("/local/templates/pizzapi/plugins/slickslider/slick.js");
    $APPLICATION->AddHeadScript("/local/templates/pizzapi/script.js");

    //$APPLICATION->SetAdditionalCSS("/local/templates/pizzapi/plugins/bxslider/jquery.bxslider.css");
    $APPLICATION->SetAdditionalCSS("/local/templates/pizzapi/plugins/fotorama/fotorama.css");

    $GLOBALS['APPLICATION']->AddHeadString('<link rel="icon" href="/favicon.ico" type="image/x-icon" />');
    $GLOBALS['APPLICATION']->AddHeadString('<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />');

    $APPLICATION->IncludeComponent("dev:component.empty", "open-graph", Array( ), false);
    CJSCore::Init();
    $GLOBALS['APPLICATION']->ShowHead();
    $APPLICATION->ShowPanel();
    ?>

    <?

    $uAgent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match("#Version\/8\.0.*[S,s]afari#", $uAgent)){
        $classSafariOld = 'safari__old';
    };
    ?>

</head>
<body class="<?=$classSafariOld?>">
<header class="sticky header__convert">
    <div class="header">
        <div class="container container__header">
            <div class="header-wrapper">
                <input class="header__hamburger-input" type="checkbox" id="hamburger-input">
                <label class="header__hamburger-label" for="hamburger-input"><i class="fa fa-list fa-3x" aria-hidden="true"></i></label>
                <div class="logo_media"><a href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo_small.png"></a></div>
                <div class="logo"><a href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo_small.png"></a></div>


                <div class="navigation">

                    <? $APPLICATION->IncludeComponent("bitrix:menu", "pizzamenu", Array(
                        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                        "DELAY" => "N",    // Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",    // Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    ); ?>
                    <? global $USER; ?>


                    <? if ($USER->IsAuthorized() && !CUsersData::isGuest()): ?>

                        <div class="navigation__item <? echo ($APPLICATION->GetCurPage(false) === '/personal/') ? 'navigation__item--active' : '' ?>">
                            <a href="/personal/">ЛК</a></div>
                        <div class="navigation__item"><a href="/?logout=yes"><i class="fa fa-sign-out"></i></a></div>
                    <? else: ?>
                        <div class="navigation__item"><a href="/auth/" id="loginBtn">ВХОД</a></div>
                    <? endif; ?>

                    <div class="navigation__item--cart--wrapper">
                        <a class="navigation__item--cart" href="/personal/cart/"><img
                                    src="<?= SITE_TEMPLATE_PATH ?>/img/cartw.png"/>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket.line",
                                "pizzabasketheader",
                                Array(
                                    "HIDE_ON_BASKET_PAGES" => "Y",
                                    "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                    "POSITION_FIXED" => "N",
                                    "SHOW_IMAGE" => "Y",
                                    "SHOW_NOTAVAIL" => "N",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PRICE" => "Y",
                                    "SHOW_PRODUCTS" => "Y",
                                    "SHOW_SUBSCRIBE" => "Y",
                                    "SHOW_SUMMARY" => "Y",
                                    "SHOW_TOTAL_PRICE" => "Y",
                                )
                            ); ?>

                        </a>


                    </div>

                    <div class="phone">+7 495 781-781-9 <br> +7 495 134-131-3</div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mobile-substrate"></div>
<div class="alert-informer" style="display: none;">
    <span></span>
</div>

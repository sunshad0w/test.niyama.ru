<footer>
    <div class="container">
        <div class="subscribe">
            <div class="subscribe__title">Подпишитесь на новости и акции</div>
            <form action="" class="footer__form">
                <div class="footer__form-wrapper">
                    <div>
                        <input type="text" placeholder="E-mail" class="footer__input" name="email_subscribe" required></div>
                    <div>
                        <button type="submit" class="footer__button" >Отправить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="socialblock">
            <a class="btn btn-block btn-social btn-facebook" href="https://www.facebook.com/pizzapi.ru" target="_blank">
                <span class="fa fa-facebook"></span>
            </a>
            <a class="btn btn-block btn-social btn-vk" href="http://vk.com/pizzapi" target="_blank">
                <span class="fa fa-vk"></span>
            </a>
            <a class="btn btn-block btn-social btn-instagram" href="http://instagram.com/pizzapi_ru" target="_blank">
                <span class="fa fa-instagram"></span>
            </a>
            <a class="btn btn-block btn-social btn-twitter" href="https://twitter.com/Pizzapi_ru" target="_blank">
                <span class="fa fa-twitter"></span>
            </a>
        </div>
        <div class="navigation navigation__footer">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "pizzamenu", Array(
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            ); ?>


            <?
                if(CUsersData::isGuest()) {
                    $USER->Logout();
                }
            ?>

            <?if ($USER->IsAuthorized()):?>

                <div class="navigation__item <?echo ($APPLICATION->GetCurPage(false) === '/personal/')?'navigation__item--active':''?>"><a href ="/personal/">ЛК</a></div>
                <div class="navigation__item"><a href ="/?logout=yes"><i class="fa fa-sign-out"></i></a></div>
            <?else:?>
                <div class="navigation__item"><a href ="/auth/" id="loginBtn">ВХОД</a></div>
            <?endif;?>
            <div class="phone">+7 495 781-781-9</div>
        </div>
    </div>

     <? $APPLICATION->IncludeComponent("dev:pizza.partners", "partners", Array(
        'ALL_DATA' => $GLOBALS['PARTNERS_DATA'],
    ),
        false
    ); ?>
    <script type="application/javascript">
        function addFooterEmailMask() {
            var c_b_obj = {
                "onincomplete" : function () {
                    $(this).closest('.footer__form').find('button[type=submit]').prop('disabled', true);
                },
                "oncomplete"   : function () {
                    $(this).closest('.footer__form').find('button[type=submit]').prop('disabled', false);
                }
            };
            $('input.footer__input').inputmask('email', c_b_obj);
        }

        //addFooterEmailMask();
    </script>
</footer>

<!-- Google Analytics script-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-46047978-2', 'auto');
    ga('send', 'pageview');
</script>
<!-- /Google Analytics script-->
<!-- Calltouch script-->
<script type="text/javascript">
    function ct(w,d,e,c){
        var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
        var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
        s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
        if(!w.jQuery){var jq=d.createElement(e);
            jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
            jq.onload=function(){
                p.parentNode.insertBefore(s,p);};
            p.parentNode.insertBefore(jq,p);}else{
            p.parentNode.insertBefore(s,p);}}
    if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){
        if (!!window[window.GoogleAnalyticsObject].getAll()[0])
        {ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
        else{ct(window,document,'script', null);}});
    }else{ct(window,document,'script', null);}
</script>
<!-- /Calltouch script -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter25059578 = new Ya.Metrika({
                    id: 25059578,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/25059578" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript">
    (function (w, d) {
        try {
            var el = 'getElementsByTagName', rs = 'readyState';
            if (d[rs] !== 'interactive' && d[rs] !== 'complete') {
                var c = arguments.callee;
                return setTimeout(function () { c(w, d) }, 100);
            }
            var s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = s.defer = true;
            s.src = '//aprtx.com/code/PizzaPi/';
            var p = d[el]('body')[0] || d[el]('head')[0];
            if (p) p.appendChild(s);
        } catch (x) { if (w.console) w.console.log(x); }
    })(window, document);
</script>

<!-- Convead Widget -->
<script>
    window.ConveadSettings = {
        <?php if($GLOBALS['USER']->IsAuthorized()) {
        ?>
        visitor_uid: '<?=trim($GLOBALS['USER']->GetID());?>',
        visitor_info: {
            first_name: '<?=trim($GLOBALS['USER']->GetFirstName());?>',
            last_name: '<?=trim($GLOBALS['USER']->GetLastName());?>',
            email: '<?=trim($GLOBALS['USER']->GetEmail());?>',
        },
        <?php } ?>
        app_key: "8573c0e8c97f3038c6b82517e6fbc504"
        /* For more information on widget configuration please see:
         http://convead.ru/help/kak-nastroit-sobytiya-vizitov-prosmotrov-tovarov-napolneniya-korzin-i-pokupok-dlya-vashego-sayta
         */
    };
    (function(w,d,c){w[c]=w[c]||function(){(w[c].q=w[c].q||[]).push(arguments)};var ts = (+new Date()/86400000|0)*86400;var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.src = '//tracker.convead.io/widgets/'+ts+'/widget-8573c0e8c97f3038c6b82517e6fbc504.js';var x = d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);})(window,document,'convead');
</script>
<!-- /Convead Widget -->
<script>
    var moscowTime = new Date(Date.now() + (new Date()).getTimezoneOffset()*60*1000 + 3*60*60*1000);
    if (window.location.pathname == '/' && moscowTime.getHours() >= 6 && moscowTime.getHours() < 9){
        convead('widget', 'show', {id: 6571});
        //console.log('pixel');
    }
</script>
<script>
    window.minPriceOrderInCart = <?= intval(CNiyamaCustomSettings::GetStringValue('min_sum_by_order'));?>;
</script>
</body>
</html>
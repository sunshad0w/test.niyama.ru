/**
 * Created by user on 2017-06-20.
 */
;(function() {


    function FilterTextInput() {

        // Вспомогательная функция. Возвращает символ по его  коду
        getChar = function(event) {
            if (event.which == null) {
                if (event.keyCode < 32) return null;
                return String.fromCharCode(event.keyCode) // IE
            }

            if (event.which != 0 && event.charCode != 0) {
                if (event.which < 32) return null;
                return String.fromCharCode(event.which) // остальные
            }

            return null; // специальная клавиша
        }

        // обработчик для входных символов события keypress. Позволяет вводить только буквы
        this.inputOnlyLetters = function(e) {
            e = e || event;

            if (e.ctrlKey || e.altKey || e.metaKey) return;

            var chr = getChar(e);

            // с null надо осторожно в неравенствах,
            // т.к. например null >= '0' => true
            // на всякий случай лучше вынести проверку chr == null отдельно
            if (chr == null) return;

            if ( (('A' <= chr) && (chr <= 'z')) || (('А' <= chr) && (chr <= 'я')) || chr == ' ' || chr == 'ё' || chr == 'Ё') {
                return;
            } else {
                return false;
            }
        }

        // обработчик для входных символов события keypress. Позволяет вводить только цифры
        this.inputOnlyNumbers = function(e) {
            e = e || event;

            if (e.ctrlKey || e.altKey || e.metaKey) return;

            var chr = getChar(e);

            // с null надо осторожно в неравенствах,
            // т.к. например null >= '0' => true
            // на всякий случай лучше вынести проверку chr == null отдельно
            if (chr == null) return;

            if (chr < '0' || chr > '9') {
                return false;
            }
        }

        // защита от некорректных данных при вставке (через paste).
        this.protectFromWrongPaste = function(value, type){
            var result = value;
            switch (type) {
                // только символы
                case 'onlyLetters' : {
                    if (/[^a-zA-Zа-яА-ЯёЁ ]/i.test(value)) {
                        result = value.replace(/[^a-zA-Zа-яА-ЯёЁ ]/g, "");
                    }
                    break;
                }
                // только цифры
                case 'onlyNumbers' : {
                    if (/[^0-9]/i.test(value)) {
                        result = value.replace(/[^0-9]/g, "");
                    }
                    break;
                }
            }
            return result;
        }
    }

    window.FilterTextInput = new FilterTextInput();

}());
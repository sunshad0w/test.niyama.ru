/**
 * Created by user on 2017-04-05.
 */
;(function ($) {
    'use strict';

    $.widget('pizzapi.streetAutocomplete', {
        _create: function () {
            this.$city = this.element.find("select[data-delivery='city']");

            this._initEvents();
            this._initPlugins();
        },
        _initEvents: function () {
            this._on({

            });
        },
        _initPlugins: function () {
            var self = this;

            function getAddressesData ( request, response ) {

                var city = self.$city.find('option:selected').text();

                $.ajax({
                    url: '/ajax/search_streets.php',
                    dataType: "json",
                    data: {
                        address: request.term,
                        city: city,
                    },
                    success: function( data ) {
                        var array;

                        if ( data.buildings ) {
                            array = $.map( data.buildings, function(value, index) {
                                return [value.full_name];
                            });
                        } else {
                            array = $.map( data.streets, function(value, index) {
                                return [value.name];
                            });
                        }


                        response( array );
                    }
                });
            }


            self.element.find("input[data-delivery='street']").autocomplete({
                source: getAddressesData,
                minLength: 3,
                open: function() {
                    var $this = $(this);

                    $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

                    var menu = $this.data("uiAutocomplete").menu.element;
                    menu.addClass("ordering-form__autocomplete _toppest");
                    menu.wrapInner('<div />');

                },

                close: function() {
                    var $this = $(this);

                    $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

                    var menu = $this.data("uiAutocomplete").menu.element;

                },

                change: function() {
                    var $this = $(this);

                    var menu = $this.data("uiAutocomplete").menu.element;
                    menu.addClass("ordering-form__autocomplete _toppest");

                    // var pane = $(".ordering-form__autocomplete"),
                    // 	api = pane.data('jsp');
                    // 	api.reinitialise();;

                },

                focus: function( event, ui ) {
                    event.preventDefault(); // without this: keyboard movements reset the input to ''

                    // console.log(event, ui);

                    $(this).val(ui.item.label);
                }
            });

        }
    });
})(jQuery);

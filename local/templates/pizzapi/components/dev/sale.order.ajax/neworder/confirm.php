<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="container container__popup">
    <div class="confirm-order_wrapper">
        <?
        $arOrderData = CNiyamaOrders::GetOrderById($arResult["ORDER"]["ACCOUNT_NUMBER"], true);
        $GLOBALS['PARTNERS_DATA'] = $arResult['ORDER'];
        if (!empty($arResult["ORDER"])) {
            ?>
            <b>
                <div class="order__title order__title_popup"><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></div>
            </b><br/><br/>
            <table class="sale_order_full_table">
                <tr>
                    <td>
                        <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arOrderData['ORDER']['UF_FO_ORDER_CODE'])) ?>
                        <br/><br/>
                    </td>
                </tr>
            </table>
            <?
            if (!empty($arResult["PAY_SYSTEM"])) {
                ?>
                <br/><br/>
                <input type="hidden" value="<?= $arResult["PAY_SYSTEM"]["PATH_TO_ACTION"] ?>"/>
                <table class="sale_order_full_table">
                    <tr>
                        <td class="ps_logo">
                            <div class="pay_name"><?= GetMessage("SOA_TEMPL_PAY") ?></div>
                            <?= CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false); ?>
                            <div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div>
                            <br>
                        </td>
                    </tr>
                </table>


                <?
            }

            if(CUsersData::isGuest()){
                $USER->Logout();
            }

            if (!$USER->IsAuthorized()) {

                $APPLICATION->IncludeComponent("bitrix:main.register", "popup", Array(
                        "USER_PROPERTY_NAME" => "",
                        "SEF_MODE" => "Y",
                        "SHOW_FIELDS" => Array(),
                        "REQUIRED_FIELDS" => Array(),
                        "AUTH" => "Y",
                        "USE_BACKURL" => "N",
                        "SUCCESS_PAGE" => "",
                        "SET_TITLE" => "N",
                        "USER_PROPERTY" => Array(),
                        "SEF_FOLDER" => "/",
                        "VARIABLE_ALIASES" => Array()
                    )
                );
            }

        } else {
            ?>
            <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br/><br/>

            <table class="sale_order_full_table">
                <tr>
                    <td>
                        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?>
                        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
                    </td>
                </tr>
            </table>
            <?
        }
        $APPLICATION->IncludeComponent("dev:pizza.partners", "partners", Array(
            'ALL_DATA' => $GLOBALS['PARTNERS_DATA'],
            'PAGE_TYPE' => 'thanks'
        ),
            false
        );
        ?>
    </div>
</div>
<!-- храним информацию о платежной системе -->
<input type="hidden" name="paySystemId" value="<?= $arResult["ORDER"]["PAY_SYSTEM_ID"] ?>"/>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$GLOBALS['PARTNERS_DATA'] = $arResult['GRID']['ROWS'];
$GLOBALS['ORDER_TOTAL_PRICE'] = $arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE'];

$name = $USER->GetFirstName();
$mail = $USER->GetEmail();
$data = CUser::GetByID(
    $USER->GetID()
);
$userData = $data->Fetch();
CUtil::InitJSCore(array('ajax', 'window', 'popup'));

if ($USER->IsAuthorized()) {
    foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $index => $userProp) {
        if ($userProp['CODE'] == 'CUSTOMER_NAME') {
            $arResult["ORDER_PROP"]["USER_PROPS_N"][$index]['VALUE'] = $name;
        }
        if ($userProp['CODE'] == 'CUSTOMER_EMAIL') {
            $arResult["ORDER_PROP"]["USER_PROPS_N"][$index]['VALUE'] = $mail;
        }
        if ($userProp['CODE'] == 'CUSTOMER_PHONE') {
            $arResult["ORDER_PROP"]["USER_PROPS_N"][$index]['VALUE'] = $userData['PERSONAL_PHONE'];
        }
    }
} else {
    $mailForDelivery = CNiyamaCustomSettings::GetStringValue('mail_for_delivery');
}

if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
    if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
    {
        if(strlen($arResult["REDIRECT_URL"]) > 0)
        {

            $APPLICATION->RestartBuffer();
            ?>          <script type="text/javascript">
                //window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
            </script>
            <?
            die();
        }

    }
}
$this->addExternalJS($templateFolder."/order.js");
$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
?>

<a name="order_form"></a>

<div id="order_form_div" class="order-checkout" onclick="">
<NOSCRIPT>
    <div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
    function getColumnName($arHeader)
    {
        return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
    }
}

if (!function_exists("cmpBySort"))
{
    function cmpBySort($array1, $array2)
    {
        if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
            return -1;

        if ($array1["SORT"] > $array2["SORT"])
            return 1;

        if ($array1["SORT"] < $array2["SORT"])
            return -1;

        if ($array1["SORT"] == $array2["SORT"])
            return 0;
    }
}
?>
    <div class="order__header">
        <h4 class="order__title"><?=GetMessage("SOA_TEMPL_PROP_INFO")?></h4>
        <a href="/personal/cart/" alt ="назад" class="order__back__link">Назад</a>
    </div>
<div class="bx_order_make">

    <?
    if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
    {
        if(!empty($arResult["ERROR"]))
        {
            foreach($arResult["ERROR"] as $v)
                echo ShowError($v);
        }
        elseif(!empty($arResult["OK_MESSAGE"]))
        {
            foreach($arResult["OK_MESSAGE"] as $v)
                echo ShowNote($v);
        }

        include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
    }
    else
    {
        if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
        {
            if(strlen($arResult["REDIRECT_URL"]) == 0)
            {
                if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
                {
                    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
                }
            } 
        }
        else
        {
            ?>
            <script type="text/javascript">

            <?if(CSaleLocation::isLocationProEnabled()):?>

                <?
                // spike: for children of cities we place this prompt
                $city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
                ?>

                BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
                    'source' => $this->__component->getPath().'/get.php',
                    'cityTypeId' => intval($city['ID']),
                    'messages' => array(
                        'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
                        'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
                        'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
                            '#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
                            '#ANCHOR_END#' => '</a>'
                        )).'</div>'
                    )
                ))?>);

            <?endif?>


            var emailChecker = function (value) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
                return re.test(value.trim());
            };


            var BXFormPosting = false;
            function submitForm(val)
            {
                if (val == 'Y'){
                    if (false === $("#ORDER_FORM").parsley().validate())	{
                        var $deliveryCoin = $("#ORDER_FORM").find("input[data-delivery='coin']");
                        if($deliveryCoin.length){
                            if(!$deliveryCoin.parsley().isValid()){
                                window.Informer.error("Сумма сдачи должна быть не менее " + $("[data-delivery='coin']").data("parsley-min") + ' руб.');
                            }
                        }
                        return false;
                    }
                    var dt = validateForDateTime();
                    if(dt !== false){
                        $(dt).focus();
                        return false;
                    }
                } else {
                    $("#ORDER_FORM").parsley().destroy();
                }


                if (BXFormPosting === true)
                    return true;

                BXFormPosting = true;
                if(val != 'Y')
                    BX('confirmorder').value = 'N';

                var orderForm = BX('ORDER_FORM');
                BX.showWait();

                <?if(CSaleLocation::isLocationProEnabled()):?>
                    BX.saleOrderAjax.cleanUp();
                <?endif?>

                BX.ajax.submit(orderForm, ajaxResult);

                return true;
            }

            function ajaxResult(res)
            {
                var orderForm = BX('ORDER_FORM');
                var paySystemId = "<?= $arResult['ORDER_DATA']['PAY_SYSTEM_ID']?>";
                try
                {

                    // if json came, it obviously a successfull order submit

                    var json = JSON.parse(res);

                    BX.closeWait();

                    if (json.error)
                    {
                        BXFormPosting = false;
                        return;
                    }
                    else if (json.redirect)
                    {
                        window.responseJSON = json;
                        sendAjaxConfirm(json.redirect);
                    }
                }
                catch (e)
                {

                    // json parse failed, so it is a simple chunk of html

                    BXFormPosting = false;
                    BX('order_form_content').innerHTML = res;

                    <?if(CSaleLocation::isLocationProEnabled()):?>
                        BX.saleOrderAjax.initDeferredControl();
                    <?endif?>
                }

                BX.closeWait();
                BX.onCustomEvent(orderForm, 'onAjaxSuccess');



                if(!$('#order_form_div').is( ":data('pizzapi-orderingForm')" )){
                    $('#order_form_div').orderingForm({mailDelivery: "<?= $mailForDelivery?>"});
                } else {
                    $('#order_form_div').orderingForm('destroy');
                    $('#order_form_div').orderingForm({mailDelivery: "<?= $mailForDelivery?>"});
                }

            }

            function SetContact(profileId)
            {
                BX("profile_change").value = "Y";
                submitForm();
            }
            </script>
            <?if($_POST["is_ajax_post"] != "Y")
            {
                ?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data" data-parsley-validate>
                <?=bitrix_sessid_post()?>
                <div id="order_form_content">
                <?
            }
            else
            {
                $APPLICATION->RestartBuffer();
            }

            if($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
            {
                ?>
                <input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
                <?
            }


            if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
            {
                foreach($arResult["ERROR"] as $v)
                    echo ShowError($v);
                ?>
                <script type="text/javascript">
                    top.BX.scrollToNode(top.BX('ORDER_FORM'));
                </script>
                <?
            }

            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props_for_payment.php");
            
            if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
                echo $arResult["PREPAY_ADIT_FIELDS"];
            ?>

            <?if($_POST["is_ajax_post"] != "Y")
            {
                ?>
                    </div>
                    <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                    <input type="hidden" name="profile_change" id="profile_change" value="N">
                    <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                    <input type="hidden" name="json" value="Y">
                    <div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="order__makeorder__button " data-price="<?=$arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE']?>"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>
                </form>
                <?
                if($arParams["DELIVERY_NO_AJAX"] == "N")
                {
                    ?>
                    <div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
                    <?
                }
            }
            else
            {
                ?>
                <script type="text/javascript">
                    top.BX('confirmorder').value = 'Y';
                    top.BX('profile_change').value = 'N';
                </script>
                <?
                die();
            }
        }
    }
    ?>
    </div>
</div>

<?if(CSaleLocation::isLocationProEnabled()):?>

    <div style="display: none">
        <?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.steps", 
            ".default", 
            array(
            ),
            false
        );?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.search", 
            ".default", 
            array(
            ),
            false
        );?>
    </div>

<?endif?>


<script>

    BX.ready(
        function () {

            var oPopupConfirm = new BX.PopupWindow('confirm', window.body, {
                autoHide: true,
                offsetTop: 1,
                offsetLeft: 0,
                lightShadow: true,
                closeIcon: true,
                closeByEsc: true,
                overlay: {
                    backgroundColor: 'black', opacity: '80'
                },
                events: {
                    onPopupClose: function () {
                        window.location.href = "/";
                    }
                }

            });

            window.sendAjaxConfirm = function (url) {

                $.post(url, {'dataType': 'html'},
                    function (response) {
                        if (response) {

                            var paySystemId = $(response).find("input[name='paySystemId']").val();

                            if(paySystemId == '2') {
                                window.location.href = window.responseJSON.paymentRedirect;
                            } else {
                                $(response).find('.bx_order_make');
                                var dataPopup = $(response).find('.bx_order_make').html();
                                oPopupConfirm.setContent(dataPopup);
                                oPopupConfirm.show();
                                yaCounter25059578.reachGoal('zakaz');
                            }

                        } else {
                            console.log('sale.order.ajax: sendAjaxConfirm response is null');
                        }
                    });
            };
        }
    );


    function getDateFromFormat(date) {
        var d = date.split('.');

        var nowDate = new Date();
        var getCurrentHours = nowDate.getHours();
        var getCurrentMinutes = nowDate.getMinutes();
        var newDate = new Date(parseInt(d[2]), parseInt(d[1])-1, parseInt(d[0]));
        newDate.setHours(getCurrentHours);
        newDate.setMinutes(getCurrentMinutes);
        return newDate;
    }

    function addTimeFromFormat(date, time) {
        var t = time.split(':');
        date.setHours(parseInt(t[0]));
        date.setMinutes(parseInt(t[1]));
        return date;
    }

    function validateForDateTime() {

        var inputTime = $(".DELIVERY_TIME input[type='text']");
        var inputDate = $(".DELIVERY_DATE input[type='text']");

        if(inputTime.length < 1  || inputDate.length < 1) return false;

        var inputDateValue = inputDate.val();
        var inputTimeValue = inputTime.val();


        var inputTimeParent = inputTime.closest('div');
        var inputDateParent = inputDate.closest('div');

        var nowDateTime = new Date();
        var date = new Date();
        nowDateTime.setMinutes(nowDateTime.getMinutes() + 30);

        var inputDateTime = getDateFromFormat(inputDateValue);

        var inputDateOnlyDate = new Date(inputDateTime.getFullYear(), inputDateTime.getMonth(), inputDateTime.getDate());
        var nowDateOnlyDate = new Date(nowDateTime.getFullYear(), nowDateTime.getMonth(), nowDateTime.getDate());

        inputDateTime = addTimeFromFormat(inputDateTime, inputTimeValue);

        if(inputDateOnlyDate < nowDateOnlyDate) {
            if(!$("div.order__delivery_date_error").length){
                inputDateParent.append("<div class='order__delivery_date_error'>Вы не можете заказывать на дату из прошлого</div>");
            }
            if(!inputDate.hasClass('notValid')){
                inputDate.addClass('notValid');
            }
            if($("div.order__delivery_time_error").length){
                $("div.order__delivery_time_error").remove();
            }
            if(inputTime.hasClass('notValid')){
                inputTime.removeClass('notValid');
            }
            return inputDate;
        } else {
            if($("div.order__delivery_date_error").length){
                $("div.order__delivery_date_error").remove();
            }
            if(inputDate.hasClass('notValid')){
                inputDate.removeClass('notValid');
            }
        }

        if(inputDate.inputmask("isComplete") && inputTime.inputmask("isComplete")){
            if(inputDateTime >= nowDateTime) {
                if($("div.order__delivery_time_error").length){
                    $("div.order__delivery_time_error").remove();
                }
                if(inputTime.hasClass('notValid')){
                    inputTime.removeClass('notValid');
                }
            } else {
                if(!$("div.order__delivery_date_error").length){
                    if(!$("div.order__delivery_time_error").length){
                        inputTimeParent.append("<div class='order__delivery_time_error'>Необходимо не менее 30 минут для подготовки заказа</div>");
                    }
                    if(!inputTime.hasClass('notValid')){
                        inputTime.addClass('notValid');
                    }
                    return inputTime;
                }
            }
        }

        return false;
    }


    if(!$('#order_form_div').is( ":data('pizzapi-orderingForm')" )){
        $('#order_form_div').orderingForm({mailDelivery: "<?= $mailForDelivery?>" });
    }

</script>
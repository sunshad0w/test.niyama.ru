/**
 * Created by user on 2017-04-05.
 */
;(function ($) {
    'use strict';

    $.widget('pizzapi.orderingForm', {
        options: {
            mailDelivery: ""
        },
        _create: function () {

            this.$form = this.element.find('form');

            this.currentBlock = 1;

            this.isDelivery = true;

            this.$orderTotalSum = this.element.find('.order__amount-sum');

            this.$mkadCheckbox = this.element.find('input._mkad');

            this.$bodies = this.element.find('.ordering-form__body');
            this.$isCash = $("#iscash");

            this.$sidebar = this.element.find('.ordering-form__side');

            // inputs for desc

            // block1
            this.$name = this.element.find(".CUSTOMER_NAME input[type='text']");
            this.$phone = this.element.find(".CUSTOMER_PHONE input[type='text']");
            this.$email = this.element.find(".CUSTOMER_EMAIL input[type='text']");
            this.$quantity = this.element.find(".QUANTITY_PERSON input[type='text']");

            // block2
            this.$city = this.element.find("select[data-delivery='city']").eq(0);
            this.$metro = this.element.find("input[data-delivery='metro']");
            this.$other = this.element.find("input[data-delivery='other']");
            this.$restaurant = this.element.find("select[data-delivery='restaurant']");

            this.$address = this.element.find("input[data-delivery='street']");
            this.$house = this.element.find("input[data-delivery='house']");
            this.$corpus = this.element.find("input[data-delivery='corpus']");
            this.$build = this.element.find("input[data-delivery='build']");
            this.$apart = this.element.find("input[data-delivery='apart']");
            this.$porch = this.element.find("input[data-delivery='porch']");
            this.$intercom = this.element.find("input[data-delivery='intercom']");
            this.$floor = this.element.find("input[data-delivery='floor']");


            this.$deliveryRadios = this.element.find("input[name='DELIVERY_ID']");
            this.$deliveryRadiosChecked = this.element.find("input[name='DELIVERY_ID']:checked");

            this.$deliveryDate = this.element.find(".DELIVERY_DATE input[type='text']");
            this.$deliveryTime = this.element.find(".DELIVERY_TIME input[type='text']");

            this.$deliveryCoin = this.element.find("input[data-delivery='coin']");

            // способы доставки
            this.$deliveryTypeGroup = $('#deliveryTypeGroup');
            this.$deliveryControls = $('#deliveryControls');
            this.$pickupControls = $('#pickupControls');

            this.$pickupControls.find('.ordering-form__row._rests').hide().eq(0).show();
            this.$pickupControls.detach();

            // Скрипт, чтобы убрать email из самовывоза
            // if(this.$deliveryRadiosChecked.val() == '4'){
            //     this.$email.removeAttr("data-parsley-required");
            //     this.$email.attr('type','hidden');
            //     var date = new Date();
            //     if(this.$email.val() == ""){
            //         this.$email.val(date.getTime() + this.options.mailDelivery);
            //     }
            //     $(".CUSTOMER_EMAIL").css('display','none');
            // }

            this._initEvents();
            this._initPlugins();

            // Первоначальный выбор типа доставки
            this._initDeliveryType();

        },
        _initEvents: function () {
            this._on({
                'click button': this._handleButtonClick,
                'submit form': this._handleFormSubmit,
                'click .ordering-form__edit': this._handleEditClick,
                'keypress .input': this._handleEnterClick
            });

            this.$deliveryRadios.on('change', $.proxy(this._handleDeliveryRadioChange, this));


            // Обработчики на пересчёт доставки

            this.$city.on('change', $.proxy(this._sendDeliveryAjax, this));
            this.$mkadCheckbox.on('change', $.proxy(this._sendDeliveryAjax, this));


            // обработчик на налик - потребуется ли сдача?
            this.element.find('.ordering-form__table').find('input').on('change', $.proxy(this._handlePaymentCheckboxesChange, this));

            this.$metro.on('change', function () {
                var $this = $(this);

                if ($this.val !== '') {
                    $this.siblings('.chosen-container-error').removeClass('chosen-container-error');
                }
            });

            var self = this;
            this.$sidebar.find('.goods__add-btn').on('click', function () {
                self.handleAddToCartClick(this, self);
                return false;
            });
        },

        _initDeliveryType: function () {
            var $target = $('#deliverytype').find('input:checked');
            this._changeDeliveryType($target);
        },

        _initPlugins: function () {
            var self = this;



            var c_b_obj = {
                "onincomplete" : function () {
                    $(this).addClass('notValid');
                },
                "oncomplete"   : function () {
                    $(this).removeClass('notValid');
                }
            };
            var validatorTime = {
                "onincomplete" : function () {
                    $(this).addClass('notValid');
                    if($("div.order__delivery_time_error").length){
                        $("div.order__delivery_time_error").remove();
                    }
                    if($("div.order__delivery_date_error").length){
                        $("div.order__delivery_date_error").remove();
                    }
                },
                "oncomplete"   : function () {
                    validateForDateTime();
                }
            };

            var validatorDate = {
                "onincomplete" : function () {
                    $(this).addClass('notValid');
                    if($("div.order__delivery_time_error").length){
                        $("div.order__delivery_time_error").remove();
                    }
                    if($("div.order__delivery_date_error").length){
                        $("div.order__delivery_date_error").remove();
                    }
                },
                "oncomplete"   : function () {
                    validateForDateTime();
                }
            };

            //this.$name.inputmask('Regex', {regex : "[a-zA-Zа-яА-Я- ]+"});
            $(document).on('keypress', ".CUSTOMER_NAME input[type='text']", FilterTextInput.inputOnlyLetters);
            //this.$email.inputmask('email', c_b_obj);
            this.$quantity.inputmask('integer');
            this.$apart.inputmask('integer');

            //this.$phone.inputmask('phoneru', c_b_obj);

            this.$phone.inputmask("+7 (999) 999-99-99", c_b_obj); // +7 (___) ___-__-__
            this.$deliveryDate.inputmask({'alias' : "dd.mm.yyyy", 'placeholder' : 'дд.мм.гггг'});
            this.$deliveryTime.inputmask({'alias' : "hh:mm", 'placeholder' : 'чч:мм'});
            this.$deliveryDate.inputmask(validatorDate);
            this.$deliveryTime.inputmask(validatorTime);


            $(function() {
                function log( message ) {
                    $( "<div>" ).text( message ).prependTo( "#log" );
                    $( "#log" ).scrollTop( 0 );
                }

                function getHousesData ( request, response, street ) {
                    var city = self.$city.find('option:selected').text();

                    var address = self.selectedStreet || request.term;

                    var house = request.term;

                    $.ajax({
                        url: '/ajax/search_streets.php',
                        dataType: "json",
                        data: {
                            address: address,
                            city: city,
                            house: house
                        },
                        success: function( data ) {
                            var array;

                            if ( data.buildings ) {
                                array = $.map( data.buildings, function(value, index) {
                                    return [value.full_name];
                                });
                            } else {
                                array = $.map( data.streets, function(value, index) {
                                    return [value.name];
                                });
                            }


                            response( array );
                        }
                    });
                }

                function getAddressesData ( request, response, street ) {

                    var city = self.$city.find('option:selected').text();

                    //var address = self.selectedStreet || request.term;
                    var address = self.$address.val();

                    $.ajax({
                        url: '/ajax/search_streets.php',
                        dataType: "json",
                        data: {
                            address: address,
                            city: city,
                        },
                        success: function( data ) {
                            var array;

                            if ( data.buildings ) {
                                array = $.map( data.buildings, function(value, index) {
                                    return [value.full_name];
                                });
                            } else {
                                array = $.map( data.streets, function(value, index) {
                                    return [value.name];
                                });
                            }


                            response( array );
                        }
                    });
                }

                function getMetroData ( request, response, station ) {

                    var city = self.$city.find('option:selected').text();

                    //var address = self.selectedStreet || request.term;
                    var address = self.$metro.val();

                    $.ajax({
                        url: '/ajax/search_metro.php',
                        dataType: "json",
                        data: {
                            address: address,
                            city: city,
                        },
                        success: function( data ) {
                            var array;

                            if ( data.buildings ) {
                                array = $.map( data.buildings, function(value, index) {
                                    return [value.full_name];
                                });
                            } else {
                                array = $.map( data.streets, function(value, index) {
                                    return [value.name];
                                });
                            }


                            response( array );
                        }
                    });
                }

                function parseKladrAddress(kladrAddress) {
                    var result = kladrAddress.match( /([0-9]+(\/[0-9]+)?[А-ЯЁ]?)(к([0-9]+))?(стр([0-9]+[А-ЯЁ]?))?/u );
                    var house, corpus, build;
                    if(result[1]){
                        house = result[1];
                    }else {
                        house = "";
                    }
                    if(result[4]){
                        corpus = result[4];
                    }else{
                        corpus = "";
                    }
                    if(result[6]){
                        build = result[6];
                    }else{
                        build = "";
                    }
                    self.$house.val(house);
                    self.$corpus.val(corpus);
                    self.$build.val(build);
                }

                function initHouseAutocomplete(street) {
                    self.selectedStreet = street;

                    self.$house.autocomplete({
                        source: getHousesData,
                        select: function( event, ui ) {
                            self.$house.focus();
                            parseKladrAddress(ui.item.label);
                            log( ui.item ?
                                "Selected: " + ui.item.label :
                                "Nothing selected, input was " + this.value);
                            return false;
                        },

                        open: function() {
                            var $this = $(this);
                            $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                            var menu = $this.data("uiAutocomplete").menu.element;
                            menu.addClass("ordering-form__autocomplete _house");
                            $this.removeClass("ui-autocomplete-loading");
                            menu.wrapInner('<div />');
                            $("ul.ui-menu").width( $this.innerWidth() );
                        },

                        close: function() {
                            var $this = $(this);
                            $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                            var menu = $this.data("uiAutocomplete").menu.element;
                        },

                        change: function() {
                            var $this = $(this);

                            var menu = $this.data("uiAutocomplete").menu.element;
                            menu.addClass("ordering-form__autocomplete _house");
                        },

                        focus: function( event, ui ) {
                            event.preventDefault(); // without this: keyboard movements reset the input to ''

                            //$(this).val(ui.item.label);
                            parseKladrAddress(ui.item.label);

                        }

                    });
                }


                self.element.find("input[data-delivery='street']").autocomplete({
                    source: getAddressesData, minLength: 3,
                    select: function( event, ui ) {
                        initHouseAutocomplete(self.$address.val());
                        self.$house.focus();
                        log( ui.item ?
                            "Selected: " + ui.item.label :
                            "Nothing selected, input was " + this.value);
                    },

                    open: function() {
                        var $this = $(this);
                        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

                        var menu = $this.data("uiAutocomplete").menu.element;
                        $this.removeClass("ui-autocomplete-loading");
                        menu.addClass("ordering-form__autocomplete");
                        menu.wrapInner('<div />');
                        $("ul.ui-menu").width( $this.innerWidth() );

                    },

                    close: function() {
                        var $this = $(this);

                        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

                        var menu = $this.data("uiAutocomplete").menu.element;

                    },

                    change: function() {
                        var $this = $(this);

                        var menu = $this.data("uiAutocomplete").menu.element;
                        menu.addClass("ordering-form__autocomplete");

                        // var pane = $(".ordering-form__autocomplete"),
                        // 	api = pane.data('jsp');
                        // 	api.reinitialise();;

                    },

                    focus: function( event, ui ) {
                        event.preventDefault(); // without this: keyboard movements reset the input to ''

                        delete self.selectedStreet;
                        // console.log(event, ui);

                        $(this).val(ui.item.label);
                    }

                });

                self.element.find("input[data-delivery='metro']").autocomplete({
                    source: getMetroData,
                    minLength: 3,
                    select: function( event, ui ) {
                        log( ui.item ?
                            "Selected: " + ui.item.label :
                            "Nothing selected, input was " + this.value);
                    },

                    open: function() {
                        var $this = $(this);

                        $this.removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );

                        var menu = $this.data("uiAutocomplete").menu.element;
                        menu.addClass("ordering-form__autocomplete");
                        menu.wrapInner('<div />');
                        $("ul.ui-menu").width( $this.innerWidth() );

                    },

                    close: function() {
                        var $this = $(this);

                        $this.removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );

                        var menu = $this.data("uiAutocomplete").menu.element;

                    },

                    change: function() {
                        var $this = $(this);

                        var menu = $this.data("uiAutocomplete").menu.element;
                        menu.addClass("ordering-form__autocomplete");

                        // var pane = $(".ordering-form__autocomplete"),
                        // 	api = pane.data('jsp');
                        // 	api.reinitialise();;

                    },

                    focus: function( event, ui ) {
                        event.preventDefault(); // without this: keyboard movements reset the input to ''

                        delete self.selectedStreet;
                        // console.log(event, ui);

                        $(this).val(ui.item.label);
                    }
                })

            });

        },

        _handleButtonClick: function (e) {
            // var $target = $(e.currentTarget),
            // 	current = $target.data('currentBlock'),
            // 	next = $target.data('nextBlock');

            this._validateStep();
        },

        _handleAddressKeyup: function (e) {
            var city = this.$city.find('option:selected').text(),
                address = $(e.currentTarget).val();

            $.ajax({
                type: "GET",
                url: '/ajax/search_streets.php',
                dataType: 'json',
                data: {
                    city: city,
                    address: address
                },
                success: function (data) {
                    $.each(data.streets, function (k, v) {
                        // console.log(k, v);
                    })
                }
            });
        },

        _handleFormSubmit: function () {

        },

        _handleEnterClick: function (e) {
            if (e.keyCode == 13) {
                this._validateStep();

                return false;
            }
        },

        _handleEditClick: function (e) {
            var $target =  $(e.currentTarget),
                blockID = $target.data('block'),
                self = this;

            $('.block' + this.currentBlock).slideUp();
            $('.block' + blockID).slideDown('', function () {
                $(this).css({
                    'overflow': 'visible'
                });
            });

            $('#desc' + blockID).slideUp();

            $target.removeClass("_visible");

            for (var i = blockID+1; i < 10; i++) {
                $('#edit' + i).removeClass("_visible");
                $('.block' + i ).siblings('.ordering-form__counter').removeClass('_orange');
            };

            self.currentBlock = blockID;

        },

        _handleDeliveryTypeChange: function (e) {
            var $target = $(e.currentTarget);

            this._changeDeliveryType($target);
        },

        _changeDeliveryType: function ($target) {
            var self = this,
                $title = $('#desc2').siblings('.ordering-form__title');

            if ( $target.hasClass('_delivery') ) {
                this.isDelivery = true;

                this.$pickupControls.slideUp(function () {
                    self.$pickupControls.detach();
                });

                this.$deliveryTypeGroup.prepend(this.$deliveryControls);
                this.$deliveryControls.slideDown('', function () {
                    $(this).css({
                        'overflow': 'visible'
                    });
                });

                var $edit = $('#edit2');
                $edit.detach();

                $title.text("Доставка").append($edit);

            } else {

                this.isDelivery = false;

                this.$deliveryControls.slideUp(function () {
                    self.$deliveryControls.detach();
                });

                this.$deliveryTypeGroup.prepend(this.$pickupControls);
                this.$pickupControls.slideDown('', function () {
                    $(this).css({
                        'overflow': 'visible'
                    });
                });

                var $edit = $('#edit2');
                $edit.detach();

                $title.text("Самовывоз").append($edit);
            }
        },

        _handleDeliveryRadioChange: function (e) {
            var $target = $(e.target);

        },

        _handlePaymentCheckboxesChange: function (e) {
            var $target = $(e.currentTarget);

            if ($target.hasClass('_cash')) {
                this.$isCash.slideDown();
                $('#submitOrder').val('Оформить');
            } else {
                this.$isCash.slideUp();
                this.$isCash.find('input').val('');
                $('#submitOrder').val('Оплатить');
            }
        },

        _createDesc: function () {
            var $desc = $('<div />');

            if (this.currentBlock === 1) {

                var $name = $('<p />').text(this.$name.val() + " " + this.$surname.val()),
                    $phone = $('<p />').text(this.$phone.val()),
                    $email = $('<p />').text(this.$email.val());

                $desc.append($name, $phone, $email);

            } else if (this.currentBlock === 2) {
                if (!this.isDelivery) {
                    var $restAddress = $('<p />').text(
                        this.$city.find('option:selected').text() + ", " + $("#rest_" + $("#deliveryTypeGroup").find('select._city').val() ).find('option:selected').text()
                    )

                    $desc.append($restAddress);

                } else {
                    var $city = $('<p />').text(
                            this.$city.find('option:selected').text() + ", " + this.$metro.find('option:selected').text()
                        ),

                        $address = this.$address,
                        $apart = this.$apart,
                        $porch = this.$porch,
                        $intercom = this.$intercom,
                        $floor = this.$floor;

                    var $addressLine = $('<p />'),
                        addressText = '',
                        $delivery = $('<p />'),
                        deliveryText;

                    if ( $address.val() ) {
                        addressText += $address.val();
                    }

                    if ( $apart.val() ) {
                        addressText += ', кв. ' + $apart.val();
                    }

                    if ( $porch.val() ) {
                        addressText += ', подъезд ' + $porch.val();
                    }

                    if ( $floor.val() ) {
                        addressText += ', ' + $floor.val() + ' этаж';
                    }

                    if ( $intercom.val() ) {
                        addressText += ', домофон: ' + $intercom.val();
                    }
                    $addressLine.text(addressText);


                    var $deliveryCheckbox = $('[name="deliverytime"]:checked');
                    if ($deliveryCheckbox.hasClass('_soon')) {
                        deliveryText = "Время доставки: " + $deliveryCheckbox.siblings('label').text();
                    } else {
                        deliveryText = "Время доставки: " + $('.ordering-form__row._certain').find('.input').val() + ', ' + $('.ordering-form__row._certain').find('option:selected').text();
                    }
                    $delivery.append(deliveryText);


                    $desc.append($city, $addressLine, $delivery);
                }

            }

            $('#desc' + this.currentBlock).html($desc).slideDown();
        },

        _sendDeliveryAjax: function () {

            var data = {},
                self = this;

            data.id = this.$city.val();
            data.totalPrice = $('#ORDER_CONFIRM_BUTTON').data('price');

            var region = this.$city.find('option:selected').data("city");

            if ( region === "REGION_1" ) {
                data.inMKAD = this.$mkadCheckbox.prop("checked");
            }

            if (this.$certain.prop("checked")) {
                data.time = this.$certainTime.val();
                data.date = this.$certainDate.val();
            }

            $.ajax({
                type: "GET",
                url: '/ajax/calcOrderDelivery.php',
                // url: '/testdeliveryJSON.json',
                dataType: 'json',
                data: data,
                success: function (result) {
                    self.$orderTotalSum.html(result.totalSum + ' ');
                    if(!result.bDeliveryAvailable ){
                        Informer.error("Минимальная стоимость заказа для доставки по вашему адресу составляет " + result.min_price + " рублей.");
                    }

                }
            });
        },

        handleAddToCartClick: function (goodsButton, self) {
            var $goodsButton = $(goodsButton);

            var pid = $goodsButton.data('id'),
                price = $goodsButton.data('price'),
                el = $goodsButton.parent().parent().parent(),
                // clone.find('.goods__add-btn').remove();
                // clone.find('.goods__num').remove();
                // clone.append('<div class="goods__change"> </div><div class="goods__remove" data-id="' + data.ID + '"> </div></div>');
                clone = el.clone().hide().css({position:'absolute',top:el.position().top, left:el.position().left}),
                clone2 = el.clone().css({position:'absolute',top:el.offset().top - 10, left:el.offset().left - 4, width: el.width(), 'z-index': 100}).appendTo('body');
            clone2.animate({top:'300px', opacity:0},
                1000,
                function(){
                    clone2.remove();
                    var cloneHTML = clone.removeAttr('style').addClass('_table').removeClass('ui-draggable');
                    $.publish('addGoods', cloneHTML);
                });

            $.ajax({
                type: "GET",
                url: '/ajax/add2cart.php',
                dataType: 'json',
                data: {
                    pid: pid,
                    quantity: 1,
                    fromOrderPromo: true
                },
                success: function (data) {
                    window.ajaxData = data;

                    $.ajax({
                        url: self.element.data('sidebarUrl'),
                        type: 'POST',
                        data: self.$form.serializeArray(),
                        success: function (data) {
                            self.$sidebar.html(data);

                            //$.initWidgets(self.$sidebar);

                            /*self.$sidebar.find('.order__add-carousel').owlCarousel({
                             singleItem: true,
                             navigation: true,
                             mouseDrag: true,
                             touchDrag: true
                             });*/

                            self.$sidebar.find('.goods__add-btn').off('click').on('click', function () {
                                self.handleAddToCartClick(this, self);
                                return false;
                            });
                        }
                    });

                    $('.cart-buy__sum i').text(data.TOTAL_PRICE);
                    $('#guests__card-sum').text(data.SUB_PRICE);
                }
            });

            return false

        }

    });
})(jQuery);





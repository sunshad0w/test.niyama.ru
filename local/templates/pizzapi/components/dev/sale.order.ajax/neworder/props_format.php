<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*echo "<pre>";
echo print_r($arResult["ORDER_PROP"]["USER_PROPS_N"], true);*/
?>
<?
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}
if(!function_exists("deliveryPropertiesMap")){
    function deliveryPropertiesMap(){
        return [
            "DELIVERY_REGION" => "city", // �����
            "DELIVERY_ADDRESS" => "street", // �����
            "DELIVERY_APART" => "apart", // ��������
            "DELIVERY_PORCH" => "porch", // �������
            "DELIVERY_HOUSE" => "house", // ���
            "DELIVERY_RESTAURANT" => "restaurant", // �������� ����������
            "DELIVERY_DATE" => "date", // ���� ��������
            "DELIVERY_LOCATION" => "location", // ��������������
            "DELIVERY_COIN" => "coin", // ����� � �����
            "DELIVERY_TIME" => "time", // ����� ��������
            "DELIVERY_CORPUS" => "corpus", // ������
            "DELIVERY_BUILD" => "build", // ��������
            "DELIVERY_FLOOR" => "floor", // ����
            "DELIVERY_INTERCOM" => "intercom", // �������
        ];
    }
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default")
	{
		if (!empty($arSource))
		{
		    $deliveryProps = deliveryPropertiesMap();

			?>
				<div  class="order__wrapper__props">
					<?
					foreach ($arSource as $arProperties)
					{
					?>
						<div data-property-id-row="<?=intval(intval($arProperties["ID"]))?>" class="<?= $arProperties['CODE'] ?>">

						<?
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">

							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<input
                                        type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>"
                                        id="<?=$arProperties["FIELD_NAME"]?>"
                                        value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>
                                        class ="<?= ($arProperties["REQUIED_FORMATED"] == "Y")?'required':'' ?>"
                                >

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>

							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
							?>

                            <div class="bx_block r1x3 pt8 order__personal__data__title">
                                <?=$arProperties["NAME"]?>
                                <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                    <span class="bx_sof_req">*</span>
                                <?endif;?>
                            </div>

                            <?if($arProperties["CODE"]=="DELIVERY_REGION"):?>
                                <?
                                    //
                                    // ����������� �� ��������
                                    //
                                    $arTmp = CNiyamaOrders::GetDeliveryRegions();
                                    $arResult['DELIVERY_REGIONS'] = array();
                                    foreach($arTmp as $arItem) {
                                        if($arItem['ACTIVE'] == 'Y') {
                                            $arResult['DELIVERY_REGIONS'][$arItem['ID']] = $arItem;
                                        }
                                    }
                                $arResult['SUBWAY_STATIONS'] = CNiyamaIBlockSubway::GetAllStationsList();
                                if($arResult['DELIVERY_REGIONS']) {
                                ?>
                                <div class="bx_block r3x1">

                                        <select name="<?= $arProperties["FIELD_NAME"] ?>" class="select _city order__personal__data__form" <?= (key_exists($arProperties['CODE'],$deliveryProps))? "data-delivery='{$deliveryProps[$arProperties['CODE']]}'" : ""; ?> <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required' : '' ?> >
                                            <?

                                                foreach($arResult['DELIVERY_REGIONS'] as $arItem) {
                                                    $sTmpSelected = '';
                                                    if($arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION']) {
                                                        $sTmpSelected = ' selected="selected"';
                                                    }
                                                    //$sTmpSelected = $arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION'] ? ' selected="selected"' : '';
                                                    $sTmpDataCity = $arItem['NAME'] == 'Москва' ? 'REGION_1' : 'REGION_X';

                                                    ?><option<?=$sTmpSelected?> data-city="<?=$sTmpDataCity?>" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
                                                }

                                            ?>
                                        </select><?
                                    ?>

                                </div><?
                                // !!!
                                echo ' ';
                                }?>

                            <?else:?>


                                <div class="bx_block r3x1">
                                    <?
                                        // Если фиктивная почта, то не выводить
                                        $mailForDelivery = CNiyamaCustomSettings::GetStringValue('mail_for_delivery');
                                        if(strpos($arProperties["VALUE"],$mailForDelivery) !== false) $arProperties["VALUE"] = "";
                                    ?>
                                    <input type="text" maxlength="250" size="<?= $arProperties["SIZE1"] ?>" value="<?= $arProperties["VALUE"] ?>"
                                           name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>"
                                           <?= $arProperties['IS_PHONE'] == 'Y' ? 'data-parsley-pattern="\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}"' : ""?>
                                           <?= $arProperties['IS_EMAIL'] == 'Y' ? 'data-parsley-type="email"' : ""?>
                                        <?= (key_exists($arProperties['CODE'],$deliveryProps))? "data-delivery='{$deliveryProps[$arProperties['CODE']]}'" : ""; ?>
                                           class="order__personal__data__form <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                        <?= ($arProperties["REQUIED_FORMATED"] == "Y") ?
                                            'data-parsley-required'.
                                                    ($deliveryProps[$arProperties['CODE']] == 'time' ? ' data-parsley-pattern="^([1]\d|0[1-9]|2[0123])\:?([012345]\d)$"' : '').
                                                    ($arProperties["CODE"]=="DELIVERY_COIN"? " data-parsley-min=\"{$GLOBALS['ORDER_TOTAL_PRICE']}\"":'')
                                            : '' ?>
                                    />
                                    <?
                                    if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                        ?>
                                        <div class="bx_description">
                                            <?=$arProperties["DESCRIPTION"]?>
                                        </div>
                                        <?
                                    endif;
                                    ?>
                                </div>
                            <?endif;?>
				
							<?
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
					
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

                        <div class="bx_block r3x1">



								<select name="<?=$arProperties["FIELD_NAME"]?>"
                                        id="<?=$arProperties["FIELD_NAME"]?>"
                                        size="<?=$arProperties["SIZE1"]?>"
                                        class="order__personal__data__form <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                        <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required' : '' ?> >

									<?foreach($arProperties["VARIANTS"] as $arVariants):?>

                                        <? $restName = CNiyamaOrders::GetRestaurantByXmlId($arVariants["VALUE"]); ?>
                                        <?if($restName):?>
                                            <option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$restName;?></option>
                                        <?endif;?>

									<?endforeach;?>
								</select>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>
                            <div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>"
                                        id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>"
                                        class="<?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                        <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required' : '' ?>
                                >
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>

										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>

								<?

								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<br/>
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>"
                                          name="<?=$arProperties["FIELD_NAME"]?>"
                                          id="<?=$arProperties["FIELD_NAME"]?>"
                                          class="<?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                          <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required' : '' ?>
                                >
                                    <?=$arProperties["VALUE"]?>
                                </textarea>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							?>
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">

								<?
								$value = 0;
								if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
								{
									foreach ($arProperties["VARIANTS"] as $arVariant)
									{
										if ($arVariant["SELECTED"] == "Y")
										{
											$value = $arVariant["ID"];
											break;
										}
									}
								}

								// here we can get '' or 'popup'
								// map them, if needed
								if(CSaleLocation::isLocationProMigrated())
								{
									$locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
									$locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
								}
								?>

								<?if($locationTemplateP == 'steps'):?>
									<input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]"
                                           name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]"
                                           value="<?=($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0')?>"
                                           class="<?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                    />
								<?endif?>

								<?CSaleLocation::proxySaleAjaxLocationsComponent(array(
									"AJAX_CALL" => "N",
									"COUNTRY_INPUT_NAME" => "COUNTRY",
									"REGION_INPUT_NAME" => "REGION",
									"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
									"CITY_OUT_LOCATION" => "Y",
									"LOCATION_VALUE" => $value,
									"ORDER_PROPS_ID" => $arProperties["ID"],
									"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
									"SIZE1" => $arProperties["SIZE1"],
								),
								array(
									"ID" => $value,
									"CODE" => "",
									"SHOW_DEFAULT_LOCATIONS" => "Y",

									// function called on each location change caused by user or by program
									// it may be replaced with global component dispatch mechanism coming soon
									"JS_CALLBACK" => "submitFormProxy",

									// function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
									// it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
									"JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

									// an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
									// it may be replaced with global component dispatch mechanism coming soon
									"JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

									"DISABLE_KEYBOARD_INPUT" => "Y",
									"PRECACHE_LAST_LEVEL" => "Y",
									"PRESELECT_TREE_TRUNK" => "Y",
									"SUPPRESS_ERRORS" => "Y"
								),
								$locationTemplateP,
								true,
								'location-block-wrapper'
								)?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>

							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">

								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?> 

										<input
											type="radio"class="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?>
                                            class="<?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '' ?>"
                                            <?= ($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required' : '' ?>
                                        />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>" class="order__delivery_radiobutton_title"><?=$arVariants["NAME"]?></label></br>

									<?
									endforeach;
								}
								?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>

							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8 order__personal__data__title">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>

							<div style="clear: both;"></div><br/>
							<?
						} elseif (($arProperties["TYPE"] == "DATE")) { ?>

                            <div class="bx_block r1x3 pt8 order__personal__data__title">
                                <?=$arProperties["NAME"]?>
                                <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                    <span class="bx_sof_req">*</span>
                                <?endif;?>
                            </div>

                            <div class="bx_block r3x1">
                                <div class="data__picker__order">
                                    <? global $APPLICATION; ?>
                                    <? $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'SHOW_INPUT' => 'Y',
                                            'FORM_NAME' => 'ORDER_FORM',
                                            'INPUT_NAME' => 'ORDER_PROP_'. $arProperties["ID"],
                                            'INPUT_VALUE' => '',
                                            'SHOW_TIME' => 'N',
                                            'INPUT_ADDITIONAL_ATTR' => 'class="order__personal__data__form '.(($arProperties["REQUIED_FORMATED"] == "Y") ? 'required' : '').'"'.((key_exists($arProperties['CODE'],$deliveryProps))? "data-delivery='{$deliveryProps[$arProperties['CODE']]}'":"" ).(($arProperties["REQUIED_FORMATED"] == "Y") ? 'data-parsley-required data-parsley-pattern="^([12]\d|0[1-9]|3[01])\D?(0[1-9]|1[0-2])\D?(\d{4})$"' : '')
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?>
                                </div>

                                <?
                                if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                                    ?>
                                    <div class="bx_description">
                                        <?=$arProperties["DESCRIPTION"]?>
                                    </div>
                                    <?
                                endif;
                                ?>
                            </div>
                        <? } ?>
						</div>

						<?if(CSaleLocation::isLocationProEnabled()):?>

							<?
							$propertyAttributes = array(
								'type' => $arProperties["TYPE"],
								'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
							);

							if(intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
								$propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

							if(intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
								$propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

							if($arProperties['IS_ZIP'] == 'Y')
								$propertyAttributes['isZip'] = true;
							?>

							<script>

								<?// add property info to have client-side control on it?>
								(window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
									'id' => intval($arProperties["ID"]),
									'attributes' => $propertyAttributes
								))?>);

							</script>
						<?endif?>

						<?
					}
					?>
                    <div class="prop_space_hack"></div>
				</div>
	<?
		}
	}
}
?>
<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2017-05-15
 * Time: 18:37
 */
//
// Оформление заказа - страница "Спасибо за заказ" (при оплате наличными)
//

// пауза для обработки заказа в ФО
sleep(10);

$iOrderId = intval($_REQUEST['comment']);
if ($iOrderId <= 0) {
    localredirect(SITE_DIR);
    return;
}

$arOrderData = CNiyamaOrders::GetOrderById($iOrderId, true);
$arPayments = CNiyamaOrders::GetPayments();

//echo "<pre>".print_r($arOrderData,1)."</pre>";

if (!$arOrderData || $arOrderData['CAN_ACCESS'] != 'Y') {
    localredirect(SITE_DIR);
    return;
}


// Костыль для получения ID-шника фаст оператора
if (empty($arOrderData['ORDER']['UF_FO_ORDER_CODE'])) {
    $res = CSaleOrder::GetList(array(), array('ID' => $arOrderData['ORDER']['ID']), false, false, array('UF_FO_ORDER_CODE'));
    if ($fetch = $res->Fetch()) {
        $arOrderData['ORDER']['UF_FO_ORDER_CODE'] = $fetch['UF_FO_ORDER_CODE'];
    }

}


?>

<div class="container container-thanks-page">
    <div class="confirm-order_wrapper">
        <?
        $GLOBALS['PARTNERS_DATA'] = $arOrderData['ORDER'];

        if (!empty($arOrderData["ORDER"])) {
            ?>
            <b>
                <div class="order__title container-thanks-page__title"><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></div>
            </b><br/><br/>
            <table class="container-thanks-page__body">
                <tr>
                    <td>
                        <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arOrderData['ORDER']['DATE_INSERT'], "#ORDER_ID#" => $arOrderData['ORDER']['UF_FO_ORDER_CODE'])) ?>
                        <br/><br/>
                    </td>
                </tr>
            </table>

            <? if ($arOrderData['ORDER']['PAY_SYSTEM_ID']) : ?>
                <table class="container-thanks-page__body">
                    <tr>
                        <td class="ps_logo">
                            <div class="pay_name"><?= GetMessage("SOA_TEMPL_PAY") ?></div>
                            <?= CFile::ShowImage($arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['PSA_LOGOTIP'], 100, 100, "border=0", "", false); ?>
                            <div class="paysystem_name"><?= $arPayments[$arOrderData['ORDER']['PAY_SYSTEM_ID']]['PSA_NAME'] ?></div>
                            <br>
                        </td>
                    </tr>
                </table>
            <? endif; ?>
            <?

            if(CUsersData::isGuest()){
                $USER->Logout();
            }

            if (!$USER->IsAuthorized()) {
                $APPLICATION->IncludeComponent("bitrix:main.register", "popup", Array(
                        "USER_PROPERTY_NAME" => "",
                        "SEF_MODE" => "Y",
                        "SHOW_FIELDS" => Array(),
                        "REQUIRED_FIELDS" => Array(),
                        "AUTH" => "Y",
                        "USE_BACKURL" => "N",
                        "SUCCESS_PAGE" => "",
                        "SET_TITLE" => "N",
                        "USER_PROPERTY" => Array(),
                        "SEF_FOLDER" => "/",
                        "VARIABLE_ALIASES" => Array()
                    )
                );
            }


        } else {
            ?>
            <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br/><br/>

            <table class="sale_order_full_table">
                <tr>
                    <td>
                        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $iOrderId)) ?>
                        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
                    </td>
                </tr>
            </table>
            <?
        }
        $APPLICATION->IncludeComponent("dev:pizza.partners", "partners", Array(
            'ALL_DATA' => $GLOBALS['PARTNERS_DATA'],
            'PAGE_TYPE' => 'thanks'
        ),
            false
        );
        ?>
    </div>
</div>

<script type="text/javascript">
    window.onload = function() {
        yaCounter25059578.reachGoal('zakaz');
    }
</script>



/* Function for ours ajax inquiry  */
function ajaxpostshow(urlres, datares, wherecontent) {
    $.ajax({
        type: "POST",
        url: urlres,
        data: datares,
        dataType: "html",
        success: function (data) {
            var $block = $(data);

            if ($('.navigation__item--cart--count').text() != "") {
                $(".navigation__item--cart--count").removeClass("navigation__item--cart--count-hidden");
                var count_catalog = $(data).find('.catalog--count').text();
                $('.catalog__cart--count, .navigation__item--cart--count').text(count_catalog);
            }
            else {
                $(".catalog__cart--count").addClass("catalog__cart--count-hidden");
                $(".navigation__item--cart--count").addClass("navigation__item--cart--count-hidden");
            };


        },
        error: function (err) {
            console.log(err.message);
        }
    });
}


$(document).ready(function () {

    $('.catalog__detail__element__size').on('click', function () {
        $(this).parents('.catalog__detail__element__offers-block').find('.catalog__detail__element__offer').removeClass('catalog__detail__element__offer--active');
        $(this).parents('.catalog__detail__element__offer').addClass('catalog__detail__element__offer--active');
    });

    $('.add2cart').click(function () {
        var offer_id = $(this).siblings('.obertka').find('.catalog__detail__element__offer.catalog__detail__element__offer--active.detail').attr('id');
        if (offer_id === undefined) {
            offer_id = $(this).parents('.catalog__detail__element__description').find('.catalog__item__offer.catalog__item__offer--active.catalog__item__offer--single').attr('id');

        }
        var image_orig = $(".element__detail").find(".catalog__detail__element__image");
        var image = image_orig.clone();

        image.css({
            'position': 'absolute',
            'z-index': '11100',
            'min-width': '0',
            top: image_orig.offset().top,
            left: image_orig.offset().left
        })
            .appendTo("body")
            .animate({
                opacity: 0.05,
                left: $(".navigation__item--cart--wrapper").offset()['left'],
                top: $(".navigation__item--cart--wrapper").offset()['top'],
                width: 20, height: 20
            }, 500, function () {
                image.remove();
            });

        var addbasketid = $(this).attr('id');
        var add_id = addbasketid + offer_id;
        ajaxpostshow("/ajax/addToCart.php", add_id, ".basket");
        Informer.success('Товар успешно добавлен в корзину');
        return false;
    });

});

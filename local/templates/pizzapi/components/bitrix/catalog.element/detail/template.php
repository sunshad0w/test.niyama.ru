<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$GLOBALS['PARTNERS_DATA'] = false;
$GLOBALS['PARTNERS_DATA'] = $arResult;



$this->setFrameMode(true);
$templateLibrary = array('popup');
$noImage = '/local/templates/pizzapi/components/bitrix/catalog.element/.default/images/no_photo.png';
?>

<?
// Изображение блюда
$arDetailPicture = $arResult['DETAIL_PICTURE']? CFile::GetFileArray($arResult['DETAIL_PICTURE']['ID']) : array();
$arPreviewPicture = $arResult['DETAIL_PICTURE']? CFile::GetFileArray($arResult['DETAIL_PICTURE']['ID']) : array();
if(!empty($arDetailPicture['SRC'])) {
    $arTmpImg = CFile::ResizeImageGet(
        $arPreviewPicture,
        array(
            'width' => 340,
            'height' => 340
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
        true
    );
    if($arTmpImg) {
        $arPreviewPicture = array(
            'SRC' => $arTmpImg['src'],
            'WIDTH' => $arTmpImg['width'],
            'HEIGHT' => $arTmpImg['height'],
        );
    }
    $arTmpImg = CFile::ResizeImageGet(
        $arDetailPicture,
        array(
            'width' => 800,
            'height' => 800
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
        true
    );
    if($arTmpImg) {
        $arDetailPicture = array(
            'SRC' => $arTmpImg['src'],
            'WIDTH' => $arTmpImg['width'],
            'HEIGHT' => $arTmpImg['height'],
        );
    }
}

$arResult['DETAIL_PICTURE'] = ($arDetailPicture['SRC'])? $arDetailPicture : $arResult['DETAIL_PICTURE'];
$arResult['PREVIEW_PICTURE'] = ($arPreviewPicture['SRC'])? $arPreviewPicture : $arResult['PREVIEW_PICTURE'];

?>

<div class="element__detail" id="<? echo $arItemIDs['ID']; ?>" itemscope itemtype="http://schema.org/Product">
    <div class="container">
        <div class="element__detail__wrapper detail">
            <div class="catalog__detail__element detail" data-id="<? echo $arResult['ID']; ?>">
                <div class="catalog__detail__element__image detail">
                    <a href="<?= (is_null($arResult['DETAIL_PICTURE']['SRC'])) ? $noImage : $arResult['DETAIL_PICTURE']['SRC']; ?>" data-fancybox href="javascript:;">
                        <img itemprop="image" src="<?= (is_null($arResult['PREVIEW_PICTURE']['SRC'])) ? $noImage : $arResult['PREVIEW_PICTURE']['SRC']; ?>"/>
                    </a>
                </div>


                <div class="catalog__detail__element__description detail">
                    <h1 class="catalog__detail__element__title detail" itemprop="name"><? echo $arResult['NAME']; ?></h1>
                    <div class="catalog__detail__element__text detail "  itemprop="description">
                        <? if($arResult['DETAIL_TEXT'] != '0') {
                            echo $arResult['DETAIL_TEXT'];
                        }?>
                    </div>

                    <div class="catalog__detail__element__offers-block detail">
                        <div class="obertka">
                        <? if (isset($arResult['OFFERS'])): ?>
                            <? if ($arResult['PROPERTIES']['PIZZA_TRUE']['VALUE'] == 1): ?>
                                <? if (count($arResult['OFFERS']) > 1): ?>
                                    <? foreach ($arResult['OFFERS'] as $key => $arOffer): ?>
                                        <?
                                        $price = CPrice::GetBasePrice($arOffer['ID']);
                                        $active = [
                                            0 => ['class' => 'catalog__detail__element__size__left', 'active' => 'catalog__detail__element__offer--active'],
                                            1 => ['class' => 'catalog__detail__element__size__right', 'active' => ''],
                                        ];
                                        ?>
                                        <div class="catalog__detail__element__offer <?= $active[$key]['active'] ?> detail"
                                             id="<?= $arOffer['ID'] ?>"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                            <div class="catalog__detail__element__price detail">
                                                <div><?= $arOffer['CATALOG_WEIGHT']; ?>&nbsp;<span>г</span></div>
                                                <div itemprop="price"><?= round($price['PRICE']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="qqqq catalog__detail__element__size <?= $active[$key]['class'] ?> detail">
                                                <div></div>
                                                <?= $arOffer['CATALOG_LENGTH']; ?> cм
                                            </div>
                                        </div>
                                    <? endforeach ?>
                                <?else:?>

                                    <?$price = CPrice::GetBasePrice($arResult['OFFERS'][0]['ID']);?>
                                    <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single detail"
                                         id="<?= $arResult['OFFERS'][0]['ID'] ?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

                                        <div class="catalog__item__price catalog__item__price--popup detail">
                                            <div class="height36">
                                                <? if ($arResult['OFFERS'][0]['CATALOG_WEIGHT'] != 0) {echo $arResult['OFFERS'][0]['CATALOG_WEIGHT'];}?> Грамм
                                            </div>
                                            <div itemprop="price"><?= round($price['PRICE']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>

                                        </div>
                                        <div class="catalog__item__size catalog__item__size--single detail">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                            <?= $arResult['OFFERS'][0]['CATALOG_LENGTH']?> cм
                                        </div>
                                    </div>
                                <?endif;?>
                            <?else:?>
                                <?$price = CPrice::GetBasePrice($arResult['OFFERS'][0]['ID']);?>
                                <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single detail"
                                     id="<?= $arResult['OFFERS'][0]['ID'] ?>">

                                    <div class="catalog__item__price catalog__item__price--popup detail">
                                        <div class="height36">
                                            <? if ($arResult['OFFERS'][0]['CATALOG_WEIGHT'] != 0 && !empty($arResult['OFFERS'][0]['CATALOG_WEIGHT'])) {echo $arResult['OFFERS'][0]['CATALOG_WEIGHT'] . " г";}?>
                                        </div>
                                        <div><?= round($price['PRICE'])?>&nbsp;<span>руб.</span></div>
                                    </div>
                                    <? if($arResult['OFFERS'][0]['CATALOG_LENGTH'] != 0):?>
                                        <div class="catalog__item__size catalog__item__size--single detail">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                            <?= $arResult['OFFERS'][0]['CATALOG_LENGTH']?> cм
                                        </div>

                                    <? endif;?>
                                </div>
                            <?endif;?>
                        <?else:?>
                            <div class="catalog__detail__item__offer--single detail" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <div class="catalog__detail__item__price catalog__item__price--singl detail">
                                    <?= round($arResult['CATALOG_PURCHASING_PRICE']) ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <div class="catalog__detail__item__size--single detail" itemprop="price">
                                        <?= ($arResult['CATALOG_WEIGHT'] != '0')?$arResult['CATALOG_WEIGHT']: '' ?>

                                        <span> грамм</span>
                                    </div>
                                </div>
                            </div>
                        <?endif;?>
                        </div>
                        <div class="addtocart add2cart detail" id="ajaxaction=add&ajaxaddid="
                             rel="nofollow">
                            <a href="<? echo $arElement['ADD_URL']; ?>">
                                <? echo $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

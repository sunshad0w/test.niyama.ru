<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$GLOBALS['PARTNERS_DATA'] = false;
$GLOBALS['PARTNERS_DATA'] = $arResult;



$this->setFrameMode(true);
$templateLibrary = array('popup');
$noImage = '/local/templates/pizzapi/components/bitrix/catalog.element/.default/images/no_photo.png';

?>
<style>
    .element__detail__wrapper > .catalog__detail__element__offers-block {
        width: 265px
    }

    .catalog__detail__element__offers-block {
        margin-top: 15px;
        width: 240px;
        justify-content: center;

    }

    @media (max-width: 768px){
        .catalog__detail__element__offers-block {
            margin:auto;
        }
    }

    .catalog__detail__element__offer {
        display: flex;
        flex-direction: column;
        width: 100%;
        line-height: 40px;
        font-size: 30px;
        color: gray;
        letter-spacing: 2px;
        box-sizing: border-box;
    }

    .catalog__detail__element__offer--active {
        color: black;
    }

    .catalog__detail__element__offer--active > .catalog__detail__element__offer div span {
        color: black;
    }

    .catalog__detail__element__size__left {
        margin-top: 10px;
        width: 100%;
        color: black;
        font-size: 17px;
        height: 43px;
        letter-spacing: 1px;
        border-radius: 200px 0px 0px 200px;
        -moz-border-radius: 200px 0px 0px 200px;
        -webkit-border-radius: 200px 0px 0px 200px;
        border: 3px solid #71752a;
    }

    .catalog__detail__element__offer--active > .catalog__detail__element__size {
        background-color: #71752a;
    }

    .catalog__detail__element__size__right {
        margin-top: 10px;
        width: 100%;
        color: black;
        font-size: 17px;
        height: 43px;
        letter-spacing: 1px;
        border-radius: 0px 200px 200px 0px;
        -moz-border-radius: 0px 200px 200px 0px;
        -webkit-border-radius: 0px 200px 200px 0px;
        border: 3px solid #71752a;
    }

    .catalog__detail__element__size {
        cursor: pointer;
        transition: 0.5s;
    }

    .catalog__detail__element__size span {
        display: block;
    }

    .catalog__detail__element__size img {
        height: 25px;
        margin: 0 10px;
        display: block;
    }

    .catalog__detail__element__offer--active div {
        color: black;
    }

    .catalog__detail__element__offer--active > .catalog__detail__element__size {
        color: #fff;
    }

    .catalog__detail__element__offer--active > .catalog__detail__element__size div {
        background: url("<?= SITE_TEMPLATE_PATH ?>/img/diametr.png") no-repeat;
    }

    .catalog__detail__element__size div {
        margin: 0 11px;
        width: 25px;
        height: 25px;
        background: url("<?= SITE_TEMPLATE_PATH ?>/img/diametrBlack.png") no-repeat;
    }
    .popup-window-close-icon {
        display: block;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 15px;
        right: 15px;
        background: url("/local/templates/pizzapi/img/closepopup.png") no-repeat;
        background-size: contain;
    }

    .popup-window-close-icon:hover {
        width: 20px;
        height: 20px;
        display: block;
        background: url("/local/templates/pizzapi/img/closepopup.png") no-repeat;
        background-size: contain;

    }
    .catalog__item__price.catalog__item__price--popup{
        color: black;
    }
    .fancybox-container .message img{
        width: 100%;
    }

</style>

<?

// Изображение блюда
$arDetailPicture = $arResult['DETAIL_PICTURE']? CFile::GetFileArray($arResult['DETAIL_PICTURE']['ID']) : array();
$arPreviewPicture = $arResult['DETAIL_PICTURE']? CFile::GetFileArray($arResult['DETAIL_PICTURE']['ID']) : array();
if(!empty($arDetailPicture['SRC'])) {
    $arTmpImg = CFile::ResizeImageGet(
        $arPreviewPicture,
        array(
            'width' => 340,
            'height' => 340
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
        true
    );
    if($arTmpImg) {
        $arPreviewPicture = array(
            'SRC' => $arTmpImg['src'],
            'WIDTH' => $arTmpImg['width'],
            'HEIGHT' => $arTmpImg['height'],
        );
    }
    $arTmpImg = CFile::ResizeImageGet(
        $arDetailPicture,
        array(
            'width' => 800,
            'height' => 800
        ),
        BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
        true
    );
    if($arTmpImg) {
        $arDetailPicture = array(
            'SRC' => $arTmpImg['src'],
            'WIDTH' => $arTmpImg['width'],
            'HEIGHT' => $arTmpImg['height'],
        );
    }
}

$arResult['DETAIL_PICTURE'] = ($arDetailPicture['SRC'])? $arDetailPicture : $arResult['DETAIL_PICTURE'];
$arResult['PREVIEW_PICTURE'] = ($arPreviewPicture['SRC'])? $arPreviewPicture : $arResult['PREVIEW_PICTURE'];

?>

<div class="element__detail" id="<? echo $arItemIDs['ID']; ?>">
    <div class="container">
        <div class="element__detail__wrapper">
            <div class="catalog__detail__element" data-id="<? echo $arResult['ID']; ?>">
                <div class="catalog__detail__element__image">
                    <a data-fancybox data-type="ajax"  data-src="<?= (is_null($arResult['DETAIL_PICTURE']['SRC'])) ? $noImage : $arResult['DETAIL_PICTURE']['SRC']; ?>" href="javascript:;">
                        <img src="<?= (is_null($arResult['PREVIEW_PICTURE']['SRC'])) ? $noImage : $arResult['PREVIEW_PICTURE']['SRC']; ?>"/>
                    </a>
                </div>


                <div class="catalog__detail__element__description">
                    <div class="catalog__detail__element__title"><? echo $arResult['NAME']; ?></div>
                    <div class="catalog__detail__element__text">
                        <? if($arResult['DETAIL_TEXT'] != '0') {
                            echo $arResult['DETAIL_TEXT'];
                        }?>
                    </div>

                    <div class="catalog__detail__element__offers-block">
                        <div class="obertka">
                        <? if (isset($arResult['OFFERS'])): ?>
                            <? if ($arResult['PROPERTIES']['PIZZA_TRUE']['VALUE'] == 1): ?>
                                <? if (count($arResult['OFFERS']) > 1): ?>
                                    <? foreach ($arResult['OFFERS'] as $key => $arOffer): ?>
                                        <?
                                            $price = CPrice::GetBasePrice($arOffer['ID']);
                                            $active = [
                                                0 => ['class' => 'catalog__detail__element__size__left', 'active' => 'catalog__detail__element__offer--active'],
                                                1 => ['class' => 'catalog__detail__element__size__right', 'active' => ''],
                                            ];
                                        ?>
                                        <div class="catalog__detail__element__offer <?= $active[$key]['active'] ?>"
                                             id="<?= $arOffer['ID'] ?>">
                                            <div class="catalog__detail__element__price">
                                                <div><?= $arOffer['CATALOG_WEIGHT']; ?>&nbsp;<span>г</span></div>
                                                <div><?= round($price['PRICE']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="qqqq catalog__detail__element__size <?= $active[$key]['class'] ?>">
                                                <div></div>
                                                <?= $arOffer['CATALOG_LENGTH']; ?> cм
                                            </div>
                                        </div>
                                    <? endforeach ?>
                                <?else:?>

                                    <?$price = CPrice::GetBasePrice($arResult['OFFERS'][0]['ID']);?>
                                                <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single"
                                                     id="<?= $arResult['OFFERS'][0]['ID'] ?>">

                                                    <div class="catalog__item__price catalog__item__price--popup">
                                                        <div class="height36">
                                                            <? if ($arResult['OFFERS'][0]['CATALOG_WEIGHT'] != 0) {echo $arResult['OFFERS'][0]['CATALOG_WEIGHT'] . ' г';}?>
                                                        </div>
                                                        <div><?= round($price['PRICE']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>

                                                    </div>
                                                    <div class="catalog__item__size catalog__item__size--single">
                                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                                        <?= $arResult['OFFERS'][0]['CATALOG_LENGTH']?> cм
                                                    </div>
                                                </div>
                                <?endif;?>
                            <?else:?>
                                <?$price = CPrice::GetBasePrice($arResult['OFFERS'][0]['ID']);?>
                                <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single"
                                     id="<?= $arResult['OFFERS'][0]['ID'] ?>">

                                    <div class="catalog__item__price catalog__item__price--popup">
                                        <div class="height36">
                                            <? if ($arResult['OFFERS'][0]['CATALOG_WEIGHT'] != 0) {echo $arResult['OFFERS'][0]['CATALOG_WEIGHT'] . ' г' ;} ?>
                                        </div>
                                        <div><?= round($price['PRICE']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>
                                    </div>
                                    <? if($arResult['OFFERS'][0]['CATALOG_LENGTH'] != 0):?>
                                    <div class="catalog__item__size catalog__item__size--single">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                        <?= $arResult['OFFERS'][0]['CATALOG_LENGTH']?> cм
                                    </div>

                                    <? endif;?>
                                </div>
                            <?endif;?>
                        <?else:?>
                            <div class="catalog__detail__item__offer--single">
                                <div class="catalog__detail__item__price catalog__item__price--singl">
                                    <?= round($arResult['CATALOG_PURCHASING_PRICE']) ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <div class="catalog__detail__item__size--single">
                                        <?= ($arResult['CATALOG_WEIGHT'] != '0')?$arResult['CATALOG_WEIGHT']: '' ?>

                                        <span> грамм</span>
                                    </div>
                                </div>
                            </div>
                        <?endif;?>
                    </div>
                        <div class="addtocart add2cart detail" id="ajaxaction=add&ajaxaddid="
                             rel="nofollow">
                            <a href="<? echo $arElement['ADD_URL']; ?>">
                                <? echo $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>


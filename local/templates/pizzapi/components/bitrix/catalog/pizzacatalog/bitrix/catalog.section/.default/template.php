<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$GLOBALS['PARTNERS_DATA'] = $arResult;
$this->setFrameMode(true);

?>



<? if (!empty($arResult['ITEMS'])) : ?>
    <? foreach ($arResult["ITEMS"] as $cell => $arElement): ?>

        <?
        $title_img = CFile::GetFileArray($arElement['PROPERTIES']['TITLE_IMAGE']['VALUE']);
        $main_img_old = CFile::GetFileArray($arElement['PROPERTIES']['IMAGE']['VALUE']);
        ?>


        <div class="catalog__item" data-id="<? echo $arElement['ID']; ?>">

            <a href="<?= $arElement['DETAIL_PAGE_URL'] ?>">
                <div class="catalog__item__title">
                    <? echo $arElement['NAME']; ?>
                </div>
            </a>
            <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx__catalog__item__link"
               data-id="<? echo $arElement['ID']; ?>" title="<? echo $imgTitle; ?>">
                <div class="catalog__item__image">
                    <img src="<?= $arElement['PREVIEW_PICTURE']['SRC'] ?>"/>
                    <!-- <div class="catalog__item--count">1</div>-->
                </div>
                <div class="catalog__item__text"><?= $arElement['PREVIEW_TEXT'] ?></div>
            </a>


            <div class="catalog__item__offers-block">
                <? if (isset($arElement['OFFERS'])): ?>
                    <? if ($arElement['PROPERTIES']['PIZZA_TRUE']['VALUE'] == 1): ?>
                        <? if (count($arElement['OFFERS']) > 1): ?>
                            <? foreach ($arElement['OFFERS'] as $key => $arOffer): ?>
                                <?
                                $active = [
                                    0 => ['class' => 'catalog__item__size__left', 'active' => 'catalog__item__offer--active'],
                                    1 => ['class' => 'catalog__item__size__right', 'active' => ''],
                                ];
                                ?>
                                <div class="catalog__item__offer <?= $active[$key]['active'] ?>" id="<?= $arOffer['ID'] ?>">
                                    <div class="catalog__item__price">
                                        <div>
                                            <span class="catalog__item__price-weight"><?= $arOffer['CATALOG_WEIGHT'];?>&nbsp;</span><span class="catalog__item__price-measure">г</span>
                                        </div>
                                        <div><?= round($arOffer['CATALOG_PRICE_2']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>
                                    </div>
                                    <div class="catalog__item__size <?= $active[$key]['class'] ?>">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                        <?= $arOffer['CATALOG_LENGTH']?> cм
                                    </div>
                                </div>
                            <? endforeach; ?>
                        <? else: ?>

                            <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single" id="<?=$arElement['OFFERS'][0]['ID'] ?>">
                                <div class="catalog__item__price">
                                    <div>
                                        <? if ($arElement['OFFERS'][0]['CATALOG_WEIGHT'] != 0) : ?>
                                            <span class="catalog__item__price-weight"><?= $arElement['OFFERS'][0]['CATALOG_WEIGHT']; ?>&nbsp;</span><span class="catalog__item__price-measure">г</span>
                                        <? endif; ?>
                                    </div>
                                    <div><?= round($arElement['OFFERS'][0]['CATALOG_PRICE_2']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>

                                </div>
                                <div class="catalog__item__size catalog__item__size--single">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/diametr.png">
                                    <?= $arElement['OFFERS'][0]['CATALOG_LENGTH']?> cм
                                </div>
                            </div>
                        <? endif; ?>
                    <? else: ?>
                       <div class="catalog__item__offer--single ">
                            <div class="catalog__item__offer catalog__item__offer--active catalog__item__offer--single" id="<?=$arElement['OFFERS'][0]['ID'] ?>" >
                                <div class="catalog__item__price">
                                    <div>
                                        <? if ($arElement['OFFERS'][0]['CATALOG_WEIGHT'] != 0) : ?>
                                            <span class="catalog__item__price-weight"><?= $arElement['OFFERS'][0]['CATALOG_WEIGHT']; ?>&nbsp;</span><span class="catalog__item__price-measure">г</span>
                                        <? endif;?>
                                    </div>
                                    <div><?= round($arElement['OFFERS'][0]['CATALOG_PRICE_2']); ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span></div>

                                </div>

                                    <?/* if ($arElement['OFFERS'][0]['CATALOG_LENGTH'] != 0) {
                                    echo '<img src="'. SITE_TEMPLATE_PATH . '/img/diametr.png">';
                                    echo $arElement['OFFERS'][0]['CATALOG_LENGTH'] .  'cм';
                                    }*/?>
                            </div>
                        </div>
                    <? endif; ?>
                <?else:?>
                    <div class="catalog__detail__item__offer--single">
                        <div class="catalog__detail__item__price catalog__item__price--singl">
                            <?= round($arResult['CATALOG_PURCHASING_PRICE']) ?>&nbsp;<span><i class="fa fa-rub" aria-hidden="true"></i></span>
                            <div class="catalog__detail__item__size--single">
                                <?= $arResult['CATALOG_WEIGHT'] ?>
                                <span> грамм</span>
                            </div>
                        </div>
                    </div>
                <?endif;?>
            </div>


            <div class="addtocart add2cart" id="ajaxaction=add&ajaxaddid="
                 rel="nofollow">
                <a href="<? echo $arElement['ADD_URL']; ?>">
                    <? echo $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>
            </div>


        </div>

    <? endforeach; ?>
    <!--NOT DELET-->
    <div class="space-hack"></div>
    <div class="space-hack"></div>
    <div class="space-hack"></div>

<? else: ?>
    <div style="color:#FFFFFF;">ЭТОТ РАЗДЕЛ ПУСТ</div>
<? endif; ?>


<div id="element__detail__wrapper" style="display:none">
</div>
<div class="catalog__section__description"><?=$arResult['DESCRIPTION']?></div>
<? CUtil::InitJSCore(array('window')); ?>
<script>
    $('.catalog__item__size').on('click', function () {
        $(this).parents('.catalog__item__offers-block').find('.catalog__item__offer').removeClass('catalog__item__offer--active');
        $(this).parents('.catalog__item__offer').addClass('catalog__item__offer--active');
    });

    var sendAjax = function (id, oPopup) {
        $.post('/ajax/getCatalogElement.php', {id: id, 'dataType': 'html'},
            function (response) {
                if (response) {
                    oPopup.setContent(response);
                    oPopup.show();
                } else {
                    console.log('ошибка')
                }
            });
    };

    BX.ready(function () {
        var oPopup = new BX.PopupWindow('CatalogElement', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'black', opacity: '80'
            },
            events: {
                //Игорь вот эта сучка работает
                onPopupShow: function () {
                    $('.catalog__detail__element__size').on('click', function () {
                        $(this).parents('.catalog__detail__element__offers-block').find('.catalog__detail__element__offer').removeClass('catalog__detail__element__offer--active');
                        $(this).parents('.catalog__detail__element__offer').addClass('catalog__detail__element__offer--active');
                    })
                    $('.add2cart').click(function () {
                        var offer_id = $(this).first().parents('.catalog__detail__element__description').find('.catalog__detail__element__offer.catalog__detail__element__offer--active').attr('id')
                        if (offer_id === undefined) {
                            offer_id = $(this).parents('.catalog__detail__element__description').find('.catalog__item__offer.catalog__item__offer--active.catalog__item__offer--single').attr('id');

                        }
                        var image_orig = $(this).closest(".catalog__detail__element").find(".catalog__detail__element__image");
                        var image = image_orig.clone();

                        image.css({
                            'position': 'absolute',
                            'z-index': '11100',
                            top: image_orig.offset().top,
                            left: image_orig.offset().left
                        })
                            .appendTo("body")
                            .animate({
                                opacity: 0.05,
                                left: $(".catalog__navigation__item--cart").offset()['left'],
                                top: $(".catalog__navigation__item--cart").offset()['top'],
                                width: 20, height: 20
                            }, 500, function () {
                                image.remove();
                            });

                        var addbasketid = $(this).attr('id');
                        var add_id = addbasketid + offer_id;
                        ajaxpostshow("/ajax/addToCart.php", add_id, ".basket");
                        return false;
                    });


                    $(document).on('onComplete.fb', function( e, instance, slide ) {
                        $(".fancybox-container").click(function(e){
                            e.stopPropagation();
                        });
                    });

                    $(".catalog__detail__element__image").on('click', function() {
                        // $.fancybox.open('<div class="message"><img src="/upload/iblock/6c8/6c8a1beb35c211905bb1b54e4070448b.jpg"><p>You are awesome!</p></div>');
                        var src = $(".catalog__detail__element__image a").data('src');
                        $.fancybox.open('<div class="message"><img src=" '+ src +'"><p></p></div>');
                    });

                }
            }
        });

        BX.bindDelegate(
            document.body, 'click', {className: 'bx__catalog__item__link'},
            BX.proxy(function (e) {
                if (!e)
                    e = window.event;
                var elem;
                if ($(e.target).hasClass('bx__catalog__item__link')) {
                    elem = $(e.target);
                } else {
                    elem = $(e.target).parents('.bx__catalog__item__link')
                }
                BX.PreventDefault(e);
                sendAjax(elem.data('id'), this);
            }, oPopup)
        );


    });


    /* Function for ours ajax inquiry  */
    function ajaxpostshow(urlres, datares, wherecontent) {
        $.ajax({
            type: "POST",
            url: urlres,
            data: datares,
            dataType: "html",
            success: function (data) {
                var $block = $(data);
                $('.catalog__cart--open').html($(data).find(".catalog__cart--open__wrapper").html());
                if ($('.catalog__cart--count').text() != "") {
                    $(".catalog__cart--count").removeClass("catalog__cart--count-hidden");
                    $(".navigation__item--cart--count").removeClass("navigation__item--cart--count-hidden");
                    var count_catalog = $(data).find('.catalog--count').text();
                    $('.catalog__cart--count, .navigation__item--cart--count').text(count_catalog);
                }
                else {
                    $(".catalog__cart--count").addClass("catalog__cart--count-hidden");
                    $(".navigation__item--cart--count").addClass("navigation__item--cart--count-hidden");
                };
                var price = $('#summary_price').data('price');

            },
            error: function (err) {
                console.log(err.message);
            }
        });
    }


    $('.add2cart').click(function () {
        var offer_id = $(this).first().parents('.catalog__item').find('.catalog__item__offer.catalog__item__offer--active').attr('id')
        if (offer_id === undefined) {
            offer_id = $(this).parents('.catalog__item').data('id');

        }
        // анимашка для корзины ////////////////////////////////////////////////
        var image_orig = $(this).closest(".catalog__item").find(".catalog__item__image");
        var image = image_orig.clone();
        image.css({
            'position': 'absolute',
            'z-index': '11100',
            top: image_orig.offset().top,
            left: image_orig.offset().left
        })
            .appendTo("body")
            .animate({
                opacity: 0.05,
                left: $(".catalog__navigation__item--cart").offset()['left'],
                top: $(".catalog__navigation__item--cart").offset()['top'],
                width: 20, height: 20
            }, 500, function () {
                image.remove();
            });
        /////////////////////////////////////////////////////////////////////////

        var addbasketid = $(this).attr('id');
        var add_id = addbasketid + offer_id;
        ajaxpostshow("/ajax/addToCart.php", add_id, ".basket");
        return false;
    });

    /* Inquiry ajax at change of quantity of the goods */
    $(".basket  .basket-count-update").change(function (e) {
        var countbasketid = $(this).attr('id');
        var countbasketcount = $(this).val();
        var ajaxcount = countbasketid + '&ajaxbasketcount=' + countbasketcount;
        ajaxpostshow("/ajax/addToCart.php", ajaxcount, ".basket");
        return false;
    });

    $(document).ready(function () {
        $(".qqqq").on('click', function () {

        });
      $(".catalog__navigation__item--cart--toggle, .catalog__cart__hide").on('click', function () {
            if ($(".catalog__cart--open").css('display') == 'none') {
                $(".catalog__cart--open").fadeIn(500);
            } else {
                $(".catalog__cart--open").fadeOut(500);
            }
        });
    });

</script>

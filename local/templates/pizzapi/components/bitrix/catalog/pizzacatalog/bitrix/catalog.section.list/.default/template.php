<? if (!empty($arResult['SECTIONS'])) : ?>
    <? foreach ($arResult['SECTIONS'] as &$arSection): ?>
        <?
        $main_section = "/catalog/pizza/";
        if ($APPLICATION->GetCurPage(false) === "/" && $arSection['SECTION_PAGE_URL'] === "/catalog/pizza/") {
            $active = "catalog__navigation__item--active";
        } elseif ($APPLICATION->GetCurPage(false) === $arSection['SECTION_PAGE_URL']) {
            $active = "catalog__navigation__item--active";
        } else {
            $active = "";
        }
        ?>
        <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
           class="catalog__navigation__item <? echo($active) ?>"><? echo $arSection['NAME']; ?></a>
    <? endforeach; ?>
    <div class="catalog__navigation__item catalog__navigation__item--cart">
        <div class="catalog__navigation__item--cart--toggle">
            <img src="/local/templates/pizzapi/img/cart.png">
        </div>


        <? $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "pizzabasketpopup",
            Array(
                "HIDE_ON_BASKET_PAGES" => "Y",
                "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                "POSITION_FIXED" => "Y",
                "SHOW_IMAGE" => "Y",
                "SHOW_NOTAVAIL" => "N",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_PRICE" => "Y",
                "SHOW_PRODUCTS" => "Y",
                "SHOW_SUBSCRIBE" => "Y",
                "SHOW_SUMMARY" => "Y",
                "SHOW_TOTAL_PRICE" => "Y"
            )
        ); ?>

    </div>


<? else: ?>
    <div>нет разделов</div>
<? endif; ?>



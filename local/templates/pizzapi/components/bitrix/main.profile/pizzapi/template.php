<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?><? //ShowError($arResult["strProfileError"]);
//var_dump($arResult['POST']);
//var_dump($arResult['strProfileError']);
//if (isset($arResult['POST']) && count($arResult['POST'])) {
    if (isset($arResult['strProfileError']) && $arResult['strProfileError']) {
        echo json_encode(array(
            'errors' => $arResult['strProfileError']
        ));
        return;
    }
//}
?>
<? if ($arResult['DATA_SAVED'] == 'Y') { ?>
        <?
        $arResizedFile = CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array("width"=>100, "height"=>100), BX_RESIZE_IMAGE_EXACT, false, false, true, false)
        ?>
        <div class="profile-data__ava">
            <img src="<?=$arResizedFile['src']?>"/> 
        </div>
        <div class="profile-data__text">
            <div class="profile-data__name">
                <?= $arResult["arUser"]['NAME'] ?>              <?= $arResult["arUser"]['LAST_NAME'] ?>
            </div>
            <div class="profile-data__email">
                <?= $arResult["arUser"]['EMAIL'] ?>
            </div>
            <div class="profile-data__phone">
                <?= $arResult["arUser"]['PERSONAL_PHONE'] ?>
            </div>
        </div>

    <? return;
} ?>


<div class="container">
    <div class="profile-popup">
        <h1>Личные данные</h1>

        <div id="profilePopupErrors"></div>
        <script type="text/javascript">
            <!--
            var opened_sections = [<?
            $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
            $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
            if (strlen($arResult["opened"]) > 0)
            {
                echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
            } else {
                $arResult["opened"] = "reg";
                echo "'reg'";
            }
            ?>];
            //-->

            var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
        </script>

        <form method="post"  
              name="profile-form-popup" 
              id="profile-form-popup" 
              action="<?=$arResult["FORM_TARGET"]?>" 
              enctype="multipart/form-data"
              class="profile-popup__form">

            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Аватар</label>

                        <div class="profile-popup__ava">
                            <? if ($arResult["arUser"]['PERSONAL_PHOTO']) : ?>
                                <? $arResizedFile = CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array("width"=>100, "height"=>100), BX_RESIZE_IMAGE_EXACT, false, false, true, false) ?>
                                <img src="<?=$arResizedFile['src']?>" id="personalPhotoImg"/>
                                <div class="deleteAva" title="Удалить аватар" onclick="delAvatar(this);return false;"></div>
                            <? else : ?>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkNDQ0ZGRUQ1MUY1ODExRTc5NzU0QTE2OEMwN0FFQTIxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkNDQ0ZGRUQ2MUY1ODExRTc5NzU0QTE2OEMwN0FFQTIxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Q0NDRkZFRDMxRjU4MTFFNzk3NTRBMTY4QzA3QUVBMjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Q0NDRkZFRDQxRjU4MTFFNzk3NTRBMTY4QzA3QUVBMjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7385oHAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAAAxJREFUeNpiYAAIMAAAAgABT21Z4QAAAABJRU5ErkJggg==" id="personalPhotoImg" style="background: transparent"/>
                                <div class="deleteAva" title="Удалить аватар" onclick="delAvatar(this);return false;" style="display: none;"></div>
                            <? endif ?>

                            <input name="PERSONAL_PHOTO" id="personalPhotoInput" class="typefile" size="20" type="file" title="Установить аватар">
                            <input name="PERSONAL_PHOTO_del" type="hidden" id="deletePersonalPhoto">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Имя</label>
                        <input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Фамилия</label>
                        <input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Дата рождения</label>
                        <div class="profile-calendar">

                                <? if(is_null($arResult['arUser']['PERSONAL_BIRTHDAY'])):?>
                                        <?$APPLICATION->IncludeComponent(
                                            'bitrix:main.calendar',
                                            '',
                                            array(
                                                'SHOW_INPUT' => 'Y',
                                                'FORM_NAME' => 'profile-form-popup',
                                                'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                                'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                                'SHOW_TIME' => 'N'
                                            ),
                                            null,
                                            array('HIDE_ICONS' => 'Y')
                                        );

                                    //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "profile-form-popup", "15")
                                ?>
                            <? else:?>
                                   <span class='disabled_input' name="PERSONAL_BIRTHDAY"><?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?></span>
                                <?endif;?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Телефон</label>
                        <input type="text" name="PERSONAL_PHONE" maxlength="50" class="popup__personal_phone" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">E-mail</label>
                        <span class='disabled_input' name="EMAIL"><?=$arResult["arUser"]["EMAIL"]?></span>
                    </div>
                </div>
            </div>
            <div class="profile-popup__separator">
            </div>

                      <div class="profile-popup__save-btn">
                <input type="submit" name="save" value="Сохранить">
            </div>
        </form>
    </div>
</div>

        


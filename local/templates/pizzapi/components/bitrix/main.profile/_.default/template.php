<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?><style type="text/css">
    .profile-popup {
        padding: 10px 40px;
    }

    .profile-popup h1 {
        font-size: 38px;
        text-transform: uppercase;
    }
    .profile-popup__input {
        margin: 0 0 10px;
    }
    .profile-popup__label {
        display: block;
        width: 100%;
        font-size: 16px;
    }
    .profile-popup__input input {
        display: block;
        width: 100%;
        font-size: 16px;
        border: 2px solid black;
    }
    .profile-calendar {
        position: relative;
    }
    .calendar-icon {
        position: absolute;
        right: 3px;
        top: 3px;
    }
    .profile-popup__save-btn input {
        display: block;
        width: 260px;
        heigth: 54px;
        line-height: 54px;
        color: white;
        background: #dd401d;
        border-radius: 50%;
        border: none;
        outline: none
    }
</style>

<div class="container">
    <div class="profile-popup">
        <h1>Личные данные</h1>

        <? ShowError($arResult["strProfileError"]); ?>
        <? if ($arResult['DATA_SAVED'] == 'Y') {
            ShowNote(GetMessage('PROFILE_DATA_SAVED'));
        } ?>
        <script type="text/javascript">
            <!--
            var opened_sections = [<?
            $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
            $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
            if (strlen($arResult["opened"]) > 0)
            {
                echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
            } else {
                $arResult["opened"] = "reg";
                echo "'reg'";
            }
            ?>];
            //-->

            var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
        </script>

        <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">

            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Имя</label>
                        <input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Фамилия</label>
                        <input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Дата рождения</label>
                        <div class="profile-calendar">
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                    'SHOW_INPUT' => 'Y',
                                    'FORM_NAME' => 'form1',
                                    'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
                                    'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
                                    'SHOW_TIME' => 'N'
                                    ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                                );

                                //=CalendarDate("PERSONAL_BIRTHDAY", $arResult["arUser"]["PERSONAL_BIRTHDAY"], "form1", "15")
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Телефон</label>
                        <input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">E-mail</label>
                        <input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" />
                    </div>
                </div>
            </div>
            <div class="profile-popup__separator">
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Город</label>
                        <input type="text" name="PERSONAL_CITY" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Станция метро</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Улица</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Дом</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Квартира</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Подъезд</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Домофон</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="profile-popup__input">
                        <label class="profile-popup__label">Этаж</label>
                        <input type="text" name="" maxlength="50" value="" />
                    </div>
                </div>
            </div>
            <div class="profile-popup__save-btn">
                <input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
            </div>
        </form>
    </div>
</div>
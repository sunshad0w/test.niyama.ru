<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
    if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
    {
        if(strlen($arResult["REDIRECT_URL"]) > 0)
        {

            $APPLICATION->RestartBuffer();
            ?>          <script type="text/javascript">
                //window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
            </script>
            <?
            die();
        }

    }
}


$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
?>

<?
//var_dump ($templateFolder."/confirm.php");
?>
<a name="order_form"></a>

<div id="order_form_div" class="order-checkout">
<NOSCRIPT>
    <div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
    function getColumnName($arHeader)
    {
        return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
    }
}

if (!function_exists("cmpBySort"))
{
    function cmpBySort($array1, $array2)
    {
        if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
            return -1;

        if ($array1["SORT"] > $array2["SORT"])
            return 1;

        if ($array1["SORT"] < $array2["SORT"])
            return -1;

        if ($array1["SORT"] == $array2["SORT"])
            return 0;
    }
}
?>
    <div class="order__header">
        <h4 class="order__title"><?=GetMessage("SOA_TEMPL_PROP_INFO")?></h4>
        <a href="/personal/cart/" alt ="назад" class="order__back__link">Назад</a>
    </div>
<div class="bx_order_make">

    <?
    if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
    {
        if(!empty($arResult["ERROR"]))
        {
            foreach($arResult["ERROR"] as $v)
                echo ShowError($v);
        }
        elseif(!empty($arResult["OK_MESSAGE"]))
        {
            foreach($arResult["OK_MESSAGE"] as $v)
                echo ShowNote($v);
        }

        include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
    }
    else
    {
        if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
        {
            if(strlen($arResult["REDIRECT_URL"]) == 0)
            {
                include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
            }
        }
        else
        {
            ?>
            <script type="text/javascript">

            <?if(CSaleLocation::isLocationProEnabled()):?>

                <?
                // spike: for children of cities we place this prompt
                $city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
                ?>

                BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
                    'source' => $this->__component->getPath().'/get.php',
                    'cityTypeId' => intval($city['ID']),
                    'messages' => array(
                        'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
                        'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
                        'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
                            '#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
                            '#ANCHOR_END#' => '</a>'
                        )).'</div>'
                    )
                ))?>);

            <?endif?>


            var BXFormPosting = false;
            function submitForm(val)
            {

                if (BXFormPosting === true)
                    return true;

                BXFormPosting = true;
                if(val != 'Y')
                    BX('confirmorder').value = 'N';

                var orderForm = BX('ORDER_FORM');
                BX.showWait();

                <?if(CSaleLocation::isLocationProEnabled()):?>
                    BX.saleOrderAjax.cleanUp();
                <?endif?>

                BX.ajax.submit(orderForm, ajaxResult);

                return true;
            }

            function ajaxResult(res)
            {
                var orderForm = BX('ORDER_FORM');
                try
                {

                    // if json came, it obviously a successfull order submit

                    var json = JSON.parse(res);
                    BX.closeWait();

                    if (json.error)
                    {
                        BXFormPosting = false;
                        return;
                    }
                    else if (json.redirect)
                    {
                    //window.top.location.href = json.redirect;
                    sendAjaxConfirm(json.redirect);

                    }
                }
                catch (e)
                {

                    // json parse failed, so it is a simple chunk of html

                    BXFormPosting = false;
                    BX('order_form_content').innerHTML = res;

                    <?if(CSaleLocation::isLocationProEnabled()):?>
                        BX.saleOrderAjax.initDeferredControl();
                    <?endif?>
                }

                BX.closeWait();
                BX.onCustomEvent(orderForm, 'onAjaxSuccess');
            }

            function SetContact(profileId)
            {
                BX("profile_change").value = "Y";
                submitForm();
            }
            </script>
            <?if($_POST["is_ajax_post"] != "Y")
            {
                ?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
                <?=bitrix_sessid_post()?>
                <div id="order_form_content">
                <?
            }
            else
            {
                $APPLICATION->RestartBuffer();
            }

            if($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
            {
                ?>
                <input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
                <?
            }
            

            if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
            {
                foreach($arResult["ERROR"] as $v)
                    echo ShowError($v);
                ?>
                <script type="text/javascript">
                    top.BX.scrollToNode(top.BX('ORDER_FORM'));
                </script>
                <?
            }

            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");                
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props_for_payment.php");        

            if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
                echo $arResult["PREPAY_ADIT_FIELDS"];
            ?>

            <?if($_POST["is_ajax_post"] != "Y")
            {
                ?>
                    </div>
                    <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                    <input type="hidden" name="profile_change" id="profile_change" value="N">
                    <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                    <input type="hidden" name="json" value="Y">
                    <div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="order__makeorder__button "><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>
                </form>
                <?
                if($arParams["DELIVERY_NO_AJAX"] == "N")
                {
                    ?>
                    <div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
                    <?
                }
            }
            else
            {
                ?>
                <script type="text/javascript">
                    top.BX('confirmorder').value = 'Y';
                    top.BX('profile_change').value = 'N';
                </script>
                <?
                die();
            }
        }
    }
    ?>
    </div>
</div>

<?if(CSaleLocation::isLocationProEnabled()):?>

    <div style="display: none">
        <?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.steps", 
            ".default", 
            array(
            ),
            false
        );?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:sale.location.selector.search", 
            ".default", 
            array(
            ),
            false
        );?>
    </div>

<?endif?>




<?CUtil::InitJSCore(array('ajax', 'window', 'popup'));?>

<script>

    BX.ready(
        function () {
            var oPopupConfirm = new BX.PopupWindow('confirm', window.body, {
                autoHide: true,
                offsetTop: 1,
                offsetLeft: 0,
                lightShadow: true,
                closeIcon: true,
                closeByEsc: true,
                overlay: {
                    backgroundColor: 'black', opacity: '80'
                },
                events: {
                    onPopupClose: function () {
                        window.location.href = "/";
                    }
                }

            });


            window.sendAjaxConfirm = function (url) {

                $.post(url, {'dataType': 'html'},
                    function (response) {
                        if (response) {
                            console.log($(response).find('.bx_order_make'));
                            $(response).find('.bx_order_make');
                            dataPopup = $(response).find('.bx_order_make').html();
                            oPopupConfirm.setContent(dataPopup);
                            oPopupConfirm.show();

                        } else {
                            console.log('ошибка')
                        }
                    });
            };
        }
    );


document.body.addEventListener('DOMSubtreeModified', function () {
    $(".CUSTOMER_PHONE input[type='text']").mask("+7 (999) 999-99-99");
}, false);



 $(document).on('keypress', ".CUSTOMER_NAME input[type='text']", function (e) {
     return inputOnlyLetters(e);
 });
 $(document).on('change', ".CUSTOMER_NAME input[type='text']", function () {
     this.value = protectFromWrongPaste(this.value,'onlyLetters');
 });

 $(document).on('keypress', ".QUANTITY_PERSON input[type='text']", function (e) {
     return inputOnlyNumbers(e);
 });
 $(document).on('change', ".QUANTITY_PERSON input[type='text']", function () {
     this.value = protectFromWrongPaste(this.value,'onlyNumbers');
 });
 $(document).on('keypress', ".DELIVERY_APART input[type='text']", function (e) {
     return inputOnlyNumbers(e);
 });
 $(document).on('change', ".DELIVERY_APART input[type='text']", function () {
     this.value = protectFromWrongPaste(this.value,'onlyNumbers');
 });

</script>
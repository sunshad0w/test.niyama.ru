<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
?><div class="section">
	<h4 class="order__title order__title_micro">Адреса доставки</h4>
	<?
	$bHideProps = true;

	if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"])):
		if ($arParams["ALLOW_NEW_PROFILE"] == "Y"):
		?>
			<div class="bx_block r3x1">

				<? foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) { ?>
					<?
					$value = false;
					$dbPropVals = CSaleOrderUserPropsValue::GetList( // сорян, но думать не хотелось
						array("SORT" => "ASC"),
						array("USER_PROPS_ID" => $arUserProfiles["ID"],
								"CODE" => 'DELIVERY_ADDRESS',),
						false,
						false,
						array("ID", "ORDER_PROPS_ID", "VALUE", "SORT", "CODE")
					);
					if ($arPropVals = $dbPropVals->GetNext()) {
						$value = $arPropVals["VALUE"];
					} 
					if (!$value) {
						continue;
					}
					?>
					<div class="order__adress-item">
					    <input type="radio" 
					           class="radio" 
					           id="PROFILE_ID_<?= $arUserProfiles['ID'] ?>" 
					           name="PROFILE_ID"
					           value="<?= $arUserProfiles["ID"] ?>" 
					           <?if ($arUserProfiles["CHECKED"]=="Y") echo " checked=\"checked\" ";?>
					           onclick="SetContact(<?= $arUserProfiles["ID"] ?>)">
				        <label for="PROFILE_ID_<?= $arUserProfiles['ID'] ?>" 
				               onclick="BX('PROFILE_ID_<?= $arUserProfiles['ID'] ?>').checked=true;SetContact(<?= $arUserProfiles["ID"] ?>);" 
				               class="order__delivery_radiobutton_title">
				            <div class="bx_description">
				                <div class="name"><strong>
				                	<?= $value ?>
				                	<!--<?= $arUserProfiles["NAME"] ?>-->
				                </strong></div>
				            </div>
				        </label>
				    </div>
			    <? } ?>
				<input type="radio" 
			           style="display:none;" 
			           id="PROFILE_ID_0"
			           name="PROFILE_ID"
			           value="0"
			           onclick="SetContact('0')">
			    <div class="order__adress-item">
			    	<a href="" onclick="BX('PROFILE_ID_0').checked=true;SetContact('0'); return false;">
			    		Добавить адрес
			    	</a>
				</div>	

				<?/*select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
					<option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
					<?
					foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
					{
						?>



						<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
						<?
					}
					?>
				</select*/?>
				<div style="clear: both;"></div>
			</div>
		<?
		else:
		?>
			<div class="bx_block r1x3">
				<?=GetMessage("SOA_TEMPL_EXISTING_PROFILE")?>
			</div>
			<div class="bx_block r3x1">
					<?
					if (count($arResult["ORDER_PROP"]["USER_PROFILES"]) == 1)
					{
						foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
						{
							echo "<strong>".$arUserProfiles["NAME"]."</strong>";
							?>
							<input type="hidden" name="PROFILE_ID" id="ID_PROFILE_ID" value="<?=$arUserProfiles["ID"]?>" />
							<?
						}
					}
					else
					{
						?>
						<select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
							<?
							foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
							{
								?>
								<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
								<?
							}
							?>
						</select>
						<?
					}
					?>
				<div style="clear: both;"></div>
			</div>
		<?
		endif;
	else:
		$bHideProps = false;
	endif;
	?>
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);


$arSelect = array("ID", "IBLOCK_ID", 'PROPERTY_LONG_ALT', 'PROPERTY_LAT_ALT');
$arFilter = array(
    'IBLOCK_ID' => 2,
);


$id = null;
$restouran = null;
$restouran = $_REQUEST['ELEMENT_CODE'];
foreach ($arResult["ITEMS"] as $arItem) {
    if ($restouran == $arItem['CODE']) {
        $id = $arItem['PROPERTIES']['R_RESTAURANT']['VALUE'];
    }
}

if(!empty($_REQUEST['ELEMENT_CODE']) && empty($id)){
    if (!defined("ERROR_404"))
        define("ERROR_404", "Y");

    \CHTTP::setStatus("404 Not Found");

    if ($APPLICATION->RestartWorkarea()) {
        require(\Bitrix\Main\Application::getDocumentRoot()."/404.php");
        die();
    }
}
$rs = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
$coordinates = array();
while ($ar = $rs->GetNext()) {
    if ($restouran) {
        if ($ar['ID'] == $id) {
            $coordinates[$ar['ID']] = array('long' => $ar['PROPERTY_LONG_ALT_VALUE'], 'lat' => $ar['PROPERTY_LAT_ALT_VALUE']);
        }
    } else {
        $coordinates[$ar['ID']] = array('long' => $ar['PROPERTY_LONG_ALT_VALUE'], 'lat' => $ar['PROPERTY_LAT_ALT_VALUE']);
    }
}

?>

<script type="application/javascript">
    window.coords = <?=json_encode($coordinates)?>;
</script>

<div class="news-list">
    <div class="restourant__item__list">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <div class="restourant__item">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="restourant__item__link">
                    <div class="restourant__image"><img src="/upload/iconrestoran.png"/></div>
                    <div class="restourant__item__description">
                        <div class="restourant__name"><? echo $arItem["NAME"] ?></div>
                        <div class="restourant__adress">
                            Адрес: <? echo $arItem["PROPERTIES"]["S_ADDRESS"]["VALUE"] ?></div>
                        <div class="restourant__phone">Телефон: <? echo $arItem["PROPERTIES"]["S_TEL"]["VALUE"] ?></div>
                    </div>
                </a>
            </div>
            <? foreach ($arItem["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                <small style="display: none;">
                    <span class="r_restaurant_id"><?= $arProperty['VALUE'] ?></span>
                    <?= $arProperty["NAME"] ?>:&nbsp;
                    <? if (is_array($arProperty["DISPLAY_VALUE"])): ?>
                        <?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
                    <? else: ?>
                        <?= $arProperty["DISPLAY_VALUE"]; ?>
                    <? endif ?>
                </small>
            <? endforeach; ?>

        <? endforeach; ?>


    </div>
    <div class="maps__detail">
        <div id="map" style="max-height: 450px; flex-grow: 1">
            <div class="loader-container">
                <div class="loader-spinner"></div>
            </div>
        </div>
        <div class="detail__restoran">
            <? if (is_null($restouran)): ?>
                <?= $arResult['DESCRIPTION'] ?>
            <? else: ?>
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <? if ($restouran == $arItem['CODE']): ?>
                        <?
                       $APPLICATION->SetTitle($arItem['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
                        echo $arItem['DETAIL_TEXT']; ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
    <script type="application/javascript">
        ymaps.ready(init);
        var map;
        function init() {
            var center_coord;
            if (Object.keys(window.coords).length > 1) {
                center_coord = {
                    center: [55.75, 37.64],
                    zoom: 9
                }
            } else {
                var current_rest = window.coords[Object.keys(window.coords)[0]];
                center_coord = {
                    center: [current_rest.lat, current_rest.long],
                    zoom: 13
                }
            }
            var map = new ymaps.Map("map", center_coord);

            var MyHintLayoutClass = ymaps.templateLayoutFactory.createClass(
                '<div class="my-layout" style="padding: 8px;">' +
                '<span style="white-space: pre; font-size: 14px;">{{ properties.hintContent }}</span>' +
                '</div>'
            );

            map.geoObjects.options.set({
                hintContentLayout: MyHintLayoutClass
//            iconLayout: MyIconLayoutClass, // макет иконок всех объектов карты
//            balloonLayout: MyBalloonLayoutClass, // макет балунов всех объектов карты
//            clusterBalloonLayout: // макет балунов кластеров
            });

            var PlacemarkHintLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="placemark_hint_layout" style="padding: 8px;">' +
                '<div style="font-weight: bold">{{ properties.name }}</div>' +
                '<div >{{ properties.adress }}</div>' +
                '<div >{{ properties.phone }}</div>' +
                '</div>'
            );


            $('.r_restaurant_id').each(function () {
                var $rest_id = parseInt($(this).text());

                if (!window.coords[$rest_id]) {
                    return
                }

                var rest_dom = $(this).parent('small').prev();
                var name = rest_dom.find('.restourant__name').html();
                var adress = rest_dom.find('.restourant__adress').html();
                var phone = rest_dom.find('.restourant__phone').html();
                var id = $(this).text();

                var rest = new ymaps.Placemark([coords[id].lat, coords[id].long],
                    {
                        name: name,
                        adress: adress,
                        phone: phone
                    },
                    {
                        iconLayout: 'default#image',
                        iconImageHref: '/upload/pointer.png',
                        iconImageSize: [30, 42],
                        iconImageOffset: [-3, -42],
                        hintContentLayout: PlacemarkHintLayout
                    });
                map.geoObjects.add(rest);
            });

            window.map = map;
            //map.geoObjects.add(rest1);
        }

        var fly = function (obj) {
            var top = $("#map").offset().top - $('header').height() - 30;
            $('html, body').animate({
                scrollTop: top
            }, ($('body').scrollTop() <= top) ? 0 : 500, 'swing', function () {
                var id = $(obj).next().find('.r_restaurant_id').text();
                map.panTo([parseFloat(coords[id].lat), parseFloat(coords[id].long)],
                    {
                        flying: true,
                        duration: 1000,
                        //checkZoomRange: true,
                    }
                ).then(function () {
                    if (map.getZoom() <= 11) map.setZoom(15, {duration: 2000});
                });
            });

        };


    </script>
</div>

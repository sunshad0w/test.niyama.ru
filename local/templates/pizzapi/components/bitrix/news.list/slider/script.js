$(document).ready(function() {
	function fotoramaInit() {
        var $initMainRama = $('.js-fotorama_mainpage').fotorama();
        var  mainRama = $initMainRama.data('fotorama');

        mainRama.resize({
            width: '100%',
            ratio: 32/5
        });

        mainRama.setOptions({
            nav: false,
            arrows: true,
            loop: true,
            click: false,
            swipe: true,
            trackpad: true,
            allowfullscreen: false,
            fit: 'contain',
            transition: 'slide',
            autoplay: 6000,
            //stopautoplayontouch: true
        });

    }


	function mobileFotoramaInit() {
        var $initMobileRama = $('.js-fotorama-mobile_mainpage').fotorama();
        var mobileRama = $initMobileRama.data('fotorama');

        mobileRama.resize({
            width: '100%',
            ratio: 32/20
        });
        mobileRama.setOptions({
            nav: false,
            arrows: true,
            loop: true,
            click: false,
            swipe: true,
            trackpad: true,
            allowfullscreen: false,
            fit: 'contain',
            transition: 'slide',
            autoplay: 6000,
            //stopautoplayontouch: true
        });
    }


    function resize(){
        if (!$('.js-fotorama_mainpage').hasClass('fotorama--hidden')) {
            fotoramaInit();

        }
        if (!$('.js-fotorama-mobile_mainpage').hasClass('fotorama--hidden')) {
            mobileFotoramaInit()
        }
    }


	function toogleSlider($checkpoint) {
	    var width = document.documentElement.clientWidth;
        if(width < $checkpoint){
            $(".js-fotorama-mobile_mainpage").removeClass('fotorama--hidden');
            $(".js-fotorama_mainpage").addClass('fotorama--hidden');
        } else {
            $(".js-fotorama-mobile_mainpage").addClass('fotorama--hidden');
            $(".js-fotorama_mainpage").removeClass('fotorama--hidden');
        }
        resize();
    }


    toogleSlider(500);


    window.addEventListener("resize", function(e) {
        toogleSlider(500);
    });

});
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="fotorama js-fotorama_mainpage">
	<?foreach ($arResult['ITEMS'] as $item):?>
        <div data-img ="<?=$item['PREVIEW_PICTURE']['SRC']?>">
            <?if(!empty($item['PROPERTIES']['343']['VALUE'])):?>
                <a class="clickFotorama" href="<?=$item['PROPERTIES']['343']['VALUE']?>"></a>
            <?endif;?>
        </div>
	<?endforeach;?>
</div>
<div class="fotorama-mobile js-fotorama-mobile_mainpage">
    <?foreach ($arResult['ITEMS'] as $item):?>
        <div data-img ="<?=$item['DETAIL_PICTURE']['SRC']?>">
            <?if(!empty($item['PROPERTIES']['343']['VALUE'])):?>
                <a class="clickFotorama" href="<?=$item['PROPERTIES']['343']['VALUE']?>"></a>
            <?endif;?>
        </div>
    <?endforeach;?>
</div>

<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');
$quantity_all = 0;
$sum = 0;
$cartId = $arParams['cartId'];
?>

<div class="catalog__cart--open">
    <div class="catalog__cart__wrapper">

<? foreach ($arResult["ITEMS"] as $item): ?>
<?
        $sum +=  $item["QUANTITY"] *  $item["PRICE"];
        $quantity_all = $quantity_all + 1;
  
?>


                <div class="catalog__cart__wrapper__item">

                    <div class="catalog__cart__image">
                        <a href="<?= $item["DETAIL_PAGE_URL"] ?>">
                            <img src="<?= $item["PICTURE_SRC"] ?>"/>
                        </a>
                    </div>
                    <div class="catalog__cart__title">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <?= $item["NAME"] ?>
                        </a>
                    </div>


                    <div class="catalog__cart__countplus">
                        <span>+</span>
                    </div>
                    <div class="catalog__cart__count"><? echo($item["QUANTITY"]); ?></div>

                    <div class="catalog__cart__countminus">-</div>
                    <div class="catalog__cart__price">
                        <span>"<?= $item["PRICE"] ?></span>Руб.
                    </div>
                    <div class="catalog__cart__deleteitem">
                     <a href="/personal/cart/?action=delete&id=<?=$item['ID'];?>" class="basket-list-delete" id="ajaxaction=delete&ajaxdeleteid=<?=$item['ID'];?>">удалить</a>
                    </div>
                </div>
            <?endforeach; ?>



            <div class="catalog__cart__itogo">
                <span class="catalog__cart__wrapper_itogo">Итого:</span>
                1440
                <span>руб.</span>
            </div>

            <div class="catalog__cart__buttons">
                <div class="catalog__cart__hide">Свернуть</div>
                <div class="catalog__cart__makeorder">Заказать</div>
            </div>
        </div>
    </div>



    <script>
        BX.ready(function(){
            <?=$cartId?>.fixCart();
        });
    </script>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>

    <div class="news__detail">
        <div class="action__name">
            <?= $arResult['NAME'] ?>
        </div>
        <div class="action__picture">
            <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>"/>
        </div>
        <div class="action__text">
            <?= $arResult['DETAIL_TEXT'] ?>
        </div>
    </div>

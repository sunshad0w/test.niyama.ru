<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>


<?
$space = ' ';
$changespace = 'nbsp;';
$curUrl = $APPLICATION->GetCurPage(false);
$pattern = "/catalog/";
$catalogFlag = substr($APPLICATION->GetCurPage(false), 0, strlen($pattern)) === $pattern;
?>

<?foreach($arResult as $i => $arItem):?>

	<?if ($arItem["PERMISSION"] > "D"):?>
		<a class="navigation__item <?= ($APPLICATION->GetCurPage(false) === $arItem['LINK'])  || ($i === 0 && $catalogFlag)? 'navigation__item--active':''?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]/*str_replace($space, $changespace, $arItem["TEXT"])*/?></a>
	<?endif?>

<?endforeach?>



<?endif?>
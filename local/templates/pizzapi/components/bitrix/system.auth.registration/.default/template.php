<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 15.04.2014
 * @time: 9:43
 */

ShowMessage($arParams["~AUTH_RESULT"]);

$APPLICATION->IncludeComponent(
    "bitrix:main.register",
    "",
    Array(
        "USER_PROPERTY_NAME" => "",
        "SEF_MODE" => "N",
        "SHOW_FIELDS" => array(),
        "REQUIRED_FIELDS" => array(),
        "AUTH" => "N",
        "USE_BACKURL" => "Y",
        "SUCCESS_PAGE" => "/auth/?register=yes&confirmation=yes",
        "SET_TITLE" => "N",
        "USER_PROPERTY" => Array("UF_SUBSCRIPTION")
    )
);

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

CJSCore::Init(array("popup"));
$arSkuTemplate = array();

?>

<? if (!empty($arResult['ITEMS'])): ?>

    <div class="order_recommend__title"><? echo GetMessage('CATALOG_RECOMMENDED_PRODUCTS_HREF_TITLE') ?>:</div>

    <div class="order__recommend">
        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <? foreach ($arItem['OFFERS'] as $key => $offers): ?>
                <? $price = CPrice::GetBasePrice($offers['ID']);?>

                <div class="order__recommend__item" data-id="<?= $offers['ID'] ?>">
                    <div class="order__recommend__item__image"><img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"></div>

                    <div class="order__recommend__item__description">
                        <div class="order__recommend__item__description__title"><?= $offers['NAME'] ?></div>
                        <div
                            class="order__recommend__item__description__price"><?=$price['PRICE']?>
                            руб.
                        </div>
                        <div class="order__recommend__item__description__add add2cart"
                             id="ajaxaction=add&ajaxaddid=<?= $offers['ID'] ?>" rel="nofollow">+
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        <? endforeach; ?>
        <div class="recommend__space__hack"></div>

    </div>

    <script type="text/javascript">



        function ajaxpostshow(urlres, datares, wherecontent) {
            $.ajax({
                type: "POST",
                url: urlres,
                data: datares,
                dataType: "html",
                success: function (data) {
                    var $block = $(data);
  $('.bitrix_sale_basket_basket').html($block).html();
                    console.log('$block');
                    console.log($block);
                }
            });
        }


        $('.add2cart').click(function () {
            var addbasketid = $(this).attr('id');
            ajaxpostshow("/ajax/addtocartforbasket.php", addbasketid, ".basket");
            return false;
        });

    </script>

<? endif ?>

<?
/**
* Bitrix Framework
* @package bitrix
* @subpackage main
* @copyright 2001-2014 Bitrix
*/

/**
* Bitrix vars
* @global CMain $APPLICATION
* @global CUser $USER
* @param array $arParams
* @param array $arResult
* @param CBitrixComponentTemplate $this
*/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (isset($arResult["ERRORS"]) && count($arResult["ERRORS"]) > 0) {
	foreach ($arResult["ERRORS"] as $key => $error) {
		if (intval($key) == 0 && $key !== 0) {
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
		}
	}
}

if ((isset($arParams['AJAX']) && $arParams['AJAX'] === 'Y') && $_SERVER["REQUEST_METHOD"] == "POST") {
    if ($USER->IsAuthorized()) {
        echo json_encode(array('result' => true, 'confirm' => GetMessage("MAIN_REGISTER_AUTH")));
        return;
    }
    if (isset($arResult["ERRORS"]) && count($arResult["ERRORS"]) > 0) {
		echo json_encode(array(
			'errors' => $arResult["ERRORS"]
		));
    	return;
	} elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y") {
	    echo json_encode(array('result' => true, 'confirm' => GetMessage("REGISTER_EMAIL_WILL_BE_SENT")));
	    return;
    }

} 
?>

<div class="authform">
	<?if($USER->IsAuthorized()):?>

		<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

	<?else:?>

		<h1>Зарегистрироваться</h1>

		<form id="regFormPopup" class="ajax-reg-form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">

			<div class="authform_errors">
				<?
                    if (count($arResult["ERRORS"]) > 0):
                        ShowError(implode("<br />", $arResult["ERRORS"]));
                    endif
                ?>
			</div>
            <div class="authform_ok"></div>

			<?
			if($arResult["BACKURL"] <> ''):
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?
			endif;
			?>


			<div class="row">
				<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
				<!-- <?= $FIELD ?> -->
					<? if ($FIELD === 'LOGIN') continue ?>
					<?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>

						<div class="col-md-3">
							<div class="authform__input">
								<label class="authform__label">
									<?echo GetMessage("main_profile_time_zones_auto")?><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?>
								</label>
								<select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
									<option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
									<option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
									<option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="authform__input">
								<label class="authform__label">
									<?echo GetMessage("main_profile_time_zones_zones")?>
								</label>
								<select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
									<?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
									<option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
									<?endforeach?>
								</select>
							</div>
						</div>


					<?else:?>
						<div class="col-md-3">
							<div class="authform__input">
								<label class="authform__label">
									<?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?>
								</label>
								<?
								switch ($FIELD)
								{
									case "PASSWORD":
										?><input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="bx-auth-input" />
										<?if($arResult["SECURE_AUTH"]):?>
										<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
											<div class="bx-auth-secure-icon"></div>
										</span>
										<noscript>
											<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
												<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
											</span>
										</noscript>
										<script type="text/javascript">
										document.getElementById('bx_auth_secure').style.display = 'inline-block';
										</script>
										<?endif?>
										<?
										break;
									case "CONFIRM_PASSWORD":
										?><input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /><?
										break;

									case "PERSONAL_GENDER":
										?><select name="REGISTER[<?=$FIELD?>]">
										<option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
										<option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
										<option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
										</select><?
										break;

									case "PERSONAL_COUNTRY":
									case "WORK_COUNTRY":
										?><select name="REGISTER[<?=$FIELD?>]"><?
										foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
										{
											?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
											<?
										}
										?></select><?
										break;

									case "PERSONAL_PHOTO":
									case "WORK_LOGO":
										?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
										break;

									case "PERSONAL_NOTES":
									case "WORK_NOTES":
										?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
										break;
									default:
										if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
										?><input size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /><?
										if ($FIELD == "PERSONAL_BIRTHDAY")
											$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'SHOW_INPUT' => 'N',
													'FORM_NAME' => 'regform',
													'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
													'SHOW_TIME' => 'N'
													),
												null,
												array("HIDE_ICONS"=>"Y")
												);
										?><?
								}?>
							</div>
						</div>
					<?endif?>
				<?endforeach?>
				<?// ********************* User properties ***************************************************?>
				<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>

					<h2><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></h2>
					<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
						<div class="col-md-3">
							<div class="authform__input">
								<label class="authform__label">
									<?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?>

								</label>

								<?$APPLICATION->IncludeComponent(
									"bitrix:system.field.edit",
									$arUserField["USER_TYPE"]["USER_TYPE_ID"],
									array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>


							</div>
						</div>
					<?endforeach;?>

				<?endif;?>
				<?// ******************** /User properties ***************************************************?>
				<?
				/* CAPTCHA */
				if ($arResult["USE_CAPTCHA"] == "Y")
				{
					?>


					<div class="col-md-3">
						<div class="authform__input">
							<label class="authform__label">
								<b><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></b>

							</label>

							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />


						</div>
					</div>



					<div class="col-md-3">
						<div class="authform__input">
							<label class="authform__label">
								<?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span>

							</label>

							<input type="text" name="captcha_word" maxlength="50" value="" />

						</div>
					</div>


					<?
				}
				/* !CAPTCHA */
				?>
			</div>
            <div class="authform__input">
                <div class="authform__checkbox">
                    <input id="email_subscribe_checkbox" type="checkbox" />
                    <label for="email_subscribe_checkbox">Хочу получать информацию о скидках, подарках и специальных предложениях ресторана:</label>
                </div>
            </div>

			<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>

			<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

			<p class="authform__save-btn">
				<input type="submit" name="register_submit_button" value="Зарегистрироваться" onclick="subscribe_me();"/>
			</p>

			<? if (isset($arParams['SHOW_LOGIN_LINK']) && $arParams['SHOW_LOGIN_LINK']) : ?>
				<p>
					<a class="loginBtn" href="/auth/" rel="nofollow">Авторизоваться</a>
				</p>
			<? endif ?>
		</form>
	<?endif?>
</div>

$(function () {

    var c_b_obj = {
        "onincomplete" : function () {
            $(this).addClass('notValid');
        },
        "oncomplete"   : function () {
            $(this).removeClass('notValid');
        }
    };
    $(document).on('submit', 'form', function(e) {
        var requiredInputs = $(this).find('input[data-required]');
        var notValid = false;
        var firstNotValid = false;
        var emailComplete = $("input[name='USER_EMAIL']").inputmask("isComplete");
        requiredInputs.each(function(index, element){
            if($(element).val() == '') {
                notValid = true;
                if(!firstNotValid) {
                    firstNotValid = true;
                    $(element).focus();
                }
                $(element).addClass('notValid');
            } else {
                if($(element).attr('name') == 'USER_EMAIL' && !emailComplete ){
                    notValid = true;
                    if(!$(element).hasClass('notValid')) $(element).addClass('notValid');
                } else {
                    if($(element).hasClass('notValid')) $(element).removeClass('notValid');
                }

            }
        });
        if(notValid || !emailComplete) e.preventDefault();
    });
    $("input[name='USER_EMAIL']").inputmask('email', c_b_obj);

});
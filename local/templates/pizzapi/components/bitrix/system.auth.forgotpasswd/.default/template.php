
<?
  $optionalClass =  (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))? "" : "auth-block" ;
?>

<div class="main-content">
    <div class="main-content__wrapper">
        <div class="container">
            <div class="authform <?=$optionalClass?>">
                <h1>Восстановление пароля</h1>

                <?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
                    ShowMessage($arParams["~AUTH_RESULT"]);
                ?>

                <form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                    <?
                    if (strlen($arResult["BACKURL"]) > 0)
                    {
                    ?>
                        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                    }
                    ?>
                    <input type="hidden" name="AUTH_FORM" value="Y">
                    <input type="hidden" name="TYPE" value="SEND_PWD">
                    <p>
                        <?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
                    </p>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="authform__input">
                                <label class="authform__label">
                                    <?=GetMessage("AUTH_EMAIL")?>
                                </label>
                                <input type="text" name="USER_EMAIL" maxlength="255" data-required />
                            </div>
                        </div>
                    </div>
                    <?if($arResult["USE_CAPTCHA"]):?>
                        <div class="authform__input">
                            <label class="authform__label">
                                <?echo GetMessage("system_auth_captcha")?>
                            </label>
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                            <input type="text" name="captcha_word" maxlength="50" value="" />
                        </div>
                    <?endif?>
                    <p class="authform__save-btn">
                        <input type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
                    </p>
                    <p>
                        <a href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a>
                    </p> 
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    

document.bform.USER_LOGIN.focus();
</script>

<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;
$wdth = 167;
$hght = 167;
if ($normalCount > 0):
    ?>    <div id="basket_items_list">
        <div class="bx_ordercart_order_table_container">
            <table id="basket_items">
                <thead>
                <tr>
                    <td class="margin"></td>
                    <?
                    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
                        $arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
                        if ($arHeader["name"] == '')
                            $arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
                        $arHeaders[] = $arHeader["id"];

                        // remember which values should be shown not in the separate columns, but inside other columns
                        if (in_array($arHeader["id"], array("TYPE")))
                        {
                            $bPriceType = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "PROPS")
                        {
                            $bPropsColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELAY")
                        {
                            $bDelayColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELETE")
                        {
                            $bDeleteColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "WEIGHT")
                        {
                            $bWeightColumn = true;
                        }

                        if ($arHeader["id"] == "NAME"):
                            ?>
                            <td class="item" colspan="2" id="col_<?=$arHeader["id"];?>">
                            <?
                        elseif ($arHeader["id"] == "PRICE"):
                            ?>
                            <td class="price" id="col_<?=$arHeader["id"];?>">
                            <?
                        else:
                            ?>
                            <td class="custom" id="col_<?=$arHeader["id"];?>">
                            <?
                        endif;
                        ?>
                        <?=$arHeader["name"]; ?>
                        </td>
                        <?
                    endforeach;

                    if ($bDeleteColumn || $bDelayColumn):
                        ?>
                        <td class="custom"></td>
                        <?
                    endif;
                    ?>
                    <td class="margin"></td>
                </tr>
                </thead>

                <tbody class="basket__items">
                <?
                foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
                    $productsID [] = $arItem["PRODUCT_ID"];

                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                        ?>
                        <tr id="<?=$arItem["ID"]?>">

                            <td class="margin"></td>

                            <?
                            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                                if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
                                    continue;

                                if ($arHeader["id"] == "NAME"):
                                    ?>

                                    <td class="itemphoto">
                                        <div class="bx_ordercart_photo_container">
                                            <?

                                            $image_resize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => $wdth, "height" => $hght));

                                            if ($image_resize == false) {
                                                $image_no = "/local/templates/pizzapi/components/bitrix/catalog.recommended.products/pizzarecommend/images/no_photo.png";
                                            }
                                            ?>

                                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" ><?endif;?>
                                                <div class="bx_ordercart_photo"><img src="<?=($image_resize == false)? $image_no : $image_resize["src"] ?>"/></div>
                                                <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                                        </div>
                                        <?
                                        if (!empty($arItem["BRAND"])):
                                            ?>
                                            <div class="bx_ordercart_brand">
                                                <img alt="" src="<?=$arItem["BRAND"]?>" />
                                            </div>
                                            <?
                                        endif;
                                        ?>
                                    </td>
                                    <td class="item">
                                        <h2 class="bx_ordercart_itemtitle">
                                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" ><?endif;?>
                                                <?=$arItem["NAME"];?>
                                                <?
                                                $ar_res = CCatalogProduct::GetByID($arItem['PRODUCT_ID']);
                                                if ($ar_res['LENGTH'] != '0'){
                                                    echo $ar_res['LENGTH'] . 'см';
                                                }
                                                ?>

                                                <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                                        </h2>
                                        <div class="bx_ordercart_itemart">
                                            <?
                                            if ($bPropsColumn):
                                                foreach ($arItem["PROPS"] as $val):

                                                    if (is_array($arItem["SKU_DATA"]))
                                                    {
                                                        $bSkip = false;
                                                        foreach ($arItem["SKU_DATA"] as $propId => $arProp)
                                                        {
                                                            if ($arProp["CODE"] == $val["CODE"])
                                                            {
                                                                $bSkip = true;
                                                                break;
                                                            }
                                                        }
                                                        if ($bSkip)
                                                            continue;
                                                    }

                                                    echo $val["NAME"].": <span>".$val["VALUE"]."<span><br/>";
                                                endforeach;
                                            endif;
                                            ?>
                                        </div>
                                        <?
                                        if (is_array($arItem["SKU_DATA"]) && !empty($arItem["SKU_DATA"])):
                                            foreach ($arItem["SKU_DATA"] as $propId => $arProp):

                                                // if property contains images or values
                                                $isImgProperty = false;
                                                if (!empty($arProp["VALUES"]) && is_array($arProp["VALUES"]))
                                                {
                                                    foreach ($arProp["VALUES"] as $id => $arVal)
                                                    {
                                                        if (!empty($arVal["PICT"]) && is_array($arVal["PICT"])
                                                            && !empty($arVal["PICT"]['SRC']))
                                                        {
                                                            $isImgProperty = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $countValues = count($arProp["VALUES"]);
                                                $full = ($countValues > 5) ? "full" : "";

                                                if ($isImgProperty): // iblock element relation property
                                                    ?>
                                                    <div class="bx_item_detail_scu_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

                                                        <div class="bx_scu_scroller_container">

                                                            <div class="bx_scu">
                                                                <ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
                                                                    style="width: 200%; margin-left:0%;"
                                                                    class="sku_prop_list"
                                                                >
                                                                    <?
                                                                    foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

                                                                        $selected = "";
                                                                        foreach ($arItem["PROPS"] as $arItemProp):
                                                                            if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
                                                                            {
                                                                                if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
                                                                                    $selected = "bx_active";
                                                                            }
                                                                        endforeach;
                                                                        ?>
                                                                        <li style="width:10%;"
                                                                            class="sku_prop <?=$selected?>"
                                                                            data-value-id="<?=$arSkuValue["XML_ID"]?>"
                                                                            data-element="<?=$arItem["ID"]?>"
                                                                            data-property="<?=$arProp["CODE"]?>"
                                                                        >
                                                                            <a href="javascript:void(0);">
                                                                                <span style="background-image:url(<?=$arSkuValue["PICT"]["SRC"]?>)"></span>
                                                                            </a>
                                                                        </li>
                                                                        <?
                                                                    endforeach;
                                                                    ?>
                                                                </ul>
                                                            </div>

                                                            <div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                            <div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                        </div>

                                                    </div>
                                                    <?
                                                else:
                                                    ?>
                                                    <div class="bx_item_detail_size_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

                                                        <div class="bx_size_scroller_container">
                                                            <div class="bx_size">
                                                                <ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
                                                                    style="width: 200%; margin-left:0%;"
                                                                    class="sku_prop_list"
                                                                >
                                                                    <?
                                                                    foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

                                                                        $selected = "";
                                                                        foreach ($arItem["PROPS"] as $arItemProp):
                                                                            if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
                                                                            {
                                                                                if ($arItemProp["VALUE"] == $arSkuValue["NAME"])
                                                                                    $selected = "bx_active";
                                                                            }
                                                                        endforeach;
                                                                        ?>
                                                                        <li style="width:10%;"
                                                                            class="sku_prop <?=$selected?>"
                                                                            data-value-id="<?=($arProp['TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory' ? $arSkuValue['XML_ID'] : $arSkuValue['NAME']); ?>"
                                                                            data-element="<?=$arItem["ID"]?>"
                                                                            data-property="<?=$arProp["CODE"]?>"
                                                                        >
                                                                            <a href="javascript:void(0);"><?=$arSkuValue["NAME"]?></a>
                                                                        </li>
                                                                        <?
                                                                    endforeach;
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                            <div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                            <div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                        </div>

                                                    </div>
                                                    <?
                                                endif;
                                            endforeach;
                                        endif;
                                        ?>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "QUANTITY"):
                                    ?>
                                    <td class="custom">
                                        <div class="centered">
                                            <table cellspacing="0" cellpadding="0" class="counter">
                                                <tr>
                                                    <?
                                                    if (!isset($arItem["MEASURE_RATIO"]))
                                                    {
                                                        $arItem["MEASURE_RATIO"] = 1;
                                                    }
                                                    $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                                    $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
                                                    $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                                    $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");

                                                    ?>

                                                    <td id="basket_quantity_control">
                                                        <div class="basket_quantity_control">
                                                            <a href="javascript:void(0);" class="plus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <input
                                                            type="text"
                                                            size="3"
                                                            id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                                            name="QUANTITY_INPUT_<?=$arItem["ID"]?>"

                                                            maxlength="18"
                                                            min="0"
                                                            <?=$max?>
                                                            step="<?=$ratio?>"
                                                            style="max-width: 50px;"
                                                            value="<?=$arItem["QUANTITY"]?>"
                                                            onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
                                                        >
                                                    </td>
                                                    <?
                                                    if (!isset($arItem["MEASURE_RATIO"]))
                                                    {
                                                        $arItem["MEASURE_RATIO"] = 1;
                                                    }

                                                    if (
                                                        floatval($arItem["MEASURE_RATIO"]) != 0
                                                    ):
                                                        ?>

                                                        <td>
                                                            <div class="basket_quantity_control">
                                                                <a href="javascript:void(0);" class="minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                            </div>
                                                        </td>
                                                        <?
                                                    endif;

                                                    ?>
                                                </tr>
                                            </table>
                                        </div>
                                        <input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "PRICE"):
                                    ?>
                                    <td class="price">
                                        <div class="current_price" id="current_price_<?=$arItem["ID"]?>">
                                            <?=preg_replace('/\D/', '', $arItem["SUM"])?>&nbsp;<i class="fa fa-rub" aria-hidden="true" style="font-size: 20px;"></i>

                                        </div>
                                        <div class="old_price" id="old_price_<?=$arItem["ID"]?>">
                                            <?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
                                                <?=$arItem["FULL_PRICE_FORMATED"]?>

                                            <?endif;?>
                                        </div>

                                        <?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
                                            <div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
                                            <div class="type_price_value"><?=$arItem["NOTES"]?></div>
                                        <?endif;?>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "DISCOUNT"):
                                    ?>
                                    <td class="custom">
                                        <span><?=$arHeader["name"]; ?>:</span>
                                        <div id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?></div>
                                    </td>
                                    <?
                                elseif ($arHeader["id"] == "WEIGHT"):
                                    ?>
                                    <td class="custom">
                                        <span><?=$arHeader["name"]; ?>:</span>
                                        <?=$arItem["WEIGHT_FORMATED"]?>
                                    </td>
                                    <?
                                else:
                                    ?>
                                    <td class="custom">
                                        <span><?=$arHeader["name"]; ?>:</span>
                                        <?
                                        if ($arHeader["id"] == "SUM"):
                                        ?>
                                        <div id="sum_<?=$arItem["ID"]?>">
                                            <?
                                            endif;

                                            echo $arItem[$arHeader["id"]];

                                            if ($arHeader["id"] == "SUM"):
                                            ?>
                                        </div>
                                    <?
                                    endif;
                                    ?>
                                    </td>
                                    <?
                                endif;
                            endforeach;

                            if ($bDelayColumn || $bDeleteColumn):
                                ?>
                                <td class="control">
                                    <?
                                    if ($bDeleteColumn):
                                        ?>
<? $deleteUrl = "/personal/cart/?action=delete&id=";?>
                                        <a class="order__delete" href="<?=$deleteUrl . $arItem["ID"]?>"></a>

                                        <?
                                    endif;
                                    if ($bDelayColumn):
                                        ?>
                                        <a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delay"])?>"><?=GetMessage("SALE_DELAY")?></a>
                                        <?
                                    endif;
                                    ?>
                                </td>
                                <?
                            endif;
                            ?>

                            <td class="margin"></td>

                        </tr>
                        <?
                    endif;
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
        <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
        <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
        <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
        <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
        <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />



        <? $number = array_rand($productsID, 1); ?>

    <?/* echo "<pre>";
    var_dump($productsID);
    var_dump($productsID[$number]);*/
    ?>
        <?/************************INCLUDE Сопутствующие товары************************************/?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.recommended.products",
            "pizzarecommend",
            array(
                "IBLOCK_TYPE" => "offers",
                "IBLOCK_ID" => "60",
                "ID" =>"$productsID[$number]",
                "CODE" => $_REQUEST["PRODUCT_CODE"],
                "PROPERTY_LINK" => "RECOMMEND",
                "OFFERS_PROPERTY_LINK" => "RECOMMEND",
                "HIDE_NOT_AVAILABLE" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_NAME" => "Y",
                "SHOW_IMAGE" => "Y",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "PAGE_ELEMENT_COUNT" => "2",
                "LINE_ELEMENT_COUNT" => "3",
                "TEMPLATE_THEME" => "blue",
                "DETAIL_URL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "86400",
                "SHOW_OLD_PRICE" => "N",
                "PRICE_CODE" => array(
                ),
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "Y",
                "CURRENCY_ID" => "RUB",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "USE_PRODUCT_QUANTITY" => "Y",
                "PROPERTY_CODE_58" => array(
                    0 => "",
                    1 => "WIDTH,LENGHT",
                    2 => "",
                ),
                "CART_PROPERTIES_58" => array(
                    0 => "",
                    1 => "SIZE",
                    2 => "",
                ),
                "ADDITIONAL_PICT_PROP_58" => "IMAGE",
                "LABEL_PROP_58" => "-",
                "PROPERTY_CODE_29" => array(
                    0 => "SIZE",
                    1 => "COLOR",
                ),
                "CART_PROPERTIES_29" => array(
                    0 => "SIZE",
                ),
                "OFFER_TREE_PROPS_29" => array(
                    0 => "SIZE",
                ),
                "COMPONENT_TEMPLATE" => "pizzarecommend",
                "SHOW_PRODUCTS_58" => "Y",
                "PROPERTY_CODE_60" => array(
                    0 => "",
                    1 => "",
                ),
                "CART_PROPERTIES_60" => array(
                    0 => "",
                    1 => "",
                ),
                "ADDITIONAL_PICT_PROP_60" => "",
                "OFFER_TREE_PROPS_60" => array(
                    0 => "-",
                )
            ),
            false
        );?>
        <?/************************end include************************************/?>


        <div class="bx_ordercart_order_pay">

            <div class="wrapper__bx_ordercart_order_pay">
                <div class="bx_ordercart_order_pay_left_left" id="coupons_block_good">
                    <? foreach ($arResult['COUPON_LIST'] as $coupon):?>
                        <? if ($coupon['JS_STATUS'] == 'APPLYED'):?>
                            <div class="bx_ordercart_coupon">
                                <input disabled="true" readonly="true" class="good" type="text"
                                       name="OLD_COUPON[]" style="display: none">
                                <span data-coupon="" class="good"><?=$coupon['COUPON']?></span>
                                <div class="bx_ordercart_coupon_notes">применен</div>

                            </div>
                        <?endif; ?>
                    <?endforeach; ?>
                </div>
                    <div class="bx_ordercart_order_pay_left" id="coupons_block">
                        <span><?=GetMessage("STB_COUPON_PROMT")?></span><input type="text" id="coupon" name="COUPON" value="">
                        <button class="order__price__input__button" onclick="enterCoupon(); return false;">Применить промокод</button>
                    </div>
                    <div class="bx_ordercart_order_pay_right">
                        <table class="bx_ordercart_order_sum">
                            <?if ($bWeightColumn):?>
                                <tr>
                                    <td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
                                    <td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?></td>
                                </tr>
                            <?endif;?>
                            <?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
                                <tr>
                                    <td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
                                    <td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
                                </tr>
                                <tr>
                                    <td><?echo GetMessage('SALE_VAT_INCLUDED')?></td>
                                    <td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
                                </tr>
                            <?endif;?>
                            <tr>
                            <td> <?=GetMessage("SALE_TOTAL")?> </td>

                            <td id="PRICE_WITHOUT_DISCOUNT"><?=preg_replace('/\D/', '', $arResult["PRICE_WITHOUT_DISCOUNT"]);?>&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></td>
                            </tr>                            
                           
                            <tr>
                                <td>Скидка: </td>
                                <?$lenght = strlen($arResult["DISCOUNT_PRICE_ALL_FORMATED"])-4?>
                                <td class="custom_t1" id="DISCOUNT_PRICE_ALL_FORMATED"><?=preg_replace('/\D/', '', $arResult["DISCOUNT_PRICE_ALL_FORMATED"]);?></td>
                                
                                <td>&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></td>
                                <td class="margin"></td>
                            </tr>
                            <tr class="margin-heght"></tr>
                            <tr>
                                <td class="fwb">Общая сумма:&nbsp;</td>
                                <td class="fwb" id="allSum"><?=$arResult["allSum"];?></td>
                                <td>&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></td>
                            </tr>

                        </table>
                        <div style="clear:both;"></div>
                    </div>
               </div>


            <div class="bx_ordercart_order_pay_center">

                <?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
                    <?=$arResult["PREPAY_BUTTON"]?>
                    <span><?=GetMessage("SALE_OR")?></span>
                <?endif;?>

                <a href="<?=$arParams["PATH_TO_ORDER"]?>" onclick="checkOut();" class="checkout"><?=GetMessage("SALE_ORDER")?></a>
            </div>

        </div>
    </div>
    <?
else:
    ?>
    <div id="basket_items_list">
        <table>
            <tbody>
            <tr>
                <td colspan="<?=$numCells?>" style="text-align:center">
                    <div class="basket__no__items"><?=GetMessage("SALE_NO_ITEMS");?></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?
endif;
?>



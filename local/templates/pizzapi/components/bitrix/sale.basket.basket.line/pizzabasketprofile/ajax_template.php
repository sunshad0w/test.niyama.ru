<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');
$quantity_all = 0;
$summary_all = 0;
$cartId = $arParams['cartId'];

//require(realpath(dirname(__FILE__)).'/top_template.php');
?>     <h2>Повторить заказ</h2> <?
if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {
    ?>    <div class="catalog__cart--open__wrapper">
        <?if ($arResult['NUM_PRODUCTS'] ===0):?>
        <div class="catalog__cart__wrapper">
            <div class="catalog__cart__wrapper__item">
                <?=$arResult['ERROR_MESSAGE']?>
            </div>
        </div>
<?else:?>
        <? foreach ($arResult["CATEGORIES"] as $category => $items):
            if (empty($items))
                continue;
            ?>

        <? foreach ($items as $v): ?>

            <?
            /*echo "<pre>";
            var_dump($v);*/
            $quantity_all += $v["QUANTITY"];
            $summary_all = $summary_all + (((int)$v["PRICE"])*((int)$v["QUANTITY"]));
            ?>
            <div class="catalog__cart__wrapper">
                <div class="catalog__cart__wrapper__item">
                    <div class="catalog__cart__item catalog__cart__image">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <img src="<?= $v["PICTURE_SRC"] ?>"/>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__title">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <?= $v["NAME"] ?>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__count--change" onclick="<?=$cartId?>.basketItemUpdate(<?=$v['ID'];?>, <?= $v['QUANTITY']+1 ?>)">
                      <i class="fa fa-plus" aria-hidden="true"></i>

                    </div>

                    <div class="catalog__cart__item catalog__cart__count"><? echo($v["QUANTITY"]); ?></div>
                    <div class="catalog__cart__item catalog__cart__count--change"onclick="<?=$cartId?>.basketItemUpdate(<?=$v['ID'];?>, <?= $v['QUANTITY']-1 ?>)">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                     </div>

                    <div class="catalog__cart__item catalog__cart__price">
                        <span><?= $v["BASE_PRICE"]* $v['QUANTITY']?>&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></span>

                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <? endforeach; ?>
        <div class="catalog__cart__itogo">
            Итого:
            <?= $summary_all ?>
            руб.
        </div>
    </div>
    <?endif;?>
    <div class="profile-cart__oprder-btn">
        <a href="/personal/order/">Оформить заказ</a>
    </div>

    <div class="catalog--count" style="display:none">
        <?=$quantity_all;?>
    </div>



    <script>
        BX.ready(function () {
            <?=$cartId?>.fixCart();
        });
    </script>
    <?
}
?>
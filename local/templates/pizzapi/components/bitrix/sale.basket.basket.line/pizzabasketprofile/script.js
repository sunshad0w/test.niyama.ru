'use strict';

function BitrixSmallCart(){}

BitrixSmallCart.prototype = {

    activate: function ()
    {
        this.cartElement = BX(this.cartId);
        this.fixedPosition = this.arParams.POSITION_FIXED == 'Y';
        if (this.fixedPosition)
        {
            this.cartClosed = true;
            this.maxHeight = false;
            this.itemRemoved = false;
            this.verticalPosition = this.arParams.POSITION_VERTICAL;
            this.horizontalPosition = this.arParams.POSITION_HORIZONTAL;
            this.topPanelElement = BX("bx-panel");

            this.fixAfterRender(); // TODO onready
            this.fixAfterRenderClosure = this.closure('fixAfterRender');

            var fixCartClosure = this.closure('fixCart');
            this.fixCartClosure = fixCartClosure;

            if (this.topPanelElement && this.verticalPosition == 'top')
                BX.addCustomEvent(window, 'onTopPanelCollapse', fixCartClosure);

            var resizeTimer = null;
            BX.bind(window, 'resize', function() {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(fixCartClosure, 200);
            });
        }
        this.setCartBodyClosure = this.closure('setCartBody');
        this.setRefreshCartCallbackClosure = this.closure('refreshCartCallback');
        BX.addCustomEvent(window, 'OnBasketChange', this.closure('refreshCart', {}));
    },

    fixAfterRender: function ()
    {
        this.statusElement = BX(this.cartId + 'status');
        if (this.statusElement)
        {
            if (this.cartClosed)
                this.statusElement.innerHTML = this.openMessage;
            else
                this.statusElement.innerHTML = this.closeMessage;
        }
        this.productsElement = BX(this.cartId + 'products');
        this.fixCart();
    },

    closure: function (fname, data)
    {
        var obj = this;
        return data
            ? function(){obj[fname](data)}
            : function(arg1){obj[fname](arg1)};
    },

    toggleOpenCloseCart: function ()
    {
        if (this.cartClosed)
        {
            BX.removeClass(this.cartElement, 'bx-closed');
            BX.addClass(this.cartElement, 'bx-opener');
            this.statusElement.innerHTML = this.closeMessage;
            this.cartClosed = false;
            this.fixCart();
        }
        else // Opened
        {
            BX.addClass(this.cartElement, 'bx-closed');
            BX.removeClass(this.cartElement, 'bx-opener');
            BX.removeClass(this.cartElement, 'bx-max-height');
            this.statusElement.innerHTML = this.openMessage;
            this.cartClosed = true;
            var itemList = this.cartElement.querySelector("[data-role='basket-item-list']");
            if (itemList)
                itemList.style.top = "auto";
        }
        setTimeout(this.fixCartClosure, 100);
    },

    setVerticalCenter: function(windowHeight)
    {
        var top = windowHeight/2 - (this.cartElement.offsetHeight/2);
        if (top < 5)
            top = 5;
        this.cartElement.style.top = top + 'px';
    },

    fixCart: function()
    {
        
    },

    refreshCart: function (data)
    {
        if (this.itemRemoved)
        {
            this.itemRemoved = false;
            return;
        }

        data.sessid = BX.bitrix_sessid();
        data.siteId = this.siteId;
        data.templateName = this.templateName;
        data.arParams = this.arParams;
        BX.ajax({
            url: this.ajaxPath,
            method: 'POST',
            dataType: 'html',
            data: data,
            onsuccess: this.setRefreshCartCallbackClosure
        });
    },

    refreshCartCallback: function (result) {
        console.log('refreshCartCallback');
        var catalog_count_new = $(result);
        var $catalog_count_new = parseInt($(catalog_count_new[6]).html());
        if ($catalog_count_new > 0) {
            $('.catalog__cart--count').html($catalog_count_new);
        } else {
            $('.catalog__cart--count').addClass('catalog__cart--count-hidden');
        }


        if (this.cartElement)
            this.cartElement.innerHTML = result;
        if (this.fixedPosition)
            setTimeout(this.fixAfterRenderClosure, 100);
    },

    setCartBody: function (result)
    {
        if (this.cartElement)
            this.cartElement.innerHTML = result;
        if (this.fixedPosition)
            setTimeout(this.fixAfterRenderClosure, 100);
    },

    removeItemFromCart: function (id)
    {
        this.refreshCart ({sbblRemoveItemFromCart: id});
        this.itemRemoved = true;
        BX.onCustomEvent('OnBasketChange');
    },

    basketItemUpdate: function (id, count) {
        if (count == 0) {//this.removeItemFromCart(id)
             }
        else{
        $.ajax({
            url: '/ajax/addToCartProfile.php',
            method: 'POST',
            dataType: 'html',
            data: {
                ajaxaction: 'update',
                ajaxbasketcountid: id,
                ajaxbasketcount: count,
            },
            success: function (data) {
                var $block = $(data).find(".catalog__cart--open__wrapper");
                $(".catalog__cart--open__wrapper").html($block.html());
                var count_catalog = $(data).find('.catalog--count').text();
                $('.catalog__cart--count').text(count_catalog);
            }
        });
        }
    }
};
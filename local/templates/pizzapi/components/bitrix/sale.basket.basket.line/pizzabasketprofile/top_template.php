<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<style>
 .catalog__cart__makeorder a {
        width: 100%;
        height: 100%;
    }
    .catalog__cart__makeorder--disabled a{
        pointer-events: none;
        cursor: default;
    }
    .catalog__cart__makeorder--disabled {
        background-color: grey;
        cursor: default;
    }

    .catalog__cart__makeorder {
        position: relative;
    }
</style>
    <?if ($arResult['NUM_PRODUCTS'] ===0):?>
        <div class="catalog__cart__wrapper">
            <div class="catalog__cart__wrapper__item">
                <?=$arResult['ERROR_MESSAGE']?>
            </div>
       </div>
<div class="catalog__cart__buttons">
        <div class="catalog__cart__hide">Свернуть</div>
        <div class="catalog__cart__makeorder catalog__cart__makeorder--disabled"> <a href="/personal/order/">Заказать</a></div>
    </div>
<?endif;?>
<script>
      $(".catalog__cart__hide").on('click', function () {
            if ($(".catalog__cart--open").css('display') == 'none') {
                $(".catalog__cart--open").fadeIn(200);
            } else {
                $(".catalog__cart--open").fadeOut(200);
            }
        });
</script>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$cartStyle = 'bx-basket';
$cartId = "bx_basket".$component->getNextNumber();
$arParams['cartId'] = $cartId; ?>
<? if (!isset($arParams['EMPTY']) && $arParams['EMPTY'] !== 'Y') :?>
<div id="<?=$cartId?>_container" class="profile-cart" >
    <? require(realpath(dirname(__FILE__)).'/ajax_template.php'); ?>
</div>
<? endif ?>
<script>
    var <?=$cartId?> = new BitrixSmallCart;
    <?=$cartId?>.siteId       = '<?=SITE_ID?>';
    <?=$cartId?>.cartId       = '<?=$cartId?>_container';
    <?=$cartId?>.ajaxPath     = '<?=$componentPath?>/ajax.php';
    <?=$cartId?>.templateName = '<?=$templateName?>';
    <?=$cartId?>.arParams     =  <?=CUtil::PhpToJSObject ($arParams)?>;
    <?=$cartId?>.closeMessage = '<?=GetMessage('TSB1_COLLAPSE')?>';
    <?=$cartId?>.openMessage  = '<?=GetMessage('TSB1_EXPAND')?>';
    <?=$cartId?>.activate();
    window['basketProfile'] = <?=$cartId?>;
</script>
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');
$quantity_all = 0;
$summary_all = 0;
$cartId = $arParams['cartId'];
$wdth = 167;
$hght = 167;

require(realpath(dirname(__FILE__)) . '/top_template.php');

    if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {
    ?>

    <div class="catalog__cart--open__wrapper">
        <? if ($arResult['NUM_PRODUCTS'] === 0): ?>
            <div class="catalog__cart__wrapper">
                <div class="catalog__cart__wrapper__item">
                    <?= $arResult['ERROR_MESSAGE'] ?>
                </div>
            </div>
        <?else: ?>
        <? foreach ($arResult["CATEGORIES"] as $category => $items):
            if (empty($items))
                continue;


  //      $main_img = CFile::GetFileArray($arElement['PROPERTIES']['IMAGE']['VALUE']);
            ?>

            <? foreach ($items as $v): ?>

            <?
$image_resize = CFile::ResizeImageGet($v["DETAIL_PICTURE"], array("width" => $wdth, "height" => $hght));

            $productsID [] = $v["PRODUCT_ID"];
            $quantity_all += $v["QUANTITY"];
            $summary_all = $summary_all + (((int)$v["PRICE"]) * ((int)$v["QUANTITY"]));
            ?>
            <div class="catalog__cart__wrapper">
                <div class="catalog__cart__wrapper__item">
                    <div class="catalog__cart__item catalog__cart__image">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <img src="<?=$image_resize["src"] ?>"/>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__title">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <?= $v["NAME"] ?>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__countplus"
                         onclick="<?= $cartId ?>.basketItemUpdate(<?= $v['ID']; ?>, <?= $v['QUANTITY'] + 1 ?>)">
                        <span>+</span>
                    </div>

                    <div class="catalog__cart__item catalog__cart__count"><? echo($v["QUANTITY"]); ?></div>
                    <div class="catalog__cart__item catalog__cart__countminus"
                         onclick="<?= $cartId ?>.basketItemUpdate(<?= $v['ID']; ?>, <?= $v['QUANTITY'] - 1 ?>)">-
                    </div>
                    <div class="catalog__cart__item catalog__cart__price">
                        <span><?= explode('.', $v["SUM"]) [0] ?><br>руб</span>
                    </div>

                    <div class="catalog__cart__item delete"
                         onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"
                         title="<?= GetMessage("TSB1_DELETE") ?>"> X
                    </div>

                </div>
            </div>
        <? endforeach; ?>
        <? endforeach; ?>
    </div>

    <? $number = array_rand($productsID, 1); ?>

  <?/************************INCLUDE Сопутствующие товары************************************/?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.recommended.products",
        "pizzarecommend",
        array(
            "IBLOCK_TYPE" => "offers",
            "IBLOCK_ID" => "60",
            "ID" =>"$productsID[$number]",
            "CODE" => $_REQUEST["PRODUCT_CODE"],
            "PROPERTY_LINK" => "RECOMMEND",
            "OFFERS_PROPERTY_LINK" => "RECOMMEND",
            "HIDE_NOT_AVAILABLE" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_NAME" => "Y",
            "SHOW_IMAGE" => "Y",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "PAGE_ELEMENT_COUNT" => "2",
            "LINE_ELEMENT_COUNT" => "3",
            "TEMPLATE_THEME" => "blue",
            "DETAIL_URL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "86400",
            "SHOW_OLD_PRICE" => "N",
            "PRICE_CODE" => array(
            ),
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "USE_PRODUCT_QUANTITY" => "Y",
            "PROPERTY_CODE_58" => array(
                0 => "",
                1 => "WIDTH,LENGHT",
                2 => "",
            ),
            "CART_PROPERTIES_58" => array(
                0 => "",
                1 => "SIZE",
                2 => "",
            ),
            "ADDITIONAL_PICT_PROP_58" => "IMAGE",
            "LABEL_PROP_58" => "-",
            "PROPERTY_CODE_29" => array(
                0 => "SIZE",
                1 => "COLOR",
            ),
            "CART_PROPERTIES_29" => array(
                0 => "SIZE",
            ),
            "OFFER_TREE_PROPS_29" => array(
                0 => "SIZE",
            ),
            "COMPONENT_TEMPLATE" => "pizzarecommend",
            "SHOW_PRODUCTS_58" => "Y",
            "PROPERTY_CODE_60" => array(
                0 => "",
                1 => "",
            ),
            "CART_PROPERTIES_60" => array(
                0 => "",
                1 => "",
            ),
            "ADDITIONAL_PICT_PROP_60" => "",
            "OFFER_TREE_PROPS_60" => array(
                0 => "-",
            )
        ),
        false
    );?>
    <?/************************end include************************************/?>

  <div class="order__price__block">
        <div class="order__price__block__promo">
            <div class="order__price__block__promo__title">Промокод</div>
                <input type="text" class="order__price__input">

                <button class="order__price__input__button"
                        onclick="<?= $cartId ?>.addPromo(this)">применить промокод</button>
        </div>


        <div class="order__price__block__itogo">
            <span class="order__price__block__price">Итого:</span>
            <?= $summary_all ?>
            <span>руб.</span>
        </div>
    </div>

<?endif; ?>

    <script>
        BX.ready(function () {
            <?=$cartId?>.
            fixCart();
        });
    </script>
    <?
}
?>

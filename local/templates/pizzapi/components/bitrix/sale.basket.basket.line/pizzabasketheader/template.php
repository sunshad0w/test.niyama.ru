<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


foreach ($arResult["CATEGORIES"] as $category => $items){
    if (empty($items)){
        continue;
    };
    foreach ($items as $v) {
        $quantity_all += $v["QUANTITY"];

    };

};
?>


<div class="navigation__item--cart--count <?= ($quantity_all > 0) ? "" : "navigation__item--cart--count-hidden" ?>">
	<?= ($quantity_all < 99) ? $quantity_all : '99+' ; ?>
</div>


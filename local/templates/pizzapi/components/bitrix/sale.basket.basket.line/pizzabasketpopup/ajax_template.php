<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');
$quantity_all = 0;
$summary_all = 0;
$cartId = $arParams['cartId'];


?>
<div class="catalog__cart--open__wrapper">
    <? require(realpath(dirname(__FILE__)) . '/top_template.php'); ?>

    <? if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {

        ?>

        <? if ($arResult['NUM_PRODUCTS'] === 0): ?>
        <div class="catalog__cart__wrapper">
            <div class="catalog__cart__wrapper__item">
                <?= $arResult['ERROR_MESSAGE'] ?>
            </div>
        </div>
    <? else: ?>
    <div class="catalog__cart__wrapper">
        <div class="catalog__cart__wrapper_scroll">
        <? foreach ($arResult["CATEGORIES"] as $category => $items):
        if (empty($items))
            continue;
        ?>

        <? foreach ($items as $v): ?>

        <?


        $quantity_all += $v["QUANTITY"];
        $summary_all = $summary_all + (((int)$v["PRICE"]) * ((int)$v["QUANTITY"]));
        ?>


                <div class="catalog__cart__wrapper__item">
                    <div class="catalog__cart__item catalog__cart__image">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <img src="<?= $v["PICTURE_SRC"] ?>"/>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__title">
                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                            <? echo $v["NAME"] . ' ';
                            $ar_res = CCatalogProduct::GetByID($v['PRODUCT_ID']);
                            if ($ar_res['LENGTH'] != '0'){
                                echo $ar_res['LENGTH'] . 'см';
                            }

                            ?>
                        </a>
                    </div>
                    <div class="catalog__cart__item catalog__cart__count--change"
                         onclick="<?= $cartId ?>.basketItemUpdate(<?= $v['ID']; ?>, <?= $v['QUANTITY'] + 1 ?>)">
                        <i class="fa fa-plus" aria-hidden="true"></i>

                    </div>

                    <div class="catalog__cart__item catalog__cart__count"><? echo($v["QUANTITY"]); ?></div>
                    <div class="catalog__cart__item catalog__cart__count--change"
                         onclick="<?= $cartId ?>.basketItemUpdate(<?= $v['ID']; ?>, <?= $v['QUANTITY'] - 1 ?>)">
                        <i class="fa fa-minus" aria-hidden="true"></i>
                    </div>

                    <div class="catalog__cart__item catalog__cart__price">
                        <span><?= $v["BASE_PRICE"]* $v['QUANTITY']?>&nbsp;<i class="fa fa-rub" aria-hidden="true"></i></span>

                    </div>

                    <div class="catalog__cart__item delete"
                         onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"
                         title="<?= GetMessage("TSB1_DELETE") ?>">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>

                </div>


    <? endforeach; ?>
    <? endforeach; ?>
        </div>
    </div>
        <div class="catalog__cart__itogo">
            <span class="catalog__cart__wrapper_itogo">Итого:</span>
            <span id="summary_price" data-price="<?= $summary_all ?>"><?= $summary_all ?></span>
            <span>руб.</span>
        </div>

    <? endif; ?>
        <div class="catalog__cart__buttons">
            <div class="catalog__cart__hide">Свернуть</div>
            <div class="catalog__cart__makeorder"><a href="/personal/cart/" class="catalog__cart__makeorder__button">Заказать</a>
            </div>
        </div>

        <div class="catalog--count" style="display:none">
            <?= $quantity_all; ?>
        </div>


        <script>
            BX.ready(function () {
                <?=$cartId?>.
                fixCart();
            });
        </script>
        <?
    }
    ?>
</div>

<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$APPLICATION->setTitle('Подтверждение email');
$APPLICATION->AddChainItem("Подтверждение email");


//here you can place your own messages
switch($arResult["MESSAGE_CODE"])
{
    case "E01":
        ?><? //When user not found
        break;
    case "E02":
        ?><? //User was successfully authorized after confirmation
        break;
    case "E03":
        ?><? //User already confirm his registration
        break;
    case "E04":
        ?><? //Missed confirmation code
        break;
    case "E05":
        ?><? //Confirmation code provided does not match stored one
        break;
    case "E06":
        ?><? //Confirmation was successfull
        break;
    case "E07":
        ?><? //Some error occured during confirmation
        break;
}
?>

<?if($arResult["SHOW_FORM"]):

?><div class="main-content">
    <div class="main-content__wrapper">
        <div class="container">
            <div class="authform">
                <h1 class="auth__title align-center">Подтверждение email</h1>
                <div class="auth"><?
                    if (!empty($arResult["MESSAGE_TEXT"])) :
                        ?><div class="b-msg _error" style="margin-bottom: 25px;">
                        <?=  $arResult["MESSAGE_TEXT"]; ?>
                        </div><?
                    endif; ?>
                    <div class="authform__form-wrapper-confirmation"><?

                        ?><form method="post" action="<?echo $arResult["FORM_ACTION"]?>">

                            <label><?echo "Email: " ?></label>
                            <input class="input auth__input" type="text" name="<?echo $arParams["LOGIN"]?>" maxlength="50" value="<?echo (strlen($arResult["LOGIN"]) > 0? $arResult["LOGIN"]: $arResult["USER"]["LOGIN"])?>" size="17" >

                            <label><?echo "Код подтверждения: " ?></label>
                            <input class="input auth__input" type="text" name="<?echo $arParams["CONFIRM_CODE"]?>" maxlength="50" value="<?echo $arResult["CONFIRM_CODE"]?>" size="17" >

                            <input type="submit" value="Подтвердить" class="btn _style_2 auth__submit">
                            <input type="hidden" name="<?echo $arParams["USER_ID"]?>" value="<?echo $arResult["USER_ID"]?>" />
                        </form>

                    </div>
                </div><?

                ?>
                <?elseif(!$USER->IsAuthorized()):?>
                    <?
                    $USER->Authorize($arResult["USER_ID"]);
                    LocalRedirect('/index.php');
                    /*$APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array())*/;?>
                <?endif?>
            </div>
        </div>
    </div>
</div>


<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$ID = IntVal($_REQUEST["id"]);



$APPLICATION->IncludeComponent("bitrix:sale.personal.order.detail","pizzapi",Array(
        "PATH_TO_LIST" => "/personal/",
        "PATH_TO_CANCEL" => "/personal/",
        "PATH_TO_PAYMENT" => "/personal/",
        "PATH_TO_COPY" => "",
        "ID" => $ID,
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "PICTURE_WIDTH" => "110",
        "PICTURE_HEIGHT" => "110",
        "PICTURE_RESAMPLE_TYPE" => "1",
        "CUSTOM_SELECT_PROPS" => array(),
        "PROP_1" => Array(),
        "PROP_2" => Array()
    )
);

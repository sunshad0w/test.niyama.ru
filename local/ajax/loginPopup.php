<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?$APPLICATION->IncludeComponent("bitrix:system.auth.form","popup",Array(
     "REGISTER_URL" => "/auth/",
     "FORGOT_PASSWORD_URL" => "/auth/",
     "PROFILE_URL" => "/personal/",
     "SHOW_ERRORS" => "Y",
     "AJAX" => "Y",
     )
);?>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$iCur = $this->NavPageNomer;
$iEnd = $this->NavPageCount;

if ($iEnd > 1) {
	$iStart = $iCur - 2;
	$iFinish = $iCur + 2;
	if ($iStart < 1) {
		// Это если нужно сдвигать конец
		//$iFinish -= $iStart - 1;
		$iStart = 1;
	}
	if ($iFinish > $iEnd) {
		// Это если нужно сдвигать начало
		// $iStart -= $iFinish - $iEnd;
		if ($iStart < 1) {
			$iStart = 1;
		}
		$iFinish = $iEnd;
	}

	?><div onclick="return {'pagination': {}}" class="pagination js-widget"><?
	
		// Предыдущая, следующая
		?><div class="pagination__arrows"><?
			if ($iCur > 1) {

				?><a href="page=<?= $iCur-1 ?>" class="pagination__prev">предыдущая</a><?

			}
			else {
//				?><!--<span class="pagination__prev _disabled">предыдущая</span>--><?//
			}

			if ($iCur < $iEnd) {
                if ($iCur > 1) {
                 ?><span class="pagination__separator">|</span><?
                }
				?><a href="page=<?= $iCur+1 ?>" class="pagination__next">следующая</a><?
			}
			else {
//				?><!--<span class="pagination__next _disabled">следующая</span>--><?//
			}
		?></div><?
		
		// Странички
		?><div class="pagination__items"><?
			if ($iStart > 1) {
				?><a href="page=1" class="pagination__item">1</a><?
				if ($iStart > 2) {
					?><span class="pagination__dots">...</span><?
				}
			}
			for ($i = $iStart; $i <= $iFinish; $i++) {
				$sCurrent = $i == $iCur ? ' _active' : '';
				?><a href="page=<?=$i?>" class="pagination__item<?=$sCurrent?>"><?=$i?></a><?
			}
			if ($iFinish < $iEnd) {
				if ($iFinish < $iEnd - 1) {
					?><span class="pagination__dots">...</span><?
				}
				?><a href="page=<?=$iEnd?>" class="pagination__item"><?=$iEnd?></a><?
			}
        ?></div><?
		
	?></div><?
}

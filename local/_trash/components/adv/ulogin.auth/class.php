<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 29.05.2014
 * @time: 15:02
 */

/**
 * Class CUloginAuth 
 *
 */
Class CUloginAuth  extends CBitrixComponent {

    /**
     * default variables
     * @var
     */
    private $uri;
    private $sUloginUrl   = "http://ulogin.ru/token.php";  # Default ulogin api url
    private $arFieldsSoc  = array("first_name","last_name","nickname","city","photo","photo_big","bdate","sex","email","network");
    private $error        = array();
    private $success      = array();
    private $backurl      = "/";
    private $uloginDefaultPhoto = array("https://ulogin.ru//img/photo.png","https://ulogin.ru/img/photo.png","https://ulogin.ru/img/photo_big.png");
    private $noAuthGroup  = array(1); # Default Group id
    private $bOrderPage = false;


    /**
     * @param null $component
     */
    public function __construct($component = null)
    {
        parent::__construct($component);

        /** @var $security Получаем объект класса */
        $security = new Bitrix\Security\Filter\Request();

        /** @var $security Устанавливаем аудит на SQL enjection и XSS attack */
        $security->setAuditors(Array(
            'SQL' => new Bitrix\Security\Filter\Auditor\Sql(),
            'XSS' => new Bitrix\Security\Filter\Auditor\Xss()
        ));

        /** @var $arURI фильтруем входящие параметры. */
        $this->uri = $security->filter(array(
            'get'     => $_GET,
            'request' => $_REQUEST,
            'post'    => $_POST
        ), false);
    }

    /**
     * getResponse
     */
    public function getResponse()
    {
        if (!empty($this->uri['post']['token']) && !$GLOBALS['USER']->isAuthorized()) {
            $sContent = file_get_contents($this->sUloginUrl.'?token=' . $this->uri['post']['token'] . '&host=' . $_SERVER['HTTP_HOST']);
            if ($sContent) {
                if ( $arProfileData = json_decode($sContent, true) ) {

                    /* Проверяем на наличие в связке. */
                    if ( $iUserID = $this->checkInLink($arProfileData['uid']) ){

                        /* Проверяем на активацию */
                        if ($this->bUserCheckActive($iUserID)) {

                            /* Проверяем на администратора */
                            if (!$this->bUserCheckAdminGroup($iUserID)) {

                                /* Авторизуем, и редерикт */
                                $GLOBALS['USER']->Authorize($iUserID);

                                foreach(GetModuleEvents("main", "OnAfterSocAuthorize", true) as $arEvent) {
                                    ExecuteModuleEventEx($arEvent, Array($iUserID,$arProfileData));
                                }

                                if (isset($this->arParams['ORDER_PAGE']) && $this->arParams['ORDER_PAGE'] == 'Y') {
                                    LocalRedirect("/order/confirm/");
                                } else {
                                    LocalRedirect($this->backurl);
                                }

                            } else {
                                $this->setError(GetMessage("ULC_ADMIN_NO_AUTH"));
                                return;
                            }

                        } else {
                            $this->setError(GetMessage("ULC_NOT_ACTIVE"));
                            return;
                        }

                    } else {

                        /* Ищем по email уже в существующих */
                        if ( $iUserID = $this->bUserCheckByEmail($arProfileData['email']) ) {

                            /* Возвращаем сообщение о необходимости прикрепления соц сети из профиля */
                            // $this->setSuccess(str_replace("%s",$arProfileData['first_name']." ".$arProfileData['last_name'],GetMessage("ULC_LINK_INPROFILE")));
                            //return;

                            // Добавляем в связку
                            $arSocData = array(
                                "UF_USER_ID"  => $iUserID,
                                "UF_UID"      => $arProfileData['uid'],
                                "UF_SERVICES" => $arProfileData['network'],
                                "UF_IDENTITY" => $arProfileData['identity']
                            );

                            $obAdd = UsersocTable::add($arSocData);
                            if (!$obAdd->isSuccess()){
                                $this->setError(implode("<br>",$obAdd->getErrorMessages()));
                            } else {

                                $arUserParam["USER_ID"] = $iUserID;
                                $arEventFields = $arUserParam;
                                unset($arEventFields["PASSWORD"]);
                                unset($arEventFields["CONFIRM_PASSWORD"]);
                                $event = new CEvent;
                                $event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);

                                /* Авторизуем, и редерикт */
                                $GLOBALS['USER']->Authorize($iUserID);

                                foreach(GetModuleEvents("main", "OnAfterSocAuthorize", true) as $arEvent) {
                                    ExecuteModuleEventEx($arEvent, Array($iUserID,$arProfileData));
                                }
                                LocalRedirect($this->backurl);
                            }

                        } else {

                            /* регистрируем и отправляем письмо */
                            $this->registerUser($arProfileData);

                        }

                    }

                }
            }
        }
    }

    /**
     * Получаем аватар пользователя
     * setPersonalPhoto
     * @param $sPhotoUri
     * @return array|bool|null|string
     */
    public function setPersonalPhoto($sPhotoUri)
    {
        /* Проверка на дефолтные аватары */
        if ( !in_array($sPhotoUri,$this->uloginDefaultPhoto) ) {
            $imageContent = file_get_contents($sPhotoUri);
            $ext = strtolower(substr($sPhotoUri,-3));

            if (!in_array($ext,array('jpg','jpeg','png','gif','bmp'))) {
                $ext = 'jpg';
            }

            $tmpName = $tmpName = randString(10).'.'.$ext;
            $tmpName = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/social_avatar/".$tmpName;

            if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/upload")) {
                mkdir($_SERVER["DOCUMENT_ROOT"]."/upload", 0777, true);
            }
            if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/upload/tmp")) {
                mkdir($_SERVER["DOCUMENT_ROOT"]."/upload/tmp", 0777, true);
            }
            if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/social_avatar")) {
                mkdir($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/social_avatar", 0777, true);
            }

            /* => в upload tmp */
            if (file_put_contents($tmpName,$imageContent)){
                $arIMAGE = CFile::MakeFileArray($tmpName);
                $arIMAGE["MODULE_ID"] = "main";
            }
            else {
                $arIMAGE = '';
            }

            return $arIMAGE;
        } else {
            /* Функционал установки одного из списка стандартных аватаров */
            return $this->setNiyamaDefaultAvatar();
        }
    }

    /**
     * Устанавливаем Первый по списку из дефолтных аватаров.
     *
     * setNiyamaDefaultAvatar
     */
    public function setNiyamaDefaultAvatar()
    {
        $iAvatarId = CUsersData::getFirstDefaultAvatar();
        if ( $iAvatarId ) {
            $arFirstAva =  CFile::GetFileArray($iAvatarId);
            if(!empty( $arFirstAva['SRC'])) {
               return CFile::MakeFileArray($arFirstAva['SRC']);
            }
        }
        return false;
    }

    /**
     * Регистрируем нового пользователя.
     * registerUser
     * @param $arProfileData
     * @return bool
     */
    public function registerUser($arProfileData)
    {
        list($d, $m, $y) = explode('.', $arProfileData['bdate']);

        $arUserParam['LOGIN'] = $arProfileData['email'];
        $arUserParam['NAME']  = $GLOBALS['APPLICATION']->ConvertCharset($arProfileData['first_name'], "UTF-8", SITE_CHARSET);
        $arUserParam['LAST_NAME'] = $GLOBALS['APPLICATION']->ConvertCharset($arProfileData['last_name'], "UTF-8", SITE_CHARSET);
        $arUserParam['EMAIL'] =  $arProfileData['email'];

        $arUserParam['EXTERNAL_AUTH_ID'] =  $arProfileData['identity'];
        $arUserParam['ADMIN_NOTES'] =  $arProfileData['network'];

        /* В рамках бага NIYAMA-519 отменили подтверждение регистрации.  */
        //$bConfirmReq = COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" ? true : false;
        $bConfirmReq = false;

        $arUserParam["CHECKWORD"] = md5(CMain::GetServerUniqID().uniqid());
        $arUserParam["~CHECKWORD_TIME"] = $GLOBALS['DB']->CurrentTimeFunction();
        $arUserParam["ACTIVE"] = $bConfirmReq? "N": "Y";
        $arUserParam["CONFIRM_CODE"] = $bConfirmReq? randString(8): "";
        $arUserParam["LID"] = SITE_ID;
        $arUserParam["USER_IP"] = $_SERVER["REMOTE_ADDR"];
        $arUserParam["USER_HOST"] = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);

        $sUserPswd = randString(6);
        $arUserParam["PASSWORD"] = $sUserPswd;
        $arUserParam["CONFIRM_PASSWORD"] = $sUserPswd;

        $arUserParam["PERSONAL_PHOTO"] = $this->setPersonalPhoto($arProfileData['photo_big']);

        /* Устанавливаем группу */
        $def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
        if($def_group != "") {
            $arUserParam["GROUP_ID"] = explode(",", $def_group);
        }

        $GLOBALS['USER_FIELD_MANAGER']->EditFormAddFields("USER", $arUserParam);        
        $bSuccessIteration = true;
        
        /* Проводим событие */
        $events = GetModuleEvents("main", "OnBeforeUserRegister", true);
        foreach($events as $arEvent)
        {
            if(ExecuteModuleEventEx($arEvent, array(&$arUserParam)) === false)
            {
                if($err = $GLOBALS['APPLICATION']->GetException())
                    $this->setError($err->GetString());

                $bSuccessIteration = false;
                break;
            }
        }

        $ID = 0;
        $user = new CUser();
        if ($bSuccessIteration)
        {
            $ID = $user->Add($arUserParam);
        }

        if (intval($ID) > 0)
        {
            // Добавляем в связку
            $arSocData = array(
                "UF_USER_ID"  => $ID,
                "UF_UID"      => $arProfileData['uid'],
                "UF_SERVICES" => $arProfileData['network'],
                "UF_IDENTITY" => $arProfileData['identity']
            );

            $obAdd = UsersocTable::add($arSocData);

            if (!$obAdd->isSuccess()){
                $this->setError(implode("<br>",$obAdd->getErrorMessages()));
            } else {

                $arUserParam["USER_ID"] = $ID;
                $arEventFields = $arUserParam;
                unset($arEventFields["PASSWORD"]);
                unset($arEventFields["CONFIRM_PASSWORD"]);

                $event = new CEvent;
                $event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);
                if($bConfirmReq) {
                    $event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);
                    $this->setSuccess(GetMessage("ULC_SEND_MAIL"));
                }

                /* Авторизуем, и редерикт */
                $GLOBALS['USER']->Authorize($ID);

                foreach(GetModuleEvents("main", "OnAfterSocAuthorize", true) as $arEvent) {
                    ExecuteModuleEventEx($arEvent, Array($ID,$arProfileData));
                }

                ADV_set_flashdata("IS_AUTH_FINISH","yes");

                //LocalRedirect($this->backurl);
                if (isset($this->arParams['ORDER_PAGE']) && $this->arParams['ORDER_PAGE'] == 'Y') {
                    ADV_set_flashdata("BACKURL","/order/confirm/");
                }

                LocalRedirect("/auth/?register=yes&step=finish");

            }

        } else {
            $this->setError($user->LAST_ERROR);
        }

        $events = GetModuleEvents("main", "OnAfterUserRegister", true);
        foreach ($events as $arEvent) {
            ExecuteModuleEventEx($arEvent, array(&$arUserParam));
        }

        foreach(GetModuleEvents("main", "OnAfterSocUserRegister", true) as $arEvent) {
            ExecuteModuleEventEx($arEvent, Array($arUserParam["USER_ID"],  $arSocData));
        }
    }

    /**
     * Проверяем на наличие в связке
     * checkInLink
     * @param $uid
     * @return bool
     */
    public function checkInLink($uid)
    {
        $obLink = UsersocTable::getList(array(
            "filter" => array(
                "=UF_UID" => $uid
            )
        ));
        $ID = false;
        if ( $arLink = $obLink->fetch() ){
            $ID = $arLink['UF_USER_ID'];
        }
        return $ID;
    }

    /**
     * Проверяем на активацию пользователя
     * bUserCheckActive
     * @param $iUserID
     * @return bool
     */
    public function bUserCheckActive($iUserID)
    {
        $by = "email";
        $order = "desk";
        $rsUsers = CUser::GetList(
            $by,
            $order,
            array(
                "ID" => $iUserID,
                "ACTIVE" => "Y"
            )
        );
        if ($arUser = $rsUsers->GetNext()) {
            if ( ($arUser['ID'] == $iUserID) && ($arUser['ACTIVE'] == "Y") ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверяем пользователя по Email и возварщаем ID
     * bUserCheckByEmail
     * @param $sEmail
     * @return bool
     */
    public function bUserCheckByEmail($sEmail)
    {
        $by = "email";
        $order = "desk";
        $rsUsers = CUser::GetList(
            $by,
            $order,
            array(
                "LOGIN" => $sEmail,
                "ACTIVE" => "Y"
            )
        );
        if ($arUser = $rsUsers->GetNext()) {
            if ( ($arUser['LOGIN'] == $sEmail) && ($arUser['ACTIVE'] == "Y") ) {
                return $arUser['ID'];
            }
        }
        return false;
    }

    /**
     * bUserCheckAdminGroup
     * @param $iID
     * @return bool
     */
    public function bUserCheckAdminGroup($iID)
    {
        $arGroups = CUser::GetUserGroup($iID);
        foreach ($this->noAuthGroup as $iIDAdminGroup) {
            if ( in_array($iIDAdminGroup,$arGroups) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $sUloginUrl
     */
    public function setSUloginUrl($sUloginUrl)
    {
        $this->sUloginUrl = $sUloginUrl;
    }

    /**
     * Инициализация параметров.
     * initParams
     */
    public function initParams()
    {
        /* Устанавливаем страницу редиректа. */
        $this->setSRedirectUrl($this->arParams['REDIRECT_URI']);
        /* Устанавливаем страницу ресивера */
        $this->arResult['receiver'] = urlencode('http://' . $_SERVER['HTTP_HOST'] .$GLOBALS['APPLICATION']->GetCurPageParam("receiver",array("receiver")));
        /* Устанавливаем нужные поля */
        $this->setArFieldsSoc($this->arParams['SOC_FIELDS']);
        /* Устанавливаем BACKURL */
        $this->setBackurl($this->arParams['BACKURL']);
        /* Устанавливаем no_auth */
        $this->setNoAuthGroup($this->arParams['NO_AUTH_GROUP']);
        /* Установка флага страницы заказа */
        $bOrderPage = isset($this->arParams['ORDER_PAGE']) && $this->arParams['ORDER_PAGE'] == 'Y';
        $this->setBOrderPage($bOrderPage);

    }

    /**
     * JS Windows popup redirect and close
     * doReceiver
     */
    public function doReceiver()
    {
        if  ( isset($this->uri['request']['receiver']) ){
          $GLOBALS['APPLICATION']->RestartBuffer();
            ob_start();
            /* JS :) this is hardcore */
            ?><html><head></head><body>
              <script type="text/javascript">
              q = new Object();tmp = location.search.substr(1).split('&');for (i = 0; i < tmp.length; i++) {tmp2 = tmp[i].split('=');q[tmp2[0]] = tmp2[1];}
              if (q['callback']){window.opener[q['callback']](q['token']);}else{window.opener.redirect(q['token'], q['redirect_uri']);}window.close();
              </script></body></html><?
            $sReceiverPageContent = ob_get_contents();
            ob_end_clean();

            echo $sReceiverPageContent;
          die();
        }
    }

    /**
     * @param boolean $sRedirectUrl
     */
    public function setSRedirectUrl($sRedirectUrl)
    {
        $this->arResult['redirect_uri'] = (!empty($sRedirectUrl)) ?  urlencode($sRedirectUrl) :
            urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    }

    /**
     * @param array $arFieldsSoc
     */
    public function setArFieldsSoc($arFieldsSoc)
    {
        $this->arResult['soc_fields'] = (!empty($arFieldsSoc)) ? implode(",",$arFieldsSoc) : implode(",",$this->arFieldsSoc);
    }

    /**
     * Роутинг действий.
     */
    public function doActions()
    {
        /* receiver= */
        $this->doReceiver();
        /* Обрабатываем */
        $this->getResponse();
    }
    

    /**
     * @return mixed
     */
    public function executeComponent()
    {
        /* Инициализируем параметры. */
        $this->initParams();
        /* Выполняем действия */
        $this->doActions();
        $this->arResult['ERROR']   = implode("<br>",$this->getError());
        $this->arResult['SUCCESS'] = implode("<br>",$this->getSuccess());

        if (!empty($this->arResult['ERROR'])) {
            ADV_set_flashdata("auth_error", $this->arResult['ERROR']);
            if (isset($this->arParams['REGISTRATION'])) {
                if ($this->arParams['REGISTRATION'] == "Y") {
                    LocalRedirect("/auth/?register=yes");
                }
            } else {
                if (isset($this->arParams['ORDER_PAGE']) && $this->arParams['ORDER_PAGE'] == 'Y') {
                    LocalRedirect("/order/confirm/");
                } else {
                    LocalRedirect("/auth/?backurl=/");
                }
            }
        }
        if (!empty($this->arResult['SUCCESS'])) {
            ADV_set_flashdata("AUTH_MESSAGE", $this->arResult['SUCCESS']);
            if (isset($this->arParams['REGISTRATION'])) {
                if ($this->arParams['REGISTRATION'] == "Y") {
                    LocalRedirect("/auth/?register=yes");
                }
            } else {
                if (isset($this->arParams['ORDER_PAGE']) && $this->arParams['ORDER_PAGE'] == 'Y') {
                    LocalRedirect("/order/confirm/");
                } else {
                    LocalRedirect("/auth/?backurl=/");
                }
            }
        }

        /** @var $this Вывод в шаблон */
        $this->IncludeComponentTemplate();
    }


    /**
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param array $error
     */
    public function setError($error)
    {
        $this->error[] = $error;
    }

    /**
     * @return array
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param array $success
     */
    public function setSuccess($success)
    {
        $this->success[] = $success;
    }

    /**
     * @param string $backurl
     */
    public function setBackurl($backurl)
    {
        $this->backurl = (!empty($backurl)) ? $backurl : $this->backurl;
    }

    /**
     * @param array $noAuthGroup
     */
    public function setNoAuthGroup($noAuthGroup)
    {
        $this->noAuthGroup = (!empty($noAuthGroup)) ? $noAuthGroup : $this->noAuthGroup;
    }

    /**
     * @return boolean
     */
    public function getBOrderPage()
    {
        return $this->bOrderPage;
    }

    /**
     * @param boolean $bOrderPage
     */
    public function setBOrderPage($bOrderPage)
    {
        $this->bOrderPage = $bOrderPage;
    }


}
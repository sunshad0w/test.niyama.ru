<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$iOrderId = isset($_REQUEST['ORDER_ID']) ? intval($_REQUEST['ORDER_ID']) : 0;
if($iOrderId <= 0) {
	$iOrderId = isset($_REQUEST['ID']) ? intval($_REQUEST['ID']) : 0;
}
if($iOrderId <= 0) {
	$iOrderId = isset($_REQUEST['cf']) ? intval($_REQUEST['cf']) : 0;
}
$_REQUEST['ORDER_ID'] = $iOrderId;

if($iOrderId && $arOrder = CSaleOrder::GetById($iOrderId)) {
	// ��������� ��������� ���� /local/modules/adv.acquiropay/public/acquiring/result_rec.php
	$APPLICATION->IncludeComponent(
		'bitrix:sale.order.payment.receive',
		'',
		array(
			'PAY_SYSTEM_ID' => $arOrder['PAY_SYSTEM_ID'],
			'PERSON_TYPE_ID' => $arOrder['PERSON_TYPE_ID']
		),
		null,
		array(
			'HIDE_ICONS' => 'Y'
		)
	);
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');

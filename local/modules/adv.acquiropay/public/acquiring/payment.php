<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule('adv.acquiropay') || !CModule::IncludeModule('sale')) {
	return;
}
$sHostUrl = 'http://'.$_SERVER['HTTP_HOST'];

$iOrderId = CSalePaySystemAction::GetParamValue('ORDER_ID');
$dOrderPrice = number_format(CSalePaySystemAction::GetParamValue('SHOULD_PAY'), 2, '.', '');

$sRequestUrl = CSalePaySystemAction::GetParamValue('URL_PAY');

$sCallbackUrl = trim(CSalePaySystemAction::GetParamValue('CB_URL'));
if(strlen($sCallbackUrl)) {
	$sCallbackUrl .= (strpos($sCallbackUrl, '?') !== false ? '&' : '?').'ORDER_ID='.$iOrderId;
	$sCallbackUrl = strpos($sCallbackUrl, 'http') === 0 ? $sCallbackUrl : $sHostUrl.$sCallbackUrl;
}

$sOkUrl = CSalePaySystemAction::GetParamValue('SUCCESS_URL');
$sOkUrl = strpos($sOkUrl, 'http') === 0 ? $sOkUrl : $sHostUrl.$sOkUrl;

$sFailUrl = CSalePaySystemAction::GetParamValue('DECLINE_URL');
$sFailUrl = strpos($sFailUrl, 'http') === 0 ? $sFailUrl : $sHostUrl.$sFailUrl;

$arPostParams = array(
	'product_id' => CSalePaySystemAction::GetParamValue('PRODUCT_ID'),
	'product_name' => CSalePaySystemAction::GetParamValue('PRODUCT_NAME').CSalePaySystemAction::GetParamValue('ORDER_ID'),
	'amount' => $dOrderPrice,
	'cf' => $iOrderId,
	'cf2' => $dOrderPrice,
	'cf3' => '',
	'cb_url' => $sCallbackUrl,
	'ok_url' => $sOkUrl,
	'ko_url' => $sFailUrl,
	'first_name' => CSalePaySystemAction::GetParamValue('F_NAME'),
	'last_name' => CSalePaySystemAction::GetParamValue('S_NAME'),
	'email' => CSalePaySystemAction::GetParamValue('EMAIL'),
	'street' => '',
	'city' => '',
	'state' => '',
	'zip' => '',
	'country' => '',
	'phone' => '',
	'token' => '',
);
// MD5(merchant_id + product_Id + [amount] + cf + cf2 + cf3 + secret_word4)
$arPostParams['token'] = md5(CSalePaySystemAction::GetParamValue('MERCHANT_ID').$arPostParams['product_id'].$arPostParams['amount'].$arPostParams['cf'].$arPostParams['cf2'].$arPostParams['cf3'].CSalePaySystemAction::GetParamValue('SW'));

$sTmp = trim(CSalePaySystemAction::GetParamValue('PP_IDENTITY'));
if(strlen($sTmp)) {
	$arPostParams['pp_identity'] = $sTmp;
}

?><form id="acquiropayform" action="<?=$sRequestUrl?>" method="post" accept-charset="UTF-8"><?
	foreach($arPostParams as $sFieldName => $sVal) {
		?><input type="hidden" name="<?=$sFieldName?>" value="<?=$sVal?>"><?
	}
?></form><?

?><script type="text/javascript">document.getElementById('acquiropayform').submit();</script><?

// логирование
if(defined('_ACQUIROPAY_DEBUG_') && _ACQUIROPAY_DEBUG_) {
	if(class_exists('CCustomLog')) {
		$arLogInfo['sRequestUrl'] = $sRequestUrl;
		$arLogInfo['arPostParams'] = $arPostParams;
		$obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/acquiropay/payment/'.date('Y-m-d_H-i-s').'_'.$iOrderId.'.log');
		$obCustomLog->Log($arLogInfo);
	}
}

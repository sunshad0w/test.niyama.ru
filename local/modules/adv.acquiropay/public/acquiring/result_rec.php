<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule('adv.acquiropay') && CModule::IncludeModule('sale')) {
	return;
}

$arLogInfo = array();
$arLogInfo['_REQUEST'] = $_REQUEST;
$arLogInfo['_SERVER'] = $_SERVER;

$arResult = array(
	'ERRORS' => array(),
	'PAY_SYSTEM_ID' => intval($arParams['PAY_SYSTEM_ID']),
	'PERSON_TYPE_ID' => intval($arParams['PERSON_TYPE_ID']),
	'ORDER' => array(),
	'FIELDS' => array()
);

$iOrderId = isset($_REQUEST['ORDER_ID']) ? intval($_REQUEST['ORDER_ID']) : 0;
$arResult['ORDER'] = array();
if($iOrderId > 0) {
	$arResult['ORDER'] = CSaleOrder::GetByID($iOrderId);
}

if($arResult['ORDER']) {
	CSalePaySystemAction::InitParamArrays($arResult['ORDER'], $arResult['ORDER']['ID']);

	$bWithdraw = false;
	$arResult['INCOMING_PARAMS'] = array(
		'sign' => isset($_REQUEST['sign']) ? $_REQUEST['sign'] : '',
		'status' => isset($_REQUEST['status']) ? $_REQUEST['status'] : '',
		'payment_id' => isset($_REQUEST['payment_id']) ? $_REQUEST['payment_id'] : '',
		'product_id' => isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : '',
		'cf' => isset($_REQUEST['cf']) ? $_REQUEST['cf'] : '',
		'cf2' => isset($_REQUEST['cf2']) ? $_REQUEST['cf2'] : '',
		'cf3' => isset($_REQUEST['cf3']) ? $_REQUEST['cf3'] : '',
		'amount' => isset($_REQUEST['amount']) ? $_REQUEST['amount'] : '',
		'pp_identity' => isset($_REQUEST['pp_identity']) ? $_REQUEST['pp_identity'] : '',
	);

	// MD5(merchaint_id + payment_id + status + cf + cf2 + cf3 + secret_word)
	// status - Uppercase like received
	$arResult['CHECK_TOKEN'] = md5(CSalePaySystemAction::GetParamValue('MERCHANT_ID').$arResult['INCOMING_PARAMS']['payment_id'].$arResult['INCOMING_PARAMS']['status'].$arResult['INCOMING_PARAMS']['cf'].$arResult['INCOMING_PARAMS']['cf2'].$arResult['INCOMING_PARAMS']['cf3'].CSalePaySystemAction::GetParamValue('SW'));

	$iProductId = intval(CSalePaySystemAction::GetParamValue('PRODUCT_ID'));

	if($arResult['INCOMING_PARAMS']['sign'] != $arResult['CHECK_TOKEN']) {
		$arResult['ERRORS']['ERR_WRONG_TOKEN'] = 'ERR_WRONG_TOKEN';
	} elseif($iProductId <= 0) {
		$arResult['ERRORS']['ERR_WRONG_PRODUCT_ID'] = 'ERR_WRONG_PRODUCT_ID';
	}

	if(!$arResult['ERRORS']) {

		$sRequestUrl = CSalePaySystemAction::GetParamValue('URL_GATE');

		// запрос выписки по платежам
		/*
		$sVars = 'opcode=check_status';
		$sVars .= '&product_id='.$iProductId;
		$sVars .= '&date_from='.date('Y-m-d\TH:i:s', strtotime($arResult['ORDER']['DATE_STATUS']) - 3600);
		$sVars .= '&date_to='.date('Y-m-d\TH:i:s', strtotime($arResult['ORDER']['DATE_STATUS']) + 3600 * 72);
		$sVars .= '&additional_info=true';
		$sVars .= '&token='.md5(CSalePaySystemAction::GetParamValue('MERCHANT_ID').$arResult['INCOMING_PARAMS']['product_id'].$arResult['INCOMING_PARAMS']['cf'].$arResult['INCOMING_PARAMS']['cf2'].$arResult['INCOMING_PARAMS']['cf3'].CSalePaySystemAction::GetParamValue('SW'));

		$arResult['RESPONSE'] = file_get_contents($sRequestUrl.'?'.$sVars);

		if(strlen($arResult['RESPONSE'])) {
			$obXmlResult = @new SimpleXMLElement($arResult['RESPONSE']);
			if(!$obXmlResult) {
				$arResult['ERRORS']['ERR_WRONG_RESPONSE_XML_DATA'] = 'ERR_WRONG_RESPONSE_XML_DATA';
			} elseif(isset($obXmlResult->payments->payment)) {
				$obXmlResult = (array)$obXmlResult->payments;
				if(is_array($obXmlResult['payment']) && count($obXmlResult['payment'])) {
					foreach($obXmlResult['payment'] as $arRespPayment) {
						$arRespPayment = (array)$arRespPayment;
						$arRespPayment['additional'] = (array)$arRespPayment['additional'];
						if($arRespPayment['additional']['cf'] == $arResult['ORDER']['ID']) {
							if($arRespPayment['status'] == 'OK' && in_array($arRespPayment['extended_status'], array('PURCHASE', 'CAPTURE'))) {
								$sDescription = '';
								$sStatusMessage = '';
								$dRespAmount = 0;
								$dRespCurrency = '';
								$arResult['FIELDS'] = array(
									'PS_STATUS' => 'Y',
									'PS_STATUS_CODE' => '-',
									'PS_STATUS_DESCRIPTION' => $sDescription,
									'PS_STATUS_MESSAGE' => $sStatusMessage,
									'PS_SUM' => $dRespAmount,
									'PS_CURRENCY' => $dRespCurrency,
									'PS_RESPONSE_DATE' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
									'USER_ID' => $arResult['ORDER']['USER_ID']
								);

								if($arResult['ORDER']['PAYED'] != 'Y') {
									$arResult['FIELDS']['PAYED'] = 'Y';
									$arResult['FIELDS']['DATE_PAYED'] = date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG)));
									$arResult['FIELDS']['EMP_PAYED_ID'] = false;
								} else {
									$arResult['ERRORS']['ERR_ORDER_ALREADY_PAID'] = 'ERR_ORDER_ALREADY_PAID';
								}

								// изменим статус на оплачено
								CSaleOrder::PayOrder($arResult['ORDER']['ID'], 'Y', $bWithdraw, true, 0, $arResult['FIELDS']);
								CSaleOrder::Update($arResult['ORDER']['ID'], $arResult['FIELDS']);
								break;
							}
						}
					}
				}
			}
		} else {
			$arResult['ERRORS']['ERR_WRONG_RESPONSE_DATA'] = 'ERR_WRONG_RESPONSE_DATA';
		}
		*/

		$sVars .= '?opcode=2';
		$sVars .= '&payment_id='.urlencode($arResult['INCOMING_PARAMS']['payment_id']);
		$sVars .= '&additional_info=true';
		$sVars .= '&token='.md5(CSalePaySystemAction::GetParamValue('MERCHANT_ID').$arResult['INCOMING_PARAMS']['payment_id'].CSalePaySystemAction::GetParamValue('SW'));
		$arResult['REQUEST_URL'] = $sRequestUrl.$sVars;
		$arResult['RESPONSE'] = file_get_contents($arResult['REQUEST_URL']);


		if($arResult['RESPONSE']) {
			$obXmlResult = @new SimpleXMLElement($arResult['RESPONSE']);
			if($obXmlResult) {
				$sTmp = (string)$obXmlResult->additional->cf;
				$iOrderId = intval($sTmp);
				if($iOrderId == $arResult['ORDER']['ID']) {

					// сумма заказа
					$dOrderAmount = 0;
					$sTmpOrderAmountXml = (string)$obXmlResult->additional->cf2;
					if($sTmpOrderAmountXml) {
						$dOrderAmount = str_replace(',', '.', $sTmpOrderAmountXml);
						$dOrderAmount = doubleval(preg_replace('/[^\d\-\.]/i', '', $dOrderAmount));
					}
					$dOrderAmount = $dOrderAmount > 0 ? $dOrderAmount : 0;
					if(!$dOrderAmount && $arResult['INCOMING_PARAMS']['amount']) {
						$dOrderAmount = str_replace(',', '.', $arResult['INCOMING_PARAMS']['amount']);
						$dOrderAmount = doubleval(preg_replace('/[^\d\-\.]/i', '', $dOrderAmount));
					}
					$dOrderAmount = $dOrderAmount > 0 ? $dOrderAmount : 0;

					$arResult['FIELDS'] = array(
						'PS_STATUS' => 'N',
						'PS_STATUS_CODE' => (string)$obXmlResult->status,
						'PS_STATUS_DESCRIPTION' => (string)$obXmlResult->extended_status,
						'PS_STATUS_MESSAGE' => trim($arResult['INCOMING_PARAMS']['pp_identity']),
						'PS_SUM' => $dOrderAmount,
						'PS_CURRENCY' => '',
						'PS_RESPONSE_DATE' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
						'USER_ID' => $arResult['ORDER']['USER_ID']
					);

					if($arResult['FIELDS']['PS_STATUS_CODE'] == 'OK' && in_array($arResult['FIELDS']['PS_STATUS_DESCRIPTION'], array('PURCHASE', 'CAPTURE'))) {
						$arResult['FIELDS']['PS_STATUS'] = 'Y';
					}

					if($arResult['FIELDS']['PS_STATUS'] == 'Y' && $dOrderAmount < $arResult['ORDER']['PRICE']) {
						$arResult['FIELDS']['PS_STATUS'] = 'N';
						$arResult['ERRORS']['ERR_ORDER_AMOUNT_FAIL'] = 'ERR_ORDER_AMOUNT_FAIL';
					}

					if($arResult['FIELDS']['PS_STATUS'] == 'Y') {
						if($arResult['ORDER']['PAYED'] != 'Y') {
							$arResult['FIELDS']['PAYED'] = 'Y';
							$arResult['FIELDS']['DATE_PAYED'] = $arResult['FIELDS']['PS_RESPONSE_DATE'];
							$arResult['FIELDS']['EMP_PAYED_ID'] = false;
							CSaleOrder::PayOrder($arResult['ORDER']['ID'], 'Y', $bWithdraw, true, 0, $arResult['FIELDS']);
						} else {
							$arResult['ERRORS']['ERR_ORDER_ALREADY_PAID'] = 'ERR_ORDER_ALREADY_PAID';
						}
					}
					CSaleOrder::Update($arResult['ORDER']['ID'], $arResult['FIELDS']);
				} else {
					$arResult['ERRORS']['ERR_ORDER_ID_NOT_EQUAL'] = 'ERR_ORDER_ID_NOT_EQUAL';
				}
			} else {
				$arResult['ERRORS']['ERR_WRONG_RESPONSE_XML_DATA'] = 'ERR_WRONG_RESPONSE_XML_DATA';
			}
		} else {
			$arResult['ERRORS']['ERR_WRONG_RESPONSE_DATA'] = 'ERR_WRONG_RESPONSE_DATA';
		}
	}
} else {
	$arResult['ERRORS']['ERR_ORDER_NOT_FOUND'] = 'ERR_ORDER_NOT_FOUND';
}

// логирование
if(defined('_ACQUIROPAY_DEBUG_') && _ACQUIROPAY_DEBUG_) {
	if(class_exists('CCustomLog')) {
		$arLogInfo['arResult'] = $arResult;
		$obCustomLog = new CCustomLog($_SERVER['DOCUMENT_ROOT'].'/_log/acquiropay/result_rec/'.date('Y-m-d_H-i-s').'_'.$iOrderId.'.log');
		$obCustomLog->Log($arLogInfo);
	}
}

return $arResult;

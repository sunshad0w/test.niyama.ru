<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED !== true)die();
if(!CModule::IncludeModule('adv.acquiropay')) {
	return;
}

IncludeModuleLangFile(__FILE__);

$psTitle = GetMessage('ADV_ACQUIROPAY_OPT_PS_TITLE');
$psDescription = GetMessage('ADV_ACQUIROPAY_OPT_PS_DESCRIPTION');

$arPSCorrespondence = array(
	'URL_PAY' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_URL_PAY'),
		'DESCR' => '',
		'VALUE' => 'https://secure.acquiropay.com/',
		'TYPE' => ''
	),
	'URL_GATE' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_URL_PAY'),
		'DESCR' => '',
		'VALUE' => 'https://gateway.acquiropay.com/',
		'TYPE' => ''
	), 
	'SW' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_SECRET_WORD'),
		'DESCR' => '',
		'VALUE' => '',
		'TYPE' => ''
	),
	'MERCHANT_ID' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_MERCHANT'),
		'DESCR' => '',
		'VALUE' => '',
		'TYPE' => ''
	),
	'PRODUCT_ID' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_PRODUCT'),
		'DESCR' => '',
		'VALUE' => '0',
		'TYPE' => ''
	),
	'PP_IDENTITY' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_PP_IDENTITY'),
		'DESCR' => '',
		'VALUE' => '',
		'TYPE' => ''
	),

	'SHOULD_PAY' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_AMOUNT'),
		'DESCR' => '',
		'VALUE' => 'SHOULD_PAY',
		'TYPE' => 'ORDER'
	), 
	'PRODUCT_NAME' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_PRODUCT_NAME'),
		'DESCR' => '',
		'VALUE' => 'Order #',
		'TYPE' => ''
	),
	'ORDER_ID' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_ORDER_ID'),
		'DESCR' => '',
		'VALUE' => 'ID',
		'TYPE' => 'ORDER'
	),
	'CB_URL' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_CB_URL'),
		'DESCR' => '',
		'VALUE' => 'http://'.$_SERVER['HTTP_HOST'].'/order/pay/payment_result.php',
		'TYPE' => ''
	),
	'SUCCESS_URL' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_OK_URL'),
		'DESCR' => '',
		'VALUE' => 'http://'.$_SERVER['HTTP_HOST'].'/order/pay/payment_success.php',
		'TYPE' => ''
	),
	'DECLINE_URL' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_KO_URL'),
		'DESCR' => '',
		'VALUE' => 'http://'.$_SERVER['HTTP_HOST'].'/order/pay/payment_failed.php',
		'TYPE' => ''
	),
	'F_NAME' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_FNAME'),
		'DESCR' => '',
		'VALUE' => 'NAME',
		'TYPE' => 'USER'
	),
	'S_NAME' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_LNAME'),
		'DESCR' => '',
		'VALUE' => 'LAST_NAME',
		'TYPE' => 'USER'
	),
	'EMAIL' => array(
		'NAME' => GetMessage('ADV_ACQUIROPAY_OPT_EMAIL'),
		'DESCR' => '',
		'VALUE' => 'EMAIL',
		'TYPE' => 'USER'
	),
);

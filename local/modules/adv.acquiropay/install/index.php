<?
/**
 * ���������� ������ adv.acquiropay
 *
 * @author Sergey Leshchenko, 2014
 */

IncludeModuleLangFile(__FILE__);
//$sPath2Lang = str_replace('\\', '/', __FILE__);
//$sPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/install/index.php'));
//include(GetLangFileName($sPath2Lang.'/lang/', '/install.php'));


class adv_acquiropay extends CModule {
	public $MODULE_ID = "adv.acquiropay";
	public $sModulePrefix = 'ADV_ACQUIROPAY_';
	public $sComponentsDir = 'acquiropay';
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $PARTNER_NAME;
	public $PARTNER_URI;
	public $errors;
	protected $sBxDir = 'bitrix';
	public $sPath2UserPSFiles;
	public $sPaymentDir = 'acquiropay_adv';

	public function __construct() {
		$this->errors = array();
		$arPathInfo = pathinfo(str_replace('\\', '/', __FILE__));
		include($arPathInfo['dirname'].'/version.php');

		// ��������� ����� ���������: local ��� bitrix
		$sTmpModulePath = '/modules/'.$this->MODULE_ID.'/install';
		$arDirs = array_reverse(explode('/', str_replace('\\', '/', substr(__DIR__, 0, '-'.strlen($sTmpModulePath)))));
		if($arDirs && $arDirs[0] == 'local') {
			$this->sBxDir = 'local';
		}

		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

		$this->MODULE_NAME = $this->GetMessage('MODULE_NAME');
		$this->MODULE_DESCRIPTION = $this->GetMessage('MODULE_DESCRIPTION');

		// ��� ������� ������������ � ����� �������� �������
		$this->PARTNER_NAME = GetMessage("PARTNER_NAME");

		$this->PARTNER_NAME = $this->GetMessage('PARTNER_NAME');
		$this->PARTNER_URI = 'http://www.adv.ru/';

		$sPath2UserPSFiles = COption::GetOptionString('sale', 'path2user_ps_files', $this->sBxDir.'/php_interface/include/sale_payment/');
		$sPath2UserPSFiles = '/'.trim(trim($sPath2UserPSFiles), '/'); // ���� � ����� �� �����
		$this->sPath2UserPSFiles = str_replace('//', '/', $sPath2UserPSFiles);
	}

	public function GetMessage($sMessageCode, $arReplace = false) {
		return GetMessage($this->sModulePrefix.$sMessageCode, $arReplace);
	}

	public function DoInstall() {
		/*patchinstallmutatormark1*/

		$GLOBALS['mErrors'] = false;
		$GLOBALS['step'] = intval($GLOBALS['step']);
		$GLOBALS['_INSTALLING_MODULE_OBJ_'] =& $this;
		$iIncludeStepFile = 1;
		switch($GLOBALS['step']) {
			default:
				$iIncludeStepFile = 1;
				$this->CheckRequiredModules();
			break;
			case 2:
				$iIncludeStepFile = 2;
				if($this->InstallDB()) {
					RegisterModule($this->MODULE_ID);
					$this->InstallFiles();
					if(empty($this->errors)) {
						$this->InstallModuleDependences();
						$this->InstallDefaultOptions();
					} else {
						$this->UnInstallFiles();
						UnRegisterModule($this->MODULE_ID);
					}
				}
			break;
		}
		if($iIncludeStepFile) {
			$GLOBALS['mErrors'] = $this->errors;
			$GLOBALS['sPath2UserPSFiles'] = $this->sPath2UserPSFiles;
			$GLOBALS['APPLICATION']->IncludeAdminFile($this->GetMessage('INSTALL_TITLE'), $_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/step'.$iIncludeStepFile.'.php');
		}
	}

	public function DoUninstall() {
		$GLOBALS['mErrors'] = false;
		$GLOBALS['step'] = intval($GLOBALS['step']);
		$GLOBALS['sPath2UserPSFiles'] = $this->sPath2UserPSFiles;
		$GLOBALS['_INSTALLING_MODULE_OBJ_'] = $this;

		if(!$this->CheckUnistall()) {
			$GLOBALS['step'] = 1;
		}
		if($GLOBALS['step'] < 2) {
			$GLOBALS['mErrors'] = $this->errors;
			$GLOBALS['APPLICATION']->IncludeAdminFile($this->GetMessage('UNINSTALL_TITLE'), $_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/unstep1.php');
		} else {
			// ������� ���� � �����
			$this->UnInstallDB(
				array(
					'savedata' => $_REQUEST['savedata'],
					'saveoptions' => $_REQUEST['saveoptions']
				)
			);
			// ������� �����
			$this->UnInstallFiles(
				array(
					'savefiles' => $_REQUEST['savefiles']
				)
			);

			$GLOBALS['mErrors'] = $this->errors;

			$GLOBALS['CACHE_MANAGER']->CleanAll();
			$GLOBALS['stackCacheManager']->CleanAll();
			$GLOBALS['APPLICATION']->IncludeAdminFile($this->GetMessage('UNINSTALL_TITLE'), $_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/unstep2.php');
		}
	}

	public function CheckRequiredModules() {
		return true;
	}

	public function CheckUnistall() {
		$bResult = true;
		return $bResult;
	}

	public function InstallDB() {
		/*patchinstallmutatormark2*/
		if(!empty($this->errors)) {
			$GLOBALS['APPLICATION']->ThrowException(implode('', $this->errors));
			return false;
		}
		return true;
	}

	public function UnInstallDB($arParams = null) {
		$arParams = $arParams === null ? array() : $arParams;
		$this->UnInstallTriggerModuleDependences();
		$this->UnInstallOptions($arParams);
		$this->UnInstallModuleDependences();
		UnRegisterModule($this->MODULE_ID);
		return true;
	}

	public function InstallFiles() {
		$bRewriteFiles = true;
		CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/sale_payment/', $_SERVER['DOCUMENT_ROOT'].$this->sPath2UserPSFiles, $bRewriteFiles, true);

		// module admin files
		//CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true);
		// themes
		//CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/themes/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes', true, true);
	}

	public function UnInstallFiles($arParams = null) {
		$arParams = $arParams === null ? array() : $arParams;
		DeleteDirFilesEx($this->sPath2UserPSFiles.'/'.$this->sPaymentDir.'/');

		// module admin files
		//DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
		// themes
		//DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/'.$this->sBxDir.'/modules/'.$this->MODULE_ID.'/install/themes/.default/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default');
	}

	public function InstallDefaultOptions() {
		// ������� ����� �� ��������� (������������ ��������)
		//if(CModule::IncludeModule($this->MODULE_ID)) {
		//}
	}

	public function UnInstallOptions($arParams = null) {
		$arParams = $arParams === null ? array() : $arParams;
		//$bSave = !empty($arParams['saveoptions']) && $arParams['saveoptions'] == 'Y';
		//if(!$bSave) {
		//	// ������ ����� ������
		//	COption::RemoveOption($this->MODULE_ID);
		//}
	}

	public function UnInstallTriggerModuleDependences() {
	}

	public function InstallModuleDependences() {
		// ��������������� ������
		//RegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, '', '', 1);
	}

	public function UnInstallModuleDependences() {
		// �������� ��������������� ������
		//UnRegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, '', '');
	}
}
?>
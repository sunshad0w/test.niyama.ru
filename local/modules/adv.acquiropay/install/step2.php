<?
if(!check_bitrix_sessid()) {
	return;
}

if(!empty($GLOBALS['_INSTALLING_MODULE_OBJ_'])) {
	$obModule =& $GLOBALS['_INSTALLING_MODULE_OBJ_'];
	$sModuleId = $obModule->MODULE_ID;
} else {
	return;
}

if(empty($GLOBALS['mErrors'])) {
	echo CAdminMessage::ShowNote($obModule->GetMessage('INSTALL_COMPLETE_OK'));
} else {
	$sErrors = 'Error while installing';
	if(is_array($GLOBALS['mErrors'])) {
		$sErrors = '';
		foreach($GLOBALS['mErrors'] as $sErrMsg) {
			$sErrors .= $sErrMsg.'<br />';
		}
	}
	echo CAdminMessage::ShowMessage(
		array(
			'TYPE' => 'ERROR', 
			'MESSAGE' => $obModule->GetMessage('INSTALL_COMPLETE_ERROR'), 
			'DETAILS' => $sErrors, 
			'HTML' => true
		)
	);
	unset($sErrors);
}

?><div style="clear:both"></div><?
?><form style="float:left;" action="<?=$GLOBALS['APPLICATION']->GetCurPage()?>"><div><?
	?><input type="hidden" name="lang" value="<?=LANG?>" /><?
	?><p><?
		?><input type="submit" name="" value="<?=$obModule->GetMessage('INSTALL_BACK')?>" /><?
	?></p><?
?></div></form><?

/*
if(empty($GLOBALS['mErrors'])) {
	$sSettingsUrl = '/bitrix/admin/settings.php';
	?><form style="float:left; margin-left: 10px;" action="<?=$sSettingsUrl?>"><?
		?><input type="hidden" name="lang" value="<?=LANG?>" /><?
		?><input type="hidden" name="mid" value="<?=$sModuleId?>" /><?
		?><input type="hidden" name="mid_menu" value="1" /><?
		?><input type="hidden" name="backurl" value="<?=$GLOBALS['APPLICATION']->GetCurPage()?>" /><?
		?><p><?
			?><input type="submit" name="" value="<?=$obModule->GetMessage('CHANGE_MODULE_OPTIONS')?>" /><?
		?></p><?
	?></form><?
}
*/

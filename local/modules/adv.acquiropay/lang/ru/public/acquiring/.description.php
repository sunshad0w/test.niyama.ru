<?
global $MESS;

$MESS['ADV_ACQUIROPAY_OPT_PS_TITLE'] = 'AcquiroPay (adv)';
$MESS['ADV_ACQUIROPAY_OPT_DESCRIPTION'] = 'Прием платежей в режиме интернет-эквайринга через систему AcquiroPay';

$MESS['ADV_ACQUIROPAY_OPT_MERCHANT'] = 'ID торговца';
$MESS['ADV_ACQUIROPAY_OPT_PRODUCT'] = 'ID продукта';
$MESS['ADV_ACQUIROPAY_OPT_PRODUCT_NAME'] = 'Назначение платежа "Оплата за ..."';
$MESS['ADV_ACQUIROPAY_OPT_SECRET_WORD'] = 'Секретное слово';
$MESS['ADV_ACQUIROPAY_OPT_URL_PAY'] = 'URL отправки платежей';
$MESS['ADV_ACQUIROPAY_OPT_URL_GATE'] = 'URL получения статуса платежа';
$MESS['ADV_ACQUIROPAY_OPT_CB_URL'] = 'URL результата';
$MESS['ADV_ACQUIROPAY_OPT_OK_URL'] = 'Success URL';
$MESS['ADV_ACQUIROPAY_OPT_KO_URL'] = 'Error URL';

$MESS['ADV_ACQUIROPAY_OPT_AMOUNT'] = 'Сумма к оплате';
$MESS['ADV_ACQUIROPAY_OPT_ORDER_ID'] = 'Номер заказа';
$MESS['ADV_ACQUIROPAY_OPT_LNAME'] = 'Фамилия';
$MESS['ADV_ACQUIROPAY_OPT_FNAME'] = 'Имя';
$MESS['ADV_ACQUIROPAY_OPT_EMAIL'] = 'E-mail';
$MESS['ADV_ACQUIROPAY_OPT_PP_IDENTITY'] = 'Идентификатор платежной системы<small><br />card - Банковские карты<br />wm - WebMoney<br />qiwi - QIWI Кошелек<br />rbkmoney - RBK Money<br />yandexmoney - Яндекс.Деньги<br />mailru - Деньги@Mail.ru<br />euroset - Евросеть<br />rapida - Rapida<br />contact - CONTACT<br />western_union - Western Union<br />mobpay - Мобильный платеж<br />mobpay_beeline - Мобильный платеж Билайн<br />mobpay_megafon - Мобильный платеж Мегафон<br />mobpay_mts - Мобильный платеж МТС<br />eleksnet - Терминалы Элекснет</small>';

<?
$MESS['ADV_ACQUIROPAY_MODULE_NAME'] = 'Платёжная система AcquiroPay (adv)';
$MESS['ADV_ACQUIROPAY_MODULE_DESCRIPTION'] = 'Оплата товаров и услуг с помощью банковских карт и небанковских платежных систем';
$MESS['ADV_ACQUIROPAY_INSTALL_COMPLETE_OK'] = 'Установка завершена.';
$MESS['ADV_ACQUIROPAY_INSTALL_COMPLETE_ERROR'] = 'Установка завершена с ошибками';
$MESS['ADV_ACQUIROPAY_INSTALL_PUBLIC_SETUP'] = 'Установить';
$MESS['ADV_ACQUIROPAY_INSTALL_TITLE'] = 'Установить модуль "AcquiroPay"';
$MESS['ADV_ACQUIROPAY_INSTALL_START_TITLE'] = 'Установка модуля "AcquiroPay"';
$MESS['ADV_ACQUIROPAY_UNINSTALL_TITLE'] = 'Удаление модуля "AcquiroPay"';
$MESS['ADV_ACQUIROPAY_UNINSTALL_WARNING'] = 'Внимание! Модуль будет удален из системы.';
$MESS['ADV_ACQUIROPAY_MOD_UNINST_SAVE_OPTIONS'] = 'Сохранить настройки модуля';

$MESS['ADV_ACQUIROPAY_UNINSTALL_DEL'] = 'Удалить';
$MESS['ADV_ACQUIROPAY_UNINSTALL_ERROR'] = 'Ошибки при удалении:';
$MESS['ADV_ACQUIROPAY_UNINSTALL_COMPLETE'] = 'Удаление завершено.';
$MESS['ADV_ACQUIROPAY_INSTALL_BACK'] = 'Вернуться к списку модулей';
$MESS['ADV_ACQUIROPAY_CHANGE_MODULE_OPTIONS'] = 'Перейти к настройкам модуля';


$MESS['ADV_ACQUIROPAY_MOD_STEP_LIST'] = 'Вернуться';
$MESS['ADV_ACQUIROPAY_MOD_STEP_2'] = 'Установить модуль';

$MESS['ADV_ACQUIROPAY_PARTNER_NAME'] = "ADV/web-engineering";
$MESS['ADV_ACQUIROPAY_PARTNER_SITE'] = "http://www.adv.ru";


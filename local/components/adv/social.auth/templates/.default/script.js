/**
 * @author: Kazim Bayramukov
 * @email: bayramukov@adv.ru
 * @copyright Copyright (c) 2014, ADV/web-engineering co.
 */


function openWindow(url, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open(url, '', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}



//авторизация и регистрация из соц сети
$('.social._auth').on('click', '.social__item', function() {
    var that = this;
    var site = $(this).data('site').trim();
    objSite = SOCIAL_SITES_PARAMS[site];
    openWindow(objSite.DIALOG_URL, objSite.WIDTH, objSite.HEIGHT);
});
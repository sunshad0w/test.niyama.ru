<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
?>

<div class="social _auth">
    <div class="_facebook social__item" data-site="facebook" ></div>
    <div class="_vk social__item" data-site="vkontakte"></div>
    <div class="_google social__item" data-site="googleplus"></div>
    <div class="_mail social__item" data-site="mailru"></div>
    <div class="_ya social__item" data-site="yandex" ></div>
</div>
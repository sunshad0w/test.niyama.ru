<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
?>
<h1 class="auth__title align-center">Привязка аккаунта</h1>
<div class="auth">
    <div class="auth__center">
        <form data-parsley-validate name="form_auth" method="post" target="_top"><?
            if(isset($_REQUEST['backurl'])) {
                ?><input type="hidden" name="backurl" value="<?= $_REQUEST['backurl'] ?>" /><?
            }
            ?><label class="label auth__label">Имя учётной записи</label><?
            $sClass = "";
            if(!empty($arResult['ERROR_MESSAGE'])) {
                $sClass = "parsley-error";
            }
            if($err_txt = ADV_get_flashdata("auth_error")) {
                $sClass = "parsley-error";
            }
            if($err_txtAUTH_MESSAGE = ADV_get_flashdata("AUTH_MESSAGE")) {
                $sClass = "parsley-error";
            }
            if(!empty($arParams['~AUTH_RESULT']['MESSAGE'])) {
                $sClass = "parsley-error";
            }

            ?><input required  class="input auth__input <?= $sClass ?>" name="USER_LOGIN" maxlength="255" value="<?=$arResult['LAST_LOGIN']?>" /><?

            if(!empty($arResult['ERROR_MESSAGE'])) {
                ?><div class="auth__error"><?
                echo implode("<br>",$arResult['ERROR_MESSAGE']);
                ?></div><?
            }
            if($err_txt = ADV_get_flashdata("auth_error")) {
                ?><div class="auth__error"><?
                echo $err_txt;
                ?></div><?
            }
            if($err_txtAUTH_MESSAGE = ADV_get_flashdata("AUTH_MESSAGE")) {
                ?><div class="auth__error"><?
                echo $err_txtAUTH_MESSAGE;
                ?></div><?
            }
            if(!empty($arParams['~AUTH_RESULT']['MESSAGE'])) {
                ?><div class="auth__error"><?
                echo $arParams['~AUTH_RESULT']['MESSAGE'];
                ?></div><?
            }


            ?><a href="/auth/?forgot_password=yes" class="auth__fogot pull-right">Забыли пароль?</a>
            <label class="label auth__label">Пароль</label>
            <input required  class="input auth__input" type="password" name="USER_PASSWORD" maxlength="255" />
            <input type="submit" value="Привязать" class="btn _style_2 auth__submit" />
        </form>
    </div>
</div><?
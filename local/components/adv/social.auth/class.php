<?php
/**
 * Авторизация через социальные сети
 *
 * @author: Kazim Bayramukov
 * @email: bayramukov@adv.ru
 * @copyright Copyright (c) 2014, ADV/web-engineering co.
 */

use ADV\Authentication\Adapter\AdapterFactory;
use ADV\Authentication\Adapter\UserAbstract;


class CADVloginAuth extends CBitrixComponent {

    public $arSites = array();
    public $redirectUri = 'http://niyama.b.adv.ru/auth/';
    public $backUrl = '';


    public function __construct($component = null) {
        parent::__construct($component);

		\Bitrix\Main\Loader::IncludeModule('socialservices');

        $this->SetRedirectUri('http://'.SITE_SERVER_NAME.'/auth/');

        /** @var $security Получаем объект класса */
        $security = new Bitrix\Security\Filter\Request();

        /** @var $security Устанавливаем аудит на SQL enjection и XSS attack */
        $security->setAuditors(
			array(
	            'SQL' => new Bitrix\Security\Filter\Auditor\Sql(),
	            'XSS' => new Bitrix\Security\Filter\Auditor\Xss()
	        )
		);

        /** @var $arURI фильтруем входящие параметры. */
        $this->uri = $security->filter(
        	array(
	            'get' => $_GET,
	            'request' => $_REQUEST,
	            'post' => $_POST
    	    ), 
    	    false
		);

        // TODO вынести из конструктора
        $this->arSites = array(
            'facebook' => array(
                'client_id' => $this->GetSocServOptionValue('facebook_appid', '749764901738711'),
                'client_secret' => $this->GetSocServOptionValue('facebook_appsecret', '6ab60a3d0285c610c3f09dc1fcf553ab'),
            ),
            'vkontakte' => array(
                'client_id' => $this->GetSocServOptionValue('vkontakte_appid', '4764895'),
                'client_secret' => $this->GetSocServOptionValue('vkontakte_appsecret', '6qFFzr1yPKq9HUm0UMEN'),
            ),
            'yandex' => array(
                'client_id' => CNiyamaCustomSettings::GetStringValue('yandex_oauth_client_id', '4f4c8a70fca74a1fb37bef3234bfce55'),//$this->GetSocServOptionValue('', '5d046dd34b5c4959805ace681e9cc0aa'),
                'client_secret' => CNiyamaCustomSettings::GetStringValue('yandex_oauth_client_secret', 'c0c89e760ec34561ace95754a6e16e88'),//$this->GetSocServOptionValue('', 'fb79679e58584bf5bcd5667b2f76f5a8'),
            ),
            'googleplus' => array(
                'client_id' => $this->GetSocServOptionValue('google_appid', '243330341122-uetkb8tr9baqttlgb2ea9ut8stl1n2mr.apps.googleusercontent.com'),
                'client_secret' => $this->GetSocServOptionValue('google_appsecret', '4xNDohZ2MpfyexJFlHeRP9WE'),
            ),
            'mailru' => array(
                'client_id' => $this->GetSocServOptionValue('mailru_id', '729889'),
                'client_secret' => $this->GetSocServOptionValue('mailru_secret_key', '9ac342fc6750af606671d2b9341d58ea'),
            ),
            'odnoklassniki' => array(
                'client_id' => $this->GetSocServOptionValue('odnoklassniki_appid', '1106558976'),
                'public_key' => $this->GetSocServOptionValue('odnoklassniki_appkey', 'CBABMFPCEBABABABA'),
                'client_secret' => $this->GetSocServOptionValue('odnoklassniki_appsecret', '7ED2B2F69006D2756C612E20'),
            )
        );

		// расчет скидок и бонусов для регистрации через соцсети по умолчанию включаем
		$this->arParams['IGNORE_TIMELINE'] = isset($this->arParams['IGNORE_TIMELINE']) && $this->arParams['IGNORE_TIMELINE'] == 'Y' ? 'Y' : 'N';
		// синхронизацию с FastOperator по умолчанию выключаем
		$this->arParams['SKIP_FO_SYNC'] = isset($this->arParams['SKIP_FO_SYNC']) && $this->arParams['SKIP_FO_SYNC'] == 'N' ? 'N' : 'Y';
    }

	public function SetRedirectUri($sRedirectUri) {
		$this->redirectUri = trim($sRedirectUri);
	}

	public function GetRedirectUri() {
		return $this->redirectUri;
	}

	protected function GetSocServOptionValue($sOptionCode, $sDefValue = '') {
		$sReturn = strlen($sOptionCode) ? trim(CSocServAuth::GetOption($sOptionCode)) : '';
		$sReturn = strlen($sReturn) ? $sReturn : $sDefValue;
		return $sReturn;
	}

    /**
     * Регистрируем нового пользователя
     * @param $arData массив с данными пользователя из соц сети
     * @return int возвращает ID нового пользователя
     * @throws Exception При ошибке добавления нового пользователя
     */
    public function registerNewUser($arData) {
        $password = randString(8);

        $arFields = array(
            'LOGIN' => $arData['LOGIN'],
            'EMAIL' => $arData['EMAIL'],
            'NAME' => $arData['NAME'],
            'LAST_NAME' => $arData['LAST_NAME'],
            'PERSONAL_GENDER' => $arData['PERSONAL_GENDER'],
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $password,
            'ACTIVE' => 'Y',
        );
		if($this->arParams['IGNORE_TIMELINE'] && $this->arParams['IGNORE_TIMELINE'] == 'Y') {
			$arFields['_NIYAMA_IGNORE_TIMELINE_'] = 'Y';
		}
		if($this->arParams['SKIP_FO_SYNC'] && $this->arParams['SKIP_FO_SYNC'] == 'Y') {
			$arFields['_SKIP_FO_SYNC_'] = 'Y';
		}

        $def_group = COption::GetOptionString('main', 'new_user_registration_def_group', '');
        if(strlen($def_group)) {
            $arUserParam['GROUP_ID'] = explode(',', $def_group);
        }

        if(isset($arData['PERSONAL_BIRTHDAY'])) {
            $arFields['PERSONAL_BIRTHDAY'] = $arData['PERSONAL_BIRTHDAY'];
        }

        if(false && isset($arData['AVATAR'])) {
            $arImage = CFile::MakeFileArray($arData['AVATAR']);
            $arImage['MODULE_ID'] = 'main';
            $arFields['PERSONAL_PHOTO'] = $arImage;
        }

		$GLOBALS['DB']->StartTransaction();

        $obUser = new CUser();
        $iUserId = intval($obUser->Add($arFields));
		if($iUserId) {
			// привязали пользователя
			$obAdd = $this->linkUser($iUserId, $arData);
			if(!$obAdd->isSuccess()) {
				$GLOBALS['DB']->Rollback();
				throw new \Exception(join(';', $obAdd->getErrors()));
			}
			$GLOBALS['DB']->Commit();

			// событие OnAfterSocUserRegister
			$arEventHandlers = GetModuleEvents('main', 'OnAfterSocUserRegister', true);
			foreach($arEventHandlers as $arEvent) {
				ExecuteModuleEventEx($arEvent, array($iUserId, $arData));
			}

            CUser::SendUserInfo($iUserId, 's1', 'Приветствуем Вас как нового пользователя нашего сайта!', true, 'USER_INVITE');

            return $iUserId;
        }

        throw new Exception('User add error');
    }

	public function AuthorizeUser($iUserId) {
		if($GLOBALS['USER']->Authorize($iUserId)) {
			$this->AuthorizeUserEvent($iUserId);
		}
	}

	protected function AuthorizeUserEvent($iUserId) {
		// событие OnAfterSocAuthorize
		$arEventHandlers = GetModuleEvents('main', 'OnAfterSocAuthorize', true);
		foreach($arEventHandlers as $arEvent) {
			ExecuteModuleEventEx($arEvent, array($iUserId));
		}
	}

    public function bindAuth() {
        global $USER, $DB;

        $site = $this->uri['request']['site'];

        if(!isset($this->arSites[$site])) {
            return;
        }

        if(isset($this->uri['request']['error']) && $this->uri['request']['error'] == 'access_denied') {
            $GLOBALS['APPLICATION']->RestartBuffer();
            echo '<html><head></head><body><script type="text/javascript">window.close();</script></body></html>';
            die;
        }

        $obSocUser = AdapterFactory::createUserObject($site, $this->arSites[$site]);
        $obSocUser->setRedirectUri($this->GetRedirectUri().'?site='.$site);

        /**
         * Привязка
         */
        if(isset($this->uri['post']['USER_LOGIN']) && isset($this->uri['post']['USER_PASSWORD'])) {
            if(isset($this->uri['get']['linkto']) && isset($this->uri['get']['token'])) {
                /**
                 * Условия для привязки пользователя:
                 * 1. Пользователь правильно ввел пароль от аккаунта на сайте
                 * 2. Его почта совпадает с почтой полученной из соц сети по токену
                 */
                define('NIYAMA_IGNORE_LOGIN_EVENTS', true);
                $authResult = $USER->Login(
                	$this->uri['post']['USER_LOGIN'],
                    $this->uri['post']['USER_PASSWORD'], 
                    'Y', 
                    'Y'
                );

                if($authResult !== true) {
                	$this->arParams['~AUTH_RESULT'] = $authResult;
                } else {
                    $arFields =& $_SESSION[$this->uri['request']['token']];
                    if(!is_array($arFields)) {
                        throw new Exception('Авторизуйтесь в социальной сети');
                    }

                    if($arFields['EMAIL'] == $GLOBALS['USER']->GetEmail()) {
                        // вяжем
                        $obAdd = $this->linkUser($GLOBALS['USER']->GetID(), $arFields);
                        if(!$obAdd->isSuccess()) {
                            throw new \Exception(join(';', $obAdd->getErrors()));
                        }
						$this->AuthorizeUserEvent($GLOBALS['USER']->GetId());

                        LocalRedirect($this->backUrl);
                    }
                }
            }
        }

        /**
         * Ловим code для получения токена
         * и авторизовываем пользователя
         */
        if(isset($this->uri['get']['code'])) {

            $that = $this;

            // получили token
            $obSocUser->getToken($this->uri['get']['code'], function($token) use ($obSocUser, $DB, $USER, $that) {
                // получаем данные пользователя из соц сети
                $obSocUser->getUserInfo($token, function($arFields) use($DB, $USER, $token, $that) {
                    /**
                     * Тут мы имеем пользователя, который авторизовался в соц сети и имеет email $arFields['EMAIL']
                     * @param $arFields UF_UID, UF_IDENTITY, LOGIN, EMAIL, NAME, LAST_NAME, PERSONAL_GENDER [, PERSONAL_BIRTHDAY [, AVATAR ]]
                     */
                    $_SESSION[$token] = $arFields;
                    // если пользователь привязан
                    if($USER_ID = $that->isLinkedUser($arFields['UF_UID'], $arFields['UF_SERVICES'])) {
						$that->AuthorizeUser($USER_ID);
                    } else {
                        // проверяем есть ли пользователь с таким email
                        $rsUser = CUser::GetByLogin($arFields['EMAIL']);
                        $arUser = $rsUser->Fetch();
                        // добавляем нового пользователя если его еще нет
                        if(!$arUser) {
                            $USER_ID = $that->registerNewUser($arFields);
                            $that->AuthorizeUser($USER_ID);
                        } else {
                            /**
                             * редиректим на форму авторизации для запроса пароля от аккаунта
                             */
                            $redirect_url = '/auth/link/?linkto='.$arFields['EMAIL'].'&token='.$token.'&'.DeleteParam(array('code'));

                            $that->javaScriptCallback('redirect', array($redirect_url));
                        }
                    }

                    // редиректим
                    $that->javaScriptCallback('redirect', array($that->backUrl));
                });
            });
        }
    }

    public function javaScriptCallback($method, $params) {
        $this->arResult['method'] = $method;
        $this->arResult['params'] = $params;
        $this->IncludeComponentTemplate('callback_js');
    }

    /**
     * @param $userId
     * @param $arFields
     * @return \Bitrix\Main\Entity\AddResult
     */
    public function linkUser($iUserId, $arFields) {
        $arSocData = array(
            'UF_USER_ID' => $iUserId,
            'UF_UID' => isset($arFields['UF_UID']) ? $arFields['UF_UID'] : '',
            'UF_SERVICES' => isset($arFields['UF_SERVICES']) ? $arFields['UF_SERVICES'] : '',
            'UF_IDENTITY' => isset($arFields['UF_IDENTITY']) ? $arFields['UF_IDENTITY'] : ''
        );

        return UsersocTable::add($arSocData);
    }

    public function isLinkedUser($uid, $service) {
    	$mReturn = false;
        // получаем привязку к аккаунту из соц сети
		$dbItems = UsersocTable::GetList(
			array(
				'order' => array(
					'ID' => 'DESC'
				),
				'filter' => array(
					'=UF_UID' => $uid,
					'=UF_SERVICES' => $service,
					'!UF_USER_ID' => false
				),
			)
		);
		while($arItem = $dbItems->Fetch()) {
			if($arItem['UF_USER_ID']) {
				$mReturn = $arItem['UF_USER_ID'];
				break;
			}
		}
		return $mReturn;
    }

    /**
     * @return mixed
     */
    public function executeComponent() {

        $this->backUrl =& $_SESSION['SOCAUTH_BACK_URL'];

        if (!empty($this->arParams['BACK_URL'])) {
            $this->backUrl = $this->arParams['BACK_URL'];
        }

        if(isset($this->uri['request']['backurl'])) {
            $this->backUrl = $this->uri['request']['backurl'];
        }

        try {
            $this->bindAuth();
        } catch(\Exception $e) {
        	$this->arResult['EXCEPTION_MESSAGE'] = $e->getMessage();
        	$this->arResult['EXCEPTION_OBJECT'] = $e;
            $this->IncludeComponentTemplate('request_error');
            return;
        }

        // если нужно привязать пользователя к аккаунту
        if(isset($this->arParams['LINK_ACCOUNTS']) && $this->arParams['LINK_ACCOUNTS'] == 'Y' && isset($this->uri['request']['linkto']) && isset($this->uri['request']['token'])) {
            $this->arResult['LAST_LOGIN'] = $this->uri['request']['linkto'];
            $this->IncludeComponentTemplate('link_account');
        } else {
            /**
             * Выводим кнопки для авторизации через соц сети
             */
            $this->arResult['SOCIAL_PARAMS'] = array();
            foreach($this->arSites as $site => $params) {
                $obSocUser = AdapterFactory::createUserObject($site, $params);
                $obSocUser->setRedirectUri($this->GetRedirectUri().'?site='.$site);
                $this->arResult['SOCIAL_PARAMS'][$site] = array(
                    'DIALOG_URL' => $obSocUser->getLoginDialogUrl(),
                    'WIDTH' => $obSocUser->loginDialogWidth,
                    'HEIGHT' => $obSocUser->loginDialogHeight,
                );
            }

			if($this->arParams['ADD_HEAD_SCRIPT_DEFAULT'] && $this->arParams['ADD_HEAD_SCRIPT_DEFAULT'] == 'Y') {
    	        $GLOBALS['APPLICATION']->AddHeadString('<script type="text/javascript">var SOCIAL_SITES_PARAMS = '.json_encode($this->arResult['SOCIAL_PARAMS']).'</script>');
			}

            $this->IncludeComponentTemplate();
        }
    }
}
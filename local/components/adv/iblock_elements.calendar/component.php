<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Компонент генерации календаря по датам элементов инфоблоков
 * @author Sergey Leshchenko, 2014
 * rev.: 04.07.2014 (DD.MM.YYYY)
 */

$arResult = array();
if(empty($arParams['IBLOCK_ID'])) {
	return;
}
if(!strlen(trim($arParams['ELEMENT_FILTER_NAME']))) {
	$arExtElementFilter = array();
} elseif(!preg_match('#^[A-Za-z_][A-Za-z01-9_]*$#', $arParams['ELEMENT_FILTER_NAME'])) {
	$arExtElementFilter = array();
} else {
	$arExtElementFilter = isset($GLOBALS[$arParams['ELEMENT_FILTER_NAME']]) ? $GLOBALS[$arParams['ELEMENT_FILTER_NAME']] : array();
	if(!is_array($arExtElementFilter)) {
		$arExtElementFilter = array();
	}
}

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}
$arParams['INCLUDE_TEMPLATE'] = isset($arParams['INCLUDE_TEMPLATE']) && $arParams['INCLUDE_TEMPLATE'] == 'Y' ? 'Y' : 'N';
$arParams['CACHE_TEMPLATE'] = isset($arParams['CACHE_TEMPLATE']) && $arParams['CACHE_TEMPLATE'] == 'Y' ? 'Y' : 'N';
$arParams['CACHE_GROUPS'] = isset($arParams['CACHE_GROUPS']) && $arParams['CACHE_GROUPS'] == 'N' ? 'N' : 'Y';
$arParams['CACHE_EMPTY_RESULT'] = isset($arParams['CACHE_EMPTY_RESULT']) && $arParams['CACHE_EMPTY_RESULT'] == 'Y' ? 'Y' : 'N';

$arParams['GROUP_DATE_FIELD'] = 'ACTIVE_FROM';
// готовые шаблоны: DAY = %%Y.%%m.%%d, MONTH = %%Y.%%m, YEAR = %%Y
$arParams['GROUP_TYPE'] = isset($arParams['GROUP_TYPE']) && strlen(trim($arParams['GROUP_TYPE'])) ? trim($arParams['GROUP_TYPE']) : 'DAY';
$arParams['SORT_ORDER'] = isset($arParams['SORT_ORDER']) && $arParams['SORT_ORDER'] == 'ASC' ? 'ASC' : 'DESC';

$arParams['DATE_FILTER_FROM'] = $arParams['DATE_FILTER_FROM'] ? trim($arParams['DATE_FILTER_FROM']) : '';
$arParams['DATE_FILTER_TO'] = $arParams['DATE_FILTER_TO'] ? trim($arParams['DATE_FILTER_TO']) : '';

$mCacheNavigation = false;
$arCacheParams = $arParams;

$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/iblock_elements.calendar';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$sCacheId = md5(serialize(array($arCacheParams, $arGroups, $arExtElementFilter, $mCacheNavigation)));
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	// старт тегированного кэша
	$bUseTagCache = defined('BX_COMP_MANAGED_CACHE') && $arParams['CACHE_TIME'] && isset($arParams['USE_TAG_CACHE']) && $arParams['USE_TAG_CACHE'] == 'Y';
	if($bUseTagCache) {
		$GLOBALS['CACHE_MANAGER']->StartTagCache($sCachePath);
	}

	$arFilter = array();
	$arFilter['=IBLOCK_ID'] = $arParams['IBLOCK_ID'];
	$arFilter['=ACTIVE'] = 'Y';
	if($arParams['DATE_FILTER_FROM']) {
		$arFilter['!'.$arParams['GROUP_DATE_FIELD']] = false;
		$arFilter['>='.$arParams['GROUP_DATE_FIELD']] = CProjectUtils::GetDateTimeFieldFilter($arParams['DATE_FILTER_FROM']);
	}
	if($arParams['DATE_FILTER_TO']) {
		$arFilter['!'.$arParams['GROUP_DATE_FIELD']] = false;
		$arFilter['<'.$arParams['GROUP_DATE_FIELD']] = CProjectUtils::GetDateTimeFieldFilter($arParams['DATE_FILTER_TO']);
	}
	$arFilter = array_merge($arFilter, $arExtElementFilter);

	switch($arParams['GROUP_TYPE']) {
		case 'YEAR':
			$sGroupDateFormat = '%%Y';
		break;

		case 'MONTH':
			$sGroupDateFormat = '%%Y.%%m';
		break;

		case 'DAY':
			$sGroupDateFormat = '%%Y.%%m.%%d';
		break;

		default:
			$sGroupDateFormat = $arParams['GROUP_TYPE'];
		break;
	}
	
	$arResult['ITEMS'] = array();

	if(class_exists('\\Bitrix\\Main\\Entity\\ExpressionField')) {
		$obExpr1 = new \Bitrix\Main\Entity\ExpressionField('GROUP_DATE_FIELD', 'DATE_FORMAT(%s, "'.$sGroupDateFormat.'")', $arParams['GROUP_DATE_FIELD'], array('data_type' => 'string'));
		$obExpr2 = new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(1)', null, array('data_type' => 'integer'));
		$arSelect = array(
			$obExpr1,
			$obExpr2
		);
	} else {
		$arSelect = array(
			'GROUP_DATE_FIELD' => array(
				'expression' => array('DATE_FORMAT(%s, "'.$sGroupDateFormat.'")', $arParams['GROUP_DATE_FIELD']), 
				'data_type' => 'string'
			),
			'CNT' => array(
				'expression' => array('COUNT(1)'), 
				'data_type' => 'integer'
			),
		);
	}

	$dbItems = \ADV\IBlock\IBlockElementTable::GetList(
		array(
			'filter' => $arFilter,
			'order' => array(
				'GROUP_DATE_FIELD' => $arParams['SORT_ORDER']
			),
			'select' => $arSelect,
		)
	);
	while($arItem = $dbItems->GetNext(false, true)) {
		$arResult['ITEMS'][] = $arItem;
	}

	$bCache = false;
	if(!empty($arResult['ITEMS']) || $arParams['EMPTY_RESULT_IS_VALID'] == 'Y') {
		$bCache = true;
	}

	if(!$bCache) {
		$this->AbortResultCache();
	}

	if($arParams['INCLUDE_TEMPLATE'] == 'Y' && $arParams['CACHE_TEMPLATE'] == 'Y') {
		$this->IncludeComponentTemplate();
	} elseif($bCache) {
		$this->EndResultCache();
	}

}

if($arParams['INCLUDE_TEMPLATE'] == 'Y' && $arParams['CACHE_TEMPLATE'] != 'Y') {
	$this->IncludeComponentTemplate();
}

return $arResult;

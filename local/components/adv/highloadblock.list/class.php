<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 15.05.2014
 * @time: 14:17
 *
 * @update: 22.05.2014
 *
 * ====================================
 * Компонент вывода списка Элементов ХЛ Блока.
 * @example:
 *
 *     $APPLICATION->IncludeComponent (
 *        "adv:highloadblock.list",
 *        "shops_list",
 *        Array(
 *
 *            "HLBLOCK_CLASS" => "SKIDKA\\Entity\\ProgramsTable", # Класс сущьности (в приоритете над HLBLOCK_CODE) или
 *            //"HLBLOCK_CODE"  => "COUPONS",                     # Символьный код ХЛБлока.
 *
 *           "USE_PAGINATION" => true,                            # Если не используем пагинацию то ROWS_PER_PAGE будет общим лимитом.
 *           "ROWS_PER_PAGE"  => 10,                              # Кол-во на странице ( Если 0 и USE_PAGINATION = false то все записи )
 *            "NAV_TEMPLATE"   => "",                             # Шаблон пагинатора (стандратный для GetPageNavString)
 *            "NAV_TITLE"      => "Магазины",                     # Тайтл к пагинации
 *
 *            "SELECT"         => array("ID", "UF_NAME","UF_SITE_URL","UF_GOTO","UF_LOGO"),   # выбор
 *            //"FILTER"       => array("=ID" => "9284"),                                     # фильтр
 *            //"ORDER"        => array("ID"=>"ASC");                                         # сортировка
 *
 *            "DETAIL_URL"      => "#UF_GOTO#",                           # Url к подробностям например: ( /detail/#CATEGORY.UF_CODE#/#ID#/ )
 *            "USE_UF_FUNCTION" => true,                                  # Выводить элементы как обработаные UF?
 *
 *            "CACHE_REFRESH"   => false,                                 # Обновить кэш  - (По умолчанию false)
 *            "CACHE_ID"        => array(),                               # Дополнительный ID кэша если требуется.
 *            "CACHE_TIME"      => 3600,                                  # Время кэша, по умолчанию 3600
 *            "CACHE_TAG"       => array()                                # Тэги (по умолчанию вешается имя темплейта)
 *
 *        ),
 *        false
 *    );
 *
 * @notes:
 *  Поумолчанию если отсутвует массив параметров CACHE_TAG то всегда вешается название темплейта (например: shops_list)
 *  $CACHE_MANAGER->ClearByTag("shops_list");
 *
 */



use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;


/**
 * Class CHighloadblockListClass 
 *
 */
Class CHighloadblockListClass  extends CBitrixComponent {

    /**
     * @var
     */
    private $uri;
    public  $sHLBlockCode = null;
    public  $iHLBlockId   = 0;
    public  $sDetailUrl = "detail.php?id=#ID#";
    public  $sEmptyUrlVariable = "empty";
    private $error   = array();
    private $defaultSize = 10;
    private $requiredModules =  array('highloadblock');
    private $requiredClasses =  array('CHLEntity','CExtCache','CStaticCache'); # Для корректной работы необходимы Классы Хелперы 'CHLEntity','CExtCache','CStaticCache' (Лещенко С)
    private $arSelect = array('*');
    private $sHLBlockClass = null;
    private $arFilter = array();
    private $bUseUFFunction = false;
    private $arOrder = array("ID"=>"DESC"); # default order
    private $bUsePagination = true;
    private $bRefreshCache = false;
    private $iRowsPerPage = 0; # default rows
    private $arNavParam = array();
    private $iDefaultCacheTime = 3600;

    /**
     * @param null $component
     */
    public function __construct($component = null)
    {
        parent::__construct($component);

        /** @var $security Получаем объект класса */
        $security = new Bitrix\Security\Filter\Request();

        /** @var $security Устанавливаем аудит на SQL enjection и XSS attack */
        $security->setAuditors(Array(
            'SQL' => new Bitrix\Security\Filter\Auditor\Sql(),
            'XSS' => new Bitrix\Security\Filter\Auditor\Xss()
        ));

        /** @var $arURI фильтруем входящие параметры. */
        $this->uri = $security->filter(array(
            'get'     => $_GET,
            'request' => $_REQUEST,
            'post'    => $_POST
        ), false);

    }

    /**
     * Инициализация параметров.
     */
    private function init()
    {
        /* Подключение и проверка на наличие нужных модулей */
        foreach ($this->requiredModules as $requiredModule)
        {
            if (!\Bitrix\Main\Loader::includeModule($requiredModule))
            {
                $this->setError(GetMessage("HLLIST_NO_REQUIRED_PARAM",array("#PARAM#"=>$requiredModule)));
            }
        }
        /* Проверка на наличие обязательных классов */
        foreach ($this->requiredClasses as $requiredClass)
        {
            if (!class_exists($requiredClass))
            {
                $this->setError(GetMessage("HLLIST_NO_REQUIRED_PARAM",array("#PARAM#"=>$requiredClass)));
            }
        }

        $this->setSHLBlockCode($this->arParams['HLBLOCK_CODE']);      # Устанавливаем код HL-Блока.
        $this->setSDetailUrl($this->arParams['DETAIL_URL']);          # Страница подробностей.
        $this->setArSelect($this->arParams['SELECT']);                # Устанавливаем поля.
        $this->setSHLBlockClass($this->arParams['HLBLOCK_CLASS']);    # Устанавлтваем название класса сущьности.
        $this->setArFilter($this->arParams['FILTER']);                # Устанавливаем фильтр.
        $this->setBUseUFFunction($this->arParams['USE_UF_FUNCTION']); # Устанвилваем использование UF обвертки.
        $this->setArOrder($this->arParams['ORDER']);                  # Устанавливаем Сортировку.
        $this->setBUsePagination($this->arParams['USE_PAGINATION']);  # Используем ли пагинацию.
        $this->setBRefreshCache($this->arParams['CACHE_REFRESH']);    # Обновить кэш?
        $this->setIRowsPerPage($this->arParams['ROWS_PER_PAGE']);     # Устанавливаем кол-во записей на странице (онже общий лимит при устолвии  USE_PAGINATION => false)
        $this->buildNavParam();                                       # Устанавливаем параметры навигации.
    }

    /**
     * buildNavParam
     */
    public function buildNavParam()
    {
        // page navigation
        $this->arNavParam = array(
            'nPageSize' => $this->iRowsPerPage,
            'iNumPage'  => is_set($this->uri['request']['PAGEN_1']) ? $this->uri['request']['PAGEN_1'] : 1,
            'bShowAll'  => true
        );
    }

    /**
     * gerResultsByClass
     * @param null $obBaseEntity
     * @return \Bitrix\Main\DB\ArrayResult|\Bitrix\Main\DB\Result|CDBResult
     */
    public function getQueryResult($obBaseEntity = null)
    {
        /* Получаем фильтр */
        $arFilter = $this->getArFilter();

        $enQuery =  (!is_null($obBaseEntity)) ? $obBaseEntity : $this->sHLBlockClass;
        $obQuery = new Bitrix\Main\Entity\Query($enQuery);
        $obQuery->setSelect($this->arSelect);

        if (!empty($arFilter)) {
            $obQuery->setFilter($arFilter);
        }

        $obQuery->setOrder($this->arOrder);
        $obQuery->countTotal(true);

        if ( !$this->bUsePagination ) {
            if ($this->iRowsPerPage) {
                $obQuery->setLimit($this->iRowsPerPage);
            }
        } else {
            $obQuery->setLimit($this->arNavParam['nPageSize']);
            $obQuery->setOffset(($this->arNavParam['iNumPage'] - 1) * $this->arNavParam['nPageSize']);
        }

        $result = $obQuery->exec();
        $result = new CDBResult($result);

        $arCParam = array();
        if (!empty($arFilter)) {
            $arCParam = array("filter"=>$arFilter);
        }

        /* Получаем класс */
        if (!is_null($obBaseEntity)) {
            $obClass  = $obBaseEntity->GetDataClass();
            $obQueryEnt = $obClass::getList($arCParam);
            $NavRecordCount = $obQueryEnt->GetSelectedRowsCount();
        } else {
            $obClass = $this->getSHLBlockClass();
            $obQueryEnt = $obClass->getList($arCParam);
            $NavRecordCount = $obQueryEnt->GetSelectedRowsCount();
        }

        if ( $this->bUsePagination ) {
            $arPagerTmp =$result->GetNavParams($this->arNavParam);
            $result->NavRecordCount = $NavRecordCount;
            $result->NavPageNomer   = $arPagerTmp['PAGEN'];
            $result->NavPageSize    = intval($this->arParams['ROWS_PER_PAGE']) > 0 ? intval($this->arParams['ROWS_PER_PAGE']) : $this->defaultSize ;
            $result->NavNum         = 1;
            $result->nPageWindow    = $this->arParams['ROWS_PER_PAGE'];
            $result->NavPageCount   = ceil($result->NavRecordCount / $result->NavPageSize);

            /* записываем в $arResult */
            $this->arResult["NAV_STRING"] = $result->GetPageNavString(
                (is_set($this->arParams['NAV_TITLE'])) ? $this->arParams['NAV_TITLE'] : '',
                (is_set($this->arParams['NAV_TEMPLATE'])) ? $this->arParams['NAV_TEMPLATE'] : 'arrows');
        }

        return $result;
    }


    /**
     * GetResult
     */
    public function GetResult()
    {
        if (!is_null($this->sHLBlockClass)) {

            $obResult = $this->getQueryResult();
            if ($this->bUseUFFunction) {
                $ob = new $this->sHLBlockClass();
                if (method_exists($ob,'getBaseEntityId')) {
                $this->iHLBlockId = $ob->getBaseEntityId();
                } else {
                    $this->setError(GetMessage("HLLIST_ENT_ID",array("#PARAM#" =>  $this->sHLBlockClass)));
                    $this->checkError();
                }

            }

        } else {

            $obBaseEntity = $this->getBaseEntity();
            if (!$obBaseEntity){
                $this->setError(GetMessage("HLLIST_NO_FOUND",array("#PARAM#" => $this->getSHLBlockCode())));
                $this->checkError();
            }
            if ($this->bUseUFFunction) {
                $this->iHLBlockId =  $this->getBaseEntityId();
            }

            $obResult = $this->getQueryResult($obBaseEntity);
        }

        if ($this->bUseUFFunction) {
            $fields = $this->getUFdata();
        }

        $rows = array();
        $i = 0;
        while ($arItem = $obResult->Fetch()) {
            if ($this->bUseUFFunction) {
                foreach ($arItem as $k => $v)
                {
                    $arUserField = $fields[$k];

                    if ($arUserField["SHOW_IN_LIST"]!="Y")
                    {
                        continue;
                    }

                    $html = call_user_func_array(
                        array($arUserField["USER_TYPE"]["CLASS_NAME"], "getadminlistviewhtml"),
                        array(
                            $arUserField,
                            array(
                                "NAME" => "FIELDS[".$arItem['ID']."][".$arUserField["FIELD_NAME"]."]",
                                "VALUE" => htmlspecialcharsbx($v)
                            )
                        )
                    );
                    if($html == '')
                    {
                        $html = '&nbsp;';
                    }
                    $arItem[$k] = $html;

                }
                $rows[$i] = $arItem;
                $rows[$i]['DETAIL_URL'] = $this->getSDetailUrl($arItem);
            } else {
                $rows[$i] = $arItem;
                $rows[$i]['DETAIL_URL'] = $this->getSDetailUrl($arItem);
            }
         $i++;
        }

        $this->arResult['rows'] = $rows;
        return (!empty($rows)) ? true : false;
    }

    /**
     * getUFdata
     * @return bool
     */
    public function getUFdata()
    {
        if ($this->bUseUFFunction) {
            if ($this->iHLBlockId) {
                return $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$this->iHLBlockId, 0, LANGUAGE_ID);
            }
        }
        return false;
    }


    /**
     * @return mixed
     */
    public function getBaseEntity()
    {
        return CHLEntity::GetEntityByName($this->getSHLBlockCode());
    }

    /**
     * @return mixed
     */
    public function getBaseEntityId()
    {
        return CHLEntity::GetEntityIdByName($this->getSHLBlockCode());
    }

    /**
     *
     */
    public function onIncludeComponentLang()
    {
        parent::onIncludeComponentLang();
    }

    /**
     * @param bool $additionalCacheID
     * @return string
     */
    public function getCacheID($additionalCacheID = false)
    {
        return parent::getCacheID($additionalCacheID);
    }

    /**
     * @return null
     */
    public function getSHLBlockCode()
    {
        return $this->sHLBlockCode;
    }

    /**
     * @return array
     */
    public function getArSelect()
    {
        return $this->arSelect;
    }

    /**
     * @param array $arSelect
     */
    public function setArSelect($arSelect)
    {
        if ( !empty($arSelect) ) {
            foreach ($arSelect as $key => $val) {
                if ($val != "*") {
                    $this->arResult['tableColumns'][] = (is_numeric($key)) ? $val : $key;
                }
            }
            $this->arSelect = $arSelect;
        }
    }

    /**
     * @return null
     */
    public function getSHLBlockClass()
    {
        if (class_exists($this->sHLBlockClass)) {
            return new $this->sHLBlockClass();
        } else {
            $this->setError(GetMessage("HLLIST_NO_HLBLOCK"));
        }
        return false;
    }

    /**
     * @param null $sHLBlockClass
     */
    public function setSHLBlockClass($sHLBlockClass)
    {
        if (!is_null($sHLBlockClass)) {
            $this->sHLBlockClass = $sHLBlockClass;
        }
    }

    /**
     * @param null $sHLBlockCode
     */
    public function setSHLBlockCode($sHLBlockCode)
    {
        if (!is_null($sHLBlockCode)) {
            $this->sHLBlockCode = $sHLBlockCode;
        }
    }

    /**
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * checkError
     */
    private function checkError()
    {
        if ($arError = $this->getError()) {
            ShowError(implode("<br>", $arError));
            exit;
        }
    }

    /**
     * @param array $error
     */
    public function setError($error)
    {
        $this->error[] = $error;
    }

    /**
     * getSDetailUrl
     * @param null $arItem
     * @return string
     */
    public function getSDetailUrl($arItem = null)
    {
        $DetailUrl = $this->sDetailUrl;
        if (!is_null($arItem)) {
            if (strstr($this->sDetailUrl,"#")) {
                foreach ($arItem as $field => $val) {
                    $val = (empty($val)) ? $this->sEmptyUrlVariable : $val;
                    $DetailUrl = str_replace("#". $field."#", $val, $DetailUrl);
                }
            }
        }
        return $DetailUrl;
    }

    /**
     * @param string $sDetailUrl
     */
    public function setSDetailUrl($sDetailUrl)
    {
        if (!is_null($sDetailUrl)) {
            $this->sDetailUrl = $sDetailUrl;
        }
    }

    /**
     * @param boolean $bUseUFFunction
     */
    public function setBUseUFFunction($bUseUFFunction)
    {
        $this->bUseUFFunction = $bUseUFFunction;
    }

    /**
     * @return array
     */
    public function getArFilter()
    {
        return $this->arFilter;
    }

    /**
     * @param array $arFilter
     */
    public function setArFilter($arFilter)
    {
        $this->arFilter = $arFilter;
    }

    /**
     * действия.
     */
    public function doActions()
    {
        # Выполняем действия
    }

    /**
     * @return array
     */
    public function getArOrder()
    {
        return $this->arOrder;
    }

    /**
     * @param array $arOrder
     */
    public function setArOrder($arOrder)
    {
        if (!is_null($arOrder)) {
            $this->arOrder = $arOrder;
        }
    }

    /**
     * @param boolean $bUsePagination
     */
    public function setBUsePagination($bUsePagination)
    {
       if (!is_null($bUsePagination)) {
        $this->bUsePagination = $bUsePagination;
       }
    }

    /**
     * @param boolean $bUseCache
     */
    public function setBRefreshCache($bUseCache)
    {
        if (!is_null($bUseCache)) {
            $this->bRefreshCache = $bUseCache;
        }
    }

    /**
     * @param int $iRowsPerPage
     */
    public function setIRowsPerPage($iRowsPerPage)
    {
        if (!is_null($iRowsPerPage)) {
            $this->iRowsPerPage = $iRowsPerPage;
        }
    }

    /**
     * buildCacheId
     * @return array
     */
    public function buildCacheId()
    {
        return array( $this->arNavParam, $this->arParams, $this->getTemplateName() );
    }

    /**
     * getCacheTime
     * @return int
     */
    public function getCacheTime()
    {
        return (is_set($this->arParams['CACHE_TIME'])) ? $this->arParams['CACHE_TIME'] : $this->iDefaultCacheTime;
    }

    /**
     * clearCahe
     * Отчистка кэша
     */
    public function clearCache()
    {
        $this->clearComponentCache($this->getName(),SITE_ID);
    }

    /**
     * @return mixed
     */
    public function executeComponent()
    {
        $this->init();        # Инициализируем параметры
        $this->checkError();  # Проверка на ошибки
        $this->doActions();   # Выполняем действия

        $sCacheAddParam = $this->buildCacheId();         # дополнительный параметр для ID кэша
        $sCacheId = $this->getTemplateName();            # идентификатор кэша (обязательный и уникальный параметр)

        /* Устанавливаем тег */
        $arDefaultTag = array($this->getTemplateName()); # Вешаем тэг с именем темплейта по умолчанию
        if (is_set($this->arParams['CACHE_TAG'])){
            $arAddCacheTags = array_merge((array) $arDefaultTag,(array) $this->arParams['CACHE_TAG']);
        } else {
            $arAddCacheTags = $arDefaultTag;
        }

        $sCachePath = '/'.__CLASS__.'/' .$this->getTemplateName(). '/'; # путь для сохранения кэша
        $bUseStaticCache = true; # сохранять ли значения дополнительно в виртуальном кэше
        $iStaticCacheEntityMaxSize = 100; # максимальное количество записей виртуального кэша
        $arCacheIdParams = array(__FUNCTION__, $sCacheId, $arAddCacheTags, $sCacheAddParam); # соберем в массив идентификационные параметры кэша
        $obExtCache = new CExtCache($arCacheIdParams, $sCachePath, $arAddCacheTags, $bUseStaticCache, $iStaticCacheEntityMaxSize); # Получаем объект расширенного кэша.
        $obExtCache->SetDefaultCacheTime($this->getCacheTime()); #  Устанавливаем время кэширования

        if(!$this->bRefreshCache && $obExtCache->InitCache()) {
            $this->arResult = $obExtCache->GetVars();
        } else {
            $obExtCache->StartDataCache(); # открываем кэшируемый участок

                if (!$this->GetResult()) {
                  $obExtCache->AbortDataCache();
                }

            $obExtCache->EndDataCache($this->arResult); # закрываем кэшируемый участок
        }
        $this->IncludeComponentTemplate(); # Вывод в шаблон
    }


}
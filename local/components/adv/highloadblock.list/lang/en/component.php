<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 15.05.2014
 * @time: 18:05
 */

$MESS["HLLIST_NO_HLBLOCK"] = "Incorrectly Set code HL-Block";
$MESS["HLLIST_NO_REQUIRED_PARAM"] = "For correct operation of the component connection is required - #PARAM#";
$MESS["HLLIST_NO_FOUND"] = "HL-Block #PARAM# was not found";
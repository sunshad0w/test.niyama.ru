<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @author: Stanislav Semenov (CJP2600)
 * @email: cjp2600@ya.ru
 *
 * @date: 15.05.2014
 * @time: 18:05
 */

$MESS["HLLIST_NO_HLBLOCK"] = "Некорректно указан код HL-Блока";
$MESS["HLLIST_NO_REQUIRED_PARAM"] = "Для корректной работы компонента необходим подключение - #PARAM#";
$MESS["HLLIST_NO_FOUND"] = "HL-Блок #PARAM# не найден";
$MESS["HLLIST_ENT_ID"] = "Не найден метод getBaseEntityId() в классе описания сущьности #PARAM#";
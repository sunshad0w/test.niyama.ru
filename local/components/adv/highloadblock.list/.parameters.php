<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
        "HL_BLOCK_CODE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage('HLLIST_COMPONENT_HL_BLOCK_CODE_PARAM'),
            "TYPE" => "TEXT"
        ),
		"DETAIL_URL" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage('HLLIST_COMPONENT_DETAIL_URL_PARAM'),
			"TYPE" => "TEXT"
		),
	),
);
<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$arComponentDescription = array(
        'NAME' => GetMessage('COMPONENT_NAME'),
        'DESCRIPTION' => GetMessage('COMPONENT_DESCRIPTION'),
	'ICON' => '/images/icon.gif',
        'CACHE_PATH' => 'Y',
        'SORT' => 500,
        'PATH' => array(
                'ID' => 'dev',
                'NAME' => GetMessage('COMPONENTS'),
                'CHILD' => array(
                        'SORT' => 10,
                        'ID' => 'component',
                        'NAME' => GetMessage('COMPONENTS_GROUP_NAME'),
		),
	),
);


<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * User: j30att
 * Date: 11.04.2017
 * Time: 13:06
 * component  make  CPA partners
 */
//массив всего и вся
$arResult = array();
$pageType = false;

$page = $APPLICATION->GetCurPage();

if ($page == "/") {
    $pageType = 'list';
}

$split = explode('/', $APPLICATION->GetCurPage());

if (count($split) === 2 && !empty($split[1])) {
    $pageType = 'list';
}

if (count($split) === 3 && !empty($split[2])) {
    $pageType = 'card';
}


if (stristr($page, 'cart')) {
    $pageType = 'basket';
}
if (stristr($page, 'order')) {
    $pageType = 'order';
}


if ($arParams['PAGE_TYPE'] === 'thanks') {
    $pageType = 'thanks';
}

$arResult['PAGE_TYPE'] = $pageType;


if (isset($_GET['utm_source'])) {
    $utm_source = $_GET['utm_source'];
} else {
    $utm_source = false;
}
if (isset($_GET['utm_medium'])) {
    $utm_medium = $_GET['utm_medium'];
} else {
    $utm_medium = false;
}
if (isset($_GET['utm_campaign'])) {
    $utm_campaign = $_GET['utm_campaign'];
} else {
    $utm_campaign = false;
}
if (isset($_GET['utm_content'])) {
    $utm_content = $_GET['utm_content'];
} else {
    $utm_content = false;
}
if (isset($_GET['utm_term'])) {
    $utm_term = $_GET['utm_term'];
} else {
    $utm_term = false;
}


if ($utm_source && $utm_medium && $utm_campaign) {


    $cookies = array(
        'utm_source' => $utm_source,
        'utm_medium' => $utm_medium,
        'utm_campaign' => $utm_campaign,
    );

    switch ($utm_source) {
        case 'actionpay' :
            $partner = null;
            $traffic = null;
            if (isset($_GET['actionpay'])) {
                $partner = 'actionpay';
                $traffic = $_GET['actionpay'];
            } else if (isset($_GET['apclick']) && isset($_GET['apsource'])) {
                $partner = 'actionpay';
                $traffic = $_GET['apclick'] . '.' . $_GET['apsource'];
            }



            if ($partner && $traffic && $utm_source && $utm_medium && $utm_campaign && $utm_term) {
                $traffic = htmlspecialchars($traffic);
                $cookies['cpa_partner'] = $partner;
                $cookies['cpa_traffic'] = $traffic;
                $cookies['utm_term'] = $utm_term;

                setCookies($cookies);
            }

            break;

        case 'gdeslon':
            if ($utm_term) {
                if ($_COOKIE['utm_term'] == $utm_term) {
                    setcookie('gdeslon_repeat', true, time() + 60 * 60 * 24 * 16, '/');
                } else {
                    setcookie('gdeslon_repeat', true, time() - 60 * 60 * 24 * 16, '/');
                }
                $cookies['utm_term'] = $utm_term;
            }
            setCookies($cookies);

            break;

        case 'otclick':
            if ($utm_content) {
                $cookies['utm_content'] = $utm_content;
            }
            if ($utm_term) {
                $cookies['utm_term'] = $utm_term;
            }
            setCookies($cookies);
            break;

        default:
            return false;
    }
};

switch ($pageType) {
    case 'list':
        $arResult['SECTION_ID'] = $arParams['ALL_DATA']['ID'];
        $arResult['SECTION_NAME'] = $arParams['ALL_DATA']['NAME'];

        foreach ($arParams['ALL_DATA']['ITEMS'] as $items) {
            foreach ($items["OFFERS"] as $offer) {
                $arResult['GDESLON_ITEMS'] .= $offer['XML_ID'];
                $arResult['GDESLON_ITEMS'] .= ':';
                $arResult['GDESLON_ITEMS'] .= $offer['PRICES']['baseprice']['VALUE'];
                $arResult['GDESLON_ITEMS'] .= ',';
            }
        }
        $arResult['GDESLON_ITEMS'] = substr($arResult['GDESLON_ITEMS'], 0, -1);
        $arResult['MERCHANT_ID'] = 79914;

        break;

    case 'card':
        $arResult['SECTION_ID'] = $arParams['ALL_DATA']['IBLOCK_SECTION_ID'];
        $arResult['SECTION_NAME'] = $arParams['ALL_DATA']['SECTION']['NAME'];

        foreach ($arParams['ALL_DATA']['OFFERS'] as $offer) {
            $price = CPrice::GetBasePrice($offer['ID']);
            $arResult['GDESLON_ITEMS'] .= $offer['XML_ID'];
            $arResult['GDESLON_ITEMS'] .= ':';
            $arResult['GDESLON_ITEMS'] .= round($price['PRICE']);
            $arResult['GDESLON_ITEMS'] .= ',';
            $arResult['APRT'][$offer['ID']] = [
                'XML_ID' => $offer['XML_ID'],
                'NAME' => $arParams['ALL_DATA']['NAME'],
                'PRICE' => round($price['PRICE'])
            ];
        }
        $arResult['GDESLON_ITEMS'] = substr($arResult['GDESLON_ITEMS'], 0, -1);
        $arResult['MERCHANT_ID'] = 79914;
        break;

    case 'basket': //pagetype Action pay 4
        foreach ($arParams['ALL_DATA']['ITEMS']['AnDelCanBuy'] as $item) {

            $db_res = CCatalogProduct::GetList(
                array(),
                array("ID" => $item['PRODUCT_ID']),
                false,
                false,
                array('ELEMENT_XML_ID')
            );

            $res = $db_res->Fetch();
            $arResult['APRT'][$item['ID']] = [
                'XML_ID' => $res['ELEMENT_XML_ID'],
                'NAME' => $item['NAME'],
                'PRICE' => round($item['PRICE']),
                'QUANTITY' => $item['QUANTITY']
            ];
            $arResult['GDESLON_ITEMS'] .= $res['ELEMENT_XML_ID'];
            $arResult['GDESLON_ITEMS'] .= ':';
            $arResult['GDESLON_ITEMS'] .= round($item['PRICE']);
            $arResult['GDESLON_ITEMS'] .= ',';
        }
        $arResult['GDESLON_ITEMS'] = substr($arResult['GDESLON_ITEMS'], 0, -1);
        $arResult['MERCHANT_ID'] = 79914;


        break;

    case 'order':
        foreach ($arParams['ALL_DATA'] as $items) {
            $db_res = CCatalogProduct::GetList(
                array(),
                array("ID" => $items['data']['PRODUCT_ID']),
                false,
                false,
                array('ELEMENT_XML_ID')
            );
            $res = $db_res->Fetch();
            $arResult['APRT'][$items['data']['ID']] = [
                'XML_ID' => $res['ELEMENT_XML_ID'],
                'NAME' => $items['data']['NAME'],
                'PRICE' => round($items['data']['PRICE']),
                'QUANTITY' => $items['data']['QUANTITY']
            ];
        }
        break;

    case 'thanks':

        $arResult['ORDER_ID'] = $arParams['ALL_DATA']['ID'];
        $arResult['SUMMARY'] = round($arParams['ALL_DATA']['PRICE']);

        $basket_items = CSaleBasket::GetList(array(), array('ORDER_ID' => $arParams['ALL_DATA']['ID']), false, false, ('PRODUCT_ID'));
        while ($arItems = $basket_items->Fetch()) {

            $db_res = CCatalogProduct::GetList(
                array(),
                array("ID" => $arItems['PRODUCT_ID']),
                false,
                false,
                array()
            );
            $res = $db_res->Fetch();

            $arResult['APRT'][$arItems['ID']] = [
                'XML_ID' => $res['ELEMENT_XML_ID'],
                'NAME' => $arItems['NAME'],
                'PRICE' => round($arItems['PRICE']),
                'QUANTITY' => $arItems['QUANTITY']
            ];
            $arResult['GDESLON_ITEMS'] .= $res['ELEMENT_XML_ID'];
            $arResult['GDESLON_ITEMS'] .= ':';
            $arResult['GDESLON_ITEMS'] .= round($arItems['PRICE']);
            $arResult['GDESLON_ITEMS'] .= ',';
        }


        $res = CSaleOrder::GetList(array(), array('ID' => $arParams['ALL_DATA']['ID']), false, false, array('UF_FO_ORDER_CODE'));
        if ($fetch = $res->Fetch()) {
            $arResult['ORDER_ID'] = $fetch['UF_FO_ORDER_CODE'];
        }


        $arResult['GDESLON_ITEMS'] = substr($arResult['GDESLON_ITEMS'], 0, -1);

        if ($_COOKIE['utm_source'] == 'gdeslon') {
            if ($_COOKIE['gdeslon_repeat'] == 1 && !empty($_COOKIE['gdeslonTimeOrder'])) {
                $arResult['GDESLON_CODES'] = '003';
            } else {
                if (!empty($_COOKIE['gdeslonTime'])) {
                    $arResult['GDESLON_CODES'] = '002';
                } else {
                    $arResult['GDESLON_CODES'] = '001';
                }
            }
            $arResult['MERCHANT_ID'] = 79914;
        }
        if ($_COOKIE['utm_source'] == 'actionpay') {
            $arResult['MERCHANT_ID'] = 12073;

            if ($_COOKIE['utm_term'] == $_COOKIE['ActionPayTime'] && (!empty($_COOKIE['startTimeOrder']))) {
                $arResult['MERCHANT_ID'] = 12075;
            } else if (!empty($_COOKIE['startTimeOrder'])) {
                $arResult['MERCHANT_ID'] = 12074;
            }

            $arData = array(
                'ORDER_ID' => $arResult['ORDER_ID'],
                'PRICE' => $arResult['SUMMARY'],
                'STATUS' => '1',
                'AIM' => $arResult['MERCHANT_ID'],
                'CLICK' => $_COOKIE['cpa_partner'],
                'SOURCE' => $_COOKIE['cpa_traffic'],
            );

            $result = CPizzaPiActionPayTable::add($arData);
        }

        $arResult['MASTER_ID'] = $_COOKIE['utm_term'];
        $arResult['PARTNERS'] = $_COOKIE['utm_source'];
        $arResult['TERM'] = $_COOKIE['utm_term'];


        break;

    default:
        break;
};

function setCookies($cookies)
{
    if (!empty($cookies)) {
        foreach ($cookies as $cookieName => $cookieValue) {
            setcookie($cookieName, $cookieValue, time() + 180 * 86400, '/');
            $_COOKIE[$cookieName] = $cookieValue;
        }
    }
}

$this->IncludeComponentTemplate();


<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult['PAGE_TYPE'] == 'list'): ?>
    <script async='true' type='text/javascript'
            src='https://www.gdeslon.ru/landing.js?mode=list&codes=<?=$arResult['GDESLON_ITEMS']?>&cat_id=<?=$arResult['SECTION_ID']?>&mid=<?=$arResult['MERCHANT_ID']?>'></script>
    <script>
        window.APRT_DATA = {
            pageType: 3,
            currentCategory: {
                id: <?=$arResult['SECTION_ID']?>,
                name:"<?=$arResult['SECTION_NAME']?>"
            },
            childCategories: []
        };
    </script>

<? elseif ($arResult['PAGE_TYPE'] == 'card'): ?>
    <script async='true' type='text/javascript'
            src='https://www.gdeslon.ru/landing.js?mode=card&codes=<?=$arResult['GDESLON_ITEMS']?>&mid=<?=$arResult['MERCHANT_ID']?>'></script>
    <script>
        window.APRT_DATA = {
            pageType: 2,
            currentCategory: {
                id: <?=$arResult['SECTION_ID']?>,
                name: "<?=$arResult['SECTION_NAME']?>"
            },
            currentProduct:[
            <? foreach($arResult['APRT'] as $offer):?>
                {
                    id:<?=$offer['XML_ID']?>,
                    name: "<?=$offer['NAME']?>",
                    price: <?=$offer['PRICE']?>,
                },
            <?endforeach;?>
            ]
        };
    </script>
<? elseif ($arResult['PAGE_TYPE'] == 'basket'): ?>
    <script async='true' type='text/javascript'
            src='https://www.gdeslon.ru/landing.js?mode=basket&codes=&&mid=<?=$arResult['GDESLON_ITEMS']?>&mid=<?=$arResult['MERCHANT_ID']?>'></script>
    <script>
        window.APRT_DATA = {
            pageType: 4, basketProducts: [
                <?foreach ($arResult['APRT'] as $offer):?>
                {
                    id:<?=$offer['XML_ID']?>,
                    name: "<?=$offer['NAME']?>",
                    price: <?=$offer['PRICE']?>,
                    quantity: <?=$offer['QUANTITY']?>
                },
                <?endforeach;?>
            ]
        };

        </script>
<? elseif ($arResult['PAGE_TYPE'] == 'order'): ?>
    <script>
        window.APRT_DATA = {
            pageType: 5, basketProducts: [
                <?foreach ($arResult['APRT'] as $offer):?>
                {
                    id:<?=$offer['XML_ID']?>,
                    name: "<?=$offer['NAME']?>",
                    price: <?=$offer['PRICE']?>,
                    quantity: <?=$offer['QUANTITY']?>
                },
                <?endforeach;?>
            ]
        };
    </script>
<? elseif ($arResult['PAGE_TYPE'] == 'thanks'): ?>
    <script async='true' type='text/javascript'
            src='https://www.gdeslon.ru/landing.js?mode=thanks&codes=&mid=<?=$arResult['GDESLON_ITEMS']?>&mid=<?=$arResult['MERCHANT_ID']?>'></script>
    <script>
        window.APRT_DATA = {
            pageType: 6, basketProducts: [
                <?foreach ($arResult['APRT'] as $offer):?>
                {
                    id:<?=$offer['XML_ID']?>,
                    name: "<?=$offer['NAME']?>",
                    price: <?=$offer['PRICE']?>,
                    quantity: <?=$offer['QUANTITY']?>
                },
                <?endforeach;?>
            ]
        };
    </script>
    <? if ($arResult['PARTNERS'] == 'gdeslon'): ?>
        <? if ($_COOKIE['checkShowed'] != $arResult['ORDER_ID']): ?>
            <script type="text/javascript"
                    src="https://www.gdeslon.ru/thanks.js?codes=<?=$arResult['GDESLON_CODES']?>,<?= $arResult['GDESLON_ITEMS'] ?>&order_id=<?= $arResult['ORDER_ID'] ?>&mid=<?= $arResult['MERCHANT_ID'] ?>"></script>
            <?
            setcookie("checkShowed", $arResult['ORDER_ID'], time() + 180 * 86400, '/');
            setcookie('gdeslonTimeOrder', true, time() + 60 * 60 * 24 * 16);
            setcookie('gdeslonTime', true, time() + 60 * 60 * 24 * 90);
            ?>

        <? endif; ?>

    <? elseif ($arResult['PARTNERS'] == 'actionpay'): ?>
        <? if ($_COOKIE['checkShowed'] != $arResult['ORDER_ID']): ?>
            <img class='partner_pixel'
                 src='//apypxl.com/ok/<?=$arResult['MERCHANT_ID'] ?>.png?actionpay=<?= $arResult['TERM'] ?>&apid=<?= $arResult['ORDER_ID'] ?>&price=<?= $arResult['SUMMARY'] ?>'>
            <?
            setcookie("checkShowed", $arResult['ORDER_ID'], time() + 180 * 86400, '/');
            setcookie('startTimeOrder', true, time() + 60 * 60 * 24 * 16,'/');
            setcookie('ActionPayTime', $arResult['MASTER_ID'], time() + 60 * 60 * 24 * 90, '/');
            ?>
        <? endif;?>

    <? elseif ($arResult['PARTNERS'] == 'otclick'): ?>
        <?if ($_COOKIE['checkShowed'] != $arResult['ORDER_ID']):?>
            <img class='partner_pixel' src='http://track.otclickcpa.ru/SPM0?adv_sub=<?$arResult['ORDER_ID']?>&transaction_id=<?$arResult['TERM']?>'>
            <? setcookie("checkShowed", $arResult['ORDER_ID'], time() + 180 * 86400, '/');?>
        <? endif;?>
    <? endif;?>

<? endif; ?>
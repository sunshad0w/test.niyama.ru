<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!CModule::IncludeModule("sale")) {
    ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
    return;
}
if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("SALE_ACCESS_DENIED"));
}

$arJsonResult = array();

$arParams["PATH_TO_DETAIL"] = Trim($arParams["PATH_TO_DETAIL"]);
if (strlen($arParams["PATH_TO_DETAIL"]) <= 0)
    $arParams["PATH_TO_DETAIL"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?ID=#ID#");

$arParams["PER_PAGE"] = (intval($arParams["PER_PAGE"]) <= 0 ? 20 : intval($arParams["PER_PAGE"]));

$arParams["SET_TITLE"] = ($arParams["SET_TITLE"] == "N" ? "N" : "Y");
if ($arParams["SET_TITLE"] == 'Y')
    $APPLICATION->SetTitle(GetMessage("SPPL_DEFAULT_TITLE"));

//Delete profile
$errorMessage = "";
$del_id = IntVal($_REQUEST["del_id"]);

if ($del_id > 0 && check_bitrix_sessid()) {
    $dbUserProps = CSaleOrderUserProps::GetList(
        array(),
        array(
            "ID" => $del_id,
            "USER_ID" => IntVal($USER->GetID())
        )
    );
    if ($arUserProps = $dbUserProps->Fetch()) {
        if (!CSaleOrderUserProps::Delete($arUserProps["ID"])) {
            $errorMessage = GetMessage("SALE_DEL_PROFILE");
        }
    } else {
        $errorMessage = GetMessage("SALE_NO_PROFILE");
    }
}

$DELIVERY_REGION = isset($_REQUEST["DELIVERY_REGION"]) ? $_REQUEST["DELIVERY_REGION"] :'';
$DELIVERY_ADDRESS = isset($_REQUEST["DELIVERY_ADDRESS"]) ? $_REQUEST["DELIVERY_ADDRESS"] : false;
$DELIVERY_HOUSE = isset($_REQUEST["DELIVERY_HOUSE"]) ? $_REQUEST["DELIVERY_HOUSE"] : '';
$DELIVERY_CORPUS = isset($_REQUEST["DELIVERY_CORPUS"]) ? $_REQUEST["DELIVERY_CORPUS"] : '';
$DELIVERY_BUILD = isset($_REQUEST["DELIVERY_BUILD"]) ? $_REQUEST["DELIVERY_BUILD"] : '';
$DELIVERY_PORCH = isset($_REQUEST["DELIVERY_PORCH"]) ? $_REQUEST["DELIVERY_PORCH"] : '';
$DELIVERY_FLOOR = isset($_REQUEST["DELIVERY_FLOOR"]) ? $_REQUEST["DELIVERY_FLOOR"] : '';
$DELIVERY_INTERCOM = isset($_REQUEST["DELIVERY_INTERCOM"]) ? $_REQUEST["DELIVERY_INTERCOM"] : '';
$DELIVERY_APART = isset($_REQUEST["DELIVERY_APART"]) ? $_REQUEST["DELIVERY_APART"] : '';

$DELIVERY_ID = isset($_REQUEST["DELIVERY_ID"]) ? (int)$_REQUEST["DELIVERY_ID"] : false;

$WHANTS_JSON = isset($_REQUEST["WHANTS_JSON"]) ? true : false;


if ($DELIVERY_ID > 0 && check_bitrix_sessid()) {

//получает весь список свойств
$addressList = array();
$propsData = array();

    $propsDataAll = CSaleOrderUserPropsValue::GetList(array(), Array("USER_PROPS_ID"=>$DELIVERY_ID));
    while ($props = $propsDataAll->Fetch()) {
        if (in_array($props['PROP_CODE'], array('DELIVERY_REGION','DELIVERY_ADDRESS', 'DELIVERY_HOUSE', 'DELIVERY_CORPUS', 'DELIVERY_BUILD', 'DELIVERY_PORCH', 'DELIVERY_FLOOR', 'DELIVERY_INTERCOM', 'DELIVERY_APART'))) {
            CSaleOrderUserPropsValue::Update(
                (int)$props['ID'],
                array(
                    'VALUE' => ${$props['PROP_CODE']},
                )
            );
        }
    }

}
else if ($DELIVERY_ADDRESS && $DELIVERY_ID === 0) {

//add new profile

    $orderPropertiesList = CSaleOrderProps::GetList(
        array(),
        array(// 'CODE' => 'DELIVERY_ADDRESS'
        ),
        false,
        false,
        array("ID", "NAME", 'CODE')
    );
    $USER_PROPS_ID = CSaleOrderUserProps::Add(array(
        'NAME' => implode(' ', array($DELIVERY_ADDRESS, $DELIVERY_HOUSE, $DELIVERY_APART)),
        'PERSON_TYPE_ID' => 1,
        'USER_ID' => IntVal($USER->GetID()),
    ));
    if ($USER_PROPS_ID ) {
        while ($orderProperty = $orderPropertiesList->GetNext()) {

            if (in_array($orderProperty['CODE'], array('DELIVERY_REGION', 'DELIVERY_ADDRESS', 'DELIVERY_HOUSE', 'DELIVERY_CORPUS', 'DELIVERY_BUILD', 'DELIVERY_PORCH', 'DELIVERY_FLOOR', 'DELIVERY_INTERCOM', 'DELIVERY_APART'))) {
                CSaleOrderUserPropsValue::Add(array(
                    "USER_PROPS_ID" => $USER_PROPS_ID,
                    "ORDER_PROPS_ID" => $orderProperty['ID'],
                    "NAME" => $orderProperty['NAME'],
                    "VALUE" => ${$orderProperty['CODE']},
                    'CODE' => $orderProperty['CODE'],
                ));
            }
        }

    }
}

if (IntVal($_REQUEST["del_id"]) > 0)
    $errorMessage = GetMessage("SALE_DEL_PROFILE", array("#ID#" => IntVal($_REQUEST["del_id"])));
elseif (IntVal($_REQUEST["success_del_id"]) > 0)
    $errorMessage = GetMessage("SALE_DEL_PROFILE_SUC", array("#ID#" => IntVal($_REQUEST["success_del_id"])));

if (strLen($errorMessage) >= 0)
    $arResult["ERROR_MESSAGE"] = $errorMessage;

$by = (strlen($_REQUEST["by"]) > 0 ? $_REQUEST["by"] : "DATE_UPDATE");
$order = (strlen($_REQUEST["order"]) > 0 ? $_REQUEST["order"] : "DESC");

$dbUserProps = CSaleOrderUserProps::GetList(
    array($by => $order),
    array("USER_ID" => IntVal($GLOBALS["USER"]->GetID())),
    false,
    false,
    array("ID", "NAME", "USER_ID", "PERSON_TYPE_ID", "DATE_UPDATE")
);
$dbUserProps->NavStart($arParams["PER_PAGE"]);
$arResult["NAV_STRING"] = $dbUserProps->GetPageNavString(GetMessage("SPPL_PAGES"));
$arResult["PROFILES"] = Array();
while ($arUserProps = $dbUserProps->GetNext()) {
    $arResultTmp = Array();
    $arResultTmp = $arUserProps;
    $arResultTmp["PERSON_TYPE"] = CSalePersonType::GetByID($arUserProps["PERSON_TYPE_ID"]);
    $arResultTmp["PERSON_TYPE"]["NAME"] = htmlspecialcharsEx($arResultTmp["PERSON_TYPE"]["NAME"]);
    $arResultTmp["URL_TO_DETAIL"] = CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_DETAIL"], Array("ID" => $arUserProps["ID"]));
    $arResultTmp["URL_TO_DETELE"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?del_id=" . $arUserProps["ID"] . "&" . bitrix_sessid_get());

    $arPropValsTmp = Array();
    $dbPropVals = CSaleOrderUserPropsValue::GetList(
        array("ID" => ""),
        array("USER_PROPS_ID" => $arUserProps["ID"],

        ),
        false,
        false,
        array("ID", "ORDER_PROPS_ID", "VALUE", "SORT", "CODE")
    );
    while ($arPropVals = $dbPropVals->GetNext()) {
        $arPropValsTmp[$arPropVals["CODE"]] = $arPropVals["VALUE"];
    }
    $arResultTmp["PROPS_VALUES"] = $arPropValsTmp;


    $arResult["PROFILES"][] = $arResultTmp;

}

if ($WHANTS_JSON) {


    foreach ($arResult["PROFILES"] as $val) {

            $arJsonResult[$val['ID']] = array(
                'DELIVERY_REGION' => $val['PROPS_VALUES']['DELIVERY_REGION'],
                'DELIVERY_ADDRESS' => $val['PROPS_VALUES']['DELIVERY_ADDRESS'],
                'DELIVERY_HOUSE' => $val['PROPS_VALUES']['DELIVERY_HOUSE'],
                'DELIVERY_CORPUS' => $val['PROPS_VALUES']['DELIVERY_CORPUS'],
                'DELIVERY_BUILD' => $val['PROPS_VALUES']['DELIVERY_BUILD'],
                'DELIVERY_PORCH' => $val['PROPS_VALUES']['DELIVERY_PORCH'],
                'DELIVERY_FLOOR' => $val['PROPS_VALUES']['DELIVERY_FLOOR'],
                'DELIVERY_INTERCOM' => $val['PROPS_VALUES']['DELIVERY_INTERCOM'],
                'DELIVERY_APART' => $val['PROPS_VALUES']['DELIVERY_APART']
            );
        }

    return json_encode($arJsonResult);
}

$this->IncludeComponentTemplate();
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?$this->addExternalJS(SITE_TEMPLATE_PATH."/js/address-autocomplete.js");?>

<div class="profile-addresses" id="profileAddressesContainer">
    <? if (strlen($arResult["ERROR_MESSAGE"]) > 0)
        ShowError($arResult["ERROR_MESSAGE"]); ?>


    <? foreach ($arResult["PROFILES"] as $val): ?>

        <div class="profile-address-control"><i class="fa fa-pencil" data-id="<?= $val['ID'] ?>"></i><span
                    class="profile-address__del"
                    data-id="<?= $val['ID'] ?>">×</span>
        </div>
        <div class="profile-address profile-address-<?= $val['ID'] ?>" data-region = "<?= $val['PROPS_VALUES']['DELIVERY_REGION'] ?>" data-address = "<?= $val['PROPS_VALUES']['DELIVERY_ADDRESS']?>" data-house = "<?= $val['PROPS_VALUES']['DELIVERY_HOUSE']?>" data-corpus = "<?= $val['PROPS_VALUES']['DELIVERY_CORPUS']?>" data-build = "<?= $val['PROPS_VALUES']['DELIVERY_BUILD']?>" data-porch = "<?= $val['PROPS_VALUES']['DELIVERY_PORCH']?>" data-floor = "<?= $val['PROPS_VALUES']['DELIVERY_FLOOR']?>" data-intercom = "<?= $val['PROPS_VALUES']['DELIVERY_INTERCOM']?>" data-apart = "<?= $val['PROPS_VALUES']['DELIVERY_APART']?>">
            <?= $val['PROPS_VALUES']['DELIVERY_ADDRESS'] ?>
            <?= $val['PROPS_VALUES']['DELIVERY_HOUSE'] ?>
            <?= $val['PROPS_VALUES']['DELIVERY_APART'] ?>
        </div>


    <? endforeach; ?>


</div>

<?
    //
    // Справочники по доставке
    //
    $arTmp = CNiyamaOrders::GetDeliveryRegions();
    $arResult['DELIVERY_REGIONS'] = array();
    foreach($arTmp as $arItem) {
        if($arItem['ACTIVE'] == 'Y') {
            $arResult['DELIVERY_REGIONS'][$arItem['ID']] = $arItem;
        }
    }
    $arResult['SUBWAY_STATIONS'] = CNiyamaIBlockSubway::GetAllStationsList();

?>

<div class="profile-addresses__add-btn">
    Добавить адрес
</div>

<div class="profile-addresses__add-form" style="display: none;">

    <div class="container">
        <div class="profile-popup">

            <h1>Добавить адрес</h1>

            <form id="profileAddress" data-parsley-validate>
                <div class="row">
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Город
                            </label>
                            <select  id='DELIVERY_REGION'
                                    class='profile-addresses__add-input'  data-delivery="city"  style="padding-bottom: 3px;padding-top: 3px;">
                                <?
                                $sRegion = '';
                                $bMsk = !empty($arResult['ORDER_INFO']['DELIVERY_REGION']) ? false : true;
                                foreach($arResult['DELIVERY_REGIONS'] as $arItem) {
                                    $sTmpSelected = '';
                                    if($arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION']) {
                                        $sTmpSelected = ' selected="selected"';
                                        $sRegion = $arItem['NAME'];
                                    }
                                    //$sTmpSelected = $arItem['ID'] == $arResult['ORDER_INFO']['DELIVERY_REGION'] ? ' selected="selected"' : '';
                                    $sTmpDataCity = $arItem['NAME'] == 'Москва' ? 'REGION_1' : 'REGION_X';
                                    if(2 == $arItem['ID']) {
                                        $sTmpDataCity='REGION_OTHER';
                                    }
                                    ?><option<?=$sTmpSelected?> data-city="<?=$sTmpDataCity?>" value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option><?
                                }
                                if($sRegion == 'Москва') {
                                    $bMsk = true;
                                }
                                ?>
                            </select>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Улица*
                            </label>
                            <input type='text' maxlength='250'
                                   id='DELIVERY_ADDRESS'
                                   class='profile-addresses__add-input'
                                   data-delivery="street"
                                   required
                            >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Дом*
                            </label>

                            <input type='text' maxlength='250'
                                    id='DELIVERY_HOUSE'
                                    class='profile-addresses__add-input'
                                    data-delivery="house"
                                    required
                                >

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Корпус
                            </label>

                            <input type='text' maxlength='250'
                                   id='DELIVERY_CORPUS'
                                   class='profile-addresses__add-input'
                                   data-delivery="corpus"
                            >

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Строение
                            </label>
                            <input type='text' maxlength='250'
                                   id='DELIVERY_BUILD'
                                   class='profile-addresses__add-input'
                                   data-delivery="build"
                            >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Подъезд
                            </label>
                            <input type='text' maxlength='250'
                                   id='DELIVERY_PORCH'
                                   class='profile-addresses__add-input'
                                   data-delivery="porch"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Этаж
                            </label>

                            <input type='text' maxlength='250'
                                   id='DELIVERY_FLOOR'
                                   class='profile-addresses__add-input'
                                   data-delivery="floor"
                            >

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Домофон
                            </label>
                            <input type='text' maxlength='250'
                                   id='DELIVERY_INTERCOM'
                                   class='profile-addresses__add-input'
                                   data-delivery="intercom"
                            >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='profile-popup__input'>
                            <label class='profile-popup__label'>
                                Квартира / Офис
                            </label>
                            <input type='text' maxlength='250'
                                   id='DELIVERY_APART'
                                   class='profile-addresses__add-input'
                                   data-delivery="apart"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <input type="hidden" id='DELIVERY_ID'>
                        <input type="submit" class='profile-addresses__add-save' value="Сохранить" />
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

<script type="text/javascript">
    BX.ready(function () {

        var AddAdressPopup = new BX.PopupWindow('AddAdress', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: 'black', opacity: '80'
            },
            events: {
                //перед показом я найду id этого адреса и соберу данные data-addr data-house
                //$('#DELIVERY_ADDRESS').val( $('#23423').data('house'));
                onPopupShow: function () {
                    $('.profile-addresses__add-save').click(function (e) {
                        if($(".popup-window #profileAddress").parsley().validate() == false){
                            return false;
                        }
                        var formData = new FormData();
                        formData.append('DELIVERY_REGION', $('#AddAdress #DELIVERY_REGION').val());
                        formData.append('DELIVERY_ADDRESS', $('#AddAdress #DELIVERY_ADDRESS').val());
                        formData.append('DELIVERY_HOUSE', $('#AddAdress #DELIVERY_HOUSE').val());
                        formData.append('DELIVERY_CORPUS', $('#AddAdress #DELIVERY_CORPUS').val());
                        formData.append('DELIVERY_BUILD', $('#AddAdress #DELIVERY_BUILD').val());
                        formData.append('DELIVERY_PORCH', $('#AddAdress #DELIVERY_PORCH').val());
                        formData.append('DELIVERY_FLOOR', $('#AddAdress #DELIVERY_FLOOR').val());
                        formData.append('DELIVERY_INTERCOM', $('#AddAdress #DELIVERY_INTERCOM').val());
                        formData.append('DELIVERY_APART', $('#AddAdress #DELIVERY_APART').val());
                        formData.append('DELIVERY_ID', $('#AddAdress #DELIVERY_ID').val());
                        formData.append('WHANTS_JSON', true);
                        sendUserAddressAjax(formData);
                        AddAdressPopup.close()
                    });
                    if(!$('#AddAdress').is( ":data('pizzapi-addressAutocomplete')" )){
                        $('#AddAdress').addressAutocomplete({});
                    } else {
                        $('#AddAdress').addressAutocomplete('destroy');
                        $('#AddAdress').addressAutocomplete({});
                    }

                }
            }

        });

        function showConfirmPopup(id){
            var flag = false;
            var confirmPopup = new BX.PopupWindow('Confirm', window.body, {
                autoHide: true,
                offsetTop: 1,
                offsetLeft: 0,
                lightShadow: true,
                closeIcon: true,
                closeByEsc: true,
                overlay: {
                    backgroundColor: 'black', opacity: '80'
                },
                events: {
                    onPopupShow:function () {
                        $('.confirm_button').click(function () {
                            var formData = new FormData();
                            formData.append('del_id', id);
                            formData.append('WHANTS_JSON', true);
                            sendUserAddressAjax(formData);
                            confirmPopup.close();
                            Informer.success('Указанный адресс успешно удален');

                        });
                        $('.cancel_button').click(function () {
                            confirmPopup.close();

                        });

                    }
                }
            });


            var confirmForm ="<div class='confirm__wrapper'><div class='confirm__text'>Вы уверены, что хотите удалить выбранный адресс?</div><div class='confirm__controll'><button class='confirm_button'>Да</button><button class='cancel_button' autofocus>Нет</button></div></div>";
            confirmPopup.setContent(confirmForm);
            confirmPopup.show();
        } ;



       $(document).on('click', '.profile-address__del', function (e) {
           var id = $(this).data('id');
           showConfirmPopup(id);

        });







        $(document).on('click', '.profile-address-control .fa.fa-pencil', function (e) {
            var id = $(this).data('id');
            popup_form = generateForm(id);
            AddAdressPopup.setContent(popup_form);
            AddAdressPopup.show();
            $(".popup-window #DELIVERY_REGION").val($(".profile-address-"+id).data("region")); // показать нужный город в select
        });

        /*
            template: добавить в адресс data-address data-house data-...
            backend: отдать этот результат
            pop-up: в зависимости от действия забрать даты и id или нет и передать все в аякс
            ajax: отдает все данные полученные и получает и рисует все адрессы

         */

        function sendUserAddressAjax(formData) {
            $.ajax({
                type: "POST",
                url: '<?= htmlspecialcharsbx('/local/ajax/getProfileAddresses.php?' . bitrix_sessid_get()) ?>',
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                headers: {
                    "Upgrade-Insecure-Requests": 1
                },
                success: function (data_json) {
                    console.log(data_json);
                    var result = '';
                    if (!$.isEmptyObject(data_json)) {

                        for (var addr_id in data_json) {
                            if (data_json.hasOwnProperty(addr_id)) {
                                console.log(data_json[addr_id]);
                                var user_address = data_json[addr_id];
                                result += '<div class="profile-address-control"><i class="fa fa-pencil" data-id="' + addr_id + '"></i><span class="profile-address__del" data-id="' + addr_id + '">×</span></div>';
                                result += '<div class="profile-address profile-address-' + addr_id + '" data-region="'+ user_address['DELIVERY_REGION'] +'" data-address = "'+ user_address['DELIVERY_ADDRESS']+'" data-house = "'+ user_address['DELIVERY_HOUSE']+'" data-corpus = "'+ user_address['DELIVERY_CORPUS']+'" data-build = "'+ user_address['DELIVERY_BUILD']+'" data-porch = "'+ user_address['DELIVERY_PORCH']+'" data-floor = "'+ user_address['DELIVERY_FLOOR']+'" data-intercom = "'+ user_address['DELIVERY_INTERCOM']+'" data-apart = "'+ user_address['DELIVERY_APART']+'">';
                                result += user_address['DELIVERY_ADDRESS'] + ' ';
                                result += user_address['DELIVERY_HOUSE'] + ' ';
                                result += user_address['DELIVERY_APART'];
                                result += '</div>';
                            }
                        }
                    }
                    $('#profileAddressesContainer').html(result);
                }
            });
        }

        function generateForm(id)
        {
            var $obj = $('.profile-addresses__add-form').clone();
            $obj.find("#profileAddress").attr("data-parsley-validate",true);
            if (id >0) {
                $obj.find('#DELIVERY_REGION').val($('.profile-address-'+id).data('region'));

                // Улица
                $obj.find('#DELIVERY_ADDRESS').attr('value', $('.profile-address-'+id).data('address'));
                $obj.find('#DELIVERY_ADDRESS').attr("data-parsley-required", true);

                // Дом
                $obj.find('#DELIVERY_HOUSE').attr('value', $('.profile-address-'+id).data('house'));
                $obj.find('#DELIVERY_HOUSE').attr("data-parsley-required", true);

                $obj.find('#DELIVERY_CORPUS').attr('value', $('.profile-address-'+id).data('corpus'));
                $obj.find('#DELIVERY_BUILD').attr('value', $('.profile-address-'+id).data('build'));
                $obj.find('#DELIVERY_PORCH').attr('value', $('.profile-address-'+id).data('porch'));
                $obj.find('#DELIVERY_FLOOR').attr('value', $('.profile-address-'+id).data('floor'));
                $obj.find('#DELIVERY_INTERCOM').attr('value', $('.profile-address-'+id).data('intercom'));
                $obj.find('#DELIVERY_APART').attr('value', $('.profile-address-'+id).data('apart'));
                $obj.find('#DELIVERY_ID').attr('value', id);

                $obj.find("h1").text("Изменить  адрес");

                var maked_form =  $obj.html().replace(/[\s\r\n]/,'');
            } else {
                var maked_form = $obj.html().replace(/[\s\r\n]/, '');
            }
            return maked_form;
        }

        function showAddForm(type) {
            var addForm = $('.profile-addresses__add-form').html();
            AddAdressPopup.setContent(addForm);
            AddAdressPopup.show();
        }


        $('.profile-addresses__add-btn').click(function (e) {
            var addForm = generateForm();
            AddAdressPopup.setContent(addForm);
            AddAdressPopup.show();
        });

    });
</script>


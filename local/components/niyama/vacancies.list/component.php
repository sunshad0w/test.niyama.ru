<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Список вакансий города
 * @author Sergey Leshchenko, 2014
 * rev.: 06.08.2014 (DD.MM.YYYY)
 */

$arResult = array();
if(empty($arParams['IBLOCK_CODE'])) {
	return;
}

$arParams['CITY_LEVEL_SECTION_ID'] = isset($arParams['CITY_LEVEL_SECTION_ID']) ? intval($arParams['CITY_LEVEL_SECTION_ID']) : 0;
$arParams['CITY_LEVEL_SECTION_ID'] = $arParams['CITY_LEVEL_SECTION_ID'] > 0 ? $arParams['CITY_LEVEL_SECTION_ID'] : 0;
if(!$arParams['CITY_LEVEL_SECTION_ID']) {
	return;
}

$arParams['RESTAURANT_LEVEL_SECTION_ID'] = isset($arParams['RESTAURANT_LEVEL_SECTION_ID']) ? intval($arParams['RESTAURANT_LEVEL_SECTION_ID']) : 0;
$arParams['RESTAURANT_LEVEL_SECTION_ID'] = $arParams['RESTAURANT_LEVEL_SECTION_ID'] > 0 ? $arParams['RESTAURANT_LEVEL_SECTION_ID'] : 0;

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}
$arParams['CACHE_GROUPS'] = isset($arParams['CACHE_GROUPS']) && $arParams['CACHE_GROUPS'] == 'N' ? 'N' : 'Y';

$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/vacancies.list';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sCacheId = md5(serialize(array($arGroups)));
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	//
	// Параметры, которые не должны приниматься из запросов браузера
	//
	$arOrder = array();
	$arParams['SORT_BY1'] = trim($arParams['SORT_BY1']);
	if(!empty($arParams['SORT_BY1'])) {
		$arParams['SORT_ORDER1'] = $arParams['SORT_ORDER1'] != 'DESC' ? 'ASC' : 'DESC';
		$arOrder[$arParams['SORT_BY1']] = $arParams['SORT_ORDER1'];
	}
	$arParams['SORT_BY2'] = trim($arParams['SORT_BY2']);
	if(!empty($arParams['SORT_BY2'])) {
		$arParams['SORT_ORDER2'] = $arParams['SORT_ORDER2'] != 'DESC' ? 'ASC' : 'DESC';
		$arOrder[$arParams['SORT_BY2']] = $arParams['SORT_ORDER2'];
	}

	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['IBLOCK_CODE']);
	$iItemsSectionId = 0;

	//
	// город
	//
	$arResult['CITY'] = array();
	$arSelect = array(
		'ID',
		'IBLOCK_ID',
		'CODE',
		'NAME',
		'SECTION_PAGE_URL',
		'LIST_PAGE_URL',
		'LEFT_MARGIN',
		'RIGHT_MARGIN',
		'UF_*'
	);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
		'DEPTH_LEVEL' => '1',
		'ID' => $arParams['CITY_LEVEL_SECTION_ID']
	);
	$dbItems = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
	if($arItem = $dbItems->GetNext(true, false)) {
		$arResult['CITY'] = $arItem;
	}

	//
	// рестораны
	//
	$arResult['RESTAURANTS'] = array();
	if($arResult['CITY']) {
		$iItemsSectionId = $arResult['CITY']['ID'];

		$arSelect = array(
			'ID',
			'IBLOCK_ID',
			'CODE',
			'NAME',
			'SECTION_PAGE_URL',
			'LIST_PAGE_URL',
			//'UF_*'
		);
		$arFilter = array(
			'IBLOCK_ID' => $iIBlockId,
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'N',
			'DEPTH_LEVEL' => '2',
			'>LEFT_MARGIN' => $arResult['CITY']['LEFT_MARGIN'],
			'<RIGHT_MARGIN' => $arResult['CITY']['RIGHT_MARGIN'],
		);
		$dbItems = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
		while($arItem = $dbItems->GetNext(true, false)) {
			$arResult['RESTAURANTS'][$arItem['ID']] = $arItem;
		}
	}

	if($arParams['RESTAURANT_LEVEL_SECTION_ID']) {
		$iItemsSectionId = 0;
		if($arResult['RESTAURANTS'][$arParams['RESTAURANT_LEVEL_SECTION_ID']]) {
			$iItemsSectionId = $arParams['RESTAURANT_LEVEL_SECTION_ID'];
		}
	}

	//
	// вакансии
	//
	$arResult['ITEMS'] = array();
	if($iItemsSectionId) {
		$arResult['IMG_TYPE_ENUM'] = CCustomProject::GetEnumPropValues($iIBlockId, 'IMG_TYPE');

		$arSelect = array(
			'ID',
			'IBLOCK_ID',
			'CODE',
			'NAME',
			'DETAIL_PICTURE',
			'DETAIL_TEXT',
			'IBLOCK_SECTION_ID',
			'PROPERTY_IMG_TYPE'
		);
		$arFilter = array(
			'IBLOCK_ID' => $iIBlockId,
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'N',
			array(
				'LOGIC' => 'OR',
				// вакансии для ресторанов всех городов
				array(
					'SECTION_ID' => 0
				),
				// вакансии для всех ресторанов города или конкретных ресторанов города
				array(
					'INCLUDE_SUBSECTIONS' => 'Y',
					'SECTION_GLOBAL_ACTIVE' => 'Y',
					'SECTION_SCOPE' => 'IBLOCK',
					'SECTION_ID' => $iItemsSectionId
				)
			)
		);
		$dbItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
		$arItemsId = array();
		while($arItem = $dbItems->GetNext(true, false)) {
			$arItem['_IMG_TYPE_'] = '1';
			if($arItem['PROPERTY_IMG_TYPE_VALUE']) {
				if($arResult['IMG_TYPE_ENUM'][$arItem['PROPERTY_IMG_TYPE_VALUE']]) {
					$arItem['_IMG_TYPE_'] = substr($arResult['IMG_TYPE_ENUM'][$arItem['PROPERTY_IMG_TYPE_VALUE']]['XML_ID'], 5);
				}
			}
			$iTmpPictureId = 0;
			if($arItem['DETAIL_PICTURE']) {
				$iTmpPictureId = $arItem['DETAIL_PICTURE'];
				$arItem['DETAIL_PICTURE'] = CFile::GetFileArray($arItem['DETAIL_PICTURE']);
			}
			// группируем вакансии по полям изображений + название
			$sKey = md5($arItem['_IMG_TYPE_'].'||||'.ToUpper($arItem['NAME']).'||||'.$iTmpPictureId);
			$arResult['ITEMS'][$sKey][$arItem['ID']] = $arItem;
			$arItemsId[] = $arItem['ID'];
		}

		// секции для элементов
		$arResult['ITEMS_SECTIONS'] = array();
		$dbItems = CIBlockElement::GetElementGroups($arItemsId, true, array('ID', 'IBLOCK_ELEMENT_ID'));
		while($arItem = $dbItems->Fetch()) {
			$arResult['ITEMS_SECTIONS'][$arItem['IBLOCK_ELEMENT_ID']][] = $arItem['ID'];
		}
	}

	if(empty($arResult['ITEMS'])) {
		$this->AbortResultCache();
	} else {
		$this->EndResultCache();
	}
}

$this->IncludeComponentTemplate();

return $arResult;

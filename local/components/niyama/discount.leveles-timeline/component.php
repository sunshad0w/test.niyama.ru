<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Таймлайн скидок пользователя
 * @author Sergey Leshchenko, 2014
 * rev.: 11.08.2014 (DD.MM.YYYY)
 */

$arResult = array();
if(empty($arParams['DISCOUNT_LEVELS_IBLOCK_CODE'])) {
	return;
}

if(empty($arParams['USERS_DISCOUNT_LEVELS_IBLOCK_CODE'])) {
	return;
}

$arParams['USER_ID'] = isset($arParams['USER_ID']) ? intval($arParams['USER_ID']) : 0;
$arParams['USER_ID'] = $arParams['USER_ID'] > 0 ? $arParams['USER_ID'] : $GLOBALS['USER']->GetId();

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}


$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/discount.leveles-timeline';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$arParams['CACHE_GROUPS'] = 'N';
$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sCacheId = md5(serialize(array($arGroups)));
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	//
	// Достижения юзера
	//
	$arResult['USER_LEVELS'] = array();
	if($arParams['USER_ID'] > 0) {
		$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['USERS_DISCOUNT_LEVELS_IBLOCK_CODE']);
		$arFilter = array(
			'IBLOCK_ID' => $iIBlockId,
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => 'N',
			'=PROPERTY_R_USER' => $arParams['USER_ID']
		);
		$dbItems = CIBlockElement::GetList(
			array(), 
			$arFilter, 
			false,
			false,
			array(
				'ID', 'IBLOCK_ID', 'DATE_CREATE',
				'PROPERTY_R_DISCOUNT_LEVEL'
			)
		);
		while($arItem = $dbItems->Fetch()) {
			if($arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE']) {
				$arResult['USER_LEVELS'][$arItem['PROPERTY_R_DISCOUNT_LEVEL_VALUE']] = $arItem;
			}
		}
	}

	//
	// Активные уровни скидок
	//
	$arResult['LEVELS'] = array();
	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['DISCOUNT_LEVELS_IBLOCK_CODE']);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'CHECK_PERMISSIONS' => 'N',
	);
	if($arResult['USER_LEVELS']) {
		$arFilter[] = array(
			'LOGIC' => 'OR',
			array(
				'ACTIVE' => 'Y'
			),
			array(
				'ID' => array_keys($arResult['USER_LEVELS'])
			)
		);
	} else {
		$arFilter['ACTIVE'] = 'Y';
	}
	$dbItems = CIBlockElement::GetList(
		array(
			'SORT' => 'ASC',
		), 
		$arFilter, 
		false,
		false,
		array(
			'ID', 'IBLOCK_ID', 'ACTIVE', 'NAME', 'SORT', 'PREVIEW_TEXT', 
			//'DETAIL_TEXT', 
			//'PREVIEW_PICTURE', 'DETAIL_PICTURE',
			'PROPERTY_*'
		)
	);
	while($obItem = $dbItems->GetNextElement(true, false)) {
		$arItem = $obItem->GetFields();
		$arItem['PROPERTIES'] = $obItem->GetProperties();
		$arResult['LEVELS'][$arItem['ID']] = $arItem;
	}

	//$this->EndResultCache();
	$this->IncludeComponentTemplate();
}

return $arResult;

<? if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Оформление заказа
 * @author Sergey Leshchenko, 2014
 * rev.: 05.02.2015 (DD.MM.YYYY)
 */

if(!CModule::IncludeModule('sale')) {
	return;
}

$sCurStep = 'cart';

// костыль для кнопки "назад" с шага информации о заказе
$sCurPage = $GLOBALS['APPLICATION']->GetCurPage(false);
if($_SESSION['NIYAMA']['LAST_PAGE_URL'] && $sCurPage == '/order/' && $_SESSION['NIYAMA']['LAST_PAGE_URL'] == '/order/confirm/' && !isset($_REQUEST['step_back'])) {
	$_REQUEST['step_back'] = 'cart';
}

if($_SESSION['NIYAMA']['LAST_PAGE_URL'] /*&& $sCurPage == '/order/'*/ && (strpos($_SESSION['NIYAMA']['LAST_PAGE_URL'], '/order/pay/') !== false)) {
	$_REQUEST['step_back'] = 'order_info';
	$_SESSION['NIYAMA_ORDER']['STEP'] = 'order_info';
	$sCurStep = 'order_info';
}

if($_SESSION['NIYAMA']['LAST_PAGE_URL'] && (strpos($_SESSION['NIYAMA']['LAST_PAGE_URL'], '/auth/') === false) && (strpos($_SESSION['NIYAMA']['LAST_PAGE_URL'], '/order/') === false)) {
	$_SESSION['NIYAMA_ORDER']['STEP'] = 'cart';
}

if(isset($_REQUEST['step_back']) && $_REQUEST['step_back'] == 'cart') {
	if($_SESSION['NIYAMA_ORDER']['STEP']) {
		$_SESSION['NIYAMA_ORDER']['STEP'] = 'cart';
		$_SESSION['NIYAMA_LIGHT_TEMPLATE'] = false;
		$_SESSION['NIYAMA']['LAST_PAGE_URL'] = $sCurPage;
		localredirect($sCurPage);
	}
}

$arResult = array();

// общие параметры оформления заказа
$arResult['ORDER_DISH_MAX_QUANTITY'] = intval(CNiyamaCustomSettings::GetStringValue('order_dish_max_quantity', 12));
$arResult['ORDER_DISH_MAX_QUANTITY'] = $arResult['ORDER_DISH_MAX_QUANTITY'] > 0 ? $arResult['ORDER_DISH_MAX_QUANTITY'] : 12;
$arResult['ORDER_ALT_TEL'] = CNiyamaCustomSettings::GetStringValue('order_alt_tel', '');
$arResult['ORDER_MIN_PRICE'] = CNiyamaCustomSettings::GetStringValue('min_sum_by_order', 0);
$arResult['STEP_ERRORS'] = array();

$arAvailSteps = array('cart', 'confirm_cart', 'auth', 'skip_auth', 'order_info');
$arLightTemplateSteps = array('auth', 'order_info');


//
// Обработка запросов
//
$bWasPost = false;
$bRedirect = false;
$bMakeOrder = false;
$bCheckCustomerInfo = false;
$arUpdateCartItems = array();
$arUpdateCustomerInfo = array();
$bWasAjax = false;
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$sStepCode = isset($_POST['step']) ? trim(htmlspecialcharsbx($_POST['step'])) : '';
	switch($sStepCode) {
		case 'cart':
			$bWasAjax = true;
		break;

		case 'confirm_cart':
			$bWasPost = true;
			$bRedirect = true;
			$sCurStep = 'order_info';
			if(!$GLOBALS['USER']->IsAuthorized()) {
				$sCurStep = 'auth';
			}
		break;

		case 'auth':
			$bWasPost = true;
			$sCurStep = 'auth';
			if($GLOBALS['USER']->IsAuthorized()) {
				$sCurStep = 'order_info';
			}
		break;

		case 'skip_auth':
			$bWasPost = true;
			$sCurStep = 'order_info';
		break;

		case 'order_info':
			$bWasPost = true;
			$bWasAjax = true;
			$sCurStep = 'order_info';
			$bCheckCustomerInfo = true;
			$bMakeOrder = true;
			$arUpdateCustomerInfo['CUSTOMER_NAME'] = '';
			if(isset($_POST['CUSTOMER_NAME'])) {
				$arUpdateCustomerInfo['CUSTOMER_NAME'] = trim($_POST['CUSTOMER_NAME']);
			}

			$arUpdateCustomerInfo['CUSTOMER_LAST_NAME'] = '';
			if(isset($_POST['CUSTOMER_LAST_NAME'])) {
				$arUpdateCustomerInfo['CUSTOMER_LAST_NAME'] = trim($_POST['CUSTOMER_LAST_NAME']);
			}

			$arUpdateCustomerInfo['CUSTOMER_PHONE'] = '';
			if(isset($_POST['CUSTOMER_PHONE'])) {
				$arUpdateCustomerInfo['CUSTOMER_PHONE'] = trim($_POST['CUSTOMER_PHONE']);
			}

			$arUpdateCustomerInfo['CUSTOMER_EMAIL'] = '';
			if(isset($_POST['CUSTOMER_EMAIL'])) {
				$arUpdateCustomerInfo['CUSTOMER_EMAIL'] = trim($_POST['CUSTOMER_EMAIL']);
			}

			// тип доставки
			$arUpdateCustomerInfo['DELIVERY_TYPE'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_TYPE'])) {
				$arUpdateCustomerInfo['DELIVERY_TYPE'] = trim($_POST['CUSTOMER_DELIVERY_TYPE']);
			}
			$_SESSION['NIYAMA']['ORDER']['CUSTOMER_DELIVERY_TYPE'] = $arUpdateCustomerInfo['DELIVERY_TYPE'];

			// сдача при оплате наличкой
			$arUpdateCustomerInfo['DELIVERY_CHANGE_AMOUNT'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_CHANGE_AMOUNT'])) {
				$arUpdateCustomerInfo['DELIVERY_CHANGE_AMOUNT'] = trim($_POST['CUSTOMER_DELIVERY_CHANGE_AMOUNT']);
			}

			// поля для доставки курьером
			$arUpdateCustomerInfo['DELIVERY_REGION'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_REGION'])) {
				$arUpdateCustomerInfo['DELIVERY_REGION'] = intval($_POST['CUSTOMER_DELIVERY_REGION']);
			}
			$arUpdateCustomerInfo['DELIVERY_CITY_DOP'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_CITY_DOP'])) {
				$arUpdateCustomerInfo['DELIVERY_CITY_DOP'] = trim($_POST['CUSTOMER_DELIVERY_CITY_DOP']);
			}

			$arUpdateCustomerInfo['DELIVERY_SUBWAY'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_SUBWAY_STATION'])) {
				$arUpdateCustomerInfo['DELIVERY_SUBWAY'] = $_POST['CUSTOMER_DELIVERY_SUBWAY_STATION'];
			}

			$arUpdateCustomerInfo['DELIVERY_ADDRESS'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_ADDRESS'])) {
				$arUpdateCustomerInfo['DELIVERY_ADDRESS'] = trim($_POST['CUSTOMER_DELIVERY_ADDRESS']);
			}
			
			$arUpdateCustomerInfo['DELIVERY_HOUSE'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_HOUSE'])) {
				$arUpdateCustomerInfo['DELIVERY_HOUSE'] = trim($_POST['CUSTOMER_DELIVERY_HOUSE']);
			}

			$arUpdateCustomerInfo['DELIVERY_APART'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_APART'])) {
				$arUpdateCustomerInfo['DELIVERY_APART'] = trim($_POST['CUSTOMER_DELIVERY_APART']);
			}

			$arUpdateCustomerInfo['DELIVERY_PORCH'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_PORCH'])) {
				$arUpdateCustomerInfo['DELIVERY_PORCH'] = trim($_POST['CUSTOMER_DELIVERY_PORCH']);
			}

			$arUpdateCustomerInfo['DELIVERY_INTERCOM'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_INTERCOM'])) {
				$arUpdateCustomerInfo['DELIVERY_INTERCOM'] = trim($_POST['CUSTOMER_DELIVERY_INTERCOM']);
			}

			$arUpdateCustomerInfo['DELIVERY_FLOOR'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_FLOOR'])) {
				$arUpdateCustomerInfo['DELIVERY_FLOOR'] = trim($_POST['CUSTOMER_DELIVERY_FLOOR']);
			}

			$arUpdateCustomerInfo['DELIVERY_BY_DATE'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_BY_DATE'])) {
				$arUpdateCustomerInfo['DELIVERY_BY_DATE'] = trim($_POST['CUSTOMER_DELIVERY_BY_DATE']);
			}

			$arUpdateCustomerInfo['DELIVERY_BY_TIME'] = '';
			if(isset($_POST['CUSTOMER_DELIVERY_BY_TIME'])) {
				$arUpdateCustomerInfo['DELIVERY_BY_TIME'] = trim($_POST['CUSTOMER_DELIVERY_BY_TIME']);
			}

			if(!strlen($arUpdateCustomerInfo['DELIVERY_BY_DATE']) || !strlen($arUpdateCustomerInfo['DELIVERY_BY_TIME'])) {
				$arUpdateCustomerInfo['DELIVERY_BY_TIME'] = '';
				$arUpdateCustomerInfo['DELIVERY_BY_DATE'] = '';
			}

			$arUpdateCustomerInfo['DELIVERY_WITHIN_MKAD'] = 'Y';
			if(isset($_POST['CUSTOMER_DELIVERY_WITHIN_MKAD'])) {
				$arUpdateCustomerInfo['DELIVERY_WITHIN_MKAD'] = $_POST['CUSTOMER_DELIVERY_WITHIN_MKAD'] == 'Y' ? 'Y' : 'N';
			}

			// поля для самовывоза
			$arUpdateCustomerInfo['DELIVERY_RESTAURANT'] = '';
			if(isset($_POST['_CUSTOMER_DELIVERY_RESTAURANT_CITY_'])) {
				$iTmpRestaurantCityId = intval($_POST['_CUSTOMER_DELIVERY_RESTAURANT_CITY_']);
				if($iTmpRestaurantCityId && isset($_POST['_CUSTOMER_DELIVERY_RESTAURANT_'][$iTmpRestaurantCityId])) {
					$arUpdateCustomerInfo['DELIVERY_RESTAURANT'] = trim($_POST['_CUSTOMER_DELIVERY_RESTAURANT_'][$iTmpRestaurantCityId]);
				}
			}

			// ! не свойство заказа !
			$arUpdateCustomerInfo['PAYMENT'] = '';
			if(isset($_POST['CUSTOMER_PAYMENT'])) {
				$arUpdateCustomerInfo['PAYMENT'] = trim($_POST['CUSTOMER_PAYMENT']);
			}

			$arUpdateCustomerInfo['USER_DESCRIPTION'] = '';
			if(isset($_POST['USER_DESCRIPTION'])) {
				$arUpdateCustomerInfo['USER_DESCRIPTION'] = trim($_POST['USER_DESCRIPTION']);
			}

		break;
	}
}

$arResult['DRAFT_ORDER'] = CNiyamaOrders::GetCurUserDraftOrder();
$arResult['HAS_NOT_ACTIVE_PRODUCTS'] = 'N';

if($arResult['DRAFT_ORDER']) {
	//
	// Дополнительные компоненты корзины
	//
	CNiyamaOrders::RefreshCartAddComps($arResult['DRAFT_ORDER']['ID']);
	$arTmp = CNiyamaCart::GetCartDopCompList($arResult['DRAFT_ORDER']['ID']);

	$arResult['ADD_COMP'] = $arTmp['ADD_COMP'];

	//
	// Корзина посетителя
	//
	$arResult['CART'] = array();
	$arTableTypes = array('current', 'guest', 'common');
	$arCart = CNiyamaCart::GetCartList();
//_log_array($arCart, '$arCart');
	$arResult['guest_count'] = $arCart['GUEST_CNT'];
	$arCurCartItems = array();
	foreach($arTableTypes as $sTableType) {
		if($arCart[$sTableType]) {
			foreach($arCart[$sTableType] as $iGuestId => $arGuestTable) {
				if($arGuestTable['ITEMS']) {
					foreach($arGuestTable['ITEMS'] as $mKey => $arItem) {
						$arCurCartItems[$arItem['ID']] = $arItem['QUANTITY'];
						$arTmp = CNiyamaOrders::GetProductDataByBasketItem($arItem, true, $GLOBALS['USER']->GetId());
						if($arTmp) {
							if($arTmp['ACTIVE'] != 'Y') {
								$arResult['HAS_NOT_ACTIVE_PRODUCTS'] = 'Y';
							}
							//if(!isset($arResult['CART'][$sTableType][$iGuestId]['CLEAR_PRICE'])) {
							//	$arResult['CART'][$sTableType][$iGuestId]['CLEAR_PRICE'] = 0;
							//}
							if(!isset($arResult['CART'][$sTableType][$iGuestId]['SUM_QUANTITY'])) {
								$arResult['CART'][$sTableType][$iGuestId]['SUM_QUANTITY'] = 0;
							}
							if(($arTmp['TYPE'] != 'coupon') && ($arTmp['TYPE'] != CNiyamaCart::$arCartItemType[3])) {
								//$arResult['CART'][$sTableType][$iGuestId]['CLEAR_PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
								$arResult['CART'][$sTableType][$iGuestId]['SUM_QUANTITY'] += $arItem['QUANTITY'];
							}
							$arResult['CART'][$sTableType][$iGuestId]['ITEMS'][$mKey] = $arItem;
							$arResult['CART'][$sTableType][$iGuestId]['ITEMS'][$mKey]['_PRODUCT_'] = $arTmp;
						}
					}
					if($arResult['CART'][$sTableType][$iGuestId]['ITEMS']) {
						// берем из корзины, т.к. там учтены уже скидки
						$arResult['CART'][$sTableType][$iGuestId]['SUM_PRICE'] = $arGuestTable['PRICE'];
						$arResult['CART'][$sTableType][$iGuestId]['OLD_PRICE'] = $arGuestTable['OLD_PRICE'];
					}
				}
			}
		}
	}

	// хэш заказа, для того, чтобы можно было вернуться к шагу, на котором остановились
	$arResult['ORDER_HASH'] = md5(serialize($arCurCartItems));

	// текущий массив значений сессии
	$arSessionNiyamaOrderPrev = isset($_SESSION['NIYAMA_ORDER']) && is_array($_SESSION['NIYAMA_ORDER']) ? $_SESSION['NIYAMA_ORDER'] : array();

	if(!$bWasPost) {
		$sCurStep = isset($_SESSION['NIYAMA_ORDER']['STEP']) && strlen($_SESSION['NIYAMA_ORDER']['STEP']) ? $_SESSION['NIYAMA_ORDER']['STEP'] : $sCurStep;
		if(!$arResult['CART'] || !$_SESSION['NIYAMA_ORDER']['HASH'] || $_SESSION['NIYAMA_ORDER']['HASH'] != $arResult['ORDER_HASH'] || !in_array($sCurStep, $arAvailSteps)) {
			$sCurStep = 'cart';
		}
	}

	if($sCurStep == 'auth' && $GLOBALS['USER']->IsAuthorized()) {
		$sCurStep = 'order_info';
	}

	if($arResult['HAS_NOT_ACTIVE_PRODUCTS'] == 'Y') {
		$bMakeOrder = false;
		$sCurStep = 'cart';
	}

	$_SESSION['NIYAMA_ORDER']['STEP'] = $sCurStep;
	$_SESSION['NIYAMA_ORDER']['HASH'] = $arResult['ORDER_HASH'];
	$_SESSION['NIYAMA_LIGHT_TEMPLATE'] = false;
	if(in_array($sCurStep, $arLightTemplateSteps) && !$_SESSION['NIYAMA_LIGHT_TEMPLATE']) {
		$_SESSION['NIYAMA_LIGHT_TEMPLATE'] = true;
	}

	$sCheck1 = md5(serialize(array($arSessionNiyamaOrderPrev, $_SESSION['NIYAMA_LIGHT_TEMPLATE'])));
	$sCheck2 = md5(serialize(array($_SESSION['NIYAMA_ORDER'], $_SESSION['NIYAMA_LIGHT_TEMPLATE'])));
	if($sCheck1 != $sCheck2) {
		$bRedirect = true;
	}

	// костыль для кнопки "назад" браузера
	if(!$bWasAjax && !$bWasPost) {
		if($sCurPage == '/order/' && $sCurStep != 'cart') {
			localredirect('/order/confirm/');
		} elseif($sCurPage == '/order/confirm/' && $sCurStep == 'cart') {
			localredirect('/order/');
		}
	}

	if(!$bWasAjax && $bRedirect) {
		localredirect($GLOBALS['APPLICATION']->GetCurPage(false));
	}
	if($sCurStep == 'auth') {
		$_SESSION['NIYAMA_ORDER']['LAST_PAGE_URL'] = $_SESSION['NIYAMA']['LAST_PAGE_URL'];
	}
	$arResult['STEP'] = $sCurStep;

	//
	// Информация о персонах
	//
	$arGuest = CNiyamaCart::GetCurUserGuestsList();

	// Данные посетителя
	$arResult['USER'] = array();
	if($GLOBALS['USER']->IsAuthorized()) {
		$iUserId = $GLOBALS['USER']->GetId();
		$arResult['USER'] = CUsersData::GetDataUserForLK($iUserId);
		$arResult['USER']['ID'] = $iUserId;
	}
	if($arResult['CART']['current']) {
		$arResult['CART']['current'][0]['_PERSON_'] = array(
			'ID' => 0,
			'NAME' => 'Мой стол',
			'IMG_ID' => 0
		);
		if($arResult['USER']) {
			if($arResult['USER']['AVATAR']) {
				$arResult['CART']['current'][0]['_PERSON_']['IMG_ID'] = $arResult['USER']['AVATAR'];
			}
		}
	}
	if($arResult['CART']['guest']) {
		foreach($arResult['CART']['guest'] as $iGuestId => $arGuestTable) {
			$arResult['CART']['guest'][$iGuestId]['_PERSON_'] = array(
				'ID' => $iGuestId,
				'NAME' => $arGuest[$iGuestId] ? $arGuest[$iGuestId]['NAME'] : 'Гость',
				'IMG_ID' => $arGuest[$iGuestId]['PREVIEW_PICTURE'] ? $arGuest[$iGuestId]['PREVIEW_PICTURE'] : 0
			);
		}
	}

	//
	// Итоговая информация по заказу
	//
	if($arResult['DRAFT_ORDER']['ID'] > 0) {
		$arTmpPrice = CNiyamaOrders::GetOrderPrices($arResult['DRAFT_ORDER']['ID']);
		if($arTmpPrice['ORDER_PRICE'] > 0) {
			$arResult['DRAFT_ORDER']['PRICE'] = $arTmpPrice['ORDER_PRICE'];
		}
	}
	$arResult['ORDER'] = array();
	$arResult['ORDER']['PRODUCTS_PRICE'] = 0;
	$arResult['ORDER']['PRICE'] = $arResult['DRAFT_ORDER']['PRICE'];
	$arResult['ORDER']['PRICE_DELIVERY'] = 0;
	$arResult['ORDER']['PERSONS_CNT'] = 0;
	$arResult['ORDER']['GUEST_CNT'] = 0;
	$arResult['ORDER']['CURRENT_CNT'] = 0;
	$arTmp = array();
	foreach($arResult['CART'] as $sTableType => $arTypeCarts) {
		foreach($arTypeCarts as $arGuestTable) {
			if($sTableType != 'common') {
				$arResult['ORDER']['PERSONS_CNT'] += 1;
				if($sTableType == 'guest') {
					$arResult['ORDER']['GUEST_CNT'] += 1;
				} elseif($sTableType == 'current') {
					$arResult['ORDER']['CURRENT_CNT'] += 1;
				}
			}
			$arResult['ORDER']['PRODUCTS_PRICE'] += $arGuestTable['SUM_PRICE'];
			foreach($arGuestTable['ITEMS'] as $arItem) {
				if($arItem['PROPS']['type'] == 'product') {
					$arTmp[$arItem['PRODUCT_ID']] = $arItem['PRODUCT_ID'];
				}
			}
		}
	}

	if((!empty($arResult['guest_count']['QUANTITY'])) && ($arResult['guest_count']['QUANTITY'] > 0)) {
		$arResult['ORDER']['PERSONS_CNT'] = intval($arResult['guest_count']['QUANTITY']);
	}

	$arResult['ORDER']['DISHES_CNT'] = count($arTmp);

	// проверка на минимальную сумму заказа
	if($arResult['ORDER']['PRICE'] < $arResult['ORDER_MIN_PRICE']) {
		$sCurStep = 'cart';
		$arResult['STEP'] = $sCurStep;
		$bMakeOrder = false;
	}

	$arTmp = CNiyamaIBlockCartLine::GetLineByPriceVal($arResult['ORDER']['PRICE']);
	$arResult['ORDER_TIMELINE_BONUSES'] = array();
	foreach($arTmp as $arTmpItem) {
		if($arTmpItem['ACTIVE'] == 'Y') {
			$arResult['ORDER_TIMELINE_BONUSES'][$arTmpItem['XML_ID']] = array(
				'NAME' => $arTmpItem['NAME']
			);
		}
	}

	$arResult['ORDER']['FREE_DELIVERY_CODE'] = '';
	if($arResult['ORDER_TIMELINE_BONUSES']['free_delivery']) {
		$arResult['ORDER']['FREE_DELIVERY_CODE'] = 'free_delivery';
	} elseif($arResult['ORDER_TIMELINE_BONUSES']['free_delivery_mkad']) {
		$arResult['ORDER']['FREE_DELIVERY_CODE'] = 'free_delivery_mkad';
	}

	//
	// Шаг контактной информации
	//
	if($sCurStep == 'order_info' || $bMakeOrder) {
		//
		// Справочники по доставке
		//
		$arTmp = CNiyamaOrders::GetDeliveryRegions();
		$arResult['DELIVERY_REGIONS'] = array();
		foreach($arTmp as $arItem) {
			if($arItem['ACTIVE'] == 'Y') {
				$arResult['DELIVERY_REGIONS'][$arItem['ID']] = $arItem;
			}
		}
		$arResult['SUBWAY_STATIONS'] = CNiyamaIBlockSubway::GetAllStationsList();

		$arResult['PAYMENTS'] = array();

		$arPayments = CNiyamaOrders::GetPayments();
		foreach($arPayments as $arItem) {
			if($arItem['ACTIVE'] == 'Y' && $arItem['PSA_PERSON_TYPE_ID'] == $arResult['DRAFT_ORDER']['PERSON_TYPE_ID']) {
				$arResult['PAYMENTS'][$arItem['ID']] = $arItem;
			}
		}

		$arTmpListProps = CNiyamaOrders::GetOrderListProps(true);
		$arResult['DELIVERY_TYPES'] = array();
		if($arTmpListProps['DELIVERY_TYPE']['VALUES']) {
			$arResult['DELIVERY_TYPES'] = $arTmpListProps['DELIVERY_TYPE']['VALUES'];
		}

		$arResult['DELIVERY_RESTARAUNTS'] = CNiyamaOrders::GetDeliveryRestaurantsList();
		$arResult['DEPARTMENTS'] = CNiyamaIBlockDepartments::GetAllDepartments();

		$arResult['ORDER_INFO'] = array(
			'CUSTOMER_NAME' => '',
			'CUSTOMER_LAST_NAME' => '',
			'CUSTOMER_PHONE' => '',
			'CUSTOMER_EMAIL' => '',
			'DELIVERY_ADDRESS' => '',
			'DELIVERY_HOUSE' => '',
			'DELIVERY_APART' => '',
			'DELIVERY_PORCH' => '',
			'DELIVERY_INTERCOM' => '',
			'DELIVERY_FLOOR' => '',
			'DELIVERY_BY_DATE' => '',
			'DELIVERY_BY_TIME' => '',
			'DELIVERY_REGION' => '',
			'DELIVERY_CITY_DOP' => '',
			'DELIVERY_SUBWAY' => '',

			'DELIVERY_TYPE' => '',
			'DELIVERY_CHANGE_AMOUNT' => '',
			'DELIVERY_RESTAURANT' => '',
			'DELIVERY_WITHIN_MKAD' => '',

			'USER_DESCRIPTION' => ''
		);

		$arDefAddress = CUsersData::getDefaultAddressByUser($arResult['USER']['ID']);
		if(!empty($arDefAddress)) {
			$arResult['ORDER_INFO']['DELIVERY_ADDRESS'] = $arDefAddress['PROPERTY_ADDRESS_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_HOUSE'] = $arDefAddress['PROPERTY_HOUSE_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_APART'] = $arDefAddress['PROPERTY_HOME_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_PORCH'] = $arDefAddress['PROPERTY_PORCH_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_INTERCOM'] = $arDefAddress['PROPERTY_INTERCOM_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_FLOOR'] = $arDefAddress['PROPERTY_FLOOR_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_WITHIN_MKAD'] = true;/*$arDefAddress['PROPERTY_WITHIN_MKAD_VALUE'] == 1 ? true : false;*/
			$arResult['ORDER_INFO']['DELIVERY_REGION'] = $arDefAddress['PROPERTY_REGION_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_CITY_DOP'] = $arDefAddress['PROPERTY_CITY_DOP_VALUE'];
			$arResult['ORDER_INFO']['DELIVERY_SUBWAY'] = $arDefAddress['PROPERTY_SUBWAY_VALUE'];
		}

		$arOrderPropsValue = array();
		$dbItems = CSaleOrderPropsValue::GetOrderProps($arResult['DRAFT_ORDER']['ID']);
		while($arItem = $dbItems->Fetch()) {
			$arOrderPropsValue[$arItem['ORDER_PROPS_ID']][$arItem['ID']] = $arItem;
		}

		if($arUpdateCustomerInfo) {
			$arResult['ORDER_INFO'] = $arUpdateCustomerInfo;
			if(isset($arUpdateCustomerInfo['DELIVERY_REGION'])) {
				$iValId = $arUpdateCustomerInfo['DELIVERY_REGION'];
				$arResult['ORDER_INFO']['DELIVERY_REGION'] = isset($arResult['DELIVERY_REGIONS'][$iValId]) ? $iValId : 0;
			}
			/*
			if(isset($arUpdateCustomerInfo['DELIVERY_SUBWAY'])) {
				$iValId = $arUpdateCustomerInfo['DELIVERY_SUBWAY'];
				$arResult['ORDER_INFO']['DELIVERY_SUBWAY'] = isset($arResult['DELIVERY_SUBWAY']) ? $arResult['DELIVERY_SUBWAY']: '';
			}
			*/
		} elseif($arOrderPropsValue) {
			foreach($arOrderPropsValue as $iPropId => $arPropVals) {
				foreach($arPropVals as $arItem) {
					$arResult['ORDER_INFO'][$arItem['CODE']] = $arItem['VALUE'];
				}
			}
		} elseif($arResult['USER']) {
			$arResult['ORDER_INFO']['CUSTOMER_NAME'] = $arResult['USER']['NAME'];
			$arResult['ORDER_INFO']['CUSTOMER_LAST_NAME'] = $arResult['USER']['LAST_NAME'];
			$arResult['ORDER_INFO']['CUSTOMER_EMAIL'] = $arResult['USER']['EMAIL'];
			$arResult['ORDER_INFO']['CUSTOMER_PHONE'] = $arResult['USER']['PHONE'];
		}
	}

	if($bCheckCustomerInfo) {
		$arTmpErrors = array();
		if(!strlen($arUpdateCustomerInfo['CUSTOMER_NAME'])) {
			$arTmpErrors['EMPTY_CUSTOMER_NAME'] = 'Не задано имя';
		}
		if(!strlen($arUpdateCustomerInfo['CUSTOMER_PHONE'])) {
			$arTmpErrors['EMPTY_CUSTOMER_PHONE'] = 'Не задан контактный телефон';
		} else {
			$sTmp = preg_replace('#[^0-9]+#u', '', $arUpdateCustomerInfo['CUSTOMER_PHONE']);
			if(strlen($sTmp) == 11 && strpos($sTmp, '7') === 0) {
				$sTmp = substr($sTmp, 1);
			}

			if(strlen($sTmp) > 10) {
				$arTmpErrors['EMPTY_CUSTOMER_PHONE'] = 'Контактный телефон должен содержать до 10 символов';
			}
		}
		if(!strlen($arUpdateCustomerInfo['CUSTOMER_EMAIL'])) {
			$arTmpErrors['EMPTY_CUSTOMER_EMAIL'] = 'Не задана электронная почта';
		} elseif(!check_email($arUpdateCustomerInfo['CUSTOMER_EMAIL'])) {
			$arTmpErrors['WRONG_CUSTOMER_EMAIL'] = 'Электронная почта задана некорректно';
		}

		if(!$arUpdateCustomerInfo['PAYMENT'] || !$arResult['PAYMENTS'][$arUpdateCustomerInfo['PAYMENT']]) {
			$arTmpErrors['WRONG_PAYMENT'] = 'Способ оплаты задан некорректно';
		}

		if(!$arUpdateCustomerInfo['DELIVERY_TYPE'] || !$arResult['DELIVERY_TYPES'][$arUpdateCustomerInfo['DELIVERY_TYPE']]) {
			$arTmpErrors['WRONG_DELIVERY_TYPE'] = 'Способ доставки задан некорректно';
		} else {
			switch($arResult['DELIVERY_TYPES'][$arUpdateCustomerInfo['DELIVERY_TYPE']]['VALUE']) {
				case 'courier':
					$arUpdateCustomerInfo['DELIVERY_RESTAURANT'] = '';

					if($arUpdateCustomerInfo['DELIVERY_REGION'] <= 0) {
						$arTmpErrors['EMPTY_DELIVERY_REGION'] = 'Не задан город доставки';
					}
					if(!strlen($arUpdateCustomerInfo['DELIVERY_ADDRESS'])) {
						$arTmpErrors['EMPTY_DELIVERY_ADDRESS'] = 'Не задан адрес доставки';
					}
				break;

				case 'self':
					$arUpdateCustomerInfo['DELIVERY_REGION'] = '';
					$arUpdateCustomerInfo['DELIVERY_CITY_DOP'] = '';
					$arUpdateCustomerInfo['DELIVERY_SUBWAY'] = '';
					$arUpdateCustomerInfo['DELIVERY_ADDRESS'] = '';
					$arUpdateCustomerInfo['DELIVERY_HOUSE'] = '';
					$arUpdateCustomerInfo['DELIVERY_APART'] = '';
					$arUpdateCustomerInfo['DELIVERY_PORCH'] = '';
					$arUpdateCustomerInfo['DELIVERY_INTERCOM'] = '';
					$arUpdateCustomerInfo['DELIVERY_FLOOR'] = '';
					$arUpdateCustomerInfo['DELIVERY_BY_DATE'] = '';
					$arUpdateCustomerInfo['DELIVERY_BY_TIME'] = '';

					if(!$arUpdateCustomerInfo['DELIVERY_RESTAURANT']) {
						$arTmpErrors['EMPTY_DELIVERY_RESTAURANT'] = 'Не задан ресторан для самовывоза';
					} else {
						$arTmpErrors['WRONG_DELIVERY_RESTAURANT'] = 'Некорректно задан ресторан для самовывоза';
						foreach($arResult['DELIVERY_RESTARAUNTS'] as $arItem) {
							if($arItem['DEPARTMENT_XML_ID'] == $arUpdateCustomerInfo['DELIVERY_RESTAURANT']) {
								unset($arTmpErrors['WRONG_DELIVERY_RESTAURANT']);
								break;
							}
						}
					}
				break;

			}
		}

		if($arTmpErrors) {
			$arResult['STEP_ERRORS'] = $arTmpErrors;
		}
	}

	if(!$arResult['STEP_ERRORS'] && $arUpdateCustomerInfo['DELIVERY_TYPE'] == 'courier') {
		// определим возможность и стоимость доставки
		$arTmp = CNiyamaOrders::CalcOrderCourierDelivery(
			array(
				'LOCATION_ID' => $arUpdateCustomerInfo['DELIVERY_REGION'],
				'ORDER_PRICE' => $arResult['ORDER']['PRICE'],
				'B_WITHIN_MKAD' => $arUpdateCustomerInfo['DELIVERY_WITHIN_MKAD'] == 'Y',
				'TIME_PERIOD' => $arUpdateCustomerInfo['DELIVERY_BY_TIME'],
			)
		);

		if($arTmp['DELIVERY_AVAILABLE'] == 'Y') {
			$arResult['ORDER']['PRICE_DELIVERY'] = $arTmp['DELIVERY_PRICE'];
		} else {
			if($arTmp['ORDER_PRICE_LEFT'] > 0) {
				$arResult['DELIVERY_ORDER_PRICE_LEFT'] = $arTmp['ORDER_PRICE_LEFT'];
				$dTmpSum = $arResult['ORDER']['PRICE'] + $arTmp['ORDER_PRICE_LEFT'];
				$arResult['STEP_ERRORS']['INSUF_ORDER_PRICE'] = 'По указанным параметрам минимальная сумма заказа для доставки: '.CProjectUtils::FormatNum($dTmpSum).' руб.';
			} else {
				$arResult['STEP_ERRORS']['DELIVERY_UNVAILABLE'] = 'По указанным параметрам доставка невозможна';
			}
		}
	}

	if($arResult['DRAFT_ORDER']['PRICE_DELIVERY'] != $arResult['ORDER']['PRICE_DELIVERY']) {
		CSaleOrder::Update($arResult['DRAFT_ORDER']['ID'], array('PRICE_DELIVERY' => $arResult['ORDER']['PRICE_DELIVERY']));
	}	
	if($arResult['STEP_ERRORS']) {
		$bMakeOrder = false;
	}

	if($bMakeOrder && $arParams['NO_MAKE_ORDER'] && $arParams['NO_MAKE_ORDER'] == 'Y') {
		$bMakeOrder = false;
	}


    // оформление заказа
	if($bMakeOrder) {


		if($arUpdateCustomerInfo) {
			$dbItems = CSaleOrderProps::GetList(
				array(
					'ID' => 'ASC'
				),
				array(
					'PERSON_TYPE_ID' => $arResult['DRAFT_ORDER']['PERSON_TYPE_ID']
				)
			);

			while($arItem = $dbItems->Fetch()) {
				$arUpdateFields = array();
				switch($arItem['CODE']) {
					case 'CUSTOMER_NAME':
					case 'CUSTOMER_LAST_NAME':
					case 'CUSTOMER_PHONE':
					case 'CUSTOMER_EMAIL':
					case 'DELIVERY_ADDRESS':
					case 'DELIVERY_HOUSE':
					case 'DELIVERY_APART':
					case 'DELIVERY_PORCH':
					case 'DELIVERY_INTERCOM':
					case 'DELIVERY_FLOOR':
					case 'DELIVERY_BY_DATE':
					case 'DELIVERY_BY_TIME':
					case 'DELIVERY_TYPE':
					case 'DELIVERY_CHANGE_AMOUNT':
					case 'DELIVERY_RESTAURANT':
					case 'DELIVERY_WITHIN_MKAD':
					case 'DELIVERY_CITY_DOP':
					case 'DELIVERY_SUBWAY':
						if(isset($arUpdateCustomerInfo[$arItem['CODE']])) {
							$arUpdateFields = array(
								'ORDER_ID' => $arResult['DRAFT_ORDER']['ID'],
								'ORDER_PROPS_ID' => $arItem['ID'],
								'NAME' => $arItem['NAME'],
								'VALUE' => $arUpdateCustomerInfo[$arItem['CODE']],
								'CODE' => $arItem['CODE'],
							);
						}
					break;

                    // сохранение источника заказа, для выгрузки экшнпей
                    case 'SOURCE':
                        if(isset($_COOKIE["actionpay"]) && $_COOKIE["actionpay"] != '') {
                            $arUpdateFields = array(
                                'ORDER_ID' => $arResult['DRAFT_ORDER']['ID'],
                                'ORDER_PROPS_ID' => $arItem['ID'],
                                'NAME' => $arItem['NAME'],
                                'VALUE' => 'actionpay',
                                'CODE' => $arItem['CODE'],
                            );
                        }
                        break;
                    case 'ACTION_PAY_DATA':
                        if(isset($_COOKIE["actionpay"]) && $_COOKIE["actionpay"] != '') {
                            $arUpdateFields = array(
                                'ORDER_ID' => $arResult['DRAFT_ORDER']['ID'],
                                'ORDER_PROPS_ID' => $arItem['ID'],
                                'NAME' => $arItem['NAME'],
                                'VALUE' => $_COOKIE["actionpay"],
                                'CODE' => $arItem['CODE'],
                            );
                        }
                    break;

					case 'DELIVERY_REGION':
						if(isset($arUpdateCustomerInfo[$arItem['CODE']])) {
							$iValId = $arUpdateCustomerInfo[$arItem['CODE']];
							$iValId = isset($arResult['DELIVERY_REGIONS'][$iValId]) ? $iValId : 0;
							$arUpdateFields = array(
								'ORDER_ID' => $arResult['DRAFT_ORDER']['ID'],
								'ORDER_PROPS_ID' => $arItem['ID'],
								'NAME' => $arItem['NAME'],
								'VALUE' => $iValId,
								'CODE' => $arItem['CODE'],
							);
						}
					break;

					/*
					case 'DELIVERY_SUBWAY':
						if(isset($arUpdateCustomerInfo[$arItem['CODE']])) {
							$iValId = $arUpdateCustomerInfo[$arItem['CODE']];
							$sVal = isset($arResult['SUBWAY_STATIONS'][$iValId]) ? $arResult['SUBWAY_STATIONS'][$iValId]['NAME'] : '';
							$arUpdateFields = array(
								'ORDER_ID' => $arResult['DRAFT_ORDER']['ID'],
								'ORDER_PROPS_ID' => $arItem['ID'],
								'NAME' => $arItem['NAME'],
								'VALUE' => $sVal,
								'CODE' => $arItem['CODE'],
							);
						}
					break;
					*/
				}

				if($arUpdateFields) {
					if($arOrderPropsValue[$arUpdateFields['ORDER_PROPS_ID']]) {
						$arTmp = reset($arOrderPropsValue[$arUpdateFields['ORDER_PROPS_ID']]);
						CSaleOrderPropsValue::Update($arTmp['ID'], $arUpdateFields);
					} else {
						CSaleOrderPropsValue::Add($arUpdateFields);
					}
				}
			}

			// акутализация адресов юзера
			if($GLOBALS['USER']->IsAuthorized() && $arUpdateCustomerInfo['DELIVERY_TYPE'] == 'courier') {
				$iAddressIBlockId = CProjectUtils::GetIBlockIdByCode('delivery_address', 'NIYAMA_FOR_USER');
				if($iAddressIBlockId) {
					/*
					$arEnum = CCustomProject::GetEnumPropValues($iAddressIBlockId, 'REGION');
					$iRegionEnumId = 0;
					if($arUpdateCustomerInfo['DELIVERY_REGION'] && $arResult['DELIVERY_REGIONS'][$arUpdateCustomerInfo['DELIVERY_REGION']]) {
						$sTmpCity = trim($arResult['DELIVERY_REGIONS'][$arUpdateCustomerInfo['DELIVERY_REGION']]['NAME']);
						$sTmpCity = $sTmpCity == 'Москва' ? 'Москва' : 'Московская область';
						if($arEnum[$sTmpCity]) {
							$iRegionEnumId = $arEnum[$sTmpCity]['ID'];
						}
					}
					if(!$iRegionEnumId) {
						$arTmp = reset($arEnum);
						$iRegionEnumId = $arTmp['ID'];
					}
					*/
					if($arUpdateCustomerInfo['DELIVERY_REGION'] && $arResult['DELIVERY_REGIONS'][$arUpdateCustomerInfo['DELIVERY_REGION']]) {
						$sRegion=$arUpdateCustomerInfo['DELIVERY_REGION'];
					}
					$sSubway = '';
					if($arResult['DELIVERY_REGIONS'][$arUpdateCustomerInfo['DELIVERY_REGION']]['NAME'] == 'Москва') {
						$sSubway = $arUpdateCustomerInfo['DELIVERY_SUBWAY'];
					}
					$sCityDop = $arUpdateCustomerInfo['DELIVERY_CITY_DOP'];
					// улица
					$sAddress = $arUpdateCustomerInfo['DELIVERY_ADDRESS'];
					// дом
					$sHouse = $arUpdateCustomerInfo['DELIVERY_HOUSE'];
					// корпус, строение, квартира
					$sHome = $arUpdateCustomerInfo['DELIVERY_APART'];
					// подъезд, домофон, этаж
					/*
					$arTmp = array();
					if($arUpdateCustomerInfo['DELIVERY_PORCH']) {
						$arTmp[] = $arUpdateCustomerInfo['DELIVERY_PORCH'];
					}
					if($arUpdateCustomerInfo['DELIVERY_INTERCOM']) {
						$arTmp[] = $arUpdateCustomerInfo['DELIVERY_INTERCOM'];
					}
					if($arUpdateCustomerInfo['DELIVERY_FLOOR']) {
						$arTmp[] = $arUpdateCustomerInfo['DELIVERY_FLOOR'];
					}
					$sPorch = implode(', ', $arTmp);
					*/
					// подъезд
					$sPorch = $arUpdateCustomerInfo['DELIVERY_PORCH'];
					// домофон
					$sIntercom = $arUpdateCustomerInfo['DELIVERY_INTERCOM'];
					// этаж
					$sFloor = $arUpdateCustomerInfo['DELIVERY_FLOOR'];

					$sWithinMkad = 1;//$arUpdateCustomerInfo['DELIVERY_WITHIN_MKAD'] == 'Y' ? 1 : 0;

					$arUserAddresses = CUsersData::GetAddressList($GLOBALS['USER']->GetId());
					$iIsDefault = empty($arUserAddresses) ? 1 : 0;

					$bAdd = true;
					$sCheckStr = $sRegion.'|||'.$sSubway.'|||'.$sAddress.'|||'.$sHouse.'|||'.$sHome.'|||'.$sPorch.'|||'.$sIntercom.'|||'.$sFloor.'|||'/*.$sWithinMkad.'|||'*/.$sCityDop;
					foreach($arUserAddresses as $arTmpItem) {
						$sCheckStr2 = $arTmpItem['PROPERTY_REGION_VALUE'].'|||'.$arTmpItem['PROPERTY_SUBWAY_VALUE'].'|||'.$arTmpItem['PROPERTY_ADDRESS_VALUE'].'|||'.$arTmpItem['PROPERTY_HOUSE_VALUE'].'|||'.$arTmpItem['PROPERTY_HOME_VALUE'].'|||'.$arTmpItem['PROPERTY_PORCH_VALUE'].'|||'.$arTmpItem['PROPERTY_INTERCOM_VALUE'].'|||'.$arTmpItem['PROPERTY_FLOOR_VALUE'].'|||'/*.$arTmpItem['PROPERTY_WITHIN_MKAD_VALUE'].'|||'*/.$arTmpItem['PROPERTY_CITY_DOP_VALUE'];

						if($sCheckStr == $sCheckStr2) {
							$bAdd = false;
							break;
						}
					}
					if($bAdd) {
						CUsersData::AddAddress($sRegion, $sCityDop, $sSubway, $sAddress, $sHouse, $sHome, $sPorch, $sIntercom, $sFloor, $sWithinMkad, $iIsDefault, $GLOBALS['USER']->GetId());
					}
				}
			}

			$arUpdateOrderFields = array();
			// платежная система
			$bPaymentCash = true;
			if($arUpdateCustomerInfo['PAYMENT'] && $arResult['PAYMENTS'][$arUpdateCustomerInfo['PAYMENT']]) {
				$arUpdateOrderFields['PAY_SYSTEM_ID'] = $arUpdateCustomerInfo['PAYMENT'];
				if($arResult['PAYMENTS'][$arUpdateCustomerInfo['PAYMENT']]['IS_CASH'] != 'Y') {
					$bPaymentCash = false;
				}
			}


			if(isset($arUpdateCustomerInfo['USER_DESCRIPTION'])) {
				$arUpdateOrderFields['USER_DESCRIPTION'] = $arUpdateCustomerInfo['USER_DESCRIPTION'];
			}

			if($arUpdateOrderFields) {
				CSaleOrder::Update($arResult['DRAFT_ORDER']['ID'], $arUpdateOrderFields);
			}

			$_SESSION['NIYAMA_LIGHT_TEMPLATE'] = false;

			if($bPaymentCash) {
				// сменим статус на "экспорт в FO" (отправку заказа в FO выполнит обработчик)
				$_SESSION['NIYAMA_ORDER'] = array();
				CSaleOrder::StatusOrder($arResult['DRAFT_ORDER']['ID'], 'E');
			} else {
				// сменим статус на "ожидает оплаты"
				CSaleOrder::StatusOrder($arResult['DRAFT_ORDER']['ID'], 'P');
			}

			if(!$bPaymentCash) {
				$_SESSION['NIYAMA']['LAST_PAGE_URL'] = '/order/pay/?id='.$arResult['DRAFT_ORDER']['ID'];
				localredirect('/order/pay/?id='.$arResult['DRAFT_ORDER']['ID']);
			} else {
				localredirect('/order/success/?id='.$arResult['DRAFT_ORDER']['ID']);
			}
		}
	}
}

$this->IncludeComponentTemplate();

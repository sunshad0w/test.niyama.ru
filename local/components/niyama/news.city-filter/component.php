<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Список городов и ресторнаов для фильтрации новостей
 * @author Sergey Leshchenko, 2014
 * rev.: xx.06.2014 (DD.MM.YYYY)
 */

$arResult = array();
$arParams['NEWS_IBLOCK_ID'] = isset($arParams['NEWS_IBLOCK_ID']) ? intval($arParams['NEWS_IBLOCK_ID']) : 0;
if($arParams['NEWS_IBLOCK_ID'] <= 0) {
	return;
}

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}
$arParams['CACHE_GROUPS'] = isset($arParams['CACHE_GROUPS']) && $arParams['CACHE_GROUPS'] == 'N' ? 'N' : 'Y';

if(!strlen(trim($arParams['ELEMENT_FILTER_NAME']))) {
	$arExtElementFilter = array();
} elseif(!preg_match('#^[A-Za-z_][A-Za-z01-9_]*$#', $arParams['ELEMENT_FILTER_NAME'])) {
	$arExtElementFilter = array();
} else {
	$arExtElementFilter = isset($GLOBALS[$arParams['ELEMENT_FILTER_NAME']]) ? $GLOBALS[$arParams['ELEMENT_FILTER_NAME']] : array();
	if(!is_array($arExtElementFilter)) {
		$arExtElementFilter = array();
	}
}
$arParams['_EXT_FILTER_'] = $arExtElementFilter;

$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/news.city-filter';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sCacheId = md5(serialize(array($arGroups, $arExtElementFilter)));
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	// города и рестораны, явно связанные с новостями
	$arResult['NEWS_FOUND'] = 'N';
	$arResult['R_CITIES'] = array();
	$arResult['R_RESTAURANTS'] = array();
	$arFilter = array(
		'IBLOCK_ID' => $arParams['NEWS_IBLOCK_ID'],
		'ACTIVE' => 'Y',
		'ACTIVE_DATE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
	);
	$arFilter = array_merge($arFilter, $arExtElementFilter);
	$dbItems = CIBlockElement::GetList(
		array(),
		$arFilter,
		array(
			'PROPERTY_R_CITY', 'PROPERTY_R_RESTAURANT'
		)
	);
	while($arItem = $dbItems->Fetch()) {
		$arResult['NEWS_FOUND'] = 'Y';
		if($arItem['PROPERTY_R_CITY_VALUE']) {
			$arResult['R_CITIES'][$arItem['PROPERTY_R_CITY_VALUE']] = $arItem['PROPERTY_R_CITY_VALUE'];
		}
		if($arItem['PROPERTY_R_RESTAURANT_VALUE']) {
			$arResult['R_RESTAURANTS'][$arItem['PROPERTY_R_RESTAURANT_VALUE']] = $arItem['PROPERTY_R_RESTAURANT_VALUE'];
		}
	}

	$arResult['RESTAURANTS'] = array();
	$arResult['CITIES'] = array();
	$arResult['CITY-RESTAURANTS'] = array();
	if($arResult['NEWS_FOUND'] == 'Y') {
		// список ресторанов
		$arResult['RESTAURANTS'] = CCustomProject::GetRestaurantsList();
		// список городов ресторанов
		$arResult['CITIES'] = CCustomProject::GetRestaurantsCitiesList();

		foreach($arResult['RESTAURANTS'] as $arItem) {
			$arResult['CITY-RESTAURANTS'][$arItem['PROPERTY_R_CITY_VALUE']][$arItem['ID']] = $arItem['ID'];
		}
	}

	if($arResult['NEWS_FOUND'] != 'Y') {
		$this->AbortResultCache();
	} else {
		$this->EndResultCache();
	}
}

$this->IncludeComponentTemplate();

return $arResult;

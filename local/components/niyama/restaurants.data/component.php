<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Панель фильтра ресторанов
 * @author Sergey Leshchenko, 2014
 * rev.: 30.07.2014 (DD.MM.YYYY)
 */

$arResult = array();
if(empty($arParams['RESTAURANTS_IBLOCK_CODE'])) {
	return;
}

if(empty($arParams['SERVICES_IBLOCK_CODE'])) {
	return;
}

if(empty($arParams['CITIES_IBLOCK_CODE'])) {
	return;
}

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}

if(!isset($arParams['CITY'])) {
	$arParams['CITY'] = 0;
}

if(!isset($arParams['SUBWAY'])) {
	$arParams['SUBWAY'] = 0;
}

$arParams['CACHE_GROUPS'] = isset($arParams['CACHE_GROUPS']) && $arParams['CACHE_GROUPS'] == 'N' ? 'N' : 'Y';

$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/restaurants.data';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sElementCode = isset($_REQUEST['ELEMENT_CODE']) ? trim($_REQUEST['ELEMENT_CODE']) : '';
$bDetailPage = strlen($sElementCode) ? true : false;

$sCacheId = md5(serialize(array($arGroups)).$sElementCode);
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	//
	// Сервисы
	//
	$arResult['SERVICES'] = array();
	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['SERVICES_IBLOCK_CODE']);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
	);
	$dbItems = CIBlockElement::GetList(
		array(
			'SORT' => 'ASC',
			'NAME' => 'ASC'
		), 
		$arFilter, 
		false,
		false,
		array(
			'ID', 'NAME', 'CODE', 'PREVIEW_PICTURE'
		)
	);
	while($arItem = $dbItems->GetNext(true, false)) {
		$arItem['_USED_'] = 'N';
		unset($arItem['SORT']);
		if($arItem['PREVIEW_PICTURE']) {
			$arItem['PREVIEW_PICTURE'] = CFile::GetFileArray($arItem['PREVIEW_PICTURE']);
		}
		$arResult['SERVICES'][$arItem['ID']] = $arItem;
	}

	//
	// Города
	//
	$arResult['CITIES'] = array();
	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['CITIES_IBLOCK_CODE']);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
	);
	$dbItems = CIBlockElement::GetList(
		array(
			'SORT' => 'ASC',
			'NAME' => 'ASC'
		), 
		$arFilter, 
		false,
		false,
		array(
			'ID', 'NAME', 'CODE'
		)
	);
	while($arItem = $dbItems->GetNext(true, false)) {
		$arItem['_USED_'] = 'N';
		unset($arItem['SORT']);
		$arResult['CITIES'][$arItem['ID']] = $arItem;
	}

	//
	// Метро
	//
	$arResult['SUBWAY'] = array();
	$arAllStationsList = CNiyamaIBlockSubway::GetAllStationsList();
	foreach($arAllStationsList as $arItem) {
		$arItem['_USED_'] = 'N';
		$arResult['SUBWAY'][$arItem['ID']] = $arItem;
	}

	//
	// Рестораны
	//
	$arResult['RESTAURANTS'] = array();
	$arResult['SUBWAY2CITY'] = array();
	$arResult['SERVICES2CITY'] = array();

	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['RESTAURANTS_IBLOCK_CODE']);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
	);
	$dbItems = CIBlockElement::GetList(
		array(
			'SORT' => 'ASC',
			'NAME' => 'ASC'
		), 
		$arFilter, 
		false, 
		false,
		array(
			'ID', 'NAME', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'IBLOCK_SECTION_ID', 'DETAIL_TEXT',
			'PROPERTY_*',
		)
	);
	while($obItem = $dbItems->GetNextElement(true, false)) {
		$arItem = $obItem->GetFields();
		$arItem['PROPERTIES'] = $obItem->GetProperties();

		foreach($arItem['PROPERTIES'] as $sPropCode => $arProp) {
			if($arProp['PROPERTY_TYPE'] == 'F' && $arProp['VALUE']) {
				$arTmpVal = is_array($arProp['VALUE']) ? $arProp['VALUE'] : array($arProp['VALUE']);
				foreach($arTmpVal as $iTmpVal) {
					$arItem['PROPERTIES'][$sPropCode]['_VALUE_'][$iTmpVal] = CFile::GetFileArray($iTmpVal);
				}
			}
		}

		$iTmpCityId = 0;
		if($arItem['PROPERTIES']['R_CITY']['VALUE']) {
			$iTmpCityId = $arItem['PROPERTIES']['R_CITY']['VALUE'];
			if($arResult['CITIES'][$iTmpCityId]) {
				$arResult['CITIES'][$iTmpCityId]['_USED_'] = 'Y';
			}
		}

		if($arItem['PROPERTIES']['R_SERVICES']['VALUE'] && is_array($arItem['PROPERTIES']['R_SERVICES']['VALUE'])) {
			foreach($arItem['PROPERTIES']['R_SERVICES']['VALUE'] as $iTmpVal) {
				if($arResult['SERVICES'][$iTmpVal]) {
					$arResult['SERVICES'][$iTmpVal]['_USED_'] = 'Y';
					$arResult['SERVICES2CITY'][$iTmpCityId][$iTmpVal] = $iTmpVal;
				}
			}
		}
		if($arItem['PROPERTIES']['R_SUBWAY']['VALUE']) {
			$iTmpId = $arItem['PROPERTIES']['R_SUBWAY']['VALUE'];
			if($arResult['SUBWAY'][$iTmpId]) {
				$arResult['SUBWAY'][$iTmpId]['_USED_'] = 'Y';
				$arResult['SUBWAY2CITY'][$iTmpCityId][$iTmpId] = $iTmpId;
			}
		}

		if($bDetailPage) {
			if($sElementCode == $arItem['CODE']) {
				$arParams['CITY'] = $arItem['PROPERTIES']['R_CITY']['VALUE'];
				$arParams['SUBWAY'] = $arItem['PROPERTIES']['R_SUBWAY']['VALUE'];

			}
		}

		$arResult['RESTAURANTS'][$arItem['ID']] = $arItem;
	}

	//
	// Группы ресторанов
	//
	$arResult['GROUPS'] = array();

	$iIBlockId = CProjectUtils::GetIBlockIdByCode($arParams['RESTAURANTS_IBLOCK_CODE']);
	$arFilter = array(
		'IBLOCK_ID' => $iIBlockId,
		'ACTIVE' => 'Y',
		'GLOBALS_ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
	);
	$dbItems = CIBlockSection::GetList(
		array(
			// !!!
			'LEFT_MARGIN' => 'ASC',
		),
		$arFilter, 
		false,
		array(
			'ID', 'NAME', 'DEPTH_LEVEL', 'LEFT_MARGIN', 'RIGHT_MARGIN', 'IBLOCK_SECTION_ID'
		)
	);
	while($arItem = $dbItems->GetNext(true, false)) {
		$arResult['GROUPS'][$arItem['ID']] = $arItem;
	}

	$this->EndResultCache();
}

$this->IncludeComponentTemplate();

return $arResult;

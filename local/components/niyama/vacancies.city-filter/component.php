<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();
/**
 * Список городов для фильтрации вакансий
 * @author Sergey Leshchenko, 2014
 * rev.: xx.06.2014 (DD.MM.YYYY)
 */

$arResult = array();
if(empty($arParams['IBLOCK_CODE'])) {
	return;
}

if(!isset($arParams['CACHE_TIME'])) {
	$arParams['CACHE_TIME'] = 43200;
}
if($arParams['CACHE_TYPE'] == 'N' || ($arParams['CACHE_TYPE'] == 'A' && COption::GetOptionString('main', 'component_cache_on', 'Y') == 'N')) {
	$arParams['CACHE_TIME'] = 0;
}
$arParams['CACHE_GROUPS'] = isset($arParams['CACHE_GROUPS']) && $arParams['CACHE_GROUPS'] == 'N' ? 'N' : 'Y';
$arParams['EXT_FILTER_NAME'] = strlen($arParams['EXT_FILTER_NAME']) ? $arParams['EXT_FILTER_NAME'] : 'arVacanciesFilterExt';

$sCacheDir = !empty($arParams['CACHE_DIR']) ? $arParams['CACHE_DIR'] : SITE_ID.'/vacancies.city-filter';
$sCacheDir = rtrim($sCacheDir, '/').'/';
$sCacheDir = '/'.ltrim($sCacheDir, '/');
$sCachePath = $sCacheDir; 

$arGroups = $arParams['CACHE_GROUPS'] == 'Y' ? $GLOBALS['USER']->GetGroups() : array();

$sCacheId = md5(serialize(array($arGroups)));
if($this->StartResultCache($arParams['CACHE_TIME'], $sCacheId, $sCachePath)) {
	if(!CModule::IncludeModule('iblock')) {
		$this->AbortResultCache();
		return;
	}

	//
	// Параметры, которые не должны приниматься из запросов браузера
	//
	$arOrder = array();
	$arParams['SORT_BY1'] = trim($arParams['SORT_BY1']);
	if(!empty($arParams['SORT_BY1'])) {
		$arParams['SORT_ORDER1'] = $arParams['SORT_ORDER1'] != 'DESC' ? 'ASC' : 'DESC';
		$arOrder[$arParams['SORT_BY1']] = $arParams['SORT_ORDER1'];
	}
	$arParams['SORT_BY2'] = trim($arParams['SORT_BY2']);
	if(!empty($arParams['SORT_BY2'])) {
		$arParams['SORT_ORDER2'] = $arParams['SORT_ORDER2'] != 'DESC' ? 'ASC' : 'DESC';
		$arOrder[$arParams['SORT_BY2']] = $arParams['SORT_ORDER2'];
	}

	$arResult['ITEMS'] = array();
	$arSelect = array(
		'ID',
		'IBLOCK_ID',
		'CODE',
		'NAME',
		'SECTION_PAGE_URL',
		'LIST_PAGE_URL',
		'UF_*'
	);
	$arFilter = array(
		'IBLOCK_ID' => CProjectUtils::GetIBlockIdByCode($arParams['IBLOCK_CODE']),
		'ACTIVE' => 'Y',
		'CHECK_PERMISSIONS' => 'N',
		'DEPTH_LEVEL' => '1',
		'ELEMENT_SUBSECTIONS' => 'Y',
		'CNT_ACTIVE' => 'Y',
	);
	$dbItems = CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
	while($arItem = $dbItems->GetNext(true, false)) {
		$arResult['ITEMS'][$arItem['ID']] = $arItem;
	}

	$this->EndResultCache();
}

$this->IncludeComponentTemplate();

return $arResult;

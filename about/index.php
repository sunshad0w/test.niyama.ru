<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Ресторан Пицца Пи - отменная еда и напитки с доставкой на дом в Москве! Акции и скидки каждый день! Звони! ✆ +7 495 781-781-9");
$APPLICATION->SetTitle("Официальный сайт ресторана Пицца Пи - О нас");

?>
<div class="main-content">
    <div class="main-content__wrapper">
        <div class="catalog-wrapper">
            <div class="container">

                <h4 class="page__title">О нас</h4>

                <div class="page__block">
                    <!--<img class="page__img" src="/upload/iblock/a4b/a4bea34ca816b2828e4946cd84cb1713.jpg" />-->
                </div>

                <div class="page__block">
                    <p>5 интересных фактов о нашей компании, которые вы сможете рассказать всем своим знакомым и друзьям.</p>
                    <ol>
                    <li>Пицца, сделанная настоящими профессионалами из Италии.<br>
                        Чтобы рецепты нашей пиццы были индивидуальными и не похожими на любые другие, мы пригласили для работы искусного мастера из Италии. Именно он сделал нашу пиццу незабываемой.</li>
                    <li>Пицца с дымком.<br>
                        Именно с дымком, а точнее с его ароматом, так как изготовление пиццы происходит в печи топка, которой происходит на настоящих дровах. Именно поэтому пицца полностью пропекается и сохраняет все вкусы и ароматы.</li>
                    <li>Исключительная паста.<br>
                    Мы изготавливаем пасту исключительно из высших сортов муки и чистой воды. В некоторых случаях чтобы сделать вкус пасты индивидуальным, наши мастера добавляют маленькую каплю чернил каракатицы или же мелко порубленный базилик.</li>
                    <li>Оригинальное обслуживание.<br>
                    В нашем ресторане вас могут обслуживать персона с настоящими итальянскими именами. Конечно они не итальянцы, но это одно из правил нашего заведения, чтобы вы почувствовали полноценную итальянскую атмосферу.</li>
                    <li>Доставка пиццы.</li>
                    Пиццу можно доставить по Москве и Московской области. Готовое блюдо можно заказать домой или на работу, а также вы можете сами забрать ее из наших ресторанов. Доставка осуществляется в течение одного часа и при этом она бесплатна.
                    </ol>
                    <p>Наш ресторан порадует вас самой оригинальной итальянской пиццей и пастой. В нашем заведении используется лишь самое высокое качество продуктов, которое доказано всеми сертификатами соответствия. Наши повара изготовят наивкуснейшее блюдо вкус, которого запомнится вам надолго. Качественное тесто, вкуснейший сыр Моцарелла и оригинальный соус – это все 100% пицца. Заказывайте пиццу у нас, и мы привезем ее вам в самые короткие сроки.</p>
                </div>

                <p class="about__text _last" style="border-top: 1px dashed lightgray; display: block;">
                </p>
                <div class="page__block">
                    <h1 class="req">Наши реквизиты</h1><span class="toggle_requisits">показать</span>
                    <div class="requisits" style="display: none;">
                    <h2>Общество с ограниченной ответственностью «Домашние Системы»</h2>
                    <p class="about__text _last">
                        <strong>Краткое наименование организации:</strong> ООО «Домашние Системы»<br>
                        <strong>ОГРН:</strong> 1107746354275<br>
                        <strong>ИНН/КПП:</strong> 7731649107/773101001<br>
                        <strong>Юридический адрес:</strong> 121471, г. Москва, Можайское ш., дом 29<br>
                        <strong>Фактический адрес:</strong> 140053,МО, г. Люберцы, Новорязанское ш., д. 7<br>
                        <strong>Почтовый адрес:</strong> 121552,РФ, г. Москва, ул. Ярцевская, д.25А<br>
                        <strong>ОКПО:</strong> 66390623<br>
                        <strong>ОКВЭД:</strong> 56.10<br>
                        <strong>Телефон центрального офиса:</strong> 8(495)287-95-44<br>
                        <strong>Телефон ресторана:</strong> 8(495)790-34-15<br>
                        <strong>Банковские реквизиты:</strong><br>
                        Р/с № 40702810196430000116 <br>
                        в ПАО «РОСБАНК»<br>
                        БИК 044525256 <br>
                        к/с 30101810000000000256<br>
                        <strong>Генеральный директор:</strong> Паршуткина Валентина Дмитриевна
                    </p>
                    <h2 >Общество с ограниченной ответственностью «Пицца и Cуши»</h2>
                    <p class="about__text _last">
                        <strong>Краткое наименование организации:</strong> ООО «Пицца и Cуши»<br>
                        <strong>ОГРН:</strong> 1147746148330<br>
                        <strong>ИНН/КПП:</strong> 7724909996/772401001<br>
                        <strong>Юридический адрес:</strong> 115682, г. Москва, улица Шипиловская, д. 64, корп. 1, офис 147<br>
                        <strong>Фактический адрес:</strong> г. Москва, ул. Пятницкая, д.3 /4, стр.1 <br>
                        <strong>Почтовый адрес:</strong> 115682, г. Москва, улица Шипиловская, д. 64, корп. 1, офис 147<br>
                        <strong>ОКПО:</strong> 29014098 <br>
                        <strong>ОКТМО:</strong> 45916000000 <br>
                        <strong>ОКВЭД:</strong> 56.10<br>
                        <strong>Телефон:</strong> 8(495)287-95-44<br>
                        <strong>Банковские реквизиты:</strong> Р/с № 40702810996430000167 в ПАО «РОСБАНК», <br>
                        БИК 044525256 к/с 30101810000000000256<br>
                        <strong>Генеральный директор:</strong> Миргородская Лариса Васильевна<br>
                    </p>
                    <h2 >Общество с ограниченной ответственностью «Каппабаши»</h2>
                    <p class="about__text _last">
                        <strong>Краткое наименование организации:</strong> ООО «Каппабаши»<br>
                        <strong>ОГРН:</strong> 1107746922766<br>
                        <strong>ИНН/КПП:</strong> 7731660340/773101001<br>
                        <strong>Юридический адрес:</strong> 121471, г. Москва, Можайское ш., дом 29<br>
                        <strong>Фактический адрес:</strong> 113152 г. Москва, Севастопольский пр-т, д. 11Е <br>
                        <strong>Почтовый адрес:</strong> 121471, г. Москва, Можайское шоссе, дом 29<br>
                        <strong>ОКПО:</strong> 68958246<br>
                        <strong>ОКВЭД:</strong> 56.10 <br>
                        <strong>Телефон центрального офиса:</strong> 8(495)287-95-44<br>
                        <strong>Телефон ресторана:</strong> 8(495)961-59-14<br>
                        <strong>Банковские реквизиты:</strong> Р/с № 40702810196430000145 в ПАО «РОСБАНК» <br>
                        БИК 044525256, к/с 30101810000000000256<br>
                        <strong>Генеральный директор:</strong> Даниленко Сергей Анатольевич<br>
                    </p>
                    <h2 >Общество с ограниченной ответственностью «Кастильоне»</h2>
                    <p class="about__text _last">
                        <strong>Краткое наименование организации:</strong> ООО «Кастильоне»<br>
                        <strong>ОГРН:</strong> 1147746195541<br>
                        <strong>ИНН/КПП:</strong> 7724911610/772401001<br>
                        <strong>Юридический адрес:</strong> 115682, г. Москва, улица Шипиловская, д. 64, корп. 1, офис 147<br>
                        <strong>Фактический адрес:</strong> г.Москва, ул. Митинская, д.39<br>
                        <strong>Почтовый адрес:</strong> 115682, г. Москва, улица Шипиловская, д. 64, корп. 1, офис 147<br>
                        <strong>ОКПО:</strong> 29089222 <br>
                        <strong>ОКТМО:</strong> 45916000000<br>
                        <strong>ОКВЭД:</strong> 56.10<br>
                        <strong>Телефон:</strong> 8(495)287-95-44<br>
                        <strong>Банковские реквизиты:</strong> р/с № 40702810596430000172 в ПАО "РОСБАНК" <br>
                        БИК 044525256 корреспондентский счет № 30101810000000000256<br>
                        <strong>Генеральный директор:</strong> Черненок Александр Алексеевич
                    </p>
                    </div>
                </div>

                <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

            </div>
        </div>
    </div>
</div>
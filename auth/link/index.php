<?php

define("NIYAMA_LIGHT_TEMPLATE", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


$APPLICATION->IncludeComponent(
	'adv:social.auth', 
	'niyama.1.0',
	array(
    	'LINK_ACCOUNTS' => 'Y'
	), 
	null, 
	array(
		'HIDE_ICONS' => 'Y'
	)
);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

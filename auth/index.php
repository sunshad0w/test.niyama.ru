<?
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

$APPLICATION->SetTitle('Авторизация');
?>
<div class="main-content">
	<div class="main-content__wrapper">

            <? if(CUser::IsAuthorized()): ?>

                <div class="catalog-wrapper">
                    <div class="container">
                        <h4 class="page__title">Вы зарегистрированы и успешно авторизовались</h4>
                        <div class="page__block">
                            <div class="auth">
                                <p><a href="/">Вернуться на главную страницу</a></p>
                            </div>
                        </div>
                    </div>
                </div>

            <? else:?>
                <?
                    $APPLICATION->IncludeComponent('bitrix:system.auth.confirmation' ,'', array(
                        "USER_ID" => "confirm_user_id",
                        "CONFIRM_CODE" => "confirm_code",
                        "LOGIN" => "login"
                    ));
                ?>
            <? endif;?>


			<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

	</div>
</div>

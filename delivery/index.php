<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Настоящая итальянская еда от ресторана Пицца-Пи с круглосуточной бесплатной доставкой. Акции и скидки каждый день! ☎+7 495 781-781-9");
//$APPLICATION->SetTitle("Условия доставки пиццы и горячих блюд от ресторана Пицца Пи в Москве");

$json = json_decode(file_get_contents(__DIR__ . '/../user834.wlp'), true);
$zones = array();
foreach ($json['zones'] as $zone) {
    if (preg_match('/(зона|Зона)/i', $zone['d'])) {
        $zones[] = array(
            'color' => $zone['c'],
            'description' => trim(preg_replace('/^.*\:/U', '', $zone['d'])),
            'points' => $zone['p'],
            'id' => $zone['id'],
            'name' => $zone['n']
        );
    }
}
?>
    <script type="application/javascript">
        window.zones = <?=json_encode($zones)?>;
    </script>

    <div class="main-content">
        <div class="main-content__wrapper">

            <div class="catalog-wrapper">
                <div class="container">
                    <div class="specific">


                        <h4 class="page__delivery__title">Как получить заказ</h4>

                        <div class="how__take__order block__wrapper">

                            <div class="block__delivery__description courer__delivery">
                                <img class="delivery__img"
                                     src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/pizzadeliverycourer.png"/>
                                <div class="delivery__title">Доставка курьером</div>
                            </div>

                            <div class="block__delivery__description courer__delivery__totime">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/watch.png"/>
                                <div class="delivery__title">Доставка курьером ко времени</div>
                            </div>

                            <div class="block__delivery__description pickup__delivery">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/bag.png"/>
                                <div class="delivery__title">Самовывоз из ресторана</div>
                            </div>
                        </div>


                        <h4 class="page__delivery__title">Способы оплаты</h4>
                        <div class="how__to__pay block__wrapper">
                            <div class="block__delivery__description cashe">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/cashe.png"/>
                                <div class="delivery__title">Наличными</div>
                            </div>
                            <div class="block__delivery__description card">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/card.png"/>
                                <div class="delivery__title">Банковской картой</div>
                            </div>
                            <div class="block__delivery__description card">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/card.png"/>
                                <div class="delivery__title">Банковской картой курьеру</div>
                            </div>
                            <div class="delivery__block__spacehack"></div>
                        </div>


                        <h4 class="page__delivery__title">Условия доставки</h4>
                        <div class="how__to__delivery block__wrapper">

                            <div class="block__delivery__description evrytime">
                                <img class="delivery__img" src="<?= SITE_TEMPLATE_PATH ?>/img/delivery/sun.png"/>
                                <div class="delivery__title">Круглосуточно
                                    <span>(за исключением одинцово и митино)</span>
                                </div>
                            </div>

                            <!-- <div class="block__delivery__description allresourans">
                            <img class="delivery__img" src="<? /*= SITE_TEMPLATE_PATH */ ?>/img/delivery/hous.png" />
                            <div class="delivery__title">Все рестораны</div>
                        </div>-->

                            <div class="block__delivery__description deliverytime">
                                <div class="delivery__no__images">
                                    <span class="delivery__no__images__Text">от</span>
                                    <span class="delivery__no__images__number">60</span>
                                    <span class="delivery__no__images__Text">Минут</span>
                                </div>
                                <div class="delivery__title delivery__title__custom">Время доставки*</div>
                            </div>

                            <div class="block__delivery__description minprice">
                                <div class="delivery__no__images delivery__no__images__minprice">
                                    <span class="delivery__no__images__Text">от</span>
                                    <span class="delivery__no__images__number">781</span>
                                    <span class="delivery__no__images__Text">рублей</span>
                                </div>
                                <div class="delivery__title delivery__title__custom">Минимальная сумма**</div>
                            </div>

                           <div class="block__delivery__description">
                                <div class="delivery__no__images delivery__no__images__minprice">
                                    <span class="delivery__no__images__Text">с</span>
                                    <span class="delivery__no__images__number">6:00</span>
                                    <span class="delivery__no__images__Text">до</span>
                                    <span class="delivery__no__images__number">9:00</span>
                                </div>
                                <div class="delivery__title delivery__title__custom">Технический перерыв</div>
                            </div>

                            <div class="block__delivery__description">
                                <div class="delivery__no__images delivery__no__images__minprice">
                                    <span class="delivery__no__images__Text">от</span>
                                    <span class="delivery__no__images__number">500</span>
                                    <span class="delivery__no__images__Text">рублей</span>
                                </div>
                                <div class="delivery__title delivery__title__custom">Самовывоз**</div>
                            </div>

                            <div class="delivery__block__spacehack"></div>
                            <div class="delivery__block__spacehack"></div>
                        </div>


                        <h4 class="page__delivery__title">Скидки</h4>

                        <div class="how__to__delivery block__wrapper">
                            <div class="block__delivery__description minprice">
                                <div class="delivery__no__images">
                                    <span class="delivery__no__images__number">20</span>
                                    <span class="delivery__no__images__percent">%</span>
                                </div>
                                <div class="delivery__title">При оформлении самовывоза по телефону</div>
                            </div>
                            <div class="block__delivery__description minprice">
                                <div class="delivery__no__images">
                                    <span class="delivery__no__images__number">10</span>
                                    <span class="delivery__no__images__percent">%</span>
                                </div>
                                <div class="delivery__title">При заказе через мобильное приложение</div>
                            </div>
                            <div class="delivery__block__spacehack"></div>
                        </div>
                        <span style="color: #ffffff; font-size: 12px;">* Время доставки и минимальная сумма могут быть изменены, подробности у оператора.</span>
                        <br>
                        <span style="color: #ffffff; font-size: 12px; display: block; margin-bottom: 20px;">** Суммы указаны с учётом всех скидок.</span>

                    </div>
                </div>
                <div id="map" style="max-width: 1000px; height: 500px; margin: auto;position:relative;">
                    <div class="loader-container">
                        <div class="loader-spinner"></div>
                    </div>
                </div>
                <div id="legend-container" class="_flex">
                    <div class="_flex flex-d-column">
                        <div class="f-781 legend-column">
                            <div class="legend-header">
                                от 781 <i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="f-781 legend-column">
                            <div class="legend-circle" style="background-color: #418cb1">
                            </div>
                            <div class="legend-text">
                                Доставка: 45 мин.
                            </div>
                        </div>
                        <div class="f-781 legend-column">
                            <div class="legend-circle" style="background-color: #78f251">
                            </div>
                            <div class="legend-text">
                                Доставка: 60 мин.
                            </div>
                        </div>
                        <div class="f-781 legend-column">
                            <div class="legend-circle" style="background-color: #e4f74b">
                            </div>
                            <div class="legend-text">
                                Доставка: 75 мин.
                            </div>
                        </div>
                        <div class="f-781 legend-column">
                            <div class="legend-circle" style="background-color: #ffa86d">
                            </div>
                            <div class="legend-text">
                                Доставка: 90 мин.
                            </div>
                        </div>
                        <div class="f-781 legend-column">
                            <div class="legend-circle" style="background-color: #b2b8f9">
                            </div>
                            <div class="legend-text">
                                Доставка: 120 мин.
                            </div>
                        </div>
                    </div>
                    <div class="_flex flex-d-column">
                        <div class="f-1500 legend-column">
                            <div class="legend-header">
                                от 1500 <i class="fa fa-rub" aria-hidden="true"></i>.
                            </div>
                        </div>
                        <div class="f-1500 legend-column">
                            <div class="legend-circle" style="background-color: #f9d67d">
                            </div>
                            <div class="legend-text">
                                Доставка: 75 мин.
                            </div>
                        </div>
                        <div class="f-1500 legend-column">
                            <div class="legend-circle" style="background-color: #ec8b8b">
                            </div>
                            <div class="legend-text">
                                Доставка: 90 мин.
                            </div>
                        </div>
                        <div class="f-1500 legend-column">
                            <div class="legend-circle" style="background-color: #e1b2f9">
                            </div>
                            <div class="legend-text">
                                Доставка: 120 мин.
                            </div>
                        </div>
                    </div>
                    <div class="_flex flex-d-column">
                        <div class="f-781-1500 legend-column">
                            <div class="legend-header" style="line-height: 22px;">
                                от 781 <i class="fa fa-rub" aria-hidden="true"></i> - 12:00-02:00 <br>
                                от 1 500 <i class="fa fa-rub" aria-hidden="true"></i> - в остальное
                            </div>
                        </div>
                        <div class="f-781-1500 legend-column">
                            <div class="legend-circle" style="background-color: #bfb7bc">
                            </div>
                            <div class="legend-text">
                                Доставка: 60 мин.
                            </div>
                        </div>
                        <div class="f-781-1500 legend-column">
                            <div class="legend-circle" style="background-color: #aaebec">
                            </div>
                            <div class="legend-text">
                                Доставка: 75 мин.
                            </div>
                        </div>
                        <div class="f-781-1500 legend-column">
                            <div class="legend-circle" style="background-color: #b1b1b1">
                            </div>
                            <div class="legend-text">
                                Доставка: 90 мин.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        var map;
        ymaps.ready(init);

        function init() {
            var map = new ymaps.Map("map", {
                center: [55.75, 37.64],
                zoom: 10
            });

            $.each(window.zones, function () {
                if (this.name.match(/\(ПП\)/)) {
                    var points = [];
                    var polygon = this;
                    var color = "#" + (this.color).toString(16).substr(2);
                    var hintText = this.description;
                    $.each(polygon.points, function () {
                        points.push([this.y, this.x]);
                    });
                    var myPolygon = new ymaps.Polygon([points], {hintContent: hintText, color: color}, {
                        // Курсор в режиме добавления новых вершин.
                        editorDrawingCursor: "crosshair",
                        // Максимально допустимое количество вершин.
                        editorMaxPoints: 5,
                        // Цвет заливки.
                        fillColor: color,
                        // Цвет обводки.
                        strokeColor: '#000066',
                        // Прозрачность обводки
                        strokeOpacity: 0.7,
                        // Ширина обводки.
                        strokeWidth: 1,
                        opacity: 0.4,
                    });
                    map.geoObjects.add(myPolygon);
                }
            });

            var MyHintLayoutClass = ymaps.templateLayoutFactory.createClass(
                '<div class="my-layout" style="padding: 8px;">' +
                '<span style="white-space: pre; font-size: 14px;">{{ properties.hintContent }}</span>' +
                '</div>'
            );

            map.geoObjects.options.set({
                hintContentLayout: MyHintLayoutClass
//            iconLayout: MyIconLayoutClass, // макет иконок всех объектов карты
//            balloonLayout: MyBalloonLayoutClass, // макет балунов всех объектов карты
//            clusterBalloonLayout: // макет балунов кластеров
            });

            var PlacemarkHintLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="placemark_hint_layout" style="padding: 8px;">' +
                '<div style="font-weight: bold">{{ properties.name }}</div>' +
                '<div >{{ properties.phone }}</div>' +
                '</div>'
            );

            $('.r_restaurant_id').each(function () {
                var rest_dom = $(this).parent('small').prev();
                var name = rest_dom.find('.restourant__name').html();
                var phone = rest_dom.find('.restourant__phone').html();
                var id = $(this).text();
                var rest = new ymaps.Placemark([coords[id].lat, coords[id].long],
                    {
                        name: name,
                        phone: phone
                    },
                    {
                        iconLayout: 'default#image',
                        iconImageHref: '/upload/pointer.png',
                        iconImageSize: [30, 42],
                        iconImageOffset: [-3, -42],
                        hintContentLayout: PlacemarkHintLayout
                    });
                map.geoObjects.add(rest);
            });

            window.map = map;
            //map.geoObjects.add(rest1);
        }

        var fly = function (obj) {
            var top = $("#map").offset().top - $('header').height() - 30;
            $('html, body').animate({
                scrollTop: top
            }, ($('body').scrollTop() <= top) ? 0 : 500, 'swing', function () {
                var id = $(obj).next().find('.r_restaurant_id').text();
                map.panTo([parseFloat(coords[id].lat), parseFloat(coords[id].long)],
                    {
                        flying: true,
                        duration: 1000,
                        //checkZoomRange: true,
                    }
                ).then(function () {
                    if (map.getZoom() <= 11) map.setZoom(15, {duration: 2000});
                });
            });

        };

        $('.restourant__item').each(function () {
            $(this).attr('onclick', 'fly(this);');
        });

    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>